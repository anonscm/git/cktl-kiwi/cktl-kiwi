/*
 * TemplateJDialog.java
 *
 * Created on 1 octobre 2008, 08:26
 */

package org.cocktail.kiwi.client.nibctrl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kiwi.client.metier.EOTarifSncf;
import org.cocktail.kiwi.common.utilities.CocktailFormats;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class TauxSncfView extends javax.swing.JDialog {

	private ZEOTableCellRenderer myRenderer;
	private boolean canUpdate;
	private CellEditor myCellEditor = new CellEditor(new JTextField());

    protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;

    
    /** Creates new form TemplateJDialog */
    public TauxSncfView(java.awt.Frame parent, boolean modal, EODisplayGroup displayGroup, ZEOTableCellRenderer renderer, boolean update) {
        super(parent, modal);

        eod = displayGroup;
        
        myRenderer= renderer;
        
        canUpdate = update;
        
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewTable = new javax.swing.JPanel();
        btnValider = new javax.swing.JButton();
        btnAnnuler = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        classesSncf = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Gestion des taux SNCF");
        setResizable(false);

        viewTable.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        viewTable.setLayout(new java.awt.BorderLayout());

        btnValider.setToolTipText("Valider les modifications");
        btnValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValiderActionPerformed(evt);
            }
        });

        btnAnnuler.setToolTipText("Annuler la saisie");

        jLabel1.setText("CLASSE : ");

        classesSncf.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 51, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(classesSncf, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 127, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(classesSncf, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 530, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(12, 12, 12))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-607)/2, (screenSize.height-636)/2, 607, 636);
    }// </editor-fold>//GEN-END:initComponents

private void btnValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValiderActionPerformed

    dispose();

}//GEN-LAST:event_btnValiderActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	TrajetsSelectView dialog = new TrajetsSelectView(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnValider;
    private javax.swing.JComboBox classesSncf;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

   
    
    
    private void initGui() {
        
    	btnValider.setIcon(CocktailIcones.ICON_VALID);
    	btnAnnuler.setIcon(CocktailIcones.ICON_CANCEL);
        
		Vector myCols = new Vector();
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EOTarifSncf.CLASSE_KEY, "Classe", 30);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EOTarifSncf.DT_DEBUT_KEY, "Début", 30);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EOTarifSncf.DT_FIN_KEY, "Fin", 150);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EOTarifSncf.FACTEUR_KEY, "Facteur", 75);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(true);
		col.setTableCellRenderer(myRenderer);
		col.setTableCellEditor(myCellEditor);			
  	    col.setMyModifier(new FacteurModifier());
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EOTarifSncf.PRIX_KM_KEY, "Prix", 75);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(true);
		col.setTableCellRenderer(myRenderer);
		col.setTableCellEditor(myCellEditor);			
  	    col.setMyModifier(new TarifModifier());
		myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(new Color(230, 230, 230));
		myEOTable.setSelectionBackground(new Color(127,155,165));
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

		classesSncf.removeAllItems();
		classesSncf.addItem("1ère Classe");
		classesSncf.addItem("2ème Classe");
		
    }

    
	private final class CellEditor extends ZEOTableModelColumn.ZEONumFieldTableCellEditor {
		
		private  JTextField myTextField;
		
		public CellEditor(JTextField textField) {
			super(textField, CocktailFormats.FORMAT_DECIMAL);
		}
		
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			
			myTextField = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);
			
			myTextField.setBorder(BorderFactory.createLineBorder(Color.RED));	
			myTextField.setEditable(canUpdate);
			
			return myTextField;
		}
	}

	
	private class TarifModifier implements ZEOTableModelColumn.Modifier {
		
		/**
		 * @see org.cocktail.kiwi.client.swing.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			
			EOTarifSncf tmp = (EOTarifSncf) eod.displayedObjects().objectAtIndex(row);
			
			String stringValue = StringCtrl.replace(value.toString(), ",", ".");
			tmp.setPrixKm(new Double(stringValue));
			
		}		
	} 

	
	
	private class FacteurModifier implements ZEOTableModelColumn.Modifier {
		
		/**
		 * @see org.cocktail.kiwi.client.swing.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			
			EOTarifSncf tmp = (EOTarifSncf) eod.displayedObjects().objectAtIndex(row);
			
			String stringValue = StringCtrl.replace(value.toString(), ",", ".");
			tmp.setFacteur(new Double(stringValue));
			
		}		
	} 

	
	
	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public javax.swing.JButton getBtnValider() {
		return btnValider;
	}

	public void setBtnValider(javax.swing.JButton btnValider) {
		this.btnValider = btnValider;
	}

	public javax.swing.JComboBox getClassesSncf() {
		return classesSncf;
	}

	public void setClassesSncf(javax.swing.JComboBox classesSncf) {
		this.classesSncf = classesSncf;
	}
}
