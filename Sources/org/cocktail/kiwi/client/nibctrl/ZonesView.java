/*
 * KxElementsCtrl.java
 *
 * Created on 24 septembre 2008, 08:33
 */

package org.cocktail.kiwi.client.nibctrl;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class ZonesView extends javax.swing.JFrame {

   	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;
	protected ZEOTableCellRenderer myRenderer;

    /** Creates new form KxElementsCtrl */
    public ZonesView(java.awt.Frame parent, boolean modal, EODisplayGroup eod, ZEOTableCellRenderer renderer) {
        
    	this.eod = eod;
    	myRenderer= renderer;

        initComponents();

        initGui();

    }

    public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewTable = new javax.swing.JPanel();
        btnAjouter = new javax.swing.JButton();
        btnModifier = new javax.swing.JButton();
        btnSupprimer = new javax.swing.JButton();
        btnFermer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("GESTION DES ZONES");
        setResizable(false);

        viewTable.setLayout(new java.awt.BorderLayout());

        btnAjouter.setToolTipText("Edition d'un arrêté");
        btnAjouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjouterActionPerformed(evt);
            }
        });

        btnModifier.setToolTipText("Edition d'un arrêté");
        btnModifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifierActionPerformed(evt);
            }
        });

        btnSupprimer.setToolTipText("Edition d'un arrêté");
        btnSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSupprimerActionPerformed(evt);
            }
        });

        btnFermer.setText("Fermer");
        btnFermer.setToolTipText("");
        btnFermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFermerActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(btnFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 113, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(btnFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-414)/2, (screenSize.height-457)/2, 414, 457);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAjouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjouterActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_btnAjouterActionPerformed

    private void btnModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifierActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnModifierActionPerformed

    private void btnSupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSupprimerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSupprimerActionPerformed

    private void btnFermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFermerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnFermerActionPerformed

//    /**
//    * @param args the command line arguments
//    */
//    public static void main(String args[]) {
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new KxElementsView().setVisible(true);
//            }
//        });
//    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjouter;
    private javax.swing.JButton btnFermer;
    private javax.swing.JButton btnModifier;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

        
    /**
     * 
     */
    private void initGui() {
    	                
    	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        btnFermer .setIcon(CocktailIcones.ICON_CLOSE);
                
        btnAjouter.setIcon(CocktailIcones.ICON_ADD);
        btnModifier.setIcon(CocktailIcones.ICON_UPDATE);
        btnSupprimer.setIcon(CocktailIcones.ICON_DELETE);

        Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EORembZone.REM_TAUX_KEY, "CODE", 40);
		col.setTableCellRenderer(myRenderer);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EORembZone.REM_LIBELLE_KEY, "LIBELLE", 200);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EORembZone.TEM_VALIDE_KEY, "Val.", 40);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
		myCols.add(col);
                
		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    }

	public javax.swing.JButton getBtnAjouter() {
		return btnAjouter;
	}

	public void setBtnAjouter(javax.swing.JButton btnAjouter) {
		this.btnAjouter = btnAjouter;
	}

	public javax.swing.JButton getBtnFermer() {
		return btnFermer;
	}

	public void setBtnFermer(javax.swing.JButton btnFermer) {
		this.btnFermer = btnFermer;
	}

	public javax.swing.JButton getBtnModifier() {
		return btnModifier;
	}

	public void setBtnModifier(javax.swing.JButton btnModifier) {
		this.btnModifier = btnModifier;
	}

	public javax.swing.JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(javax.swing.JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

}
