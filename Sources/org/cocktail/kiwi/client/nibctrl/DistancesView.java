/*
 * KxElementsCtrl.java
 *
 * Created on 24 septembre 2008, 08:33
 */

package org.cocktail.kiwi.client.nibctrl;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kiwi.client.metier.EODistancesKm;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;

import com.webobjects.eointerface.EODisplayGroup;
/**
 *
 * @author  cpinsard
 */
public class DistancesView extends javax.swing.JFrame {

   	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;

    /** Creates new form KxElementsCtrl */
    public DistancesView() {
        
    	eod = new EODisplayGroup();
    	
        initComponents();

        initGui();

    }

    public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewTable = new javax.swing.JPanel();
        buttonClose = new javax.swing.JButton();
        filtreDepart = new javax.swing.JTextField();
        filtreArrivee = new javax.swing.JTextField();
        buttonAdd = new javax.swing.JButton();
        buttonUpdate = new javax.swing.JButton();
        buttonDelete = new javax.swing.JButton();
        lblNbVehicules = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Gestion des distances");
        setResizable(false);

        viewTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        viewTable.setLayout(new java.awt.BorderLayout());

        buttonClose.setText("Fermer");
        buttonClose.setToolTipText("Fermer la fenêtre");
        buttonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCloseActionPerformed(evt);
            }
        });

        filtreDepart.setFont(new java.awt.Font("Tahoma", 0, 10));

        filtreArrivee.setFont(new java.awt.Font("Tahoma", 0, 10));

        buttonAdd.setToolTipText("Ajouter un nouveau trajet");

        buttonUpdate.setToolTipText("Modifier le trajet sélectionné");

        buttonDelete.setToolTipText("Supprimer le trajet sélectionné");

        lblNbVehicules.setForeground(new java.awt.Color(102, 102, 255));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(12, 12, 12)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(filtreDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 124, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(filtreArrivee, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 130, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 420, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(buttonDelete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 30, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(buttonUpdate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 30, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(buttonAdd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 30, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                    .add(buttonClose, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 94, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(lblNbVehicules, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 111, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(574, 574, 574)
                .add(lblNbVehicules, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(27, 27, 27))
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(filtreDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(filtreArrivee, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(buttonAdd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonUpdate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonDelete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 533, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 13, Short.MAX_VALUE)
                .add(buttonClose)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-504)/2, (screenSize.height-654)/2, 504, 654);
    }// </editor-fold>//GEN-END:initComponents

private void buttonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCloseActionPerformed
// TODO add your handling code here:
    dispose();
}//GEN-LAST:event_buttonCloseActionPerformed

//    /**
//    * @param args the command line arguments
//    */
//    public static void main(String args[]) {
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new KxElementsView().setVisible(true);
//            }
//        });
//    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton buttonAdd;
    private javax.swing.JButton buttonClose;
    protected javax.swing.JButton buttonDelete;
    protected javax.swing.JButton buttonUpdate;
    protected javax.swing.JTextField filtreArrivee;
    protected javax.swing.JTextField filtreDepart;
    protected javax.swing.JLabel lblNbVehicules;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

        
    private void initGui() {
    	                
        buttonClose.setIcon(CocktailIcones.ICON_CLOSE);
                
        buttonAdd.setIcon(CocktailIcones.ICON_ADD);
        buttonUpdate.setIcon(CocktailIcones.ICON_UPDATE);
        buttonDelete.setIcon(CocktailIcones.ICON_DELETE);

        Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EODistancesKm.LIEU_DEPART_KEY, "DEPART", 150);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EODistancesKm.LIEU_ARRIVEE_KEY, "ARRIVEE", 150);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EODistancesKm.DISTANCE_KEY, "Kms", 75);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
                
		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    }

}
