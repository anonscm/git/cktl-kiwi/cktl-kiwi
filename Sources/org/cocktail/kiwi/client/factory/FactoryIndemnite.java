package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.kiwi.client.finders.FinderWebMiss;
import org.cocktail.kiwi.client.metier.EOIndemnite;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOWebmiss;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryIndemnite {

	private static FactoryIndemnite sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryIndemnite sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryIndemnite();
		return sharedInstance;
	}
	
    /**Initialise un object de la classe Indemnite
     * @param segmentTarif son segment
     * @param webmiss son indemnite de base 
     * @param calculNb doit on faire le calcul du nombre d indemnite
     * @param calculMontant doit on calculer le montant de l indemenite
     * @param etranger est un segment a l etranger
     */
     public EOIndemnite creerIndemnites(EOEditingContext ec, EOSegmentTarif segmentTarif,EOWebmiss webmiss) {

    	 EOIndemnite indemnite = (EOIndemnite)Factory.instanceForEntity(ec, EOIndemnite.ENTITY_NAME);
    	 
    	 indemnite.addObjectToBothSidesOfRelationshipWithKey(segmentTarif,"segmentTarif");
    	 indemnite.addObjectToBothSidesOfRelationshipWithKey(webmiss,"webmiss");
    	 indemnite.setIndMontantTotal(new BigDecimal(0));
    	 indemnite.setIndMontantPaiement(new BigDecimal(0));
    	 indemnite.setIndJours(new BigDecimal(0));
    	 indemnite.setIndJoursGratuits(new BigDecimal(0));
    	 indemnite.setIndRepasGratuits(new Integer(0));
    	 indemnite.setIndNuitsGratuites(new Integer(0));
    	 indemnite.setIndEtat("VALIDE");

    	 indemnite.setIndMontantAuto(new BigDecimal(0));
    	 indemnite.setIndJoursAuto(new BigDecimal(0));
    	 indemnite.setIndNuitsAuto(new Integer(0));
    	 indemnite.setIndRepasAuto(new Integer(0));

    	 ec.insertObject(indemnite);
    	 return indemnite;
     }

 	/**
 	 * 
 	 * @param ec
 	 * @param segment
 	 */
 	public static void associerIndemnitesSegment(EOEditingContext ec, EOSegmentTarif segment)	{
 		
		EOWebmiss localWebMiss = (EOWebmiss)FinderWebMiss.findWebMissForPaysAndPeriode(ec, segment.webpays(), segment.segDebut(), segment.segFin());

 		NSArray indemnites = segment.indemnites();
 		for (int i=0;i<indemnites.count();i++)	{
 			EOIndemnite indemnite =(EOIndemnite)indemnites.objectAtIndex(i);
 			indemnite.addObjectToBothSidesOfRelationshipWithKey(segment,"segmentTarif");
 			indemnite.addObjectToBothSidesOfRelationshipWithKey(localWebMiss,"webmiss");
 		}
 	}
 	
	/**
	 * 
	 * @param ec
	 * @param segment
	 */
	public void supprimerIndemnites(EOEditingContext ec, EOSegmentTarif segment)	{
		
		NSArray indemnites = segment.indemnites();
		for (int i=0;i<indemnites.count();i++)	{
			EOIndemnite indemnite =(EOIndemnite)indemnites.objectAtIndex(i);
			indemnite.removeObjectFromBothSidesOfRelationshipWithKey(segment,"segmentTarif");
			ec.deleteObject(indemnite);
		}
	}
	
	
	
}
