package org.cocktail.kiwi.client.factory;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionReimput;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryMissionReimput {

	private static FactoryMissionReimput sharedInstance;

	/**
	 * 
	 * @return
	 */
	public static FactoryMissionReimput sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryMissionReimput();
		return sharedInstance;
	}
	

	/**
	 * 
	 * @param ec
	 * @param missionPaiement
	 * @param depense
	 * @return
	 */
	public EOMissionReimput creer ( EOEditingContext ec, EOMissionPaiement missionPaiement, EODepense depense)	{
		
		EOMissionReimput record = (EOMissionReimput)Factory.instanceForEntity(ec, EOMissionReimput.ENTITY_NAME);
			
		record.setMissionPaiementRelationship(missionPaiement);
		
		record.setDepenseRelationship(depense);
				
		ec.insertObject(record);
		
		return record;
	}

	/**
	 * 
	 * @param exercice
	 * @param record
	 * @param typeCredit
	 * @param organ
	 * @param planComptable
	 * @param convention
	 * @param canal
	 * @param codeExer
	 */
	public void update(EOExercice exercice, EOMissionReimput record, 
			EOTypeCredit typeCredit, EOOrgan organ, EOPlanComptable planComptable,
			EOConvention convention, EOCodeAnalytique canal, EOCodeExer codeExer)	{

		record.setOrganRelationship(organ);
		record.setToTypeCreditRelationship(typeCredit);
		record.setToCodeExerRelationship(codeExer);
		record.setConventionRelationship(convention);
		record.setCodeAnalytiqueRelationship(canal);

		if (planComptable != null) {
			record.setPlanComptableRelationship(planComptable);
			record.setPcoNum(planComptable.pcoNum());
		}
	}

}
