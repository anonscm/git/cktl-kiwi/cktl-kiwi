package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOTypeTransport;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryTypeTransport {

	private static FactoryTypeTransport sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryTypeTransport sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryTypeTransport();
		return sharedInstance;
	}
	

	
	/**
	 * 
	 * Creation d'un nouvel object Vehicule.
	 * 
	 * @param ec
	 * @param fournisseur	Fournisseur associe
	 * @param typeTransport	Type de vehicule
	 * @return
	 */
	public EOTypeTransport creer(EOEditingContext ec) {
		
		// On regarde s'il n'y a pas plusieurs remboursements journaliers.
		
		EOTypeTransport record = (EOTypeTransport)Factory.instanceForEntity(ec, EOTypeTransport.ENTITY_NAME);
				
		record.setTemValide("O");
		
		ec.insertObject(record);
		
		return record;
		
	}	
	
}
