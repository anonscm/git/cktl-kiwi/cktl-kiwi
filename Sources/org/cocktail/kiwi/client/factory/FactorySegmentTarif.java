package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOIndemnite;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EONuits;
import org.cocktail.kiwi.client.metier.EORepas;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTransports;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactorySegmentTarif {

	private static FactorySegmentTarif sharedInstance;

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static FactorySegmentTarif sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactorySegmentTarif();
		return sharedInstance;
	}

	/**
	 * 
	 * Creation d'un nouvel objet EOSegmentInfos
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public EOSegmentTarif creerSegmentTarif(EOEditingContext ec, EOMission mission)	{

		EOSegmentTarif segment = (EOSegmentTarif)Factory.instanceForEntity(ec, EOSegmentTarif.ENTITY_NAME);

		segment.addObjectToBothSidesOfRelationshipWithKey(mission, "mission");

		segment.setSegPayeur(mission.misPayeur());

		segment.setSegDebut(mission.misDebut());
		segment.setSegFin(mission.misFin());

		segment.setSegEtat("VALIDE");

		ec.insertObject(segment);

		return segment;		
	}

	
	
	
	/**
	 * 
	 * @param ec
	 * @param mission
	 * @param missionReference
	 */
	public EOSegmentTarif dupliquerSegmentTarif(EOEditingContext ec, EOMission mission, 
			EOMission missionReference, boolean addNuits, boolean addTransports, boolean addIndemnites)	{

		EOSegmentTarif newTrajet = null;

		NSArray<EOSegmentTarif> segments = EOSegmentTarif.findSegmentsTarifsForMission(ec, missionReference);

		for (EOSegmentTarif segment : segments)	 {

			newTrajet = creerSegmentTarif(ec, mission);

			newTrajet.setRembZoneRelationship(segment.rembZone());
			newTrajet.setWebpaysRelationship(segment.webpays());
			newTrajet.setWebtauxRelationship(segment.webtaux());

			newTrajet.setRemTaux(segment.remTaux());
			newTrajet.setWpaCode(segment.wpaCode());
			newTrajet.setSegDebut(segment.segDebut());
			newTrajet.setSegFin(segment.segFin());
			newTrajet.setSegLibelle(segment.segLibelle());
			newTrajet.setSegPayeur(segment.segPayeur());
			newTrajet.setSegLieuDepart(segment.segLieuDepart());
			newTrajet.setSegLieuDestination(segment.segLieuDestination());

			System.err.println("DUPLICATION TRAJET DU " + segment.segDebut() + " au " + segment.segFin());
			
			if (addNuits) {

				NSArray<EONuits> nuits = EONuits.findForSegment(ec, segment);
				for (EONuits nuit : nuits) {

					EONuits newNuit = FactoryNuits.sharedInstance().creerNuits(ec, newTrajet, nuit.rembJournalier(), nuit.webmiss());

					newNuit.setNuiEtat(nuit.nuiEtat());
					newNuit.setNuiMontantPaiement(nuit.nuiMontantPaiement());
					newNuit.setNuiNbNuits(nuit.nuiNbNuits());
					newNuit.setNuiNuitGratuits(nuit.nuiNuitGratuits());

				}

				NSArray<EORepas> repas = EORepas.findForSegment(ec, segment);
				for (EORepas unRepas : repas) {

					EORepas newRepas = FactoryRepas.sharedInstance().creerRepas(ec, newTrajet, unRepas.rembJournalier(), unRepas.webmiss());

					newRepas.setRepEtat(unRepas.repEtat());
					newRepas.setRepMontantPaiement(unRepas.repMontantPaiement());
					newRepas.setRepNbRepas(unRepas.repNbRepas());
					newRepas.setRepRepasAdm(unRepas.repRepasAdm());
					newRepas.setRepRepasGratuits(unRepas.repRepasGratuits());

				}

			}

			if (addTransports) {

				NSArray transports = EOTransports.findForSegment(ec, segment);
				for (int t=0;t<transports.count();t++) {

					EOTransports unTransport = (EOTransports)transports.objectAtIndex(t);
					EOTransports newTransport  = (EOTransports)Factory.instanceForEntity(ec, EOTransports.ENTITY_NAME);

					newTransport.setSegmentTarifRelationship(newTrajet);
					newTransport.setWebtauxRelationship(unTransport.webtaux());
					newTransport.setIndemniteKmRelationship(unTransport.indemniteKm());
					newTransport.setTarifSncfRelationship(unTransport.tarifSncf());
					newTransport.setTypeTransportRelationship(unTransport.typeTransport());
					newTransport.setVehiculeRelationship(unTransport.vehicule());
					
					newTransport.setTraEtat(unTransport.traEtat());
					newTransport.setTraDepart(unTransport.traDepart());
					newTransport.setTraArrivee(unTransport.traArrivee());
					newTransport.setTraDuree(unTransport.traDuree());
					newTransport.setTraKmSaisie(unTransport.traKmSaisie());
					newTransport.setTraLibFrais(unTransport.traLibFrais());
					newTransport.setTraMontant(unTransport.traMontant());
					newTransport.setTraMontantKm(unTransport.traMontantKm());
					newTransport.setTraMontantPaiement(unTransport.traMontantPaiement());
					newTransport.setTraUtilisateur(unTransport.traUtilisateur());

					newTransport.setSegKm1(unTransport.segKm1());
					newTransport.setSegKm2(unTransport.segKm2());
					newTransport.setSegKm3(unTransport.segKm3());

					ec.insertObject(newTransport);

				}

				if (addIndemnites) {

					NSArray indemnites = EOIndemnite.findForSegment(ec, segment);
					for (int ind=0;ind<indemnites.count();ind++) {

						EOIndemnite uneIndemnite = (EOIndemnite)indemnites.objectAtIndex(ind);
						EOIndemnite newIndemnite = FactoryIndemnite.sharedInstance().creerIndemnites(ec, newTrajet, uneIndemnite.webmiss());

						newIndemnite.setIndEtat(uneIndemnite.indEtat());
						newIndemnite.setIndJours(uneIndemnite.indJours());
						newIndemnite.setIndJoursAuto(uneIndemnite.indJoursAuto());
						newIndemnite.setIndJoursGratuits(uneIndemnite.indJoursGratuits());
						newIndemnite.setIndMontantAuto(uneIndemnite.indMontantAuto());
						newIndemnite.setIndMontantPaiement(uneIndemnite.indMontantPaiement());
						newIndemnite.setIndMontantSaisi(uneIndemnite.indMontantSaisi());
						newIndemnite.setIndMontantTotal(uneIndemnite.indMontantTotal());
						newIndemnite.setIndNbNuits(uneIndemnite.indNbNuits());
						newIndemnite.setIndNbRepas(uneIndemnite.indNbRepas());
						newIndemnite.setIndNuitsAuto(uneIndemnite.indNuitsAuto());
						newIndemnite.setIndNuitsGratuites(uneIndemnite.indNuitsGratuites());
						newIndemnite.setIndRepasAuto(uneIndemnite.indRepasAuto());
						newIndemnite.setIndRepasGratuits(uneIndemnite.indRepasGratuits());
						newIndemnite.setIndNbNuits(uneIndemnite.indNbNuits());

					}

				}	    		

			}
		}	

		return newTrajet;

	}
	
	/**
	 * 
	 * @param ec
	 * @param mission
	 * @param missionReference
	 */
	public EOSegmentTarif dupliquerTrajet(EOEditingContext ec, EOMission mission, EOSegmentTarif segment, 
			EOMission missionReference, boolean addNuits, boolean addTransports, boolean addIndemnites)	{

		EOSegmentTarif newTrajet = null;

			newTrajet = creerSegmentTarif(ec, mission);

			newTrajet.setRembZoneRelationship(segment.rembZone());
			newTrajet.setWebpaysRelationship(segment.webpays());
			newTrajet.setWebtauxRelationship(segment.webtaux());

			newTrajet.setRemTaux(segment.remTaux());
			newTrajet.setWpaCode(segment.wpaCode());
			newTrajet.setSegDebut(segment.segDebut());
			newTrajet.setSegFin(segment.segFin());
			newTrajet.setSegLibelle(segment.segLibelle());
			newTrajet.setSegPayeur(segment.segPayeur());
			newTrajet.setSegLieuDepart(segment.segLieuDepart());
			newTrajet.setSegLieuDestination(segment.segLieuDestination());
			newTrajet.setSegEtat(EOMission.ETAT_VALIDE);
			
			if (addNuits) {

				NSArray<EONuits> nuits = EONuits.findForSegment(ec, segment);
				for (EONuits nuit : nuits) {

					EONuits newNuit = FactoryNuits.sharedInstance().creerNuits(ec, newTrajet, nuit.rembJournalier(), nuit.webmiss());

					newNuit.setNuiEtat(nuit.nuiEtat());
					newNuit.setNuiMontantPaiement(nuit.nuiMontantPaiement());
					newNuit.setNuiNbNuits(nuit.nuiNbNuits());
					newNuit.setNuiNuitGratuits(nuit.nuiNuitGratuits());

				}

				NSArray<EORepas> repas = EORepas.findForSegment(ec, segment);
				for (EORepas unRepas : repas) {

					EORepas newRepas = FactoryRepas.sharedInstance().creerRepas(ec, newTrajet, unRepas.rembJournalier(), unRepas.webmiss());

					newRepas.setRepEtat(unRepas.repEtat());
					newRepas.setRepMontantPaiement(unRepas.repMontantPaiement());
					newRepas.setRepNbRepas(unRepas.repNbRepas());
					newRepas.setRepRepasAdm(unRepas.repRepasAdm());
					newRepas.setRepRepasGratuits(unRepas.repRepasGratuits());

				}

			}

			if (addTransports) {

				NSArray transports = EOTransports.findForSegment(ec, segment);
				for (int t=0;t<transports.count();t++) {

					EOTransports unTransport = (EOTransports)transports.objectAtIndex(t);
					EOTransports newTransport  = (EOTransports)Factory.instanceForEntity(ec, EOTransports.ENTITY_NAME);

					newTransport.setSegmentTarifRelationship(newTrajet);
					newTransport.setWebtauxRelationship(unTransport.webtaux());
					newTransport.setIndemniteKmRelationship(unTransport.indemniteKm());
					newTransport.setTarifSncfRelationship(unTransport.tarifSncf());
					newTransport.setTypeTransportRelationship(unTransport.typeTransport());
					newTransport.setVehiculeRelationship(unTransport.vehicule());
					
					newTransport.setTraEtat(unTransport.traEtat());
					newTransport.setTraDepart(unTransport.traDepart());
					newTransport.setTraArrivee(unTransport.traArrivee());
					newTransport.setTraDuree(unTransport.traDuree());
					newTransport.setTraKmSaisie(unTransport.traKmSaisie());
					newTransport.setTraLibFrais(unTransport.traLibFrais());
					newTransport.setTraMontant(unTransport.traMontant());
					newTransport.setTraMontantKm(unTransport.traMontantKm());
					newTransport.setTraMontantPaiement(unTransport.traMontantPaiement());
					newTransport.setTraUtilisateur(unTransport.traUtilisateur());

					newTransport.setSegKm1(unTransport.segKm1());
					newTransport.setSegKm2(unTransport.segKm2());
					newTransport.setSegKm3(unTransport.segKm3());

					ec.insertObject(newTransport);

				}

				if (addIndemnites) {

					NSArray indemnites = EOIndemnite.findForSegment(ec, segment);
					for (int ind=0;ind<indemnites.count();ind++) {

						EOIndemnite uneIndemnite = (EOIndemnite)indemnites.objectAtIndex(ind);
						EOIndemnite newIndemnite = FactoryIndemnite.sharedInstance().creerIndemnites(ec, newTrajet, uneIndemnite.webmiss());

						newIndemnite.setIndEtat(uneIndemnite.indEtat());
						newIndemnite.setIndJours(uneIndemnite.indJours());
						newIndemnite.setIndJoursAuto(uneIndemnite.indJoursAuto());
						newIndemnite.setIndJoursGratuits(uneIndemnite.indJoursGratuits());
						newIndemnite.setIndMontantAuto(uneIndemnite.indMontantAuto());
						newIndemnite.setIndMontantPaiement(uneIndemnite.indMontantPaiement());
						newIndemnite.setIndMontantSaisi(uneIndemnite.indMontantSaisi());
						newIndemnite.setIndMontantTotal(uneIndemnite.indMontantTotal());
						newIndemnite.setIndNbNuits(uneIndemnite.indNbNuits());
						newIndemnite.setIndNbRepas(uneIndemnite.indNbRepas());
						newIndemnite.setIndNuitsAuto(uneIndemnite.indNuitsAuto());
						newIndemnite.setIndNuitsGratuites(uneIndemnite.indNuitsGratuites());
						newIndemnite.setIndRepasAuto(uneIndemnite.indRepasAuto());
						newIndemnite.setIndRepasGratuits(uneIndemnite.indRepasGratuits());
						newIndemnite.setIndNbNuits(uneIndemnite.indNbNuits());

					}

				}	    		

			}

		return newTrajet;

	}

}