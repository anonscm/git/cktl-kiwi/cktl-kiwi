package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTransports;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryTransports {

	private static FactoryTransports sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryTransports sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryTransports();
		return sharedInstance;
	}
	

	
	/**
	 * 
	 * @param ec
	 * @param segment
	 */
	public static void supprimerTransports(EOEditingContext ec, EOSegmentTarif segment)	{
		
		NSArray transports = segment.transports();
		for (int i=0;i<transports.count();i++)	{
			EOTransports transport =(EOTransports)transports.objectAtIndex(i);
			transport.removeObjectFromBothSidesOfRelationshipWithKey(segment,"segmentTarif");
			ec.deleteObject(transport);
		}
	}
	
	
 	/**
 	 * 
 	 * @param ec
 	 * @param segment
 	 */
 	public static void associerIndemnitesSegment(EOEditingContext ec, EOSegmentTarif segment)	{
 		
 		NSArray transports = segment.transports();
 		for (int i=0;i<transports.count();i++)	{

 			EOTransports transport =(EOTransports)transports.objectAtIndex(i);
 			transport.setSegmentTarifRelationship(segment);
 			transport.setWebtauxRelationship(segment.webtaux());
 			
 			transport.calculerMontantPaiement();
 		}
 	}
	
}
