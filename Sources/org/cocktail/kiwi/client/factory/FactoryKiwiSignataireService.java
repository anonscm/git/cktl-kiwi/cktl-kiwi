package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.finders.FinderKiwiSignataireService;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireService;
import org.cocktail.kiwi.client.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryKiwiSignataireService {

	private static FactoryKiwiSignataireService sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryKiwiSignataireService sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryKiwiSignataireService();
		return sharedInstance;
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param organ
	 * @return
	 */
     public EOKiwiSignataireService creer(EOEditingContext ec, EOIndividu individu, EOStructure structure) {

    	 // On regarde si le signataire existe deja ou non 
    	EOKiwiSignataireService sign = FinderKiwiSignataireService.findSignataireForIndividuAndService(ec, individu, structure);

    	 if (sign != null)
    		 return null;
    	 
    	 EOKiwiSignataireService signataire = (EOKiwiSignataireService)Factory.instanceForEntity(ec, EOKiwiSignataireService.ENTITY_NAME);
    	 
    	 signataire.addObjectToBothSidesOfRelationshipWithKey(individu, EOKiwiSignataireService.INDIVIDU_KEY);
    	 signataire.addObjectToBothSidesOfRelationshipWithKey(structure,EOKiwiSignataireService.STRUCTURE_KEY);

    	 ec.insertObject(signataire);
    	 return signataire;
     }
	
	
}
