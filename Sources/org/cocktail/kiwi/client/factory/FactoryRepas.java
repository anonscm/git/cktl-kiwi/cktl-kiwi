package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.finders.FinderRembJournalier;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOPlageHoraire;
import org.cocktail.kiwi.client.metier.EORembJournalier;
import org.cocktail.kiwi.client.metier.EORepas;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOWebmiss;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryRepas {

	private static FactoryRepas sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryRepas sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryRepas();
		return sharedInstance;
	}
	
	/**Initialise un object de la classe EORepas
	 * @param segmentTarif son segment
	 * @param rembJournalier remboursement Journalier 
	 * @param webmiss son indemnite de base 
	 */
	public EORepas creerRepas(EOEditingContext ec, EOSegmentTarif segmentTarif,
			EORembJournalier rembJournalier, EOWebmiss webmiss) {
		
		// On regarde s'il n'y a pas plusieurs remboursements journaliers.
		
		EORepas repas = (EORepas)Factory.instanceForEntity(ec, EORepas.ENTITY_NAME);
		
		repas.setSegmentTarifRelationship(segmentTarif);
		repas.setWebmissRelationship(webmiss);
		repas.setRembJournalierRelationship(rembJournalier);
		repas.setRepMontantPaiement(new BigDecimal(0));
		repas.setRepNbRepas(new BigDecimal(0));
		repas.setRepRepasGratuits(new BigDecimal(0));
		repas.setRepRepasAdm(new BigDecimal(0));
		repas.setRepEtat("VALIDE");
						
		ec.insertObject(repas);
		return repas;
		
	}
	
	/**
	 * 
	 * @param ec
	 * @param segment
	 */
	public static void associerRepasSegment(EOEditingContext ec, EOSegmentTarif segment)	{
		
		NSArray repass = segment.repas();
		for (int i=0;i<repass.count();i++)	{
			EORepas repas =(EORepas)repass.objectAtIndex(i);
			repas.addObjectToBothSidesOfRelationshipWithKey(segment,"segmentTarif");
			EORembJournalier localRembJournalier = (EORembJournalier)FinderRembJournalier.findRembForZoneAndPeriode(ec, segment.rembZone(), new Integer(2), segment.segDebut(), segment.segFin());
			repas.addObjectToBothSidesOfRelationshipWithKey(localRembJournalier, "rembJournalier");
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param segment
	 */
	public static void supprimerRepas(EOEditingContext ec, EOSegmentTarif segment)	{
		
		NSArray repass = segment.repas();
		for (int i=0;i<repass.count();i++)	{
			EORepas repas =(EORepas)repass.objectAtIndex(i);
			repas.removeObjectFromBothSidesOfRelationshipWithKey(segment,"segmentTarif");
			ec.deleteObject(repas);
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param segment
	 * @param utilisateur
	 * @throws Exception
	 */
	public void calculerRepas(EOEditingContext ec, EOSegmentTarif segment, EOUtilisateur utilisateur) throws Exception {

		try {
			EORembJournalier localRembJournalier = null;

			// On verifie le nombre de remboursements journaliers
			NSArray rembs = FinderRembJournalier.findRembsForZoneAndPeriode(ec, segment.rembZone(), new Integer(EOPlageHoraire.CLE_REPAS_MIDI), segment.segDebut(), segment.segFin());

			if (rembs.count() == 0)
				throw new Exception("Aucun tarif n'est defini pour les repas !!!");

			EORepas currentRepas = null;

			if (rembs.count() == 1)	{
				localRembJournalier = (EORembJournalier)rembs.objectAtIndex(0);
				currentRepas = FactoryRepas.sharedInstance().creerRepas(ec, segment, localRembJournalier, null);
				currentRepas.calculerRepas(ec, segment.segDebut(), segment.segFin());
				currentRepas.calculerMontant();
			}
			else	{
				localRembJournalier = (EORembJournalier)rembs.objectAtIndex(0);

				NSTimestamp rembFin = DateCtrl.stringToDate(DateCtrl.dateToString(localRembJournalier.remDateFin()) + " 23:59","%d/%m/%Y %H:%M"); 

				currentRepas = FactoryRepas.sharedInstance().creerRepas(ec, segment, localRembJournalier, null);
				currentRepas.calculerRepas(ec, segment.segDebut(), rembFin);
				currentRepas.calculerMontant();					

				localRembJournalier = (EORembJournalier)rembs.objectAtIndex(1);

				NSTimestamp rembDebut = DateCtrl.stringToDate(DateCtrl.dateToString(localRembJournalier.remDate()) + " 00:01","%d/%m/%Y %H:%M"); 

				currentRepas = FactoryRepas.sharedInstance().creerRepas(ec, segment, localRembJournalier, null);
				currentRepas.calculerRepas(ec, rembDebut, segment.segFin());
				currentRepas.calculerMontant();					
			}

			segment.mission().addObjectToBothSidesOfRelationshipWithKey(utilisateur, EOMission.UTILISATEUR_MODIFICATION_KEY);		
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}
	
	
}
