package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.finders.FinderRembJournalier;
import org.cocktail.kiwi.client.metier.EONuits;
import org.cocktail.kiwi.client.metier.EORembJournalier;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOWebmiss;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryNuits {
	
	private static FactoryNuits sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryNuits sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryNuits();
		return sharedInstance;
	}
	
	/**Initialise un object de la classe EONuits
	 * @param segmentTarif son segment
	 * @param rembJournalier remboursement Journalier 
	 * @param webmiss son indemnite de base 
	 */
	public EONuits creerNuits(EOEditingContext ec, EOSegmentTarif segmentTarif,
			EORembJournalier rembJournalier, EOWebmiss webmiss) {
		
		EONuits nuit = (EONuits)Factory.instanceForEntity(ec, EONuits.ENTITY_NAME);
		
		nuit.addObjectToBothSidesOfRelationshipWithKey(segmentTarif,"segmentTarif");
		nuit.addObjectToBothSidesOfRelationshipWithKey(webmiss,"webmiss");
		nuit.addObjectToBothSidesOfRelationshipWithKey(rembJournalier,"rembJournalier");
		nuit.setNuiMontantPaiement(new BigDecimal(0));
		nuit.setNuiNbNuits(new Integer(0));
		nuit.setNuiNuitGratuits(new Integer(0));
		nuit.setNuiEtat("VALIDE");
				
		ec.insertObject(nuit);
		return nuit;
		
	}
	
	/**
	 * 
	 * @param ec
	 * @param segment
	 */
	public static void associerNuitsSegment(EOEditingContext ec, EOSegmentTarif segment)	{
		
		NSArray nuits = segment.nuits();
		for (int i=0;i<nuits.count();i++)	{
			EONuits nuit =(EONuits)nuits.objectAtIndex(i);
			nuit.addObjectToBothSidesOfRelationshipWithKey(segment,"segmentTarif");

			EORembJournalier localRembJournalier = (EORembJournalier)FinderRembJournalier.findRembForZoneAndPeriode(ec, segment.rembZone(), new Integer(1), segment.segDebut(), segment.segFin());
			nuit.addObjectToBothSidesOfRelationshipWithKey(localRembJournalier, "rembJournalier");
		}
	}

	/**
	 * 
	 * @param ec
	 * @param segment
	 */
	public static void supprimerNuits(EOEditingContext ec, EOSegmentTarif segment)	{
		
		NSArray nuits = segment.nuits();
		for (int i=0;i<nuits.count();i++)	{
			EONuits nuit =(EONuits)nuits.objectAtIndex(i);
			nuit.removeObjectFromBothSidesOfRelationshipWithKey(segment,"segmentTarif");
			ec.deleteObject(nuit);
		}
	}

	/**
	 * 
	 * @param ec
	 * @param segment
	 */
	public void supprimerNuit(EOEditingContext ec, EONuits nuit)	{		
			nuit.removeObjectFromBothSidesOfRelationshipWithKey(nuit.segmentTarif(),EONuits.SEGMENT_TARIF_KEY);
			ec.deleteObject(nuit);
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param segment
	 * @param utilisateur
	 * @throws Exception
	 */
	public void calculerNuits(EOEditingContext ec, EOSegmentTarif segment, EOUtilisateur utilisateur) throws Exception {
		
		try {
			EONuits currentNuit = null;
			EORembJournalier localRembJournalier = null;

			// On verifie le nombre de remboursements journaliers
			NSArray rembs = FinderRembJournalier.findRembsForZoneAndPeriode(ec, segment.rembZone(), new Integer(1), segment.segDebut(), segment.segFin());

			if (rembs.count() == 0)
				throw new Exception("Aucun tarif n'est defini pour les nuits !!!");

			if (rembs.count() == 1)	{
				localRembJournalier = (EORembJournalier)rembs.objectAtIndex(0);
				currentNuit = FactoryNuits.sharedInstance().creerNuits(ec, segment, localRembJournalier, null);
				currentNuit.calculerNuits(ec, segment.segDebut(), segment.segFin());
				currentNuit.calculerMontant();
			}
			else	{

				EORembJournalier localRembJournalier1 = (EORembJournalier)rembs.objectAtIndex(0);
				EORembJournalier localRembJournalier2 = (EORembJournalier)rembs.objectAtIndex(1);

				NSTimestamp rembFin = DateCtrl.stringToDate(DateCtrl.dateToString(localRembJournalier2.remDate()) + " 05:01","%d/%m/%Y %H:%M"); 

				currentNuit = FactoryNuits.sharedInstance().creerNuits(ec, segment, localRembJournalier1, null);
				currentNuit.calculerNuits(ec, segment.segDebut(), rembFin);
				currentNuit.calculerMontant();					

				NSTimestamp rembDebut = DateCtrl.stringToDate(DateCtrl.dateToString(localRembJournalier2.remDate()) + " 05:02","%d/%m/%Y %H:%M"); 

				currentNuit = FactoryNuits.sharedInstance().creerNuits(ec, segment, localRembJournalier2, null);
				currentNuit.calculerNuits(ec, rembDebut, segment.segFin());
				currentNuit.calculerMontant();
			}

			segment.mission().addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateurModification");		
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	

}
