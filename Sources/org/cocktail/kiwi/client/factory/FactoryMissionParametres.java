package org.cocktail.kiwi.client.factory;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.EOMissionParametres;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryMissionParametres {

	
	private static FactoryMissionParametres sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryMissionParametres sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryMissionParametres();
		return sharedInstance;
	}
	
	
	/** Initialisation d'une nouvelle ligne budgetaire */
	public static EOMissionParametres creerMissionParametre(EOEditingContext ec, EOExercice exercice, String code, String commentaire)	{
		
		EOMissionParametres param = (EOMissionParametres)Factory.instanceForEntity(ec, EOMissionParametres.ENTITY_NAME);

		param.setToExerciceRelationship(exercice);
		param.setParamKey(code);
		param.setCommentaires(commentaire);
		
		ec.insertObject(param);
		
		return param;
	}

}
