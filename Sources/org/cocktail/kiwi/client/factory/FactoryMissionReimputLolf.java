package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.kiwi.client.metier.EOMissionReimput;
import org.cocktail.kiwi.client.metier.EOMissionReimputLolf;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryMissionReimputLolf {

	private static FactoryMissionReimputLolf sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryMissionReimputLolf sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryMissionReimputLolf();
		return sharedInstance;
	}
	
	/**
	 * 
	 * @param ec
	 * @param missionPaiement
	 * @param preferences
	 * @return
	 */
		public EOMissionReimputLolf creer (
				EOEditingContext ec,
				EOMissionReimput missionReimput, 
				EOLolfNomenclatureDepense action,
				BigDecimal pourcentage
		)	{
			
			EOMissionReimputLolf record = (EOMissionReimputLolf)Factory.instanceForEntity(ec, EOMissionReimputLolf.ENTITY_NAME);
				
			record.setMissionReimputRelationship(missionReimput);

			record.setLolfRelationship(action);

			record.setMrlPourcentage(pourcentage);
			record.setMrlMontant(new BigDecimal(0));
			
			ec.insertObject(record);
			
			return record;
		}

	
	
}
