package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOPlageHoraire;
import org.cocktail.kiwi.client.metier.EORembJournalier;
import org.cocktail.kiwi.client.metier.EORembZone;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryRembJournalier {

	private static FactoryRembJournalier sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryRembJournalier sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryRembJournalier();
		return sharedInstance;
	}
	
	/**
	 * 
	 * @param ec
	 * @param zone
	 * @param plageHoraire
	 * @return
	 */
	public EORembJournalier creer(EOEditingContext ec, EORembZone zone, EOPlageHoraire plageHoraire) {
				
		EORembJournalier record = (EORembJournalier)Factory.instanceForEntity(ec, EORembJournalier.ENTITY_NAME);

		record.setToZoneRelationship(zone);
		record.setToPlageHoraireRelationship(plageHoraire);
		
		ec.insertObject(record);
		return record;
		
	}	
	
}
