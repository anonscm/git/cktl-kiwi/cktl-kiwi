package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDestin;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionPreferencesPerso;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOParamCodeExer;
import org.cocktail.kiwi.client.metier.EOParamImputation;
import org.cocktail.kiwi.client.metier.EOParamModePaiement;
import org.cocktail.kiwi.client.metier.EOParamTypeCredit;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryMissionPaiementEngage {

	private static FactoryMissionPaiementEngage sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryMissionPaiementEngage sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryMissionPaiementEngage();
		return sharedInstance;
	}
	
/**
 * 
 * @param ec
 * @param missionPaiement
 * @param preferences
 * @return
 */
	public EOMissionPaiementEngage creerMissionPaiementEngage (
			EOEditingContext ec,
			EOMissionPaiement missionPaiement,
			EOExercice exercice,
			EOMissionPreferencesPerso preferences
	)	{
		
		EOMissionPaiementEngage missionPaiementEngage = (EOMissionPaiementEngage)Factory.instanceForEntity(ec, EOMissionPaiementEngage.ENTITY_NAME);
			
		missionPaiementEngage.addObjectToBothSidesOfRelationshipWithKey(missionPaiement, EOMissionPaiementEngage.MISSION_PAIEMENT_KEY);
		missionPaiementEngage.addObjectToBothSidesOfRelationshipWithKey(exercice, EOMissionPaiementEngage.TO_EXERCICE_KEY);

		// Mise a jour en fonction des preferences utilisateur
		if (preferences != null &&  ( missionPaiement.mission().isMissionPaiement() || missionPaiement.mission().isSaisieLbud() ))	{
			
			missionPaiementEngage.setToTypeCreditRelationship(preferences.toTypeCredit());
			missionPaiementEngage.setModePaiementRelationship(preferences.modePaiement());
			missionPaiementEngage.setPlanComptableRelationship(preferences.planComptable());
			missionPaiementEngage.setCodeAnalytiqueRelationship(preferences.codeAnalytique());

			// Destinations
			if (preferences.typeAction() != null){
				EOMissionPaiementDestin currentPaiementDestin = (EOMissionPaiementDestin)Factory.instanceForEntity(ec, "MissionPaiementDestin");
				ec.insertObject(currentPaiementDestin);
				
				currentPaiementDestin.addObjectToBothSidesOfRelationshipWithKey(preferences.typeAction(), "typeAction");
				currentPaiementDestin.addObjectToBothSidesOfRelationshipWithKey(missionPaiementEngage, "missionPaiementEngage");
				currentPaiementDestin.setMpdPourcentage(new BigDecimal(100));
				currentPaiementDestin.setMpdMontant(new BigDecimal(0));
			}
		}

		// Verification des parametrages budgetaires
		NSArray<EOParamTypeCredit> paramsTypeCredit = EOParamTypeCredit.findParamsTypeCredit(ec, exercice);
		if (paramsTypeCredit.size() == 1)
			missionPaiementEngage.setToTypeCreditRelationship(paramsTypeCredit.objectAtIndex(0).toTypeCredit());
			
		NSArray<EOParamModePaiement> paramsModePaiement = EOParamModePaiement.findForExercice(ec, exercice);
		if (paramsModePaiement.size() == 1)
			missionPaiementEngage.setModePaiementRelationship(paramsModePaiement.objectAtIndex(0).modePaiement());

		NSArray<EOParamCodeExer> paramsCodeExer = EOParamCodeExer.findForExercice(ec, exercice);
		if (paramsCodeExer.size() == 1)
			missionPaiementEngage.setToCodeExerRelationship(paramsCodeExer.objectAtIndex(0).toCodeExer());

		NSArray<EOParamImputation> paramsPlanComptable = EOParamImputation.findParamImputations(ec, exercice);
		if (paramsPlanComptable.size() == 1)
			missionPaiementEngage.setPlanComptableRelationship(paramsPlanComptable.objectAtIndex(0).planComptable());

		missionPaiementEngage.setMpeMontantEngagement(new BigDecimal(0));
		missionPaiementEngage.setMpeMontantRembourse(new BigDecimal(0));
		missionPaiementEngage.setMpeMontantBudgetaire(new BigDecimal(0));
		missionPaiementEngage.setMpePourcentage(new BigDecimal(100));

		missionPaiementEngage.setMpeEtat("VALIDE");
		
		ec.insertObject(missionPaiementEngage);
		
		return missionPaiementEngage;
	}

/**
 * 
 * @param exercice
 * @param lbud
 * @param typeCredit
 * @param organ
 * @param planComptable
 * @param modePaiement
 * @param convention
 * @param canal
 * @param codeExer
 */
	public void updateMissionPaiementEngage(EOExercice exercice, EOMissionPaiementEngage lbud, 
			EOTypeCredit typeCredit, EOOrgan organ, EOPlanComptable planComptable, EOModePaiement modePaiement,
			EOConvention convention, EOCodeAnalytique canal, EOCodeExer codeExer)	{

		lbud.addObjectToBothSidesOfRelationshipWithKey(exercice, EOMissionPaiementEngage.TO_EXERCICE_KEY);
		
		lbud.setOrganRelationship(organ);
		lbud.setModePaiementRelationship(modePaiement);
		lbud.setPlanComptableRelationship(planComptable);
		lbud.setToTypeCreditRelationship(typeCredit);
		lbud.setConventionRelationship(convention);
		lbud.setToCodeExerRelationship(codeExer);

		if (canal != null)
			lbud.addObjectToBothSidesOfRelationshipWithKey(canal,EOMissionPaiementEngage.CODE_ANALYTIQUE_KEY);
		else
			lbud.removeObjectFromBothSidesOfRelationshipWithKey(lbud.codeAnalytique(),EOMissionPaiementEngage.CODE_ANALYTIQUE_KEY);
				
	}


}
