package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOTitreMission;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryTitreMission {

	private static FactoryTitreMission sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryTitreMission sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryTitreMission();
		return sharedInstance;
	}
	


	public EOTitreMission creer(EOEditingContext ec) {
		
		// On regarde s'il n'y a pas plusieurs remboursements journaliers.
		
		EOTitreMission titre = (EOTitreMission)Factory.instanceForEntity(ec, EOTitreMission.ENTITY_NAME);
		
		titre.setTitEtat("VALIDE");
						
		ec.insertObject(titre);
		return titre;
		
	}	
	
}
