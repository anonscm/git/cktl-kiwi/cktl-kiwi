package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOPayeur;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryPayeur {

	private static FactoryPayeur sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryPayeur sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryPayeur();
		return sharedInstance;
	}
	


	public EOPayeur creer(EOEditingContext ec) {
		
		// On regarde s'il n'y a pas plusieurs remboursements journaliers.
		
		EOPayeur record = (EOPayeur)Factory.instanceForEntity(ec, EOPayeur.ENTITY_NAME);
		record.setTemValide("O");
		ec.insertObject(record);
		return record;
		
	}	
	
}
