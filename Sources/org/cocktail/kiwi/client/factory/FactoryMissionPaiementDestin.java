package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.kiwi.client.metier.EOMissionPaiementDestin;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryMissionPaiementDestin {

	private static FactoryMissionPaiementDestin sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryMissionPaiementDestin sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryMissionPaiementDestin();
		return sharedInstance;
	}
	
	/**
	 * 
	 * @param ec
	 * @param missionPaiement
	 * @param preferences
	 * @return
	 */
		public EOMissionPaiementDestin creerMissionPaiementDestin (
				EOEditingContext ec,
				EOMissionPaiementEngage missionPaiementEngage, 
				EOLolfNomenclatureDepense action,
				BigDecimal pourcentage
		)	{
			
			EOMissionPaiementDestin missionPaiementDestin = (EOMissionPaiementDestin)Factory.instanceForEntity(ec, EOMissionPaiementDestin.ENTITY_NAME);
				
			missionPaiementDestin.setMissionPaiementEngageRelationship(missionPaiementEngage);

			missionPaiementDestin.addObjectToBothSidesOfRelationshipWithKey(action, EOMissionPaiementDestin.TYPE_ACTION_KEY);

			missionPaiementDestin.setMpdPourcentage(pourcentage);
			missionPaiementDestin.setMpdMontant(new BigDecimal(0));
			
			ec.insertObject(missionPaiementDestin);
			
			return missionPaiementDestin;
		}

	
	
}
