package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.kiwi.client.finders.FinderSegmentInfos;
import org.cocktail.kiwi.client.finders.FinderWebTaux;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOSegmentInfos;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactorySegmentInfos {

	private static FactorySegmentInfos sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactorySegmentInfos sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactorySegmentInfos();
		return sharedInstance;
	}
/**
 * 
 * Creation d'un nouvel objet EOSegmentInfos
 * 
 * @param ec
 * @param mission
 * @return
 */
	public EOSegmentInfos creerSegmentInfos(EOEditingContext ec, EOMission mission)	{
		
		EOSegmentInfos segment = (EOSegmentInfos)Factory.instanceForEntity(ec, EOSegmentInfos.ENTITY_NAME);
		
		segment.addObjectToBothSidesOfRelationshipWithKey(mission, "mission");
		
		segment.addObjectToBothSidesOfRelationshipWithKey(FinderWebTaux.findDefaultWebTaux(ec),"webtaux");

		segment.setSeiMontantSaisie(new BigDecimal(0));
		segment.setSeiMontantPaiement(new BigDecimal(0));
		segment.setSeiEtat("VALIDE");

		ec.insertObject(segment);

		return segment;		
	}
	
    /**
     * 
     * @param ec
     * @param mission
     * @param missionReference
     */
    public void dupliquerSegmentInfos(EOEditingContext ec, EOMission mission, EOMission missionReference)	{
    	
    	NSArray remboursements = FinderSegmentInfos.findSegmentsInfosForMission(ec, missionReference);

    	for (int i=0;i<remboursements.count();i++)	 {

    		EOSegmentInfos remboursement = (EOSegmentInfos)remboursements.objectAtIndex(i);
    		
    		EOSegmentInfos newRemboursement = creerSegmentInfos(ec, mission);

    		newRemboursement.addObjectToBothSidesOfRelationshipWithKey(remboursement.webtaux(), "webtaux");
    		newRemboursement.addObjectToBothSidesOfRelationshipWithKey(remboursement.segTypeInfo(), "segTypeInfo");    		
    		newRemboursement.setSeiDivers(remboursement.seiDivers());
    		newRemboursement.setSeiJefyco(remboursement.seiJefyco());
    		newRemboursement.setSeiMontantSaisie(remboursement.seiMontantSaisie());
    		newRemboursement.setSeiMontantPaiement(remboursement.seiMontantPaiement());
    		newRemboursement.setSeiLibelle(remboursement.seiLibelle());    		
    	}	
    }

}
