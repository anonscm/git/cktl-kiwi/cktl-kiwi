package org.cocktail.kiwi.client.factory;

import java.math.BigDecimal;

import org.cocktail.kiwi.client.metier.EOWebmiss;
import org.cocktail.kiwi.client.metier.EOWebmon;
import org.cocktail.kiwi.client.metier.EOWebpays;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryWebMiss {

	private static FactoryWebMiss sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryWebMiss sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryWebMiss();
		return sharedInstance;
	}
	
    /**Initialise un object de la classe Indemnite
     * @param segmentTarif son segment
     * @param webmiss son indemnite de base 
     * @param calculNb doit on faire le calcul du nombre d indemnite
     * @param calculMontant doit on calculer le montant de l indemenite
     * @param etranger est un segment a l etranger
     */
     public EOWebmiss creerWebMiss(EOEditingContext ec, EOWebpays pays, EOWebmon monnaie) {

    	 EOWebmiss webMiss = (EOWebmiss)Factory.instanceForEntity(ec, EOWebmiss.ENTITY_NAME);
    	 
    	 webMiss.addObjectToBothSidesOfRelationshipWithKey(pays, EOWebmiss.WEBPAYS_KEY);
    	 webMiss.addObjectToBothSidesOfRelationshipWithKey(monnaie, EOWebmiss.WEBMON_KEY);

    	 webMiss.setWmiType("X");
    	 webMiss.setWpaCode(pays.wpaCode());
    	 webMiss.setWmoCode(monnaie.wmoCode());
    	 
    	 webMiss.setWmiGroupe1(new BigDecimal(0));
    	 webMiss.setWmiGroupe2(new BigDecimal(0));
    	 webMiss.setWmiGroupe3(new BigDecimal(0));
    	 webMiss.setWmiGroupe4(new BigDecimal(0));
    	 webMiss.setWmiGroupe5(new BigDecimal(0));

    	 ec.insertObject(webMiss);
    	 return webMiss;
     }


	
	
}
