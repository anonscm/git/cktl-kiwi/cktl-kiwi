package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.finders.FinderKiwiSignataireOm;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireOm;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryKiwiSignataireOm {

	private static FactoryKiwiSignataireOm sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryKiwiSignataireOm sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryKiwiSignataireOm();
		return sharedInstance;
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param organ
	 * @return
	 */
     public EOKiwiSignataireOm creerSignataire(EOEditingContext ec, EOIndividu individu, EOOrgan organ) {

    	 // On regarde si le signataire existe deja ou non 
    	 EOKiwiSignataireOm sign = FinderKiwiSignataireOm.findSignataireForIndividuAndOrgan(ec, individu, organ);

    	 if (sign != null)
    		 return null;
    	 
    	 EOKiwiSignataireOm signataire = (EOKiwiSignataireOm)Factory.instanceForEntity(ec, EOKiwiSignataireOm.ENTITY_NAME);
    	 
    	 signataire.addObjectToBothSidesOfRelationshipWithKey(individu, EOKiwiSignataireOm.INDIVIDU_KEY);
    	 signataire.addObjectToBothSidesOfRelationshipWithKey(organ,EOKiwiSignataireOm.ORGAN_KEY);

    	 ec.insertObject(signataire);
    	 return signataire;
     }
	
	
}
