package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.metier.EOVehicule;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryVehicule {

	private static FactoryVehicule sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryVehicule sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryVehicule();
		return sharedInstance;
	}
	

	
	/**
	 * 
	 * Creation d'un nouvel object Vehicule.
	 * 
	 * @param ec
	 * @param fournisseur	Fournisseur associe
	 * @param typeTransport	Type de vehicule
	 * @return
	 */
	public EOVehicule creerVehicule(EOEditingContext ec, EOFournis fournisseur, EOTypeTransport typeTransport) {
		
		// On regarde s'il n'y a pas plusieurs remboursements journaliers.
		
		EOVehicule vehicule = (EOVehicule)Factory.instanceForEntity(ec, EOVehicule.ENTITY_NAME);

		vehicule .addObjectToBothSidesOfRelationshipWithKey(fournisseur, EOVehicule.FOURNIS_KEY);
		vehicule .addObjectToBothSidesOfRelationshipWithKey(typeTransport, EOVehicule.TYPE_TRANSPORT_KEY);
		
		vehicule.setVehDate(new NSTimestamp());
		vehicule.setVehEtat("VALIDE");
						
		ec.insertObject(vehicule);
		return vehicule;
		
	}	
	
}
