package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireEtatFrais;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryKiwiSignataireEtatFrais {

	private static FactoryKiwiSignataireEtatFrais sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryKiwiSignataireEtatFrais sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryKiwiSignataireEtatFrais();
		return sharedInstance;
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param organ
	 * @return
	 */
     public EOKiwiSignataireEtatFrais creerSignataire(EOEditingContext ec, EOIndividu individu, EOOrgan organ) {

    	 EOKiwiSignataireEtatFrais signataire = (EOKiwiSignataireEtatFrais)Factory.instanceForEntity(ec, EOKiwiSignataireEtatFrais.ENTITY_NAME);
    	 
    	 signataire.addObjectToBothSidesOfRelationshipWithKey(individu, EOKiwiSignataireEtatFrais.INDIVIDU_KEY);
    	 signataire.addObjectToBothSidesOfRelationshipWithKey(organ,EOKiwiSignataireEtatFrais.ORGAN_KEY);

    	 ec.insertObject(signataire);
    	 return signataire;
     }
	
	
}
