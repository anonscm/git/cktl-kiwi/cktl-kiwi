/*
 * Created on 6 oct. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.factory;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedClassDescription;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Factory {

	/**
	 * 
	 * Creation d'une nouvelle instance d'objet dans l'editingContext ec
	 * 
	 * @param ec
	 * @param entity
	 * @return
	 */
    public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {

        EODistributedClassDescription description = (EODistributedClassDescription) EOClassDescription.classDescriptionForEntityName(entity);

        EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);

        return object;

    }
    
    
}
