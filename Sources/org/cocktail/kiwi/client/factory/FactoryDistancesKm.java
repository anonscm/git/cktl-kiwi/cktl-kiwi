package org.cocktail.kiwi.client.factory;

import org.cocktail.kiwi.client.metier.EODistancesKm;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryDistancesKm {

	private static FactoryDistancesKm sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryDistancesKm sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryDistancesKm();
		return sharedInstance;
	}
	

	
	/**
	 * 
	 * Creation d'un nouvel object Vehicule.
	 * 
	 * @param ec
	 * @param fournisseur	Fournisseur associe
	 * @param typeTransport	Type de vehicule
	 * @return
	 */
	public EODistancesKm creerTrajet(EOEditingContext ec) {
		
		// On regarde s'il n'y a pas plusieurs remboursements journaliers.
		
		EODistancesKm trajet = (EODistancesKm)Factory.instanceForEntity(ec, EODistancesKm.ENTITY_NAME);
		
		trajet.setDModification(new NSTimestamp());
						
		ec.insertObject(trajet);
		return trajet;
		
	}	
	
}
