package org.cocktail.kiwi.client.editions;

import java.awt.Dimension;
import java.awt.Window;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swing.ZAbstractPanel;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.finders.FinderKiwiSignataireEtatFrais;
import org.cocktail.kiwi.client.finders.FinderKiwiSignataireOm;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderOrgan;
import org.cocktail.kiwi.client.finders.FinderOrganSignataire;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOMissionReimput;
import org.cocktail.kiwi.client.metier.EOReimputaiton;
import org.cocktail.kiwi.client.metier.EOTransports;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CommonImprCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class ReportsCtrl  extends CommonImprCtrl {


	// IRCANTEC
	private final static String JASPER_NAME_DEMANDE_AVANCE = "demande_avance_mission.jasper";
	private final static String PDF_NAME_DEMANDE_AVANCE = "demande_avance_mission.pdf";

	private final static String JASPER_NAME_OM = "ordre_mission.jasper";
	private final static String PDF_NAME_OM = "OrdreMission.pdf";

	private final static String JASPER_NAME_ETAT_FRAIS = "etat_frais_mission.jasper";
	private final static String PDF_NAME_ETAT_FRAIS = "EtatFrais_mission.pdf";

	private final static String JASPER_NAME_VEHICULE= "autorisation_vehicule_mission.jasper";
	private final static String PDF_NAME_VEHICULE = "AutorisationVehicule";

	private final static String JASPER_NAME_BUDGET = "kiwi_budget.jasper";
	private final static String PDF_NAME_BUDGET = "kiwi_budget.pdf";

	private final static String JASPER_NAME_BUDGET_LIQUIDEES = "kiwi_budget_liquidees.jasper";
	private final static String PDF_NAME_BUDGET_LIQUIDEES = "kiwi_budget_liquidees.pdf";

	private final static String JASPER_NAME_BUDGET_EN_COURS = "kiwi_budget_encours.jasper";
	private final static String PDF_NAME_BUDGET_EN_COURS = "kiwi_budget_encours.pdf";

	private final static String JASPER_REIMPUTATION = "reimputation.jasper";
	private final static String PDF_REIMPUTATION = "Reimputation.pdf";

	private final static String JASPER_REIMPUTATIONS = "kiwi_reimput_exer.jasper";
	private final static String PDF_REIMPUTATIONS = "Reimputations.pdf";

	private static ReportsCtrl sharedInstance;

	String nbSignatairesOm = "1";
	String nbSignatairesEtatFrais = "1";
	String ordonnateur = "UB";
	String origineSignataires = "JEFY_ADMIN";
	String typeSignatairesOm = "FINANCIER";

	private ApplicationClient NSApp ;
	private EOEditingContext edc;

	/**
	 * 
	 *
	 */
	public ReportsCtrl(EOEditingContext editingContext)	{

		super();
		edc = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

	}
	
	/**
	 * 
	 * @param edc
	 * @param exercice
	 */
	public void setParamsSignataires(EOEditingContext edc, EOExercice exercice) {
		
		origineSignataires = FinderMissionParametres.getValue(edc, exercice, EOMissionParametres.ID_ORIGINE_SIGNATAIRES);
		if (origineSignataires == null)
			origineSignataires = EOMissionParametres.DEFAULT_VALUE_ORIGINE_SIGNATAIRES;

		nbSignatairesOm = FinderMissionParametres.getValue(edc, exercice, EOMissionParametres.ID_NB_SIGNATAIRES_OM);
		if (nbSignatairesOm == null)
			nbSignatairesOm = EOMissionParametres.DEFAULT_VALUE_NB_SIGNATAIRES_OM;

		nbSignatairesEtatFrais = FinderMissionParametres.getValue(edc, exercice, EOMissionParametres.ID_NB_SIGNATAIRES_ETAT_FRAIS);
		if (nbSignatairesEtatFrais == null)
			nbSignatairesEtatFrais = EOMissionParametres.DEFAULT_VALUE_NB_SIGNATAIRES_ETAT_FRAIS;

		ordonnateur = FinderMissionParametres.getValue(edc, exercice, EOMissionParametres.ID_ORDONNATEUR);
		if (ordonnateur == null)
			ordonnateur = EOMissionParametres.DEFAULT_VALUE_ORDONNATEUR;

		typeSignatairesOm = FinderMissionParametres.getValue(edc, exercice, EOMissionParametres.ID_TYPE_SIGNATAIRES_OM);
		if (typeSignatairesOm == null)
			typeSignatairesOm = EOMissionParametres.DEFAULT_VALUE_TYPE_SIGNATAIRES_OM;
		
		
	}


	/**
	 * 
	 * @param value
	 */
	public void setOrigineSignataires(String value) {
		origineSignataires = value;
	}


	/**
	 * 
	 * @param value
	 */
	public void setNbSignatairesOm(String value) {
		nbSignatairesOm = value;
	}

	/**
	 * 
	 * @param value
	 */
	public void setOrdonnateur(String value) {
		ordonnateur = value;
	}

	public void setNbSignatairesEtatFrais(String value) {
		nbSignatairesEtatFrais = value;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ReportsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ReportsCtrl(editingContext);
		return sharedInstance;
	}


	public void onImprimer() {
		// TODO Auto-generated method stub

	}

	public String title() {
		// TODO Auto-generated method stub
		return null;
	}

	public Dimension defaultDimension() {
		// TODO Auto-generated method stub
		return null;
	}


	public ZAbstractPanel mainPanel() {
		// TODO Auto-generated method stub
		return null;
	}



	/**
	 * 
	 * @param dico
	 * @param jasperName
	 * @param pdfName
	 */
	public void print(NSDictionary dico, String jasperName, String pdfName, Window win) {

		try {

			String pdfPath = imprimerReportByThreadPdf(
					edc, 
					NSApp.temporaryDir, 
					dico, 
					jasperName, 
					pdfName, 
					null, 
					win,
					null);

			System.out.println("ReportsCtrl.print() " + pdfPath);
			NSApp.openFile(pdfPath);

		}	        	
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}		
	}


	/**
	 * 
	 * @param exercice
	 * @param win
	 */
	public void printOrdreMission(EOMission mission, EOMissionPaiement paiement, Window win)	{

		setParamsSignataires(edc, paiement.mission().toExercice());
		
		NSMutableDictionary dico = new NSMutableDictionary();		
		dico.setObjectForKey(ServerProxy.clientSideRequestPrimaryKeyForObject(edc, mission).objectForKey("misOrdre"), "MIS_ORDRE");

		String signatairesCr = "", signatairesUb = "";
		NSMutableArray listeSignatairesCr = new NSMutableArray() , listeSignatairesUb = new NSMutableArray();

		if (!nbSignatairesOm.equals("0")) {

			NSArray paiementsEngage = EOMissionPaiementEngage.findForMission(edc, paiement.mission());

			if (typeSignatairesOm.equals("FINANCIER")) {

				for (int i=0;i<paiementsEngage.count();i++) {

					EOMissionPaiementEngage paiementEngage = (EOMissionPaiementEngage)paiementsEngage.objectAtIndex(i);

					if (paiementEngage.organ() != null && paiementEngage.toTypeCredit() != null) {

						String signataire = "";
						if ("KIWI".equals(origineSignataires))
							signataire = FinderKiwiSignataireOm.getStringSignataires(edc,paiementEngage.organ(), nbSignatairesOm);
						else
							signataire = FinderOrganSignataire.getStringSignataires(edc,paiementEngage.organ(), paiementEngage.mpeMontantBudgetaire(), new NSTimestamp(), paiementEngage.toTypeCredit(), nbSignatairesOm);

						if (!listeSignatairesCr.containsObject(signataire)) 
							listeSignatairesCr.addObject(signataire);

						// SIGNATAIRES UB
						EOOrgan ub = FinderOrgan.findUbForOrgan(edc, paiementEngage.organ());;

						if (ub.orgLibelle().equals("IUT DE LA ROCHELLE")) {
							signataire = FinderKiwiSignataireOm.getStringSignataires(edc, ub, nbSignatairesOm);
						}
						else {						
							if (!ordonnateur.equals("UB"))
								ub = FinderOrgan.findEtabForOrgan(edc, paiementEngage.organ());

							if ("KIWI".equals(origineSignataires))
								signataire = FinderKiwiSignataireOm.getStringSignataires(edc, ub, nbSignatairesOm);
							else
								signataire = FinderOrganSignataire.getStringSignataires(edc, ub, paiementEngage.mpeMontantBudgetaire(), new NSTimestamp(), paiementEngage.toTypeCredit(), nbSignatairesOm);

						}

						if (!listeSignatairesUb.containsObject(signataire)) 
							listeSignatairesUb.addObject(signataire);
					}
				}

				for (int i=0;i<listeSignatairesCr.count();i++)
					signatairesCr = signatairesCr + (String)listeSignatairesCr.objectAtIndex(i) + "\n";		

				if (signatairesCr.length() > 0)
					signatairesCr = signatairesCr.substring(0, (signatairesCr.length()-1));

				for (int i=0;i<listeSignatairesUb.count();i++)
					signatairesUb = signatairesUb + (String)listeSignatairesUb.objectAtIndex(i) + "\n";

				if (signatairesUb.length() > 0)
					signatairesUb = signatairesUb.substring(0, (signatairesUb.length()-1));
			}
			else {

				NSMutableDictionary dico2 = new NSMutableDictionary();		
				dico2.setObjectForKey(ServerProxy.clientSideRequestPrimaryKeyForObject(edc, EOIndividu.findIndividuForPersIdInContext(edc, mission.fournis().persId())).objectForKey("noIndividu"), "noIndividu");
				signatairesCr = ServerProxy.clientSideRequestGetResponsableService(edc, (Number)dico2.objectForKey("noIndividu"));

				for (int i=0;i<paiementsEngage.count();i++) {

					EOMissionPaiementEngage paiementEngage = (EOMissionPaiementEngage)paiementsEngage.objectAtIndex(i);

					if (paiementEngage.organ() != null && paiementEngage.toTypeCredit() != null) {

						String signataire = "";
						EOOrgan ub = null;

						if (ordonnateur.equals("UB"))
							ub = FinderOrgan.findUbForOrgan(edc, paiementEngage.organ());
						else
							ub = FinderOrgan.findEtabForOrgan(edc, paiementEngage.organ());

						if ("KIWI".equals(origineSignataires))
							signataire = FinderKiwiSignataireOm.getStringSignataires(edc, ub, nbSignatairesOm);
						else
							signataire = FinderOrganSignataire.getStringSignataires(edc, ub, paiementEngage.mpeMontantBudgetaire(), new NSTimestamp(), paiementEngage.toTypeCredit(), nbSignatairesOm);


						if (!listeSignatairesUb.containsObject(signataire)) 
							listeSignatairesUb.addObject(signataire);
					}
				}

				for (int i=0;i<listeSignatairesUb.count();i++)
					signatairesUb = signatairesUb + (String)listeSignatairesUb.objectAtIndex(i) + "\n";

				if (signatairesUb.length() > 0)
					signatairesUb = signatairesUb.substring(0, (signatairesUb.length()-1));


			}

		}	

		dico.setObjectForKey(signatairesCr, "ORDONNATEURS_CR");
		dico.setObjectForKey(signatairesUb, "ORDONNATEURS_UB");

		//		if ( mission.isMissionPaiement() && mission.payeur().ordonnateur() != null) {
		//			
		//			dico.setObjectForKey(mission.payeur().ordonnateur(), "ORDONNATEURS_UB");			
		//			
		//		}


		print (dico, JASPER_NAME_OM, PDF_NAME_OM, win);

	}
	
	public void printReimputations(NSDictionary parametres, Window win)	{

			
		print (parametres, JASPER_REIMPUTATIONS, PDF_REIMPUTATIONS, win);

	}

	
	public void printReimputaiton(EOMissionReimput reimputation, Window win)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey((ServerProxy.clientSideRequestPrimaryKeyForObject(edc, reimputation.reimputation())).objectForKey(EOReimputaiton.REIM_ID_KEY), "REIM_ID");

		EOMissionParametres parametre = FinderMissionParametres.findParametre(edc, "UNIV");
		
		if (parametre != null)
			dico.setObjectForKey(parametre.paramValue(), "UNIV");
		else
			dico.setObjectForKey("", "UNIV");
			
		print (dico, JASPER_REIMPUTATION, PDF_REIMPUTATION, win);

	}

	/**
	 * 
	 * @param exercice
	 * @param win
	 */
	public void printAutorisationVehicule(EOMission mission, EOTransports transport, int numero, Window win)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(ServerProxy.clientSideRequestPrimaryKeyForObject(edc, mission).objectForKey("misOrdre"), "MIS_ORDRE");
		dico.setObjectForKey(ServerProxy.clientSideRequestPrimaryKeyForObject(edc, transport).objectForKey("traOrdre"), "TRA_ORDRE");

		String pdfName = PDF_NAME_VEHICULE+"_"+numero+CocktailConstantes.EXTENSION_PDF;
		print (dico, JASPER_NAME_VEHICULE, pdfName , win);

	}

	/**
	 * 
	 * @param exercice
	 * @param win
	 */
	public void printEtatFrais(EOMission mission, EOMissionPaiement paiement, Window win)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		setParamsSignataires(edc, paiement.mission().toExercice());

		dico.setObjectForKey(ServerProxy.clientSideRequestPrimaryKeyForObject(edc, mission).objectForKey("misOrdre"), "MIS_ORDRE");

		String signatairesCr = "", signatairesUb = "";
		NSMutableArray listeSignatairesCr = new NSMutableArray() , listeSignatairesUb = new NSMutableArray();

		if (!nbSignatairesEtatFrais.equals("0")) {

			NSArray paiementsEngage = EOMissionPaiementEngage.findForMission(edc, paiement.mission());

			for (int i=0;i<paiementsEngage.count();i++) {

				EOMissionPaiementEngage paiementEngage = (EOMissionPaiementEngage)paiementsEngage.objectAtIndex(i);

				if (paiementEngage.organ() != null && paiementEngage.toTypeCredit() != null) {

					String signataire = "";
					if ("KIWI".equals(origineSignataires))
						signataire = FinderKiwiSignataireEtatFrais.getStringSignataires(edc,paiementEngage.organ(), nbSignatairesEtatFrais);
					else
						signataire = FinderOrganSignataire.getStringSignataires(edc, paiementEngage.organ(), paiementEngage.mpeMontantBudgetaire(), new NSTimestamp(), paiementEngage.toTypeCredit(), nbSignatairesOm);

					if (!listeSignatairesCr.containsObject(signataire)) 
						listeSignatairesCr.addObject(signataire);

					EOOrgan ub = FinderOrgan.findUbForOrgan(edc, paiementEngage.organ());;

					if (ub.orgLibelle().equals("IUT DE LA ROCHELLE")) {
						signataire = FinderKiwiSignataireOm.getStringSignataires(edc, ub, nbSignatairesOm);
					}
					else {											
						if (!ordonnateur.equals("UB"))
							ub = FinderOrgan.findEtabForOrgan(edc, paiementEngage.organ());

						if ("KIWI".equals(origineSignataires))
							signataire = FinderKiwiSignataireEtatFrais.getStringSignataires(edc, ub, nbSignatairesEtatFrais);
						else
							signataire = FinderOrganSignataire.getStringSignataires(edc, ub, paiementEngage.mpeMontantBudgetaire(), new NSTimestamp(), paiementEngage.toTypeCredit(), nbSignatairesOm);
					}

					if (!listeSignatairesUb.containsObject(signataire)) 
						listeSignatairesUb.addObject(signataire);
				}
			}

			for (int i=0;i<listeSignatairesCr.count();i++)
				signatairesCr = signatairesCr + (String)listeSignatairesCr.objectAtIndex(i) + "\n";	
			
			if (signatairesCr.length() > 0)
				signatairesCr = signatairesCr.substring(0, (signatairesCr.length()-1));

			for (int i=0;i<listeSignatairesUb.count();i++)
				signatairesUb = signatairesUb + (String)listeSignatairesUb.objectAtIndex(i) + "\n";
			if (signatairesUb.length() > 0)
				signatairesUb = signatairesUb.substring(0, (signatairesUb.length()-1));

		}	

		dico.setObjectForKey(signatairesCr, "ORDONNATEURS_CR");
		dico.setObjectForKey(signatairesUb, "ORDONNATEURS_UB");		

		print (dico, JASPER_NAME_ETAT_FRAIS, PDF_NAME_ETAT_FRAIS, win);

	}


	public void printBudgetMissions(Number exercice, Window win)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(exercice, "EXERCICE");

		print (dico, JASPER_NAME_BUDGET, PDF_NAME_BUDGET, win);

	}

	public void printBudgetMissionsLiquidees(Number exercice, Window win)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(exercice, "EXERCICE");

		print (dico, JASPER_NAME_BUDGET_LIQUIDEES, PDF_NAME_BUDGET_LIQUIDEES, win);

	}	

	public void printBudgetMissionsEnCours(Number exercice, Window win)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(exercice, "EXERCICE");

		print (dico, JASPER_NAME_BUDGET_EN_COURS, PDF_NAME_BUDGET_EN_COURS, win);

	}	

	

	public void printDemandeAvance(EOMission mission,  Window win)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		setParamsSignataires(edc, mission.toExercice());

		dico.setObjectForKey(ServerProxy.clientSideRequestPrimaryKeyForObject(edc, mission).objectForKey("misOrdre"), "MIS_ORDRE");

		// Ajout du ou des ordonnateurs
		String signatairesUb = "";
		NSMutableArray listeSignatairesUb = new NSMutableArray();

		if (!nbSignatairesEtatFrais.equals("0")) {
			
			NSArray paiementsEngage = EOMissionPaiementEngage.findForMission(edc, mission);

			for (int i=0;i<paiementsEngage.count();i++) {

				EOMissionPaiementEngage paiementEngage = (EOMissionPaiementEngage)paiementsEngage.objectAtIndex(i);

				if (paiementEngage.organ() != null && paiementEngage.toTypeCredit() != null) {

					String signataire = "";
					EOOrgan ub = FinderOrgan.findUbForOrgan(edc, paiementEngage.organ());;

					if (ub.orgLibelle().equals("IUT DE LA ROCHELLE"))
						signataire = FinderKiwiSignataireOm.getStringSignataires(edc, ub, nbSignatairesOm);
					else {											
						if (!ordonnateur.equals("UB"))
							ub = FinderOrgan.findEtabForOrgan(edc, paiementEngage.organ());

						if ("KIWI".equals(origineSignataires))
							signataire = FinderKiwiSignataireEtatFrais.getStringSignataires(edc, ub, nbSignatairesEtatFrais);
						else
							signataire = FinderOrganSignataire.getStringSignataires(edc, ub, paiementEngage.mpeMontantBudgetaire(), new NSTimestamp(), paiementEngage.toTypeCredit(), nbSignatairesOm);
					}

					if (!listeSignatairesUb.containsObject(signataire)) 
						listeSignatairesUb.addObject(signataire);
				}
			}

			for (int i=0;i<listeSignatairesUb.count();i++)
				signatairesUb = signatairesUb + (String)listeSignatairesUb.objectAtIndex(i) + "\n";
			
			if (signatairesUb.length() > 0)
				signatairesUb = signatairesUb.substring(0, (signatairesUb.length()-1));

		}	

		dico.setObjectForKey(signatairesUb, "ORDONNATEURS_UB");		

		print (dico, JASPER_NAME_DEMANDE_AVANCE, PDF_NAME_DEMANDE_AVANCE, win);

	}	
	

}
