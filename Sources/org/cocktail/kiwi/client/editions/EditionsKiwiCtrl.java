/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kiwi.client.editions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ApplicationIcones;
import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.client.nibctrl.EditionsKiwiView;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class EditionsKiwiCtrl {

	private static EditionsKiwiCtrl sharedInstance;
	
	private static Boolean MODE_MODAL = Boolean.FALSE;
	
	private ApplicationClient NSApp;
	private EOEditingContext ec;
	
	private EditionsKiwiView myView;

	private OngletChangeListener listenerOnglets = new OngletChangeListener();
	private PopupExerciceListener listenerExercice = new PopupExerciceListener();

	private EOExercice currentExercice;
	
	public EditionsKiwiCtrl(EOEditingContext editingContext) {
		
		super();
		
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		ec = editingContext;
		
		myView = new EditionsKiwiView(new JFrame(), MODE_MODAL.booleanValue());
		
		myView.setListeExercices((NSArray)(FinderExercice.findExercices(ec)).valueForKey(EOExercice.EXE_EXERCICE_KEY));
		
		currentExercice = FinderExercice.exerciceCourant(ec);
		myView.setSelectedExercice(currentExercice.exeExercice());
						
		myView.getOnglets().addTab ("Cumul Kms", ApplicationIcones.ICON_VOITURE , EditionCumulKmsCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Etat Budgétaire", CocktailIcones.ICON_EURO , EditionEtatBudgetaireCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Réimputations", CocktailIcones.ICON_EURO , EditionReimputationsCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Reversements", null , EditionReversementsCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Avances", null , EditionAvancesCtrl.sharedInstance(ec).getView());
		
        myView.getOnglets().addChangeListener(listenerOnglets);

        setParametres();
        
        myView.getExercices().addActionListener(listenerExercice);
        
        listenerOnglets.stateChanged(null);
        
	}
	

	
	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

	    	CRICursor.setWaitCursor(myView);

	    	setParametres();
	    	
	    	switch (myView.getOnglets().getSelectedIndex()) {
	    	
	    	case 0 : EditionCumulKmsCtrl.sharedInstance(ec).actualiser();break;	    	        	
        	case 1 : EditionEtatBudgetaireCtrl.sharedInstance(ec).actualiser();break;
	    	case 2 : EditionReimputationsCtrl.sharedInstance(ec).actualiser();break;	    	        	
	    	case 3 : EditionReversementsCtrl.sharedInstance(ec).actualiser();break;	    	        	
	    	case 4 : EditionAvancesCtrl.sharedInstance(ec).actualiser();break;	    	        	

	    	}
	    	
	    	CRICursor.setDefaultCursor(myView);
			
		}
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionsKiwiCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EditionsKiwiCtrl(editingContext);
		return sharedInstance;
	}

	
	/**
	 *
	 */
	public void open() {
		
		myView.setVisible(true);

	}

	/**
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupExerciceListener implements ActionListener	{
        public PopupExerciceListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	currentExercice = FinderExercice.findExercice(ec, (Number)myView.getExercices().getSelectedItem());
        	setParametres();
        	listenerOnglets.stateChanged(null);

        }
    }  
    
	/**
	 * 
	 */
	public void setParametres() {
			EditionReimputationsCtrl.sharedInstance(ec).setParametres(currentExercice);
			EditionCumulKmsCtrl.sharedInstance(ec).setParametres(currentExercice);
			EditionEtatBudgetaireCtrl.sharedInstance(ec).setParametres(currentExercice);
			EditionReversementsCtrl.sharedInstance(ec).setParametres(currentExercice);
			EditionAvancesCtrl.sharedInstance(ec).setParametres(currentExercice);
	}
	
}
