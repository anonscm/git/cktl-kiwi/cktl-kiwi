package org.cocktail.kiwi.client.editions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.client.finders.FinderKiwiExportDocuments;
import org.cocktail.kiwi.client.metier.EOKiwiExportDocuments;
import org.cocktail.kiwi.client.nibctrl.EditionsView;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class ExportsExcel {

	private static ExportsExcel sharedInstance;
	
	private EOEditingContext ec;
	
	private EditionsView myView;

	private PopupPeriodeListener listenerPeriode = new PopupPeriodeListener();

	private EODisplayGroup eod = new EODisplayGroup();
	
	private EOExercice currentExercice;
	private EOKiwiExportDocuments currentDocument;
	private Integer currentMois;

	public ExportsExcel(EOEditingContext editingContext) {
		
		super();
				
		ec = editingContext;
		
		myView = new EditionsView(new JFrame(), false, eod);
		
		myView.setListeExercices((NSArray)(FinderExercice.findExercices(ec)).valueForKey(EOExercice.EXE_EXERCICE_KEY));
		
		myView.setListeMois(CocktailConstantes.LISTE_MOIS);
		currentMois = new Integer(myView.getListeMois().getSelectedIndex() + 1);
		
		currentExercice = FinderExercice.exerciceCourant(ec);
		myView.setSelectedExercice(currentExercice.exeExercice());
				        
        myView.getListeExercices().addActionListener(listenerPeriode);
        myView.getListeMois().addActionListener(listenerPeriode);

		myView.getButtonExportXls().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exportXls();
			}
		});
		
		myView.getButtonSaveSql().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveSql();
			}
		});

		myView.getMyEOTable().addListener(new ListenerDocuments());


	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ExportsExcel sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ExportsExcel(editingContext);
		return sharedInstance;
	}
	
	
	private void actualiserDocuments() {
		
		eod.setObjectArray(FinderKiwiExportDocuments.findDocuments(ec));
		myView.getMyEOTable().updateData();
		
	}
	
	/**
	 *
	 */
	public void open() {
		
		actualiserDocuments();
		
		myView.setVisible(true);
	}
	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupPeriodeListener implements ActionListener	{
        public PopupPeriodeListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	CRICursor.setWaitCursor(myView);
        	      	
       		currentExercice = FinderExercice.findExercice(ec, (Number)myView.getListeExercices().getSelectedItem());
        	
        	currentMois= new Integer(myView.getListeMois().getSelectedIndex() + 1);
        	        	
        	CRICursor.setDefaultCursor(myView);

        }
    }


    private String replaceParametres(String sql) {
    	
    	String retour = sql;
    	
    	// Exercice
    	
    	retour = StringCtrl.replace(retour, "$P{EXER}", currentExercice.exeExercice().toString());

    	// Mois

    	retour = StringCtrl.replace(retour, "$P{MOIS}", currentMois.toString());
    	
    	String moisCode = currentExercice.exeExercice().toString();
    	if (currentMois.intValue() < 10)
    		moisCode = moisCode + "0" + currentMois.toString();
    	else
    		moisCode = moisCode + currentMois.toString();

    	retour = StringCtrl.replace(retour, "$P{MOISCODE}", moisCode);

    	// UB
    	
    	return retour;
    }

    /**
     * 
     */
    private void exportXls() {

    	String sql = replaceParametres(currentDocument.kedSql());
    	String template = currentDocument.kedTemplate() + ".xls";
    	String resultat = template+returnTempStringName()+".xls";
    	
    	try {
    		//NSApp.getToolsCocktailExcel().exportWithJxls(template,replaceParametres(sql),resultat);
    	} catch (Throwable e) {
    		System.out.println ("XLS !!!"+e);
    	}

    }
    
    
    private void saveSql() {
    	
    	try {
    	
    		currentDocument.setKedSql(myView.getConsoleSql().getText());
    		
    		ec.saveChanges();
    		
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    		ec.revert();
    	}
    	
    }
    
	private String 	returnTempStringName(){
		java.util.Calendar currentTime = java.util.Calendar.getInstance();

		String lastModif = "-"+currentTime.get(java.util.Calendar.DAY_OF_MONTH)+"."+currentTime.get(java.util.Calendar.MONTH)+"."+currentTime.get(java.util.Calendar.YEAR)+"-"
		+currentTime.get(java.util.Calendar.HOUR_OF_DAY)+"h"+currentTime.get(java.util.Calendar.MINUTE)+"m"+currentTime.get(java.util.Calendar.SECOND);

		return lastModif;
	}
	
	private class ListenerDocuments implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			myView.getConsoleSql().setText("");
			currentDocument = (EOKiwiExportDocuments)eod.selectedObject();

			if (currentDocument != null) {
				
				myView.getConsoleSql().setText(currentDocument.kedSql());
				
			}
			
		}
	}

	public void exporterBordereauLiquidatif(String templateName, String sql) {
		
    	String template = templateName;
    	String resultat = template+returnTempStringName()+".xls";
    	
    	try {
    		//NSApp.getToolsCocktailExcel().exportWithJxls(template,replaceParametres(sql),resultat);
    	} catch (Throwable e) {
    		System.out.println ("XLS !!!"+e);
    	}

		
	}
	
	public void exporterLiquidations(String templateName, String sql) {
		
    	String template = templateName;
    	String resultat = template+returnTempStringName()+".xls";
    	
    	try {
    		//NSApp.getToolsCocktailExcel().exportWithJxls(template,replaceParametres(sql),resultat);
    	} catch (Throwable e) {
    		System.out.println ("XLS !!!"+e);
    	}

		
	}
}
