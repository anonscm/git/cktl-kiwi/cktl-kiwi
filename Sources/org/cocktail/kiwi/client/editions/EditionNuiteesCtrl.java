/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kiwi.client.editions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.nibctrl.EditionEtatBudgetaireView;
import org.cocktail.kiwi.common.utilities.CRICursor;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EditionNuiteesCtrl {

	private static EditionNuiteesCtrl sharedInstance;
		
	private EOEditingContext ec;

	private EODisplayGroup eod;
	
	private EditionEtatBudgetaireView myView;

	private EOExercice currentExercice;
	
	public EditionNuiteesCtrl(EOEditingContext editingContext) {
		
		super();
				
		ec = editingContext;

		eod = new EODisplayGroup();
		
		myView = new EditionEtatBudgetaireView(eod);

		myView.getBtnImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getBtnExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});
		myView.getBtnExporter().setEnabled(false);
		
        myView.getEtatsMission().addActionListener(new PopupEtatistener());


	}
	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionNuiteesCtrl sharedInstance(EOEditingContext editingContext)	{
				
		if (sharedInstance == null)	
			sharedInstance = new EditionNuiteesCtrl(editingContext);
		return sharedInstance;
	}
	

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}
			
	
    private class PopupEtatistener implements ActionListener	{
        public PopupEtatistener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	CRICursor.setWaitCursor(myView);
        	      	        	        	        	        	
       		actualiser();
       		
        	CRICursor.setDefaultCursor(myView);

        }
    }


	/**
	 * @return
	 */
	private String getQualifierRecherche() {
		
		String sqlQualifier = 
			" SELECT o.org_ub UB, o.org_cr CR, o.org_souscr SOUS_CR, sum(mpe.mpe_montant_budgetaire) MONTANT, sum(mpe.mpe_montant_rembourse) MONTANT_REMB "+
			" from jefy_mission.mission m, jefy_admin.organ o, jefy_mission.mission_paiement mp, jefy_mission.mission_paiement_engage mpe, "+
			" jefy_depense.engage_budget eb "+
			" where m.exe_ordre = " + currentExercice.exeExercice() + " and m.mis_ordre = mp.mis_ordre and mp.mip_ordre = mpe.mip_ordre and mpe.org_id = o.org_id "+
			" and mpe.exe_ordre = " + currentExercice.exeExercice() + " " + 
			" and mpe.eng_id = eb.eng_id ";
			
			switch (myView.getEtatsMission().getSelectedIndex()) {
			case 0 : sqlQualifier = sqlQualifier + " and m.mis_etat in ('VALIDE', 'PAYEE', 'LIQUIDEE') ";break;
			case 1 : sqlQualifier = sqlQualifier + " and m.mis_etat in ('VALIDE') ";break;
			case 2 : sqlQualifier = sqlQualifier + " and m.mis_etat in ('PAYEE', 'LIQUIDEE') ";break;

			}
			
			sqlQualifier = sqlQualifier + " group by o.org_ub,o.org_cr, o.org_souscr ";
			sqlQualifier = sqlQualifier + " order by o.org_ub,o.org_cr, o.org_souscr ";
				
		return sqlQualifier;
	}

	/**
	 * 
	 * @param sender
	 */
	public void actualiser()	{
	
		NSArray retourRequete = ServerProxy.clientSideRequestSqlQuery(ec, getQualifierRecherche());
		eod.setObjectArray(retourRequete);

		myView.getMyEOTable().updateData();
	}

	public void refresh() {
		
		
		
	}
	
	
	private EOQualifier filterQualifier() {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
				
		return new EOAndQualifier(mesQualifiers);
		
	}
	

	private void filter() {
		
		eod.setQualifier(filterQualifier());
		
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();
				
	}
	
	/**
	 * 
	 * @param exercice
	 * @param mois
	 * @param gesCode
	 */
	public void setParametres(EOExercice exercice) {
				
		currentExercice = exercice;
		
	}	
	
	/**
	 * 
	 */
	public void exporter() {
		
		//EditionsCtrl.sharedInstance(ec).exporterBordereauLiquidatif("template_liquidat_export.xls",LiquidatFinderCtrl.sharedInstance(ec).getSqlQualifier("ECRAN"));
		
	}
	

	/**
	 * 
	 */
	private void imprimer() {
		
		switch (myView.getEtatsMission().getSelectedIndex())
		{
		case 0 : ReportsCtrl.sharedInstance(ec).printBudgetMissions(currentExercice.exeExercice(), new JFrame());break;
		case 1 : ReportsCtrl.sharedInstance(ec).printBudgetMissionsEnCours(currentExercice.exeExercice(), new JFrame());break;
		case 2 : ReportsCtrl.sharedInstance(ec).printBudgetMissionsLiquidees(currentExercice.exeExercice(), new JFrame());break;
		
		}
		
		
			
	}
	
	
	
	private void updateUI() {

	}
	
	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	
}
