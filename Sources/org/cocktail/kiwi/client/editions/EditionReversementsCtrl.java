/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kiwi.client.editions;

import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.nibctrl.EditionReversementsView;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EditionReversementsCtrl {

	private static EditionReversementsCtrl sharedInstance;
		
	private EOEditingContext ec;

	private EODisplayGroup eod;
	
	private EditionReversementsView myView;

	private EOExercice currentExercice;
	
	public EditionReversementsCtrl(EOEditingContext editingContext) {
		
		super();
		
		ec = editingContext;

		eod = new EODisplayGroup();
		
		myView = new EditionReversementsView(eod, null);

		myView.getBtnExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});
		
		myView.getBtnExporter().setEnabled(false);

	}
	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionReversementsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EditionReversementsCtrl(editingContext);
		return sharedInstance;
	}
	

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}
			


	private String getQualifierRecherche() {
		
		String qualifierSelect = "";
		String qualifierFrom = "";
		String qualifierWhere = "";
		String qualifierOrder = "";
		
		// SELECT
		qualifierSelect = 
			"SELECT " +
			" eng_libelle LIBELLE_ENG, i.nom_usuel NOM_USUEL, i.prenom PRENOM, " +
			" dep_ttc_saisie TTC, org_ub UB , org_cr CR, org_souscr SOUS_CR, " +
			" m.man_numero MAN_NUMERO, dpp_date_saisie DATE_DEPENSE,  i2.nom_usuel UTL_DEPENSE ";
		
		// FROM
		qualifierFrom = 
			" FROM " +
			" jefy_depense.depense_papier dpp, jefy_depense.depense_budget dep, " +
			" jefy_depense.engage_budget e, jefy_admin.type_application t, " +
			" jefy_depense.depense_ctrl_planco dcp, maracuja.mandat m, jefy_admin.organ o, " +
			" grhum.fournis_ulr f, grhum.individu_ulr i, grhum.individu_ulr i2, " + 
			" jefy_admin.utilisateur u ";
		
		// WHERE
		qualifierWhere = 
			" WHERE " + 
			" dpp.dpp_id = dep.dpp_id " +
			" and dep.eng_id = e.eng_id and e.tyap_id = t.tyap_id " +
			" and e.org_id = o.org_id and dep.Dep_id = dcp.Dep_id and dcp.man_id = m.man_id(+) " +
			" and e.fou_ordre = f.fou_ordre and f.pers_id = i.pers_id " +
			" and tyap_libelle = 'KIWI' " + 
			" and dep.utl_ordre = u.utl_ordre and u.no_individu = i2.no_individu " +
			" and e.exe_ordre = " +  currentExercice.exeExercice() + 
			" and dep.dep_ttc_saisie < 0 ";

		// ORDER BY
		qualifierOrder = 
			" ORDER BY dep_ttc_saisie desc ";
								
		return qualifierSelect + qualifierFrom + qualifierWhere + qualifierOrder;
		
	}



	public void actualiser()	{
	
		NSArray retourRequete = ServerProxy.clientSideRequestSqlQuery(ec, getQualifierRecherche());
		
		eod.setObjectArray(retourRequete);
				
		myView.getMyEOTable().updateData();
		
		filter();
	}

	
	
	public void refresh() {
		
	}
	
	
	
	private EOQualifier filterQualifier() {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
				
		return new EOAndQualifier(mesQualifiers);
		
	}
	
	
	
	private void filter() {
		
		eod.setQualifier(filterQualifier());
		
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();
				
	}
	

	public void setParametres(EOExercice exercice) {
				
		currentExercice = exercice;
		
	}	
	


	public void exporter() {
		
		//EditionsCtrl.sharedInstance(ec).exporterBordereauLiquidatif("template_liquidat_export.xls",LiquidatFinderCtrl.sharedInstance(ec).getSqlQualifier("ECRAN"));
		
	}
	

	
}
