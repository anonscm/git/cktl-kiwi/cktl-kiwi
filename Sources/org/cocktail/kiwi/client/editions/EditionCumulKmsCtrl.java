/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kiwi.client.editions;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.nibctrl.EditionCumulKmsView;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.UtilitairesFichier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class EditionCumulKmsCtrl {

	private static EditionCumulKmsCtrl sharedInstance;
		
	private EOEditingContext ec;

	private EODisplayGroup eod;
	
	private EditionCumulKmsView myView;

	private EOExercice currentExercice;
	
	public EditionCumulKmsCtrl(EOEditingContext editingContext) {
		
		super();
		
		ec = editingContext;

		eod = new EODisplayGroup();
		
		myView = new EditionCumulKmsView(eod);

		myView.getBtnImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getBtnExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});
		
		myView.getBtnImprimer().setEnabled(false);
		myView.getBtnExporter().setEnabled(false);


	}
	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionCumulKmsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EditionCumulKmsCtrl(editingContext);
		return sharedInstance;
	}
	

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}
			

	/**
	 * @return
	 */
	private String getQualifierRecherche() {
		
		String qualifierSelect = "";
		String qualifierFrom = "";
		String qualifierWhere = "";
		String qualifierGroupBy = "";
		String qualifierOrder = "";
		String qualifierHaving = "";
		
		// SELECT
		qualifierSelect = 
			"SELECT i.pers_id PERS_ID, i.nom NOM, i.prenom PRENOM, " +
			"SUM(t.seg_km1)+SUM(t.seg_km2)+SUM(t.seg_km3) KMS\n";
		
		//FROM 
		qualifierFrom = 
			" FROM grhum.v_fournis_individu i, jefy_mission.MISSION m, grhum.fournis_ulr f, " +
			" jefy_mission.SEGMENT_TARIF st, jefy_mission.TRANSPORTS t\n";
		
		// WHERE
		qualifierWhere = 
			" WHERE i.fou_ordre = m.fou_ordre " +
			" AND m.fou_ordre = f.fou_ordre " +
			" AND m.mis_ordre = st.mis_ordre " +
			" AND st.seg_ordre = t.seg_ordre " +
			" AND mis_etat != '" + EOMission.ETAT_ANNULE + "' " + 
			" AND m.mis_debut >= TO_DATE('01/01/"+currentExercice.exeExercice()+"','dd/mm/yyyy') " +
			" AND m.mis_debut <= TO_DATE('31/12/"+currentExercice.exeExercice()+"','dd/mm/yyyy')\n";

		// GROUP BY
		qualifierGroupBy = 
			" GROUP BY i.pers_id, i.nom, i.prenom\n";

		qualifierHaving =
			" HAVING SUM(t.seg_km1)+SUM(t.seg_km2)+SUM(t.seg_km3) > 0\n";

		// ORDER BY
		qualifierOrder = 
			" ORDER BY i.nom, i.prenom";
						
		return qualifierSelect+qualifierFrom+qualifierWhere+qualifierGroupBy+qualifierHaving+qualifierOrder;

	}

	/**
	 * 
	 * @param sender
	 */
	public void actualiser()	{
	
		NSArray retourRequete = ServerProxy.clientSideRequestSqlQuery(ec, getQualifierRecherche());
		eod.setObjectArray(retourRequete);
		myView.getMyEOTable().updateData();
		
		filter();
	}

	public void refresh() {
		
	}
	
	
	private EOQualifier filterQualifier() {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
				
		return new EOAndQualifier(mesQualifiers);
		
	}
	
	private void filter() {
		
		eod.setQualifier(filterQualifier());
		
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();
				
	}
	
	/**
	 * 
	 * @param exercice
	 * @param mois
	 * @param gesCode
	 */
	public void setParametres(EOExercice exercice) {
				
		currentExercice = exercice;
		
	}	
	
	private void exporter() {
		
		try {
		JFileChooser saveDialog= new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

		String nomFichierExport = "CUMULS_KMS_" + currentExercice.exeExercice();

		saveDialog.setSelectedFile(new File(nomFichierExport + ".csv"));

		if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

			File file = saveDialog.getSelectedFile();
			CRICursor.setWaitCursor(myView);
			
			String texte = enteteExport();
			
			for (NSDictionary myRecord : (NSArray<NSDictionary>)eod.displayedObjects())
				texte += texteExportPourRecord(myRecord) + CocktailConstantes.SAUT_DE_LIGNE;
			
			UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

			CRICursor.setDefaultCursor(myView);

			CocktailUtilities.openFile(file.getPath());			
		}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
	private String ajouterChamp(String value) {

		if (value == null)
			return CocktailConstantes.SEPARATEUR_EXPORT;

		return value + CocktailConstantes.SEPARATEUR_EXPORT;
	}

	private String enteteExport() {
		
		String entete = "";
		
		entete += "NOM";
		entete += CocktailConstantes.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailConstantes.SEPARATEUR_EXPORT;
		entete += "KMS";
		entete += CocktailConstantes.SAUT_DE_LIGNE;
		
		return entete;
	}

	private String texteExportPourRecord(NSDictionary dico)    {

		String texte = "";

			// NOM
			texte += ajouterChamp((String)dico.objectForKey("NOM"));
			// PRENOM
			texte += ajouterChamp((String)dico.objectForKey("NOM"));
			// DEBUT
			texte += ajouterChamp((String)dico.objectForKey("KMS"));

		return texte;
	}
	

	/**
	 * 
	 */
	private void imprimer() {
			
	}
	
	
	private void updateUI() {

	}
	
	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	
}
