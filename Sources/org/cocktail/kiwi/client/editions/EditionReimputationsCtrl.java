/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kiwi.client.editions;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.finders.FinderMissionReimput;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionReimput;
import org.cocktail.kiwi.client.nibctrl.EditionReimputationsView;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EditionReimputationsCtrl {

	private static EditionReimputationsCtrl sharedInstance;
		
	private EOEditingContext ec;

	private EODisplayGroup eod;
	
	private EditionReimputationsView myView;

	private EOExercice currentExercice;
	
	public EditionReimputationsCtrl(EOEditingContext editingContext) {
		
		super();
				
		ec = editingContext;

		eod = new EODisplayGroup();
		
		myView = new EditionReimputationsView(eod, null);

		myView.getBtnImprimerListe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimerListe();
			}
		});

		myView.getBtnImprimerAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimerAgent();
			}
		});

		myView.getBtnExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});
		
		NSArray mySort = new NSArray(new EOSortOrdering(EOMissionReimput.MISSION_PAIEMENT_KEY+"."+EOMissionPaiement.MISSION_KEY+"."+EOMission.MIS_NUMERO_KEY, EOSortOrdering.CompareDescending));
		eod.setSortOrderings(mySort);

		myView.getBtnExporter().setEnabled(false);

	}
	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionReimputationsCtrl sharedInstance(EOEditingContext editingContext)	{
		
		if (sharedInstance == null)	
			sharedInstance = new EditionReimputationsCtrl(editingContext);
		return sharedInstance;
	}
	

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}
			

	
	public void refresh() {
		
		
		
	}
	
	
	private EOQualifier filterQualifier() {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
				
		return new EOAndQualifier(mesQualifiers);
		
	}
	
	/**
	 * 
	 */
	public void actualiser() {
				
		eod.setObjectArray(FinderMissionReimput.findReimputations(ec, currentExercice));

		filter();
				
		updateUI();
	}

	private void filter() {
		
		eod.setQualifier(filterQualifier());
		
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();
				
	}
	
	/**
	 * 
	 * @param exercice
	 * @param mois
	 * @param gesCode
	 */
	public void setParametres(EOExercice exercice) {
				
		currentExercice = exercice;
		
	}	
	
	/**
	 * 
	 */
	public void exporter() {
		
		//EditionsCtrl.sharedInstance(ec).exporterBordereauLiquidatif("template_liquidat_export.xls",LiquidatFinderCtrl.sharedInstance(ec).getSqlQualifier("ECRAN"));
		
	}
	


	private void imprimerListe() {
		
		NSMutableDictionary parametres = new NSMutableDictionary();
		
		parametres.setObjectForKey(currentExercice.exeExercice(), "EXERCICE");
		
		ReportsCtrl.sharedInstance(ec).printReimputations(parametres, new JFrame());
			
	}
	
	
	private void imprimerAgent() {
			
		ReportsCtrl.sharedInstance(ec).printReimputaiton((EOMissionReimput)eod.selectedObject(), Superviseur.sharedInstance(ec).mainFrame());			
			
	}

	
	private void updateUI() {

	}
	
	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	
}
