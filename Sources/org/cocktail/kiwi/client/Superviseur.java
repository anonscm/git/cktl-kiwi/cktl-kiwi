package org.cocktail.kiwi.client;

import java.awt.Color;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.application.client.tools.CocktailIcones;
import org.cocktail.kiwi.client.menus.MainMenu;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.nibctrl.SuperviseurView;
import org.cocktail.kiwi.client.paiement.PaiementCtrl;
import org.cocktail.kiwi.client.remboursements.AvancesCtrl;
import org.cocktail.kiwi.client.remboursements.RemboursementsCtrl;
import org.cocktail.kiwi.client.trajets.TrajetsCtrl;
import org.cocktail.kiwi.common.utilities.BusyGlassPane;

import com.webobjects.eocontrol.EOEditingContext;
;

public class Superviseur extends SuperviseurView {

	private static Superviseur sharedInstance;

	private static final long serialVersionUID = 4504464890259714143L;

	private ApplicationClient 	NSApp = (ApplicationClient) ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private EOMission 		currentMission;
	private	BusyGlassPane	myBusyGlassPane;

	/**
	 * Constructeur
	 *
	 */
	public Superviseur( EOEditingContext editingContext) {

		super();
		ec = editingContext;

		myBusyGlassPane = new BusyGlassPane();
		setGlassPane(myBusyGlassPane);

		swapViewEntete.add(EnteteMission.sharedInstance(ec).view());

		onglets.addTab("   	    TRAJETS    	    ", null, TrajetsCtrl.sharedInstance(ec).view());
		onglets.addTab("        BILLETS         ", null, RemboursementsCtrl.sharedInstance(ec).view());
		onglets.addTab("        AVANCES         ", null, AvancesCtrl.sharedInstance(ec).view());
		onglets.addTab("ETAT DE FRAIS / PAIEMENT", CocktailIcones.ICON_EURO, PaiementCtrl.sharedInstance(ec).view());

		onglets.setBackgroundAt(0,new Color(166,173,201));
		onglets.addChangeListener(new OngletChangeListener());

		onglets.setEnabledAt(0, false);
		onglets.setEnabledAt(1, false);
		onglets.setEnabledAt(2, false);
		onglets.setEnabledAt(3, false);

        setIconImage(CocktailIcones.ICON_APP_LOGO.getImage()); 

        getRootPane().setJMenuBar(MainMenu.sharedInstance(ec));

	}

	/**
	 * Initialisation du Superviseur.
	 *
	 */
	public void init (String title)	{

		setTitle(title);

		TrajetsCtrl.sharedInstance(ec).updateInterface();

		currentMission = null;

		ZUiUtil.centerWindow(this);
		setVisible(true);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static Superviseur sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)		
			sharedInstance = new Superviseur(editingContext);
		return sharedInstance;
	}

	public  JFrame mainFrame()	{
		return this;
	}


	/**
	 * 
	 * @param notif
	 */
	public void missionHasChanged(EOMission mission)	{

		currentMission = mission;

		onglets.setEnabledAt(0, currentMission !=  null);
		onglets.setEnabledAt(1, currentMission !=  null);
		onglets.setEnabledAt(2, currentMission !=  null && ( currentMission.isMissionPaiement() || currentMission.isSaisieLbud()) );		
		onglets.setEnabledAt(3, currentMission !=  null);

		TrajetsCtrl.sharedInstance(ec).setCurrentMission(currentMission);
		TrajetsCtrl.sharedInstance(ec).actualiser();

		RemboursementsCtrl.sharedInstance(ec).setCurrentMission(currentMission);
		RemboursementsCtrl.sharedInstance(ec).actualiser();

		AvancesCtrl.sharedInstance(ec).setCurrentMission(currentMission);
		AvancesCtrl.sharedInstance(ec).actualiser();

		ongletsHasChanged();

	}


	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	
			ongletsHasChanged();
		}
	}

	/**
	 *
	 */
	private void ongletsHasChanged()	{

		if (currentMission != null)
			NSApp.setWaitCursor();

		switch (onglets.getSelectedIndex())	{
		case 3:
			PaiementCtrl.sharedInstance(ec).setCurrentMission(currentMission);
			PaiementCtrl.sharedInstance(ec).actualiser();
			break;
		}

		if (currentMission != null)
			NSApp.setDefaultCursor();
	}


	public void showSouthPanel(boolean yn)	 {
		onglets.setVisible(yn);
	}


	public void setGlassPane(boolean bool) {
		myBusyGlassPane.setVisible(bool);
	}

	/**
	 * 
	 *
	 */
	public void setDisabled()	{	
		onglets.setEnabledAt(0,false);
		onglets.setEnabledAt(1,false);
		onglets.setEnabledAt(2,false);	
		onglets.setEnabledAt(3,false);	
	}


	public class MainWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
			NSApp.quitter();
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent event) {
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			NSApp.quitter();
		}
	}


}