package org.cocktail.kiwi.client.menus;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.cocktail.kiwi.client.ApplicationClient;

public class MenuAide extends JMenu
{
	public ApplicationClient NSApp;

	protected JMenuItem		itemVersion, itemForums, itemSiteWeb, itemLogs, itemSiteMinefe;

	/** Constructeur */
	public MenuAide (ApplicationClient app, String titre)
	{
		super (titre);
		NSApp = app;
		initObject();		
	}

	/** */
	public void initObject() {

		itemSiteMinefe = new JMenuItem(new ActionMinefe("MINEFI - Taux chancellerie"));
		itemLogs = new JMenuItem(new ActionLogs("Logs Client / Serveur"));
		
		add(itemSiteMinefe);
		add(itemLogs);
	}

	/** */
	public class ActionMinefe extends AbstractAction
	{
		public ActionMinefe (String name)
		{
			putValue(Action.NAME,name);
		}

		public void actionPerformed(ActionEvent event)
		{
			NSApp.openURL("http://www.economie.gouv.fr/dgfip/mission_taux_chancellerie/frais");
		}
	}
	
	/** */
	public class ActionLogs extends AbstractAction	{
		public ActionLogs (String name)
		{
			putValue(Action.NAME,name);
		}

		public void actionPerformed(ActionEvent event)	{
			NSApp.showLogs();
		}
	}

}
