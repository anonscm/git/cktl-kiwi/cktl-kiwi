package org.cocktail.kiwi.client.menus;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.cocktail.application.client.tools.CocktailIcones;
import org.cocktail.application.client.tools.ConnectedUsersCtrl;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.admin.AdminBilletsCtrl;
import org.cocktail.kiwi.client.admin.DistancesCtrl;
import org.cocktail.kiwi.client.admin.EngagementsCtrl;
import org.cocktail.kiwi.client.admin.GestionPreferencesPerso;
import org.cocktail.kiwi.client.admin.MotifsCtrl;
import org.cocktail.kiwi.client.admin.ParametrageBudgetaireCtrl;
import org.cocktail.kiwi.client.admin.ParametrageCtrl;
import org.cocktail.kiwi.client.admin.PayeursCtrl;
import org.cocktail.kiwi.client.admin.PlagesHorairesCtrl;
import org.cocktail.kiwi.client.admin.RelancesCtrl;
import org.cocktail.kiwi.client.admin.SignatairesCtrl;
import org.cocktail.kiwi.client.admin.SignatairesServicesCtrl;
import org.cocktail.kiwi.client.admin.TauxChancellerieCtrl;
import org.cocktail.kiwi.client.admin.TauxIndemnitesCtrl;
import org.cocktail.kiwi.client.admin.TauxNuiteesCtrl;
import org.cocktail.kiwi.client.admin.TauxRepasCtrl;
import org.cocktail.kiwi.client.admin.TauxSncfCtrl;
import org.cocktail.kiwi.client.admin.TitreMissionCtrl;
import org.cocktail.kiwi.client.admin.UtilisateursCtrl;
import org.cocktail.kiwi.client.admin.VehiculesCtrl;
import org.cocktail.kiwi.client.admin.ZonesCtrl;
import org.cocktail.kiwi.client.editions.EditionsKiwiCtrl;
import org.cocktail.kiwi.client.editions.ExportsExcel;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.mission.DuplicationCtrl;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.mission.MissionDetailCtrl;
import org.cocktail.kiwi.client.mission.NumerotationCtrl;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class MainMenu extends JMenuBar {

	public static MainMenu sharedInstance;

	private ApplicationClient	NSApp;
	
	private EOEditingContext ec;
	
	JMenu		menuApplication, menuEditions, menuAdministration, menuOutils, menuMission, menuAide;
	
	JMenu		menuTaux = new JMenu("Taux");
	JMenu		menuParametrage = new JMenu("Paramétrage");
	JMenu		sousMenuMission = new JMenu("Mission");

	JMenuItem	miParamRepas, miParamNuitees, miParamIndemnites, miEngagements, miDupliquer, miZones;
	JMenuItem	miRafraichir, miNumerotation, miValidation, miPref, miDetail, miQuitter;
	JMenuItem   miSignataires, miSignatairesServices, miBillets, miRelances;
	JMenuItem	miMotifsCtrl, miUtilisateurs, miUtilisateursConnectes, miParametrage;
	JMenuItem	miParamBudgetaire, miDupliquerGroupe, miCumulKm;
	JMenuItem	miCheckMissionsPayees, miTauxChancellerie, miTauxSncf, miApropos, miLog, miEditions;
	JMenuItem	miTitres, miTrajets, miPlagesHoraires, miPayeurs, miVehicules, miExercice, miExportsExcel;
	
	public MainMenu(EOEditingContext globalEc) {
	
		NSApp = (ApplicationClient)EOApplication.sharedApplication();
		
		ec = globalEc;
		
		menuApplication = new JMenu("Application");
		menuAdministration = new JMenu("Administration");
		menuMission = new JMenu("Mission");
		menuOutils = new JMenu("Outils");
		menuEditions = new JMenu("Editions");
		menuAide = new JMenu("Aide");

		miUtilisateurs = new JMenuItem(new MenuAction("Utilisateurs"));
		miUtilisateursConnectes = new JMenuItem(new MenuAction("Utilisateurs Connectés"));
		miSignataires = new JMenuItem(new MenuAction("Signataires"));
		miSignatairesServices = new JMenuItem(new MenuAction("Signataires Services"));

		miEditions = new JMenuItem(new MenuAction("Editions / Statistiques"));
		miExportsExcel = new JMenuItem(new MenuAction("Exports Excels"));
		
		miVehicules = new JMenuItem(new MenuAction("Vehicules"));

		miDupliquerGroupe = new JMenuItem(new MenuAction("Dupliquer pour un groupe"));
		//miDupliquer = new JMenuItem(new MenuAction("Dupliquer toute la mission"));
		miRafraichir = new JMenuItem(new MenuAction("Rafraichir la mission"));

		miNumerotation = new JMenuItem(new MenuAction("Renuméroter la mission"));
		miValidation = new JMenuItem(new MenuAction("Revalider la mission"));
		miDetail = new JMenuItem(new MenuAction("Détail de la mission"));
		miExercice = new JMenuItem(new MenuAction("Modifier l'exercice de la mission"));
		miCheckMissionsPayees = new JMenuItem(new MenuAction("Mise à jour des missions mandatées (Etat 'PAYEE')"));

		miParamRepas = new JMenuItem(new MenuAction("Taux Repas"));
		miParamNuitees = new JMenuItem(new MenuAction("Taux Nuitées"));
		miParamIndemnites = new JMenuItem(new MenuAction("Taux Indemnités"));
		miPlagesHoraires = new JMenuItem(new MenuAction("Plages Horaires"));
		miPayeurs = new JMenuItem(new MenuAction("Payeurs"));
		miBillets = new JMenuItem(new MenuAction("Remboursements"));
		miTitres = new JMenuItem(new MenuAction("Titres de mission"));
		miZones = new JMenuItem(new MenuAction("Zones"));
		miMotifsCtrl = new JMenuItem(new MenuAction("Motifs"));

		miTauxChancellerie = new JMenuItem(new MenuAction("Taux Chancellerie"));
		miTauxSncf = new JMenuItem(new MenuAction("Taux Sncf"));

		miQuitter = new JMenuItem(new ActionQuitter("Quitter"));
		miPref = new JMenuItem(new MenuAction("Préférences Personnelles"));

		miTrajets = new JMenuItem(new MenuAction("Trajets "));
		miCumulKm = new JMenuItem(new MenuAction("Cumul Kilométrique..."));

		miEngagements = new JMenuItem(new MenuAction("Etat des engagements"));
		miRelances = new JMenuItem(new MenuAction("Relances Automatiques"));
		
		miApropos = new JMenuItem(new MenuAction("A Propos"));
		miLog = new JMenuItem(new MenuAction("Logs client/serveur"));
		
		menuApplication.add(miUtilisateursConnectes);
		menuApplication.add(miUtilisateurs);
		menuApplication.add(miSignataires);
		menuApplication.add(miSignatairesServices);
		menuApplication.add(miPref);
		menuApplication.addSeparator();
		menuApplication.add(miQuitter);
		
		
		miParametrage = new JMenuItem(new MenuAction("Général"));
		miParamBudgetaire = new JMenuItem(new MenuAction("Budgétaire"));
		miParamBudgetaire.setIcon(CocktailIcones.ICON_OUTILS_16);
		miParametrage.setIcon(CocktailIcones.ICON_OUTILS_16);

		menuParametrage.add(miParametrage);
		menuParametrage.add(miParamBudgetaire);
		
		menuTaux.add(miTauxChancellerie);
		menuTaux.add(miParamIndemnites);
		menuTaux.add(miParamRepas);
		menuTaux.add(miParamNuitees);
		menuTaux.add(miTauxSncf);
		
		sousMenuMission.add(miTitres);
		sousMenuMission.add(miMotifsCtrl);
		sousMenuMission.add(miBillets);
		sousMenuMission.add(miPayeurs);

		menuAdministration.add(menuParametrage);
		menuAdministration.add(menuTaux);
		menuAdministration.add(sousMenuMission);

		if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
			menuAdministration.add(miPlagesHoraires);
		if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
			menuAdministration.add(miZones);
		if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
			menuAdministration.add(miVehicules);
		if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
			menuAdministration.add(miTrajets);
		
		menuEditions.add(miEditions);

		menuMission.add(miRafraichir);
		menuMission.add(miDupliquerGroupe);
		//menuMission.add(miDupliquer);
		menuMission.add(miDetail);
		
//		if (NSApp.hasFonction(EOFonction.ID_FCT_NUMEROTATION))
//			menuMission.add(miNumerotation);

		if (NSApp.hasFonction(EOFonction.ID_FCT_REVALIDATION))
			menuMission.add(miValidation);

//		if (NSApp.hasFonction(EOFonction.ID_FCT_EXERCICE))
//			menuMission.add(miExercice);
		
		if (NSApp.hasFonction(EOFonction.ID_FCT_SOLDE_ENG))
			menuOutils.add(miEngagements);

		if (NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION))
			menuOutils.add(miRelances);

		if (NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION))
			menuOutils.add(miCheckMissionsPayees);

		
		menuAide.add(miLog);
		menuAide.addSeparator();
		menuAide.add(miApropos);

//		miTauxSncf.setIcon(KiwiConstantes.ICON_SNCF);
//		miVehicules.setIcon(KiwiConstantes.ICON_VOITURE);

		menuApplication.setIcon(CocktailIcones.ICON_APP_LOGO);
		menuAdministration.setIcon(CocktailIcones.ICON_OUTILS_16);
		//menuMission.setIcon(CocktailIcones.ICON_PRINTER_16);
		menuEditions.setIcon(CocktailIcones.ICON_PRINTER_16);

		add(menuApplication);
		add(menuAdministration);
		add(menuMission);
		add(menuOutils);
		add(menuEditions);
		
		add(new MenuAide(NSApp, "?"));
	}
	
	public static MainMenu sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new MainMenu(editingContext);
		return sharedInstance;
	}

	
	/** Quitte l'application */
	private class ActionQuitter extends AbstractAction	{
		
		public ActionQuitter (String name)		{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_EXIT_16);
		}
		
		public void actionPerformed(ActionEvent event)		{
			NSApp.quit();
		}
	}
	
	
	public class MenuAction extends AbstractAction {
		
		public MenuAction (String name) {
			putValue(Action.NAME,name);
		}
		
		public void actionPerformed(ActionEvent event) {
			Object src = event.getSource();

			if(src==miQuitter) {
				NSApp.quit();
			}
			if(src==miPref) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				GestionPreferencesPerso.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			
			if(src==miParamRepas) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				TauxRepasCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			
			if(src==miParamNuitees) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				TauxNuiteesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miPlagesHoraires) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				PlagesHorairesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miVehicules) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				VehiculesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miMotifsCtrl) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				MotifsCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			if(src==miZones) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				ZonesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miCheckMissionsPayees) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				ServerProxy.clientSideRequestCheckMissionsPayees(ec);
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miTrajets) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				DistancesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miEngagements) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				EngagementsCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			
			if(src==miRelances) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				RelancesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miPayeurs) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				PayeursCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miBillets) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				AdminBilletsCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			
			if(src==miTitres) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				TitreMissionCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			

			if(src==miParamIndemnites) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				TauxIndemnitesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miTauxChancellerie) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				TauxChancellerieCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			if(src==miTauxSncf) {
				TauxSncfCtrl.sharedInstance(ec).open();
			}
			if(src==miDupliquerGroupe) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				DuplicationCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
						
			if(src==miRafraichir) {
				EnteteMission.sharedInstance(ec).rafraichirMission();
			}
			
			if(src==miNumerotation) {
				NumerotationCtrl.sharedInstance(ec).open();
			}
			if(src==miDetail) {
				MissionDetailCtrl.sharedInstance(ec).open();
			}

			if(src==miValidation) {
				EnteteMission.sharedInstance(ec).revaliderMission();
			}

			if(src==miExercice) {
				EnteteMission.sharedInstance(ec).modifierExerciceMission();
			}

			
			if(src==miParametrage) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				ParametrageCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miParamBudgetaire) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				ParametrageBudgetaireCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miEditions) {
				EditionsKiwiCtrl.sharedInstance(ec).open();
			}
			
			if(src==miExportsExcel) {
				ExportsExcel.sharedInstance(ec).open();
			}

			if(src==miUtilisateurs) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				UtilisateursCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			
			if(src==miSignataires) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				SignatairesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}
			
			if(src==miSignatairesServices) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				SignatairesServicesCtrl.sharedInstance(ec).open();
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			}

			if(src==miUtilisateursConnectes) {
				NSApp.setWaitCursor(NSApp.getMainWindow(), true);
				ConnectedUsersCtrl win = new ConnectedUsersCtrl(ec);

				try {
					win.openDialog(Superviseur.sharedInstance(ec).mainFrame(), true);
				} catch (Exception e1) {
					e1.printStackTrace();
					EODialogs.runErrorDialog("ERREUR", e1.getMessage());
				}
				NSApp.setDefaultCursor(NSApp.getMainWindow(), false);
			} 

		}
	}
	
}
