package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderTypeTransport;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.metier.EOVehicule;
import org.cocktail.kiwi.client.nibctrl.VehiculesView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class VehiculesCtrl extends VehiculesView{



	private static final long serialVersionUID = -2128488379944500761L;

	private static VehiculesCtrl sharedInstance;
	
	private ApplicationClient NSApp;
	private EOEditingContext ec;
	
	private PopupTypeTransportListener listenerTypeTransport = new PopupTypeTransportListener();

	private EOVehicule currentVehicule;
	
	public VehiculesCtrl(EOEditingContext editingContext) {
		
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;
	
		getButtonAdd().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		getButtonUpdate().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});
		
		getButtonDelete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});
		
		getButtonDetail().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				afficherDetail();
			}
		});
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOVehicule.FOURNIS_KEY+"."+EOFournis.NOM_KEY, EOSortOrdering.CompareAscending));
		eod.setSortOrderings(mySort);

		setTypes(FinderTypeTransport.findTypesTransportVehicule(ec));
		getTypesTransport().setSelectedIndex(2);

		getTypesTransport().addActionListener(listenerTypeTransport);

		getMyEOTable().addListener(new ListenerVehicule());

		getNomFinder().getDocument().addDocumentListener(new ADocumentListener());
		getPrenomFinder().getDocument().addDocumentListener(new ADocumentListener());

		getCheckValide().setSelected(true);
		
		buttonAdd.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		buttonUpdate.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		buttonDelete.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		getCheckValide().addActionListener(new CheckBoxListener());
		getCheckHistorise().addActionListener(new CheckBoxListener());
		
	}

	
	public static VehiculesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new VehiculesCtrl(editingContext);
		return sharedInstance;
	}
	
	
	
	/**
	 * 
	 */
	private void actualiser() {
		
		eod.setObjectArray(EOVehicule.find(ec));		
		filter();
				
	}
	
	/**
	 * 
	 */
	public void open() {
		
		actualiser();
		
		setVisible(true);
		
	}
	
	
	/**
	 * 
	 */
	private void ajouter() {

		try {

			EOVehicule myVehicule = SaisieVehiculeCtrl.sharedInstance(ec).ajouterVehicule(null, (EOTypeTransport)typesTransport.getSelectedItem());

			if (myVehicule != null) {

				ec.saveChanges();
				actualiser();

			}

		}
		catch (Exception e) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur de sauvegarde du véhicule \n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();
		}

		actualiser();
		
	}
	
	/**
	 * 
	 */
	private void modifier() {
				

		try {

			if (SaisieVehiculeCtrl.sharedInstance(ec).modifierVehicule( currentVehicule )) {
				ec.saveChanges();
				myEOTable.updateData();
			}
			else
				ec.revert();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur d'enregistrement du véhicule !");
			ec.revert();
		}

	}
	
	
	/**
	 * 
	 */
	private void supprimer() {
		
		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Ce véhicule va être annulé et ne sera plus proposé dans les prochaines missions.\nConfirmez vous cette suppression ?",
				"OUI", "NON"))
			return;

		
		try {
			
			currentVehicule.setVehEtat("ANNULE");
			ec.saveChanges();
			
			actualiser();
			
		}
		catch (Exception ex) {
			
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}
		
	}
	
	/**
	 * 
	 *
	 */
	private void filter()	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		if (getCheckValide().isSelected())
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.VEH_ETAT_KEY + " = %@",new NSArray(EOVehicule.ETAT_VALIDE)));
		else
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.VEH_ETAT_KEY + " != %@",new NSArray(EOVehicule.ETAT_VALIDE)));
				
		if (!StringCtrl.chaineVide(nomFinder.getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.FOURNIS_KEY+"."+EOFournis.NOM_KEY + " caseInsensitiveLike %@",new NSArray("*"+nomFinder.getText()+"*")));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.TYPE_TRANSPORT_KEY + " = %@",new NSArray(typesTransport.getSelectedItem())));
		
		eod.setQualifier(new EOAndQualifier(mesQualifiers));

		eod.updateDisplayedObjects();
		myEOTable.updateData();
		
		lblNbVehicules.setText(eod.displayedObjects().count() + " Véhicules");

	}
	
	
	private void afficherDetail() {
		
		VehiculesDetailCtrl.sharedInstance(ec).open(currentVehicule);
		
	}
	
	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupTypeTransportListener implements ActionListener	{
        public PopupTypeTransportListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	filter();
        
        }
    }
	
    
	private class CheckBoxListener extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 5061847949657557263L;

		public CheckBoxListener ()	{
			super();
		}
		
		public void actionPerformed(ActionEvent anAction)	{
					
			filter();
			
		}
	}
	

	
	
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert � filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	
	private class ListenerVehicule implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			modifier();
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			currentVehicule = (EOVehicule)eod.selectedObject();
			updateUI();
			
		}
	}
	
	
	private void updateUI() {
		
		buttonDelete.setEnabled(currentVehicule != null);
		
		buttonUpdate.setEnabled(currentVehicule != null);
		buttonDelete.setEnabled(currentVehicule != null && currentVehicule.vehEtat().equals(EOVehicule.ETAT_VALIDE));
		
	}
	
}
