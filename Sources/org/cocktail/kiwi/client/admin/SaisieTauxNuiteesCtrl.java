package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.factory.FactoryRembJournalier;
import org.cocktail.kiwi.client.finders.FinderPlageHoraire;
import org.cocktail.kiwi.client.finders.FinderRembJournalier;
import org.cocktail.kiwi.client.finders.FinderRembZone;
import org.cocktail.kiwi.client.metier.EORembJournalier;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.nibctrl.SaisieTauxNuiteesView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.ModelePageSaisie;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieTauxNuiteesCtrl extends ModelePageSaisie
{
	private static SaisieTauxNuiteesCtrl sharedInstance;
	private SaisieTauxNuiteesView myView;
	private EORembJournalier currentRemboursement;

	/** 
	 *
	 */
	public SaisieTauxNuiteesCtrl (EOEditingContext edc) {

		super(edc);

		myView = new SaisieTauxNuiteesView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtAnnuler());
		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
				
		CocktailUtilities.initPopupAvecListe(myView.getZones(), FinderRembZone.findZonesForRepasNuits(getEdc()), false);
		myView.getZones().setEnabled(false);

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieTauxNuiteesCtrl sharedInstance(EOEditingContext edc)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieTauxNuiteesCtrl(edc);
		return sharedInstance;
	}

	public EORembJournalier getCurrentRemboursement() {
		return currentRemboursement;
	}
	public void setCurrentRemboursement(EORembJournalier currentRemboursement) {
		this.currentRemboursement = currentRemboursement;
		updateDatas();
	}


	/**
	 * 
	 * @param zone
	 * @return
	 */
	public EORembJournalier ajouter(EORembZone zone)	{

		CocktailUtilities.initTextField(myView.getTfDateFin(), false, false);

		clearDatas();

		setModeCreation(true);

		myView.getZones().setSelectedItem(zone);

		setCurrentRemboursement(FactoryRembJournalier.sharedInstance().creer(getEdc(), zone, FinderPlageHoraire.findPlageHoraire(getEdc(), new Integer(1))));

		myView.setVisible(true);

		return getCurrentRemboursement();

	}



	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifier(EORembJournalier remboursement)	{

		CocktailUtilities.initTextField(myView.getTfDateFin(), false, true);

		clearDatas();
		setModeCreation(false);

		setCurrentRemboursement(remboursement);

		myView.setVisible(true);

		return (getCurrentRemboursement() != null);
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfMontant());
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());

	}

	@Override
	protected void updateDatas() {
		
		// TODO Auto-generated method stub
		myView.getZones().setSelectedItem(getCurrentRemboursement().toZone().remLibelle());

		CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentRemboursement().remDate());
		CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentRemboursement().remDateFin());
		CocktailUtilities.setNumberToField(myView.getTfMontant(), getCurrentRemboursement().remTarif());

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {

		// TODO Auto-generated method stub
		getCurrentRemboursement().setRemDate(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentRemboursement().setRemTarif(CocktailUtilities.getBigDecimalFromField(myView.getTfMontant()));

		if (isModeCreation()) {
			NSTimestamp lastDate = getCurrentRemboursement().remDate().timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0);
			if (lastDate != null) {
				EORembJournalier lastTaux = FinderRembJournalier.findLastTaux(getEdc(), new Integer(1), getCurrentRemboursement().toZone().remTaux());				
				if (lastTaux != null)
					lastTaux.setRemDateFin(lastDate);
			}
		}
		else {
			getCurrentRemboursement().setRemDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		}

		getCurrentRemboursement().validateObjectMetier();

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

}