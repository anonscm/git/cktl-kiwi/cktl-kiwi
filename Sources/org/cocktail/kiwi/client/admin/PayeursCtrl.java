package org.cocktail.kiwi.client.admin;

import javax.swing.JPanel;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOPayeur;
import org.cocktail.kiwi.client.nibctrl.PayeurView;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class PayeursCtrl  {

	private static PayeursCtrl sharedInstance;
	private EOEditingContext ec;

	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

	private PayeurView myView;

	private EODisplayGroup eod;

	protected	ListenerPayeur listenerPayeur = new ListenerPayeur();

	protected JPanel viewTable;

	private	EOPayeur currentPayeur;	

	/**
	 * 
	 *
	 */
	public PayeursCtrl(EOEditingContext editingContext)	{		super();

	ec = editingContext;

	eod = new EODisplayGroup();

	myView = new PayeurView(eod, null);

	myView.getButtonAjouter().addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
			ajouter();
		}
	});

	myView.getButtonModifier().addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
			modifier();
		}
	});

	myView.getButtonInvalider().addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
			invalider();
		}
	});
	myView.getButtonSupprimer().addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
			supprimer();
		}
	});

	myView.getButtonAjouter().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
	myView.getButtonModifier().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
	myView.getButtonInvalider().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
	myView.getButtonSupprimer().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

	myView.getMyEOTable().addListener(new ListenerPayeur());}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PayeursCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PayeursCtrl(editingContext);
		return sharedInstance;
	}

	public void open()	{

		actualiser();

		ZUiUtil.centerWindow(myView);
		myView.setVisible(true);

	}



	public void actualiser()	{

		eod.setObjectArray(EOPayeur.find(ec));	
		myView.getMyEOTable().updateData();

		updateUI();
	}



	public void fermer() {

		if (ec.updatedObjects().count() > 0)	{
			try {

				if (EODialogs.runConfirmOperationDialog("Attention",
						"Voulez vous enregistrer les modifications apportées ?",
						"OUI", "NON"))  	       			
					ec.saveChanges();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement des modifications !");
			}
		}

		myView.setVisible(false);

	}  


	private void ajouter() {

		SaisiePayeurCtrl.sharedInstance(ec).ajouter();

		actualiser();

	}


	private void modifier() {

		if (SaisiePayeurCtrl.sharedInstance(ec).modifier( currentPayeur))
			myView.getMyEOTable().updateData();

	}

	private void invalider() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Voulez-vous réellement invalider le payeur " + currentPayeur.libellePayeurLong() + " ?",
				"OUI", "NON"))
			return;

		try {

			currentPayeur.setTemValide("N");
			ec.saveChanges();

			actualiser();

			updateUI();
			
		}
		catch (Exception ex) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));
		}
	}
	
	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Voulez vous réellement supprimer le payeur " + currentPayeur.libellePayeurLong() + " ?",
				"OUI", "NON"))
			return;

		try {
			
			CRICursor.setWaitCursor(myView);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMission.PAYEUR_KEY+"."+EOPayeur.CODE_PAYEUR_KEY + " = %@", new NSArray(currentPayeur.codePayeur()));
			EOMission mission = EOMission.fetchFirstByQualifier(ec, myQualifier);
			if (mission != null)
				throw new Exception("Ce code est utilisé par une ou plusieurs missions, vous ne pouvez le supprimer !" );

			ec.deleteObject(currentPayeur);
			
			ec.saveChanges();

			actualiser();

			updateUI();
			
		}
		catch (Exception ex) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}


	private void updateUI() {

		myView.getButtonModifier().setEnabled(currentPayeur != null && currentPayeur.estValide());
		myView.getButtonInvalider().setEnabled(currentPayeur != null && currentPayeur.estValide());
		myView.getButtonSupprimer().setEnabled(currentPayeur != null);

	}
	private class ListenerPayeur implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
				modifier();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentPayeur = (EOPayeur)eod.selectedObject();
			updateUI();

		}
	}

}
