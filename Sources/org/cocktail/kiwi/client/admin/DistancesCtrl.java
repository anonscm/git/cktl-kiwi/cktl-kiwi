package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderDistanceskm;
import org.cocktail.kiwi.client.metier.EODistancesKm;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.nibctrl.DistancesView;
import org.cocktail.kiwi.client.trajets.SaisieDistanceCtrl;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class DistancesCtrl extends DistancesView{

	private static DistancesCtrl sharedInstance;
	
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private EODistancesKm currentTrajet;
	
	public DistancesCtrl(EOEditingContext editingContext) {
		
		super();

		ec = editingContext;
	
		buttonAdd.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		buttonUpdate.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});
		
		buttonDelete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});
		
		buttonAdd.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		buttonUpdate.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		buttonDelete.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		myEOTable.addListener(new ListenerTrajet());

		filtreDepart.getDocument().addDocumentListener(new ADocumentListener());
		filtreArrivee.getDocument().addDocumentListener(new ADocumentListener());

	}

	
	public static DistancesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new DistancesCtrl(editingContext);
		return sharedInstance;
	}
	
	
	
	/**
	 * 
	 */
	public void getTrajet() {

		actualiser();
		
		show();
		
	}
	
	/**
	 * 
	 */
	private void actualiser() {
		
		eod.setObjectArray(FinderDistanceskm.findTrajets(ec));

		filter();
				
	}
	
	/**
	 * 
	 */
	public void open() {
		
		actualiser();
		
		show();
		
	}
	
	
	/**
	 * 
	 */
	private void ajouter() {

		try {

			SaisieDistanceCtrl.sharedInstance(ec).ajouter();

			actualiser();
			
		}
		catch (Exception e) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur de sauvegarde du trajet \n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();
		}

		actualiser();
		
	}
	
	/**
	 * 
	 */
	private void modifier() {
				

		try {

			if (SaisieDistanceCtrl.sharedInstance(ec).modifier( currentTrajet )) {

				ec.saveChanges();
				myEOTable.updateData();

			}
			else
				ec.revert();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur d'enregistrement du trajet !");
			ec.revert();
		}

	}
	
	
	/**
	 * 
	 */
	private void supprimer() {
		
		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Ce trajet va être supprimé de la liste et ne sera plus proposé dans les prochaines missions.\nConfirmez vous cette suppression ?",
				"OUI", "NON"))
			return;

		
		try {

			ec.deleteObject(currentTrajet);
			ec.saveChanges();
			
			actualiser();
			
		}
		catch (Exception ex) {
			
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}
		
	}
	
	/**
	 * 
	 *
	 */
	private void filter()	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		if (!StringCtrl.chaineVide(filtreDepart.getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODistancesKm.LIEU_DEPART_KEY + " caseInsensitiveLike %@",new NSArray("*"+filtreDepart.getText()+"*")));

		if (!StringCtrl.chaineVide(filtreArrivee.getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODistancesKm.LIEU_ARRIVEE_KEY + " caseInsensitiveLike %@",new NSArray("*"+filtreArrivee.getText()+"*")));
		
		eod.setQualifier(new EOAndQualifier(mesQualifiers));

		eod.updateDisplayedObjects();
		myEOTable.updateData();
		
		lblNbVehicules.setText(eod.displayedObjects().count() + " Trajets");

	}
	
	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupTypeTransportListener implements ActionListener	{
        public PopupTypeTransportListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	filter();
        
        }
    }
	
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert � filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	
	private class ListenerTrajet implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
				modifier();
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			currentTrajet = (EODistancesKm)eod.selectedObject();
			updateUI();
			
		}
	}
	
	
	public void updateUI() {
		
		buttonUpdate.setEnabled(currentTrajet != null);
		buttonDelete.setEnabled(currentTrajet != null);
		
	}
	
}
