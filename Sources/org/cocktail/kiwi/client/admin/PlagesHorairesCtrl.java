package org.cocktail.kiwi.client.admin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderPlageHoraire;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOPlageHoraire;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.TimeCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class PlagesHorairesCtrl  {

	private static PlagesHorairesCtrl sharedInstance;
	private EOEditingContext ec;

	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	private	JTextField	tfHeureDebut, tfHeureFin;
	
	private	JButton		buttonUpdate, buttonValidate, buttonCancel;
	
	private ActionUpdate actionUpdate = new ActionUpdate();   

	protected ActionValidate 	actionValidate = new ActionValidate();
	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionClose 		actionClose = new ActionClose();

	protected	ListenerPlages listenerPlages = new ListenerPlages();
	
	protected JPanel viewTable;

	private		EOPlageHoraire currentPlage;	

	/**
	 * 
	 *
	 */
	public PlagesHorairesCtrl(EOEditingContext editingContext)	{
		
		super();
		ec = editingContext;
		
		gui_init();
		gui_initView();
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PlagesHorairesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PlagesHorairesCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 */
	private void gui_initTextFields()	{
		
        tfHeureDebut = new JTextField("");
        tfHeureDebut.setPreferredSize(new Dimension(75,18));
        tfHeureDebut.setHorizontalAlignment(JTextField.CENTER);
        tfHeureDebut.setFont(new Font("Times", Font.PLAIN, 11));
        
        tfHeureFin = new JTextField("");
        tfHeureFin.setPreferredSize(new Dimension(75,18));
        tfHeureFin.setHorizontalAlignment(JTextField.CENTER);
        tfHeureFin.setFont(new Font("Times", Font.PLAIN, 11));

	}
	
	
	/**
	 * 
	 * @param yn
	 */
	private void setSaisieEnabled(boolean yn)	{

		actionValidate.setEnabled(yn);
		actionCancel.setEnabled(yn);

		actionUpdate.setEnabled(!yn);

		myEOTable.setEnabled(!yn);
		
		CocktailUtilities.initTextField(tfHeureDebut, false, yn);
		CocktailUtilities.initTextField(tfHeureFin, false, yn);
		
	}
	
	
	
	
	/**
	 * 
	 *
	 */
	private void gui_initButtons()	{
		
		buttonUpdate = new JButton(actionUpdate);
		buttonUpdate.setPreferredSize(new Dimension (22,22));
		buttonUpdate.setToolTipText("Modification de la rubrique");
						
		buttonUpdate.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		
		buttonValidate = new JButton(actionValidate);
		buttonValidate.setPreferredSize(new Dimension (22,22));
		buttonValidate.setToolTipText("Modification de la rubrique");

		buttonCancel = new JButton(actionCancel);
		buttonCancel.setPreferredSize(new Dimension (22,22));
		buttonCancel.setToolTipText("Modification de la rubrique");
		
	}

	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildUpdatePanel()	{

		JPanel updatePanel = new JPanel(new BorderLayout());

		JLabel labelHeureDebut = new JLabel("Heure Début : ");
		JLabel labelHeureFin = new JLabel("Heure Fin : ");

		JPanel flowPanel = new JPanel(new FlowLayout());
		
		flowPanel.add(labelHeureDebut);
		flowPanel.add(tfHeureDebut);
		flowPanel.add(labelHeureFin);
		flowPanel.add(tfHeureFin);

		flowPanel.add(buttonValidate);
		flowPanel.add(buttonCancel);

		updatePanel.add(flowPanel, BorderLayout.CENTER);

		return updatePanel;
	}

	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildSouthPanel() {
		
		JPanel panel = new JPanel(new BorderLayout());
	
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 115, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		panel.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelButtons, BorderLayout.EAST);


		return panel;
	}
	
	
	private JPanel gui_buildNorthPanel() {
		
		JPanel panel = new JPanel(new BorderLayout());
		
		panel.setBorder(BorderFactory.createEmptyBorder(0,0,5,0));

		return panel;
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildCenterPanel()	{

		JPanel panel = new JPanel(new BorderLayout());

		ArrayList arrayList = new ArrayList();
		arrayList.add(buttonUpdate);
		
		JPanel eastPanel = new JPanel(new BorderLayout());
		eastPanel.setBorder(BorderFactory.createEmptyBorder(2,5,2,2));
		eastPanel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		eastPanel.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.NORTH);

		panel.add(viewTable, BorderLayout.CENTER);
		panel.add(eastPanel, BorderLayout.EAST);
		panel.add(gui_buildUpdatePanel(), BorderLayout.SOUTH);

		return panel;
	}
	
	
	/**
	 * 
	 *
	 */
	private void gui_initView()	{
		
        mainWindow = new JDialog(mainFrame, " Définition des plages horaires ", true);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

        gui_initTextFields();
        
        gui_initButtons();

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(500, 350));
		mainView.add(gui_buildNorthPanel(), BorderLayout.NORTH);
		mainView.add(gui_buildCenterPanel(), BorderLayout.CENTER);
		mainView.add(gui_buildSouthPanel(), BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
		
		tfHeureDebut.addFocusListener(new ListenerTextFieldHeureDebut());
		tfHeureDebut.addActionListener(new ActionListenerHeureDebut());
		
		tfHeureFin.addFocusListener(new ListenerTextFieldHeureFin());
		tfHeureFin.addActionListener(new ActionListenerHeureFin());

		setSaisieEnabled(false);
	}
		

	public void open()	{

		updateData();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();

	}
	

	
	/**
	 * 
	 *
	 */
	public void updateData()	{
		
		eod.setObjectArray(FinderPlageHoraire.findPlagesHoraires(ec));
	
		myEOTable.updateData();
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void gui_init() {
		
		eod = new EODisplayGroup();
				
        viewTable = new JPanel();
		
		initTableModel();
		initTable();
				
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		
		myEOTable.addListener(listenerPlages);

	}
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod,EOPlageHoraire.PLA_ORDRE_KEY, "", 30);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "plaType", "Type", 75);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOPlageHoraire.PLA_DEBUT_KEY, "Heure Début", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOPlageHoraire.PLA_FIN_KEY, "Heure Fin", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "plageObservations", "Observations", 200);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);		

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}

	
    
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CLOSE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
        	myTableModel.fireTableDataChanged();
        	
        	if (ec.updatedObjects().count() > 0)	{
        		try {
        		
        			if (EODialogs.runConfirmOperationDialog("Attention",
        					"Voulez vous enregistrer les modifications apportées ?",
        					"OUI", "NON"))  	       			
        				ec.saveChanges();
        		}
        		catch (Exception ex)	{
        			ex.printStackTrace();
        			EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement des modifications !");
        		}
        	}
        	
        	mainWindow.dispose();
        	
        }  
	} 
	
	
/**
 * 
 * @author cpinsard
 *
 */
	   private class ListenerPlages implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
				actionUpdate.actionPerformed(null);
			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		
			currentPlage = (EOPlageHoraire)eod.selectedObject();
			
			if (currentPlage != null) {

				tfHeureDebut.setText(currentPlage.plaDebut());
				tfHeureFin.setText(currentPlage.plaFin());
				
			}

		}
	   }
	    
	   
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private  final class ActionValidate extends AbstractAction {
			
			public ActionValidate() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
			}
			
			public void actionPerformed(ActionEvent e) {

				try {
					
					currentPlage.setPlaDebut(tfHeureDebut.getText());
					currentPlage.setPlaFin(tfHeureFin.getText());
					
					ec.saveChanges();
					myEOTable.updateUI();
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
				
				setSaisieEnabled(false);

			}  
		}
		
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private  final class ActionCancel extends AbstractAction {
			
			public ActionCancel() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CANCEL);
			}
			
			public void actionPerformed(ActionEvent e) {
								
				listenerPlages.onSelectionChanged();
				setSaisieEnabled(false);

			}  
		}

		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private  final class ActionUpdate extends AbstractAction {
			
			public ActionUpdate() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_UPDATE);
			}
			
			public void actionPerformed(ActionEvent e) {
											
				setSaisieEnabled(true);
				
			}  
		}
		
	
		/** 
		 * Classe d'ecoute sur le debut de contrat de travail.
		 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
		 */
		public class ActionListenerHeureDebut implements ActionListener	{
			public void actionPerformed(ActionEvent e)	{
				if ("".equals(tfHeureDebut.getText()))	return;
				
				String myTime = TimeCtrl.timeCompletion(tfHeureDebut.getText());
				if ("".equals(myTime))	{
					MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début n'est pas valide !");
					tfHeureDebut.selectAll();
				}
				else	{
					tfHeureDebut.setText(myTime);
					tfHeureFin.requestFocus();
				}
			}
		}
		
		
		/** 
		 * Classe d'ecoute sur la date de fin de contrat de travail.
		 * Permet d'effectuer la completion de cette date 
		 */
		public class ActionListenerHeureFin implements ActionListener	{
			public void actionPerformed(ActionEvent e)	{
				if ("".equals(tfHeureFin.getText()))	return;
				
				String myTime = TimeCtrl.timeCompletion(tfHeureFin.getText());
				if ("".equals(myTime))	{
					MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de fin n'est pas valide !");
					tfHeureFin.selectAll();
				}
				else	{
					tfHeureFin.setText(myTime);
				}
			}
		}
		
		/** 
		 * Classe d'ecoute sur le debut de contrat de travail.
		 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
		 */
		public class ListenerTextFieldHeureDebut implements FocusListener	{
			public void focusGained(FocusEvent e) 	{}
			
			public void focusLost(FocusEvent e)	{
				if ("".equals(tfHeureDebut.getText()))	return;
				
				String myTime = TimeCtrl.timeCompletion(tfHeureDebut.getText());
				if ("".equals(myTime))	{
					MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début n'est pas valide !");
					tfHeureDebut.selectAll();
				}
				else	{
					tfHeureDebut.setText(myTime);
					tfHeureFin.requestFocus();
				}
			}
		}
		
		/** 
		 * Classe d'ecoute sur le debut de contrat de travail.
		 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
		 */
		public class ListenerTextFieldHeureFin implements FocusListener	{
			public void focusGained(FocusEvent e) 	{}
			
			public void focusLost(FocusEvent e)	{
				if ("".equals(tfHeureFin.getText()))	return;
				
				String myTime = TimeCtrl.timeCompletion(tfHeureFin.getText());
				if ("".equals(myTime))	{
					MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de fin n'est pas valide !");
					tfHeureFin.selectAll();
				}
				else	{
					tfHeureFin.setText(myTime);
				}
			}
		}
		
}
