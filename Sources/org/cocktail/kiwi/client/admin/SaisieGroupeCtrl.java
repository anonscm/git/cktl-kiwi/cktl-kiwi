package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.metier.EOSegGroupeInfo;
import org.cocktail.kiwi.client.nibctrl.SaisieGroupeView;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieGroupeCtrl
{
	private static SaisieGroupeCtrl sharedInstance;
	
	private 	EOEditingContext	ec;
	private 	SaisieGroupeView 	myView;	
	
	private EOSegGroupeInfo currentGroupe;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieGroupeCtrl (EOEditingContext globalEc) {

		ec = globalEc;
		
		myView = new SaisieGroupeView(new JFrame(), true);
		
		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});
				
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieGroupeCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieGroupeCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		myView.getTfLibelle().setText("");
	
	}
	
	
	
	/**
	 * 
	 * Fournis = null ==> Saisie du fournisseur
	 * 
	 * @param fournis		Fournisseur associe au vehicule	
	 * @return
	 */
	public EOSegGroupeInfo ajouter()	{

		clearTextFields();

		modeModification = false;
				
		currentGroupe = EOSegGroupeInfo.creer(ec);
		
		myView.getTfLibelle().setText("");
		
		myView.setVisible(true);

		return currentGroupe;
		
	}
	
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifier(EOSegGroupeInfo groupe)	{
					
		clearTextFields();

		currentGroupe = groupe;


		modeModification = true;

		myView.getTfLibelle().setText(currentGroupe.stgLibelle());
		
		myView.setVisible(true);
		
		return (currentGroupe != null);
	}
	
	
	
	/**
	 *
	 */
	private void valider()	{
		
		try {
			
			if (myView.getTfLibelle().getText().length() == 0) {
				
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez entrer un libellé !");				
				return;
				
			}

			currentGroupe.setStgLibelle(myView.getTfLibelle().getText());
			
		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		myView.setVisible(false);
		
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentGroupe);
			
		currentGroupe = null;		
		myView.setVisible(false);
		
	}
	
}