package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactoryTypeTransport;
import org.cocktail.kiwi.client.finders.FinderTypeTransport;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.nibctrl.SaisieVehiculeView;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieTypeTransportCtrl extends SaisieVehiculeView 
{
	private static SaisieTypeTransportCtrl sharedInstance;
	
	private 	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private 	EOEditingContext		ec;
		
	private EOTypeTransport 			currentTypeTransport;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieTypeTransportCtrl (EOEditingContext globalEc) {

		super(new JFrame(), true);

		ec = globalEc;
		
		buttonValider.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		buttonAnnuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});
		
		setTypes(FinderTypeTransport.findTypesTransportVehicule(ec));

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieTypeTransportCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieTypeTransportCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		fournisseur.setText("");
		compagnie.setText("");
		police.setText("");
		marque.setText("");
		immatriculation.setText("");
		puissance.setSelectedItem("6");
	
	}
	
	
	
	/**
	 * 
	 * Fournis = null ==> Saisie du fournisseur
	 * 
	 * @param fournis		Fournisseur associe au vehicule	
	 * @return
	 */
	public EOTypeTransport ajouter()	{

		clearTextFields();
						
		// Creation et initialisation du nouveau vehicule
		currentTypeTransport = FactoryTypeTransport.sharedInstance().creer(ec);
		
		setVisible(true);

		return currentTypeTransport;
		
	}
	
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifier(EOTypeTransport type)	{
				
		clearTextFields();

		currentTypeTransport = type;

		setVisible(true);
		
		return (currentTypeTransport != null);
	}
	
		
	
	/**
	 *
	 */
	private void valider()	{
		
		try {

			
		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		dispose();
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentTypeTransport);
			
		currentTypeTransport = null;
		
		dispose();
		
	}
	
}