package org.cocktail.kiwi.client.admin;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderTitreMission;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOTitreMission;
import org.cocktail.kiwi.client.nibctrl.TitreMissionView;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;

public class TitreMissionCtrl {

	private static TitreMissionCtrl sharedInstance;
	
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private EODisplayGroup eod;
	private TitreMissionView myView;

	private EOTitreMission currentTitreMission;
	
	private TitreMissionRenderer	monRendererColor = new TitreMissionRenderer();

	public TitreMissionCtrl(EOEditingContext editingContext) {
		
		super();

		ec = editingContext;
	
		eod = new EODisplayGroup();
		
		myView = new TitreMissionView(eod, monRendererColor);
		
		myView.getButtonAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getButtonModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});
		
		myView.getButtonInvalider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				invalider();
			}
		});
		
		myView.getButtonAjouter().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getButtonModifier().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getButtonInvalider().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		
		myView.getMyEOTable().addListener(new ListenerTitre());

	}

	
	public static TitreMissionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new TitreMissionCtrl(editingContext);
		return sharedInstance;
	}
	
	
	
	/**
	 * 
	 */
	private void actualiser() {
		
		eod.setObjectArray(FinderTitreMission.findTitresMission(ec));
		myView.getMyEOTable().updateData();
				
	}
	
	/**
	 * 
	 */
	public void open() {
		
		actualiser();
		
		myView.show();
		
	}
	
	
	/**
	 * 
	 */
	private void ajouter() {

		try {

			EOTitreMission myTitre = SaisieTitreMissionCtrl.sharedInstance(ec).ajouter();

			if (myTitre != null) {

				ec.saveChanges();
				actualiser();

			}

		}
		catch (Exception e) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur d'ajout d'un nouveau Titre de Mission \n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();
		}

		actualiser();
		
	}
	
	/**
	 * 
	 */
	private void modifier() {
				
		try {

			if (SaisieTitreMissionCtrl.sharedInstance(ec).modifier( currentTitreMission)) {

				ec.saveChanges();
				myView.getMyEOTable().updateData();

			}
			else
				ec.revert();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur d'enregistrement du Titre de Mission !");
			ec.revert();
		}

	}
	
	
	/**
	 * 
	 */
	private void invalider() {
		
		try {
			
			if (currentTitreMission.titEtat().equals("VALIDE")) {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous l'invalidation du titre " + currentTitreMission.titLibelle() + " ?",
						"OUI", "NON"))
					currentTitreMission.setTitEtat("INVALIDE");
			}
			else {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous la validation du titre " + currentTitreMission.titLibelle() + " ?",
						"OUI", "NON"))
					currentTitreMission.setTitEtat("VALIDE");
			}
				
			ec.saveChanges();
			
			actualiser();
			
		}
		catch (Exception ex) {
			
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}
	}
	
	
 	
	private class ListenerTitre implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		
		public void onDbClick() {
			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
				modifier();
		}
		public void onSelectionChanged() {

			currentTitreMission = (EOTitreMission)eod.selectedObject();

			if (currentTitreMission != null) {
				if (currentTitreMission.titEtat().equals("INVALIDE"))
					myView.getButtonInvalider().setIcon(CocktailIcones.ICON_VALID);
				else
					myView.getButtonInvalider().setIcon(CocktailIcones.ICON_DELETE);
			}

			updateUI();
			
		}
	}
	
	
	private void updateUI() {
		
		myView.getButtonModifier().setEnabled(currentTitreMission != null);
		myView.getButtonInvalider().setEnabled(currentTitreMission != null);
		
	}
	
	private class TitreMissionRenderer extends ZEOTableCellRenderer	{
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (isSelected)
				return leComposant;
	
			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOTitreMission obj = (EOTitreMission) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.titEtat().equals("INVALIDE"))	{
				leComposant.setBackground(new Color(255,151,96));
			}
			else	{
					leComposant.setBackground(new Color(142,255,134));
			}
			
			return leComposant;
		}
	}

	
}
