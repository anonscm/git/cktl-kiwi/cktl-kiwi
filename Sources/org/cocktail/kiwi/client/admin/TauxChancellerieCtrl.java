/*
 * Created on 3 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOLogTelechargement;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.nibctrl.TauxChancellerieView;
import org.cocktail.kiwi.common.utilities.CRICursor;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TauxChancellerieCtrl{


	private static TauxChancellerieCtrl sharedInstance;
	private ApplicationClient NSApp;
	private	EOEditingContext ec;

	private TauxChancellerieView myView;

	public EODisplayGroup eod = new EODisplayGroup();

	private EOLogTelechargement currentTelechargement;

	private EOMissionParametres currentParametreJours;

	/** 
	 * Constructeur 
	 */
	public TauxChancellerieCtrl(EOEditingContext editingContext)	{
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;

		myView = new TauxChancellerieView(new JFrame(), true, eod);

		myView.getBtnTelecharger().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				telecharger();
			}
		});

		currentParametreJours = FinderMissionParametres.findParametre(ec, EOMissionParametres.ID_TELECHARGEMENT);			

		NSArray mySort = new NSArray(new EOSortOrdering(EOLogTelechargement.LTC_DATE_KEY, EOSortOrdering.CompareDescending));
		eod.setSortOrderings(mySort);
		myView.getBtnTelecharger().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_TAUX_CHANCELLERIE));

		myView.getMyEOTable().addListener(new ListenerTelechargement());

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TauxChancellerieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TauxChancellerieCtrl(editingContext);
		return sharedInstance;
	}

	private class ListenerTelechargement implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			currentTelechargement = (EOLogTelechargement)eod.selectedObject();

			if (currentTelechargement != null)	{
				myView.getAreaInfos1().setText(currentTelechargement.ltcInfo());
				myView.getAreaInfos2().setText(currentTelechargement.ltcMissions());
			}			
		}
	}

	private void supprimer() {

		try {

			if (currentTelechargement != null && currentTelechargement.ltcInfo() != null) {

				ec.deleteObject(currentTelechargement);
				ec.saveChanges();
				actualiserLogs();
			}
			else {
				
				EODialogs.runErrorDialog("ERREUR","Vous ne pouvez supprimer un telechargement ");
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param sender
	 */
	public void telecharger()	{

		myView.getTfObservations().setText("Téléchargement en cours ...");

		CRICursor.setWaitCursor(myView);

		Boolean result = ServerProxy.traiterTelechargements(ec);
		if (result.booleanValue())
			myView.getTfObservations().setText("Traitement terminé. OK");
		else
			myView.getTfObservations().setText("Erreur de traitement : (service info) - KO");

		// On recharge les logs
		actualiserLogs();

		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 *
	 */
	public void actualiserLogs()	{	
		eod.setObjectArray(EOLogTelechargement.fetchAll(ec, EOLogTelechargement.SORT_ARRAY_DATE_DESC));
		myView.getMyEOTable().updateData();
	}

	public void open()	{
		actualiserLogs();
		myView.setVisible(true);
	}
}