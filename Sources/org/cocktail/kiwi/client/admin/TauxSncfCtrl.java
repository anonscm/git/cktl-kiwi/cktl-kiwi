/*
 * Created on 3 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.admin;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOTarifSncf;
import org.cocktail.kiwi.client.nibctrl.TauxSncfView;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TauxSncfCtrl {


	private static TauxSncfCtrl sharedInstance;
	private ApplicationClient NSApp;
	private	EOEditingContext ec;

	private TauxSncfView myView;

	private TarifsCellRenderer	myRenderer = new TarifsCellRenderer();

	public EODisplayGroup eod = new EODisplayGroup();


	/** 
	 * Constructeur 
	 */
	public TauxSncfCtrl(EOEditingContext editingContext)	{

		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;
		
		myView = new TauxSncfView(new JFrame(), true, eod, myRenderer, NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnValider().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		myView.getClassesSncf().addActionListener(new PopupListener());

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TauxSncfCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TauxSncfCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 *
	 */
	public void open()	{

		actualiserTarifs();

		myView.setVisible(true);

	}

	/**
	 * 
	 *
	 */
	public void actualiserTarifs()	{

		CRICursor.setWaitCursor(myView);

		NSArray mySort = new NSArray(new EOSortOrdering(EOTarifSncf.DT_DEBUT_KEY, EOSortOrdering.CompareAscending));
		EOQualifier myQualifier = null;
		if (myView.getClassesSncf().getSelectedIndex() == 0)
			myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTarifSncf.CLASSE_KEY + " = 1", null);
		else
			myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTarifSncf.CLASSE_KEY + " = 2", null);

		EOFetchSpecification fs = new EOFetchSpecification(EOTarifSncf.ENTITY_NAME, myQualifier, mySort);

		eod.setObjectArray(ec.objectsWithFetchSpecification(fs));
		myView.getMyEOTable().updateData();

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @param sender
	 */
	public void valider()	{

		try {

			//			for (int i=0;i<eod.displayedObjects().count();i++) {
			//				
			//				EOTarifSncf tarif = (EOTarifSncf)eod.displayedObjects().objectAtIndex(i);
			//				tarif.setDtDebut(null)
			//			}

			ec.saveChanges();

			MsgPanel.sharedInstance().runConfirmationDialog("OK", "Modifications enregistrées");

		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur de sauvegarde des tarifs SNCF !");
			ec.revert();
			myView.getMyEOTable().updateUI();
		}
	}

	/**
	 * 
	 * @param sender
	 */
	public void annuler()	{
		ec.revert();
		myView.dispose();
	}

	/** 
	 * Classe d'ecoute pour le changement de secteur ==> Modification de la liste des agents 
	 */
	private class PopupListener implements ActionListener	{
		public PopupListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			actualiserTarifs();

		}
	}


	/**
	 * Classe servant a colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant generique, independamment de la table (passer eventuellement par une interface).
	 */
	public class TarifsCellRenderer extends ZEOTableCellRenderer	{

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if( column == 4 || column == 3)
				leComposant.setBackground(CocktailConstantes.BG_COLOR_WHITE);
			else
				leComposant.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);

			if(isSelected)
				leComposant.setBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);

			return leComposant;
		}
	}



}
