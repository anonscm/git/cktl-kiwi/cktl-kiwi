package org.cocktail.kiwi.client.admin;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOVehicule;
import org.cocktail.kiwi.client.nibctrl.VehiculesDetailView;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSMutableArray;

public class VehiculesDetailCtrl {

	private static final long serialVersionUID = -2128488379944500761L;

	private static VehiculesDetailCtrl sharedInstance;
	
	private EODisplayGroup eod = new EODisplayGroup();
	private EOEditingContext ec;
	
	private VehiculesDetailView myView;
	
	private EOVehicule currentVehicule;
	
	public VehiculesDetailCtrl(EOEditingContext editingContext) {
		
		super();

		ec = editingContext;
		
		myView = new VehiculesDetailView(eod);
			
		myView.getButtonClose().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fermer();
			}
		});
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOSegmentTarif.MISSION_KEY+"."+EOMission.TO_EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY , EOSortOrdering.CompareDescending));
		mySort.addObject(new EOSortOrdering(EOSegmentTarif.MISSION_KEY+"."+EOMission.MIS_NUMERO_KEY , EOSortOrdering.CompareDescending));
				
		eod.setSortOrderings(mySort);
		
	}

	
	public static VehiculesDetailCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new VehiculesDetailCtrl(editingContext);
		return sharedInstance;
	}
	
	
	

	private void fermer() {
		
		myView.setVisible(false);
				
	}
	

	
	public void open(EOVehicule vehicule) {
			
		currentVehicule = vehicule;
		
		myView.getTfTitre().setText(currentVehicule.fournis().nom() + " " + currentVehicule.fournis().prenom() + 
				" - Missions associées au véhicule " + currentVehicule.vehImmatriculation());

		eod.setObjectArray(EOSegmentTarif.findForVehicule(ec, currentVehicule));
		myView.getMyEOTable().updateData();
		
		myView.setVisible(true);
		
	}

	
	
	
}
