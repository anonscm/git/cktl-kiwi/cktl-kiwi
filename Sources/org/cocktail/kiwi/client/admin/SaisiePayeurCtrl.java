package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.factory.FactoryPayeur;
import org.cocktail.kiwi.client.metier.EOPayeur;
import org.cocktail.kiwi.client.nibctrl.SaisiePayeurView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisiePayeurCtrl
{
	private static SaisiePayeurCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
		
	private SaisiePayeurView myView;
	
	private EOPayeur 			currentPayeur;
	
	private	boolean modeModification;
	

	public SaisiePayeurCtrl (EOEditingContext globalEc) {

		ec = globalEc;
		
		myView = new SaisiePayeurView(new JFrame(), true);

		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});			
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisiePayeurCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisiePayeurCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		myView.getTfCode().setText("");
		myView.getTfLibelleLong().setText("");
		myView.getTfLibelleCourt().setText("");
		myView.getTfLibelleImprime().setText("");
		myView.getTfVille().setText("");
		myView.getTfOrdonnateur().setText("");
	
	}
	

	public EOPayeur ajouter()	{

		clearTextFields();

		modeModification = false;
				
		// Creation et initialisation du nouveau titre de mission
		currentPayeur = FactoryPayeur.sharedInstance().creer(ec);
				
		myView.setVisible(true);

		return currentPayeur;
		
	}
	

	public boolean modifier(EOPayeur payeur)	{
					
		clearTextFields();

		currentPayeur = payeur;

		modeModification = true;

		if (currentPayeur.codePayeur() != null)
			myView.getTfCode().setText(currentPayeur.codePayeur());
		
		if (currentPayeur.libellePayeurLong() != null)
			myView.getTfLibelleLong().setText(currentPayeur.libellePayeurLong());

		if (currentPayeur.libellePayeurCourt() != null)
			myView.getTfLibelleCourt().setText(currentPayeur.libellePayeurCourt());

		if (currentPayeur.libellePayeurImprime() != null)
			myView.getTfLibelleImprime().setText(currentPayeur.libellePayeurImprime());

		if (currentPayeur.ville() != null)
			myView.getTfVille().setText(currentPayeur.ville());

		if (currentPayeur.ordonnateur() != null)
			myView.getTfOrdonnateur().setText(currentPayeur.ordonnateur());

		myView.setVisible(true);
		
		return (currentPayeur != null);
		
	}
	
	
	private void valider()	{
		
		try {
			
			if (!StringCtrl.chaineVide(myView.getTfCode().getText())) {

				currentPayeur.setCodePayeur(myView.getTfCode().getText().toUpperCase());

				// Controle de l'existence du code
				if (!modeModification) {
					EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeur.CODE_PAYEUR_KEY + " = %@", new NSArray(currentPayeur.codePayeur()));
					EOPayeur payeur = EOPayeur.fetchFirstByQualifier(ec, myQualifier);
					if (payeur != null)
						throw new ValidationException("Ce code ('" + payeur.codePayeur() + "') est déjà utilisé ( '" + payeur.libellePayeurLong() + "' ) !" );
				}
			}
			else
				currentPayeur.setCodePayeur(null);
			
			if (!StringCtrl.chaineVide(myView.getTfLibelleLong().getText()))
				currentPayeur.setLibellePayeurLong(myView.getTfLibelleLong().getText());

			if (!StringCtrl.chaineVide(myView.getTfLibelleCourt().getText()))
				currentPayeur.setLibellePayeurCourt(myView.getTfLibelleCourt().getText());			
				
			if (!StringCtrl.chaineVide(myView.getTfLibelleImprime().getText()))
				currentPayeur.setLibellePayeurImprime(myView.getTfLibelleImprime().getText());	
			else
				currentPayeur.setLibellePayeurImprime(myView.getTfLibelleLong().getText());	

			if (!StringCtrl.chaineVide(myView.getTfVille().getText()))
				currentPayeur.setVille(myView.getTfVille().getText());

			if (!StringCtrl.chaineVide(myView.getTfOrdonnateur().getText()))
				currentPayeur.setOrdonnateur(myView.getTfOrdonnateur().getText());

			ec.saveChanges();

			myView.setVisible(false);

		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();
			return;
		}
	}
	
	private void annuler()	{

		ec.revert();
		myView.setVisible(false);
		
	}
	
}