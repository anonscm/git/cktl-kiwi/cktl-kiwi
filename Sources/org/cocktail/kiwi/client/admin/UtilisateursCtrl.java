/* SimulationBudgetaire.java created by cpinsard on Tue 24-Jun-2003 */

package org.cocktail.kiwi.client.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderUtilisateur;
import org.cocktail.kiwi.client.finders.FinderUtilisateurFonction;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;

import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


public class UtilisateursCtrl extends EOModalDialogController	{
	
	public static UtilisateursCtrl sharedInstance;
	
	// Objects graphiques
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;
	
	public JPanel viewTableUtilisateurs, viewTableFonctions;
	
	private JComboBox fonctions;
	
	public EODisplayGroup eodUtilisateurs, eodFonctions;
	
	private ZEOTable myEOTableUtilisateurs,  myEOTableFonctions;
	private ZEOTableModel myTableModelUtilisateurs, myTableModelFonctions;
	private TableSorter myTableSorterUtilisateurs, myTableSorterFonctions;
		
	private JLabel nbUtilisateurs;
	
	// Variables locales
	ApplicationClient 	NSApp;
	
	protected	EOUtilisateur	currentUtilisateur;
	
	protected ActionClose 		actionClose = new ActionClose();
	
	protected	ListenerUtilisateurs listenerUtilisateurs = new ListenerUtilisateurs();
		
	protected	EOEditingContext	ec;
	
	/** 
	 *	Constructeur 
	 */
	public UtilisateursCtrl (EOEditingContext globalEc)	{
		super();
		
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
		
		ec = globalEc;	
		
		initGUI();
		initView();
	}
	
	public static UtilisateursCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new UtilisateursCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * Initialisation de tous les objets graphiques du module.
	 * Mise en place des listeners, couleurs, boutons, EOTables, ...
	 */
	private void initView()	{
		
		mainWindow = new JDialog(mainFrame, "Consultation des utilisateurs", true);
		
		nbUtilisateurs = new JLabel();
		
		fonctions = new JComboBox();
		fonctions.addItem("*");
		NSArray fonctionsKiwi = EOFonction.findForTypeApplication(ec, ApplicationClient.APPLICATION_ID);

		for (int i=0;i<fonctionsKiwi.count();i++)
			fonctions.addItem((EOFonction)fonctionsKiwi.objectAtIndex(i));
		
		fonctions.addActionListener(new PopupListener());

		viewTableUtilisateurs.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
//		arrayList.add(actionValidate);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);
		
		JTextField labelFonctions = new JTextField(" Fonctions Autorisées");
		labelFonctions.setBorder(BorderFactory.createEmptyBorder());
		labelFonctions.setPreferredSize(new Dimension(100, 18));
		labelFonctions.setEditable(false);
		labelFonctions.setFocusable(false);
		labelFonctions.setHorizontalAlignment(JTextField.LEFT);
		labelFonctions.setForeground(CocktailConstantes.BG_COLOR_WHITE);
		labelFonctions.setBackground(new Color(194,176,190));

		JPanel panelNorthFonctions = new JPanel(new BorderLayout());
		panelNorthFonctions.add(labelFonctions, BorderLayout.CENTER);

		JPanel viewFonctions = new JPanel(new BorderLayout());
		viewFonctions.setPreferredSize(new Dimension(250, 300));
		viewFonctions.add(panelNorthFonctions, BorderLayout.NORTH);
		viewFonctions.add(viewTableFonctions, BorderLayout.CENTER);

		JPanel leftPanel = new JPanel(new BorderLayout());
		leftPanel.add(viewTableUtilisateurs, BorderLayout.CENTER);
		leftPanel.add(nbUtilisateurs, BorderLayout.SOUTH);

		
		JPanel panelRight = new JPanel(new BorderLayout());
		
		arrayList  = new ArrayList();
		arrayList.add(viewFonctions);
		
		JPanel northPanel = new JPanel(new BorderLayout());
		panelSouth.setBorder(BorderFactory.createEmptyBorder(3,3,5,3));
		northPanel.add(fonctions);
		
		panelRight.add(northPanel, BorderLayout.NORTH);
		panelRight.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.CENTER);
		
		JSplitPane splitPane = ZUiUtil.buildHorizontalSplitPane(
				leftPanel,
				panelRight, 0.60, 0.60);
				
		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(650, 575));
		mainView.add(splitPane, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
		
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
		
	}
	
	
	/**
	 * 
	 *
	 */
	public void open()	{
		
		
		eodUtilisateurs.setObjectArray(FinderUtilisateur.findUtilisateursForApplication(ec, ApplicationClient.APPLICATION_ID));
		myEOTableUtilisateurs.updateData();

		nbUtilisateurs.setText(eodUtilisateurs.displayedObjects().count() + " Utilisateurs");
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eodUtilisateurs = new EODisplayGroup();
		eodFonctions= new EODisplayGroup();
		
		eodUtilisateurs.setSortOrderings(new NSArray(new EOSortOrdering("individu.nomUsuel", EOSortOrdering.CompareAscending)));
		eodFonctions.setSortOrderings(new NSArray(new EOSortOrdering("fonction.fonDescription", EOSortOrdering.CompareAscending)));
		
		viewTableUtilisateurs = new JPanel();
		viewTableFonctions = new JPanel();
		
		initTableModel();
		initTable();
		
		myEOTableUtilisateurs.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTableUtilisateurs.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTableUtilisateurs.setSelectionForeground(CocktailConstantes.BG_COLOR_WHITE);
		myEOTableUtilisateurs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTableUtilisateurs.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableUtilisateurs.removeAll();
		viewTableUtilisateurs.setLayout(new BorderLayout());
		viewTableUtilisateurs.add(new JScrollPane(myEOTableUtilisateurs), BorderLayout.CENTER);
		
		// Fonctions
		myEOTableFonctions.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTableFonctions.setForeground(CocktailConstantes.BG_COLOR_LIGHT_GREY);
		myEOTableFonctions.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		
		myEOTableFonctions.setEnabled(false);
		
		viewTableFonctions.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableFonctions.removeAll();
		viewTableFonctions.setLayout(new BorderLayout());
		viewTableFonctions.add(new JScrollPane(myEOTableFonctions), BorderLayout.CENTER);

	}
	
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{
		
		myEOTableUtilisateurs = new ZEOTable(myTableSorterUtilisateurs);
		myEOTableUtilisateurs.addListener(listenerUtilisateurs);
		myTableSorterUtilisateurs.setTableHeader(myEOTableUtilisateurs.getTableHeader());		
		
		myEOTableFonctions = new ZEOTable(myTableSorterFonctions);
		myEOTableFonctions.setTableHeader(null);

	}
	
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodUtilisateurs, "individu.nomUsuel", "Nom", 100);
		myCols.add(col);
		col = new ZEOTableModelColumn(eodUtilisateurs, "individu.prenom", "Prénom", 100);
		myCols.add(col);
		
		myTableModelUtilisateurs = new ZEOTableModel(eodUtilisateurs, myCols);
		myTableSorterUtilisateurs = new TableSorter(myTableModelUtilisateurs);
				
		// Fonctions
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodFonctions, "fonction.fonDescription", "Fonctions Autorisées", 240);
		myCols.add(col);
		
		myTableModelFonctions = new ZEOTableModel(eodFonctions, myCols);
		myTableSorterFonctions = new TableSorter(myTableModelFonctions);
		
	}
	
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerUtilisateurs implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			
			currentUtilisateur = (EOUtilisateur) eodUtilisateurs.selectedObject();
			
			eodFonctions.setObjectArray(new NSArray());
			
			if (currentUtilisateur != null)
				eodFonctions.setObjectArray(FinderUtilisateurFonction.findFonctionsForUtilisateur(ec, currentUtilisateur, null));
			
			myEOTableFonctions.updateData();
			myTableModelFonctions.fireTableDataChanged();
			
		}
	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionClose extends AbstractAction {
		
		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CLOSE);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			mainWindow.dispose();
			
		}  
	} 
	
	
	/** 
	 * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
	 */
	private class PopupListener implements ActionListener	{
		public PopupListener() {super();}
		public void actionPerformed(ActionEvent anAction) {
			
			if (fonctions.getSelectedIndex() > 0)
				eodUtilisateurs.setObjectArray(FinderUtilisateur.findUtilisateursForFonction(ec, (EOFonction)fonctions.getSelectedItem()));
			else
				eodUtilisateurs.setObjectArray(FinderUtilisateur.findUtilisateursForApplication(ec, ApplicationClient.APPLICATION_ID));
				
			myEOTableUtilisateurs.updateData();
			
			nbUtilisateurs.setText(eodUtilisateurs.displayedObjects().count() + " Utilisateurs");
			
		}
	}
	
}