/*
 * Created on 3 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.admin;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.Factory;
import org.cocktail.kiwi.client.metier.EOMissionPreferencesPerso;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOPayeur;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EOTitreMission;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.select.CodeAnalytiqueSelectCtrl;
import org.cocktail.kiwi.client.select.LolfSelectCtrl;
import org.cocktail.kiwi.client.select.ModePaiementSelectCtrl;
import org.cocktail.kiwi.client.select.PayeurSelectCtrl;
import org.cocktail.kiwi.client.select.PlanComptableSelectCtrl;
import org.cocktail.kiwi.client.select.RembZoneSelectCtrl;
import org.cocktail.kiwi.client.select.TitreMissionSelectCtrl;
import org.cocktail.kiwi.client.select.TypeCreditSelectCtrl;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GestionPreferencesPerso extends EOModalDialogController{

	
	private static GestionPreferencesPerso sharedInstance;
	private ApplicationClient NSApp;
	private	EOEditingContext ec;
	
	public JTextField 	exercice;
	public JTextField 	codeTypeCredit, codeModePaiement, codeDestin, codePlanco, codeCanal, titreMission;
	public JTextField 	libelleTypeCredit, libellePayeur, libelleModePaiement, libelleDestin;
	public	JTextField	libellePlanco, libelleCanal, libelleTrajet;
	public JButton 		btnGetTypeCredit, btnGetPayeur, btnGetModePaiement, btnGetDestin, btnGetPlanco, btnGetCanal, btnGetTrajet, btnGetTitre;
	public JButton 		btnDelTypeCredit, btnDelPayeur, btnDelModePaiement, btnDelDestin, btnDelPlanco, btnDelCanal, btnDelTrajet;
	public	JButton		btnValider, btnAnnuler;
	public	JRadioButton temDemarrageMission, temDemarrageRecherche;
	public JCheckBox 	temUpdateDatesTrajet;
		
	private EOExercice currentExercice;
	private	EOUtilisateur currentUtilisateur;
	
	private EOMissionPreferencesPerso currentPreference;
	
	private EOCodeAnalytique 			currentCanal;
	private EOModePaiement 				currentModePaiement;
	private EOLolfNomenclatureDepense 	currentDestin;
	private EOPlanComptable 			currentPlanco;
	private EOPayeur 					currentPayeur;
	private EOTypeCredit 				currentTypeCredit;
	private	EORembZone					currentTrajet;
	private	EOTitreMission				currentTitre;
	
	/** 
	 * Constructeur 
	 */
	public GestionPreferencesPerso(EOEditingContext editingContext)	{
		super();
		EOArchive.loadArchiveNamed("GestionPreferencesPerso", this,"kiwi.client", this.disposableRegistry());
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;
		initObject();
	}	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static GestionPreferencesPerso sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new GestionPreferencesPerso(editingContext);
		return sharedInstance;
	}
	
	/** 
	 * Initialisation du module. Mise en place des objets graphiques, chargement des nibs, des delegues et des notifications.
	 */
	public void initObject()	{
		
		currentExercice = NSApp.getExerciceCourant();
		currentUtilisateur = NSApp.getUtilisateur();
		
		CocktailUtilities.initTextField(exercice, false, false);

		CocktailUtilities.initTextField(codePlanco, false, false);
		CocktailUtilities.initTextField(codeDestin, false, false);
		CocktailUtilities.initTextField(codeCanal, false, false);
		CocktailUtilities.initTextField(codeTypeCredit, false, false);
		CocktailUtilities.initTextField(codeModePaiement, false, false);

		CocktailUtilities.initTextField(titreMission, false, false);
		CocktailUtilities.initTextField(libellePayeur, false, false);
		CocktailUtilities.initTextField(libellePlanco, false, false);
		CocktailUtilities.initTextField(libelleDestin, false, false);
		CocktailUtilities.initTextField(libelleCanal, false, false);
		CocktailUtilities.initTextField(libelleTypeCredit, false, false);
		CocktailUtilities.initTextField(libelleModePaiement, false, false);
		CocktailUtilities.initTextField(libelleTrajet, false, false);

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_VALID, "Valider", btnValider,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, "Annuler", btnAnnuler,"");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetTitre,"");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetPlanco,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, null, btnDelPlanco,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetTypeCredit,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, null, btnDelTypeCredit,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetPayeur,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, null, btnDelPayeur,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetModePaiement,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, null, btnDelModePaiement,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetCanal,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, null, btnDelCanal,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetDestin,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, null, btnDelDestin,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetTrajet,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, null, btnDelTrajet,"");
		
		temDemarrageMission.setSelected(true);
		
		temDemarrageMission.setEnabled(false);
		temDemarrageRecherche.setEnabled(false);
	}

	/**
	 * 
	 *
	 */
	public void open()	{
		
		NSMutableArray args = new NSMutableArray();
		args.addObject(currentUtilisateur);
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateur = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(EOMissionPreferencesPerso.ENTITY_NAME, myQualifier, null);
		
		NSArray preferences = ec.objectsWithFetchSpecification(fs);
		
		// S'il n'y a pas de preferences, on en ajoute une par defaut
		if (preferences.count() == 0)	{
			currentPreference = (EOMissionPreferencesPerso)Factory.instanceForEntity(ec, "MissionPreferencesPerso");
			currentPreference.setUtilisateurRelationship(currentUtilisateur);
			currentPreference.setTypeLancement("MISSION");
			currentPreference.setUpdateDatesTrajet("N");
			ec.insertObject(currentPreference);
		}
		else	{
			currentPreference = (EOMissionPreferencesPerso)preferences.objectAtIndex(0);
		}
		
		updateData();
		
		activateWindow();
	}
	
	/**
	 * 
	 *
	 */
	public void updateData()	{
		
		exercice.setText(currentExercice.exeExercice().toString());
		
		String temDemarrage = currentPreference.typeLancement();
		
		temUpdateDatesTrajet.setSelected("O".equals(currentPreference.updateDatesTrajet()));

		temDemarrageMission.setSelected("MISSION".equals(temDemarrage));
		temDemarrageRecherche.setSelected("RECHERCHE".equals(temDemarrage));
		
		currentTrajet = currentPreference.rembZone();
		currentTitre = currentPreference.titre();
		currentCanal = currentPreference.codeAnalytique();
		currentTypeCredit = currentPreference.toTypeCredit();
		currentPayeur = currentPreference.payeur();
		currentModePaiement = currentPreference.modePaiement();
		currentDestin = currentPreference.typeAction();
		currentPlanco = currentPreference.planComptable();
		
		if (currentTitre != null)
			titreMission.setText(currentTitre.titLibelle());
		
		if (currentTrajet != null)	{
			libelleTrajet.setText(currentTrajet.remLibelle());
		}
		
		if( currentPlanco != null)	{
			codePlanco.setText(currentPlanco.pcoNum());
			CocktailUtilities.setTextToField(libellePlanco, currentPlanco.pcoLibelle());
		}

		if( currentDestin != null)	{
			codeDestin.setText(currentDestin.lolfCode());
			CocktailUtilities.setTextToField(libelleDestin, currentDestin.lolfLibelle());
		}

		if( currentTypeCredit != null)	{
			codeTypeCredit.setText(currentTypeCredit.tcdCode());
			CocktailUtilities.setTextToField(libelleTypeCredit, currentTypeCredit.tcdLibelle() + " (" + currentTypeCredit.exercice().exeExercice() + ")");
		}

		if( currentCanal != null)	{
			codeCanal.setText(currentCanal.canCode());
			CocktailUtilities.setTextToField(libelleCanal, currentCanal.canLibelle());
		}

		if( currentPayeur != null)	{
			CocktailUtilities.setTextToField(libellePayeur, currentPayeur.libellePayeurLong());
		}

		if( currentModePaiement!= null)	{
			codeModePaiement.setText(currentModePaiement.modCode());
			CocktailUtilities.setTextToField(libelleModePaiement, currentModePaiement.modLibelle()+ " (" + currentModePaiement.toExercice().exeExercice()+")");
		}
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void getTrajet(Object sender)	  {

		EORembZone object = RembZoneSelectCtrl.sharedInstance(ec).getZoneTrajet();
		
		if (object != null)	{
			currentPreference.setRembZoneRelationship(object);
			currentPreference.setRemTaux(object.remTaux());
			CocktailUtilities.setTextToField(libelleTrajet, object.remLibelle());
		}	
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delTrajet(Object sender)	{
		currentTrajet = null;
		libelleTrajet.setText("");
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void getTitreMission(Object sender)	{
		EOTitreMission object = TitreMissionSelectCtrl.sharedInstance(ec).getTitreMission();
		
		if (object != null)	{
			currentPreference.setTitreRelationship(object);
			CocktailUtilities.setTextToField(titreMission, object.titLibelle());
		}
	}
	
	/** 
	 * Choix d'un compte d'imputation a partir d'une liste de valeurs 
	 */
	public void getDestin(Object sender)	{
		
		EOLolfNomenclatureDepense typeAction = LolfSelectCtrl.sharedInstance(ec).getTypeAction(NSApp.getExerciceCourant());

		if (typeAction != null)	{
			
			currentPreference.setTypeActionRelationship(typeAction);
			codeDestin.setText(typeAction.lolfCode());
			CocktailUtilities.setTextToField(libelleDestin, typeAction.lolfLibelle());
		}		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delDestin(Object sender)	{
		
		currentPreference.removeObjectFromBothSidesOfRelationshipWithKey(currentPreference.typeAction(), "typeAction");
		codeDestin.setText("");
		libelleDestin.setText("");

	}

	/** 
	 * Choix d'un compte d'imputation a partir d'une liste de valeurs 
	 */
	public void getPayeur(Object sender)	{
		EOPayeur object = PayeurSelectCtrl.sharedInstance(ec).getPayeur();
		
		if (object != null)	{
			currentPreference.setPayeurRelationship(object);
			CocktailUtilities.setTextToField(libellePayeur, object.libellePayeurLong());
		}		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delPayeur(Object sender)	{
		currentPreference.setPayeurRelationship(null);
		libellePayeur.setText("");
	}
	
	/** 
	 * Choix d'un compte d'imputation a partir d'une liste de valeurs 
	 */
	public void getModePaiement(Object sender)	{
		
		NSArray modes = ModePaiementSelectCtrl.sharedInstance(ec).getModesPaiement(NSApp.getExerciceCourant(), true);
		
		if (modes != null && modes.count() > 0)	{
			
			EOModePaiement object = (EOModePaiement)modes.objectAtIndex(0);
			currentPreference.setModePaiementRelationship(object);
			codeModePaiement.setText(object.modCode());
			CocktailUtilities.setTextToField(libelleModePaiement, object.modLibelle()+ " (" + object.toExercice().exeExercice()+")");
		
		}		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delModePaiement(Object sender)	{
		currentPreference.removeObjectFromBothSidesOfRelationshipWithKey(currentPreference.modePaiement(), "modePaiement");
		codeModePaiement.setText("");
		libelleModePaiement.setText("");
	}
	
	/** 
	 * Choix d'un compte d'imputation a partir d'une liste de valeurs 
	 */
	public void getTypeCredit(Object sender)	{
		
		EOTypeCredit type = TypeCreditSelectCtrl.sharedInstance(ec).getTypeCredit(NSApp.getExerciceCourant(), true);

		if (type != null)	{						
			currentPreference.setToTypeCreditRelationship(type);
			codeTypeCredit.setText(type.tcdCode());
			CocktailUtilities.setTextToField(libelleTypeCredit, type.tcdLibelle()+ " (" + type.exercice().exeExercice() + ")");
		}		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delTypeCredit(Object sender)	{
		currentPreference.setToTypeCreditRelationship(null);
		codeTypeCredit.setText("");
		libelleTypeCredit.setText("");
	}
	
	/** 
	 * Choix d'un compte d'imputation a partir d'une liste de valeurs 
	 */
	public void getPlanco(Object sender)	{

		NSArray  plancos = PlanComptableSelectCtrl.sharedInstance(ec).getPlancos(new NSArray("6"), NSApp.getExerciceCourant(), false, true);
		
		if (plancos != null && plancos.count() > 0)	{
			
			EOPlanComptable object = (EOPlanComptable)plancos.objectAtIndex(0);
			
			currentPreference.setPlanComptableRelationship(object);
			codePlanco.setText(object.pcoNum());
			CocktailUtilities.setTextToField(libellePlanco, object.pcoLibelle());
			
		}		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delPlanco(Object sender)	{
		currentPreference.removeObjectFromBothSidesOfRelationshipWithKey(currentPreference.planComptable(), "planComptable");
		codePlanco.setText("");
		libellePlanco.setText("");
	}

	/** 
	 * Choix d'un code analytique a partir d'une liste de valeurs 
	 */
	public void getCanal(Object sender)	{

		EOCodeAnalytique canal = CodeAnalytiqueSelectCtrl.sharedInstance(ec).getCodeAnalytique(null, NSApp.getExerciceCourant());
		
		if (canal != null)	{
			currentPreference.setCodeAnalytiqueRelationship(canal);
			codeCanal.setText(canal.canCode());
			CocktailUtilities.setTextToField(libelleCanal, canal.canLibelle());
		}		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delCanal(Object sender)	{
		currentPreference.removeObjectFromBothSidesOfRelationshipWithKey(currentPreference.codeAnalytique(), "codeAnalytique");
		codeCanal.setText("");
		libelleCanal.setText("");
	}
	
	/**
	 * 
	 * @return
	 */
	public EOMissionPreferencesPerso getPreferences()	{
		
		NSMutableArray args = new NSMutableArray();
		args.addObject(currentUtilisateur);
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateur = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(EOMissionPreferencesPerso.ENTITY_NAME, myQualifier, null);
		
		NSArray preferences = ec.objectsWithFetchSpecification(fs);
		
		if (preferences.count() == 0)
			return null;

		return (EOMissionPreferencesPerso)preferences.objectAtIndex(0);
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void valider(Object sender)	{
		
		try {
			
			if (temUpdateDatesTrajet.isSelected())
				currentPreference.setUpdateDatesTrajet("O");
			else
				currentPreference.setUpdateDatesTrajet("N");

			if (temDemarrageMission.isSelected())
				currentPreference.setTypeLancement("MISSION");
			else	
				currentPreference.setTypeLancement("RECHERCHE");				
			
			ec.saveChanges();
			closeWindow();
		}
		catch (Exception e)	 {
			ec.revert();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur de sauvegarde des preferences personnelles !");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void annuler(Object sender)	{
		ec.revert();
		closeWindow();
	}
	
}
