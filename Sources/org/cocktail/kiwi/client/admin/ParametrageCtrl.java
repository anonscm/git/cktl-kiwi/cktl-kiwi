
package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.editions.ReportsCtrl;
import org.cocktail.kiwi.client.factory.FactoryMissionParametres;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.nibctrl.ParametrageView;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


public class ParametrageCtrl extends ParametrageView {

	public static ParametrageCtrl sharedInstance;

	private	ApplicationClient	NSApp;	
	private EOEditingContext 	ec;
	private EOExercice 			currentExercice;

	CheckBoxCapListener listenerCap = new CheckBoxCapListener();

	/**
	 *
	 */
	public ParametrageCtrl(EOEditingContext globalEc) {

		super(new JFrame(), true);

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = globalEc;

		checkProrata.addActionListener(new CheckBoxProrataListener());
		checkCapClassiques.addActionListener(listenerCap);
		checkCapExtourne.addActionListener(listenerCap);
		checkCapSans.addActionListener(listenerCap);

		NSArray<EOModePaiement> modes = EOModePaiement.findModesPaiements(ec, NSApp.getExerciceCourant());
		setModesAvance(modes);
		NSArray<EOModePaiement> modesDpao = EOModePaiement.findModesPaiementRegulOP(ec, NSApp.getExerciceCourant());
		setModesAvanceDPAO(modesDpao);

		setQuotientRemboursement();

		setSaisieEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		getButtonCancel().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		getButtonValidate().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		setExercices(NSApp.listeExercices());
		getPopupExercices().setSelectedItem(NSApp.getExerciceCourant());
		getPopupExercices().addActionListener(new PopupExercicesListener());

		getCheckDateCreationMission().setEnabled(false);
		getCheckSaisieMissionDate().setEnabled(false);
		getCheckClotureAutomatique().setEnabled(false);
						
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */    
	public static ParametrageCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new ParametrageCtrl(editingContext);
		return sharedInstance;
	}

	public EOExercice getCurrentExercice() {
		return (EOExercice)getPopupExercices().getSelectedItem();
	}

	public void open() {
		updateData();
		setVisible(true);
	}
    private class PopupExercicesListener implements ActionListener	{
        public PopupExercicesListener() {super();}
        public void actionPerformed(ActionEvent anAction) {
        	updateData();
        }
    }


	/**
	 * Mise a jour de la liste des modes de paiement en fonction de la gestion des charges a payer
	 */
	private void majModesPaiement() {

		NSArray<EOModePaiement> modesCap = new NSArray<EOModePaiement>();

		if (checkCapClassiques.isSelected()) {
			modesCap = EOModePaiement.findModesPaiementCapClassique(ec, NSApp.getExerciceCourant());
		}
		else
			if (checkCapExtourne.isSelected()) {
				modesCap = EOModePaiement.findModesPaiementCapExtourne(ec, NSApp.getExerciceCourant());
			}

		
		setModesCap(modesCap);

	}

	/**
	 * 
	 *
	 */
	public void updateData() {

		String paramProrata = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PRORATA);
		if (paramProrata == null)
			paramProrata = EOMissionParametres.DEFAULT_VALUE_CHECK_PRORATA;

		checkProrata.setSelected(paramProrata != null && "O".equals(paramProrata));        

		numerateurRemb.setEnabled(checkProrata.isSelected());
		denominateurRemb.setEnabled(checkProrata.isSelected());

		String paramTelechargementAuto = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_TELECHARGEMENT_AUTO);
		if (paramTelechargementAuto == null)
			paramTelechargementAuto = EOMissionParametres.DEFAULT_VALUE_TELECHARGEMENT_AUTO;

		checkTelechargement.setSelected(paramTelechargementAuto != null && "O".equals(paramTelechargementAuto));        

		String paramClotureAuto = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CLOTURE_AUTO);
		if (paramClotureAuto == null)
			paramClotureAuto = EOMissionParametres.DEFAULT_VALUE_CLOTURE_AUTO;

		checkClotureAutomatique.setSelected(paramClotureAuto != null && "O".equals(paramClotureAuto));        


		String paramForfait = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_FORFAIT);
		if (paramForfait == null)
			paramForfait = EOMissionParametres.DEFAULT_VALUE_CHECK_FORFAIT;

		temCheckForfait.setSelected(paramForfait != null && "O".equals(paramForfait));        


		String paramMultiLbud = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_MULTI_LBUD);
		if (paramMultiLbud == null)
			paramMultiLbud = EOMissionParametres.DEFAULT_VALUE_CHECK_MULTI_LBUD;

		checkMultiLbud.setSelected(paramMultiLbud != null && "O".equals(paramMultiLbud));        

		String paramNbNuits = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_NB_NUITS);
		if (paramNbNuits == null)
			paramNbNuits = EOMissionParametres.DEFAULT_VALUE_CHECK_NB_NUITS;

		checkNombreNuits.setSelected(paramNbNuits != null && "O".equals(paramNbNuits));        

		String paramRepas = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_NB_REPAS);
		if (paramRepas == null)
			paramRepas = EOMissionParametres.DEFAULT_VALUE_CHECK_NB_REPAS;

		checkNombreRepas.setSelected(paramRepas != null && "O".equals(paramRepas));        

		String paramCheckAvances = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_AVANCES);
		if (paramCheckAvances == null)
			paramCheckAvances = EOMissionParametres.DEFAULT_VALUE_CHECK_AVANCES;

		getCheckCtrlAvances().setSelected(paramCheckAvances != null && "O".equals(paramCheckAvances));        

		String paramMontantRepas = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_MONTANT_REPAS);
		if (paramMontantRepas == null)
			paramMontantRepas = EOMissionParametres.DEFAULT_VALUE_CHECK_MONTANT_REPAS;

		checkMontantRepas.setSelected(paramMontantRepas != null && "O".equals(paramMontantRepas));        

		String paramIndemnites = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_UPDATE_MONTANT_INDEMNITES);
		if (paramIndemnites == null)
			paramIndemnites = EOMissionParametres.DEFAULT_VALUE_UPDATE_MONTANT_INDEMNITES;

		checkMontantIndemnites.setSelected(paramIndemnites == null || "O".equals(paramIndemnites));        

		// OrigineSignataires
		String paramOrigineSignataires = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_ORIGINE_SIGNATAIRES);
		if (paramOrigineSignataires == null)
			paramOrigineSignataires = EOMissionParametres.DEFAULT_VALUE_ORIGINE_SIGNATAIRES;

		if ("JEFY_ADMIN".equals(paramOrigineSignataires))
			temSignatairesJefyAdmin.setSelected(true);        
		else
			temSignatairesKiwi.setSelected(true);        

		// Signataires OM
		String paramSignatairesOm = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_NB_SIGNATAIRES_OM);
		if (paramSignatairesOm == null)
			paramSignatairesOm = EOMissionParametres.DEFAULT_VALUE_NB_SIGNATAIRES_OM;

		if ("0".equals(paramSignatairesOm))
			temSansSignataireOm.setSelected(true);        
		else
			if ("1".equals(paramSignatairesOm))
				temSignataireUniqueOm.setSelected(true);        
			else
				temTousSignatairesOm.setSelected(true);        

		// TYPE Signataires OM
		String typeSignatairesOm = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_TYPE_SIGNATAIRES_OM);
		if (typeSignatairesOm == null)
			typeSignatairesOm = EOMissionParametres.DEFAULT_VALUE_TYPE_SIGNATAIRES_OM;

		if ("FINANCIER".equals(typeSignatairesOm))
			temRespFinancier.setSelected(true);        
		else
			temRespService.setSelected(true);        

		//  Ordonnateur
		String paramOrdonnateur = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_ORDONNATEUR);
		if (paramOrdonnateur == null)
			paramOrdonnateur = EOMissionParametres.DEFAULT_VALUE_ORDONNATEUR;

		if ("UB".equals(paramOrdonnateur))
			temOrdonnateurUb.setSelected(true);        
		else
			temOrdonnateurEtab.setSelected(true);        


		// Signataires ETAT FRAIS
		String paramSignatairesEtatFrais = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_NB_SIGNATAIRES_ETAT_FRAIS);
		if (paramSignatairesEtatFrais == null)
			paramSignatairesEtatFrais = EOMissionParametres.DEFAULT_VALUE_NB_SIGNATAIRES_ETAT_FRAIS;

		if ("0".equals(paramSignatairesEtatFrais))
			temSansSignatairesEtatFrais.setSelected(true);        
		else
			if ("1".equals(paramSignatairesEtatFrais))
				temSignataireUniqueEtatFrais.setSelected(true);        
			else
				temTousSignatairesEtatFrais.setSelected(true);        



		String paramTraitementAvances = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_TRAITEMENTS_AVANCES);
		if (paramTraitementAvances == null)
			paramTraitementAvances = EOMissionParametres.DEFAULT_VALUE_TRAITEMENT_AVANCES;

		if( paramTraitementAvances.equals("DEP"))
			temAvanceDepense.setSelected(true);
		else
			if( paramTraitementAvances.equals("OP"))
				temAvanceOp.setSelected(true);

		String paramPlafondNuits = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PLAFOND_NUITS);
		if (paramPlafondNuits == null)
			paramPlafondNuits = EOMissionParametres.DEFAULT_VALUE_CHECK_PLAFOND_NUITS;

		checkPlafondNuits.setSelected(paramPlafondNuits != null && "N".equals(paramPlafondNuits));        

		String paramPlafondRepas = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PLAFOND_REPAS);
		if (paramPlafondRepas == null)
			paramPlafondRepas = EOMissionParametres.DEFAULT_VALUE_CHECK_PLAFOND_REPAS;

		checkPlafondRepas.setSelected(paramPlafondRepas != null && "N".equals(paramPlafondRepas));        

		numerateurRemb.setSelectedItem(EOMissionParametres.getNumerateurRemb(ec, getCurrentExercice()));

		denominateurRemb.setSelectedItem(EOMissionParametres.getDenominateurRemb(ec, getCurrentExercice()));



		String paramRestrictionLbuds = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_RESTRICTION_LBUDS);
		if (paramRestrictionLbuds == null)
			paramRestrictionLbuds = EOMissionParametres.DEFAULT_VALUE_CHECK_RESTRICTION_LBUDS;

		temRestrictionLbuds.setSelected(paramPlafondNuits != null && "O".equals(paramRestrictionLbuds));        



		String paramConventions = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_ALL_CONVENTIONS);
		if (paramConventions == null)
			paramConventions = EOMissionParametres.DEFAULT_VALUE_ALL_CONVENTIONS;

		temAllConventions.setSelected(paramConventions != null && "O".equals(paramConventions));        

		String paramSaisieMotifs = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_SAISIE_MOTIFS);
		if (paramSaisieMotifs == null)
			paramSaisieMotifs = EOMissionParametres.DEFAULT_VALUE_SAISIE_MOTIFS;

		temCheckSaisieMotifs.setSelected(paramSaisieMotifs != null && "O".equals(paramSaisieMotifs));        

		String paramModeAvance = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_AVANCE);
		if (paramModeAvance == null)
			paramModeAvance = EOMissionParametres.DEFAULT_VALUE_MODE_AVANCE;

		EOModePaiement modeAvance = EOModePaiement.findModePaiementForCode(ec, paramModeAvance, NSApp.getExerciceCourant());
		modesAvance.setSelectedItem(modeAvance);        

		String paramModeAvanceDpao = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_DPAO);
		if (paramModeAvanceDpao == null)
			paramModeAvanceDpao = EOMissionParametres.DEFAULT_VALUE_MODE_DPAO;

		EOModePaiement modeAvanceDpao = EOModePaiement.findModePaiementForCode(ec, paramModeAvanceDpao, NSApp.getExerciceCourant());
		modesAvanceDPAO.setSelectedItem(modeAvanceDpao);        


		// CHARGES A PAYER

		String paramGestionCap = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_GESTION_CAP);
		if (paramGestionCap == null)
			paramGestionCap = EOMissionParametres.DEFAULT_VALUE_GESTION_CAP;
		checkCapSans.setSelected(paramGestionCap.equals(EOMissionParametres.CODE_SANS_CAP));
		checkCapClassiques.setSelected(paramGestionCap.equals(EOMissionParametres.CODE_CAP_CLASSIQUE));
		checkCapExtourne.setSelected(paramGestionCap.equals(EOMissionParametres.CODE_CAP_EXTOURNE));

		majModesPaiement();
		
		String paramModeCap = FinderMissionParametres.getValue(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_CAP);
		if (paramModeCap == null)
			paramModeCap = EOMissionParametres.DEFAULT_VALUE_MODE_CAP;

		EOModePaiement modeCap = EOModePaiement.findModePaiementForCode(ec, paramModeCap, NSApp.getExerciceCourant());
		if (modeCap != null)
			modesChargesAPayer.setSelectedItem(modeCap);        

		//        String paramColorMisAnnulees = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_COLOR_MIS_ANNULEES);
		//        if (paramColorMisAnnulees == null || paramColorMisAnnulees.length() == 0)
		//        	paramColorMisAnnulees = EOMissionParametres.DEFAULT_VALUE_COLOR_MIS_ANNULEES;
		//        NSArray colors = NSArray.componentsSeparatedByString(paramColorMisAnnulees, ";");
		//        Integer col1 = new Integer(colors.objectAtIndex(0).toString());
		//        Integer col2 = new Integer(colors.objectAtIndex(1).toString());
		//        Integer col3 = new Integer(colors.objectAtIndex(2).toString());
		//        colorMissionsAnnulees.setColor(new Color(col1.intValue(), col2.intValue(), col3.intValue()));

	}


	private void valider() {

		EOMissionParametres currentParametre = null;

		try {

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_TELECHARGEMENT_AUTO);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_TELECHARGEMENT_AUTO, "Telechargement automatique à chaque lancement serveur? (O / N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkTelechargement.isSelected())?"O":"N");


			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_SAISIE_MOTIFS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_SAISIE_MOTIFS, "Autoriser la saisie des motifs trajet ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((temCheckSaisieMotifs.isSelected())?"O":"N");

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_AVANCES);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_AVANCES, "Bloquer la saisie de l'avance à 75% de la mission ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((getCheckCtrlAvances().isSelected())?"O":"N");


			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_FORFAIT);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_FORFAIT, " Prise en compte du Forfait si < Montant réel des remboursements ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((temCheckForfait.isSelected())?"O":"N");

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_MULTI_LBUD);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_MULTI_LBUD, "Activer la gestion des multi-lignes budgetaires ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkMultiLbud.isSelected())?"O":"N");

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_NB_NUITS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_NB_NUITS, "Autoriser la modification du nombre de nuits ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkNombreNuits.isSelected())?"O":"N");

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_UPDATE_MONTANT_INDEMNITES);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_UPDATE_MONTANT_INDEMNITES, "Autoriser la modification du montant des indemnites ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkMontantIndemnites.isSelected())?"O":"N");


			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_NB_REPAS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_NB_REPAS, "Autoriser la modification du nombre de repas ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkNombreRepas.isSelected())?"O":"N");


			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_MONTANT_REPAS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_MONTANT_REPAS, "Autoriser la modification du montant des repas ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkMontantRepas.isSelected())?"O":"N");



			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_RESTRICTION_LBUDS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_RESTRICTION_LBUDS, "Restreindre la recherche des missions aux lignes budgetaires de l'utilisateur");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((temRestrictionLbuds.isSelected())?"O":"N");


			// ORIGINE SIGNATAIRES
			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_ORIGINE_SIGNATAIRES);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_ORIGINE_SIGNATAIRES, "Base de gestion des signataires (KIWI / JEFY_ADMIN) ?");
				ec.insertObject(currentParametre);
			}

			if (temSignatairesJefyAdmin.isSelected())
				currentParametre.setParamValue("JEFY_ADMIN");
			else
				currentParametre.setParamValue("KIWI");

			ReportsCtrl.sharedInstance(ec).setOrigineSignataires(currentParametre.paramValue());

			// TYPE SIGNATAIRES OM
			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_TYPE_SIGNATAIRES_OM);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_TYPE_SIGNATAIRES_OM, "Type Signataire OM - Responsable Financier ou Administratif");
				ec.insertObject(currentParametre);
			}

			if (temRespFinancier.isSelected())
				currentParametre.setParamValue("FINANCIER");
			else
				currentParametre.setParamValue("ADMINISTRATIF");

			// SIGNATAIRES OM
			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_NB_SIGNATAIRES_OM);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_NB_SIGNATAIRES_OM, "Nombre de signataires a afficher sur l'ordre de mission ? (0, 1 ou N)");
				ec.insertObject(currentParametre);
			}

			if (temSansSignataireOm.isSelected())
				currentParametre.setParamValue("0");
			else
				if (temSignataireUniqueOm.isSelected())
					currentParametre.setParamValue("1");
				else
					currentParametre.setParamValue("N");

			ReportsCtrl.sharedInstance(ec).setNbSignatairesOm(currentParametre.paramValue());

			// ORDONNATEUR
			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_ORDONNATEUR);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_ORDONNATEUR, "Ordonnateur. Niveau UB ou ETAB ?");
				ec.insertObject(currentParametre);
			}

			if (temOrdonnateurUb.isSelected())
				currentParametre.setParamValue("UB");
			else
				currentParametre.setParamValue("ETAB");

			ReportsCtrl.sharedInstance(ec).setOrdonnateur(currentParametre.paramValue());

			// SIGNATAIRES ETAT FRAIS
			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_NB_SIGNATAIRES_ETAT_FRAIS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_NB_SIGNATAIRES_ETAT_FRAIS, "Nombre de signataires a afficher sur l'etat de frais ? (0, 1 ou N)");
				ec.insertObject(currentParametre);
			}

			if (temSansSignatairesEtatFrais.isSelected())
				currentParametre.setParamValue("0");
			else
				if (temSignataireUniqueEtatFrais.isSelected())
					currentParametre.setParamValue("1");
				else
					currentParametre.setParamValue("N");


			ReportsCtrl.sharedInstance(ec).setNbSignatairesEtatFrais(currentParametre.paramValue());


			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_TRAITEMENTS_AVANCES);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_TRAITEMENTS_AVANCES, " Mode de gestion des avances ? (DEP / OP)");
				ec.insertObject(currentParametre);
			}

			if (temAvanceDepense.isSelected())	
				currentParametre.setParamValue("DEP");
			else
				if (temAvanceOp.isSelected())	
					currentParametre.setParamValue("OP");	        		

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PLAFOND_NUITS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PLAFOND_NUITS, "Bloquer le remboursement des nuitees au seuil de 60 euros ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkPlafondNuits.isSelected())?"N":"O");

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PLAFOND_REPAS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PLAFOND_REPAS, "Bloquer le remboursement des repas au seuil de 15.25 euros ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkPlafondRepas.isSelected())?"N":"O");


			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PRORATA);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_CHECK_PRORATA, "Les paiements au taux 5/3 sont ils autorises ? (O/N)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((checkProrata.isSelected())?"O":"N");

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_NUMERATEUR_REMB);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_NUMERATEUR_REMB, "Numerateur du quotient pour le remboursement des missions pour les personnalites exterieures");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue(numerateurRemb.getSelectedItem().toString());

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_DENOMINATEUR_REMB);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_DENOMINATEUR_REMB, "Denominteur du quotient pour le remboursement des missions pour les personnalites exterieures");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue(denominateurRemb.getSelectedItem().toString());



			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_ALL_CONVENTIONS);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_ALL_CONVENTIONS, "Proposer la liste de toutes les conventions (Limitatives ET Non Limitatives)");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue((temAllConventions.isSelected())?"O":"N");

			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_AVANCE);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_AVANCE, "Mode de paiement pour la liquidation des avances");
				ec.insertObject(currentParametre);
			}
			currentParametre.setParamValue(((EOModePaiement)modesAvance.getSelectedItem()).modCode());

			try {
				currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_DPAO);
				if (currentParametre == null) {
					currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_DPAO, "Mode de paiement pour la regul des avances");
					ec.insertObject(currentParametre);
				}

				currentParametre.setParamValue(((EOModePaiement)modesAvanceDPAO.getSelectedItem()).modCode());
			}
			catch (Exception e) {				
			}


			// CHARGES A PAYER

			// Mode de gestion SANS CLASSIQUE OU EXTOURNE
			currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_GESTION_CAP);
			if (currentParametre == null) {
				currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_GESTION_CAP, "Gestion des charges à payer (0 : SANS, 1 : CLASSIQUES , 2 : EXTOURNE)");
				ec.insertObject(currentParametre);
			}
			if (checkCapSans.isSelected()) 
				currentParametre.setParamValue(EOMissionParametres.CODE_SANS_CAP);
			else {	
				if (checkCapClassiques.isSelected())
					currentParametre.setParamValue(EOMissionParametres.CODE_CAP_CLASSIQUE);
				else
					currentParametre.setParamValue(EOMissionParametres.CODE_CAP_EXTOURNE);

				if (modesChargesAPayer.getItemCount() > 0) {

					currentParametre = FinderMissionParametres.findParametre(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_CAP);
					if (currentParametre == null) {
						currentParametre = FactoryMissionParametres.creerMissionParametre(ec, getCurrentExercice(), EOMissionParametres.ID_MODE_CAP, "Mode de paiement pour les charges à payer");
						ec.insertObject(currentParametre);
					}

					currentParametre.setParamValue(((EOModePaiement)modesChargesAPayer.getSelectedItem()).modCode());
					
				}				
			}



			//	        	currentParametre = FinderMissionParametres.findParametre(ec, EOMissionParametres.ID_COLOR_MIS_ANNULEES);
			//	        	if (currentParametre == null) {
			//	        		currentParametre = FactoryMissionParametres.creerMissionParametre(ec, EOMissionParametres.ID_COLOR_MIS_ANNULEES, "Couleur pour les missions annulees");
			//	        		ec.insertObject(currentParametre);
			//	        	}
			//	        	Color choosenColor = colorMissionsAnnulees.getColor();
			//	        	currentParametre.setParamValue(choosenColor.getRed()+";"+choosenColor.getGreen()+";"+choosenColor.getBlue());
			
			ec.saveChanges();
			dispose();

			EODialogs.runInformationDialog("OK","Les paramètres ont bien été sauvegardés.");

		}
		catch (Exception ex) {	
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement des donnees !\nMESSAGE : " + ex.getMessage());
			return;
		}
	} 


	private void annuler() {

		dispose();

	}  

	private void setSaisieEnabled(boolean yn) {

		getButtonValidate().setEnabled(yn);
		checkMultiLbud.setEnabled(yn);
		checkProrata.setEnabled(yn);
		checkNombreNuits.setEnabled(yn);
		checkNombreRepas.setEnabled(yn);
		checkMontantIndemnites.setEnabled(yn);
		checkNombreRepas.setEnabled(yn);
		checkMontantRepas.setEnabled(yn);
		checkPlafondNuits.setEnabled(yn);
		checkPlafondRepas.setEnabled(yn);
		temAllConventions.setEnabled(yn);

		temSignatairesJefyAdmin.setEnabled(yn);
		temSignatairesKiwi.setEnabled(yn);

		temSignataireUniqueOm.setEnabled(yn);
		temSansSignataireOm.setEnabled(yn);
		temTousSignatairesOm.setEnabled(yn);

		temSignataireUniqueEtatFrais.setEnabled(yn);
		temSansSignatairesEtatFrais.setEnabled(yn);
		temTousSignatairesEtatFrais.setEnabled(yn);

		temOrdonnateurEtab.setEnabled(yn);
		temOrdonnateurUb.setEnabled(yn);

		numerateurRemb.setEnabled(yn);
		denominateurRemb.setEnabled(yn);

		modesAvance.setEnabled(yn);
		modesAvanceDPAO.setEnabled(yn);

		temRestrictionLbuds.setEnabled(yn);


	}


	/** 
	 * Listener sur la coche temPayeLocale. Permet de actif le bouton permettant la saisie des infos paye 
	 */
	private class CheckBoxCapListener extends AbstractAction	{
		public CheckBoxCapListener ()	{
			super();
		}

		public void actionPerformed(ActionEvent anAction)	{
			majModesPaiement();
		}
	}


	/** 
	 * Listener sur la coche temPayeLocale. Permet de actif le bouton permettant la saisie des infos paye 
	 */
	private class CheckBoxProrataListener extends AbstractAction	{
		public CheckBoxProrataListener ()	{
			super();
		}

		public void actionPerformed(ActionEvent anAction)	{

			numerateurRemb.setEnabled(checkProrata.isSelected());
			denominateurRemb.setEnabled(checkProrata.isSelected());

		}
	}

}
