package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.Factory;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOParamCodeExer;
import org.cocktail.kiwi.client.metier.EOParamDestination;
import org.cocktail.kiwi.client.metier.EOParamImputation;
import org.cocktail.kiwi.client.metier.EOParamModePaiement;
import org.cocktail.kiwi.client.metier.EOParamTypeCredit;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.nibctrl.ParametrageBudgetaireView;
import org.cocktail.kiwi.client.select.CodeMarcheSelectCtrl;
import org.cocktail.kiwi.client.select.LolfSelectCtrl;
import org.cocktail.kiwi.client.select.ModePaiementSelectCtrl;
import org.cocktail.kiwi.client.select.PlanComptableSelectCtrl;
import org.cocktail.kiwi.client.select.TypeCreditSelectCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class ParametrageBudgetaireCtrl {

	private static ParametrageBudgetaireCtrl sharedInstance;

	private ApplicationClient NSApp;
	private EOEditingContext ec;

	private ParametrageBudgetaireView myView;

	private EODisplayGroup eodLolf, eodImputations, eodModesPaiement, eodTypesCredit, eodCodesMarche;

	public ParametrageBudgetaireCtrl(EOEditingContext edc)	{
		
		super();

		this.ec = edc;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		eodLolf = new EODisplayGroup();
		eodImputations = new EODisplayGroup();
		eodModesPaiement = new EODisplayGroup();
		eodTypesCredit = new EODisplayGroup();
		eodCodesMarche = new EODisplayGroup();

		myView= new ParametrageBudgetaireView(new JFrame(), true, eodLolf, eodImputations, eodModesPaiement, eodTypesCredit, eodCodesMarche);
		
		myView.getBtnAddLolf().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDestination();
			}
		});

		myView.getBtnDelLolf().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delDestination();
			}
		});

		myView.getBtnAddCompte().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getImputation();
			}
		});

		myView.getBtnDelCompte().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delImputation();
			}
		});

		myView.getBtnAddModePaiement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getModePaiement();
			}
		});

		myView.getBtnDeModePaiement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delModePaiement();
			}
		});

		myView.getBtnAddTypeCredit().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getTypeCredit();
			}
		});

		myView.getBtnDelTypeCredit().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delTypeCredit();
			}
		});
		myView.getBtnAddCodeExer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getCodeMarche();
			}
		});

		myView.getBtnDelCodeExer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delCodeMarche();
			}
		});

		myView.getBtnAddLolf().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnDelLolf().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnAddCompte().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnDelCompte().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnAddModePaiement().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnDeModePaiement().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnAddTypeCredit().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnDelTypeCredit().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnAddCodeExer().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));
		myView.getBtnDelCodeExer().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_BUD));

		myView.initExercices(NSApp.listeExercices());
		myView.getPopupExercice().setSelectedItem(NSApp.getExerciceCourant());
		myView.getPopupExercice().addActionListener(new PopupExercicesListener());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ParametrageBudgetaireCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ParametrageBudgetaireCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 *
	 */
	public void open()	{

		updateData();
		
		myView.setVisible(true);
	}
    private class PopupExercicesListener implements ActionListener	{
        public PopupExercicesListener() {super();}
        public void actionPerformed(ActionEvent anAction) {
        	updateData();
        }
    }

	
/**
 * 
 */
	public void updateData()	{
		
		eodTypesCredit.setObjectArray(EOParamTypeCredit.findParamsTypeCredit(ec, getSelectedExercice()));
		myView.getMyEOTableTypesCredit().updateData();

		eodImputations.setObjectArray(EOParamImputation.findParamImputations(ec, getSelectedExercice()));
		myView.getMyEOTableImputations().updateData();

		eodLolf.setObjectArray(EOParamDestination.findParamDestinations(ec, getSelectedExercice()));
		myView.getMyEOTableDestinations().updateData();

		eodModesPaiement.setObjectArray(EOParamModePaiement.findForExercice(ec, getSelectedExercice()));
		myView.getMyEOTableModesPaiement().updateData();

		eodCodesMarche.setObjectArray(EOParamCodeExer.findForExercice(ec, getSelectedExercice()));
		myView.getMyEOTableCodesMarche().updateData();

	}
	
	
	/** 
	 * Choix d'un type de credit parmi une liste de valeurs 
	 */
	public void getTypeCredit()	{
		
		EOTypeCredit type = TypeCreditSelectCtrl.sharedInstance(ec).getTypeCredit(getSelectedExercice(), false);
		
		if (type != null)	{
			
			try {
					EOParamTypeCredit newParam = (EOParamTypeCredit)Factory.instanceForEntity(ec, EOParamTypeCredit.ENTITY_NAME);
					
					newParam.setToExerciceRelationship(getSelectedExercice());
					newParam.setToTypeCreditRelationship(type);
					
					ec.insertObject(newParam);

					ec.saveChanges();
					updateData();
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delTypeCredit()	{

		for (int i=0;i<eodTypesCredit.selectedObjects().count();i++)	{
			EOParamTypeCredit currentObject = (EOParamTypeCredit)eodTypesCredit.selectedObjects().objectAtIndex(i);			
			ec.deleteObject(currentObject);
		}

		if (ec.deletedObjects().count() > 0)	{
			ec.saveChanges();
			eodTypesCredit.deleteSelection();
			myView.getMyEOTableTypesCredit().updateData();
		}
		
	}
	
	/** 
	 * Choix d'une destination a partir d'une liste de valeurs 
	 */
	public void getImputation()	{
		
		NSArray plancos = PlanComptableSelectCtrl.sharedInstance(ec).getPlancos(new NSArray("6"), getSelectedExercice(), true, false);
		
		if (plancos != null && plancos.count() > 0)	{

			try {
				for (int i=0;i<plancos.count();i++)	{
					
					EOPlanComptable planco = (EOPlanComptable)plancos.objectAtIndex(i);
					
					EOParamImputation existingParam = EOParamImputation.findParamForCode(ec, getSelectedExercice(), planco.pcoNum());

					if (existingParam == null)	{
						EOParamImputation newParam = (EOParamImputation)Factory.instanceForEntity(ec, EOParamImputation.ENTITY_NAME);
						
						newParam.setToExerciceRelationship(getSelectedExercice());
						newParam.setPlanComptableRelationship(planco);
						
						ec.insertObject(newParam);					
					}
				}

				if (ec.insertedObjects().count() > 0)	{
					ec.saveChanges();	
					updateData();
				}
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}
		}
	}
	
	public EOExercice getSelectedExercice() {
		return (EOExercice)myView.getPopupExercice().getSelectedItem();
	}
	
	public void getCodeMarche()	{
		
		NSArray<EOCodeExer> codes = CodeMarcheSelectCtrl.sharedInstance(ec).getCodesMarche(getSelectedExercice(), true, false);
		
		if (codes != null && codes.size() > 0)	{

			try {
				
				for (EOCodeExer myCode : codes) {
							
					EOParamCodeExer existingParam = EOParamCodeExer.findParamForCode(ec, getSelectedExercice(), myCode.codeMarche().cmCode());

					if (existingParam == null)	{
						EOParamCodeExer newParam = (EOParamCodeExer)Factory.instanceForEntity(ec, EOParamCodeExer.ENTITY_NAME);
						
						newParam.setToExerciceRelationship(getSelectedExercice());
						newParam.setToCodeExerRelationship(myCode);
						
						ec.insertObject(newParam);					
					}
				}

				if (ec.insertedObjects().count() > 0)	{
					ec.saveChanges();	
					updateData();
				}
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}
		}
	}
	public void delCodeMarche()	{

		for (EOParamCodeExer myParam : (NSArray<EOParamCodeExer>)eodCodesMarche.selectedObjects())	{
			ec.deleteObject(myParam);
		}

		if (ec.deletedObjects().count() > 0)	{
			ec.saveChanges();
			eodCodesMarche.deleteSelection();
			myView.getMyEOTableCodesMarche().updateData();
		}
		
	}

	public void delImputation()	{

		for (int i=0;i<eodImputations.selectedObjects().count();i++)	{
			EOParamImputation currentObject = (EOParamImputation)eodImputations.selectedObjects().objectAtIndex(i);			
			ec.deleteObject(currentObject);
		}

		if (ec.deletedObjects().count() > 0)	{
			ec.saveChanges();
			eodImputations.deleteSelection();
			myView.getMyEOTableImputations().updateData();
		}
		
	}
	
	
	/** 
	 * Choix d'une destination a partir d'une liste de valeurs 
	 */
	public void getModePaiement()	{
		
		NSArray modes = ModePaiementSelectCtrl.sharedInstance(ec).getModesPaiement(getSelectedExercice(), false);
		
		if (modes != null && modes.count() > 0)	{

			try {
				
				for (int i=0;i<modes.count();i++)	{
					
					EOModePaiement modePaiement = (EOModePaiement)modes.objectAtIndex(i);
					
					EOParamModePaiement existingParam = EOParamModePaiement.findParamForCode(ec, getSelectedExercice(), modePaiement.modCode());
					
					if (existingParam == null)	{
						EOParamModePaiement newParam = (EOParamModePaiement)Factory.instanceForEntity(ec, EOParamModePaiement.ENTITY_NAME);
						
						newParam.setToExerciceRelationship(getSelectedExercice());
						newParam.setModePaiementRelationship(modePaiement);
												
						ec.insertObject(newParam);
					}
				}

				if (ec.insertedObjects().count() > 0)	{
					ec.saveChanges();
					updateData();
				}
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void delModePaiement()	{

		for (int i=0;i<eodModesPaiement.selectedObjects().count();i++)	{
			EOParamModePaiement currentObject = (EOParamModePaiement)eodModesPaiement.selectedObjects().objectAtIndex(i);			
			ec.deleteObject(currentObject);
		}

		if (ec.deletedObjects().count() > 0)	{
			ec.saveChanges();
			eodModesPaiement.deleteSelection();
			myView.getMyEOTableModesPaiement().updateData();
		}
		
	}
	
	/** 
	 * Choix d'une destination a partir d'une liste de valeurs 
	 */
	public void getDestination()	{
		
		NSArray actions = LolfSelectCtrl.sharedInstance(ec).getTypesAction(getSelectedExercice(), false);
		
		if (actions != null && actions.count() > 0)	{

			try {
				for (int i=0;i<actions.count();i++)	{
					
					EOLolfNomenclatureDepense lolf = (EOLolfNomenclatureDepense)actions.objectAtIndex(i);
					
					EOParamDestination existingParam = EOParamDestination.findParamForCode(ec, getSelectedExercice(), lolf.lolfCode());

					if (existingParam == null)	{
						EOParamDestination newParam = (EOParamDestination)Factory.instanceForEntity(ec, EOParamDestination.ENTITY_NAME);
						
						newParam.setToExerciceRelationship(getSelectedExercice());
						newParam.setTypeActionRelationship(lolf);
						
						ec.insertObject(newParam);					
					}
				}

				if (ec.insertedObjects().count() > 0)	{
					ec.saveChanges();	
					updateData();
				}
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * 
	 * @param sender
	 */
	public void delDestination()	{

		for (int i=0;i<eodLolf.selectedObjects().count();i++)	{
			EOParamDestination currentObject = (EOParamDestination)eodLolf.selectedObjects().objectAtIndex(i);			
			ec.deleteObject(currentObject);
		}

		if (ec.deletedObjects().count() > 0)	{
			ec.saveChanges();
			eodLolf.deleteSelection();
			myView.getMyEOTableDestinations().updateData();
		}
		
	}
		
	
}
