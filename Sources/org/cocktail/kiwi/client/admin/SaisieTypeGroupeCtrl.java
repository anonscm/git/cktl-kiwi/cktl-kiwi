package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.metier.EOSegGroupeInfo;
import org.cocktail.kiwi.client.metier.EOSegTypeInfo;
import org.cocktail.kiwi.client.nibctrl.SaisieTypeGroupeView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;

public class SaisieTypeGroupeCtrl
{
	private static SaisieTypeGroupeCtrl sharedInstance;

	private 	EOEditingContext	ec;
	private 	SaisieTypeGroupeView 	myView;	

	private EOSegGroupeInfo currentGroupe;
	private EOSegTypeInfo currentTypeInfo;

	private	boolean modeModification;

	/** 
	 *
	 */
	public SaisieTypeGroupeCtrl (EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieTypeGroupeView(new JFrame(), true);

		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieTypeGroupeCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieTypeGroupeCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 *
	 */
	private void clearTextFields()	{

		myView.getTfLibelle().setText("");
		myView.getTfCommentaires().setText("");

	}



	/**
	 * 
	 * Fournis = null ==> Saisie du fournisseur
	 * 
	 * @param fournis		Fournisseur associe au vehicule	
	 * @return
	 */
	public EOSegTypeInfo ajouter(EOSegGroupeInfo groupe)	{

		currentGroupe = groupe;
		clearTextFields();

		modeModification = false;

		currentTypeInfo = EOSegTypeInfo.creer(ec, currentGroupe);

		myView.setVisible(true);

		return currentTypeInfo;

	}



	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifier(EOSegTypeInfo type)	{

		clearTextFields();

		currentTypeInfo = type;
		currentGroupe = currentTypeInfo.segGroupeInfo();

		modeModification = true;

		CocktailUtilities.setTextToField(myView.getTfLibelle(), currentTypeInfo.stiLibelle());
		CocktailUtilities.setTextToField(myView.getTfCommentaires(), currentTypeInfo.stiExplications());

		myView.getPopupSignes().setSelectedItem(currentTypeInfo.stiSigne());

		myView.setVisible(true);

		return (currentTypeInfo != null);
	}



	/**
	 *
	 */
	private void valider()	{

		if (myView.getTfLibelle().getText().length() == 0) {
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez entrer un libellé !");				
			return;
		}
		if (myView.getTfCommentaires().getText().length() == 0) {
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez entrer un commentaire !");				
			return;
		}

		currentTypeInfo.setStiLibelle(myView.getTfLibelle().getText());

		currentTypeInfo.setStiSigne((String)myView.getPopupSignes().getSelectedItem());

		if (myView.getTfCommentaires().getText().length() > 0)			
			currentTypeInfo.setStiExplications(myView.getTfCommentaires().getText());

		myView.setVisible(false);

	}


	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentTypeInfo);

		currentTypeInfo = null;		
		myView.setVisible(false);

	}

}