package org.cocktail.kiwi.client.admin;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderMotifs;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOMotifs;
import org.cocktail.kiwi.client.nibctrl.MotifsView;
import org.cocktail.kiwi.common.utilities.AskForString;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class MotifsCtrl {

	private static MotifsCtrl sharedInstance;
	
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private EODisplayGroup eod;
	private MotifsView myView;
	
	private EOMotifs currentMotif;
	
	public MotifsCtrl(EOEditingContext editingContext) {
		
		super();

		ec = editingContext;
	
		eod = new EODisplayGroup();
		myView = new MotifsView(eod);
		
		myView.getButtonAdd().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});
		
		myView.getButtonDelete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});
		
		myView.getButtonAdd().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getButtonDelete().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		myView.getMyEOTable().addListener(new ListenerMotif());

	}

	
	public static MotifsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new MotifsCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 */
	private void actualiser() {
		
		eod.setObjectArray(FinderMotifs.findMotifs(ec));
		myView.getMyEOTable().updateData();
		
	}
	
	/**
	 * 
	 */
	public void open() {
		
		actualiser();
		
		myView.show();
		
	}
	
	
	/**
	 * 
	 */
	private void ajouter() {
		
		String nouveauMotif = AskForString.sharedInstance().getString("Nouveau Motif :", "Libellé Motif : ", "");

		try {
			
			if (nouveauMotif != null && !StringCtrl.chaineVide(nouveauMotif))	{

				EOMotifs newMotif = EOMotifs.creer(ec);
				newMotif.setMotLibelle(nouveauMotif);
				
				ec.saveChanges();
				actualiser();

				eod.setSelectedObject(newMotif);
				myView.getMyEOTable().scrollToSelection(eod.selectionIndexes());
			}
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 */
	private void supprimer() {
		
		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Ce motif va être supprimé de la liste et ne sera plus proposé dans les prochaines missions.\nConfirmez vous cette suppression ?",
				"OUI", "NON"))
			return;
		
		try {

			ec.deleteObject(currentMotif);
			ec.saveChanges();
			
			actualiser();
			
		}
		catch (Exception ex) {
			
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}
		
	}	
		
	private class ListenerMotif implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			currentMotif = (EOMotifs)eod.selectedObject();
			updateUI();
			
		}
	}
	
	
	public void updateUI() {
		
		myView.getButtonDelete().setEnabled(currentMotif != null);
		
	}
	
}
