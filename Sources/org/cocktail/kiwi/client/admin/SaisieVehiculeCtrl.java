package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.factory.FactoryVehicule;
import org.cocktail.kiwi.client.finders.FinderTypeTransport;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.metier.EOVehicule;
import org.cocktail.kiwi.client.nibctrl.SaisieVehiculeView;
import org.cocktail.kiwi.client.select.FournisseurSelectCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieVehiculeCtrl extends SaisieVehiculeView 
{
	private static SaisieVehiculeCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
		
	private EOVehicule 			currentVehicule;
	private EOFournis 			currentFournis;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieVehiculeCtrl (EOEditingContext globalEc) {

		super(new JFrame(), true);

		ec = globalEc;
		
		buttonValider.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		buttonAnnuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		buttonGetFournis.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFournis();
			}
		});
		
		setTypes(FinderTypeTransport.findTypesTransportVehicule(ec));

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieVehiculeCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieVehiculeCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		fournisseur.setText("");
		compagnie.setText("");
		police.setText("");
		marque.setText("");
		immatriculation.setText("");
		puissance.setSelectedItem("6");
	
	}
	
	
	
	/**
	 * 
	 * Fournis = null ==> Saisie du fournisseur
	 * 
	 * @param fournis		Fournisseur associe au vehicule	
	 * @return
	 */
	public EOVehicule ajouterVehicule(EOFournis fournis, EOTypeTransport typeTransport)	{

		clearTextFields();

		currentFournis = fournis;
		if (currentFournis!= null)
			fournisseur.setText(currentFournis.nom()+" "+currentFournis.prenom());

		type.setSelectedItem(typeTransport);
		
		buttonGetFournis.setEnabled(fournis == null && (typeTransport.isVehiculePersonnel() || typeTransport.isVehiculeSncf()) );
		modeModification = false;
		puissance.setEnabled(true);
				
		// Creation et initialisation du nouveau vehicule
		currentVehicule = FactoryVehicule.sharedInstance().creerVehicule(ec, currentFournis, typeTransport);
		
		setVisible(true);	// Ouverture de la fenetre de saisie en mode modal.

		return currentVehicule;
		
	}
	
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifierVehicule(EOVehicule vehicule)	{
				
		// On verifie que le vehicule en question ne soit pas utilise dans une mission
		NSArray missions = EOMission.findMissionsForVehicule(ec, vehicule);
		
		if (missions.count() > 0) {
			
//			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Vous ne pouvez pas modifier ce véhicule, il est déjà utilisé dans au moins une mission !");
//			return false;

			puissance.setEnabled(false);
			
		}
		else {

			puissance.setEnabled(true);

		}
	
		clearTextFields();

		currentVehicule = vehicule;
		currentFournis = currentVehicule.fournis();

		if (currentVehicule.fournis() != null)
			fournisseur.setText(currentFournis.nom()+" "+currentFournis.prenom());

		type.setSelectedItem(currentVehicule.typeTransport());
		
		buttonGetFournis.setEnabled(false);
		modeModification = true;

		puissance.setSelectedItem(currentVehicule.vehPuissance());

		marque.setText(currentVehicule.vehMarque());
		immatriculation.setText(currentVehicule.vehImmatriculation());

		if (currentVehicule.vehCompagnie() != null)
			compagnie.setText(currentVehicule.vehCompagnie());
		
		if (currentVehicule.vehPolice() != null)
			police.setText(currentVehicule.vehPolice());

		setVisible(true);
		
		return (currentVehicule != null);
	}
	
	
	
	/**
	 * 
	 */
	private void getFournis() {
		
		EOFournis fournis = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();
		
		if (fournis != null) {
			
			currentFournis = fournis;
			
			fournisseur.setText(currentFournis.nom()+" "+currentFournis.prenom());
			
		}
	}
	
	
	
	/**
	 *
	 */
	private void valider()	{
		
		try {

			currentVehicule.setFournisRelationship(currentFournis);

			currentVehicule.setTypeTransportRelationship((EOTypeTransport)type.getSelectedItem());


			if (!StringCtrl.chaineVide(immatriculation.getText()))
				currentVehicule.setVehImmatriculation(immatriculation.getText());
			else
				currentVehicule.setVehImmatriculation(null);

			if (!StringCtrl.chaineVide(marque.getText()))
				currentVehicule.setVehMarque(marque.getText());
			else
				currentVehicule.setVehMarque(null);

			if (!StringCtrl.chaineVide(police.getText()))
				currentVehicule.setVehPolice(police.getText());
			else
				currentVehicule.setVehPolice(null);

			if (!StringCtrl.chaineVide(compagnie.getText()))
				currentVehicule.setVehCompagnie(compagnie.getText());
			else
				currentVehicule.setVehCompagnie(null);

			currentVehicule.setVehPuissance(puissance.getSelectedItem().toString());

			currentVehicule.setVehDate(new NSTimestamp());

			currentVehicule.setVehEtat("VALIDE");

			currentVehicule.validateObjectMetier();
			
		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		dispose();
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentVehicule);
			
		currentVehicule = null;
		
		dispose();
		
	}
	
}