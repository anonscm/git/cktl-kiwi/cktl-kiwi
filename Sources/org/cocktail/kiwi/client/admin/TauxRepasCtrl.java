/*
 * Created on 3 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderRembJournalier;
import org.cocktail.kiwi.client.finders.FinderRembZone;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOPlageHoraire;
import org.cocktail.kiwi.client.metier.EORembJournalier;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.nibctrl.TauxRepasView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TauxRepasCtrl{

	private static TauxRepasCtrl sharedInstance;
	private ApplicationClient NSApp;
	private	EOEditingContext ec;

	private TauxRepasView myView;

	public EODisplayGroup eod = new EODisplayGroup();

	private EORembJournalier currentRemboursement;

	/** 
	 * Constructeur 
	 */
	public TauxRepasCtrl(EOEditingContext editingContext)	{
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;

		myView = new TauxRepasView(new JFrame(), true, eod);

		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});

		NSArray mySort = new NSArray(new EOSortOrdering(EORembJournalier.REM_DATE_KEY, EOSortOrdering.CompareDescending));
		eod.setSortOrderings(mySort);

		myView.getBtnAjouter().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_TAUX_CHANCELLERIE));
		myView.getBtnModifier().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_TAUX_CHANCELLERIE));

		myView.getMyEOTable().addListener(new ListenerRemboursement());

		CocktailUtilities.initPopupAvecListe(myView.getZones(), FinderRembZone.findZonesForRepasNuits(ec), false);
		myView.getZones().addActionListener(new PopupZonesListener());

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TauxRepasCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TauxRepasCtrl(editingContext);
		return sharedInstance;
	}



	public EORembJournalier getCurrentRemboursement() {
		return currentRemboursement;
	}

	public void setCurrentRemboursement(EORembJournalier currentRemboursement) {
		this.currentRemboursement = currentRemboursement;
	}


	private class ListenerRemboursement implements ZEOTableListener {

		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentRemboursement((EORembJournalier)eod.selectedObject());

		}
	}

	/**
	 * 
	 */
	public void actualiser()	{				
		eod.setObjectArray(FinderRembJournalier.findRemboursementsForZone(ec, (EORembZone)myView.getZones().getSelectedItem(), new Integer(EOPlageHoraire.CLE_REPAS_MIDI)));
		myView.getMyEOTable().updateData();
	}

	/**
	 * 
	 */
	public void ajouter()	{
		SaisieTauxRepasCtrl.sharedInstance(ec).ajouter((EORembZone)myView.getZones().getSelectedItem());
		actualiser();
	}

	/**
	 * 
	 */
	public void modifier()	{
		SaisieTauxRepasCtrl.sharedInstance(ec).modifier( getCurrentRemboursement());
	}

	private class PopupZonesListener implements ActionListener	{
		public PopupZonesListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			actualiser();

		}
	}

	 /**
	  * 
	  *
	  */
	 public void open()	{

		 actualiser();

		 myView.setVisible(true);

	 }

}