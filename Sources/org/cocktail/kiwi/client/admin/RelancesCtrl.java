package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.metier.EOCompte;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOTitreMission;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.nibctrl.RelancesView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class RelancesCtrl {

	private static RelancesCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private FraisListener fraisListener = new FraisListener();
	private EODisplayGroup eod;
	private RelancesView myView;
	private EOMission currentMission;

	public RelancesCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new RelancesView(eod);


		myView.getBtnEnvoyer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {envoyer();}});		

		myView.getPopupDuree().setSelectedItem(new Integer(6));
		myView.getPopupDuree().addActionListener(new PopupDureeListener());

		myView.getCheckFraisAll().addActionListener(fraisListener);
		myView.getCheckFraisAvec().addActionListener(fraisListener);
		myView.getCheckFraisSans().addActionListener(fraisListener);

		myView.getMyEOTable().addListener(new ListenerMissions());

		myView.getTfObjet().setText("Relance Mission N° xxx");

		CocktailUtilities.initTextArea(myView.getTfDestinataires(), false, false);
		CocktailUtilities.initTextField(myView.getTfFrom(), false, false);
		CocktailUtilities.initTextField(myView.getTfObjet(), false, false);

	}


	public static RelancesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new RelancesCtrl(editingContext);
		return sharedInstance;
	}



	/**
	 * 
	 */
	private void actualiser() {

		EOCompte compteFrom = EOCompte.compteForPersId(ec, NSApp.getUtilisateur().persId());
		if ( compteFrom != null ) {
			CocktailUtilities.setTextToField(myView.getTfFrom(), compteFrom.cptEmail() + "@" + compteFrom.cptDomaine());
		}	

		NSTimestamp dateReference = DateCtrl.dateAvecAjoutMois(new NSTimestamp() , - (((Integer)myView.getPopupDuree().getSelectedItem()).intValue()) );
		eod.setObjectArray(EOMission.findMissionsValidesBeforeDate(ec, dateReference));
		filter();

	}


	private class FraisListener implements ActionListener	{
		public FraisListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			filter();
		}
	}

	private class PopupDureeListener implements ActionListener	{
		public PopupDureeListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}


	/**
	 * 
	 */
	public void open() {

		actualiser();
		myView.setVisible(true);

	}


	/**
	 * 
	 */
	private void envoyer() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous relancer les créateurs des missions sélectionnées ?",
				"OUI", "NON"))
			return;

		try {

			if (StringCtrl.chaineVide(myView.getTfObjet().getText()) && StringCtrl.chaineVide(myView.getTfObjet().getText())) {
				EODialogs.runErrorDialog("ERREUR", "Vous devez saisir un objet ET un message !");
				return;
			}

			String	message = myView.getTfMessage().getText();

			for (EOMission myMission : (NSArray<EOMission>) eod.selectedObjects()) {

				String sujet  = "Relance Mission N° " + myMission.misNumero();

				String destinataire = "";

				EOUtilisateur createur = myMission.utilisateurCreation();
				if (createur != null) {
					EOCompte compte = EOCompte.compteForPersId(ec,createur.individu().persId());
					if ( compte != null ) {
						destinataire = compte.cptEmail()+"@"+compte.cptDomaine();
					}	
				}

				if (destinataire.length() > 0) {
					String result = (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestEnvoyerMailAgent", 
							new Class[] {String.class,String.class,String.class,String.class}, 
							new Object[] {
							CocktailUtilities.getTextFromField(myView.getTfFrom()), 
							destinataire ,
							sujet, 
							message }, false);

					System.out.println("RelancesCtrl.envoyer() " + result);

				}

			}

			EODialogs.runInformationDialog("Relances", "Le message de relance a bien été envoyé aux créateurs des missions sélectionnées !");

		}
		catch (Exception e)	{
			e.printStackTrace();
		}

		actualiser();

	}

	/**
	 * 
	 *
	 */
	private void filter()	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		NSTimestamp dateMini = DateCtrl.dateAvecAjoutMois(new NSTimestamp() , - 24 );
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_FIN_KEY + " >= %@", new NSArray(dateMini)));

		if (myView.getCheckFraisAll().isSelected() == false) {

			if (myView.getCheckFraisAvec().isSelected())
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TITRE_MISSION_KEY+"."+EOTitreMission.TIT_PAIEMENT_KEY+"=%@", new NSArray(new Integer(1))));
			else
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TITRE_MISSION_KEY+"."+EOTitreMission.TIT_PAIEMENT_KEY+"=%@", new NSArray(new Integer(0))));
		}

		NSMutableArray orQualifiers = new NSMutableArray();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " = %@", new NSArray(EOMission.ETAT_VALIDE)));

		mesQualifiers.addObject(new EOOrQualifier(orQualifiers));

		eod.setQualifier(new EOAndQualifier(mesQualifiers));

		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert � filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerMissions implements ZEOTableListener {

		public void onDbClick() {

			EnteteMission.sharedInstance(ec).setMission(currentMission);
			//myView.setVisible(false);

		}

		public void onSelectionChanged() {

			currentMission = (EOMission) eod.selectedObject();

			String destinataires = "";
			for (EOMission myMission: (NSArray<EOMission>)eod.selectedObjects()) {

				EOUtilisateur createur = myMission.utilisateurCreation();

				if (createur != null) {
					EOCompte compte = EOCompte.compteForPersId(ec,createur.individu().persId());
					if ( compte != null ) {
						if (destinataires.length() > 0)
							destinataires = destinataires.concat(",");
						destinataires = destinataires.concat(compte.cptEmail()+"@"+compte.cptDomaine());
					}				
				}
			}

			myView.getTfDestinataires().setText(destinataires);

			updateUI();

		}
	}


	public void updateUI() {

	}

}
