/* SimulationBudgetaire.java created by cpinsard on Tue 24-Jun-2003 */

package org.cocktail.kiwi.client.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.factory.FactoryKiwiSignataireEtatFrais;
import org.cocktail.kiwi.client.factory.FactoryKiwiSignataireOm;
import org.cocktail.kiwi.client.finders.FinderKiwiSignataireEtatFrais;
import org.cocktail.kiwi.client.finders.FinderKiwiSignataireOm;
import org.cocktail.kiwi.client.finders.FinderOrgan;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireEtatFrais;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireOm;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.select.SignatairesSelectCtrl;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public class SignatairesCtrl extends EOModalDialogController	{
	
	public static SignatairesCtrl sharedInstance;
	
	// Objects graphiques
	
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;
	
	public JPanel viewTableLbud, viewTableOm, viewTableEtatFrais;
	
	public EODisplayGroup eodLbud, eodSignatairesOm, eodSignatairesEtatFrais;
	
	private ZEOTable 		myEOTableLbud, myEOTableOm, myEOTableEtatFrais;
	private ZEOTableModel 	myTableModelLbud, myTableModelOm, myTableModelEtatFrais;
	private TableSorter 	myTableSorterLbud, myTableSorterOm, myTableSorterEtatFrais;
	
	public 	JButton			btnFermer, btnAddSignataireOm, btnDelSignataireOm, btnAddSignataireEtatFrais, btnDelSignataireEtatFrais, btnInitialiser;
		
	private JTextField 		filtreUb, filtreCr, filtreSousCr, filtreEtab;
	private	JRadioButton 	checkNiveau1, checkNiveau2, checkNiveau3, checkNiveau4;

	// Variables locales
	ApplicationClient 	NSApp;
	Superviseur		  	superviseur;
	
	protected	EOOrgan currentOrgan;

	protected	NiveauOrganListener niveauOrganListener = new NiveauOrganListener ();

	private		EOKiwiSignataireOm 			currentSignataireOm;
	private		EOKiwiSignataireEtatFrais 	currentSignataireEtatFrais;
	
	protected ActionAddSignataireOm 		actionAddSignataireOm = new ActionAddSignataireOm();
	protected ActionDelSignataireOm 	actionDelSignataireOm = new ActionDelSignataireOm();
	
	protected ActionAddSignataireEtatFrais 		actionAddSignataireEtatFrais = new ActionAddSignataireEtatFrais();
	protected ActionDelSignataireEtatFrais		actionDelSignataireEtatFrais = new ActionDelSignataireEtatFrais();
	
	protected ActionClose 		actionClose = new ActionClose();
		
	protected ActionInit 		actionInit = new ActionInit();

	protected	ListenerLbud listenerLbuds = new ListenerLbud();
		
	protected	EOEditingContext	ec;
	
	/** 
	 *	Constructeur 
	 */
	public SignatairesCtrl (EOEditingContext globalEc)	{
		super();
		
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
		
		ec = globalEc;	
		
		initGUI();
		gui_initView();
	}
	
	public static SignatairesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new SignatairesCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 */
	private void gui_initButtons() {

		btnAddSignataireEtatFrais = new JButton(actionAddSignataireEtatFrais);
		btnAddSignataireEtatFrais.setPreferredSize(new Dimension(21,21));
		
		btnDelSignataireEtatFrais = new JButton(actionDelSignataireEtatFrais);
		btnDelSignataireEtatFrais.setPreferredSize(new Dimension(21,21));

		btnAddSignataireOm = new JButton(actionAddSignataireOm);
		btnAddSignataireOm.setPreferredSize(new Dimension(21,21));

		btnDelSignataireOm = new JButton(actionDelSignataireOm);
		btnDelSignataireOm.setPreferredSize(new Dimension(21,21));

		btnAddSignataireEtatFrais.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));
		btnDelSignataireEtatFrais.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));
		btnAddSignataireOm.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));
		btnDelSignataireOm.setVisible(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));

	}
	
 	private void gui_initTextFields() {
 		
        filtreEtab = new JTextField("");
        filtreEtab.setPreferredSize(new Dimension(65,18));
        filtreEtab.setFont(new Font("Times", Font.PLAIN, 11));

        filtreUb = new JTextField("");
        filtreUb.setPreferredSize(new Dimension(65,18));
        filtreUb.setFont(new Font("Times", Font.PLAIN, 11));

        filtreCr = new JTextField("");
        filtreCr.setPreferredSize(new Dimension(90,18));
        filtreCr.setFont(new Font("Times", Font.PLAIN, 11));

        filtreSousCr = new JTextField("");
        filtreSousCr.setPreferredSize(new Dimension(140,18));
        filtreSousCr.setFont(new Font("Times", Font.PLAIN, 11));

        filtreEtab.getDocument().addDocumentListener(new ADocumentListener());
        filtreUb.getDocument().addDocumentListener(new ADocumentListener());
        filtreCr.getDocument().addDocumentListener(new ADocumentListener());
        filtreSousCr.getDocument().addDocumentListener(new ADocumentListener());
 		 		 		
 	}

 	private void gui_initCheckBox() {
 		
		ButtonGroup groupeNiveaux = new ButtonGroup();
		
		checkNiveau1 = new JRadioButton("Niveau ETAB");
		checkNiveau1.setFocusable(false);
		
		checkNiveau2 = new JRadioButton("Niveau UB");
		checkNiveau2.setFocusable(false);

		checkNiveau3 = new JRadioButton("Niveau CR");
		checkNiveau3.setFocusable(false);
		checkNiveau3.setSelected(true);

		checkNiveau4 = new JRadioButton("Niveau SOUS-CR");
		checkNiveau4.setFocusable(false);		

		checkNiveau1.addItemListener(niveauOrganListener);
		checkNiveau2.addItemListener(niveauOrganListener);
		checkNiveau3.addItemListener(niveauOrganListener);
		checkNiveau4.addItemListener(niveauOrganListener);
		
		groupeNiveaux.add(checkNiveau1);
		groupeNiveaux.add(checkNiveau2);
		groupeNiveaux.add(checkNiveau3);
		groupeNiveaux.add(checkNiveau4);

 		
 	}
	
	
	private JPanel gui_buildPanelLbuds() {
				
		JPanel panel = new JPanel(new BorderLayout());
		panel.setPreferredSize(new Dimension(500, 500));

		JPanel filterPanel = new JPanel(new FlowLayout());
		filterPanel.add(filtreEtab);
		filterPanel.add(filtreUb);
		filterPanel.add(filtreCr);
		filterPanel.add(filtreSousCr);		
				
		JPanel niveauPanel = new JPanel(new FlowLayout());
		niveauPanel.add(checkNiveau1);		
		niveauPanel.add(checkNiveau2);		
		niveauPanel.add(checkNiveau3);		
		niveauPanel.add(checkNiveau4);
		
		JPanel	northPanel = new JPanel(new BorderLayout());
		northPanel.add(niveauPanel, BorderLayout.NORTH);
		northPanel.add(filterPanel, BorderLayout.WEST);

		panel.add(northPanel, BorderLayout.NORTH);
		panel.add(viewTableLbud, BorderLayout.CENTER);
		
		
		return panel;
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildPanelSignatairesOm() {
		
		JPanel panel = new JPanel(new BorderLayout());

		JTextField titre = new JTextField(" Ordres de Mission ");
		titre.setBorder(BorderFactory.createEmptyBorder());
		titre.setPreferredSize(new Dimension(150, 18));
		titre.setEditable(false);
		titre.setFocusable(false);
		titre.setHorizontalAlignment(JTextField.LEFT);
		titre.setForeground(CocktailConstantes.BG_COLOR_WHITE);
		titre.setBackground(new Color(194,176,190));
		
		ArrayList arrayList = new ArrayList();
		arrayList.add(btnAddSignataireOm);
		arrayList.add(btnDelSignataireOm);
		
		JPanel panelMenu = new JPanel(new BorderLayout());
		panelMenu.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		panelMenu.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.NORTH);

		panel.add(titre, BorderLayout.NORTH);
		panel.add(viewTableOm, BorderLayout.CENTER);
		panel.add(panelMenu, BorderLayout.EAST);

		return panel;
	}
	
	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildPanelSignatairesEtatFrais() {
		
		JPanel panel = new JPanel(new BorderLayout());

		JTextField titre = new JTextField(" Etats de Frais ");
		titre.setBorder(BorderFactory.createEmptyBorder());
		titre.setPreferredSize(new Dimension(150, 18));
		titre.setEditable(false);
		titre.setFocusable(false);
		titre.setHorizontalAlignment(JTextField.LEFT);
		titre.setForeground(CocktailConstantes.BG_COLOR_WHITE);
		titre.setBackground(new Color(194,176,190));
		
		ArrayList arrayList = new ArrayList();
		arrayList.add(btnAddSignataireEtatFrais);
		arrayList.add(btnDelSignataireEtatFrais);
		
		JPanel panelMenu = new JPanel(new BorderLayout());
		panelMenu.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		panelMenu.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.NORTH);

		panel.add(titre, BorderLayout.NORTH);
		panel.add(viewTableEtatFrais, BorderLayout.CENTER);
		panel.add(panelMenu, BorderLayout.EAST);

		return panel;
	}

	private JPanel gui_buildSouthPanel() {
		
		JPanel panel = new JPanel(new BorderLayout());
		
		
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));

		arrayList = new ArrayList();
		arrayList.add(actionInit);
		JPanel westPanel = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 175, 23));                
		westPanel.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));

		panel.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelButtons, BorderLayout.EAST);
		panel.add(westPanel, BorderLayout.WEST);
		
		return panel;
		
	}
	
	
	/**
	 * Initialisation de tous les objets graphiques du module.
	 * Mise en place des listeners, couleurs, boutons, EOTables, ...
	 */
	private void gui_initView()	{
		
		mainWindow = new JDialog(mainFrame, "KIWI - Gestion des signataires", true);
		
		viewTableLbud.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableOm.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableEtatFrais.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		
		gui_initTextFields();
		gui_initCheckBox();
		gui_initButtons();		
		
		ArrayList arrayList  = new ArrayList();
		arrayList.add(gui_buildPanelSignatairesOm());
		arrayList.add(gui_buildPanelSignatairesEtatFrais());
		
		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.CENTER);

		JPanel northPanel= new JPanel(new BorderLayout());
		northPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));

		JTextField title = new JTextField("SIGNATAIRES");
		title.setPreferredSize(new Dimension(180,23));
		title.setFont(new Font("Times", Font.BOLD, 22));
		title.setEditable(false);
		title.setFocusable(false);
		title.setBorder(BorderFactory.createEmptyBorder());
		title.setForeground(new Color(80,130,145));
		title.setHorizontalAlignment(0);

		northPanel.add(title, BorderLayout.CENTER);
		
		rightPanel.add(northPanel, BorderLayout.NORTH);
		
		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(850, 475));

		mainView.add(gui_buildPanelLbuds(), BorderLayout.WEST);
		mainView.add(rightPanel, BorderLayout.CENTER);
		mainView.add(gui_buildSouthPanel(), BorderLayout.SOUTH);
		
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
		
	}
	
	
	
	/**
	 * 
	 * @return
	 */
    public EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (checkNiveau1.isSelected())
        	mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = 1", null));
        else
            if (checkNiveau2.isSelected())
            	mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = 2", null));
            else
                if (checkNiveau3.isSelected())
                	mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = 3", null));
                else
                	mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = 4", null));
                	
        if (!StringCtrl.chaineVide(filtreEtab.getText()))	{
            NSArray args = new NSArray("*"+filtreEtab.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("orgEtab caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(filtreUb.getText()))	{
            NSArray args = new NSArray("*"+filtreUb.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("orgUb caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(filtreCr.getText()))	{
            NSArray args = new NSArray("*"+filtreCr.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("orgCr caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(filtreSousCr.getText()))	{
            NSArray args = new NSArray("*"+filtreSousCr.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("orgSouscr caseInsensitiveLike %@",args));
        }

        return new EOAndQualifier(mesQualifiers);        
    }
    
    /** 
    *
    */
   private void filter()	{
       
       eodLbud.setQualifier(getFilterQualifier());
       eodLbud.updateDisplayedObjects();              
       myEOTableLbud.updateData();

   }
	
	
	/**
	 * 
	 *
	 */
	public void open()	{

		if (eodLbud.displayedObjects().count() == 0)	{			
			
			NSMutableArray organs = new NSMutableArray(FinderOrgan.findOrgansForUtilisateur(ec, NSApp.getUtilisateur(), NSApp.getExerciceCourant()));
			
			organs.addObjectsFromArray(FinderOrgan.findOrgansForNiveau(ec, NSApp.getExerciceCourant(), new Integer(1)));
			
			eodLbud.setObjectArray(organs.immutableClone());
		}
		
		filter();
		
		updateUI();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void initGUI() {
		
		eodLbud = new EODisplayGroup();
		eodSignatairesOm = new EODisplayGroup();
		eodSignatairesEtatFrais = new EODisplayGroup();
		
		eodSignatairesOm.setSortOrderings(new NSArray(new EOSortOrdering(EOKiwiSignataireOm.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending)));
		eodSignatairesEtatFrais.setSortOrderings(new NSArray(new EOSortOrdering(EOKiwiSignataireEtatFrais.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending)));
		
		viewTableLbud = new JPanel();
		viewTableOm = new JPanel();
		viewTableEtatFrais = new JPanel();
		
		initTableModel();
		initTable();
		
		myEOTableLbud.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTableLbud.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTableLbud.setSelectionForeground(CocktailConstantes.BG_COLOR_WHITE);
		myEOTableLbud.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTableLbud.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableLbud.removeAll();
		viewTableLbud.setLayout(new BorderLayout());
		viewTableLbud.add(new JScrollPane(myEOTableLbud), BorderLayout.CENTER);
		
		myEOTableOm.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTableOm.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTableOm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTableOm.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableOm.removeAll();
		viewTableOm.setLayout(new BorderLayout());
		viewTableOm.add(new JScrollPane(myEOTableOm), BorderLayout.CENTER);
				
		myEOTableEtatFrais.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTableEtatFrais.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTableEtatFrais.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTableEtatFrais.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableEtatFrais.removeAll();
		viewTableEtatFrais.setLayout(new BorderLayout());
		viewTableEtatFrais.add(new JScrollPane(myEOTableEtatFrais), BorderLayout.CENTER);

	}
	
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{
		
		myEOTableLbud = new ZEOTable(myTableSorterLbud);
		myEOTableLbud.addListener(listenerLbuds);
		myTableSorterLbud.setTableHeader(myEOTableLbud.getTableHeader());		
		
		myEOTableOm = new ZEOTable(myTableSorterOm);
		myEOTableOm.addListener(new ListenerOm());
		myEOTableOm.setTableHeader(myEOTableOm.getTableHeader());

		myEOTableEtatFrais = new ZEOTable(myTableSorterEtatFrais);
		myEOTableEtatFrais.addListener(new ListenerEtatFrais());
		myEOTableEtatFrais.setTableHeader(myEOTableEtatFrais.getTableHeader());

	}
	
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodLbud, "orgEtab", "ETAB", 75);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodLbud, "orgUb", "UB", 75);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodLbud, "orgCr", "CR", 100);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodLbud, "orgSouscr", "SOUS CR", 150);
		myCols.add(col);

		myTableModelLbud = new ZEOTableModel(eodLbud, myCols);
		myTableSorterLbud = new TableSorter(myTableModelLbud);
		
		// Signataires OM
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodSignatairesOm, EOKiwiSignataireOm.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, "Nom", 60);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodSignatairesOm, EOKiwiSignataireOm.INDIVIDU_KEY+"."+EOIndividu.PRENOM_KEY, "Prénom", 60);
		myCols.add(col);

		myTableModelOm = new ZEOTableModel(eodSignatairesOm, myCols);
		myTableSorterOm = new TableSorter(myTableModelOm);
		
		// Signataires ETAT FRAIS
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodSignatairesEtatFrais, EOKiwiSignataireEtatFrais.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, "Nom", 240);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodSignatairesEtatFrais, EOKiwiSignataireEtatFrais.INDIVIDU_KEY+"."+EOIndividu.PRENOM_KEY, "Prénom", 240);
		myCols.add(col);
		
		myTableModelEtatFrais = new ZEOTableModel(eodSignatairesEtatFrais, myCols);
		myTableSorterEtatFrais = new TableSorter(myTableModelEtatFrais);
		
	}
	
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerLbud implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			
			currentOrgan = (EOOrgan) eodLbud.selectedObject();
			
			eodSignatairesOm.setObjectArray(FinderKiwiSignataireOm.findSignatairesForOrgan(ec, currentOrgan));
			eodSignatairesEtatFrais.setObjectArray(FinderKiwiSignataireEtatFrais.findSignatairesForOrgan(ec, currentOrgan));
			
			myEOTableOm.updateData();
			myTableModelOm.fireTableDataChanged();
			myEOTableEtatFrais.updateData();
			myTableModelEtatFrais.fireTableDataChanged();
			updateUI();
			
		}
	}
		
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerOm implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentSignataireOm = (EOKiwiSignataireOm)eodSignatairesOm.selectedObject();
			updateUI();
		}
	}
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerEtatFrais implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentSignataireEtatFrais = (EOKiwiSignataireEtatFrais)eodSignatairesEtatFrais.selectedObject();
			updateUI();
		}
	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionAddSignataireOm extends AbstractAction {
		
		public ActionAddSignataireOm() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_ADD);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			try {
				
				EOIndividu signataire = SignatairesSelectCtrl.sharedInstance(ec).getSignataire(currentOrgan);
				
				if (signataire != null)	{
					
					FactoryKiwiSignataireOm.sharedInstance().creerSignataire(ec, signataire, currentOrgan);

				}
				
				ec.saveChanges();
				
				listenerLbuds.onSelectionChanged();

				mainWindow.show();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
			}
			
		}  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionDelSignataireOm extends AbstractAction {
		
		public ActionDelSignataireOm() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_DELETE);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			try {
				
				ec.deleteObject(currentSignataireOm);				
				ec.saveChanges();
				
				listenerLbuds.onSelectionChanged();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
			}
			
			
		}  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionAddSignataireEtatFrais extends AbstractAction {
		
		public ActionAddSignataireEtatFrais() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_ADD);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			try {
				
				EOIndividu signataire = SignatairesSelectCtrl.sharedInstance(ec).getSignataire(currentOrgan);
				
				if (signataire != null)	{
					
					FactoryKiwiSignataireEtatFrais.sharedInstance().creerSignataire(ec, signataire, currentOrgan);

				}
				
				ec.saveChanges();
				
				listenerLbuds.onSelectionChanged();

				mainWindow.show();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
			}
			
		}  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionDelSignataireEtatFrais extends AbstractAction {
		
		public ActionDelSignataireEtatFrais() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_DELETE);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			try {
				
				ec.deleteObject(currentSignataireEtatFrais);				
				ec.saveChanges();
				
				listenerLbuds.onSelectionChanged();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
			}
			
			
		}  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionClose extends AbstractAction {
		
		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CLOSE);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			mainWindow.dispose();
			
		}  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionInit extends AbstractAction {
		
		public ActionInit() {
			super("Initialiser (Jefy Admin)");
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			if (!EODialogs.runConfirmOperationDialog("Attention",
					"Tous les signataires vont être supprimés et réinitialisés avec les données de Jefy Admin.\nVoulez-vous poursuivre ?",
					"OUI", "NON"))
				return;

			try {
			
				ServerProxy.clientSideRequestInitialiserSignataires(ec);
				
				MsgPanel.sharedInstance().runConfirmationDialog("OK","Les signataires ont été téléchargés en fonction des paramètres de jefy_admin.");
				
				listenerLbuds.onSelectionChanged();
				
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			
		}  
	} 
	
	
	/**
	 * 
	 *
	 */
	private void updateUI()	{
		
		actionDelSignataireOm.setEnabled(currentSignataireOm != null);
		actionDelSignataireEtatFrais.setEnabled(currentSignataireEtatFrais != null);

		actionInit.setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));

	}
	
	
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
	   
		/** 
		 * Classe d'ecoute pour le changement d'affichage de la liste des contrats.
		 */
		private class NiveauOrganListener implements ItemListener 	{
			
			public void itemStateChanged(ItemEvent e)		{
				
				if (e.getStateChange() == ItemEvent.SELECTED)
					filter();
			}
		} 
	
}