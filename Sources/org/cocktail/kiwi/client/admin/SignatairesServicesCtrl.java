/* SimulationBudgetaire.java created by cpinsard on Tue 24-Jun-2003 */

package org.cocktail.kiwi.client.admin;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.factory.FactoryKiwiSignataireService;
import org.cocktail.kiwi.client.finders.FinderKiwiSignataireService;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireEtatFrais;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireOm;
import org.cocktail.kiwi.client.metier.EOStructure;
import org.cocktail.kiwi.client.nibctrl.SignatairesServicesView;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;


public class SignatairesServicesCtrl extends EOModalDialogController	{

	public static SignatairesServicesCtrl sharedInstance;

	private SignatairesServicesView myView;

	// Objects graphiques

	public EODisplayGroup eodOm, eodFrais, eodStructure;

	// Variables locales
	ApplicationClient 	NSApp;
	Superviseur		  	superviseur;

	private		EOStructure 				currentStructure;
	private		EOKiwiSignataireOm 			currentSignataireOm;
	private		EOKiwiSignataireEtatFrais 	currentSignataireEtatFrais;

	protected	ListenerOm listenerOm = new ListenerOm();
	protected	ListenerEtatFrais listenerEtatFrais = new ListenerEtatFrais();

    protected	boolean			treeViewCharge;

	protected	EOEditingContext	ec;

	/** 
	 *	Constructeur 
	 */
	public SignatairesServicesCtrl (EOEditingContext globalEc)	{
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
		ec = globalEc;	

		eodOm = new EODisplayGroup();
		
		eodFrais = new EODisplayGroup();
		
		eodStructure = new EODisplayGroup();
		eodStructure.setDataSource(NSApp.getDatasourceForEntity(ec, EOStructure.ENTITY_NAME));  
		((EODistributedDataSource)eodStructure.dataSource()).setEditingContext(ec);

		myView = new SignatairesServicesView(eodOm, eodFrais, eodStructure);
		myView.getViewStructures().setEditingContext(ec);
		
		myView.getBtnAddSignOm().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addSignataireOm();
			}
		});

		myView.getBtnDelSignOm().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delSignataireOm();
			}
		});

		myView.getBtnInit().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				initialiserSignataires();
			}
		});
		
		myView.getBtnAddSignOm().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));
		myView.getBtnDelSignOm().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));
		myView.getBtnInit().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_SIGNATAIRES));

        // TREE VIEW
        NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("CRITreeViewDidClick",new Class[] {NSNotification.class}), "CRITreeViewDidClick",null);
        treeViewCharge = false;

	}

	public static SignatairesServicesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new SignatairesServicesCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();


		return new EOAndQualifier(mesQualifiers);        
	}

	/** 
	 *
	 */
	private void filter()	{

	}


	/**
	 * 
	 *
	 */
	public void open()	{

    	if (!treeViewCharge)	{
    		    		
    		myView.getViewStructures().setTreeTable(eodStructure);
    		myView.getViewStructures().setTableName(EOStructure.ENTITY_NAME);
    		
    		myView.getViewStructures().setDynamic(false);
    		myView.getViewStructures().setTitle("Structures");

    		myView.getViewStructures().setQualifierForFirstColumn(EOQualifier.qualifierWithQualifierFormat("cStructure = cStructurePere or cTypeStructure = 'E'", null));

    		myView.getViewStructures().setRestrictionQualifier(EOQualifier.qualifierWithQualifierFormat("repartTypeGroupes.tgrpCode = 'S'", null));            	
            
    		myView.getViewStructures().setParentRelationship(EOStructure.STRUCTURE_PARENT_KEY);
    		myView.getViewStructures().setFieldForDisplay(EOStructure.LL_STRUCTURE_KEY);
            
            if(!myView.getViewStructures().initialize(true)) 
                NSLog.out.appendln(this.getClass().getName()+".initializeTreeView() - Erreur : l'initialisation du treeView a echoue");
            
            myView.getViewStructures().setRootVisible(true); 	
    		updateUI();
        }

		filter();

		ZUiUtil.centerWindow(myView);
		myView.show();

		updateUI();

	}



	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerOm implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentSignataireOm = (EOKiwiSignataireOm)eodOm.selectedObject();
			updateUI();
		}
	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerEtatFrais implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentSignataireEtatFrais = (EOKiwiSignataireEtatFrais)eodFrais.selectedObject();
			updateUI();
		}
	}


	public void addSignataireOm() {

		try {

			if (currentStructure.grpResponsable() != null) {
				
				EOIndividu signataire = currentStructure.responsable();

				if (signataire != null)	{

					FactoryKiwiSignataireService.sharedInstance().creer(ec, signataire, currentStructure);

				}

				ec.saveChanges();
				
				actualiser();
				
			}

		}
		catch (Exception ex)	{
			ex.printStackTrace();
		}
	} 


	public void delSignataireOm() {

		try {

			ec.deleteObject(currentSignataireOm);				
			ec.saveChanges();

		}
		catch (Exception ex)	{
			ex.printStackTrace();
		}
	} 


	public void addSignataireEtatFrais() {

		try {


			ec.saveChanges();

		}
		catch (Exception ex)	{
			ex.printStackTrace();
		}
	} 



	public void delSignataireEtatFrais() {

		try {

			ec.deleteObject(currentSignataireEtatFrais);				
			ec.saveChanges();

		}
		catch (Exception ex)	{
			ex.printStackTrace();
		}

	} 


	
	public void close() {

		myView.dispose();

	} 


		public void initialiserSignataires() {

			if (!EODialogs.runConfirmOperationDialog("Attention",
					"Tous les signataires vont être supprimés et réinitialisés avec les données de l'Annuaire.\nVoulez-vous poursuivre ?",
					"OUI", "NON"))
				return;

			try {

				ServerProxy.clientSideRequestInitialiserSignatairesServices(ec);

				MsgPanel.sharedInstance().runConfirmationDialog("OK","Les signataires ont été téléchargés en fonction des paramètres de l'annuaire.");

				actualiser();
				
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}

	} 


	/**
	 * 
	 *
	 */
	private void updateUI()	{

		myView.getBtnDelSignOm().setEnabled(currentSignataireOm != null);

	}

	
	private void actualiser() {
		
		
        // Fetch des signataires
        eodOm.setObjectArray(FinderKiwiSignataireService.findSignatairesForService(ec, currentStructure));
        myView.getMyEOTableOm().updateData();

	}
	

    /** 
     * Notification revue lors d'un double click sur le tree view : une structure a ete selectionnee 
     */
    public void CRITreeViewDidClick(NSNotification aNotif)	{
        NSDictionary result = (NSDictionary)aNotif.userInfo();
        currentStructure = (EOStructure)result.objectForKey("selectedRecord");

        actualiser();
                
		updateUI();

    }
    
    
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

}