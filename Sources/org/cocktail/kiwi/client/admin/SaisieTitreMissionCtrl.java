package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactoryTitreMission;
import org.cocktail.kiwi.client.metier.EOTitreMission;
import org.cocktail.kiwi.client.nibctrl.SaisieTitreMissionView;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieTitreMissionCtrl extends SaisieTitreMissionView
{
	private static SaisieTitreMissionCtrl sharedInstance;
	
	private 	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private 	EOEditingContext		ec;
		
	private EOTitreMission 			currentTitreMission;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieTitreMissionCtrl (EOEditingContext globalEc) {

		super(new JFrame(), true);

		ec = globalEc;
		
		buttonValider.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		buttonAnnuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});
		
		checkCtrlDatesOui.setSelected(true);
		checkPaimentOui.setSelected(true);
		checkPermanenteOui.setSelected(true);		
		checkLbudOui.setSelected(true);		
		getCheckCongeNon().setSelected(false);
		getCheckPlanningNon().setSelected(false);
		
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieTitreMissionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieTitreMissionCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		tfLibelle.setText("");
	
	}
	
	
	
	/**
	 * 
	 * Fournis = null ==> Saisie du fournisseur
	 * 
	 * @param fournis		Fournisseur associe au vehicule	
	 * @return
	 */
	public EOTitreMission ajouter()	{

		clearTextFields();

		modeModification = false;
				
		currentTitreMission = FactoryTitreMission.sharedInstance().creer(ec);
		
		tfLibelle.setText("");
		
		setVisible(true);

		return currentTitreMission;
		
	}
	
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifier(EOTitreMission titre)	{
					
		clearTextFields();

		currentTitreMission = titre;


		modeModification = true;

		tfLibelle.setText(currentTitreMission.titLibelle());
		
		checkCtrlDatesOui.setSelected(currentTitreMission.titCtrlDates().intValue() == 1);
		checkCtrlDatesNon.setSelected(currentTitreMission.titCtrlDates().intValue() == 0);

		checkPermanenteOui.setSelected(currentTitreMission.titPermanent().intValue() == 0);
		checkPermanenteNon.setSelected(currentTitreMission.titPermanent().intValue() == 0);

		checkPaimentOui.setSelected(currentTitreMission.titPaiement().intValue() == 1);
		checkPaiementNon.setSelected(currentTitreMission.titPaiement().intValue() == 0);

		getCheckCongeOui().setSelected(currentTitreMission.titCtrlConge().intValue() == 1);
		getCheckCongeNon().setSelected(currentTitreMission.titCtrlConge().intValue() == 0);

		getCheckPlanningOui().setSelected(currentTitreMission.titCtrlPlanning().intValue() == 1);
		getCheckPlanningNon().setSelected(currentTitreMission.titCtrlPlanning().intValue() == 0);

		getCheckLbudOui().setSelected(currentTitreMission.titSaisieLbud().intValue() == 1);
		getCheckLbudNon().setSelected(currentTitreMission.titSaisieLbud().intValue() == 0);

		setVisible(true);
		
		return (currentTitreMission != null);
	}
	
	
	
	/**
	 *
	 */
	private void valider()	{
		
		try {
			
			if (tfLibelle.getText().length() == 0) {
				
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez entrer un libellé pour le titre de mission");
				
				return;
			}

			currentTitreMission.setTitLibelle(tfLibelle.getText());
			
			if (checkPaimentOui.isSelected())
				currentTitreMission.setTitPaiement(new Integer(1));
			else
				currentTitreMission.setTitPaiement(new Integer(0));
			
			if (checkCtrlDatesOui.isSelected())
				currentTitreMission.setTitCtrlDates(new Integer(1));
			else
				currentTitreMission.setTitCtrlDates(new Integer(0));

			if (checkPermanenteOui.isSelected())
				currentTitreMission.setTitPermanent(new Integer(1));
			else
				currentTitreMission.setTitPermanent(new Integer(0));

			if (getCheckCongeOui().isSelected())
				currentTitreMission.setTitCtrlConge(new Integer(1));
			else
				currentTitreMission.setTitCtrlConge(new Integer(0));

			if (getCheckPlanningOui().isSelected())
				currentTitreMission.setTitCtrlPlanning(new Integer(1));
			else
				currentTitreMission.setTitCtrlPlanning(new Integer(0));

			if (getCheckLbudOui().isSelected())
				currentTitreMission.setTitSaisieLbud(new Integer(1));
			else
				currentTitreMission.setTitSaisieLbud(new Integer(0));

		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		dispose();
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentTitreMission);
			
		currentTitreMission = null;
		
		dispose();
		
	}
	
}