package org.cocktail.kiwi.client.admin;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOSegGroupeInfo;
import org.cocktail.kiwi.client.metier.EOSegTypeInfo;
import org.cocktail.kiwi.client.nibctrl.AdminBilletsView;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AdminBilletsCtrl {

	private static AdminBilletsCtrl sharedInstance;
	

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private EODisplayGroup eodGroupe, eodTypeGroupe;
	private AdminBilletsView myView;

	private ListenerGroupe listenerGroupe = new ListenerGroupe();
	private ListenerTypeGroupe listenerTypeGroupe = new ListenerTypeGroupe();

	private EOSegGroupeInfo currentGroupe;
	private EOSegTypeInfo currentTypeGroupe;
	private TypeGroupeRenderer	monRendererColor = new TypeGroupeRenderer();

	public AdminBilletsCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eodGroupe = new EODisplayGroup();
		eodTypeGroupe = new EODisplayGroup();

		myView = new AdminBilletsView(eodGroupe, eodTypeGroupe, monRendererColor);

		myView.getBtnAjouterGroupe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterGroupe();
			}
		});
		myView.getBtnModifierGroupe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierGroupe();
			}
		});
		myView.getBtnSupprimerGroupe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				invaliderGroupe();
			}
		});

		myView.getBtnAjouterTypeGroupe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterTypeGroupe();
			}
		});
		myView.getBtnModifierTypeGroupe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierTypeGroupe();
			}
		});
		myView.getBtnSupprimerTypeGroupe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				invaliderTypeGroupe();
			}
		});

		myView.getBtnAjouterGroupe().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getBtnModifierGroupe().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getBtnSupprimerGroupe().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		myView.getBtnAjouterTypeGroupe().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getBtnModifierTypeGroupe().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getBtnSupprimerTypeGroupe().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		myView.getMyEOTableGroupe().addListener(listenerGroupe);
		myView.getMyEOTableTypeGroupe().addListener(listenerTypeGroupe);

		NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EOSegTypeInfo.STI_ETAT_KEY, EOSortOrdering.CompareDescending));
		mySort.addObject(new EOSortOrdering(EOSegTypeInfo.STI_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		eodTypeGroupe.setSortOrderings(mySort);

	}

	public static AdminBilletsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new AdminBilletsCtrl(editingContext);
		return sharedInstance;
	}

	private void actualiser() {

		eodGroupe.setObjectArray(EOSegGroupeInfo.findGroupesInfo(ec));
		myView.getMyEOTableGroupe().updateData();

		updateUI();

	}

	public void open() {

		actualiser();
		myView.setVisible(true);

	}

	private void ajouterGroupe() {

		try {

			EOSegGroupeInfo myGroupe = SaisieGroupeCtrl.sharedInstance(ec).ajouter();

			if (myGroupe != null) {

				ec.saveChanges();
				actualiser();

			}

		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur d'ajout d'un nouveau type de remboursement\n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();
			ec.revert();
		}

		actualiser();

	}
	private void ajouterTypeGroupe() {

		try {
			EOSegTypeInfo myGroupe = SaisieTypeGroupeCtrl.sharedInstance(ec).ajouter(currentGroupe);
			if (myGroupe != null) {
				ec.saveChanges();
				actualiser();
			}
		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur d'ajout d'un nouveau type de remboursement\n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();
			ec.revert();
		}

		actualiser();

	}

	/**
	 * 
	 */
	private void modifierGroupe() {

		try {

			if (SaisieGroupeCtrl.sharedInstance(ec).modifier( currentGroupe)) {

				ec.saveChanges();
				myView.getMyEOTableGroupe().updateData();

			}
			else
				ec.revert();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur d'enregistrement du Titre de Mission !");
			ec.revert();
		}

	}
	private void modifierTypeGroupe() {

		try {

			if (SaisieTypeGroupeCtrl.sharedInstance(ec).modifier( currentTypeGroupe)) {
				ec.saveChanges();
				myView.getMyEOTableGroupe().updateData();
			}
			else
				ec.revert();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur d'enregistrement du Titre de Mission !");
			ec.revert();
		}

	}


	/**
	 * 
	 */
	private void invaliderGroupe() {

		try {

			if (currentGroupe.stgEtat().equals("VALIDE")) {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous l'invalidation du remboursement " + currentGroupe.stgLibelle() + " ?",
						"OUI", "NON"))
					currentGroupe.setStgEtat("INVALIDE");
			}
			else {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous la validation du remboursement " + currentGroupe.stgLibelle() + " ?",
						"OUI", "NON"))
					currentGroupe.setStgEtat("VALIDE");
			}

			ec.saveChanges();

			actualiser();

		}
		catch (Exception ex) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}

	}
	private void invaliderTypeGroupe() {

		try {

			if (currentTypeGroupe.stiEtat().equals("VALIDE")) {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous l'invalidation du remboursement " + currentTypeGroupe.stiLibelle() + " ?",
						"OUI", "NON"))
					currentTypeGroupe.setEstValide(false);
			}
			else {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous la validation du remboursement " + currentTypeGroupe.stiLibelle() + " ?",
						"OUI", "NON"))
					currentTypeGroupe.setEstValide(true);
			}

			ec.saveChanges();

			actualiser();

		}
		catch (Exception ex) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}

	}



	private class ListenerGroupe implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
				modifierGroupe();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			currentGroupe = (EOSegGroupeInfo)eodGroupe.selectedObject();
			eodTypeGroupe.setObjectArray(new NSArray());

			if (currentGroupe != null) {

				if (currentGroupe.stgEtat() != null && currentGroupe.stgEtat().equals("INVALIDE"))
					myView.getBtnSupprimerGroupe().setIcon(CocktailIcones.ICON_VALID);
				else
					myView.getBtnSupprimerGroupe().setIcon(CocktailIcones.ICON_DELETE);

				eodTypeGroupe.setObjectArray(EOSegTypeInfo.findTypesInfoForGroupe(ec, currentGroupe));

			}

			myView.getMyEOTableTypeGroupe().updateData();
			updateUI();

		}
	}
	private class ListenerTypeGroupe implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
				modifierTypeGroupe();
		}
		public void onSelectionChanged() {

			currentTypeGroupe = (EOSegTypeInfo)eodTypeGroupe.selectedObject();

			if (currentTypeGroupe != null) {
				if (currentTypeGroupe.stiEtat() != null && currentTypeGroupe.stiEtat().equals("INVALIDE"))
					myView.getBtnSupprimerTypeGroupe().setIcon(CocktailIcones.ICON_VALID);
				else
					myView.getBtnSupprimerTypeGroupe().setIcon(CocktailIcones.ICON_DELETE);
			}

			updateUI();

		}
	}


	private void updateUI() {

		myView.getBtnModifierGroupe().setEnabled(currentGroupe != null);
		myView.getBtnSupprimerGroupe().setEnabled(currentGroupe != null);

		myView.getBtnAjouterTypeGroupe().setEnabled(currentGroupe != null && currentGroupe.estValide());
		myView.getBtnModifierTypeGroupe().setEnabled(currentTypeGroupe != null && currentGroupe != null && currentGroupe.estValide());
		myView.getBtnSupprimerTypeGroupe().setEnabled(currentTypeGroupe != null && currentGroupe != null && currentGroupe.estValide());

	}
	
	private class TypeGroupeRenderer extends ZEOTableCellRenderer	{
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (isSelected)
				return leComposant;
	
			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOSegTypeInfo obj = (EOSegTypeInfo) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.estValide() == false)	{
				leComposant.setBackground(new Color(255,151,96));
			}
			else	{
					leComposant.setBackground(new Color(142,255,134));
			}
			
			return leComposant;
		}
	}

}
