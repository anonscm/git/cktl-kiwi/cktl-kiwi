/*
 * Created on 3 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderRembJournalier;
import org.cocktail.kiwi.client.finders.FinderRembZone;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EORembJournalier;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.nibctrl.TauxNuiteesView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TauxNuiteesCtrl{

	private static TauxNuiteesCtrl sharedInstance;
	private ApplicationClient NSApp;
	private	EOEditingContext ec;
	
	private TauxNuiteesView myView;
			
	public EODisplayGroup eod = new EODisplayGroup();
		
	private EORembJournalier currentRemboursement;
	
	/** 
	 * Constructeur 
	 */
	public TauxNuiteesCtrl(EOEditingContext editingContext)	{
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;

		myView = new TauxNuiteesView(new JFrame(), true, eod);
		
		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});

		NSArray mySort = new NSArray(new EOSortOrdering(EORembJournalier.REM_DATE_KEY, EOSortOrdering.CompareDescending));
		eod.setSortOrderings(mySort);
		
		myView.getBtnAjouter().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_TAUX_CHANCELLERIE));
		myView.getBtnModifier().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_TAUX_CHANCELLERIE));
		
		myView.getMyEOTable().addListener(new ListenerRemboursement());
		
		System.out.println("TauxNuiteesCtrl.TauxNuiteesCtrl() " + FinderRembZone.findZonesForRepasNuits(ec).size());
		CocktailUtilities.initPopupAvecListe(myView.getZones(), FinderRembZone.findZonesForRepasNuits(ec), false);
		myView.getZones().addActionListener(new PopupZonesListener());

	}	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TauxNuiteesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TauxNuiteesCtrl(editingContext);
		return sharedInstance;
	}
	
	private class ListenerRemboursement implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			modifier();
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			currentRemboursement =(EORembJournalier)eod.selectedObject();
			
		}
	}


	
	public void ajouter()	{

		try {

			EORembJournalier myRemb = SaisieTauxNuiteesCtrl.sharedInstance(ec).ajouter((EORembZone)myView.getZones().getSelectedItem());

			if (myRemb != null) {

				ec.saveChanges();
				actualiser();

			}

		}
		catch (Exception e) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur de sauvegarde du taux \n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();
		}

		actualiser();
		
	}
	
	public void modifier()	{
				

		try {

			if (SaisieTauxNuiteesCtrl.sharedInstance(ec).modifier( currentRemboursement )) {

				ec.saveChanges();
				myView.getMyEOTable().updateData();

			}
			else
				ec.revert();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur d'enregistrement du taux de remboursement !");
			ec.revert();
		}

	}
	
    private class PopupZonesListener implements ActionListener	{
        public PopupZonesListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	actualiser();
        
        }
    }

	/**
	 * 
	 */
	public void actualiser()	{
				
		eod.setObjectArray(FinderRembJournalier.findRemboursementsForZone(ec, (EORembZone)myView.getZones().getSelectedItem(), new Integer(1)));
		myView.getMyEOTable().updateData();
		
	}
	/**
	 * 
	 *
	 */
	public void open()	{
		
		actualiser();
		
		myView.setVisible(true);
		
	}
	
}