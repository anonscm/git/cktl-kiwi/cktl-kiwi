// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.kiwi.client.admin;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.finders.FinderRembZone;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EOWebmon;
import org.cocktail.kiwi.client.metier.EOWebpays;
import org.cocktail.kiwi.client.nibctrl.SaisieZoneView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.ModelePageSaisie;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieZoneCtrl extends ModelePageSaisie
{
	private static final long 		serialVersionUID = 0x7be9cfa4L;
	private static SaisieZoneCtrl 	sharedInstance;
	private SaisieZoneView 			myView;

	private EORembZone 				currentZone;

	public SaisieZoneCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieZoneView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getCheckEtranger().setEnabled(false);
		myView.getCheckEtranger().setSelected(false);

	}

	public static SaisieZoneCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieZoneCtrl(editingContext);
		return sharedInstance;
	}



	public EORembZone getCurrentZone() {
		return currentZone;
	}

	public void setCurrentZone(EORembZone currentZone) {
		this.currentZone = currentZone;
		updateDatas();
	}

	/**
	 * 
	 * @return
	 */
	public EORembZone ajouter()	{
		clearDatas();
		CocktailUtilities.initTextField(myView.getTfCode(), true, true);
		setModeCreation(true);
		setCurrentZone(EORembZone.creer(getEdc()));
		myView.setVisible(true);
		return getCurrentZone();
	}


	/**
	 * 
	 * @param uai
	 * @return
	 */
	public boolean modifier(EORembZone zone) {
		setCurrentZone(zone);
		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		setModeCreation(false);
		myView.setVisible(true);
		return getCurrentZone() != null;
	}

	/**
	 * 
	 * @throws ValidationException
	 */
	public void traitementsAvantValidation() {

		EORembZone zoneExistante = FinderRembZone.findForCode(getEdc(), CocktailUtilities.getTextFromField(myView.getTfCode()));
		if (zoneExistante != null && zoneExistante != getCurrentZone()) {
			throw new ValidationException("Une zone porte déjà ce code !");
		}

		getCurrentZone().setRemTaux(CocktailUtilities.getTextFromField(myView.getTfCode()));
		getCurrentZone().setRemLibelle(CocktailUtilities.getTextFromField(myView.getTfLibelle()));
		getCurrentZone().setEstEtranger(myView.getCheckEtranger().isSelected());

		// Creation du webpays correspondant
		if (isModeCreation()) {
			EOWebpays.creer(getEdc(), getCurrentZone(), EOWebmon.findDefault(getEdc()));
		}
	}

	@Override
	protected void clearDatas() {

		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");

	}

	@Override
	protected void updateDatas() {

		clearDatas();

		if (getCurrentZone() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentZone().remTaux());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentZone().remLibelle());
			myView.getCheckEtranger().setSelected(getCurrentZone().estEtranger());
		}

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		setCurrentZone(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}	

}
