package org.cocktail.kiwi.client.admin;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactoryWebMiss;
import org.cocktail.kiwi.client.finders.FinderWebMiss;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOWebmiss;
import org.cocktail.kiwi.client.metier.EOWebmon;
import org.cocktail.kiwi.client.metier.EOWebpays;
import org.cocktail.kiwi.client.select.PaysSelectCtrl;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class TauxIndemnitesCtrl  {

	private static TauxIndemnitesCtrl sharedInstance;
	private EOEditingContext ec;

	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	private	JTextField	filtrePays, filtreCodeMonnaie, filtreCodePays, filtreMonnaie;
	private	JTextField 	tfPays, tfDateDebut, tfDateFin, tfMontant;
	
	private	JButton		buttonAdd, buttonUpdate, buttonValidate, buttonCancel;
	private JRadioButton	temValide, temHistorise;
	
	private	CheckBoxListener checkBoxListener = new CheckBoxListener();

	private ActionAdd 	actionAdd = new ActionAdd();
	private ActionUpdate actionUpdate = new ActionUpdate();   

	protected ActionValidate 	actionValidate = new ActionValidate();
	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionClose 		actionClose = new ActionClose();
	protected ActionSelect 		actionSelect = new ActionSelect();

	protected ActionGetPays 	actionGetPays = new ActionGetPays();

	protected	ListenerWebmiss listenerWebmiss = new ListenerWebmiss();
	
	private ActionGetDateDebut actionGetDateDebut = new ActionGetDateDebut();
	private ActionGetDateFin actionGetDateFin = new ActionGetDateFin();
	private ActionDelDateFin actionDelDateFin = new ActionDelDateFin();

	private	boolean modeCreation;
	protected JPanel viewTable;

	private		EOWebmiss currentIndemnite;
	private		EOWebpays currentPays;

	

	/**
	 * 
	 *
	 */
	public TauxIndemnitesCtrl(EOEditingContext editingContext)	{
		
		super();
		ec = editingContext;
		
		gui_init();
		gui_initView();
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TauxIndemnitesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TauxIndemnitesCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 */
	private void gui_initTextFields()	{
		
        filtrePays = new JTextField("");
        filtrePays.setPreferredSize(new Dimension(250,18));
        filtrePays.setFont(new Font("Times", Font.PLAIN, 11));

        filtreMonnaie = new JTextField("");
        filtreMonnaie.setPreferredSize(new Dimension(80,18));
        filtreMonnaie.setFont(new Font("Times", Font.PLAIN, 11));

        filtreCodeMonnaie = new JTextField("");
        filtreCodeMonnaie.setPreferredSize(new Dimension(50,18));
        filtreCodeMonnaie.setFont(new Font("Times", Font.PLAIN, 11));

        filtreCodePays = new JTextField("");
        filtreCodePays.setPreferredSize(new Dimension(50,18));
        filtreCodePays.setFont(new Font("Times", Font.PLAIN, 11));

        filtrePays.getDocument().addDocumentListener(new ADocumentListener());
        filtreCodeMonnaie.getDocument().addDocumentListener(new ADocumentListener());
        filtreCodePays.getDocument().addDocumentListener(new ADocumentListener());
        filtreMonnaie.getDocument().addDocumentListener(new ADocumentListener());

        filtreCodePays = new JTextField("");
        filtreCodePays.setPreferredSize(new Dimension(50,18));
        filtreCodePays.setFont(new Font("Times", Font.PLAIN, 11));

        tfPays = new JTextField("");
        tfPays.setPreferredSize(new Dimension(225,18));
        tfPays.setFont(new Font("Times", Font.PLAIN, 11));

        tfMontant = new JTextField("");
        tfMontant.setPreferredSize(new Dimension(50,18));
        tfMontant.setHorizontalAlignment(JTextField.RIGHT);
        tfMontant.setFont(new Font("Times", Font.PLAIN, 11));

        tfDateDebut = new JTextField("");
        tfDateDebut.setPreferredSize(new Dimension(75,18));
        tfDateDebut.setHorizontalAlignment(JTextField.CENTER);
        tfDateDebut.setFont(new Font("Times", Font.PLAIN, 11));
        CocktailUtilities.initTextField(tfDateDebut, false, false);

        tfDateFin = new JTextField("");
        tfDateFin.setPreferredSize(new Dimension(75,18));
        tfDateFin.setHorizontalAlignment(JTextField.CENTER);
        tfDateFin.setFont(new Font("Times", Font.PLAIN, 11));
        CocktailUtilities.initTextField(tfDateFin, false, false);

        tfDateDebut.addFocusListener(new ListenerTextFieldDebut());
        tfDateDebut.addActionListener(new ActionListenerDebut());
		
		tfDateFin.addFocusListener(new ListenerTextFieldFin());
		tfDateFin.addActionListener(new ActionListenerFin());

	}
	
	
	/**
	 * 
	 * @param yn
	 */
	private void setSaisieEnabled(boolean yn)	{

		actionValidate.setEnabled(yn);
		actionCancel.setEnabled(yn);

		actionAdd.setEnabled(!yn);
		actionUpdate.setEnabled(!yn);

		actionGetDateDebut.setEnabled(yn);
		actionGetDateFin.setEnabled(yn);
		actionDelDateFin.setEnabled(yn);

		myEOTable.setEnabled(!yn);
		
		CocktailUtilities.initTextField(tfPays, false, false);
		CocktailUtilities.initTextField(tfMontant, false, yn);
		
	}
	
	
	/**
	 * 
	 *
	 */
	private void gui_initButtons()	{
		
		buttonAdd = new JButton(actionAdd);
		buttonAdd.setPreferredSize(new Dimension (22,22));
		buttonAdd.setToolTipText("Ajout d'une nouvelle rubrique");
		
		buttonAdd.setVisible(false);//NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		buttonAdd.setEnabled(false);

		buttonUpdate = new JButton(actionUpdate);
		buttonUpdate.setPreferredSize(new Dimension (22,22));
		buttonUpdate.setToolTipText("Modification de la rubrique");

		buttonUpdate.setVisible(false);//NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		buttonUpdate.setEnabled(false);

		buttonValidate = new JButton(actionValidate);
		buttonValidate.setPreferredSize(new Dimension (22,22));
		buttonValidate.setToolTipText("Modification de la rubrique");

		buttonCancel = new JButton(actionCancel);
		buttonCancel.setPreferredSize(new Dimension (22,22));
		buttonCancel.setToolTipText("Modification de la rubrique");
		
	}

	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildUpdatePanel()	{

		JPanel updatePanel = new JPanel(new BorderLayout());

		JButton buttonGetDateDebut = new JButton(actionGetDateDebut);		
		buttonGetDateDebut.setPreferredSize(new Dimension(20,20));
		JButton buttonGetDateFin = new JButton(actionGetDateFin);
		buttonGetDateFin.setPreferredSize(new Dimension(20,20));
		JButton buttonDelDateFin = new JButton(actionDelDateFin);
		buttonDelDateFin.setPreferredSize(new Dimension(20,20));

		JLabel labelPays = new JLabel("Pays : ");
		JLabel labelDateDebut = new JLabel("Début : ");
		JLabel labelDateFin = new JLabel("Fin : ");
		JLabel labelMontant = new JLabel("Montant : ");
		
		JPanel flowPanel = new JPanel(new FlowLayout());
		
		flowPanel.add(labelPays);
		flowPanel.add(tfPays);
		flowPanel.add(labelDateDebut);
		flowPanel.add(tfDateDebut);
		flowPanel.add(buttonGetDateDebut);
		flowPanel.add(labelDateFin);
		flowPanel.add(tfDateFin);
		flowPanel.add(buttonGetDateFin);
		flowPanel.add(buttonDelDateFin);
		flowPanel.add(labelMontant);
		flowPanel.add(tfMontant);

		flowPanel.add(buttonValidate);
		flowPanel.add(buttonCancel);

		updatePanel.add(flowPanel, BorderLayout.CENTER);

		return updatePanel;
	}

	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildSouthPanel() {
		
		JPanel panel = new JPanel(new BorderLayout());
	
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 115, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		panel.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelButtons, BorderLayout.EAST);


		return panel;
	}
	
	
	private JPanel gui_buildNorthPanel() {
		
		JPanel panel = new JPanel(new BorderLayout());
		
		temValide = new JRadioButton("Valides");
		temValide.setSelected(true);
		temValide.addActionListener(checkBoxListener);
		
		temHistorise = new JRadioButton("Historisées");
		temHistorise.addActionListener(checkBoxListener);

		ButtonGroup matriceValideHisto = new ButtonGroup();
		matriceValideHisto.add(temValide);
		matriceValideHisto.add(temHistorise);

		JPanel filtrePanel = new JPanel(new FlowLayout());
		filtrePanel.add(filtrePays);
		filtrePanel.add(filtreMonnaie);
		filtrePanel.add(filtreCodeMonnaie);
		filtrePanel.add(filtreCodePays);
		filtrePanel.add(temValide);
		filtrePanel.add(temHistorise);
		
		panel.add(filtrePanel, BorderLayout.WEST);
		panel.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

		return panel;
	}
	
	/** 
	 * Listener sur la coche temPayeLocale. Permet de actif le bouton permettant la saisie des infos paye 
	 */
	private class CheckBoxListener extends AbstractAction	{
		public CheckBoxListener ()	{
			super();
		}
		
		public void actionPerformed(ActionEvent anAction)	{
					
			filter();
			
		}
	}
	
	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildCenterPanel()	{

		JPanel panel = new JPanel(new BorderLayout());

		ArrayList arrayList = new ArrayList();
		arrayList.add(buttonAdd);
		arrayList.add(buttonUpdate);
		
		JPanel eastPanel = new JPanel(new BorderLayout());
		eastPanel.setBorder(BorderFactory.createEmptyBorder(2,5,2,2));
		eastPanel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		eastPanel.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.NORTH);

		panel.add(viewTable, BorderLayout.CENTER);
		panel.add(eastPanel, BorderLayout.EAST);
		panel.add(gui_buildUpdatePanel(), BorderLayout.SOUTH);

		return panel;
	}
	
	
	/**
	 * 
	 *
	 */
	private void gui_initView()	{
		
        mainWindow = new JDialog(mainFrame, "Missions à l'étranger - Tarif des indemnités ", true);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

        gui_initTextFields();
        
        gui_initButtons();

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(750, 500));
		mainView.add(gui_buildNorthPanel(), BorderLayout.NORTH);
		mainView.add(gui_buildCenterPanel(), BorderLayout.CENTER);
		mainView.add(gui_buildSouthPanel(), BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
		
		setSaisieEnabled(false);
	}
		

	public void open()	{

		updateData();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();

	}

	
	/**
	 * 
	 *
	 */
	public void updateData()	{
		
		if (eod.displayedObjects().count() == 0)
			eod.setObjectArray(FinderWebMiss.findWebsMiss(ec));
		
		filter();
	
		myEOTable.updateData();
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void gui_init() {
		
		eod = new EODisplayGroup();
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering("webpays.wpaLibelle", EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering("wmiFin", EOSortOrdering.CompareDescending));

		eod.setSortOrderings(mySort);
		
        viewTable = new JPanel();
		
		initTableModel();
		initTable();
				
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		
		myEOTable.addListener(listenerWebmiss);

	}
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod,EOWebmiss.WEBPAYS_KEY+".libelleCourt", "Pays", 225);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOWebmiss.WEBMON_KEY+"."+EOWebmon.WMO_LIBELLE_KEY, "Monnaie", 125);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOWebmiss.WEBMON_KEY+"."+EOWebmiss.WMO_CODE_KEY, "Code M", 50);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOWebmiss.WPA_CODE_KEY, "Code P", 50);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOWebmiss.WMI_DEBUT_KEY, "Début", 80);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay((DateFormat)new SimpleDateFormat("dd/MM/yyyy"));
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOWebmiss.WMI_FIN_KEY, "Fin", 80);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay((DateFormat)new SimpleDateFormat("dd/MM/yyyy"));
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOWebmiss.WMI_GROUPE1_KEY, "Montant", 60);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

	    public ActionSelect() {
            super("Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	mainWindow.dispose();
        }  
	} 
	
    
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CLOSE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
        	myTableModel.fireTableDataChanged();
        	
        	if (ec.updatedObjects().count() > 0)	{
        		try {
        		
        			if (EODialogs.runConfirmOperationDialog("Attention",
        					"Voulez vous enregistrer les modifications apportées ?",
        					"OUI", "NON"))  	       			
        				ec.saveChanges();
        		}
        		catch (Exception ex)	{
        			ex.printStackTrace();
        			EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement des modifications !");
        		}
        	}
        	
        	mainWindow.dispose();
        	
        }  
	} 
	
	
/**
 * 
 * @author cpinsard
 *
 */
	   private class ListenerWebmiss implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP)) {
//				SaisieTauxIndemnites.sharedInstance(ec).updateTauxIndemnite(currentIndemnite);
//				actionUpdate.actionPerformed(null);
				myEOTable.updateData();
			}
			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		
			currentIndemnite = (EOWebmiss)eod.selectedObject();
			currentPays = null;
			
			if (currentIndemnite != null) {
				
				currentPays = currentIndemnite.webpays();
				
				CocktailUtilities.setTextToField(tfPays, currentIndemnite.webpays().wpaLibelle());

				tfDateDebut.setText(DateCtrl.dateToString(currentIndemnite.wmiDebut()));

				if (currentIndemnite.wmiFin() != null)
					tfDateFin.setText(DateCtrl.dateToString(currentIndemnite.wmiFin()));
				else
					tfDateFin.setText("");
					
				tfMontant.setText(currentIndemnite.wmiGroupe1().toString());
			}

		}
	   }	   
	   
		/**
		 * 
		 * @return
		 */
	    public EOQualifier getFilterQualifier()	{
	        NSMutableArray mesQualifiers = new NSMutableArray();
	        
	        if (!StringCtrl.chaineVide(filtrePays.getText()))	{
	            NSArray args = new NSArray("*"+filtrePays.getText()+"*");
	            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("webpays.wpaLibelle caseInsensitiveLike %@",args));
	        }

	        if (!StringCtrl.chaineVide(filtreCodeMonnaie.getText()))	{
	            NSArray args = new NSArray("*"+filtreCodeMonnaie.getText()+"*");
	            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("webmon.wmoCode caseInsensitiveLike %@",args));
	        }
	        
	        if (!StringCtrl.chaineVide(filtreCodePays.getText()))	{
	            NSArray args = new NSArray("*"+filtreCodePays.getText()+"*");
	            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("webpays.wpaCode caseInsensitiveLike %@",args));
	        }

	        if (!StringCtrl.chaineVide(filtreMonnaie.getText()))	{
	            NSArray args = new NSArray("*"+filtreMonnaie.getText()+"*");
	            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("webmon.wmoLibelle caseInsensitiveLike %@",args));
	        }
	        
	        // indemnites en cours ou historisees
	        if (temValide.isSelected()) {
	        	mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("wmiFin >= %@ or wmiFin = nil", new NSArray(new NSTimestamp())));
	        }
	        else {
	        	mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("wmiFin < %@", new NSArray(new NSTimestamp())));	        	
	        }
	        

	        return new EOAndQualifier(mesQualifiers);        
	    }
	    
	    
	    /** 
	    *
	    */
	   public void filter()	{
	       
	       eod.setQualifier(getFilterQualifier());
	       eod.updateDisplayedObjects();              
	       myEOTable.updateData();

	   }
	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
	   
	   
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private  final class ActionValidate extends AbstractAction {
			
			public ActionValidate() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
			}
			
			public void actionPerformed(ActionEvent e) {

				NSTimestamp saisieDebut = DateCtrl.stringToDate(tfDateDebut.getText());
				NSTimestamp saisieFin = DateCtrl.stringToDate(tfDateFin.getText());
				
				EOWebmiss webMissExistant = null;
				
				if (modeCreation) {
					webMissExistant = FinderWebMiss.findWebMissForPaysAndPeriode(ec, currentPays, saisieDebut, saisieFin);
					if (webMissExistant != null) {
						MsgPanel.sharedInstance().runInformationDialog("ERREUR","Une période est déjà définie pour ce pays et ces dates ! ("
								+ DateCtrl.dateToString(webMissExistant.wmiDebut())+"==>"+DateCtrl.dateToString(webMissExistant.wmiFin()) + " )!");
						return;
					}
				}

				if (tfMontant.getText().length() <= 0) {
					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez entrer un montant pour cette indemnité !");
					return;
				}

				if (tfDateDebut.getText().length() <= 0) {
					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez entrer une date de début de validité !");
					return;
				}
				
				try {
					
					// Test des dates et mise a jour de l'historique
										
					currentIndemnite.setWmiDebut(DateCtrl.stringToDate(tfDateDebut.getText()));

					if (tfDateFin.getText().length() > 0)
						currentIndemnite.setWmiFin(DateCtrl.stringToDate(tfDateFin.getText()));
					else
						currentIndemnite.setWmiFin(null);

					currentIndemnite.setWmiGroupe1(new BigDecimal(tfMontant.getText()));
					
					ec.saveChanges();
					
					myEOTable.updateUI();
					
					if (modeCreation) {
						eod.setObjectArray(new NSArray());
						updateData();
						filter();
					}
					
				}
				catch (Exception ex) {
					ex.printStackTrace();
					return;
				}
				
				setSaisieEnabled(false);

			}  
		}
		
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private  final class ActionCancel extends AbstractAction {
			
			public ActionCancel() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CANCEL);
			}
			
			public void actionPerformed(ActionEvent e) {
								
				listenerWebmiss.onSelectionChanged();
				setSaisieEnabled(false);

			}  
		}
		
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private  final class ActionAdd extends AbstractAction {
			
			public ActionAdd() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_ADD);
			}
			
			public void actionPerformed(ActionEvent e) {
						
				modeCreation= true;
				
				currentIndemnite = FactoryWebMiss.sharedInstance().creerWebMiss(ec, currentPays, currentIndemnite.webmon());
				tfMontant.setText("");
				tfDateDebut.setText("");
				tfDateFin.setText("");

				setSaisieEnabled(true);

			}  
		}
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private  final class ActionUpdate extends AbstractAction {
			
			public ActionUpdate() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_UPDATE);
			}
			
			public void actionPerformed(ActionEvent e) {
											
				modeCreation = false;
				setSaisieEnabled(true);
				
			}  
		}
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private final class ActionGetDateDebut extends AbstractAction {
			
			public ActionGetDateDebut() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CALENDAR);
			}
			
			public void actionPerformed(ActionEvent e) {
				
				CocktailUtilities.setMyTextField(tfDateDebut);
				CocktailUtilities.showDatePickerPanel(new Dialog(NSApp.getMainWindow()));

			}  
		}
		
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private final class ActionDelDateFin extends AbstractAction {
			
			public ActionDelDateFin() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_DELETE);
			}
			
			public void actionPerformed(ActionEvent e) {
				

				tfDateFin.setText("");
				
				
			}  
		}
		
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private final class ActionGetPays extends AbstractAction {
			
			public ActionGetPays() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_SELECT_16);
			}
			
			public void actionPerformed(ActionEvent e) {
				
				EOWebpays pays = PaysSelectCtrl.sharedInstance(ec).getPays(null);
				
				if (pays != null) {
					currentPays = pays;
					CocktailUtilities.setTextToField(tfPays, currentPays.wpaLibelle());
				}
			}  
		}
		
		
		/**
		 * 
		 * @author cpinsard
		 *
		 */
		private final class ActionGetDateFin extends AbstractAction {
			
			public ActionGetDateFin() {
				super();
				this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CALENDAR);
			}
			
			public void actionPerformed(ActionEvent e) {
				
				CocktailUtilities.setMyTextField(tfDateFin);
				CocktailUtilities.showDatePickerPanel(new Dialog(NSApp.getMainWindow()));
				
			}  
		}
		
				
		/** 
		 * Classe d'ecoute sur le debut de contrat de travail.
		 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
		 */
		private class ActionListenerDebut implements ActionListener	{
			public void actionPerformed(ActionEvent e)	{
				if ("".equals(tfDateDebut.getText()))	return;
				
				String myDate = DateCtrl.dateCompletion(tfDateDebut.getText());
				if ("".equals(myDate))	{
					MsgPanel.sharedInstance().runInformationDialog("Date non valide","La date de début de contrat n'est pas valide !");
					tfDateDebut.selectAll();
				}
				else	{
					tfDateDebut.setText(myDate);
					tfDateFin.requestFocus();
				}
			}
		}
		
		
		/** 
		 * Classe d'ecoute sur la date de fin de contrat de travail.
		 * Permet d'effectuer la completion de cette date 
		 */
		private class ActionListenerFin implements ActionListener	{
			public void actionPerformed(ActionEvent e)	{
				if ("".equals(tfDateFin.getText()))	return;
				
				String myDate = DateCtrl.dateCompletion(tfDateFin.getText());
				if ("".equals(myDate))	{
					tfDateFin.selectAll();
					MsgPanel.sharedInstance().runInformationDialog("Date non valide","La date de fin de contrat n'est pas valide !");
				}
				else
					tfDateFin.setText(myDate);
			}
		}
		
		/** 
		 * Classe d'ecoute sur le debut de contrat de travail.
		 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
		 */
		private class ListenerTextFieldDebut implements FocusListener	{
			public void focusGained(FocusEvent e) 	{}
			
			public void focusLost(FocusEvent e)	{
				if ("".equals(tfDateDebut.getText()))	return;
				
				String myDate = DateCtrl.dateCompletion(tfDateDebut.getText());
				if ("".equals(myDate))	{
					MsgPanel.sharedInstance().runInformationDialog("Date Invalide","La date de début de contrat n'est pas valide !");
					tfDateDebut.selectAll();
				}
				else
					tfDateDebut.setText(myDate);
			}
		}
		
		/** 
		 * Classe d'ecoute sur le debut de contrat de travail.
		 * Permet d'effectuer la completion de cette date 
		 */
		private class ListenerTextFieldFin implements FocusListener	{
			public void focusGained(FocusEvent e) 	{}
			
			public void focusLost(FocusEvent e)	{
				if ("".equals(tfDateFin.getText()))	return;
				
				String myDate = DateCtrl.dateCompletion(tfDateFin.getText());
				if ("".equals(myDate))	{
					tfDateFin.selectAll();
					MsgPanel.sharedInstance().runInformationDialog("Date Invalide","La date de fin de contrat n'est pas valide !");
				}
				else
					tfDateFin.setText(myDate);
			}
		}	

}
