package org.cocktail.kiwi.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.nibctrl.EngagementsView;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class EngagementsCtrl extends EngagementsView {

	private static EngagementsCtrl sharedInstance;
	
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	public EngagementsCtrl(EOEditingContext editingContext) {
		
		super();

		ec = editingContext;
	
		buttonSolder.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				solder();
			}
		});
		
		setExercices(NSApp.listeExercices());
		exercices.setSelectedItem(NSApp.getExerciceCourant());

		exercices.addActionListener(new PopupExerciceListener());
		
		myEOTable.addListener(new ListenerEngagement());

		getTfFiltreMisNumero().getDocument().addDocumentListener(new ADocumentListener());
		getTfFiltreNoEngagement().getDocument().addDocumentListener(new ADocumentListener());
		getTfFiltreMissionnaire().getDocument().addDocumentListener(new ADocumentListener());

	}

	
	public static EngagementsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new EngagementsCtrl(editingContext);
		return sharedInstance;
	}
	
	
	private String getSqlQualifier() {
		
		String sqlQualifier = "";
		
		sqlQualifier = sqlQualifier + 
		"SELECT " +
			" m.mis_ordre MIS_ORDRE, mpe.mpe_etat MPE_ETAT, mpe.mpe_ordre MPE_ORDRE, nom_usuel NOM, prenom PRENOM, m.mis_numero MIS_NUMERO, " +
			" t.tit_libelle MIS_TITRE, " + 
			" to_char(m.mis_fin, 'dd/mm/yyyy') FIN, to_char(m.mis_debut, 'dd/mm/yyyy') DEB, " +
			" m.mis_etat MIS_ETAT, e.eng_id ENG_ID	, mp.mip_montant_total MIS_COUT, " +  
			" mp.mip_etat ETAT_BUD, e.eng_numero ENG_NUMERO, e.eng_montant_budgetaire ENG_MONTANT, " +
			" e.eng_montant_budgetaire_reste ENG_RESTE, m.exe_ordre MIS_EXER, e.exe_ordre ENG_EXER, " +
			" (select sum(dep_montant_budgetaire) from jefy_depense.depense_budget where eng_id = e.eng_id) SUM_DEP "; 
		
		sqlQualifier = sqlQualifier + 
		" FROM " + 
			" jefy_mission.mission m, jefy_mission.mission_paiement mp, jefy_mission.mission_paiement_engage mpe, " + 
			" jefy_mission.titre_mission t , " + 
			" jefy_depense.engage_budget e, grhum.fournis_ulr f, grhum.individu_ulr i ";

		sqlQualifier = sqlQualifier +
		" WHERE " + 
			" m.mis_ordre = mp.mis_ordre and mp.mip_ordre = mpe.mip_ordre " + 
			" and m.tit_ordre = t.tit_ordre " + 
			" and e.exe_ordre = " + ((EOExercice)exercices.getSelectedItem()).exeExercice() + 
			" and mpe.eng_id = e.eng_id (+)" +
			" and e.eng_montant_budgetaire_reste > 0" + 
			" and m.fou_ordre = f.fou_ordre and f.pers_id = i.pers_id ";
		//+" and m.mis_etat in( '" + EOMission.ETAT_VALIDE + "','" + EOMission.ETAT_LIQUIDE+"','"+EOMission.ETAT_PAYE+"') ";

		sqlQualifier = sqlQualifier + 
		" UNION ALL " + 
			" SELECT 0 , ' ',0, ' ', ' ', to_number(REPLACE(ENG_LIBELLE, 'MISSION No ','')) MIS_NUMERO, " +
			" ' ', " + 
			" ' ',' ', ' ', e.eng_id, 0, ' ', eng_numero, eng_montant_budgetaire, " +
			" eng_montant_budgetaire_reste , null, e.exe_ordre, null " +
			" FROM jefy_depense.engage_budget e, jefy_admin.type_application ta " + 
			" WHERE e.tyap_id = ta.tyap_id " +
			" and ta.tyap_libelle = 'KIWI' " +
			" and e.eng_id not in (select eng_id from jefy_mission.mission_paiement_engage where eng_id is not null) " +
			" and e.eng_montant_budgetaire_reste > 0" + 
			" and e.exe_ordre =  " + ((EOExercice)exercices.getSelectedItem()).exeExercice() ;
		
		System.out.println("EngagementsCtrl.getSqlQualifier() " + sqlQualifier);
		
//		sqlQualifier = sqlQualifier + 
//		" ORDER BY " + 
//			" mis_numero desc ";
				
		return sqlQualifier;
		
	}
	
	
	
	/**
	 * 
	 */
	private void actualiser() {
		
		NSArray retourRequete = ServerProxy.clientSideRequestSqlQuery(ec, getSqlQualifier());
		eod.setObjectArray(retourRequete);

		filter();
				
	}
	
	/**
	 * 
	 */
	public void open() {
		
		actualiser();
		
		show();
		
	}
	
	
	/**
	 * 
	 */
	private void solder() {

    	if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous solder les engagements sélectionnés ?",
				"OUI", "NON"))
    	     return;   	
   	
		try {
			
			// On detruit tous les anciens engagements
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
			
			for (int i=0;i<eod.selectedObjects().count();i++) {

				NSDictionary myDico = (NSDictionary)eod.selectedObjects().objectAtIndex(i);

				if (myDico.objectForKey("MPE_ORDRE") != null && ((BigDecimal)myDico.objectForKey("MPE_ORDRE")).intValue() > 0 ) {
					
					Number mpeOrdre = (Number) myDico.objectForKey("MPE_ORDRE");

					String msg = ServerProxy.desengagerMissionPartiel(ec, mpeOrdre, utlOrdre);
					boolean success = msg.equals("OK");
					if (!success) {
						MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
						return;
					}
				}
				else {

					Number engId = (Number) myDico.objectForKey("ENG_ID");

					String msg = ServerProxy.solderEngage(ec, engId, utlOrdre);
					boolean success = msg.equals("OK");
					if (!success) {
						MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
						return;
					}

				}

			}
		}
		catch (Exception e)	{
			e.printStackTrace();
		}
	
		actualiser();
		
	}
	
	/**
	 * 
	 *
	 */
	private void filter()	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		if (!StringCtrl.chaineVide(getTfFiltreMisNumero().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("MIS_NUMERO caseInsensitiveLike %@",new NSArray("*"+getTfFiltreMisNumero().getText()+"*")));
		
		if (!StringCtrl.chaineVide(getTfFiltreNoEngagement().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ENG_NUMERO caseInsensitiveLike %@",new NSArray("*"+getTfFiltreNoEngagement().getText()+"*")));

		if (!StringCtrl.chaineVide(getTfFiltreMissionnaire().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("NOM caseInsensitiveLike %@",new NSArray("*"+getTfFiltreMissionnaire().getText()+"*")));

		eod.setQualifier(new EOAndQualifier(mesQualifiers));

		eod.updateDisplayedObjects();
		myEOTable.updateData();
		
		lblNbVehicules.setText(eod.displayedObjects().count() + " Engagements");

	}
	
	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupExerciceListener implements ActionListener	{
        public PopupExerciceListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	actualiser();
        
        }
    }
	
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert � filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	
	private class ListenerEngagement implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
	
//			Recuperation de la mission
			NSDictionary dico = (NSDictionary)eod.selectedObject();
			EOMission selectedMission = EOMission.findMissionForKey(ec, (Number)dico.objectForKey("MIS_ORDRE"));
			
			EnteteMission.sharedInstance(ec).setMission(selectedMission);
			
			dispose();
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			updateUI();
			
		}
	}
	
	
	public void updateUI() {
				
	}
	
}
