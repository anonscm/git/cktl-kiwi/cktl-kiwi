package org.cocktail.kiwi.client.admin;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.nibctrl.ZonesView;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;

public class ZonesCtrl {

	private static ZonesCtrl sharedInstance;

	private EODisplayGroup eod;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext edc;

	private ZonesView 	myView;
	private EORembZone 	currentZone;
	private ZoneRenderer	monRendererColor = new ZoneRenderer();

	public ZonesCtrl(EOEditingContext edc) {

		super();

		this.edc = edc;
		eod = new EODisplayGroup();
		myView = new ZonesView(new JFrame(), true, eod, monRendererColor);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {myView.setVisible(false);}
		});
		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {ajouter();}
		});

		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {modifier();}
		});

		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {invalider();}
		});

		myView.getBtnAjouter().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getBtnModifier().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));
		myView.getBtnSupprimer().setVisible(NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP));

		myView.getMyEOTable().addListener(new ListenerZone());

	}


	public static ZonesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new ZonesCtrl(editingContext);
		return sharedInstance;
	}



	public EORembZone getCurrentZone() {
		return currentZone;
	}


	public void setCurrentZone(EORembZone currentZone) {
		this.currentZone = currentZone;
		if (currentZone != null) {
			if (currentZone.estValide())
				myView.getBtnSupprimer().setIcon(CocktailIcones.ICON_DELETE);
			else
				myView.getBtnSupprimer().setIcon(CocktailIcones.ICON_VALID);
		}
	}

	/**
	 * 
	 */
	private void actualiser() {

		eod.setObjectArray(EORembZone.find(edc));
		myView.getMyEOTable().updateData();

	}

	/**
	 * 
	 */
	public void open() {

		actualiser();
		myView.setVisible(true);

	}


	/**
	 * 
	 */
	private void ajouter() {

		SaisieZoneCtrl.sharedInstance(edc).ajouter();
		actualiser();

	}

	/**
	 * 
	 */
	private void modifier() {


		try {

			if (SaisieZoneCtrl.sharedInstance(edc).modifier( getCurrentZone() )) {

				edc.saveChanges();
				myView.getMyEOTable().updateData();

			}
			else
				edc.revert();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur d'enregistrement de la zone !");
			edc.revert();
		}

	}


	/**
	 * 
	 */
	private void invalider() {

		try {

			if (getCurrentZone().estValide()) {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous l'invalidation de la zone " + getCurrentZone().remLibelle() + " ?",
						"OUI", "NON"))
					getCurrentZone().setEstValide(false);
			}
			else {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez vous la validation de la zone " + getCurrentZone().remLibelle() + " ?",
						"OUI", "NON"))
					getCurrentZone().setEstValide(true);
			}

			edc.saveChanges();
			actualiser();

		}
		catch (Exception ex) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerZone implements ZEOTableListener {

		public void onDbClick() {
			if (NSApp.hasFonction(EOFonction.ID_FCT_PARAMS_APP))
				modifier();
		}
		public void onSelectionChanged() {

			setCurrentZone((EORembZone)eod.selectedObject());
			updateUI();

		}
	}


	/**
	 * 
	 */
	public void updateUI() {
		myView.getBtnModifier().setEnabled(getCurrentZone() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentZone() != null);	
	}

	private class ZoneRenderer extends ZEOTableCellRenderer	{
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EORembZone obj = (EORembZone) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.estValide())	{
				leComposant.setBackground(new Color(142,255,134));
			}
			else	{
				leComposant.setBackground(new Color(255,151,96));
			}

			return leComposant;
		}
	}
}