package org.cocktail.kiwi.client;

import javax.swing.ImageIcon;

import org.cocktail.kiwi.common.utilities.CocktailConstantes;

public class ApplicationIcones extends CocktailConstantes {

    public static final ImageIcon ICON_VOITURE 		= (ImageIcon)resourceBundle.getObject("app_voiture");
    public static final ImageIcon ICON_SNCF			= (ImageIcon)resourceBundle.getObject("app_sncf");
    public static final ImageIcon ICON_LOUPE_32         = (ImageIcon)resourceBundle.getObject("cktl_loupe_32");

    public static final ImageIcon ICON_NUIT 		= (ImageIcon)resourceBundle.getObject("app_nuit");
    public static final ImageIcon ICON_REPAS 		= (ImageIcon)resourceBundle.getObject("app_repas");
    public static final ImageIcon ICON_TRANSPORT 	= (ImageIcon)resourceBundle.getObject("app_transport");
    public static final ImageIcon ICON_INDEMNITE	= (ImageIcon)resourceBundle.getObject("app_indemnites");
    public static final ImageIcon ICON_PARAMS_16_2     = (ImageIcon)resourceBundle.getObject("cktl_params_16_2");
    public static final ImageIcon ICON_MAIL_32                 = (ImageIcon)resourceBundle.getObject("cktl_mail_32");

    public static final ImageIcon ICON_ALERT_INFO     = (ImageIcon)resourceBundle.getObject("cktl_msg_alert-exclam");
    public static final ImageIcon ICON_ALERT_ERROR     = (ImageIcon)resourceBundle.getObject("cktl_msg_alert-error");
    public static final ImageIcon ICON_ALERT_OK     = (ImageIcon)resourceBundle.getObject("cktl_msg_alert-message");
    public static final ImageIcon ICON_IMPORT         = (ImageIcon)resourceBundle.getObject("cktl_download_16");

    public static final ImageIcon ICON_RELOAD 			= (ImageIcon)resourceBundle.getObject("app_reload");
    public static final ImageIcon ICON_REFRESH_32             = (ImageIcon)resourceBundle.getObject("cktl_refresh_32");
    public static final ImageIcon ICON_CORBEILLE_VIDE_32     = (ImageIcon)resourceBundle.getObject("cktl_corbeille_vide_32");
    public static final ImageIcon ICON_CLOSE_32             = (ImageIcon)resourceBundle.getObject("cktl_close_32");

}
