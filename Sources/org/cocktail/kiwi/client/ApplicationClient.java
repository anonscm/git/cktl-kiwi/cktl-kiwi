package org.cocktail.kiwi.client;

import java.awt.Component;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOGrhumParametres;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.common.CktlDockClient;
import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.client.finders.FinderGrhumParametres;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOIndemnite;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EONuits;
import org.cocktail.kiwi.client.metier.EORepas;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTransports;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.FileHandler;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StreamCtrl;
import org.cocktail.kiwi.common.utilities.XLogs;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOSimpleWindowController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;

public class ApplicationClient extends ApplicationCocktail {

	public static int USED_DECIMALES = 2;

	public static final Number APPLICATION_ID = new Integer(7);
	static final EODistributedObjectStore objectStore = (EODistributedObjectStore) EOEditingContext.defaultParentObjectStore();

	public static final String WINDOWS_FILE_PATH_SEPARATOR = "\\" + "\\";
	public static final String APPLICATION_FILE_PATH_SEPARATOR = "\\";

	// public static final String WINDOWS_EXEC = "cmd start ";
	// public static final String WINDOWS2000_EXEC = "cmd start ";

	// Chaines correspondant a System.getProperties().getProperty(os.name)
	public static final String MAC_OS_X_OS_NAME = "Mac OS X";

	public static final String MINJREVERSION = "1.5";
	public JLabel messageAttente;

	public GregorianCalendar 	myCalendar;
	private EOExercice 			currentExercice;
	protected		NSArray 	userOrgans, userFonctions;
	MyByteArrayOutputStream 	redirectedOutStream, redirectedErrStream;
	private static Integer 		usedDecimales;
	public String 				temporaryDir, osName;

	private NSArray exercices = new NSArray();

	public ApplicationClient() {
		super();
		setTYAPSTRID("KIWI");
	}
	
	/**
	 * 
	 */
	public void finishInitialization() {

		setQuitsOnLastWindowClose(false);

		try {
			super.finishInitialization();
			try {
				compareJarVersionsClientAndServer();
			} catch (Exception e) {
				e.printStackTrace();
				this.fenetreDeDialogueInformation(e.getMessage());
				quit();
			}

		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

	public void initMonApplication() {

		super.initMonApplication();

		redirectLogs();
		checkJREVersion();

		CocktailUtilities.fixWoBug_responseToMessage(getEntityList());

		setCurrentExercice(FinderExercice.exerciceCourant(getEditingContext()));		

		System.out.println(">>>>>>>>> ApplicationClient.initMonApplication()");
		if (!checkDroitsUtilisateur())
			quit();

		ServerProxy.clientSideRequestSetLoginParametres(getAppEditingContext(), getUserInfos().login() ,getIpAdress());

		initExercices();

		String windowTitle = mainWindowTitle();

		initForPlatform();

		String timeZone = defaultTimeZone();
		TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
		NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(timeZone,false));
		System.out.println(" >>>>>>>  TIME ZONE CLIENT : " + defaultTimeZone());

		Superviseur.sharedInstance(getEditingContext()).init(windowTitle);

		String paramDecimales = ServerProxy.clientSideRequestGetParam(getEditingContext(), "USED_DECIMALES");
		if (paramDecimales != null)
			usedDecimales = new Integer(paramDecimales);
		else
			usedDecimales = USED_DECIMALES;
		USED_DECIMALES = usedDecimales().intValue();

		System.out.println("ApplicationClient.ApplicationClient() ==> DECIMALES UTILISEES : " + usedDecimales());

		// Test de correction des erreurs aleatoires .. Reentered ... RespondToMessage ... ???
		CocktailUtilities.fixWoBug_responseToMessage(getEntityList());

	}

	/**
	 * 
	 * @return
	 */
	private final String[] getEntityList()	{
		return new String[] {
				EOMission.ENTITY_NAME,
				EOMissionPaiement.ENTITY_NAME,
				EOMissionPaiementEngage.ENTITY_NAME,
				EOSegmentTarif.ENTITY_NAME,
				EOTransports.ENTITY_NAME,
				EONuits.ENTITY_NAME, 
				EORepas.ENTITY_NAME,
				EOIndemnite.ENTITY_NAME,
				EOFournis.ENTITY_NAME
		};
	}
	
	public EOExercice getCurrentExercice() {
		return currentExercice;
	}
	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}
	// Utilitaires de temps
	private String defaultTimeZone() {

		EODistributedObjectStore store = (EODistributedObjectStore)getEditingContext().parentObjectStore();
		String valeur = (String)store.invokeRemoteMethodWithKeyPath(getEditingContext(), "session", "clientSideRequestGetParam", new Class[] {String.class}, new Object[] {"DEFAULT_TIME_ZONE"}, false);
		
		if (valeur == null) {
			EOGrhumParametres param = FinderGrhumParametres.parametrePourCle(getEditingContext(), "DEFAULT_TIME_ZONE");
			if (param == null) {
				valeur = "Europe/Paris";
			}
			else
				valeur = param.paramValue();
		}
		
		return valeur;
		
	}

	public EOEditingContext getEditingContext() {
		return getAppEditingContext();
	}
	public EOExercice getExerciceCourant() {
		return currentExercice;
	}

	public void setWaitCursor(EOSimpleWindowController win)	{
		CRICursor.setWaitCursor(win);
	}

	public void setDefaultCursor(EOSimpleWindowController win) {
		CRICursor.setDefaultCursor(win);
	}
	public static Integer usedDecimales() {
		return usedDecimales;
	}
	/**
	 * 
	 * @param win
	 * @param yn
	 */
	public void setDefaultCursor(Component win, boolean bool)	{

		setGlassPane(bool);
		CRICursor.setWaitCursor(win, false);

	}

	/**
	 * 
	 * @param win
	 * @param yn
	 */
	public void setWaitCursor(Component win, boolean bool)	{

		setGlassPane(bool);
		CRICursor.setWaitCursor(win, true);

	}

	public void setWaitCursor(Component win)	{
		CRICursor.setWaitCursor(win);
	}

	public void setDefaultCursor(Component win) {
		CRICursor.setDefaultCursor(win);
	}

	public void setWaitCursor()	{
		CRICursor.setWaitCursor(Superviseur.sharedInstance(getEditingContext()).mainFrame());
	}

	public void setGlassPane(boolean bool) {
		Superviseur.sharedInstance(getEditingContext()).setGlassPane(bool);
	}

	public void setDefaultCursor() {
		CRICursor.setDefaultCursor(Superviseur.sharedInstance(getEditingContext()).mainFrame());
	}


	public void initExercices() {
		exercices = FinderExercice.findExercices(getEditingContext());
	}

	public NSArray<EOExercice> listeExercices() {
		return exercices;
	}


	/**
	 * Crée et renvoie une datasource (utile pour initialiser des displayGroup).
	 *
	 * @param ec
	 * @param entityName
	 * @return
	 */
	public EODistributedDataSource getDatasourceForEntity(EOEditingContext ec, String entityName) {
		return new EODistributedDataSource(ec, entityName);
	}



	/**
	 * Recuperation d'un repertoire temporaraire ou ecrire les fichiers crees
	 * 
	 * @return Retourne le chemin complet du repertoire temporaire
	 */
	public void initForPlatform() {

		osName = System.getProperties().getProperty("os.name");
		System.out.println("     --> OS Name : " + osName);

		try {
			temporaryDir = System.getProperty("java.io.tmpdir");

			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}

		} catch (Exception e) {
			System.out
			.println("Impossible de recuperer le repertoire temporaire !");
		}

		if (osName.equals(MAC_OS_X_OS_NAME)) {
			if (temporaryDir == null)
				temporaryDir = "/tmp/";
		} else {
			// On regarde si un chemin a ete entre dans le fichier Properties
			if (temporaryDir == null) {
				try {
					EODistributedObjectStore store = (EODistributedObjectStore) getEditingContext()
					.parentObjectStore();
					temporaryDir = (String) store
					.invokeRemoteMethodWithKeyPath(getEditingContext(),
							"session",
							"clientSideRequestCheminImpressions", null,
							null, true);
				} catch (Exception e) {
				}

				if (temporaryDir == null) // Rien n'est entre dans Properties,
					// on en choisit un par defaut selon
					// le systeme d'exploitation
					temporaryDir = "c:/temp/";
			}
		}

		File tmpDir = new File(temporaryDir);
		if (!tmpDir.exists()) {
			System.out
			.println("Tentative de creation du repertoire temporaire "
					+ tmpDir);
			try {
				tmpDir.mkdirs();
				System.out.println("Repertoire " + tmpDir + " cree.");
			} catch (Exception e) {
				System.out.println("Impossible de creer le repertoire "
						+ tmpDir);
			}
		} else {
			System.out.println("     --> Repertoire Temporaire : " + tmpDir);
		}

	}

	private boolean checkJREVersion() {
		System.out
		.println("Verification des versions\n---------------------------------------------");
		System.out.println("Version minimale JRE : " + MINJREVERSION);
		System.out.println("Version utilisee JRE : "
				+ System.getProperty("java.version"));
		if (System.getProperty("java.version").compareTo(MINJREVERSION) >= 0) {
			System.out.println("Test de version Ok\n");
			return true;
		} else {
			System.out
			.println("La version JRE presente n'est pas compatible avec la version minimale requise\n");
			return false;
		}
	}


	/**
	 * 
	 */
	public void quitter() {

		if (canQuit()) {

			super.quit();

		}

	}

	/**
	 * 
	 * @return
	 */
	private String mainWindowTitle() {
		// le titre de l'appli
		StringBuffer mainWindowTitle = new StringBuffer(ServerProxy
				.serverApplicationFinalName(getEditingContext()));

		// la version de l'appli
		String tmp = version();

		if (tmp != null && tmp.length() > 0) {
			mainWindowTitle.append(" - " + tmp);
		}

		// la base a laquelle on est connecte
		tmp = ServerProxy.serverBdConnexionName(getEditingContext());
		if (tmp != null && tmp.length() > 0) {
			mainWindowTitle.append(" - " + (NSArray.componentsSeparatedByString(NSArray.componentsSeparatedByString(tmp, ":").lastObject().toString(), "@")).objectAtIndex(0) );
		}
		return mainWindowTitle.toString();
	}



	/**
	 * 
	 * @return
	 */
	public String version()	{
		return ServerProxy.clientSideRequestAppVersion(getEditingContext());
	}

	public String getConnectionName()	{
		String bdConnexionName = (String)((EODistributedObjectStore)new EOEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getEditingContext(), "session", "clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);
		return bdConnexionName;
	}



	/** Retourne l'Agent utilisateur */
	public EOUtilisateur getUtilisateur() {
		return getCurrentUtilisateur();
	}

	/**
	 * 
	 * @param currentFournis
	 * @param misDeb
	 * @param misFin
	 * @return
	 */
	public int agentDejaEnMission(EOFournis currentFournis, NSTimestamp misDeb,NSTimestamp misFin) {

		NSArray result = null;
		EOMission lastMission = EOMission.findLastMissionForFournis(getEditingContext(), currentFournis);

		if (lastMission != null) {
			EOQualifier qual = EOQualifier
			.qualifierWithQualifierFormat(
					"fournis = %@ and misEtat <> 'ANNULEE' and misEtat <> 'CREATION' and misEtat <> 'MODIF' and misDebut<%@ and misFin>%@",
					new NSArray(new Object[] { currentFournis, misFin,
							misDeb }));
			EOFetchSpecification spec = new EOFetchSpecification(
					EOMission.ENTITY_NAME, qual, null);
			result = getEditingContext().objectsWithFetchSpecification(spec);
		}
		if (result == null)
			return 0;
		else
			return result.count();
	}

	/**
	 * Convertit un chemin type Unix avec separateur "\" par un chemin avec
	 * separateur "\\" de type Windows
	 */
	protected String convertUnixPathSeparator(String unixPath) {
		String token;
		StringTokenizer st = new StringTokenizer(unixPath,
				APPLICATION_FILE_PATH_SEPARATOR);
		StringBuffer winPath = new StringBuffer();
		while (st.hasMoreTokens()) {
			token = st.nextToken();
			// cas ou le repertoire contient un espace
			if (token.indexOf(' ') != -1) {
				token = "\"" + token + "\"";
			}
			winPath.append(token + WINDOWS_FILE_PATH_SEPARATOR);
		}
		return winPath.toString();
	}

	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public String getParam(String paramKey) {
		return ServerProxy
		.clientSideRequestGetParam(getEditingContext(), paramKey);
	}

	private void redirectLogs() {
		redirectedOutStream = new MyByteArrayOutputStream(System.out, 102400); // limite
		// le
		// log
		// en
		// memoire
		// a
		// 100Ko
		redirectedErrStream = new MyByteArrayOutputStream(System.err, 102400); // limite
		// le log en memoire a 100Ko
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		// on force la sortie du NSLog (qui etait initialise par defaut sur
		// System.xxx AVANT le changement)
		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
	}

	// Retourne les logs out et err
	public String outLog() {
		return redirectedOutStream.toString();
	}

	public String errLog() {
		return redirectedErrStream.toString();
	}

	public String serverOutLogs() {
		return ServerProxy.serverLogs(getEditingContext());
	}


	/**
	 * 
	 * @param application
	 * @return
	 */
	public static String getCASUserName(EOApplication application) {
		NSDictionary arguments = application.arguments();
		String dockPort = (String)arguments.objectForKey("LRAppDockPort");
		String dockTicket = (String)arguments.objectForKey("LRAppDockTicket");
		String uname = null;
		if ((dockTicket != null) && (dockPort != null))
			uname = CktlDockClient.getNetID(null, dockPort, dockTicket);
		return uname;
	}


	/**
	 * 
	 * @param login
	 * @param password
	 */
	public boolean checkDroitsUtilisateur() {

		userFonctions = getMesUtilisateurFonction();

		if (userFonctions.count() == 0)	{
			EODialogs.runInformationDialog("ERREUR","Vous n'avez aucune fonction de définie pour utiliser cette application !");
			return false;											
		}

		userFonctions = new NSArray((NSArray)userFonctions.valueForKeyPath("fonction.fonIdInterne"));
		System.out.println("     - Fonctions (" + userFonctions.count() + ") : " + userFonctions);

		userOrgans = getMesOrgans();
		System.out.println("     - Lignes Budgétaires : " + userOrgans.count());

		if (!hasFonction(EOFonction.ID_FCT_PREMISSION) && userOrgans.count() == 0)	{
			EODialogs.runInformationDialog("ERREUR","Vous n'avez de droits sur aucune ligne budgétaire.");
			return false;											
		}

		return true;
	}


	/**
	 * 
	 * @return
	 */
	public NSArray userOrgans()	 {
		return userOrgans;
	}

	/**
	 * envoi de mail cote serveur toujours : le serveur mail n'accepte pas les
	 * connexions externes
	 */
	public boolean remoteSendMail(String exp, String dest, String cc,
			String objet, String msg) {
		Boolean ok;
		NSDictionary dicoMail;
		if (cc != null) {
			Object[] keys = { "expediteur", "destinataire", "cc", "sujet",
			"texte" };
			Object[] values = { exp, dest, cc, objet, msg };
			dicoMail = new NSDictionary(values, keys);
		} else {
			Object[] keys = { "expediteur", "destinataire", "sujet", "texte" };
			Object[] values = { exp, dest, objet, msg };
			dicoMail = new NSDictionary(values, keys);
		}

		ok = (Boolean) objectStore.invokeRemoteMethodWithKeyPath(
				getEditingContext(), "session", "clientSideRequestSendMail",
				new Class[] { NSDictionary.class }, new Object[] { dicoMail },
				false);
		return ok.booleanValue();
	}

	public String emailResponsableKiwi() {
		return (String) objectStore.invokeRemoteMethodWithKeyPath(
				getEditingContext(), "session",
				"clientSideRequestGetEmailResponsable", null, null, false);
	}

	// Classe pour rediriger les logs vers la sortie initiale tout en les
	// conservant dans un ByteArray...
	// Garde en memoire un log de taille entre maxCount/2 et maxCount octets
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;

		protected int maxCount;

		public MyByteArrayOutputStream() {
			this(System.out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			this(out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out, int maxCount) {
			super(maxCount);
			this.out = out;
			this.maxCount = maxCount;
		}

		public synchronized void write(int b) {
			if (maxCount > 0 && count + 1 > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			if (maxCount > 0 && count + len > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b, off, len);
			out.write(b, off, len);
		}

		private void shift(int shift) {
			for (int i = shift; i < count; i++) {
				buf[i - shift] = buf[i];
			}
			count = count - shift;
		}
	}

	/**
	 * 
	 * @param matrice
	 * @return
	 */
	public int getSelectedRadioButton(EOMatrix matrice) {

		NSArray components = new NSArray(matrice.getComponents());
		int btnSelectionne;

		for (btnSelectionne = 0; btnSelectionne < components.count(); btnSelectionne++) {
			if (((JRadioButton) components.objectAtIndex(btnSelectionne))
					.isSelected())
				break;
		}

		return btnSelectionne;
	}

	/**
	 * 
	 * @param typeLog
	 */
	public void sendLog(String typeLog) {

		String destinataire = emailResponsableKiwi();
		String emetteur = "cpinsard@univ-lr.fr";

		try {
			String sujet = " KIWI - Logs ("
				+ DateCtrl
				.dateToString(new NSTimestamp(), "%d/%m/%Y %H:%M")
				+ ")";

			String message = "LOGS CLIENT ET SERVEUR.";
			message = message + "\nINDIVIDU CONNECTE : "
			+ currentUtilisateur.individu().nomUsuel() + " " + currentUtilisateur.individu().prenom();

			message = message
			+ "\n\n************* LOGS CLIENT *****************";
			message = message + "\nOUTPUT log :\n\n"
			+ redirectedOutStream.toString() + "\n\nERROR log :\n\n"
			+ redirectedErrStream.toString();

			message = message
			+ "\n\n************* LOGS SERVER *****************";
			message = message + "\n*****************************************";

			String outLog = (String) ((EODistributedObjectStore) getEditingContext()
					.parentObjectStore()).invokeRemoteMethodWithKeyPath(
							getEditingContext(), "session", "clientSideRequestOutLog",
							new Class[] {}, new Object[] {}, false);
			String errLog = (String) ((EODistributedObjectStore) getEditingContext()
					.parentObjectStore()).invokeRemoteMethodWithKeyPath(
							getEditingContext(), "session", "clientSideRequestErrLog",
							new Class[] {}, new Object[] {}, false);
			message = message + "\nOUTPUT log SERVER :\n\n" + outLog
			+ "\n\nERROR log SERVER :\n\n" + errLog;

			StringBuffer mail = new StringBuffer();
			mail.append(message);

			((EODistributedObjectStore) getEditingContext().parentObjectStore())
			.invokeRemoteMethodWithKeyPath(getEditingContext(), "session",
					"clientSideRequestSendMail", new Class[] {
				String.class, String.class, String.class,
				String.class, String.class }, new Object[] {
				emetteur, destinataire, null,
				sujet, mail.toString() }, false);
			MsgPanel.sharedInstance().runInformationDialog(
					"ENVOI MAIL", "Le mail a bien été envoyé à l'adresse : " + destinataire);
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
	}

	public void cleanLogs(String type) {

		if (type.equals("CLIENT")) {
			redirectedOutStream.reset();
			redirectedErrStream.reset();
		}

		if (type.equals("SERVER")) {
			((EODistributedObjectStore) getEditingContext().parentObjectStore())
			.invokeRemoteMethodWithKeyPath(getEditingContext(), "session",
					"clientSideRequestCleanLogs", new Class[] {},
					new Object[] {}, false);
			;
		}
	}

	public String outLogs() {
		return redirectedOutStream.toString();
	}

	public String errLogs() {
		return redirectedErrStream.toString();
	}

	public void showLogs() {
		XLogs messages = new XLogs("MESSAGES CLIENT ET SERVEUR");
		messages.setVisible(true);
	}

	public void openURL(String URL) {
		try {
			Runtime runTime = Runtime.getRuntime();// .exec(WINDOWS_EXEC+filePath);
			runTime.exec(new String[] { "rundll32",
					"url.dll,FileProtocolHandler", URL });
		} catch (Exception exception) {
			MsgPanel.sharedInstance().runErrorDialog(
					"ERREUR",
					"Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "
					+ URL
					+ "\nMESSAGE : "
					+ exception.getMessage());
			exception.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public boolean afficherPdf(NSData datas, String fileName) {

		if (temporaryDir == null)
			return false;

		if (datas == null) {
			MsgPanel.sharedInstance().runErrorDialog(
					"ERREUR",
			"Impression impossible !\nVos paramètres d'impressions doivent etre mal configurés.");
			return false;
		}

		byte b[] = datas.bytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(b);

		String filePath = temporaryDir + fileName + ".pdf";

		try {
			StreamCtrl.saveContentToFile(stream, filePath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		openFile(filePath);

		return true;
	}

	/**
	 * 
	 * @return
	 */
	public String temporaryDir() {
		return temporaryDir;
	}

	/**
	 * 
	 */
	public void openFile(String filePath) {

		try {
			new FileHandler().openFile(filePath);			
		}
		catch (Exception e) {

		}

	}

	/** 
	 * Creation d'un report en fonction d'un array de valeurs et d'un nom 
	 */
	public void exportExcel(String chaine, String reportName)	{
		String 		fileName;
		File 		aFile;
		FileOutputStream	anOutputStream = null;

		if(chaine != null)	{
			fileName = temporaryDir.concat(reportName.concat(".csv"));
			aFile = new File(fileName);
			try	{
				anOutputStream = new FileOutputStream(aFile);
				anOutputStream.write(chaine.getBytes());
				anOutputStream.close();
			}
			catch(Exception exception)	{
				System.out.println(this.getClass().getName()+".imprimer() - Exception : "+exception.getMessage());
				return;
			}

			openFile(fileName);
		}
	}


	public boolean hasFonction(String idFonction)	{
		return userFonctions.containsObject(idFonction);
	}


	public JFrame getMainWindow() {
		return Superviseur.sharedInstance(getEditingContext()).mainFrame();
	}


	/**
	 * 
	 * @return
	 */
	public final String getIpAdress() {
		try {
			final String s = java.net.InetAddress.getLocalHost().getHostAddress();
			final String s2 = java.net.InetAddress.getLocalHost().getHostName() ;
			return s+" / " + s2;
		} catch (UnknownHostException e) {
			return "Machine inconnue";

		}
	}


	public class MainWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
			quitter();
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent event) {
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			quitter();
		}
	}

}