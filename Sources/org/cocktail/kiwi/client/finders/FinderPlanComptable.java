/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderPlanComptable {
	

	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static NSArray findPlancosPourClasses(EOEditingContext ec, NSArray classes)	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray qualifsPcoNum = new NSMutableArray();

		for (int i=0;i<classes.count();i++)
			qualifsPcoNum.addObject(EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + classes.objectAtIndex(i) + "*'",null));

		mesQualifiers.addObject(new EOOrQualifier(qualifsPcoNum));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_VALIDITE_KEY + " ='VALIDE'",null));
		
		NSArray mySort = new NSArray(new EOSortOrdering(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME,new EOAndQualifier(mesQualifiers),mySort);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findComptesForSelection(EOEditingContext ec, String classe) {
		
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();

			if (classe != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY+" like %@", new NSArray(classe+"*")));
			

			NSMutableArray myOrQualifier = new NSMutableArray();
			myOrQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_VALIDITE_KEY+" = %@", new NSArray("VALIDE")));
			myOrQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NIV_KEY  + " = 2", null));

			mesQualifiers.addObject(new EOOrQualifier(myOrQualifier));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			NSArray comptes = ec.objectsWithFetchSpecification(fs);
			
			return comptes;
		}
		catch (Exception e )	{
			//e.printStackTrace();
			return new NSArray();
		}
	}

}
