/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EOKiwiExportDocuments;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderKiwiExportDocuments {
	

	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findDocuments(EOEditingContext ec)	{
		
		NSArray mySort = new NSArray(new EOSortOrdering(EOKiwiExportDocuments.KED_CODE_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("", null);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOKiwiExportDocuments.ENTITY_NAME, null, mySort);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	

}
