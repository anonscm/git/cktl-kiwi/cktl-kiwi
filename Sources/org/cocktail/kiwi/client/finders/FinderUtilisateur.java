/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.metier.EOFonction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderUtilisateur {
	
	
	/** */
	public static NSArray findUtilisateursForApplication(EOEditingContext ec, Number tyapId)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateurFonctions.fonction.tyapId = %@", new NSArray(tyapId));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateur.ENTITY_NAME,myQualifier, null);

		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOUtilisateur.INDIVIDU_KEY));
		fs.setUsesDistinct(true);
		
		return ec.objectsWithFetchSpecification(fs);
	}
	
	/** */
	public static NSArray findUtilisateursForFonction(EOEditingContext ec, EOFonction fonction )	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateurFonctions.fonction = %@", new NSArray(fonction));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateur.ENTITY_NAME,myQualifier, null);

		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOUtilisateur.INDIVIDU_KEY));
		fs.setUsesDistinct(true);
		
		return ec.objectsWithFetchSpecification(fs);
	}

}
