/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOWebmon;
import org.cocktail.kiwi.client.metier.EOWebtaux;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderWebTaux {
	
	
	/**
	 * 
	 * @param ec
	 * @param monnaie
	 * @param dateFin
	 * @return
	 */
	public static EOWebtaux findWebTauxForWebmonDate(EOEditingContext ec, EOWebmon monnaie, NSTimestamp dateFin)	{

		try {
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebtaux.WEBMON_KEY + "=%@", new NSArray(monnaie)));

			if (dateFin != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebtaux.WTA_DATE_KEY + " <= %@", new NSArray(dateFin)));

			NSArray mySort = new NSArray(new EOSortOrdering("wtaDate", EOSortOrdering.CompareDescending));
			
			EOFetchSpecification	fs = new EOFetchSpecification(EOWebtaux.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);
			
			return (EOWebtaux)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			return null;
		}
	}
	
	
	
	
	/**
	 * 
	 * @param ec
	 * @param monnaie
	 * @param dateFin
	 * @return
	 */
	public static NSArray findTauxForWebmonAndDate(EOEditingContext ec, EOWebmon monnaie, NSTimestamp dateFin)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebtaux.WEBMON_KEY + "=%@", new NSArray(monnaie)));

		if (dateFin != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("wtaDate < %@", new NSArray(dateFin)));
		
		EOFetchSpecification	fs = new EOFetchSpecification(EOWebtaux.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOWebtaux findDefaultWebTaux(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOWebtaux.WTA_COURANTE_KEY + "=1", null);

		EOFetchSpecification	fs = new EOFetchSpecification(EOWebtaux.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		try {return (EOWebtaux)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
	}
	
}
