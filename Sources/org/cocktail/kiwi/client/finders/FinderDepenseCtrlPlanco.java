/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMandat;
import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderDepenseCtrlPlanco {
	
	public static EOMandat findMandatForDepenseCtrlPlanco(EOEditingContext ec, EODepenseCtrlPlanco depense)	{

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(""+
					EOMandat.DEPENSE_CTRL_PLANCOS_KEY + " = %@ ", new NSArray(depense)));
			EOFetchSpecification fs = new EOFetchSpecification(EODepense.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return (EOMandat)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			return null;
		}
	}
	
	
}
