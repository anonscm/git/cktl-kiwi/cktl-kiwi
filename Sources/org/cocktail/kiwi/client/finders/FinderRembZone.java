/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EORembZone;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderRembZone {
	
	public static EORembZone findForCode(EOEditingContext ec, String code)	{				
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EORembZone.REM_TAUX_KEY + " = %@", new NSArray(code));
		return EORembZone.fetchFirstByQualifier(ec,  qualifier);
	}
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EORembZone> findZones(EOEditingContext ec)	{
		
		NSArray mySort = new NSArray(new EOSortOrdering(EORembZone.REM_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EORembZone.TEM_VALIDE_KEY + " = %@", new NSArray("O"));
		
		EOFetchSpecification	fs = new EOFetchSpecification(EORembZone.ENTITY_NAME, qualifier, mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EORembZone> findZonesForRepasNuits(EOEditingContext ec)	{
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembZone.TEM_VALIDE_KEY + " = %@", new NSArray("O")));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembZone.REM_ETRANGER_KEY + " = %@", new NSArray("0")));
		return EORembZone.fetchAll(ec, new EOAndQualifier(qualifiers), null);
	}

}