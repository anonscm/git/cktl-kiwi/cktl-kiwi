/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderTypeCredit {
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTypeCredit findTypeCreditForCodeAndExercice(EOEditingContext ec, String code, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
	
		try {
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_CODE_KEY + " = %@", new NSArray(code)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + " = %@", new NSArray(exercice)));

			EOFetchSpecification myFetch = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);

			return (EOTypeCredit)ec.objectsWithFetchSpecification(myFetch).objectAtIndex(0);		
		}
		catch (Exception e) {
			
			return null;
			
		}

	}
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTypesCredit(EOEditingContext ec, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		EOFetchSpecification myFetch = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		
		return ec.objectsWithFetchSpecification(myFetch);		
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTypesCreditDep(EOEditingContext ec, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_TYPE_KEY + " = 'DEPENSE'", null));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		EOFetchSpecification myFetch = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		
		return ec.objectsWithFetchSpecification(myFetch);		
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTypesCreditRec(EOEditingContext ec, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_TYPE_KEY + " = 'RECETTE'", null));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		EOFetchSpecification myFetch = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		
		return ec.objectsWithFetchSpecification(myFetch);		
	}
	
	
	public static NSArray findTypesCreditDepExecutoire(EOEditingContext ec, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_TYPE_KEY + " = 'DEPENSE'", null));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_BUDGET_KEY + " = 'EXECUTOIRE'", null));

//		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		EOFetchSpecification myFetch = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		
		return ec.objectsWithFetchSpecification(myFetch);		
	}
}
