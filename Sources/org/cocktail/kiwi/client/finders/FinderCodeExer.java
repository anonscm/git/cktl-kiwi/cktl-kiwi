/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOCodeMarche;
import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderCodeExer {

	public static final EOSortOrdering SORT_CODE_ASC = new EOSortOrdering(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_CODE_ASC  = new NSArray(SORT_CODE_ASC);

	public static NSArray findCodesExer(EOEditingContext edc, EOExercice exercice)	{
		
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY + " =%@",new NSArray(exercice)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CE_SUPPR_KEY + " =%@",new NSArray("N")));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CE_ACTIF_KEY + " =%@",new NSArray("O")));
		
		return EOCodeExer.fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_CODE_ASC);
				
	}
	
}
