/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EORibfour;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderRibfour {
	
	public static NSArray findRibsValidesForFournis(EOEditingContext ec, EOFournis fournis)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ribValide = 'O' and fournis = %@", new NSArray(fournis)));
				
		EOFetchSpecification fs = new EOFetchSpecification(EORibfour.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static EORibfour findRibValideForFournis(EOEditingContext ec, EOFournis fournis)	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ribValide = 'O' and fournis = %@", new NSArray(fournis)));
				
		EOFetchSpecification fs = new EOFetchSpecification(EORibfour.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		try {return (EORibfour)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
	}
	
}
