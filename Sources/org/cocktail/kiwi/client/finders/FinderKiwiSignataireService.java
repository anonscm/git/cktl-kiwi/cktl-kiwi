/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireService;
import org.cocktail.kiwi.client.metier.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderKiwiSignataireService {
	
	

	public static NSArray findSignatairesForService(EOEditingContext ec, EOStructure structure)	{

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKiwiSignataireService.STRUCTURE_KEY + " = %@", new NSArray(structure)));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOKiwiSignataireService.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}
	/**
	 * 
	 * @param ec
	 * @param paiement
	 * @return
	 */
	public static EOKiwiSignataireService findSignataireForIndividuAndService(EOEditingContext ec, EOIndividu individu, EOStructure structure)	{

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKiwiSignataireService.STRUCTURE_KEY + " = %@", new NSArray(structure)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKiwiSignataireService.INDIVIDU_KEY + " = %@", new NSArray(individu)));

			EOFetchSpecification fs = new EOFetchSpecification(EOKiwiSignataireService.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return (EOKiwiSignataireService)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			
			return null;

		}
	}
	
	
}
