/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOTypeEtat;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.metier.budget.EOOrganAction;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderTypeAction {
	
	
	private static final String PARAM_ORGAN_DEST = "CTRL_ORGAN_DEST";
	private static final String FON_ID_INTERNE_ACTIONS_VOTEES = "DEAUTACT";
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray getTypeActions(EOEditingContext ec,EOExercice exercice)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
				
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.TO_EXERCICE_KEY +" = %@", new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.TYPE_ETAT_KEY +" = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

		EOFetchSpecification	fs = new EOFetchSpecification(EOLolfNomenclatureDepense.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findLolfsForCodes(EOEditingContext ec,EOExercice exercice,  NSArray codes)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray qualifsCodes = new NSMutableArray();
		
		if (codes != null) {
			for (int i=0;i<codes.count();i++)
				qualifsCodes.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_CODE_KEY + " = %@", new NSArray((String)codes.objectAtIndex(i))));
			
			mesQualifiers.addObject(new EOOrQualifier(qualifsCodes));
		}
		
		if (exercice != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.TO_EXERCICE_KEY +" = %@", new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.TYPE_ETAT_KEY +" = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

		EOFetchSpecification	fs = new EOFetchSpecification(EOLolfNomenclatureDepense.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @param utilisateur
	 * @return
	 */
	   public static final NSArray getTypesAction(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit,
	    		EOUtilisateur utilisateur) {
		   
		   try {
			   			   
			   String paramDepense = FinderDepenseParametres.getValue(ec, PARAM_ORGAN_DEST, exercice);
			   if (paramDepense != null && "NON".equals(paramDepense)) {
				   
				   return getTypeActions(ec, exercice);
				   
			   }
			   else {

				   NSArray fonctionsDepense = FinderUtilisateurFonction.findFonctionsForUtilisateurAndApplication(ec, utilisateur, exercice, new Integer(3));

				   if (((NSArray)fonctionsDepense.valueForKey(EOFonction.FON_ID_INTERNE_KEY)).containsObject(FON_ID_INTERNE_ACTIONS_VOTEES))
					   return getTypeActions(ec, exercice);				   
				   else {

					   NSArray organsAction = FinderOrganAction.getActions(ec, exercice, organ, typeCredit);
					   return (NSArray)organsAction.valueForKey(EOOrganAction.TYPE_ACTION_KEY);
				   }
			   }
			   			   
		   }
		   catch (Exception e) {
			   e.printStackTrace();
			   return new NSArray();			   
		   }
		   
	   }
		   
		   
}
