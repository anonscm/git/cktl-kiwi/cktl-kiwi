/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.metier.budget.EOOrganSignataire;
import org.cocktail.kiwi.client.metier.budget.EOOrganSignataireTc;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderOrganSignataire {
	
	
	public static String getStringSignataires(EOEditingContext ec, EOOrgan organ, BigDecimal montant, NSTimestamp date, EOTypeCredit typeCredit, String nbSignataires) {
		
		String signataires = "";
		
		NSArray signatairesCr = FinderOrganSignataire.getOrganSignataires(organ, date, montant, typeCredit);

		if (signatairesCr.count() > 0) {

			if ("1".equals(nbSignataires)) {	//  1 seul signataire

				EOIndividu individu = ((EOOrganSignataire)signatairesCr.objectAtIndex(0)).individu();
				signataires = signataires + individu.cCivilite()+" "+individu.prenom()+" "+individu.nomUsuel();
				
			}
			else {		// On affiche tous les signataires
			
				for (int i = 0;i< signatairesCr.count();i++) {
					EOIndividu individu = ((EOOrganSignataire)signatairesCr.objectAtIndex(i)).individu();
					signataires = signataires + individu.cCivilite()+" "+individu.prenom()+" "+individu.nomUsuel()+"\n";
				}

				return signataires.substring(0, (signataires.length()-1));

			}
			
		}
		
		return signataires;
	}
	
	
    /**
     * Recupere une liste de organSignataires associes a une branche de l'organigrame budgetaire. 
     * Si aucun signataire possible (en fonction des limitations par type de credit) n'est trouve au niveau de la branche, on remonte a la branche pere, 
     * jusqu'eventuellement en haut de l'organigramme.
     * 
     * @param organ Branche de l'organigramme sur laquelle est faite la depense
     * @param timestamp Date (logiquement la date du jour)
     * @param montant Montant de la depense
     * @param typeCredit typeCredit Type de credit associe a la depense 
     * @return un NSArray des ordonnateurs (responsables) associes a une branche de l'organigramme budgetaire.
     */
    public static NSArray getOrganSignataires(final EOOrgan organ, final NSTimestamp timestamp, final BigDecimal montant, final EOTypeCredit typeCredit) {
        final NSMutableArray signataires = new NSMutableArray();  
        EOOrgan org = organ;
        
        while (signataires.count()==0 && org != null ) {
//          Filtre sur la date        
            final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("("+EOOrganSignataire.ORSI_DATE_OUVERTURE_KEY +"=nil or " + 
                    EOOrganSignataire.ORSI_DATE_OUVERTURE_KEY +"<=%@) and (" + EOOrganSignataire.ORSI_DATE_CLOTURE_KEY +"=nil or " + 
                    EOOrganSignataire.ORSI_DATE_CLOTURE_KEY +">=%@)", new NSArray(new Object[]{timestamp, timestamp}));

            //Recuperer tous les organSignataires associes a la ligne budgetaire.
            NSArray organSignataires = EOQualifier.filteredArrayWithQualifier(org.organSignataires(), qual);
            
            NSMutableArray mySort = new NSMutableArray();
            mySort.addObject(new EOSortOrdering(EOOrganSignataire.TYSI_ID_KEY, EOSortOrdering.CompareAscending));
            mySort.addObject(new EOSortOrdering(EOOrganSignataire.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareDescending));
            
            organSignataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(organSignataires, mySort);

            signataires.addObjectsFromArray(organSignataires);
            final EOQualifier qualTcBad = EOQualifier.qualifierWithQualifierFormat(EOOrganSignataireTc.TO_TYPE_CREDIT_KEY +"=%@ and "+ EOOrganSignataireTc.OST_MAX_MONTANT_TTC_KEY + "<%@", new NSArray(new Object[]{typeCredit, montant}));
                    
//          Verifier s'il y a des limites depassees sur les types de credits, dans ce cas on ecarte les signataires
            for (int i = 0; i < organSignataires.count(); i++) {
                final EOOrganSignataire element = (EOOrganSignataire) organSignataires.objectAtIndex(i);
                if (EOQualifier.filteredArrayWithQualifier(element.organSignataireTcs(), qualTcBad).count() > 0 ) {
                    signataires.removeObject(element);
                }
            }
            org = org.organPere(); 
        }
        return signataires.immutableClone();
    }

    /**
     * 
     * @param ec
     * @return
     */
    public static NSArray findIndividusSignataires(EOEditingContext ec, EOOrgan organ) {

    	try {
    		    			
    		String sqlQualifier = "SELECT distinct no_individu, pers_id, nom_usuel, prenom FROM (";
    		
    		sqlQualifier = sqlQualifier +
    				" SELECT no_individu, pers_id, NOM_USUEL, PRENOM " +
    				" FROM grhum.individu_ulr where no_individu in ( " + 
    				" SELECT distinct no_individu FROM jefy_admin.organ_signataire os, jefy_admin.organ o " +
    				" WHERE os.tysi_id = 4 and os.org_id = o.org_id ";

    		if (organ.orgNiveau().intValue() == 1 )
    			sqlQualifier = sqlQualifier + " and o.org_niv = 1 and o.org_etab = '" + organ.orgEtab()+ "'";

    		if (organ.orgNiveau().intValue() == 2 )
    			sqlQualifier = sqlQualifier + " and o.org_niv = 2 and o.org_ub = '" + organ.orgUb()+ "'";

    		if (organ.orgNiveau().intValue() == 3)
    			sqlQualifier = sqlQualifier + " and o.org_ub = '" + organ.orgUb()  + "' and o.org_niv < 4";
    				
    		if (organ.orgNiveau().intValue() == 4)
    			sqlQualifier = sqlQualifier + " and o.org_ub = '" + organ.orgUb() + "'";

    		sqlQualifier = sqlQualifier + " ) ";

			sqlQualifier = sqlQualifier + 
			" UNION ALL " +
			" SELECT no_individu, pers_id, NOM_USUEL, PRENOM  " +
			" FROM grhum.individu_ulr " +
			" WHERE no_individu in (  SELECT distinct no_individu FROM jefy_admin.organ_signataire os, jefy_admin.organ o  " +
			" WHERE os.org_id = o.org_id and  o.org_niv in (0,1) )";  

			sqlQualifier = sqlQualifier + 
			" UNION ALL " +   
			" SELECT i.no_individu, i.pers_id, nom_usuel, prenom" + 
			" FROM jefy_admin.organ o, grhum.structure_ulr s, grhum.individu_ulr i" + 
			" WHERE o.org_ub = '"  + organ.orgUb() + "' and O.c_structure = s.c_structure and s.grp_responsable = i.no_individu ";

			
    		sqlQualifier = sqlQualifier + 		
    				" ) ORDER by nom_usuel, prenom";

    		System.out
					.println("FinderOrganSignataire.findIndividusSignataires() QUALIF SIGN : " + sqlQualifier);
    		
    		return ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);
		    
		}
	    catch (Exception e)	{
	    	return new NSArray();
	    } 
    }
    
}
