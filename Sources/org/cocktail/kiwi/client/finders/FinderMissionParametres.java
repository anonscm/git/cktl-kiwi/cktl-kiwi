/*
 * Created on 21 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.EOMissionParametres;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderMissionParametres {

	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
	*/
	public static String getValue(EOEditingContext edc, EOExercice exercice, String key)	{
		try {
			EOMissionParametres param = findParametre(edc, exercice, key);
			return param.paramValue();
		}
		catch (Exception e) {
			return null;
		}
	}
	public static String getValue(EOEditingContext edc, String key)	{
		try {
			EOMissionParametres param = findParametre(edc, key);
			return param.paramValue();
		}
		catch (Exception e) {
			return null;
		}
	}

	
	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
	*/
	public static EOMissionParametres findParametre(EOEditingContext edc, String key)	{
		try {
			
			NSMutableArray qualifiers = new NSMutableArray();
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionParametres.PARAM_KEY_KEY + " = %@", new NSArray(key)));

			//qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionParametres.TO_EXERCICE_KEY + " = '" + key + "'", null));
			
			return EOMissionParametres.fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));
			
		}
		catch (Exception e) {return null;}
	}
	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
	*/
	public static EOMissionParametres findParametre(EOEditingContext edc, EOExercice exercice, String key)	{
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionParametres.PARAM_KEY_KEY + " = %@", new NSArray(key)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionParametres.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
			return EOMissionParametres.fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {return null;}
	}

}