/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderOrgan {
	
	
	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static EOOrgan findEtabForOrgan(EOEditingContext ec, EOOrgan organ)  {

			try {
				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = %@", new NSArray(new Integer(1))));

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_ETAB_KEY + " = %@", new NSArray(organ.orgEtab())));

				EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				fs.setRefreshesRefetchedObjects(true);

				return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			}
			catch (Exception e) {
				return null;
			}
	    }
		
	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static EOOrgan findUbForOrgan(EOEditingContext ec, EOOrgan organ)  {

			try {
				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = %@", new NSArray(new Integer(2))));

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + " = %@", new NSArray(organ.orgUb())));

				EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				fs.setRefreshesRefetchedObjects(true);

				return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			}
			catch (Exception e) {
				return null;
			}
	    }

		
		
	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static NSArray findOrgansForOrganPere(EOEditingContext ec, EOOrgan organ, EOExercice exercice)  {

	        NSMutableArray mesQualifiers = new NSMutableArray();

	        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organPere = %@", new NSArray(organ)));

	        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.TO_EXERCICE_KEY + "=%@", new NSArray(exercice)));

	        EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
	        fs.setRefreshesRefetchedObjects(true);
	        	        
	        return ec.objectsWithFetchSpecification(fs);
	    }

	
	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static NSArray rechercherLignesBudgetairesNiveau(EOEditingContext ec,EOExercice exercice, 
				Number niveau)  {
	        NSMutableArray mySort=new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_UB_KEY,EOSortOrdering.CompareAscending));
	        mySort.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_CR_KEY,EOSortOrdering.CompareAscending));
	        mySort.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_SOUSCR_KEY,EOSortOrdering.CompareAscending));

	        NSMutableArray args = new NSMutableArray();
	        args.addObject(niveau);
	        args.addObject(exercice);
	        
	        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " =%@" +
	        		" and " + EOOrgan.TO_EXERCICE_KEY + " = %@", args);
	        	        
	        EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, qual, mySort);
	        return ec.objectsWithFetchSpecification(fs);
	    }

		
		/**
		 * 
		 * @param ec
		 * @param niveau
		 * @return
		 */
			public static NSArray findOrgansForNiveau(EOEditingContext ec, EOExercice exercice, Number niveau)  {

		        NSMutableArray mesQualifiers = new NSMutableArray();

		        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = %@", new NSArray(niveau)));

		        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

		        EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		        fs.setUsesDistinct(true);
		        fs.setRefreshesRefetchedObjects(true);
		        	        
		        return ec.objectsWithFetchSpecification(fs);
		    }
			
			
		
		
/**
 * 
 * @param ec
 * @param niveau
 * @param organ
 * @param exercice
 * @param agent
 * @return
 */
    public static EOOrgan rechercherOrganAvecNiveau(EOEditingContext ec, int niveau, EOOrgan organ, 
    		EOExercice exercice) {
    	
    	try {
    		NSMutableArray mesQualifiers = new NSMutableArray();
    		
    		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = %@", new NSArray(new Integer(niveau))));
    		
    		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_ETAB_KEY + " = %@", new NSArray(organ.orgEtab())));
    		
    		if (niveau > 1)
    			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + " = %@",new NSArray(organ.orgUb())));
    		
    		if (niveau > 2)
    			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY + " = %@",new NSArray(organ.orgCr())));
    		
    		if (niveau > 3)
    			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_SOUSCR_KEY + " = %@",new NSArray(organ.orgSouscr())));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.TO_EXERCICE_KEY + " = %@",new NSArray(exercice)));

    		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
    		return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	}
    	catch (Exception e) {
    		return null;
    	}
    }

    /** 
	*
	*/
	public static NSArray rechercherOrgansPourPere(EOEditingContext ec, EOOrgan pere, EOUtilisateur utilisateur)	{

		try {
			NSMutableArray args = new NSMutableArray();
			args.addObject(pere);
//			args.addObject(utilisateur);
			
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORGAN_PERE_KEY + " = %@ ", args);
			EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, qual, null);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param listeOrgans
	 * @return
	 */
	public static NSArray getDistinctUbsFromArray(NSArray listeOrgans) {
		
		NSMutableArray result = new NSMutableArray();
		
		for (int i=0;i<listeOrgans.count();i++) {
			
			EOOrgan organ = (EOOrgan)listeOrgans.objectAtIndex(i);
			
			if (!result.containsObject(organ.orgUb())) {
				result.addObject(organ.orgUb());
			}
			
		}
		
		return result.immutableClone();
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static NSArray findOrgansForUtilisateur(EOEditingContext ec, EOUtilisateur utilisateur, EOExercice exercice)  {

			try {
				
				NSMutableArray mySort =new NSMutableArray();
				mySort.addObject(new EOSortOrdering("orgEtab", EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering("orgUb", EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering("orgCr", EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering("orgSouscr", EOSortOrdering.CompareAscending));

	        NSMutableArray mesQualifiers = new NSMutableArray();

	        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("utilisateurOrgans.utilisateur = %@", new NSArray(utilisateur)));

	        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

	        EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
	        fs.setRefreshesRefetchedObjects(true);
	        	        
	        return ec.objectsWithFetchSpecification(fs);
			}
			catch (Exception e)	{
				e.printStackTrace();
				return new NSArray();
			}
	    }
	
	
}
