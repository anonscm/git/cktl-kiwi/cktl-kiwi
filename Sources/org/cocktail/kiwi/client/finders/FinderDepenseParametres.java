/*
 * Created on 21 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.budget.EODepenseParametre;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;



/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderDepenseParametres {


	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
*/
	public static String getValue(EOEditingContext ec, String key, EOExercice exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseParametre.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseParametre.PAR_KEY_KEY + " = %@", new NSArray(key)));

		EOFetchSpecification fs = new EOFetchSpecification(EODepenseParametre.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		fs.setRefreshesRefetchedObjects(true);
		
		NSArray params = ec.objectsWithFetchSpecification(fs);

		try {return ((EOMissionParametres)params.objectAtIndex(0)).paramValue();}
		catch (Exception e) {return null;}
	}
	
	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
	*/
	public static EODepenseParametre findParametre(EOEditingContext ec, String key,EOExercice exercice)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseParametre.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseParametre.PAR_KEY_KEY + " = %@", new NSArray(key)));

		EOFetchSpecification fs = new EOFetchSpecification(EODepenseParametre.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		
		NSArray params = ec.objectsWithFetchSpecification(fs);

		try {return (EODepenseParametre)params.objectAtIndex(0);}
		catch (Exception e) {return null;}
	}

	
	
}
