/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EOWebpays;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderWebPays {
	

	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findPaysForZone(EOEditingContext ec, EORembZone zone)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOWebpays.WPA_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier myQualifier = null;
		
		if (zone != null)
			myQualifier = EOQualifier.qualifierWithQualifierFormat(EOWebpays.TO_ZONE_KEY + " = %@", new NSArray(zone));
				
		EOFetchSpecification	fs = new EOFetchSpecification(EOWebpays.ENTITY_NAME, myQualifier, mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

}
