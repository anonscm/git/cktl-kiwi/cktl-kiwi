/* FinderClientEOF.java created by frivalla on Fri 23-May-2003 */
package org.cocktail.kiwi.client.finders;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
* @author frederic rivalland
 *
 *Classe permettant de faire un demande au niveau du serveur
 * pour retourner un FectSpech definit dans le model
 */

public class FinderClientEOF extends Object {


    public   FinderClientEOF() {super();}

public NSArray fetchSpec(EOEditingContext ec,String nomTable, String nomQualifier, NSDictionary leDico)
{
    NSArray result = (NSArray)((EODistributedObjectStore)ec.parentObjectStore())
    .invokeRemoteMethodWithKeyPath(ec,"session","clientSideRequestFetchSpec",
                                   new Class[] {String.class,String.class,NSDictionary.class},
                                   new Object[] {nomTable,nomQualifier,leDico},
                                   false);
    return result;
}

}
