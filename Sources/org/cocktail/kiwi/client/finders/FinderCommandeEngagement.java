/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.budget.EOCommande;
import org.cocktail.kiwi.client.metier.budget.EOCommandeEngagement;
import org.cocktail.kiwi.client.metier.budget.EOEngage;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderCommandeEngagement {
	
	
	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static EOEngage findEngageForCommande(EOEditingContext ec, EOCommande commande)	{
		
		try {
						
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeEngagement.COMMANDE_KEY + " = %@", new NSArray(commande)));

			EOFetchSpecification fs = new EOFetchSpecification(EOCommandeEngagement.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);

			return ((EOCommandeEngagement)ec.objectsWithFetchSpecification(fs).objectAtIndex(0)).engage();
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
		
	}


}
