/*
 * Created on 21 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOFournis;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderFournis {


	
	/**
     * tourver un individu par son persId
     * @param ec
     * @param persId
     * @return
     */
    public static EOFournis findFournisForPersId(EOEditingContext ec, Number persId) {

        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId));
	    EOFetchSpecification fs = new EOFetchSpecification(EOFournis.ENTITY_NAME, qual, null);

	    try {return (EOFournis)ec.objectsWithFetchSpecification(fs).lastObject();}
	    catch (Exception e)	{return null;}
    }

}
