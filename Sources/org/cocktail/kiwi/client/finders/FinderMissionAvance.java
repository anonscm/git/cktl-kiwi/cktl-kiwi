/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionAvance;
import org.cocktail.kiwi.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderMissionAvance {
	
	
	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static NSArray<EOMissionAvance> findAvancesForMission(EOEditingContext ec, EOMission mission)	{
		
		try {
			
			NSArray mySort = new NSArray(new EOSortOrdering(EOMissionAvance.MAV_DATE_SAISIE_KEY, EOSortOrdering.CompareDescending));
			
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionAvance.TYPE_ETAT_KEY +"."+EOTypeEtat.TYET_LIBELLE_KEY + " = %@", new NSArray(EOTypeEtat.ETAT_VALIDE)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionAvance.MISSION_KEY + " = %@", new NSArray(mission)));

			EOFetchSpecification fs = new EOFetchSpecification(EOMissionAvance.ENTITY_NAME,new EOAndQualifier(mesQualifiers),mySort);

			return ec.objectsWithFetchSpecification(fs);
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
		
	}

	/**
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static int countAvances (EOEditingContext ec, EOMission mission) {
		
		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionAvance.MISSION_KEY + " = %@", new NSArray(mission)));

			EOFetchSpecification fs = new EOFetchSpecification(EOMissionAvance.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);

			return ec.objectsWithFetchSpecification(fs).count();
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new Integer(0).intValue();
		}
		
	}
}
