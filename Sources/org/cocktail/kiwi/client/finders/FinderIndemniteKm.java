/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EOIndemniteKm;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.metier.EOVehicule;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderIndemniteKm {

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EOIndemniteKm> findIndemnitesForZoneAndVehicule(EOEditingContext ec, NSTimestamp dateRef, EORembZone zone, EOTypeTransport typeTransport, EOVehicule vehicule)	{

		NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();

		try {			

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndemniteKm.TO_TYPE_TRANSPORT_KEY + " = %@", new NSArray(typeTransport)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndemniteKm.TO_ZONE_KEY + " = %@", new NSArray(zone)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndemniteKm.IKM_DATE_KEY + " <= %@", new NSArray(DateCtrl.stringToDate(DateCtrl.dateToString(dateRef)))));

			NSMutableArray orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndemniteKm.IKM_DATE_FIN_KEY + " = nil", null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndemniteKm.IKM_DATE_FIN_KEY + " >= %@", new NSArray(DateCtrl.stringToDate(DateCtrl.dateToString(dateRef)))));
			
			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));

			if (vehicule != null && !typeTransport.isVelomoteur() && !typeTransport.isMotocyclette()) {
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndemniteKm.IKM_PF_DEBUT_KEY+ " <= %@", new NSArray(new Integer(vehicule.vehPuissance()))));

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndemniteKm.IKM_PF_FIN_KEY+ " >= %@", new NSArray(new Integer(vehicule.vehPuissance()))));
			}
			
			return EOIndemniteKm.fetchAll(ec, new EOAndQualifier(mesQualifiers), null);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}

}
