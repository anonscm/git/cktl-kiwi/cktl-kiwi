/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.metier.budget.EOConventionNonLimitative;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderConventionNonLimitative {
	
	
	public static EOConventionNonLimitative findConventionRA(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit) {
		
		try {
		
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConventionNonLimitative.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConventionNonLimitative.ORGAN_KEY + " = %@", new NSArray(organ)));
	
			EOFetchSpecification fs = new EOFetchSpecification(EOConventionNonLimitative.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

			return (EOConventionNonLimitative)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		
		}
		catch (Exception e) {
			
			return null;
		}
		
		
	}
	
}
