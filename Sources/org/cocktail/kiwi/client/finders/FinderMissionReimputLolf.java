/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMissionReimput;
import org.cocktail.kiwi.client.metier.EOMissionReimputLolf;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderMissionReimputLolf {
	
	public static NSArray findLolfs(EOEditingContext ec, EOMissionReimput reimputation)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMissionReimputLolf.MISSION_REIMPUT_KEY + " = %@", new NSArray(reimputation));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOMissionReimputLolf.ENTITY_NAME,myQualifier, null);

		return ec.objectsWithFetchSpecification(fs);
	}

}
