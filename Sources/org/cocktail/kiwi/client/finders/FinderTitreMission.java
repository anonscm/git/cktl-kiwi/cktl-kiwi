/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOTitreMission;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderTitreMission {
	
	/**
	 * 
	 * @param ec
	 * @return
	 */

	public static NSArray findTitresMissionValides(EOEditingContext ec)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTitreMission.TIT_ETAT_KEY + " = %@" , new NSArray("VALIDE")));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("titOrdre > 0", null));

		EOFetchSpecification	fs = new EOFetchSpecification(EOTitreMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

	
	public static NSArray findTitresMission(EOEditingContext ec)	{
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOTitreMission.TIT_ETAT_KEY, EOSortOrdering.CompareDescending));
		mySort.addObject(new EOSortOrdering(EOTitreMission.TIT_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		
		EOFetchSpecification	fs = new EOFetchSpecification(EOTitreMission.ENTITY_NAME, null, mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTitreMission findDefaultTitreMission(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("titOrdre = 1", null);

		EOFetchSpecification	fs = new EOFetchSpecification(EOTitreMission.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		try {return (EOTitreMission)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
	}
	
}
