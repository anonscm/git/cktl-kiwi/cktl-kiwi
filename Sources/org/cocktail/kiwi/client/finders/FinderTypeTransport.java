/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EOTypeTransport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderTypeTransport {
	
	
	public static EOTypeTransport findTypeTransport(EOEditingContext ec, String type )	{
		
		EOQualifier myQualifier= EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TTR_TYPE_KEY + " = %@", new NSArray(type));
		
		EOFetchSpecification	fs = new EOFetchSpecification(EOTypeTransport.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return (EOTypeTransport)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		
	}
	
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTypesTransport(EOEditingContext ec)	{
		
		EOFetchSpecification	fs = new EOFetchSpecification(EOTypeTransport.ENTITY_NAME, null, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTypesTransportValides(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TEM_VALIDE_KEY + " = %@", new NSArray("O"));
		
		EOFetchSpecification	fs = new EOFetchSpecification(EOTypeTransport.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTypesTransportVehicule(EOEditingContext ec)	{
		
		NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EOTypeTransport.TTR_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TTR_TYPE_KEY + " = %@", new NSArray(EOTypeTransport.VEHICULE_LOCATION)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TTR_TYPE_KEY + " = %@", new NSArray(EOTypeTransport.VEHICULE_SERVICE)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TTR_TYPE_KEY + " = %@", new NSArray(EOTypeTransport.VEHICULE_PERSONNEL)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TTR_TYPE_KEY + " = %@", new NSArray(EOTypeTransport.VEHICULE_SNCF)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TTR_TYPE_KEY + " = %@", new NSArray(EOTypeTransport.VELOMOTEUR)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeTransport.TTR_TYPE_KEY + " = %@", new NSArray(EOTypeTransport.MOTOCYCLETTE)));

		EOFetchSpecification	fs = new EOFetchSpecification(EOTypeTransport.ENTITY_NAME, new EOOrQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	
	
}
