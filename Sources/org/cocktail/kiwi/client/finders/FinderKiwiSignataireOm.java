/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireOm;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderKiwiSignataireOm {
	
	
	public static String getStringSignataires(EOEditingContext ec, EOOrgan organ, String nbSignataires) {
		
		String signataires = "";
		
		NSArray signatairesOm = getOrganSignataires(ec, organ);
			
		if (signatairesOm.count() > 0) {

			if ("1".equals(nbSignataires)) {	//  1 seul signataire

				EOIndividu individu = ((EOKiwiSignataireOm)signatairesOm.objectAtIndex(0)).individu();
				signataires = signataires + individu.cCivilite()+" "+individu.prenom()+" "+individu.nomUsuel();
				
			}
			else {		// On affiche tous les signataires
			
				for (int i = 0;i< signatairesOm.count();i++) {
					EOIndividu individu = ((EOKiwiSignataireOm)signatairesOm.objectAtIndex(i)).individu();
					signataires = signataires + individu.cCivilite()+" "+individu.prenom()+" "+individu.nomUsuel()+"\n";
				}

				return signataires.substring(0, (signataires.length()-1));

			}
			
		}
		
		return signataires;
	}

	
	
    public static NSArray getOrganSignataires(EOEditingContext ec, final EOOrgan organ) {
        final NSMutableArray signataires = new NSMutableArray();  
        EOOrgan org = organ;
        
        while (signataires.count()==0 && org != null ) {

            NSArray organSignataires = findSignatairesForOrgan(ec, org);
            
            NSMutableArray mySort = new NSMutableArray();
            mySort.addObject(new EOSortOrdering(EOKiwiSignataireOm.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareDescending));
            
            organSignataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(organSignataires, mySort);

            signataires.addObjectsFromArray(organSignataires);

            org = org.organPere(); 
        }
        return signataires.immutableClone();
    }

    
    
	
	
	/**
	 * 
	 * @param ec
	 * @param paiement
	 * @return
	 */
	public static NSArray findSignatairesForOrgan(EOEditingContext ec, EOOrgan organ)	{

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKiwiSignataireOm.ORGAN_KEY + " = %@", new NSArray(organ)));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOKiwiSignataireOm.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}

	
	
	/**
	 * 
	 * @param ec
	 * @param paiement
	 * @return
	 */
	public static EOKiwiSignataireOm findSignataireForIndividuAndOrgan(EOEditingContext ec, EOIndividu individu, EOOrgan organ)	{

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKiwiSignataireOm.ORGAN_KEY + " = %@", new NSArray(organ)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKiwiSignataireOm.INDIVIDU_KEY + " = %@", new NSArray(individu)));

			EOFetchSpecification fs = new EOFetchSpecification(EOKiwiSignataireOm.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return (EOKiwiSignataireOm)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			
			return null;

		}
	}
	
	
}
