/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMotifs;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderMotifs {
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findMotifs(EOEditingContext ec)	{
		
		NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EOMotifs.MOT_LIBELLE_KEY,EOSortOrdering.CompareAscending));
		
		EOQualifier myQualifier = null;
		
		EOFetchSpecification	fs = new EOFetchSpecification(EOMotifs.ENTITY_NAME,myQualifier, mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOMotifs findMotifForLibelle(EOEditingContext ec, String libelle)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("motLibelle caseInsensitiveLike %@", new NSArray(libelle));

		EOFetchSpecification	fs = new EOFetchSpecification(EOMotifs.ENTITY_NAME,myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOMotifs)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}
}
