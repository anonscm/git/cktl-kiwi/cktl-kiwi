/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOGroupesMission;
import org.cocktail.kiwi.client.metier.EOMission;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderGroupesMission {
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOGroupesMission findDefaultGroupeMission(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("grmCode = '1'", null);

		EOFetchSpecification	fs = new EOFetchSpecification(EOGroupesMission.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		try {return (EOGroupesMission)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{e.printStackTrace();return null;}
	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOGroupesMission findGroupeForMission(EOEditingContext ec, EOMission mission)	{
		
		try {		
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("missions = %@", new NSArray(mission));
			
			EOFetchSpecification	fs = new EOFetchSpecification(EOGroupesMission.ENTITY_NAME, myQualifier, null);
			fs.setRefreshesRefetchedObjects(true);
			return (EOGroupesMission)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOGroupesMission findGroupeForNumero(EOEditingContext ec, String numero)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("grmCode = %@", new NSArray(numero));

		EOFetchSpecification	fs = new EOFetchSpecification(EOGroupesMission.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		try {return (EOGroupesMission)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{e.printStackTrace();return null;}
	}
}
