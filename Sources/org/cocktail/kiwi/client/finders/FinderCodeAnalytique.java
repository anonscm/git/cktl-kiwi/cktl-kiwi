/*
 * Copyright COCKTAIL, 1995-2008 This software is governed by the CeCILL license
 * under French law and abiding by the rules of distribution of free software.
 * You can use, modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kiwi.client.finders;


import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytiqueOrgan;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderCodeAnalytique {
		
	
	/**
	 * 
	 * Retourne tous les codes analytiques d'un exercice donne
	 * 
	 * @param ec
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static final NSArray getCodesAnalytiques (EOEditingContext ec, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));			

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_KEY + " = %@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_VALIDE))));			

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_UTILISABLE_KEY + " = %@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_UTILISABLE))));			

		EOFetchSpecification fs = new EOFetchSpecification(EOCodeAnalytique.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setUsesDistinct(true);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	
	/**
	 * 
	 * Retourne les codes analytiques en fonction d'une exercice donne et d'une ligne budgetaire
	 * 
	 * Codes analytiques Valides ET Utilisables ET (Publics ou associes a la ligne budgetaire) )
	 * 
	 * @param ec
	 * @param organ
	 * @param exercice
	 * @return NSArray
	 */
	public static final NSArray getCodesAnalytiques (EOEditingContext ec, EOOrgan organ, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));			

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_KEY + " = %@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_VALIDE))));			

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_UTILISABLE_KEY + " = %@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_UTILISABLE))));			

		NSMutableArray qualifsPublic = new NSMutableArray();
		qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY + " = %@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_PUBLIC))));			

		if (organ != null) {

			NSMutableArray args = new NSMutableArray();

			args.addObject(organ.orgUb());

			//qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + " = %@", new NSArray(organ)));			
			qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY + " = %@ and codeAnalytiqueOrgan.organ.orgNiveau = 2", args));			

			args.addObject(organ.orgCr());
			qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY + " = %@ and " +
					EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY + " = %@ " +
					" and codeAnalytiqueOrgan.organ.orgNiveau = 3", args));			


			if (organ.orgSouscr() != null) {
				args.addObject(organ.orgSouscr());
				qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY + " = %@ and " +
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY + " = %@ and " +
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_SOUSCR_KEY + " = %@ " +
						" and codeAnalytiqueOrgan.organ.orgNiveau = 4", args));			
			}

		}

		mesQualifiers.addObject(new EOOrQualifier(qualifsPublic));

		EOFetchSpecification fs = new EOFetchSpecification(EOCodeAnalytique.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setUsesDistinct(true);
				
		return ec.objectsWithFetchSpecification(fs);
		
	}
	

    /**
     * Methode de tri.<BR>
     * @return
     *        un NSArray contenant des EOSortOrdering
     */
    private static NSArray sort() {
    	return new NSArray(EOSortOrdering.sortOrderingWithKey(EOCodeAnalytique.CAN_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
    }
    
    
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOCodeAnalytique findCodeAnalytiqueForCode(EOEditingContext ec, String code)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		try {
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.CAN_CODE_KEY + " = %@", new NSArray(code)));
			
			EOFetchSpecification	fs = new EOFetchSpecification(EOCodeAnalytique.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
			
			return (EOCodeAnalytique)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
//			e.printStackTrace();
			return null;
		}
	}
	
}
