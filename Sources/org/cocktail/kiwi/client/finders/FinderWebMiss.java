/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOWebmiss;
import org.cocktail.kiwi.client.metier.EOWebpays;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderWebMiss {
	

	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOWebmiss findWebMissForPaysAndDate(EOEditingContext ec, EOWebpays pays, NSTimestamp dateReference)	{

		NSMutableArray mesQualifiers = new NSMutableArray();		

		try {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebmiss.WEBPAYS_KEY + "=%@", new NSArray(pays)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebmiss.WMI_DEBUT_KEY + " <=%@", new NSArray(dateReference)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("("+EOWebmiss.WMI_FIN_KEY + " >=%@ or " + EOWebmiss.WMI_FIN_KEY + " = nil )", new NSArray(dateReference)));

			EOFetchSpecification	fs = new EOFetchSpecification(EOWebmiss.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray miss = ec.objectsWithFetchSpecification(fs);

			return (EOWebmiss)miss.objectAtIndex(0);
		}
		catch (Exception e)	{return null;}
	}
	
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOWebmiss findWebMissForPaysAndPeriode(EOEditingContext ec, EOWebpays pays, NSTimestamp debut, NSTimestamp fin)	{

		NSMutableArray mesQualifiers = new NSMutableArray();		

		if (fin == null)
			fin = DateCtrl.stringToDate("01/01/3000");
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebmiss.WEBPAYS_KEY + "=%@", new NSArray(pays)));

		NSMutableArray args = new NSMutableArray();
		args.addObject(debut);
		args.addObject(fin);
		args.addObject(fin);
		args.addObject(debut);
		
		try {	
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(wmiFin>=%@ and wmiFin<=%@)" +
					"or" + "(wmiDebut<=%@ and wmiDebut>=%@)", args));
			
			EOFetchSpecification	fs = new EOFetchSpecification(EOWebmiss.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray miss = ec.objectsWithFetchSpecification(fs);
			
			
			// Aucune donnee trouvee - Recherche simple
			if (miss.count() == 0)	{
				
				mesQualifiers.removeAllObjects();
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebmiss.WEBPAYS_KEY + " = %@", new NSArray(pays)));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebmiss.WMI_DEBUT_KEY + " <= %@", new NSArray(debut)));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebmiss.WMI_FIN_KEY + " >= %@", new NSArray(fin)));
				fs = new EOFetchSpecification(EOWebmiss.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				miss = ec.objectsWithFetchSpecification(fs);
				
			}
			
			return (EOWebmiss)miss.objectAtIndex(0);
		}
		catch (Exception e)	{return null;}
	}

	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findWebsMissForPaysAndPeriode(EOEditingContext ec, EOWebpays pays, NSTimestamp debut, NSTimestamp fin)	{

		NSMutableArray mesQualifiers = new NSMutableArray();		

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("webpays = %@", new NSArray(pays)));

		NSMutableArray args = new NSMutableArray();
		args.addObject(debut);
		args.addObject(fin);
		args.addObject(fin);
		args.addObject(debut);
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(wmiFin>=%@ and wmiFin<=%@)" +
				"or" + "(wmiDebut<=%@ and wmiDebut>=%@)", args));
				
		EOFetchSpecification	fs = new EOFetchSpecification(EOWebmiss.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		NSArray miss = ec.objectsWithFetchSpecification(fs);

		// Aucune donnee trouvee - Recherche simple
		if (miss.count() == 0)	{
			try {		
			mesQualifiers.removeAllObjects();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("webpays = %@", new NSArray(pays)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("wmiDebut <= %@", new NSArray(debut)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("wmiFin >= %@", new NSArray(fin)));

			System.out
			.println("FinderRembJournalier.findWebsMissForPaysAndPeriode() - QUALIF WEBMISS : " + new EOAndQualifier(mesQualifiers));

			fs = new EOFetchSpecification(EOWebmiss.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			miss = ec.objectsWithFetchSpecification(fs);

			System.out
			.println("FinderRembJournalier.findWebsMissForPaysAndPeriode() - WEBMISS COUNT : " + miss.count());

			}
			catch (Exception e)	{
				e.printStackTrace();
				return new NSArray();
			}
		}

		return miss;
	}
	

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findWebsMissForPays(EOEditingContext ec, EOWebpays pays)	{

		try {
			
			NSArray mySort = new NSArray(new EOSortOrdering(EOWebmiss.WMI_DEBUT_KEY, EOSortOrdering.CompareAscending));
			
			NSMutableArray mesQualifiers =new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("webpays = %@", new NSArray(pays)));

			EOFetchSpecification	fs = new EOFetchSpecification(EOWebmiss.ENTITY_NAME, null, mySort);
			fs.setRefreshesRefetchedObjects(true);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}

	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findWebsMiss(EOEditingContext ec)	{

		try {
						
			NSMutableArray mesQualifiers =new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOWebmiss.WEBPAYS_KEY + " != nil", null));

			EOFetchSpecification	fs = new EOFetchSpecification(EOWebmiss.ENTITY_NAME, null, null);
			
			fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOWebmiss.WEBPAYS_KEY));
			
			fs.setRefreshesRefetchedObjects(true);
			
			return ec.objectsWithFetchSpecification(fs);
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}

	}



}
