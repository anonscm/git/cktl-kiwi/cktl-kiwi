/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOTypeEtat;
import org.cocktail.kiwi.client.metier.EOVisaAvMission;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderVisaAvMission {
	
	
/**
 *  
 * @param ec
 * @param fournis
 * @param exercice
 * @return
 */
	public static NSArray findAvancesForMission (EOEditingContext ec, EOMission mission)	{
	
		NSMutableArray mesQualifiers = new NSMutableArray();
				
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.MISSION_KEY + " = %@", new NSArray(mission)));

		EOFetchSpecification fs = new EOFetchSpecification(EOVisaAvMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		
		return ec.objectsWithFetchSpecification(fs);
	}
	
	/**
	 *  
	 * @param ec
	 * @param fournis
	 * @param exercice
	 * @return
	 */
		public static NSArray findAvancesForPaiement (EOEditingContext ec, EOMission mission)	{
		
			NSMutableArray mesQualifiers = new NSMutableArray();
					
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + " != %@", new NSArray("REJETE")));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.MISSION_KEY + " = %@", new NSArray(mission)));

			EOFetchSpecification fs = new EOFetchSpecification(EOVisaAvMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return ec.objectsWithFetchSpecification(fs);
		}

		
}
