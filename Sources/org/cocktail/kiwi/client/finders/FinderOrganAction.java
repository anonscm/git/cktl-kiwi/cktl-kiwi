/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.metier.budget.EOOrganAction;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderOrganAction {
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray getActions(EOEditingContext ec,EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit)	{
		
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();
					
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrganAction.TO_EXERCICE_KEY +" = %@", new NSArray(exercice)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrganAction.TYPE_ACTION_KEY + "." + EOLolfNomenclatureDepense.TO_EXERCICE_KEY +" = %@", new NSArray(exercice)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrganAction.TO_TYPE_CREDIT_KEY + " = %@", new NSArray(typeCredit)));
	
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrganAction.ORGAN_KEY +" = %@", new NSArray(organ)));
				
			EOFetchSpecification	fs = new EOFetchSpecification(EOOrganAction.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			return new NSArray();
		}
		
	}
	
	}
