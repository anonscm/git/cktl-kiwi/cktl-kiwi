/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EOConventionLimitative;
import org.cocktail.kiwi.client.metier.budget.EOConventionNonLimitative;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderConvention {
	
	
	private static EOQualifier qualValide() {
		return EOQualifier.qualifierWithQualifierFormat(EOConvention.TEM_VALIDE_KEY + "=%@", new NSArray("O"));
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @return
	 */
	public static NSArray findConventionsLimitatives(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit) {
		try {
			
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(qualValide());
			
			if (organ != null && typeCredit != null)	{
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONVENTION_LIMITATIVES_KEY + "."
						+ EOConventionLimitative.TO_EXERCICE_KEY + " = %@", new NSArray(new Object[] { exercice })));
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONVENTION_LIMITATIVES_KEY + "."
						+ EOConventionLimitative.ORGAN_KEY + " = %@", new NSArray(new Object[] { organ })));
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONVENTION_LIMITATIVES_KEY + "."
						+ EOConventionLimitative.TO_TYPE_CREDIT_KEY + " = %@", new NSArray(typeCredit)));
			}
			
			EOFetchSpecification fs = new EOFetchSpecification(EOConvention.ENTITY_NAME, new EOAndQualifier(quals), null);
			
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @return
	 */
	public static NSArray findConventionsNonLimitatives(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit) {
		try {
			
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(qualValide());
			
			if (organ != null && typeCredit != null)	{
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "."
						+ EOConventionNonLimitative.TO_EXERCICE_KEY + " = %@", new NSArray(new Object[] { exercice })));
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "."
						+ EOConventionNonLimitative.ORGAN_KEY + " = %@", new NSArray(new Object[] { organ })));
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "."
						+ EOConventionNonLimitative.TO_TYPE_CREDIT_KEY + " = %@", new NSArray(typeCredit)));
			}
			
			EOFetchSpecification fs = new EOFetchSpecification(EOConvention.ENTITY_NAME, new EOAndQualifier(quals), null);
			
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

}
