/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOPlageHoraire;
import org.cocktail.kiwi.client.metier.EORembJournalier;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderRembJournalier {


	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findRembsForZoneAndPeriode(EOEditingContext ec, EORembZone zone, Number tranche, NSTimestamp debut, NSTimestamp fin)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EORembJournalier.REM_DATE_KEY,EOSortOrdering.CompareAscending));
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (zone != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_ZONE_KEY + " = %@", new NSArray(zone)));		

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_PLAGE_HORAIRE_KEY+"."+EOPlageHoraire.PLA_ORDRE_KEY +  "= %@", new NSArray(tranche)));

		NSMutableArray args = new NSMutableArray();
		args.addObject(DateCtrl.stringToDate(DateCtrl.dateToString(debut)));
		args.addObject(DateCtrl.stringToDate(DateCtrl.dateToString(fin)));
		args.addObject(DateCtrl.stringToDate(DateCtrl.dateToString(fin)));
		args.addObject(DateCtrl.stringToDate(DateCtrl.dateToString(debut)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("" +
				"(remDateFin>=%@ and remDateFin<=%@)" +
				"or" + 
				"(remDate<=%@ and remDate>=%@)", args));

		EOFetchSpecification fs = new EOFetchSpecification(EORembJournalier.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);

		NSArray<EORembJournalier> rembs = ec.objectsWithFetchSpecification(fs);

		if (rembs.count() == 0)	{

			mesQualifiers.removeAllObjects();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_ZONE_KEY + " = %@", new NSArray(zone)));		
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_PLAGE_HORAIRE_KEY+"."+EOPlageHoraire.PLA_ORDRE_KEY +  "= %@", new NSArray(tranche)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.REM_DATE_KEY + " <= %@", new NSArray(debut)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.REM_DATE_FIN_KEY + " >= %@ or remDateFin = nil)", new NSArray(fin)));
			fs = new EOFetchSpecification(EORembJournalier.ENTITY_NAME,new EOAndQualifier(mesQualifiers), mySort);
			rembs = ec.objectsWithFetchSpecification(fs);						

		}

		return rembs;
	}


	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EORembJournalier findLastTaux(EOEditingContext ec, Integer plaOrdre, String remTaux)	{

		try {
			NSArray mySort = new NSArray(new EOSortOrdering(EORembJournalier.REM_DATE_KEY,EOSortOrdering.CompareDescending));

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_PLAGE_HORAIRE_KEY+"." + EOPlageHoraire.PLA_ORDRE_KEY + " = %@", new NSArray(plaOrdre)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_ZONE_KEY+"."+EORembZone.REM_TAUX_KEY + " = %@", new NSArray(remTaux)));

			EOFetchSpecification fs = new EOFetchSpecification(EORembJournalier.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);

			return (EORembJournalier)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);

		}
		catch (Exception ex) {
			return null;
		}
	}


	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EORembJournalier> findRemboursements(EOEditingContext ec, Number tranche)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EORembJournalier.REM_DATE_KEY,EOSortOrdering.CompareDescending));
		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_PLAGE_HORAIRE_KEY+"." + EOPlageHoraire.PLA_ORDRE_KEY + " = %@", new NSArray(tranche)));

		return EORembJournalier.fetchAll(ec, new EOAndQualifier(mesQualifiers), mySort);

	}

	/**
	 * 
	 * @param ec
	 * @param zone
	 * @param tranche
	 * @return
	 */
	public static NSArray<EORembJournalier> findRemboursementsForZone(EOEditingContext ec, EORembZone zone, Integer tranche)	{

		try {
			NSArray mySort = new NSArray(new EOSortOrdering(EORembJournalier.REM_DATE_KEY,EOSortOrdering.CompareDescending));

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_ZONE_KEY + " = %@", new NSArray(zone)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_PLAGE_HORAIRE_KEY+"." + EOPlageHoraire.PLA_ORDRE_KEY + " = %@", new NSArray(tranche)));

			EOFetchSpecification fs = new EOFetchSpecification(EORembJournalier.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);

			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EORembJournalier findRembForZoneAndPeriode(EOEditingContext ec, EORembZone zone, Number tranche, NSTimestamp debut, NSTimestamp fin)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_ZONE_KEY + " = %@", new NSArray(zone)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_PLAGE_HORAIRE_KEY+"." + EOPlageHoraire.PLA_ORDRE_KEY + " = %@", new NSArray(tranche)));

		NSMutableArray args = new NSMutableArray();
		args.addObject(debut);
		args.addObject(fin);
		args.addObject(fin);
		args.addObject(debut);

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(remDateFin>=%@ and remDateFin<=%@)" +
				"or" + "(remDate<=%@ and remDate>=%@)", args));

		EOFetchSpecification fs = new EOFetchSpecification(EORembJournalier.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		NSArray rembs = ec.objectsWithFetchSpecification(fs);

		// Aucune donnee trouvee, on fait une recherche plus simple
		if (rembs.count() == 0)	{
			try {
				mesQualifiers.removeAllObjects();
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_ZONE_KEY + " = %@", new NSArray(zone)));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORembJournalier.TO_PLAGE_HORAIRE_KEY+"." + EOPlageHoraire.PLA_ORDRE_KEY + " = %@", new NSArray(tranche)));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("remDate <= %@", new NSArray(debut)));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("remDateFin >= %@", new NSArray(fin)));
				fs = new EOFetchSpecification(EORembJournalier.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
				rembs = ec.objectsWithFetchSpecification(fs);						
			}
			catch (Exception e)	{
				e.printStackTrace();
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Probleme de recherche dans la table RembJournalier");
			}
		}

		try {return (EORembJournalier)rembs.objectAtIndex(0);}
		catch (Exception e)	{return null;}
	}

}
