/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOPlageHoraire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderPlageHoraire {
	
	
	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static EOPlageHoraire findPlageHoraire(EOEditingContext ec, Number cle)	{

		try {
			
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOPlageHoraire.PLA_ORDRE_KEY + " = %@", new NSArray(cle));
						
			EOFetchSpecification fs = new EOFetchSpecification(EOPlageHoraire.ENTITY_NAME, myQualifier,null);
			return (EOPlageHoraire)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	

	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static NSArray findPlagesHoraires(EOEditingContext ec)	{

		try {
			EOFetchSpecification fs = new EOFetchSpecification(EOPlageHoraire.ENTITY_NAME,null,null);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			return new NSArray();
		}

	}
	
}
