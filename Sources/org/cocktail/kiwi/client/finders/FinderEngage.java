/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.budget.EOEngage;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderEngage {

	/**
	 * 
	 * @param ec
	 * @param paiement
	 * @return
	 */
	public static NSArray findEngagementsForPaiementEngage(EOEditingContext ec, EOMissionPaiementEngage paiement)	{

		try {
			NSMutableArray args = new NSMutableArray();
			args.addObject(paiement);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOEngage.ENGAGEMENTS_KEY + " = %@", args);
			EOFetchSpecification fs = new EOFetchSpecification(EOEngage.ENTITY_NAME,myQualifier, null);

			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param paiement
	 * @return
	 */
	public static EOEngage findEngagementForCommandeAndAnnee(EOEditingContext ec, Number cdeOrdre, Number annee)	{

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("cdeOrdre = %@" , new NSArray(cdeOrdre)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("annee = %@" , new NSArray(annee)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("engStat != 'A'" , null));

			EOFetchSpecification fs = new EOFetchSpecification(EOEngage.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			return (EOEngage)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {return null;}
	}

	/**
	 * 
	 * @param ec
	 * @param paiement
	 * @return
	 */
	public static EOEngage findEngagementForPaiementEngage(EOEditingContext ec, EOMissionPaiementEngage paiement)	{

		try {

			NSMutableArray args = new NSMutableArray();
			args.addObject(paiement);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOEngage.ENGAGEMENTS_KEY + " = %@", args);
			EOFetchSpecification fs = new EOFetchSpecification(EOEngage.ENTITY_NAME,myQualifier, null);

			NSArray engages = ec.objectsWithFetchSpecification(fs);

			if (engages.count() > 0)
				return (EOEngage)engages.objectAtIndex(0);

			return null;
		}
		catch (Exception e) {e.printStackTrace();return null;}
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findEngagementsForAnnee(EOEditingContext ec, Number annee)	{

		NSMutableArray args = new NSMutableArray();
		args.addObject(annee);
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("annee = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(EOEngage.ENTITY_NAME,myQualifier, null);

		return ec.objectsWithFetchSpecification(fs);

	}
}
