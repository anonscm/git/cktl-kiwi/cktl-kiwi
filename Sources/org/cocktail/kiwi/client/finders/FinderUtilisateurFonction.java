package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.eof.EOUtilisateurFonction;
import org.cocktail.kiwi.client.metier.EOFonction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderUtilisateurFonction {

	private static Number TYAP_ID_DEPENSE = new Integer(3);
	private static Number TYAP_ID_KIWI = new Integer(7);

	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findFonctionsForUtilisateurAndApplication(EOEditingContext ec, EOUtilisateur utilisateur, EOExercice exercice, Number tyapId)	{
		
		try {
		NSMutableArray args = new NSMutableArray();
		args.addObject(utilisateur);
		args.addObject(tyapId);
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.UTILISATEUR_KEY + " = %@ and " +
				EOUtilisateurFonction.FONCTION_KEY + "." + EOFonction.TYAP_ID_KEY + " = %@", args);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateurFonction.ENTITY_NAME, myQualifier, null);
		
		return (NSArray)ec.objectsWithFetchSpecification(fs).valueForKey(EOUtilisateurFonction.FONCTION_KEY);
		}
		catch (Exception e) {
			return new NSArray();
		}
		
	}
	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findFonctionsForUtilisateur(EOEditingContext ec, EOUtilisateur utilisateur, EOExercice exercice)	{
		
		NSMutableArray args = new NSMutableArray();
		args.addObject(utilisateur);
		args.addObject(TYAP_ID_KIWI);
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateur = %@ and fonction.tyapId = %@", args);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateurFonction.ENTITY_NAME, myQualifier, null);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
}
