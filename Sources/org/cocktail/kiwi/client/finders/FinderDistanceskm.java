/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EODistancesKm;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderDistanceskm {
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTrajets(EOEditingContext ec)	{
		
		NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EODistancesKm.LIEU_DEPART_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EODistancesKm.LIEU_ARRIVEE_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier myQualifier = null;
		
		EOFetchSpecification	fs = new EOFetchSpecification(EODistancesKm.ENTITY_NAME,myQualifier, mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	
}
