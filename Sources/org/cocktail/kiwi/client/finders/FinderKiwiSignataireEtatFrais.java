/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOKiwiSignataireEtatFrais;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.metier.budget.EOOrganSignataire;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderKiwiSignataireEtatFrais {
	
	
	public static String getStringSignataires(EOEditingContext ec, EOOrgan organ, String nbSignataires) {
		
		String signataires = "";
		
		NSArray signatairesEtatFrais = getOrganSignataires(ec, organ);
			
		if (signatairesEtatFrais.count() > 0) {

			if ("1".equals(nbSignataires)) {	//  1 seul signataire

				EOIndividu individu = ((EOKiwiSignataireEtatFrais)signatairesEtatFrais.objectAtIndex(0)).individu();
				signataires = signataires + individu.cCivilite()+" "+individu.prenom()+" "+individu.nomUsuel();
				
			}
			else {		// On affiche tous les signataires
			
				for (int i = 0;i< signatairesEtatFrais.count();i++) {
					EOIndividu individu = ((EOKiwiSignataireEtatFrais)signatairesEtatFrais.objectAtIndex(i)).individu();
					signataires = signataires + individu.cCivilite()+" "+individu.prenom()+" "+individu.nomUsuel()+"\n";
				}

				return signataires.substring(0, (signataires.length()-1));

			}
			
		}
		
		return signataires;
	}

	
	
    public static NSArray getOrganSignataires(EOEditingContext ec, final EOOrgan organ) {
        final NSMutableArray signataires = new NSMutableArray();  
        EOOrgan org = organ;
        
        while (signataires.count()==0 && org != null ) {

            NSArray organSignataires = findSignatairesForOrgan(ec, org);
            
            NSMutableArray mySort = new NSMutableArray();
            mySort.addObject(new EOSortOrdering(EOOrganSignataire.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareDescending));
            
            organSignataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(organSignataires, mySort);

            signataires.addObjectsFromArray(organSignataires);

            org = org.organPere(); 
        }
        return signataires.immutableClone();
    }

    
	/**
	 * 
	 * @param ec
	 * @param paiement
	 * @return
	 */
	public static NSArray findSignatairesForOrgan(EOEditingContext ec, EOOrgan organ)	{

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKiwiSignataireEtatFrais.ORGAN_KEY + " = %@", new NSArray(organ)));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOKiwiSignataireEtatFrais.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}

}
