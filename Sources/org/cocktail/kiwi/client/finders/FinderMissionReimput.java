/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionReimput;
import org.cocktail.kiwi.client.metier.EOReimputaiton;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderMissionReimput {
	
	public static NSArray findReimputations (EOEditingContext ec, EOExercice exercice) {
		
		try {
								
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionReimput.REIMPUTATION_KEY+"."+
						EOReimputaiton.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
			
			return EOMissionReimput.fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
		
		
		
	}
	

	public static EOMissionReimput findReimputation(EOEditingContext ec, EOMissionPaiement missionPaiement)	{

		try {
						
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
					EOMissionReimput.MISSION_PAIEMENT_KEY + " = %@", new NSArray(missionPaiement));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOMissionReimput.ENTITY_NAME,myQualifier, null);
			return (EOMissionReimput)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
}
