package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeEtat {

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTypeEtat findTypeEtat(EOEditingContext ec, String etat)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("tyetLibelle = %@", new NSArray(etat));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeEtat.ENTITY_NAME, myQualifier, null);
		
		try {return (EOTypeEtat)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
		
	}
	
}
