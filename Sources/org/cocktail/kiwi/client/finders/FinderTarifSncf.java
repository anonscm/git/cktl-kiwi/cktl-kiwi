/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOTarifSncf;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderTarifSncf {
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findTarifsForKilometres(EOEditingContext ec, Number kilometres)	{

		NSMutableArray args = new NSMutableArray();
				
		NSArray mySort = new NSArray(new EOSortOrdering(EOTarifSncf.PRIX_KM_KEY, EOSortOrdering.CompareAscending));
		
		args.addObject(kilometres);
		args.addObject(kilometres);
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("dtDebut <=%@ and dtFin >=%@", args);
		EOFetchSpecification	fs = new EOFetchSpecification(EOTarifSncf.ENTITY_NAME, myQualifier, mySort);
		fs.setRefreshesRefetchedObjects(true);
		
		return ec.objectsWithFetchSpecification(fs);
	}
	
}
