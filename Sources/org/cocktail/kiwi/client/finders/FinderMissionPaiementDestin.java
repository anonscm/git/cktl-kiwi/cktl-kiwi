/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMissionPaiementDestin;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderMissionPaiementDestin {
	
	public static NSArray findDestinationsForPaiementEngage(EOEditingContext ec, EOMissionPaiementEngage paiementEngage)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("missionPaiementEngage = %@", new NSArray(paiementEngage));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOMissionPaiementDestin.ENTITY_NAME,myQualifier, null);

		return ec.objectsWithFetchSpecification(fs);
	}

}
