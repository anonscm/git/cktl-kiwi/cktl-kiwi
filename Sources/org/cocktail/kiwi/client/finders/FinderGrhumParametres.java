
package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOGrhumParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderGrhumParametres {
		
    /**
     * 
     * @param f
     * @return
     */
	public static EOGrhumParametres parametrePourCle(EOEditingContext edc, String key) {
		try {
			
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + " = %@", new NSArray(key));	
			return EOGrhumParametres.fetchFirstByQualifier(edc, myQualifier);
						
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
		
	
}
