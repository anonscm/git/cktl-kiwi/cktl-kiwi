/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTransports;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderTransports {
	

	
	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static NSArray findTransportsForSegmentAndFournisAndExercice(EOEditingContext ec, EOSegmentTarif segment, 
			EOFournis fournis)	{
		
		try {

			NSArray mySort = new NSArray(new EOSortOrdering("segmentTarif.mission.misDebut", EOSortOrdering.CompareAscending));
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentTarif.mission.misEtat != %@", new NSArray(EOMission.ETAT_ANNULE)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentTarif.mission.fournis = %@", new NSArray(fournis)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("vehicule.fournis = %@", new NSArray(fournis)));
			
			NSTimestamp tmp = DateCtrl.stringToDate("31/12/" + DateCtrl.getYear(segment.mission().misDebut()) + " 23:59:59", "%d/%m/%Y %H:%M:%S");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentTarif.mission.misDebut <= %@", new NSArray(segment.segFin())));
//			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentTarif.mission.misDebut <= %@", new NSArray(tmp)));
			
			tmp = DateCtrl.stringToDate("01/01/" + DateCtrl.getYear(segment.mission().misDebut()) + " 00:00:00", "%d/%m/%Y %H:%M:%S");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentTarif.mission.misFin >= %@", new NSArray(tmp)));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOTransports.ENTITY_NAME,new EOAndQualifier(mesQualifiers),mySort);
			fs.setRefreshesRefetchedObjects(true);

			NSArray transports = ec.objectsWithFetchSpecification(fs);
						
			return transports;
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}
	
		
}
