/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;


import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOSegmentInfos;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderSegmentInfos {
	
	
	/**
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static int countAvances (EOEditingContext ec, EOMission mission) {
		
		int nbAvances = 0;
		
		NSArray segments = mission.segmentsInfos();
		
		for (int i=0;i<segments.count();i++) {

			EOSegmentInfos segment = (EOSegmentInfos)segments.objectAtIndex(i);
			
			if (org.cocktail.fwkcktlwebapp.common.util.StringCtrl.containsIgnoreCase(segment.segTypeInfo().stiLibelle(), "AVANCE"))
				return 1;

		}
		
		return nbAvances;
	}
	
	/**
	 * Recuperation des segments tarifs associes a une mission
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static NSArray findSegmentsInfosForMission(EOEditingContext ec, EOMission mission)	{
		
		try {

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegmentInfos.MISSION_KEY + " = %@", new NSArray(mission));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOSegmentInfos.ENTITY_NAME,myQualifier,null);
			return ec.objectsWithFetchSpecification(fs);
			
		}
		catch (Exception e)	{
			return new NSArray();
		}

	}
	
}
