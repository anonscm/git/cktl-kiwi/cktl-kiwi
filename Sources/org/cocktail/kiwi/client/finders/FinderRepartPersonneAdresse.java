/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.finders;

import org.cocktail.kiwi.client.metier.EORepartPersonneAdresse;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderRepartPersonneAdresse {
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EORepartPersonneAdresse findAdresse(EOEditingContext ec, Number persId, String type)	{
		
		try {

			NSArray mySort = new NSArray(new EOSortOrdering(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY, EOSortOrdering.CompareDescending));
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("rpaValide = 'O'", null));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tadrCode = %@", new NSArray(type)));
			
			EOFetchSpecification	fs = new EOFetchSpecification(EORepartPersonneAdresse.ENTITY_NAME,new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);
			return (EORepartPersonneAdresse)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{
			return null;
		}
	}
	
}
