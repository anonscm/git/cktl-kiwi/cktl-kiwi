package org.cocktail.kiwi.client.finders;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.eof.EOUtilisateurOrgan;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderUtilisateurOrgan {

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findOrgansForUtilisateur(EOEditingContext ec, EOUtilisateur utilisateur, EOExercice exercice)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateurOrgan.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateurOrgan.ORGAN_KEY + "." + EOOrgan.ORG_NIVEAU_KEY + " > 2", null));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateurOrgan.ORGAN_KEY + "." + EOOrgan.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateurOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setFetchLimit(1);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
}
