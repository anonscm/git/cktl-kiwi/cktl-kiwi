/* AskForValeurs.java created by cpinsard on Tue 29-Apr-2003 */
package org.cocktail.kiwi.client;
////import papayeFwkEof.client.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class AskForValeurs extends EOModalDialogController 
{
	private static AskForValeurs sharedInstance;
	
	// Objets Graphiques
	public 	EOView				swapView, viewMontant, viewValeur, viewValeurs, viewLibelleLong, viewAvance;
	
	public 	JButton				btnValider, btnAnnuler;
	public 	JComboBox			popupValeurs, modesPaiement;
	public	JTextField			montant, montantAvance, valeur, titre, libelleLong;
	public  JRadioButton		op, mandat, manuel;
	
	protected		ApplicationClient	NSApp;
	protected 	EOEditingContext 			ec;

	private boolean	selectionValide;
	private	String	typeDemande;

/**
	*	
	*/
	public AskForValeurs()	{
		super();
 		
		EOArchive.loadArchiveNamed("AskForValeurs", this,"org.cocktail.kiwi.client",this.disposableRegistry());
		
 		NSApp = (ApplicationClient)EOApplication.sharedApplication();

		ec = NSApp.getEditingContext();
		
		initView();

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AskForValeurs sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new AskForValeurs();
		return sharedInstance;
	}
	


/**
	* Initialisation de tous les objets graphiques du module.
	* Mise en place des listeners, couleurs, boutons, EOTables, ...
	*/
	public void initView()	{
		
		swapView.setBorder(BorderFactory.createEmptyBorder());
		viewMontant.setBorder(BorderFactory.createEmptyBorder());
		viewValeur.setBorder(BorderFactory.createEmptyBorder());
		viewValeurs.setBorder(BorderFactory.createEmptyBorder());
		viewAvance.setBorder(BorderFactory.createEmptyBorder());

		viewLibelleLong.setBorder(BorderFactory.createEmptyBorder());

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_VALID, "Valider",btnValider,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL, "Annuler",btnAnnuler,"");

		montant.addActionListener(new ActionListenerMontant());
		valeur.addActionListener(new ActionListenerValeur());

		manuel.setSelected(true);
				
		window().addWindowListener(new localWindowListener());
		
	}

/**
 * 
 * @param title
 * @param montant
 * @param modePaiement
 * @param typeAvance
 * @return
 */
	public NSDictionary getModePaiementAvance(String title, String montant, EOModePaiement modePaiement, String typeAvance, EOExercice exercice)	{
		
		NSMutableDictionary dicoRetour = new NSMutableDictionary();

		modesPaiement.setVisible(true);

		// Initialisation des modes de paiement
		if (typeAvance.equals("DEP")) {
			modesPaiement.removeAllItems();
			NSArray modes = EOModePaiement.findModesPaiements(ec, exercice);
			for (int i=0;i<modes.count();i++)
				modesPaiement.addItem((EOModePaiement)modes.objectAtIndex(i));
		}
		else {	// OP : Modes de paiement ayant un pco_num_paiement not null
			modesPaiement.removeAllItems();
			NSArray modes = EOModePaiement.findModesPaiementAvances(ec, exercice);
			for (int i=0;i<modes.count();i++)
				modesPaiement.addItem((EOModePaiement)modes.objectAtIndex(i));			
		}
		
		if (modePaiement != null)
			modesPaiement.setSelectedItem(modePaiement);

		CocktailUtilities.initTextField(montantAvance, false, false);
		op.setEnabled(false);manuel.setEnabled(false);mandat.setEnabled(false);

		mandat.setSelected("DEP".equals(typeAvance));
		op.setSelected("OP".equals(typeAvance));
		
		titre.setText(title);
		CocktailUtilities.putView(swapView,viewAvance);

		typeDemande = "A";
		
		montantAvance.setText(montant);
		
		activateWindow();

		if (!selectionValide)	
			return null;

		dicoRetour.setObjectForKey((EOModePaiement)modesPaiement.getSelectedItem(), "modePaiement");
		return dicoRetour;
	}
	
	/** 
	*	Recuperation d'un montant saisi par l'utilisateur 
	*/
	public NSDictionary getMontantAvance(String title, String montant, String typeAvance)	{
		
		NSMutableDictionary dicoRetour = new NSMutableDictionary();
		
		CocktailUtilities.initTextField(montantAvance, true, true);
		op.setEnabled(false);manuel.setEnabled(false);mandat.setEnabled(false);
		
		modesPaiement.setVisible("OP".equals(typeAvance));

		// Initialisation des modes de paiement
		if (typeAvance.equals("OP")) {
			modesPaiement.removeAllItems();
			modesPaiement.addItem("*");
			NSArray modes = EOModePaiement.findModesPaiementAvances(ec, NSApp.getExerciceCourant());
			for (int i=0;i<modes.count();i++)
				modesPaiement.addItem((EOModePaiement)modes.objectAtIndex(i));			
		}
		
		mandat.setSelected("DEP".equals(typeAvance));
		op.setSelected("OP".equals(typeAvance));
		
		titre.setText(title);
		CocktailUtilities.putView(swapView,viewAvance);

		typeDemande = "A";
		
		if (montant != null)
			montantAvance.setText(montant);
		
		activateWindow();

		if (!selectionValide)	return null;

		montantAvance.setText((NSArray.componentsSeparatedByString(montantAvance.getText(),",")).componentsJoinedByString("."));

		try {
			dicoRetour.setObjectForKey((new BigDecimal(montantAvance.getText())).setScale(ApplicationClient.USED_DECIMALES,BigDecimal.ROUND_HALF_UP), "montant");

			if (modesPaiement.isVisible() && modesPaiement.getSelectedIndex() > 0)
				dicoRetour.setObjectForKey((EOModePaiement)modesPaiement.getSelectedItem(), "modePaiement");
			
			return dicoRetour;
		}
		catch (NumberFormatException e )	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur du format de saisie. Veuillez entrer un nombre valide !.");
			return null;
		}
	}
	
	
/** 
	*	Recuperation d'un montant saisi par l'utilisateur 
	*/
	public BigDecimal getMontant(String title)	{
		titre.setText(title);
		CocktailUtilities.putView(swapView,viewMontant);

		typeDemande = "M";
		montant.setText("");
		activateWindow();

		if (!selectionValide)	return null;

		montant.setText((NSArray.componentsSeparatedByString(montant.getText(),",")).componentsJoinedByString("."));

		try {
			return (new BigDecimal(montant.getText())).setScale(ApplicationClient.USED_DECIMALES,BigDecimal.ROUND_HALF_UP);
		}
		catch (NumberFormatException e )	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur du format de saisie. Veuillez entrer un nombre valide !.");
			return null;
		}
	}
	
/** 
	*	VALIDER 
	*/
	public void valider(Object sender)	{
		if ("M".equals(typeDemande))	{
			if ( ("".equals(StringCtrl.recupererChaine(montant.getText())))/* || (!NumberCtrl.isANumber(montant.getText())) */)	{
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur de saisie du montant !");
				return;
			}
		}
		else
			if ("V".equals(typeDemande))	{
				if ("".equals(StringCtrl.recupererChaine(valeur.getText())))	{
					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez saisir une valeur !");
					return;
				}
			}
			else
				if ("L".equals(typeDemande))	{
					
					if (StringCtrl.chaineVide(libelleLong.getText()))	{
						MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez saisir un libelle !");
						return;
					}
				}
				else
					if ("A".equals(typeDemande))	{
						if ( ("".equals(StringCtrl.recupererChaine(montantAvance.getText()))))	{
							MsgPanel.sharedInstance().runInformationDialog("ERREUR","Erreur de saisie du montant !");
							return;
						}
					}


		selectionValide = true;
		closeWindow();
	}


	public void annuler(Object sender)	{
		selectionValide = false;
		closeWindow();
	}

/** 
	*	Class listener sur la window principale 
	*/
	private class localWindowListener implements WindowListener	{
		public localWindowListener () 	{super();}

 		public void windowActivated(WindowEvent e) 	{
			if ("V".equals(typeDemande))	{	
				setWindowTitle(window(), "Saisissez une valeur");
				valeur.requestFocus();
			}
			else	{
				setWindowTitle(window(), "Saisissez un montant");
				montant.requestFocus();			
			}
		}

		public void windowClosed(WindowEvent e) {}

		public void windowOpened(WindowEvent e) 	{
			if ("V".equals(typeDemande))	{
				setWindowTitle(window(), "Saisissez une valeur");
				valeur.requestFocus();
			}
			else	{
				setWindowTitle(window(), "Saisissez un montant");
				montant.requestFocus();			
			}
		}

		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowClosing(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
	}

/** 
	*	Permet d'effectuer la completion pour la saisie de la date de fin de contrat (Appui sur ENTER)
	*/
	public class ActionListenerMontant implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			valider(this);
		}
	}

/** 
	*	Permet d'effectuer la completion pour la saisie de la date de fin de contrat (Appui sur ENTER)
	*/
	public class ActionListenerValeur implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			valider(this);
		}
	}

}