/*
 * Created on 19 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.trajets;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EORepas;
import org.cocktail.kiwi.client.nibctrl.SaisieRepasView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SaisieRepas {

	private static SaisieRepas sharedInstance;
	private EOEditingContext ec;

	private SaisieRepasView myView;

	private boolean cancelPanel;

	/** 
	 * Constructeur 
	 */
	public SaisieRepas(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new SaisieRepasView(new JFrame(), true);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieRepas sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieRepas(editingContext);
		return sharedInstance;
	}



	/**
	 * 
	 * @param repas
	 * @return
	 */
	public NSDictionary getRepas(EORepas repas)	{

		String paramModifTotal = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_CHECK_NB_REPAS);

		if ( (paramModifTotal != null && "O".equals(paramModifTotal)) || repas.segmentTarif().mission().isMissionPermanente() )
			CocktailUtilities.initTextField(myView.getTfNbRepas(), false, true);
		else
			CocktailUtilities.initTextField(myView.getTfNbRepas(), false, false);

		myView.getTfNbRepas().setText(repas.repNbRepas().toString());
		myView.getTfNbAdmin().setText(repas.repRepasAdm().toString());
		myView.getTfNbGratuits().setText(repas.repRepasGratuits().toString());

		myView.setVisible(true);

		if (!cancelPanel)	{
			NSMutableDictionary result = new NSMutableDictionary();

			result.setObjectForKey(new BigDecimal(myView.getTfNbRepas().getText()),"totalRepas");
			result.setObjectForKey(new BigDecimal(myView.getTfNbAdmin().getText()),"repasAdmin");
			result.setObjectForKey(new BigDecimal(myView.getTfNbGratuits().getText()),"repasGratuits");

			return result;
		}

		return null;
	}

	/**
	 * 
	 * @param sender
	 */
	public void valider()	{

		try {

			myView.getTfNbAdmin().setText((NSArray.componentsSeparatedByString(myView.getTfNbAdmin().getText(),",")).componentsJoinedByString("."));
			myView.getTfNbGratuits().setText((NSArray.componentsSeparatedByString(myView.getTfNbGratuits().getText(),",")).componentsJoinedByString("."));
			
			BigDecimal nbRepasAutres = new BigDecimal(myView.getTfNbGratuits().getText()).add(new BigDecimal(myView.getTfNbAdmin().getText()));

			if (nbRepasAutres.floatValue() > new BigDecimal(myView.getTfNbRepas().getText()).floatValue())	{
				MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Le nombre de repas (Gratuits + Admin) ne peut etre supérieur au nombre de repas total !");
				return;
			}

			cancelPanel = false;
			myView.dispose();

		}
		catch (Exception e) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Veuillez vérifier le format de saisie !");

		}
	}

	public void annuler()	{

		cancelPanel = true;
		myView.dispose();

	}

}
