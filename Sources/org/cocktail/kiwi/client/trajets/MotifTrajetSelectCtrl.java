package org.cocktail.kiwi.client.trajets;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.factory.Factory;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderMotifs;
import org.cocktail.kiwi.client.metier.EOMotifs;
import org.cocktail.kiwi.client.nibctrl.MotifTrajetSelectView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class MotifTrajetSelectCtrl { 
 
	private static MotifTrajetSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	private 	EODisplayGroup		eod;

	private MotifTrajetSelectView myView;

	private EOMotifs currentObject;
	
	/** 
	 *
	 */
	public MotifTrajetSelectCtrl (EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new MotifTrajetSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnAddMotif().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				nouveauMotif();
			}
		});

        myView.getTfFiltreMotif().getDocument().addDocumentListener(new ADocumentListener());

		myView.getMyEOTable().addListener(new Listener());

		// Suivant le parametrage on autorise ou non la saisie des motifs
		String paramMotifs = FinderMissionParametres.getValue(ec, "AJOUT_MOTIFS");
		if (paramMotifs != null && paramMotifs.equals("N")) {
			myView.getBtnAddMotif().setEnabled(false);
			CocktailUtilities.initTextField(myView.getTfNouveauMotif(), false, false);
		}

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static MotifTrajetSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new MotifTrajetSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 * @param sender
	 */
	public void nouveauMotif()	{
		
		if (StringCtrl.chaineVide(myView.getTfNouveauMotif().getText()))	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez saisir un libellé pour le nouveau motif !");
			return;
		}
		
		// On verifie que cette fonction n'existe pas dans la table PayeFonction.
		EOMotifs motif = FinderMotifs.findMotifForLibelle(ec, myView.getTfNouveauMotif().getText());
		
		if (motif == null)	{
			motif = (EOMotifs)Factory.instanceForEntity(ec, "Motifs");
			motif.setMotLibelle(myView.getTfNouveauMotif().getText());
			ec.insertObject(motif);		

			NSMutableArray motifs = new NSMutableArray(motif);
			motifs.addObjectsFromArray(eod.displayedObjects());
			eod.setObjectArray(motifs);
			myView.getMyEOTable().updateData();
			myView.getTfNouveauMotif().setText("");						
		}
		
		eod.updateDisplayedObjects();
		eod.setSelectedObject(motif);
		myView.getMyEOTable().forceNewSelection(eod.selectionIndexes());
	}
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public EOMotifs getMotif()	{

		eod.setObjectArray(FinderMotifs.findMotifs(ec));
		myView.getMyEOTable().updateData();
		
		myView.show();
	
		return currentObject;

	}
	
	
	/**
	 *
	 */
	public void annuler()	{
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		
        NSMutableArray mesQualifiers = new NSMutableArray();
                
        if (!StringCtrl.chaineVide(myView.getTfFiltreMotif().getText()))	{
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMotifs.MOT_LIBELLE_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfFiltreMotif().getText()+"*")));
        }
    
        return new EOAndQualifier(mesQualifiers);        
    }


	private void filter() {

	       eod.setQualifier(getFilterQualifier());
	       eod.updateDisplayedObjects();              
	       myView.getMyEOTable().updateData();

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class Listener implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOMotifs)eod.selectedObject();
		}
	}

}