/*
 * Created on 25 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.trajets;

import java.math.BigDecimal;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactoryIndemnite;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderWebMiss;
import org.cocktail.kiwi.client.metier.EOIndemnite;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOWebmiss;
import org.cocktail.kiwi.client.nibctrl.IndemnitesView;
import org.cocktail.kiwi.common.utilities.AskForBigDecimal;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class IndemnitesCtrl {

	private static IndemnitesCtrl sharedInstance;
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private IndemnitesView myView;

	public	EOView		view;

	public	EODisplayGroup eodIndemnites, eodTarifs, eodCalculAuto;

	private	EOSegmentTarif currentSegment;
	private EOIndemnite currentIndemnite;

	private String paramModifNuits, paramModifRepas;

	JScrollPane scrollPaneIndemnites, scrollPaneIndemnites2006;

	/** 
	 * Constructeur 
	 */
	public IndemnitesCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		eodIndemnites = new EODisplayGroup();
		eodCalculAuto = new EODisplayGroup();
		eodTarifs = new EODisplayGroup();

		myView = new IndemnitesView(eodIndemnites, eodCalculAuto, eodTarifs);

		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});

		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});

		myView.getBtnRecalculer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDefaultIndemnites();
			}
		});

		myView.getBtnSasie().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateMontantIndemnites();
			}
		});

		myView.getMyEOTableIndemnite().addListener(new ListenerIndemnites());
	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static IndemnitesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new IndemnitesCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel panel()	{
		return myView;
	}


	public void setCurrentSegment(EOSegmentTarif segment)	{
		currentSegment = segment;

		if (currentSegment != null) {
			paramModifNuits = FinderMissionParametres.getValue(ec, currentSegment.mission().toExercice(), EOMissionParametres.ID_CHECK_NB_NUITS);
			paramModifRepas = FinderMissionParametres.getValue(ec, currentSegment.mission().toExercice(), EOMissionParametres.ID_CHECK_NB_REPAS);
		}

		if (paramModifNuits == null) 
			paramModifNuits = EOMissionParametres.DEFAULT_VALUE_CHECK_NB_NUITS;
		if (paramModifRepas == null) 
			paramModifRepas = EOMissionParametres.DEFAULT_VALUE_CHECK_NB_REPAS;

	}

	public void actualiser()	{

		clean();

		if (currentSegment != null)	{

			eodIndemnites.setObjectArray(EOIndemnite.findForSegment(ec, currentSegment));

			eodCalculAuto.setObjectArray(EOIndemnite.findForSegment(ec, currentSegment));

			myView.getMyEOTableIndemnite().updateData();				
			myView.getMyEOTableCalculAuto().updateData();	
			myView.getMyTableModelCalculAuto().fireTableDataChanged();
		}

		updateUI();

	}

	/**
	 * 
	 *
	 */
	public void calculerMontants()	{

		try {
			currentIndemnite.calculerMontant();
			ec.saveChanges();
		}
		catch (Exception e)	{
			ec.revert();
		}

		myView.getMyEOTableIndemnite().updateUI();
		myView.getMyEOTableCalculAuto().updateUI();
	}

	/**
	 * 
	 *
	 */
	public void clean()	{
		eodIndemnites.setObjectArray(new NSArray());
		eodTarifs.setObjectArray(new NSArray());
		eodCalculAuto.setObjectArray(new NSArray());

		myView.getMyEOTableIndemnite().updateData();
		myView.getMyEOTableCalculAuto().updateData();
		myView.getMyEOTableTarif().updateData();
	}	

	/**
	 * 
	 * @param yn
	 */
	public void updateUI()	{

		String paramMontantIndemnites = null;
		
		if (currentSegment != null)
			paramMontantIndemnites = FinderMissionParametres.getValue(ec, currentSegment.mission().toExercice(), EOMissionParametres.ID_UPDATE_MONTANT_INDEMNITES);

		if (paramMontantIndemnites == null) 
			paramMontantIndemnites = EOMissionParametres.DEFAULT_VALUE_UPDATE_MONTANT_INDEMNITES;

		// Pas de saisies de transports
		if (
				currentSegment == null || currentSegment.mission() == null
				|| (currentSegment.mission().isPayee())
				|| currentSegment.mission().isAnnulee() // Mission ANNULEE ==> Pas de modifs
		) {

			setDisabled();
		}
		else	{	// On a un segment de defini, on peut ajouter des indemnites
			myView.getBtnAjouter().setVisible(eodIndemnites.displayedObjects().count() == 0);
			myView.getBtnModifier().setVisible(currentIndemnite != null);
			myView.getBtnRecalculer().setVisible(currentIndemnite != null);
			myView.getBtnSupprimer().setVisible(currentIndemnite != null);									
			myView.getBtnSasie().setVisible( currentIndemnite != null && currentIndemnite.canUpdateMontant() && "O".equals(paramMontantIndemnites));

			/*			btnAddNuit.setVisible(paramModifNuits.equals("O") && currentIndemnite != null);
			btnDelNuit.setVisible(paramModifNuits.equals("O") && currentIndemnite != null);
			btnAddRepas.setVisible(paramModifRepas.equals("O")&& currentIndemnite != null);
			btnDelRepas.setVisible(paramModifRepas.equals("O") && currentIndemnite != null);

			btnDelNuit.setEnabled(currentIndemnite != null && currentIndemnite.indNbNuits().intValue() > 0);
			btnDelRepas.setEnabled(currentIndemnite != null && currentIndemnite.indNbRepas().intValue() > 0);
			 */
		}		
	}

	/**
	 * 
	 *
	 */
	public void setDisabled()	{

		myView.getBtnAjouter().setVisible(false);
		myView.getBtnModifier().setVisible(false);
		myView.getBtnSupprimer().setVisible(false);
		myView.getBtnRecalculer().setVisible(false);
		myView.getBtnSasie().setVisible(false);

		/*		btnDelNuit.setVisible(false);
		btnDelRepas.setVisible(false);
		btnAddNuit.setVisible(false);
		btnAddRepas.setVisible(false);
		 */
	}


	/**
	 * 
	 * @param sender
	 */
	public void delNuit()	{

		try {

			BigDecimal nbJours = currentIndemnite.indJours().subtract(EOIndemnite.TAUX_NUIT_GRATUITE);

			currentIndemnite.setIndJours(nbJours.setScale(3, BigDecimal.ROUND_HALF_UP));

			currentIndemnite.setIndNbNuits(new Integer(currentIndemnite.indNbNuits().intValue() - 1));

			currentIndemnite.calculerMontant();

			ec.saveChanges();

			actualiser();

		}
		catch (Exception ex)	{
			ex.printStackTrace();
		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void addNuit()	{

		try {

			BigDecimal nbJours = currentIndemnite.indJours().add(EOIndemnite.TAUX_NUIT_GRATUITE);

			currentIndemnite.setIndJours(nbJours.setScale(3, BigDecimal.ROUND_HALF_UP));

			currentIndemnite.setIndNbNuits(new Integer(currentIndemnite.indNbNuits().intValue()+ 1));

			currentIndemnite.calculerMontant();

			ec.saveChanges();

			actualiser();

		}
		catch (Exception ex)	{
			ex.printStackTrace();
		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void addRepas()	{

		try {

			BigDecimal nbJours = currentIndemnite.indJours().add(EOIndemnite.TAUX_REPAS_GRATUIT);

			currentIndemnite.setIndJours(nbJours.setScale(3, BigDecimal.ROUND_HALF_UP));

			currentIndemnite.setIndNbRepas(new Integer(currentIndemnite.indNbRepas().intValue()+ 1));

			currentIndemnite.calculerMontant();

			ec.saveChanges();

			actualiser();
		}
		catch (Exception ex)	{
			ex.printStackTrace();

		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void delRepas()	{

		try {

			BigDecimal nbJours = currentIndemnite.indJours().subtract(EOIndemnite.TAUX_REPAS_GRATUIT);

			currentIndemnite.setIndJours(nbJours.setScale(3, BigDecimal.ROUND_HALF_UP));

			currentIndemnite.setIndNbRepas(new Integer(currentIndemnite.indNbRepas().intValue() - 1));

			currentIndemnite.calculerMontant();

			ec.saveChanges();

			actualiser();
		}
		catch (Exception ex)	{
			ex.printStackTrace();

		}

	}

	/**
	 * 
	 * @param sender
	 */
	public void ajouter()	{

		try {

			NSApp.setWaitCursor();

			EOWebmiss localWebMiss = (EOWebmiss)FinderWebMiss.findWebMissForPaysAndPeriode(ec, currentSegment.webpays(), currentSegment.segDebut(), currentSegment.segFin());

			if (localWebMiss == null)	{
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Il n'y a pas de zone définie pour le pays sélectionné :\n " + currentSegment.webpays().wpaLibelle()+".");
				return;
			}

			currentIndemnite = FactoryIndemnite.sharedInstance().creerIndemnites(ec, currentSegment, localWebMiss);

			currentIndemnite.calculerMontantDate(ec, currentSegment.segDebut(), currentSegment.segFin());
			currentIndemnite.calculerMontant();

			currentIndemnite.setIndMontantAuto(currentIndemnite.indMontantPaiement());

			ec.saveChanges();
			actualiser();
		}
		catch (com.webobjects.foundation.NSValidation.ValidationException e) {
			MsgPanel.sharedInstance().runErrorDialog("ATTENTION", "Erreur de calcul des indemnités !\n " + e.getMessage());
			ec.revert();
		}		
		finally	{
			NSApp.setDefaultCursor();
		}

	}

	/**
	 * 
	 * @param sender
	 */
	public void modifier()	{

		NSDictionary dicoSaisie = SaisieIndemnites.sharedInstance(ec).getJours(currentIndemnite);

		if (dicoSaisie != null)	{

			try {
				currentIndemnite.setIndJours((BigDecimal)dicoSaisie.objectForKey("jours"));

				currentIndemnite.setIndJoursGratuits(((BigDecimal)dicoSaisie.objectForKey("joursDeduits")).setScale(3, BigDecimal.ROUND_HALF_UP));

				currentIndemnite.setIndRepasGratuits((Integer)dicoSaisie.objectForKey("repasDeduits"));

				currentIndemnite.setIndNuitsGratuites((Integer)dicoSaisie.objectForKey("nuitsDeduites"));

				currentIndemnite.calculerMontant();

				ec.saveChanges();

				eodIndemnites.updateDisplayedObjects();

				myView.getMyEOTableIndemnite().updateUI();
				myView.getMyEOTableCalculAuto().updateUI();
			}
			catch (Exception e )	{
				e.printStackTrace();
				ec.revert();
				actualiser();
			}
		}
	}


	/**
	 * 
	 * @param sender
	 */
	public void updateMontantIndemnites()	{

		BigDecimal montantSaisi = new BigDecimal(0);

		try {

			montantSaisi = AskForBigDecimal.sharedInstance().getMontant("Modification Indemnités", "Montant des indemnités : ", currentIndemnite.indMontantPaiement());

			if (montantSaisi != null) {

				currentIndemnite.setIndMontantPaiement(montantSaisi);
				currentIndemnite.setIndMontantTotal(montantSaisi);

				currentIndemnite.setIndMontantSaisi(currentIndemnite.indMontantPaiement());

				ec.saveChanges();

				myView.getMyEOTableIndemnite().updateUI();				
				myView.getMyEOTableCalculAuto().updateUI();				

			}

		}
		catch (Exception e)	{
			e.printStackTrace();
		}

	}



	/**
	 * 
	 * @param sender
	 */
	public void getDefaultIndemnites()	{

		try {

			if (currentIndemnite != null)	{
				//				EOWebmiss localWebMiss = (EOWebmiss)FinderWebMiss.findWebMissForPaysAndPeriode(ec, currentSegment.webpays(), currentSegment.segDebut(), currentSegment.segFin());

				currentIndemnite.setIndRepasGratuits(new Integer(0));
				currentIndemnite.setIndNuitsGratuites(new Integer(0));

				currentIndemnite.calculerMontantDate(ec, currentSegment.segDebut(), currentSegment.segFin());
				currentIndemnite.calculerMontant();

				currentIndemnite.setIndMontantAuto(currentIndemnite.indMontantPaiement());
				currentIndemnite.setIndMontantSaisi(null);

				ec.saveChanges();

				eodIndemnites.updateDisplayedObjects();
				eodCalculAuto.updateDisplayedObjects();

				myView.getMyEOTableIndemnite().updateUI();
				myView.getMyEOTableCalculAuto().updateUI();

				updateUI();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ATTENTION", "Erreur de calcul des indemnités !\n " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param sender
	 */
	public void valider()	 {

		try {
			currentIndemnite.calculerMontant();
			currentIndemnite.setIndEtat("VALIDE");
			ec.saveChanges();
		}
		catch (Exception e)	{
			ec.revert();
			e.printStackTrace();
		}

		myView.getMyEOTableIndemnite().updateData();
	}

	/**
	 * 
	 * @param sender
	 */
	public void annuler()	{

		ec.revert();
		myView.getMyEOTableIndemnite().updateData();

	}

	/**
	 * 
	 * @param sender
	 */
	public void supprimer()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Supprimer la ligne 'Indemnité' sélectionnée ?",
				"OUI", "NON")) 
			return;

		try {

			currentIndemnite.removeObjectFromBothSidesOfRelationshipWithKey(currentSegment, EOIndemnite.SEGMENT_TARIF_KEY);

			ec.deleteObject(currentIndemnite);
			ec.saveChanges();

		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors de la suppression de l'indemnité sélectionnée !");
			ec.revert();
			e.printStackTrace();
		}
		actualiser();

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerIndemnites implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			modifier();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {			
			currentIndemnite = (EOIndemnite)eodIndemnites.selectedObject();

			// Mise a jour des tarifs
			if (currentIndemnite != null)	{

				eodTarifs.setObjectArray(new NSArray(currentIndemnite.webmiss()));
				myView.getMyEOTableTarif().updateData();
				myView.getMyTableModelTarif().fireTableDataChanged();	

			}

			updateUI();

		}
	}

}
