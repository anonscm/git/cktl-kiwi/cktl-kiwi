/*
 * Created on 25 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.trajets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.admin.SaisieVehiculeCtrl;
import org.cocktail.kiwi.client.factory.Factory;
import org.cocktail.kiwi.client.finders.FinderIndemniteKm;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderTarifSncf;
import org.cocktail.kiwi.client.finders.FinderTransports;
import org.cocktail.kiwi.client.metier.EODistancesKm;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOIndemniteKm;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTarifSncf;
import org.cocktail.kiwi.client.metier.EOTransports;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.metier.EOVehicule;
import org.cocktail.kiwi.client.select.TrajetsSelectCtrl;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GestionTransports extends EOInterfaceController implements ZEOTableListener , TableModelListener{
	
	private static GestionTransports sharedInstance;
	private ApplicationClient NSApp;
	private EOEditingContext ec;
	
	public 	EOView view, vueKms, viewClasseSncf, viewIndemnites, viewDetail;
	
	public	EOView	viewTable;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;
	private Vector myCols;
	
	public EODisplayGroup eodTransports;
	
	public JButton btnAjouter, btnModifier, btnAnnuler, btnValider, btnSupprimer, btnModifierVehicule, btnGetTrajet;
	public	JTextField	cumulKms, monnaie, taux, labelKms, saisieKms, depart, arrivee, indemn1, indemn2, indemn3, libelleMotif;
	public	JTextField	puissance, marque, immatriculation, segmentKm1, segmentKm2, segmentKm3, montantKms, montantFrais, datesIndemnite;
	
	public JComboBox classeSncf;
	
	private PopupListener listenerSncf  = new PopupListener();
	
	private	EOSegmentTarif 	currentSegment;
	private	EOTransports 	currentTransport;
	private EOVehicule		currentVehicule;
	
	private boolean modeModification;
	
	/** 
	 * Constructeur 
	 */
	public GestionTransports(EOEditingContext editingContext)	{
		super();
		EOArchive.loadArchiveNamed("GestionTransports", this,"kiwi.client", this.disposableRegistry());
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;
		initObject();
	}	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static GestionTransports sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new GestionTransports(editingContext);
		return sharedInstance;
	}
	
	public void initObject()	{
		
		initGUI();
		initView();
	}
	
	/**
	 * 
	 *
	 */
	private void initView()	{
		
		view.setBorder(BorderFactory.createEmptyBorder());
		
		viewClasseSncf.setBorder(BorderFactory.createEmptyBorder());
		viewIndemnites.setBorder(BorderFactory.createEmptyBorder());
		
		vueKms.setBorder(BorderFactory.createEmptyBorder());

		viewDetail.setBorder(BorderFactory.createBevelBorder(1));

		CocktailUtilities.initTextField(monnaie, false, false);
		CocktailUtilities.initTextField(taux, false, false);

		classeSncf.addActionListener(listenerSncf);

		CocktailUtilities.initTextField(libelleMotif,false,false);
		
		CocktailUtilities.initTextField(indemn1,false,false);
		CocktailUtilities.initTextField(indemn2,false,false);
		CocktailUtilities.initTextField(indemn3,false,false);
		
		CocktailUtilities.initTextField(montantKms,false,false);
		CocktailUtilities.initTextField(cumulKms,false,false);
		
		datesIndemnite.setEditable(false);
		datesIndemnite.setForeground(new Color(160,160,160));
		indemn1.setForeground(new Color(160,160,160));
		indemn2.setForeground(new Color(160,160,160));
		indemn3.setForeground(new Color(160,160,160));
		
		CocktailUtilities.initTextField(segmentKm1,false,false);
		CocktailUtilities.initTextField(segmentKm2,false,false);
		CocktailUtilities.initTextField(segmentKm3,false,false);
		
		CocktailUtilities.initTextField(marque,false,false);
		CocktailUtilities.initTextField(immatriculation,false,false);
		CocktailUtilities.initTextField(puissance,false,false);
		
		saisieKms.setText("0");		
		saisieKms.addFocusListener(new ListenerTextFieldKms());
		saisieKms.addActionListener(new ActionListenerKms());
		
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_ADD,null,btnAjouter,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_UPDATE,null,btnModifier,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_DELETE,null,btnSupprimer,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_VALID,null,btnValider,"");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL,null,btnAnnuler,"");
		
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16,null,btnGetTrajet,"Sélection d'un trajet prédéfini");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_UPDATE,null,btnModifierVehicule,"");

	}
	
	/**
	 * 
	 *
	 */
	public void updateData()	{
		
		saisieKms.setText(currentTransport.traKmSaisie().toString());

		if (currentTransport.typeTransport() != null && currentTransport.typeTransport().isVehiculeSncf())	{
			
			classeSncf.removeActionListener(listenerSncf);
			
			majPopupSncf();
						
			classeSncf.setSelectedItem(currentTransport.tarifSncf());

			classeSncf.addActionListener(listenerSncf);

		}
		
		// S'il s'agit d'un vehicule, on affiche les infos de kms, depart et arrivee.
		if (currentTransport.typeTransport() != null && currentTransport.typeTransport().isVehicule() )	{
			
			if (currentTransport.vehicule() != null)	{

				marque.setText(currentTransport.vehicule().vehMarque());
				immatriculation.setText(currentTransport.vehicule().vehImmatriculation());
				puissance.setText(currentTransport.vehicule().vehPuissance());

				if (currentTransport.typeTransport().isVehiculePersonnel())
					cumulKms.setText(String.valueOf(getCumulKms()));// - currentTransport.traKmSaisie().floatValue()));
				else
					cumulKms.setText("0");
			}			
			
			if (!currentTransport.typeTransport().isVehiculeSncf() && currentTransport.indemniteKm() != null)	{
				
				EOIndemniteKm localIndemnite = currentTransport.indemniteKm();
				
				indemn1.setText(localIndemnite.ikmTarif1().toString());
				indemn2.setText(localIndemnite.ikmTarif2().toString());
				indemn3.setText(localIndemnite.ikmTarif3().toString());
				
				datesIndemnite.setText("Du " + DateCtrl.dateToString(localIndemnite.ikmDate())+" au "+DateCtrl.dateToString(localIndemnite.ikmDateFin()));
			}
		}
			
		monnaie.setText(currentTransport.webtaux().webmon().devises().llDevise());
		taux.setText(currentTransport.webtaux().wtaTaux().toString());
		
		if (currentTransport.traDepart() != null)
			depart.setText(currentTransport.traDepart());
		
		if (currentTransport.traArrivee() != null)
			arrivee.setText(currentTransport.traArrivee());

		if (currentTransport.traLibFrais() != null)	{
			libelleMotif.setText(currentTransport.traLibFrais());
		}
		
		segmentKm1.setText(currentTransport.segKm1().toString());
		segmentKm2.setText(currentTransport.segKm2().toString());
		segmentKm3.setText(currentTransport.segKm3().toString());
		
		montantFrais.setText(currentTransport.traMontant().toString());
		montantKms.setText(currentTransport.traMontantKm().toString());
	}
	
	public boolean testSaisieValide()	{
		
		if (currentTransport.typeTransport().isVehicule())	{
			
			if (StringCtrl.chaineVide(depart.getText()) || StringCtrl.chaineVide(arrivee.getText()))	{
				MsgPanel.sharedInstance().runInformationDialog("ATTENTION", "Vous devez saisir un lieu de départ ET un lieu d'arrivée !");
				return false;				
			}
			
			if (StringCtrl.chaineVide(saisieKms.getText()))	{
				MsgPanel.sharedInstance().runInformationDialog("ATTENTION", "Vous devez saisir un nombre de kms !");
				return false;				
			}			
			
//			if (currentTransport.typeTransport().isVehiculeSncf())	{				
//				if (!classeSncf.getSelectedItem().getClass().getName().c("EOTarifSncf"))	{
//					MsgPanel.sharedInstance().runInformationDialog("ATTENTION", "Vous devez choisir une classe pour le tarif SNCF !");
//					return false;				
//				}			
//			}
		}
		else	{		// Montant de frais obligatoire et saisi avec des chiffres !!!
			
			BigDecimal localMontant = new BigDecimal(0);
			montantFrais.setText(NSArray.componentsSeparatedByString(montantFrais.getText(), ",").componentsJoinedByString("."));

			try {
				localMontant = new BigDecimal(montantFrais.getText());
			}
			catch (Exception e)	{
				localMontant = new BigDecimal(0);
			}

//			// Le montant des frais doit obligatoirement etre > 0
//			if (localMontant.floatValue() == 0)	{
//				MsgPanel.sharedInstance().runInformationDialog("ATTENTION", "Le montant des frais doit être supérieur à 0 !");
//				return false;								
//			}
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void valider(Object sender)	{
		
		if (!testSaisieValide())	
			return;
		
		try {
			
			if (currentTransport.typeTransport() != null &&  currentTransport.typeTransport().isVehicule()) {
				currentTransport.calculerkm(getCumulKms());
				calculerMontantKm(currentTransport);
			}
			
			calculerMontantPaiement(currentTransport);
			
			if (!StringCtrl.chaineVide(depart.getText()))	
				currentTransport.setTraDepart(depart.getText());
			else
				currentTransport.setTraDepart(null);
			
			if (!StringCtrl.chaineVide(arrivee.getText()))	
				currentTransport.setTraArrivee(arrivee.getText());
			else
				currentTransport.setTraArrivee(null);
			
			if (!StringCtrl.chaineVide(libelleMotif.getText()))	
				currentTransport.setTraLibFrais(libelleMotif.getText());
			else
				currentTransport.setTraLibFrais(null);
			
			currentTransport.setTraUtilisateur("N");
			
			montantFrais.setText(NSArray.componentsSeparatedByString(montantFrais.getText(), ",").componentsJoinedByString("."));
			
			currentTransport.setTraMontant(new BigDecimal(montantFrais.getText()).setScale(2));
			currentTransport.setTraMontantKm(new BigDecimal(montantKms.getText()).setScale(2));			
			currentTransport.setTraKmSaisie(new Double(saisieKms.getText()));
			
			if (currentTransport.typeTransport() != null &&  currentTransport.typeTransport().isVehicule())
				currentTransport.calculerkm(getCumulKms());
			
			currentTransport.setTraEtat("VALIDE");
			
			currentSegment.mission().setUtilisateurModificationRelationship(NSApp.getUtilisateur());		

			ec.saveChanges();
			myEOTable.updateUI();
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors de la validation du transport !");
			ec.revert();
			e.printStackTrace();
		}		
		modeModification = false;
		updateUI();
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void annuler(Object sender)	{
		
		ec.revert();
		
		actualiser();
		modeModification = false;
		
		updateUI();
	}
	
	
	/**
	 * 
	 * @param sender
	 */
	public void getTrajet(Object sender) {
		
		EODistancesKm myDistance = TrajetsSelectCtrl.sharedInstance(ec).getTrajet();
	
		if (myDistance != null) {
			
			depart.setText(myDistance.lieuDepart());
			arrivee.setText(myDistance.lieuArrivee());
			
			saisieKms.setText(myDistance.distance().toString());
			
			calculerMontantKm(currentTransport);

			if (currentTransport.typeTransport().isVehiculeSncf()) 
				majPopupSncf();

		}

	}
	
	
	/**
	 * 
	 * @param sender
	 */
	public void supprimer(Object sender)	{
		
		if (currentTransport == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Vous devez spécifier un transport à supprimer !");
			return;
		}
		
		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Supprimer la ligne sélectionnée ?",
				"OUI", "NON")) 
			return;
		
		try {
			
			currentTransport.removeObjectFromBothSidesOfRelationshipWithKey(currentSegment, "segmentTarif");
			boolean vehiculePersonnel = currentTransport.typeTransport().isVehiculePersonnel();
			
			ec.deleteObject(currentTransport);
			currentSegment.mission().setUtilisateurModificationRelationship(NSApp.getUtilisateur());		
			ec.saveChanges();
		
			// S'il s'agissait d'un vehicule personnel, on verifie les missions qui avaient des VP			
			if (vehiculePersonnel)	{
				EOFournis fournis = currentSegment.mission().fournis();
				NSArray missionsVp = EOMission.findMissionsValidesForFournisAvecVP(ec, fournis);
			
				if (missionsVp.count() > 0)	{
					
					String messageAlerte = "Veuillez vérifier et recalculer les cumuls kilométriques de " + fournis.prenom()+" " + fournis.nom()+" pour les missions suivantes :\n";

					for (int i=0;i<missionsVp.count();i++)	{
						
						EOMission mission = (EOMission)missionsVp.objectAtIndex(i);
						messageAlerte = messageAlerte.concat("\t- Mission No "+  mission.misNumero()+"\n");
						
					}
					
					MsgPanel.sharedInstance().runInformationDialog("ATTENTION",messageAlerte);					
					
				}
			}
		
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors de la suppression du transport sélectionnée !");
			ec.revert();
			e.printStackTrace();
		}

		
		actualiser();
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void calculerMontantPaiement(EOTransports transport)	{
		
		try {
			
			BigDecimal localMontantFrais = new BigDecimal(montantFrais.getText()).setScale(2);
			BigDecimal taux = transport.webtaux().wtaTaux();
			
			localMontantFrais = localMontantFrais.multiply(taux);

			localMontantFrais = localMontantFrais.setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);

			BigDecimal localMontantKms = new BigDecimal(montantKms.getText()).setScale(2);

			if (!currentTransport.typeTransport().isVehiculeService()) {
			
				BigDecimal localMontantPaiement = localMontantFrais.add(localMontantKms).setScale(2);
	
				transport.setTraMontantPaiement(localMontantPaiement);			
			}
			else {
				transport.setTraMontantPaiement(new BigDecimal(0));			
				transport.setTraMontantKm(new BigDecimal(0));			
		}
		}		
		catch (Exception e)	{
		}
		
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void calculerMontantKm(EOTransports transport)	{
			
		if (transport == null)
			return;

		boolean baseSncf = transport.typeTransport().isVehiculeSncf();
		BigDecimal nbKilometres = new BigDecimal(0);
		
		try {
			
			if (baseSncf)	{
				
				String saisieFormattee = (NSArray.componentsSeparatedByString(saisieKms.getText(),",")).componentsJoinedByString(".");
				nbKilometres = new BigDecimal(saisieFormattee);
								
				EOTarifSncf tarifSncf = (EOTarifSncf) classeSncf.getSelectedItem();
								
				transport.setTraKmSaisie(new Double(nbKilometres.doubleValue()));
				
				if (tarifSncf != null)	{
					transport.setTarifSncfRelationship(tarifSncf);
					transport.calculerMontantBaseSncf();
				}	
				else
					transport.setTraMontantKm(new BigDecimal(0));

			}
			else	{
				
				String saisieFormattee = (NSArray.componentsSeparatedByString(saisieKms.getText(),",")).componentsJoinedByString(".");
				nbKilometres = new BigDecimal(saisieFormattee);
				transport.setTraKmSaisie(new Double(nbKilometres.doubleValue()));
				
				segmentKm1.setText(transport.segKm1().toString());
				segmentKm2.setText(transport.segKm2().toString());
				segmentKm3.setText(transport.segKm3().toString());
				
				if (transport.indemniteKm() != null && !transport.typeTransport().isVehiculeService()) {
					
					BigDecimal montantSeg1 = transport.segKm1().multiply(transport.indemniteKm().ikmTarif1());
					BigDecimal montantSeg2 = transport.segKm2().multiply(transport.indemniteKm().ikmTarif2());
					BigDecimal montantSeg3 = transport.segKm3().multiply(transport.indemniteKm().ikmTarif3());
					
					BigDecimal montantKm = (montantSeg1.add(montantSeg2).add(montantSeg3)).setScale(2, BigDecimal.ROUND_HALF_UP);
					
					transport.setTraMontantKm(montantKm);
				}
				else
					transport.setTraMontantKm(new BigDecimal(0));

			}
			
			montantKms.setText(transport.traMontantKm().toString());
			calculerMontantPaiement(transport);
		}
		catch (Exception e)	{
			e.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Vérifiez le format du nombre de kms saisi ou la validité du montant des indemnités !!");
			return;
		}						
	}
	
	/**
	 * 
	 *
	 */
	public void clearTextFields()	{
		
		depart.setText("");
		arrivee.setText("");
		
		indemn1.setText("");
		indemn2.setText("");
		indemn3.setText("");
		
		marque.setText("");
		immatriculation.setText("");
		puissance.setText("");
		
		segmentKm1.setText("");
		segmentKm2.setText("");
		segmentKm3.setText("");
		
		monnaie.setText("");
		taux.setText("");
		
		montantFrais.setText("");
		montantKms.setText("");
		
		datesIndemnite.setText("");
		
		libelleMotif.setText("");
		saisieKms.setText("");
		
	}
	
	/**
	 * 
	 * @return
	 */
	public EOView view()	{
		return view;
	}
	
	public void setCurrentSegment(EOSegmentTarif segment)	{
		currentSegment = segment;
	}
	
	/**
	 * 
	 *
	 */
	public void actualiser()	{
		
		if (currentSegment != null)	{
			clearTextFields();
			
			eodTransports.setObjectArray(EOTransports.findForSegment(ec, currentSegment));
			myEOTable.updateData();			
		}
		
		updateUI();

	}
	
	/**
	 * 
	 *
	 */
	public void updateUI()	{
						
		viewDetail.setVisible(currentTransport != null && currentSegment != null);

		btnAjouter.setVisible(!modeModification && currentSegment != null && currentSegment.mission().isValide());
		
		btnSupprimer.setVisible(!modeModification && currentTransport != null);
		btnModifier.setVisible(!modeModification && currentTransport != null);
		btnValider.setVisible(modeModification && currentTransport != null);
		btnAnnuler.setVisible(modeModification && currentTransport != null);
		btnGetTrajet.setVisible(modeModification && currentTransport != null && currentTransport.typeTransport().isVehicule());
				
		if (currentTransport != null && currentSegment != null)	{
			
			// Pas de saisies de transports
			if ((currentSegment.mission().isPayee())
					|| currentSegment.mission().isAnnulee()  // Mission ANNULEE ==> Pas de modifs
			)	{				
				btnAjouter.setVisible(false);
				btnModifier.setVisible(false);
				btnModifierVehicule.setVisible(false);
				btnValider.setVisible(false);
				btnAnnuler.setVisible(false);
				btnGetTrajet.setVisible(false);
				btnSupprimer.setVisible(false);
				CocktailUtilities.initTextField(montantFrais, false, false);
				CocktailUtilities.initTextField(libelleMotif, false, false);
				CocktailUtilities.initTextField(depart, false, false);
				CocktailUtilities.initTextField(arrivee, false, false);
				CocktailUtilities.initTextField(saisieKms, false, false);				
				
				vueKms.setVisible(currentTransport.typeTransport().isVehicule());
				viewIndemnites.setVisible(currentTransport.typeTransport().isVehiculePersonnel() || (currentTransport.typeTransport().isVelomoteur()  || currentTransport.typeTransport().isMotocyclette()) );
				viewClasseSncf.setVisible(currentTransport.typeTransport().isVehiculeSncf());
				if (currentTransport.typeTransport().isVehiculeSncf())
					labelKms.setText("Kms SNCF :");
				else
					labelKms.setText("Nb Kms :");
			}
			else	{
				
				if (currentTransport.typeTransport() != null && currentTransport.typeTransport().isVehiculeSncf())
					labelKms.setText("Kms SNCF :");
				else
					labelKms.setText("Nb Kms :");
				
				btnValider.setVisible(modeModification);
				btnAnnuler.setVisible(modeModification);
				btnSupprimer.setVisible(!modeModification);
				btnModifier.setVisible(!modeModification);
				btnModifierVehicule.setVisible(false);
												
				vueKms.setVisible(currentTransport.typeTransport() != null && currentTransport.typeTransport().isVehicule());				
				viewClasseSncf.setVisible(currentTransport.typeTransport() != null && currentTransport.typeTransport().isVehiculeSncf());
				viewIndemnites.setVisible(currentTransport.typeTransport() != null && ( currentTransport.typeTransport().isVehiculePersonnel()  || currentTransport.typeTransport().isVelomoteur()  || currentTransport.typeTransport().isMotocyclette()));

				classeSncf.setEnabled(modeModification);
				
				String paramSncf = FinderMissionParametres.getValue(ec, currentSegment.mission().toExercice(), "CLASSE_SNCF");				
				if (paramSncf != null) {classeSncf.setEnabled(false);}

				CocktailUtilities.initTextField(libelleMotif, false, modeModification);
				CocktailUtilities.initTextField(montantFrais, false, modeModification && currentTransport.typeTransport() != null && (!currentTransport.typeTransport().isVehicule() || currentTransport.typeTransport().isVehiculeLocation()));											
				CocktailUtilities.initTextField(depart, false, modeModification);
				CocktailUtilities.initTextField(arrivee, false, modeModification);
				CocktailUtilities.initTextField(saisieKms, false, modeModification  && currentTransport.typeTransport() != null &&  currentTransport.typeTransport().isVehicule());
			}
		}
	}

	/**
	 * 
	 *
	 */
	public void setDisabled()	{
		
		btnAjouter.setVisible(false);
		btnValider.setVisible(false);
		btnAnnuler.setVisible(false);
		btnSupprimer.setVisible(false);
		CocktailUtilities.initTextField(saisieKms, false, false);
		CocktailUtilities.initTextField(depart, false, false);
		CocktailUtilities.initTextField(arrivee, false, false);
		CocktailUtilities.initTextField(montantFrais, false, false);
		CocktailUtilities.initTextField(libelleMotif, false, false);
		CocktailUtilities.initTextField(saisieKms, false, false);
		classeSncf.setEnabled(false);
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void ajouter(Object sender)	{
		
		if (currentSegment.webtaux() == null || 
				currentSegment.webtaux().webmon() == null ||
				currentSegment.webtaux().webmon().devises() == null)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Pas de taux associé au trajet !");
			return;
		}
		
		clearTextFields();
		
		NSDictionary transports = TransportSelectCtrl.sharedInstance(ec).getTypeTransport(currentSegment);
		EOTransports newTransport = null;
		
		if (transports != null)	{
			
			try {
				EOTypeTransport typeTransport = (EOTypeTransport)transports.objectForKey("typeTransport");
				EOVehicule vehicule = (EOVehicule)transports.objectForKey("vehicule");
				EOIndemniteKm indemniteKm = (EOIndemniteKm)transports.objectForKey("indemniteKm");

				BigDecimal frais = new BigDecimal(0.0);
				BigDecimal nbKms = new BigDecimal(0.0);
				
				if (transports.objectForKey("montantFrais") != null)	{	
					frais = (BigDecimal)transports.objectForKey("montantFrais");
					montantFrais.setText(frais.toString());
				}

				if (transports.objectForKey("nbKms") != null)	{
					nbKms = (BigDecimal)transports.objectForKey("nbKms");
					saisieKms.setText(nbKms.toString());
				}
				else	{
					saisieKms.setText(nbKms.toString());
					montantKms.setText("0.00");					
				}
				
				// Enregistrement du transport			
				newTransport  = (EOTransports)Factory.instanceForEntity(ec, EOTransports.ENTITY_NAME);
				
				newTransport.setSegmentTarifRelationship(currentSegment);
				newTransport.setTypeTransportRelationship(typeTransport);
				newTransport.setVehiculeRelationship(vehicule);

				newTransport.setWebtauxRelationship(currentSegment.webtaux());
				newTransport.setIndemniteKmRelationship(indemniteKm);
				
				newTransport.setTraLibFrais(null);
				newTransport.setTraUtilisateur("N");
				
				newTransport.setTraMontant(new BigDecimal(0.0));
				newTransport.setTraMontantKm(new BigDecimal(0.0));
				newTransport.setTraMontantPaiement(new BigDecimal(0.0));
				newTransport.setTraKmSaisie(new Double(0.0));

				newTransport.setSegKm1(new BigDecimal(0.0));
				newTransport.setSegKm2(new BigDecimal(0.0));
				newTransport.setSegKm3(new BigDecimal(0.0));

				newTransport.setTraLibFrais((String)transports.objectForKey("remarques"));

				newTransport.setTraMontant(frais);

				if (!typeTransport.isVehicule() && !typeTransport.isVelomoteur() && !typeTransport.isMotocyclette())	{
					
					BigDecimal taux = newTransport.webtaux().wtaTaux();
					
					BigDecimal localMontantFrais = frais.multiply(taux);
					localMontantFrais = localMontantFrais.setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);

					newTransport.setTraMontantPaiement(localMontantFrais);	
					
				}
				else	{

					newTransport.setTraDepart((String)transports.objectForKey("depart"));
					newTransport.setTraArrivee((String)transports.objectForKey("arrivee"));

					if (typeTransport.isVehiculeSncf())	{
						majPopupSncf();
						newTransport.setTarifSncfRelationship((EOTarifSncf)transports.objectForKey("tarifSncf"));
						classeSncf.setSelectedItem((EOTarifSncf)transports.objectForKey("tarifSncf"));
					}

					newTransport.setTraKmSaisie(new Double(nbKms.floatValue()));
					newTransport.calculerkm(getCumulKms() + nbKms.floatValue());
					calculerMontantKm(newTransport);
					newTransport.setTraMontantPaiement(newTransport.traMontantKm());			
				}				
				
				calculerMontantPaiement(newTransport);
				
				newTransport.setTraEtat("VALIDE");
				
				currentSegment.mission().setUtilisateurModificationRelationship(NSApp.getUtilisateur());		

				ec.insertObject(newTransport);
				ec.saveChanges();
			}
			catch (Exception e)	{
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors de l'enregistrement du transport !!!");
				e.printStackTrace();
				ec.revert();
			}
			
			actualiser();
			
			eodTransports.setSelectedObject(newTransport);
			myEOTable.forceNewSelection(eodTransports.selectionIndexes());
			myEOTable.scrollToSelection();
			myEOTable.updateData();
			myEOTable.updateUI();
		}
	}
	
	/**
	 * 
	 * @param sender
	 */
	public void modifier(Object sender)	{
		
		// Si vehicule personnel, on reverifie les taux
		
		if (currentTransport.typeTransport().isVehiculePersonnel()) {
			
			NSArray indemnites = FinderIndemniteKm.findIndemnitesForZoneAndVehicule(ec, currentSegment.segDebut(), currentSegment.rembZone(), currentTransport.typeTransport(), currentVehicule);
			
			if (indemnites.count() > 0) {
				
				EOIndemniteKm localIndemnite = (EOIndemniteKm)indemnites.objectAtIndex(0);
				
				indemn1.setText(localIndemnite.ikmTarif1().toString());
				indemn2.setText(localIndemnite.ikmTarif2().toString());
				indemn3.setText(localIndemnite.ikmTarif3().toString());

				currentTransport.setIndemniteKmRelationship(localIndemnite);

			}			
		}
		
		modeModification = true;
		updateUI();
		//majPopupSncf();
		
	}
	
	
	/**
	 * 
	 * @param sender
	 */
	public void modifierVehicule(Object sender)	{
		
			if (SaisieVehiculeCtrl.sharedInstance(ec).modifierVehicule(currentVehicule))
				updateData();
		
	}
	
	
	
	/** */
	public float getCumulKms() {
		
		NSArray transports = FinderTransports.findTransportsForSegmentAndFournisAndExercice(ec, currentSegment, currentSegment.mission().fournis());

		float cumulKm = 0;
		for (int i = 0; i < transports.count(); i++)	 {
					
			EOTransports transport = (EOTransports)transports.objectAtIndex(i);
		
			cumulKm = cumulKm + (transport.traKmSaisie().floatValue());
			
		}

		return cumulKm;
	}
	
	/**
	 * 
	 *
	 */
	public void majPopupSncf()	{
		
		BigDecimal kilometres =  null;

		if (StringCtrl.chaineVide(saisieKms.getText()))	{
			saisieKms.setText("0");
		}
		else	{
			try {
				String saisieFormattee = (NSArray.componentsSeparatedByString(saisieKms.getText(),",")).componentsJoinedByString(".");
				kilometres = new BigDecimal(saisieFormattee);
			}
			catch (Exception ex)	{
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Vérifier le format de saisie des Kms SNCF !");
				return;
			}
		}

		try {

			NSArray tarifs = FinderTarifSncf.findTarifsForKilometres(ec, kilometres);
			classeSncf.setEnabled(true);	

			classeSncf.removeAllItems();
			for (int i=0;i<tarifs.count();i++) {
				classeSncf.addItem((EOTarifSncf)tarifs.objectAtIndex(i));
			}

			String paramSncf = FinderMissionParametres.getValue(ec, currentSegment.mission().toExercice(), "CLASSE_SNCF");
			
			if (paramSncf != null && "2".equals(paramSncf)) {
				classeSncf.setSelectedIndex(0);
				classeSncf.setEnabled(false);
			}

			if (paramSncf != null && "1".equals(paramSncf)) {
				classeSncf.setSelectedIndex(1);
				classeSncf.setEnabled(false);
			}


		}
		catch (Exception ex) {
			
		}
		
	}
	
	/** 
	 * Classe d'ecoute sur la date de fin de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ActionListenerKms implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{		
			
			if (e.getSource() == saisieKms) {
				
				if (currentTransport.typeTransport().isVehiculeSncf())
					majPopupSncf();
				
				calculerMontantKm(currentTransport);
			}
		}
	}
	
	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ListenerTextFieldKms implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		
		public void focusLost(FocusEvent e)	{
			
			if (e.getSource() == saisieKms) {
				
				if (currentTransport.typeTransport().isVehiculeSncf())
					majPopupSncf();

				calculerMontantKm(currentTransport);
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eodTransports = new EODisplayGroup();
		
		initTableModel();
		initTable();
		
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);

		viewTable.setBorder(BorderFactory.createEmptyBorder());
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		
	}
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
				
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eodTransports, "typeTransport.ttrLibelle", "Transport", 180);
		col1.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(eodTransports, "traLibFrais", "Remarques", 150);
		col2.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(eodTransports, "traMontant", "Montant Frais", 90);
		col3.setAlignment(SwingConstants.RIGHT);
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(eodTransports, "traMontantKm", "Montant Kms", 90);
		col4.setAlignment(SwingConstants.RIGHT);
		ZEOTableModelColumn col5 = new ZEOTableModelColumn(eodTransports, "traMontantPaiement", "Montant Paiement", 100);
		col5.setAlignment(SwingConstants.RIGHT);
		
		myCols = new Vector();
		myCols.add(col1);
		myCols.add(col2);
		myCols.add(col3);
		myCols.add(col4);
		myCols.add(col5);
		
		myTableModel = new ZEOTableModel(eodTransports, myCols);
		myTableSorter = new TableSorter(myTableModel);
	}
	
	/* (non-Javadoc)
	 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 */
	public void onSelectionChanged() {
		
		clearTextFields();
		
		currentTransport = (EOTransports)eodTransports.selectedObject();
		
		if (currentTransport != null)	{		
			currentVehicule = currentTransport.vehicule();
			updateData();
		}

		updateUI();
		
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub
		
	}

	/** 
	 * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
	 */
	private class PopupListener implements ActionListener	{
		public PopupListener() {super();}
		public void actionPerformed(ActionEvent anAction) {calculerMontantKm(currentTransport);}
	}
	
}
