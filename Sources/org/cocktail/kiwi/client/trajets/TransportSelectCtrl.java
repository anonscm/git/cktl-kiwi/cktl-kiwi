package org.cocktail.kiwi.client.trajets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.admin.SaisieVehiculeCtrl;
import org.cocktail.kiwi.client.finders.FinderIndemniteKm;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderTarifSncf;
import org.cocktail.kiwi.client.finders.FinderTypeTransport;
import org.cocktail.kiwi.client.metier.EODistancesKm;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOIndemniteKm;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTarifSncf;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.metier.EOVehicule;
import org.cocktail.kiwi.client.nibctrl.TransportSelectView;
import org.cocktail.kiwi.client.select.TrajetsSelectCtrl;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class TransportSelectCtrl 
{
	private static TransportSelectCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext	ec;

	private TransportSelectView myView;

	public 	EODisplayGroup		eodTransports, eodIndemnites, eodVehicules;

	private ListenerTransports listenerTypeTransport = new ListenerTransports();

	private	EOSegmentTarif 		currentSegment;
	private EOTypeTransport 	currentTypeTransport;
	private EOVehicule 			currentVehicule;
	private	EOIndemniteKm 		currentIndemnite;

	/** 
	 *
	 */
	public TransportSelectCtrl (EOEditingContext globalEc) {

		super();

		eodTransports = new EODisplayGroup();
		eodVehicules = new EODisplayGroup();
		eodIndemnites = new EODisplayGroup();

		myView = new TransportSelectView(new JFrame(), true, eodTransports, eodVehicules, eodIndemnites);

		ec = globalEc;

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnGetTrajet().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getTrajet();
			}
		});

		myView.getBtnAddVehicule().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterVehicule();
			}
		});

		myView.getBtnUpdateVehicule().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierVehicule();
			}
		});

		myView.getBtnDelVehicule().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				invaliderVehicule();
			}
		});

		myView.getMyEOTableTransports().addListener(new ListenerTransports());
		myView.getMyEOTableVehicules().addListener(new ListenerVehicules());
		myView.getMyEOTableIndemnites().addListener(new ListenerIndemnites());

		NSArray mySort = new NSArray(new EOSortOrdering(EOTypeTransport.TTR_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		eodTransports.setSortOrderings(mySort);

		NSMutableArray mySortVehicules = new NSMutableArray(new EOSortOrdering(EOVehicule.VEH_ETAT_KEY, EOSortOrdering.CompareAscending));
		eodVehicules.setSortOrderings(mySortVehicules);

		myView.getClassesSncf().removeAllItems();
		myView.getClassesSncf().setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myView.getTfKilometres().addFocusListener(new ListenerTextFieldSncf());
		myView.getTfKilometres().addActionListener(new ActionListenerSncf());

	}

	public EOSegmentTarif getCurrentSegment() {
		return currentSegment;
	}

	public void setCurrentSegment(EOSegmentTarif currentSegment) {
		this.currentSegment = currentSegment;
	}

	public EOTypeTransport getCurrentTypeTransport() {
		return currentTypeTransport;
	}

	public void setCurrentTypeTransport(EOTypeTransport currentTypeTransport) {
		this.currentTypeTransport = currentTypeTransport;
	}

	public EOVehicule getCurrentVehicule() {
		return currentVehicule;
	}

	public void setCurrentVehicule(EOVehicule currentVehicule) {
		this.currentVehicule = currentVehicule;
	}

	public EOIndemniteKm getCurrentIndemnite() {
		return currentIndemnite;
	}

	public void setCurrentIndemnite(EOIndemniteKm currentIndemnite) {
		this.currentIndemnite = currentIndemnite;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TransportSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TransportSelectCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 *
	 */
	private void updateUI()	{		

		System.err.println("IS VEHICULE PERSONNEL : " + currentTypeTransport.isVehiculePersonnel());
		myView.getBtnAddVehicule().setVisible(currentTypeTransport != null
				&& !NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION)
				&& ( currentTypeTransport.isVehiculePersonnel() || currentTypeTransport.isVehiculeSncf()) );
		myView.getBtnUpdateVehicule().setVisible(currentTypeTransport != null 
				&& !NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION)
				&& ( currentTypeTransport.isVehiculePersonnel() || currentTypeTransport.isVehiculeSncf()) );
		
		myView.getBtnDelVehicule().setVisible(currentTypeTransport != null 
				&& !NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION)
				&& ( currentTypeTransport.isVehiculePersonnel() || currentTypeTransport.isVehiculeSncf()) );

		myView.getViewTableIndemnites().setVisible(currentTypeTransport != null && (currentTypeTransport.isVehiculePersonnel() || currentTypeTransport.isVelomoteur()  || currentTypeTransport.isMotocyclette() ));
		myView.getClassesSncf().setVisible(currentTypeTransport != null && currentTypeTransport.isVehiculeSncf());

		CocktailUtilities.initTextField(myView.getTfDepart(), false, currentTypeTransport != null && currentTypeTransport.isVehicule());
		CocktailUtilities.initTextField(myView.getTfArrivee(), false, currentTypeTransport != null && currentTypeTransport.isVehicule());
		CocktailUtilities.initTextField(myView.getTfKilometres(), false, currentTypeTransport != null && currentTypeTransport.isVehicule());

		myView.getBtnGetTrajet().setEnabled(currentTypeTransport != null && currentTypeTransport.isVehicule());

		CocktailUtilities.initTextField(myView.getTfMontant(), false, currentTypeTransport != null && (!currentTypeTransport.isVehicule() || currentTypeTransport.isVehiculeLocation()));
	}


	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public NSDictionary getTypeTransport(EOSegmentTarif segment)	{

		NSMutableDictionary retour = new NSMutableDictionary();

		currentSegment = segment;
		myView.getTfDepart().setText("");
		myView.getTfArrivee().setText("");
		myView.getTfMontant().setText("0.00");
		myView.getTfKilometres().setText("0");
		myView.getTfRemarques().setText("");

		if (eodTransports.displayedObjects().count() == 0)	{
			eodTransports.setObjectArray(FinderTypeTransport.findTypesTransportValides(ec));
		}

		myView.getMyEOTableTransports().updateData();			

		myView.setVisible(true);

		if (currentTypeTransport != null)	{

			retour.setObjectForKey(currentTypeTransport, "typeTransport");

			if (currentVehicule != null)
				retour.setObjectForKey((EOVehicule)eodVehicules.selectedObject(), "vehicule");

			if (currentIndemnite != null)
				retour.setObjectForKey(currentIndemnite, "indemniteKm");

			if (!StringCtrl.chaineVide(myView.getTfMontant().getText()))	{
				String saisieFormattee = StringCtrl.replace(myView.getTfMontant().getText(), ",", ".");
				retour.setObjectForKey(new BigDecimal(saisieFormattee).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP), "montantFrais");
			}

			if (!StringCtrl.chaineVide(myView.getTfKilometres().getText()))	{
				String saisieFormattee = StringCtrl.replace(myView.getTfKilometres().getText(), ",", ".");
				retour.setObjectForKey(new BigDecimal(saisieFormattee).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP), "nbKms");
			}

			if (!StringCtrl.chaineVide(myView.getTfDepart().getText()))
				retour.setObjectForKey(myView.getTfDepart().getText(), "depart");

			if (!StringCtrl.chaineVide(myView.getTfArrivee().getText()))
				retour.setObjectForKey(myView.getTfArrivee().getText(), "arrivee");

			if (!StringCtrl.chaineVide(myView.getTfRemarques().getText()))
				retour.setObjectForKey(myView.getTfRemarques().getText(), "remarques");

			if (currentTypeTransport.isVehiculeSncf())	{
				retour.setObjectForKey((EOTarifSncf)myView.getClassesSncf().getSelectedItem(), "tarifSncf");				
			}

			return retour;
		}

		return null;
	}


	/**
	 * 
	 * @param sender
	 */
	private void getTrajet() {

		EODistancesKm myDistance = TrajetsSelectCtrl.sharedInstance(ec).getTrajet();

		if (myDistance != null) {

			myView.getTfDepart().setText(myDistance.lieuDepart());
			myView.getTfArrivee().setText(myDistance.lieuArrivee());

			myView.getTfKilometres().setText(myDistance.distance().toString());

			if (currentTypeTransport.isVehiculeSncf()) 
				majPopupSncf();

		}

	}	

	/**
	 *
	 */
	private void valider()	{

		if (currentTypeTransport == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez choisir un type de transport !");
			return;
		}

		if (currentTypeTransport.isVehiculePersonnel() && currentVehicule == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Pour ce type de transport vous devez sélectionner un véhicule !");
			return;
		}

		if ( (currentTypeTransport.isVehiculePersonnel() || currentTypeTransport.isVelomoteur() || currentTypeTransport.isMotocyclette())
				&& eodIndemnites.displayedObjects().size() == 0)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Aucune indemnité kilométrique n'a pu être trouvée !");
			return;
		}

		if ( (currentTypeTransport.isVehiculePersonnel() || currentTypeTransport.isVelomoteur() || currentTypeTransport.isMotocyclette())
				&& currentIndemnite == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez sélectionner une indemnité kilométrique pour ce type de transport !");
			return;
		}

		currentIndemnite = (EOIndemniteKm)eodIndemnites.selectedObject();

		if (currentTypeTransport.isVehicule() || currentTypeTransport.isMotocyclette() || currentTypeTransport.isVelomoteur())	{
			if (StringCtrl.chaineVide(myView.getTfDepart().getText()) || StringCtrl.chaineVide(myView.getTfArrivee().getText()))	{
				MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez saisir un lieu de départ ET un lieu d'arrivée !");
				return;
			}
		}
		else	{		// Le montant des frais doit etre saisi et > 0
			BigDecimal localMontant = new BigDecimal(0);
			myView.getTfMontant().setText(NSArray.componentsSeparatedByString(myView.getTfMontant().getText(), ",").componentsJoinedByString("."));

			try {
				localMontant = new BigDecimal(myView.getTfMontant().getText());
			}
			catch (Exception e)	{
				localMontant = new BigDecimal(0);
			}

//			// Le montant des frais doit obligatoirement etre > 0
//			if (localMontant.floatValue() == 0)	{
//				MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez saisir un montant de frais > 0 !");
//				return;								
//			}
		}

		if (currentTypeTransport.isVehiculeSncf())	{
			if (StringCtrl.chaineVide(myView.getTfKilometres().getText()) || new BigDecimal(myView.getTfKilometres().getText()).intValue() == 0 )	{
				MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez saisir un nombre de Kms !");
				return;
			}

			BigDecimal localMontant = new BigDecimal(0);
			myView.getTfKilometres().setText(NSArray.componentsSeparatedByString(myView.getTfKilometres().getText(), ",").componentsJoinedByString("."));

			try {
				localMontant = new BigDecimal(myView.getTfKilometres().getText());
			}
			catch (Exception e)	{
				localMontant = new BigDecimal(0);
			}

			// Le montant des frais doit obligatoirement etre > 0
			if (localMontant.floatValue() == 0)	{
				MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez saisir un nombre de kms > 0 !");
				return;								
			}

			if (myView.getClassesSncf().getItemCount () == 0)	{
				MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez spécifier une classe pour les véhicules SNCF !");
				return;			
			}
		}

		myView.dispose();
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerTransports implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (!currentTypeTransport.isVehicule())
				valider();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentTypeTransport = (EOTypeTransport)eodTransports.selectedObject();

			myView.getTfMontant().setText("0.00");

			eodVehicules.setObjectArray(new NSArray());
			myView.getMyEOTableVehicules().updateData();

			if (currentTypeTransport != null)	{

				if (currentTypeTransport.isVehiculePersonnel())
					eodVehicules.setObjectArray(EOVehicule.findForTransportAndFournis(ec, currentTypeTransport, currentSegment.mission().fournis()));	
				else {
					if (currentTypeTransport.isVehiculeSncf())
						eodVehicules.setObjectArray(EOVehicule.findForTransportAndFournis(ec, null, currentSegment.mission().fournis()));			
					else
						if (currentTypeTransport.isVehiculeService())
							eodVehicules.setObjectArray(EOVehicule.findForTransportAndFournis(ec, currentTypeTransport, null));			
				}

				myView.getMyEOTableVehicules().updateData();
			}

			updateUI();
		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerVehicules implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
			if (!NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION))
				modifierVehicule();
		}

		public void onSelectionChanged() {

			setCurrentVehicule((EOVehicule) eodVehicules.selectedObject());

			if (currentTypeTransport != null && !currentTypeTransport.isVehiculeService()) {

				eodIndemnites.setObjectArray(new NSArray());

				if (getCurrentVehicule() != null)	{
					eodIndemnites.setObjectArray(FinderIndemniteKm.findIndemnitesForZoneAndVehicule(ec, currentSegment.mission().misDebut(), currentSegment.rembZone(), currentTypeTransport, currentVehicule));
				}

				myView.getMyEOTableIndemnites().updateData();
			}
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerIndemnites implements ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentIndemnite((EOIndemniteKm)eodIndemnites.selectedObject());
		}
	}

	/**
	 *
	 */
	public void annuler()	{

		//		ec.revert();
		currentTypeTransport = null;
		myView.dispose();
	}

	/**
	 * 
	 * @param sender
	 */
	public void ajouterVehicule()	{


		EOVehicule newVehicule = null;

		if (currentTypeTransport.isVehiculePersonnel() || currentTypeTransport.isVehiculeSncf())
			newVehicule = SaisieVehiculeCtrl.sharedInstance(ec).ajouterVehicule(currentSegment.mission().fournis(), currentTypeTransport);
		else
			newVehicule = SaisieVehiculeCtrl.sharedInstance(ec).ajouterVehicule(null, currentTypeTransport);

		if (newVehicule != null)	{

			NSMutableArray vehicules = new NSMutableArray(eodVehicules.displayedObjects());
			vehicules.addObject(newVehicule);

			eodVehicules.setObjectArray(vehicules);

			myView.getMyEOTableVehicules().updateData();

			currentVehicule = newVehicule;

			eodVehicules.setSelectedObject(newVehicule);

			myView.getMyEOTableVehicules().forceNewSelection(eodVehicules.selectionIndexes());
			myView.getMyEOTableVehicules().scrollToSelection();
		}


	}



	/**
	 * 
	 * @param sender
	 */
	public void modifierVehicule()	{

		if (currentVehicule != null)	{

			if (SaisieVehiculeCtrl.sharedInstance(ec).modifierVehicule(currentVehicule))
				myView.getMyEOTableVehicules().updateData();

		}

	}


	/**
	 * 
	 * @param sender
	 */
	private void invaliderVehicule()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Ce véhicule va être annulé et ne sera plus proposé dans les prochaines missions.\nConfirmez vous cette suppression ?",
				"OUI", "NON"))
			return;


		try {

			currentVehicule.setVehEtat("ANNULE");
			ec.saveChanges();

			listenerTypeTransport.onSelectionChanged();

		}
		catch (Exception ex) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));		
		}

	}

	/**
	 * 
	 *
	 */
	private void majPopupSncf()	{
		BigDecimal kilometres =  null;

		if (StringCtrl.chaineVide(myView.getTfKilometres().getText()))	{
			myView.getTfKilometres().setText("0");
		}
		else	{
			try {
				String saisieFormattee = StringCtrl.replace(myView.getTfKilometres().getText(),",",".");
				kilometres = new BigDecimal(saisieFormattee);
			}
			catch (Exception ex)	{
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Verifier le format de saisie des Kms SNCF !");
				return;
			}
		}

		String paramSncf = FinderMissionParametres.getValue(ec, "CLASSE_SNCF");
		//classeSncf.setVisible(kilometres.floatValue() > 0);

		try {
			NSArray tarifs = FinderTarifSncf.findTarifsForKilometres(ec, kilometres);

			myView.getClassesSncf().removeAllItems();
			for (int i=0;i<tarifs.count();i++)
				myView.getClassesSncf().addItem((EOTarifSncf)tarifs.objectAtIndex(i));

			if (paramSncf != null && "2".equals(paramSncf)) {
				myView.getClassesSncf().setSelectedIndex(0);
				myView.getClassesSncf().setEnabled(false);
			}

			if (paramSncf != null && "1".equals(paramSncf)) {
				myView.getClassesSncf().setSelectedIndex(1);
				myView.getClassesSncf().setEnabled(false);
			}

		}
		catch (Exception ex) {

		}


	}


	/** 
	 * Classe d'ecoute sur la date de fin de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ActionListenerSncf implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{		

			if (e.getSource() == myView.getTfKilometres() && currentTypeTransport.isVehiculeSncf()) {
				majPopupSncf();
			}
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ListenerTextFieldSncf implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{

			if (e.getSource() == myView.getTfKilometres() && currentTypeTransport.isVehiculeSncf()) {
				majPopupSncf();
			}
		}
	}
	/**
	 * 
	 */
	public void windowClosing(WindowEvent e)	{	   
	}

}