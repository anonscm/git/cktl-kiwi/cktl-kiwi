/*
 * Created on 23 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.trajets;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ApplicationIcones;
import org.cocktail.kiwi.client.factory.FactoryIndemnite;
import org.cocktail.kiwi.client.factory.FactoryNuits;
import org.cocktail.kiwi.client.factory.FactoryRepas;
import org.cocktail.kiwi.client.factory.FactorySegmentTarif;
import org.cocktail.kiwi.client.factory.FactoryTransports;
import org.cocktail.kiwi.client.metier.EOIndemnite;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EONuits;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EORepas;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTransports;
import org.cocktail.kiwi.client.metier.EOWebpays;
import org.cocktail.kiwi.client.metier.EOWebtaux;
import org.cocktail.kiwi.client.nibctrl.TrajetsView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TrajetsCtrl   {

	private static TrajetsCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext edc;

	private TrajetsView myView;

	public EODisplayGroup eodTrajet;

	private	EOMission currentMission;
	private EOSegmentTarif currentTrajet;

	/** 
	 * Constructeur 
	 */
	public TrajetsCtrl(EOEditingContext edc)	{

		setEdc(edc);

		eodTrajet = new EODisplayGroup();
		myView = new TrajetsView(eodTrajet);


		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {ajouter();}
		});

		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {modifier();}
		});

		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {supprimer();}
		});

		myView.getMyEOTable().addListener(new ListenerSegment());

		myView.getOnglets().addTab("NUITS / REPAS", null, NuitsRepasCtrl.sharedInstance(edc).view());
		myView.getOnglets().addTab(" TRANSPORTS ", ApplicationIcones.ICON_TRANSPORT, GestionTransports.sharedInstance(edc).view());
		myView.getOnglets().addTab(" INDEMNITES ", ApplicationIcones.ICON_INDEMNITE, IndemnitesCtrl.sharedInstance(edc).panel());
		myView.getOnglets().setBackgroundAt(0,new Color(166,173,201));
		myView.getOnglets().addChangeListener(new OngletChangeListener());

		CocktailUtilities.initTextField(myView.getTfDevise(), false, false);
		CocktailUtilities.initTextField(myView.getTfTaux(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateTaux(), false, false);

	}	

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TrajetsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TrajetsCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel view()	{
		return myView;
	}

	public EOSegmentTarif getCurrentTrajet() {
		return currentTrajet;
	}

	public void setCurrentTrajet(EOSegmentTarif currentTrajet) {
		this.currentTrajet = currentTrajet;
	}

	public EOMission getCurrentMission() {
		return currentMission;
	}

	public int getNbSegments()	{
		return eodTrajet.displayedObjects().count();
	}

	public void setCurrentMission(EOMission mission)	{
		currentMission = mission;
	}

	/**
	 *
	 */
	public void clearDatas()	{

		GestionTransports.sharedInstance(edc).setCurrentSegment(getCurrentTrajet());

		CocktailUtilities.viderTextField(myView.getTfDevise());
		CocktailUtilities.viderTextField(myView.getTfTaux());
		CocktailUtilities.viderTextField(myView.getTfDateTaux());

		NuitsRepasCtrl.sharedInstance(edc).clean();
	}

	/**
	 * 
	 * Actualisation des trajets en fonction de la mission selectionnee
	 *
	 */
	public void actualiser()	{

		eodTrajet.setObjectArray(EOSegmentTarif.findSegmentsTarifsForMission(edc, getCurrentMission()));
		myView.getMyEOTable().updateData();

		updateTrajetsModifies();
		updateInterface();
		
	}


	/**
	 * 
	 *
	 */
	public void updateTrajetsModifies()	{

		boolean modif = false;

		try {

			for (int i=0;i<eodTrajet.displayedObjects().count();i++)	{

				EOSegmentTarif segment = (EOSegmentTarif)eodTrajet.displayedObjects().objectAtIndex(i);

				if (!currentMission.isPayee() && "CALCUL".equals(segment.segEtat()))	{

					modif = true;

					NSArray repass = segment.repas();
					for (int j=0;j<repass.count();j++){
						EORepas repas = (EORepas)repass.objectAtIndex(j);
						if ("CALCUL".equals(repas.repEtat()))	{
							NuitsRepasCtrl.sharedInstance(edc).getDefaultRepas();
							break;
						}
					}

					NSArray nuits = segment.nuits();
					for (int j=0;j<nuits.count();j++){
						EONuits nuit = (EONuits)nuits.objectAtIndex(j);
						if ("CALCUL".equals(nuit.nuiEtat()))	{
							NuitsRepasCtrl.sharedInstance(edc).getDefaultNuits();
							break;
						}
					}

					NSArray transports = segment.transports();
					for (int j=0;j<transports.count();j++){
						EOTransports transport = (EOTransports)transports.objectAtIndex(j);
						if ("CALCUL".equals(transport.traEtat()))	{
							transport.calculerMontantPaiement();
							transport.setTraEtat("VALIDE");
						}
					}

					NSArray indemnites = segment.indemnites();
					for (int j=0;j<indemnites.count();j++)	{
						EOIndemnite indemnite = (EOIndemnite)indemnites.objectAtIndex(j);
						if ("CALCUL".equals(indemnite.indEtat()))	{
							indemnite.calculerMontant();
							indemnite.setIndEtat("VALIDE");
						}
					}

					segment.setSegEtat("VALIDE");
				}

			}			

			if (modif)	{
				edc.saveChanges();
				GestionTransports.sharedInstance(edc).updateUI();
				IndemnitesCtrl.sharedInstance(edc).updateUI();
				MsgPanel.sharedInstance().runInformationDialog("INFO","CALCUL AUTOMATIQUE DES NUITS, REPAS, TRANSPORTS ET INDEMNITES !\nLe taux de la devise ou le montant des remboursements de perdiem a été modifié.\nLes calculs ont donc été refaits.\nMerci de vérifier ces données !");
			}
		}		
		catch (Exception e)	{
			e.printStackTrace();
			edc.revert();
		}
	}

	/**
	 * Mise a jour de l'interface principale de gestion des trajets.
	 * Affichage des boutons, des onglets ...
	 *
	 */
	public void updateInterface()	{

		myView.getOnglets().setVisible(currentTrajet != null);
		myView.getMyEOTable().setEnabled(true);

		// Si on a aucun segment tarif, on desactive les onglets et l'ecran de saisie des nuits, repas ...
		myView.getOnglets().setEnabledAt(0, currentTrajet != null && currentTrajet.zoneParisProvince());
		myView.getOnglets().setEnabledAt(1, currentTrajet != null);
		myView.getOnglets().setEnabledAt(2, currentTrajet != null && !currentTrajet.zoneParisProvince());

		// La mission est nulle ou annulee ==> On ne peut rien saisir.
		if ( currentMission == null || currentMission.misEtat() == null || currentMission.fournis() == null // Mission NULLE
				|| currentMission.isAnnulee()	// Mission ANNULEE ==> Pas de modifs
				|| (currentMission.isPayee())

				)	{

			myView.getBtnAjouter().setVisible(false);
			myView.getBtnModifier().setVisible(false);
			myView.getBtnSupprimer().setVisible(false);

		}
		else	{

			// Validation et suppression seulement si un segment est selectionne
			myView.getBtnAjouter().setVisible(true);
			myView.getBtnModifier().setVisible(currentTrajet != null);
			myView.getBtnSupprimer().setVisible(currentTrajet != null);				

			if (currentTrajet != null && 
					(currentTrajet.zoneEtranger() || currentTrajet.zoneDomTom()))
				myView.getOnglets().setSelectedIndex(2);
			else	{
				if (myView.getOnglets().getSelectedIndex() == 2)
					myView.getOnglets().setSelectedIndex(0);					
			}

			NuitsRepasCtrl.sharedInstance(edc).updateUI();
			GestionTransports.sharedInstance(edc).updateUI();			
			IndemnitesCtrl.sharedInstance(edc).updateUI();			
		}
	}

	/**
	 * 
	 *
	 */
	public void setDisabled()	{

		myView.getOnglets().setEnabledAt(0, false);
		myView.getOnglets().setEnabledAt(1, false);
		myView.getOnglets().setEnabledAt(2, false);

		myView.getBtnAjouter().setVisible(false);
		myView.getBtnModifier().setVisible(false);
		myView.getBtnSupprimer().setVisible(false);

		myView.getMyEOTable().setEnabled(false);

		NuitsRepasCtrl.sharedInstance(edc).setDisabled();
		GestionTransports.sharedInstance(edc).setDisabled();			
		IndemnitesCtrl.sharedInstance(edc).setDisabled();			
	}

	/**
	 * Mise a jour des donnees concernant les tarifs
	 *
	 */
	public void updateDatas()	{

		clearDatas();

		if (getCurrentMission() != null || getCurrentTrajet() == null) {

			NuitsRepasCtrl.sharedInstance(edc).setCurrentSegment(getCurrentTrajet());
			GestionTransports.sharedInstance(edc).setCurrentSegment(getCurrentTrajet());
			IndemnitesCtrl.sharedInstance(edc).setCurrentSegment(getCurrentTrajet());

			// Mise a jour des donnees concernant les nuits,transports, repas ...			
			NuitsRepasCtrl.sharedInstance(edc).actualiser();
			GestionTransports.sharedInstance(edc).actualiser();
			IndemnitesCtrl.sharedInstance(edc).actualiser();	

			try {
				myView.getTfDevise().setText(currentTrajet.webpays().toMonnaie().devises().llDevise());
			}
			catch (Exception e){myView.getTfDevise().setText("");}

			try {
				myView.getTfTaux().setForeground(Color.BLACK);
				myView.getTfTaux().setText(currentTrajet.webtaux().wtaTaux().toString());
				myView.getTfDateTaux().setText(DateCtrl.dateToString(currentTrajet.webtaux().wtaDate()));			
			}
			catch (Exception e){
				myView.getTfTaux().setForeground(Color.RED);
				myView.getTfTaux().setText("PAS DE TAUX !");
				myView.getTfDateTaux().setText("");
			}
		}
		
		updateInterface();

	}

	/**
	 * 
	 * @param sender
	 */
	public void ajouter()	{

		NSApp.setWaitCursor();
		NSDictionary newTrajet = TrajetSaisieCtrl.sharedInstance(edc).ajouter(getCurrentMission());
		NSApp.setDefaultCursor();

		if (newTrajet != null)	{

			try {

				setCurrentTrajet(FactorySegmentTarif.sharedInstance().creerSegmentTarif(getEdc(), getCurrentMission()));	

				EOWebtaux taux = (EOWebtaux)newTrajet.objectForKey(EOSegmentTarif.WEBTAUX_KEY);
				EORembZone zone = (EORembZone)newTrajet.objectForKey(EOSegmentTarif.REMB_ZONE_KEY);
				EOWebpays pays = (EOWebpays)newTrajet.objectForKey(EOSegmentTarif.WEBPAYS_KEY);

				getCurrentTrajet().setRembZoneRelationship(zone);
				getCurrentTrajet().setWebtauxRelationship(taux);
				getCurrentTrajet().setWebpaysRelationship(pays);

				getCurrentTrajet().setWpaCode(pays.wpaCode());
				getCurrentTrajet().setRemTaux(zone.remTaux());

				getCurrentTrajet().setSegLibelle((String)newTrajet.objectForKey("motif"));

				if (!StringCtrl.chaineVide((String)newTrajet.objectForKey("lieuDestination")))	
					getCurrentTrajet().setSegLieuDestination((String)newTrajet.objectForKey("lieuDestination"));
				else
					getCurrentTrajet().setSegLieuDestination(null);

				if (!StringCtrl.chaineVide((String)newTrajet.objectForKey("lieuDepart")))	
					getCurrentTrajet().setSegLieuDepart((String)newTrajet.objectForKey("lieuDepart"));
				else
					getCurrentTrajet().setSegLieuDepart(null);

				currentTrajet.setSegDebut((NSTimestamp)newTrajet.objectForKey("segDebut"));
				currentTrajet.setSegFin((NSTimestamp)newTrajet.objectForKey("segFin"));

				edc.saveChanges();
				actualiser();
				NuitsRepasCtrl.sharedInstance(edc).setCurrentSegment(getCurrentTrajet());
				GestionTransports.sharedInstance(edc).setCurrentSegment(getCurrentTrajet());
				IndemnitesCtrl.sharedInstance(edc).setCurrentSegment(getCurrentTrajet());

				if (!EOMission.controleDatesTrajets(getCurrentMission()))
					EODialogs.runInformationDialog("ATTENTION","Certaines dates de trajets ne correspondent pas aux dates de la mission. Merci de les modifier !");

			}
			catch (Exception e)	{
				edc.revert();
				e.printStackTrace();
			}
			myView.getOnglets().setVisible(getCurrentTrajet() != null);
		}

	}

	/**
	 * 
	 * @param sender
	 */
	public void modifier()	{

		try {

			setCurrentTrajet(TrajetSaisieCtrl.sharedInstance(edc).modifierTrajet(getCurrentTrajet()));

			// Si le trajet est a l'etranger ou en DOM TOM, on supprime les repas et les nuits
			if (!getCurrentTrajet().zoneParisProvince())	{
				FactoryNuits.supprimerNuits(edc, getCurrentTrajet());
				FactoryRepas.supprimerRepas(edc, getCurrentTrajet());
			}
			else	{
				FactoryNuits.associerNuitsSegment(edc, getCurrentTrajet());
				FactoryRepas.associerRepasSegment(edc, getCurrentTrajet());
				NuitsRepasCtrl.sharedInstance(edc).updateMontants();
				FactoryIndemnite.sharedInstance().supprimerIndemnites(edc, getCurrentTrajet());

				NSArray<EONuits> nuits = EONuits.findForSegment(edc, getCurrentTrajet());
				NSArray<EORepas> repas = EORepas.findForSegment(edc, getCurrentTrajet());

				if (nuits.size() > 0)
					NuitsRepasCtrl.sharedInstance(edc).getDefaultNuits();

				if (repas.size() > 0)	
					NuitsRepasCtrl.sharedInstance(edc).getDefaultRepas();
			}

			// Si le trajet est a l'etranger on supprime les transports
			if (getCurrentTrajet().zoneEtranger() || getCurrentTrajet().zoneDomTom())	{
				FactoryTransports.associerIndemnitesSegment(edc, getCurrentTrajet());
				FactoryIndemnite.associerIndemnitesSegment(edc, getCurrentTrajet());
				IndemnitesCtrl.sharedInstance(edc).getDefaultIndemnites();
			}

			edc.saveChanges();

			updateDatas();

			NuitsRepasCtrl.sharedInstance(edc).actualiser();
			GestionTransports.sharedInstance(edc).actualiser();
			IndemnitesCtrl.sharedInstance(edc).actualiser();
		}
		catch (Exception e)	{
			edc.revert();
			e.printStackTrace();			
		}

		myView.getMyEOTable().updateData();
		updateInterface();
		NuitsRepasCtrl.sharedInstance(edc).setCurrentSegment(currentTrajet);
		GestionTransports.sharedInstance(edc).setCurrentSegment(currentTrajet);
		IndemnitesCtrl.sharedInstance(edc).setCurrentSegment(currentTrajet);

		GestionTransports.sharedInstance(edc).updateUI();

		if (!EOMission.controleDatesTrajets(currentMission))
			EODialogs.runInformationDialog("ATTENTION","Certaines dates de trajets ne correspondent pas aux dates de la mission. Merci de les modifier !");

	}

	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	
			ongletsHasChanged();
		}
	}

	/**
	 * 
	 * @param numero
	 */
	public void setSelectedOnglet(int numero)	{
		myView.getOnglets().setSelectedIndex(numero);
	}

	/**
	 *
	 */
	public void ongletsHasChanged()	{

		for (int i=0; i<myView.getOnglets().getTabCount(); i++)
			myView.getOnglets().setBackgroundAt(i, Color.lightGray);

		myView.getOnglets().setBackgroundAt(myView.getOnglets().getSelectedIndex(),new Color(166,173,201));

	}

	/**
	 * 
	 * @param sender
	 */
	public void supprimer()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Ce trajet sera définitivement ANNULE !\nVous ne pourrez plus revenir dessus.\nConfirmez vous cette suppression ?",
				"OUI", "NON")) 
			return;

		try {

			NSArray nuits = getCurrentTrajet().nuits();
			for (int i=0;i<nuits.count();i++)	{
				EONuits nuit =(EONuits)nuits.objectAtIndex(i);
				nuit.setSegmentTarifRelationship(null);
				edc.deleteObject(nuit);
			}

			NSArray transports = getCurrentTrajet().transports();
			for (int i=0;i<transports.count();i++)	{
				EOTransports transport =(EOTransports)transports.objectAtIndex(i);
				transport.setSegmentTarifRelationship(null);
				edc.deleteObject(transport);
			}

			NSArray repass = getCurrentTrajet().repas();
			for (int i=0;i<repass.count();i++)	{
				EORepas repas =(EORepas)repass.objectAtIndex(i);
				repas.setSegmentTarifRelationship(null);
				edc.deleteObject(repas);
			}

			NSArray indemnites = getCurrentTrajet().indemnites();
			for (int i=0;i<indemnites.count();i++)	{
				EOIndemnite indemnite =(EOIndemnite)indemnites.objectAtIndex(i);
				indemnite.setSegmentTarifRelationship(null);
				edc.deleteObject(indemnite);
			}

			currentTrajet.setMissionRelationship(null);
			edc.deleteObject(getCurrentTrajet());
			edc.saveChanges();
		}
		catch (Exception e)	{
			edc.revert();
			e.printStackTrace();
		}

		actualiser();   	
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerSegment implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// La mission est nulle ou annulee ==> On ne peut rien saisir.
			if ( !getCurrentMission().isAnnulee()	// Mission ANNULEE ==> Pas de modifs
					&& !getCurrentMission().isPayee()		// Mission PAYEE ==> Pas de modifs
					)
				modifier();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			setCurrentTrajet((EOSegmentTarif)eodTrajet.selectedObject());
			updateDatas();
		}
	}
}