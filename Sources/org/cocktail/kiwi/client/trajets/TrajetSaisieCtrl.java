package org.cocktail.kiwi.client.trajets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.admin.GestionPreferencesPerso;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderRembZone;
import org.cocktail.kiwi.client.finders.FinderWebPays;
import org.cocktail.kiwi.client.finders.FinderWebTaux;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPreferencesPerso;
import org.cocktail.kiwi.client.metier.EOMotifs;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOWebpays;
import org.cocktail.kiwi.client.metier.EOWebtaux;
import org.cocktail.kiwi.client.nibctrl.TrajetSaisieView;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;
import org.cocktail.kiwi.common.utilities.TimeCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class TrajetSaisieCtrl  {
	private static TrajetSaisieCtrl sharedInstance;

	private 	EOEditingContext		edc;

	public 	EODisplayGroup		eodWebTaux, eodRembZone, eodWebPays;

	private TrajetSaisieView myView;

	private EORembZone currentRembZone;
	private	EOWebpays currentWebPays;
	private	EOWebtaux currentWebTaux;
	private	EOMotifs currentMotif;
	private	EOMission currentMission;
	private	EOSegmentTarif currentSegment;
	private	EOMissionPreferencesPerso currentPreference;

	private ActionListenerHeureDebut listenerHeureDebut = new ActionListenerHeureDebut();
	private ActionListenerHeureFin listenerHeureFin = new ActionListenerHeureFin();

	/** 
	 *
	 */
	public TrajetSaisieCtrl (EOEditingContext edc) {

		setEdc(edc);

		eodRembZone = new EODisplayGroup();
		eodWebPays = new EODisplayGroup();
		eodWebTaux = new EODisplayGroup();
		eodRembZone = new EODisplayGroup();
		myView = new TrajetSaisieView(new JFrame(), true, eodRembZone, eodWebPays, eodWebTaux);

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {valider();}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}
		});

		myView.getBtnGetMotif().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectMotif();}});

		myView.getBtnGetDebut().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getDateDebut();}});

		myView.getBtnGetFin().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDateFin();
			}
		});

		myView.getMyEOTableRembZone().addListener(new ListenerZone());
		myView.getMyEOTableWebPays().addListener(new ListenerPays());
		myView.getMyEOTableWebTaux().addListener(new ListenerTaux());

		NSArray mySortZone = new NSArray(new EOSortOrdering(EORembZone.REM_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		eodRembZone.setSortOrderings(mySortZone);

		NSArray mySort = new NSArray(new EOSortOrdering(EOWebtaux.WTA_DATE_KEY, EOSortOrdering.CompareDescending));
		eodWebTaux.setSortOrderings(mySort);		

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, false);

		// Suivant le parametrage on autorise ou non la saisie des motifs
		String paramMotifs = FinderMissionParametres.getValue(edc, "AJOUT_MOTIFS");
		if (paramMotifs != null && paramMotifs.equals("N"))
			CocktailUtilities.initTextField(myView.getTfMotif(), false, false);

		myView.getTfFiltrePays().getDocument().addDocumentListener(new FiltrePaysListener());
		myView.getTfFiltrePays().setBackground(CocktailConstantes.COLOR_FILTRES_NOMENCLATURES);

		myView.getTfHeureDebut().addFocusListener(new ListenerTextFieldHeureDebut());
		myView.getTfHeureDebut().addActionListener(listenerHeureDebut);

		myView.getTfHeureFin().addFocusListener(new ListenerTextFieldHeureFin());
		myView.getTfHeureFin().addActionListener(listenerHeureFin);

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TrajetSaisieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TrajetSaisieCtrl(editingContext);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EORembZone getCurrentRembZone() {
		return currentRembZone;
	}

	public void setCurrentRembZone(EORembZone currentRembZone) {
		this.currentRembZone = currentRembZone;
	}

	public EOWebpays getCurrentWebPays() {
		return currentWebPays;
	}

	public void setCurrentWebPays(EOWebpays currentWebPays) {
		this.currentWebPays = currentWebPays;
	}

	public EOWebtaux getCurrentWebTaux() {
		return currentWebTaux;
	}

	public void setCurrentWebTaux(EOWebtaux currentWebTaux) {
		this.currentWebTaux = currentWebTaux;
	}

	public EOMotifs getCurrentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOMotifs currentMotif) {
		this.currentMotif = currentMotif;
		CocktailUtilities.viderTextField(myView.getTfMotif());
		if (currentMotif != null)
			CocktailUtilities.setTextToField(myView.getTfMotif(), currentMotif.motLibelle());

	}

	public EOMission getCurrentMission() {
		return currentMission;
	}

	public void setCurrentMission(EOMission currentMission) {
		this.currentMission = currentMission;
	}

	public EOSegmentTarif getCurrentSegment() {
		return currentSegment;
	}

	public void setCurrentSegment(EOSegmentTarif currentSegment) {
		this.currentSegment = currentSegment;
	}

	public EOMissionPreferencesPerso getCurrentPreference() {
		return currentPreference;
	}

	public void setCurrentPreference(EOMissionPreferencesPerso currentPreference) {
		this.currentPreference = currentPreference;
	}

	/**
	 * 
	 * @param segment
	 */
	public EOSegmentTarif modifierTrajet(EOSegmentTarif segment)	{

		setCurrentMission(segment.mission());
		setCurrentSegment(segment);

		CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentSegment().segDebut(), "%d/%m/%Y");
		CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentSegment().segFin(), "%d/%m/%Y");

		CocktailUtilities.setDateToField(myView.getTfHeureDebut(), getCurrentSegment().segDebut(), "%H:%M");
		CocktailUtilities.setDateToField(myView.getTfHeureFin(), getCurrentSegment().segFin(), "%H:%M");

		CocktailUtilities.setTextToField(myView.getTfArrivee(), getCurrentSegment().segLieuDestination());
		CocktailUtilities.setTextToField(myView.getTfDepart(), getCurrentSegment().segLieuDepart());
		CocktailUtilities.setTextToField(myView.getTfMotif(), getCurrentSegment().segLibelle());
		CocktailUtilities.setTextToField(myView.getTfArrivee(), getCurrentSegment().segLieuDestination());

		// Motif
		if (currentSegment.segLibelle() != null)
			CocktailUtilities.setTextToField(myView.getTfMotif(), getCurrentSegment().segLibelle());
		else {
			setCurrentMotif(null);
		}

		if (eodRembZone.displayedObjects().count() == 0)	{
			eodRembZone.setObjectArray(FinderRembZone.findZones(edc));
			myView.getMyEOTableRembZone().updateData();
		}

		setCurrentRembZone(getCurrentSegment().rembZone());
		setCurrentWebPays(getCurrentSegment().webpays());
		setCurrentWebTaux(getCurrentSegment().webtaux());

		eodRembZone.setSelectedObject(currentRembZone);
		myView.getMyEOTableRembZone().forceNewSelection(eodRembZone.selectionIndexes());
		myView.getMyEOTableRembZone().scrollToSelection();

		eodWebPays.setSelectedObject(currentSegment.webpays());
		myView.getMyEOTableWebPays().forceNewSelection(eodWebPays.selectionIndexes());
		myView.getMyEOTableWebPays().scrollToSelection();

		if (getCurrentWebPays() != null && getCurrentWebPays().toMonnaie() != null)
			eodWebTaux.setObjectArray(FinderWebTaux.findTauxForWebmonAndDate(edc, getCurrentWebPays().toMonnaie(), null));
		else
			eodWebTaux.setObjectArray(new NSArray());

		myView.getMyEOTableWebTaux().updateData();

		selectCurrentTaux(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));

		// Validation ou annulation de la requete
		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(true);
		myView.setVisible(true);
		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(false);

		// Validation de l'écran
		if (currentRembZone != null)	{

			segment.setSegDebut(DateCtrl.stringToDate(myView.getTfDateDebut().getText()+" "+myView.getTfHeureDebut().getText(), "%d/%m/%Y %H:%M"));
			segment.setSegFin(DateCtrl.stringToDate(myView.getTfDateFin().getText()+" "+myView.getTfHeureFin().getText(), "%d/%m/%Y %H:%M"));

			segment.setRembZoneRelationship(currentRembZone);
			segment.setWebpaysRelationship((EOWebpays)eodWebPays.selectedObject());
			segment.setWebtauxRelationship((EOWebtaux)eodWebTaux.selectedObject());

			segment.setSegDebut(DateCtrl.stringToDate(myView.getTfDateDebut().getText()+" "+myView.getTfHeureDebut().getText(), "%d/%m/%Y %H:%M"));
			segment.setSegFin(DateCtrl.stringToDate(myView.getTfDateFin().getText()+" "+myView.getTfHeureFin().getText(), "%d/%m/%Y %H:%M"));

			segment.setSegEtat("VALIDE");

			segment.setSegLibelle(myView.getTfMotif().getText());

			if (!StringCtrl.chaineVide(myView.getTfDepart().getText()))
				segment.setSegLieuDepart(myView.getTfDepart().getText());
			else
				segment.setSegLieuDepart(null);

			if (!StringCtrl.chaineVide(myView.getTfArrivee().getText()))
				segment.setSegLieuDestination(myView.getTfArrivee().getText());
			else
				segment.setSegLieuDestination(null);
		}

		return segment;
	}

	/**
	 * 
	 *
	 */
	public void selectMotif()	{

		EOMotifs motif = MotifTrajetSelectCtrl.sharedInstance(edc).getMotif();		
		if (motif != null)	{
			setCurrentMotif(motif);
		}

	}

	/**
	 * 
	 * @param mission
	 * @return
	 */
	public NSDictionary ajouter(EOMission mission)	{

		setCurrentMission(mission);

		currentPreference = GestionPreferencesPerso.sharedInstance(edc).getPreferences();

		NSArray<EOSegmentTarif> segments = EOSegmentTarif.findSegmentsTarifsForMission(edc, mission);
		if (segments.count() == 0)	{

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), mission.misDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), mission.misFin());

			CocktailUtilities.setDateToField(myView.getTfHeureDebut(), mission.misDebut(), "%H:%M");
			CocktailUtilities.setDateToField(myView.getTfHeureFin(), mission.misFin(), "%H:%M");

		}
		else	{
			EOSegmentTarif dernierSegment = EOSegmentTarif.findLastSegmentForMission(edc ,mission);

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), dernierSegment.segFin());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), mission.misFin());

			CocktailUtilities.setDateToField(myView.getTfHeureDebut(), dernierSegment.segFin(), "%H:%M");
			CocktailUtilities.setDateToField(myView.getTfHeureFin(), mission.misFin(), "%H:%M");

			String hDebut = DateCtrl.dateToString(dernierSegment.segFin(), "%H");
			String minuteDebut = DateCtrl.dateToString(dernierSegment.segFin(), "%M");

			if (new Integer(minuteDebut).intValue() == 59)	{
				hDebut = String.valueOf((new Integer(hDebut)).intValue() + 1);
				minuteDebut = "0";
			}
			else
				minuteDebut = String.valueOf((new Integer(minuteDebut)).intValue() + 1);

			minuteDebut = StringCtrl.stringCompletion(minuteDebut, 2, "0","LEFT");

			myView.getTfHeureDebut().setText(DateCtrl.dateToString(dernierSegment.segFin(), hDebut+":"+minuteDebut));
			myView.getTfHeureFin().setText(DateCtrl.dateToString(mission.misFin(), "%H:%M"));			

			listenerHeureDebut.actionPerformed(null);
			listenerHeureFin.actionPerformed(null);
		}

		myView.getTfArrivee().setText("");
		myView.getTfDepart().setText("");

		currentMotif = null;
		myView.getTfMotif().setText("");

		if (eodRembZone.displayedObjects().count() == 0)	{
			eodRembZone.setObjectArray(FinderRembZone.findZones(edc));
			myView.getMyEOTableRembZone().updateData();

			if (currentPreference != null && currentPreference.rembZone() != null)	{
				eodRembZone.setSelectedObject(currentPreference.rembZone());				
				myView.getMyEOTableRembZone().forceNewSelection(eodRembZone.selectionIndexes());
				currentRembZone = currentPreference.rembZone();
			}
		}

		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(true);
		myView.setVisible(true);
		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(false);

		if (getCurrentRembZone() != null)	{

			NSMutableDictionary retour = new NSMutableDictionary();

			retour.setObjectForKey(currentRembZone, EOSegmentTarif.REMB_ZONE_KEY);
			retour.setObjectForKey((EOWebpays)eodWebPays.selectedObject(), EOSegmentTarif.WEBPAYS_KEY);
			retour.setObjectForKey((EOWebtaux)eodWebTaux.selectedObject(), EOSegmentTarif.WEBTAUX_KEY);

			if (currentMotif != null)
				retour.setObjectForKey(currentMotif.motLibelle(), "motif");
			else
				retour.setObjectForKey(myView.getTfMotif().getText(), "motif");

			retour.setObjectForKey(myView.getTfDepart().getText(), "lieuDepart");
			retour.setObjectForKey(myView.getTfArrivee().getText(), "lieuDestination");

			retour.setObjectForKey(DateCtrl.stringToDate(myView.getTfDateDebut().getText()+" "+myView.getTfHeureDebut().getText(), "%d/%m/%Y %H:%M"), "segDebut");
			retour.setObjectForKey(DateCtrl.stringToDate(myView.getTfDateFin().getText()+" "+myView.getTfHeureFin().getText(), "%d/%m/%Y %H:%M"), "segFin");

			return retour;
		}

		return null;
	}

	/**
	 *
	 */
	public void valider()	{

		if (StringCtrl.chaineVide(myView.getTfMotif().getText()))	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez choisir un motif !");
			return;
		}

		if (currentRembZone == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez choisir une zone !");
			return;
		}

		if (currentWebPays == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez choisir un pays !");
			return;
		}

		if (currentWebTaux == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez choisir un taux !");
			return;
		}

		// Controle des dates par rapport aux dates de la mission
		NSTimestamp debutMission = currentMission.misDebut();
		NSTimestamp finMission = currentMission.misFin();

		NSTimestamp debutSegment = DateCtrl.stringToDate(myView.getTfDateDebut().getText()+" "+myView.getTfHeureDebut().getText(), "%d/%m/%Y %H:%M");
		NSTimestamp finSegment = DateCtrl.stringToDate(myView.getTfDateFin().getText()+" "+myView.getTfHeureFin().getText(), "%d/%m/%Y %H:%M");

		if (DateCtrl.isBeforeEq(finSegment, debutSegment))	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","La date de fin du trajet doit être supérieure à la date de début !");
			return;
		}

		if (DateCtrl.isBefore(finMission, finSegment))	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","La date de fin du trajet ne peut être supérieure à la date de fin de la mission  !");
			return;
		}

		if (DateCtrl.isBefore(debutSegment, debutMission))	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","La date de début du trajet ne peut être antérieure à la date de début de la mission !");
			return;
		}

		// Verifier que les dates de trajet ne correspondent pas a un trajet deja existant
		NSMutableArray trajets = new NSMutableArray(currentMission.segmentsTarif());
		if (currentSegment != null)
			trajets.removeIdenticalObject(currentSegment);

		for (int i=0;i<trajets.count();i++)	{

			EOSegmentTarif trajet = (EOSegmentTarif)trajets.objectAtIndex(i);

			if (DateCtrl.isBefore(trajet.segDebut(), debutSegment) && DateCtrl.isBefore(trajet.segFin(), debutSegment) )
				continue;

			if (DateCtrl.isAfter(trajet.segDebut(), finSegment) && DateCtrl.isAfter(trajet.segFin(), finSegment) )
				continue;

			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","CHEVAUCHEMENT DE TRAJETS !");
			return;
		}

		myView.dispose();
	}

	/**
	 *
	 */
	public void annuler()	{
		currentRembZone = null;
		myView.dispose();

	}

	/**
	 * 
	 * @param sender
	 */
	public void getDateDebut()	{

		CocktailUtilities.setMyTextField(myView.getTfDateDebut());
		CocktailUtilities.showDatePickerPanel(myView);

		selectCurrentTaux(DateCtrl.stringToDate(myView.getTfDateDebut().getText()));
	}


	/**
	 * 
	 * @param sender
	 */
	public void getDateFin()	{

		CocktailUtilities.setMyTextField(myView.getTfDateFin());
		CocktailUtilities.showDatePickerPanel(myView);

	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	private class ActionListenerHeureDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfHeureDebut().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureDebut().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début de mission n'est pas valide !");
				myView.getTfHeureDebut().selectAll();
			}
			else	{
				myView.getTfHeureDebut().setText(myTime);
				myView.getTfHeureFin().requestFocus();
			}
		}
	}


	/** 
	 * Classe d'ecoute sur la date de fin de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	private class ActionListenerHeureFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfHeureFin().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureFin().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de début de mission n'est pas valide !");
				myView.getTfHeureFin().selectAll();
			}
			else	{
				myView.getTfHeureFin().setText(myTime);
			}
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	private class ListenerTextFieldHeureDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfHeureDebut().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureDebut().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début de mission n'est pas valide !");
				myView.getTfHeureDebut().selectAll();
			}
			else	{
				myView.getTfHeureDebut().setText(myTime);
				myView.getTfHeureFin().requestFocus();
			}
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	private class ListenerTextFieldHeureFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfHeureFin().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureFin().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de début de mission n'est pas valide !");
				myView.getTfHeureFin().selectAll();
			}
			else	{
				myView.getTfHeureFin().setText(myTime);
			}
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerPays implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {			

			setCurrentWebPays((EOWebpays) eodWebPays.selectedObject());

			if (getCurrentWebPays() != null && getCurrentWebPays().toMonnaie() != null)
				eodWebTaux.setObjectArray(FinderWebTaux.findTauxForWebmonAndDate(edc, getCurrentWebPays().toMonnaie(), null));
			else
				eodWebTaux.setObjectArray(new NSArray());

			myView.getMyEOTableWebTaux().updateData();

			selectCurrentTaux(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		}
	}


	/**
	 * 
	 * @param date1
	 * @param date2
	 */
	public void selectCurrentTaux(NSTimestamp date1)	{

		if (getCurrentWebPays() != null)	{

			setCurrentWebTaux(FinderWebTaux.findWebTauxForWebmonDate(edc, getCurrentWebPays().toMonnaie(), date1));		   

			eodWebTaux.setSelectedObject(currentWebTaux);
			myView.getMyEOTableWebTaux().forceNewSelection(eodWebTaux.selectionIndexes());
			myView.getMyEOTableWebTaux().scrollToSelection();

		}

	}



	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerZone implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {			

			setCurrentRembZone((EORembZone)eodRembZone.selectedObject());

			myView.getTfFiltrePays().setText("");

			if (currentRembZone != null)
				eodWebPays.setObjectArray(FinderWebPays.findPaysForZone(edc, currentRembZone));
			else
				eodWebPays.setObjectArray(new NSArray());

			myView.getMyEOTableWebPays().updateData();		
		}
	}



	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerTaux implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {			
			currentWebTaux = (EOWebtaux)eodWebTaux.selectedObject();			
		}
	}


	/**
	 * 
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFiltrePays().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltrePays().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("wpaLibelle caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);        
	}

	/** 
	 *
	 */
	public void filter()	{
		eodWebPays.setQualifier(getFilterQualifier());
		eodWebPays.updateDisplayedObjects();              
		myView.getMyEOTableWebPays().updateData();
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 * @author Rodolphe Prin
	 */	
	private class FiltrePaysListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}



	/**
	 *
	 */
	public void windowClosing(WindowEvent e)	{

	}
}