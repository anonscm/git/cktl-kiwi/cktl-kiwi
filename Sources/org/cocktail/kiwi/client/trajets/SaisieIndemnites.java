/*
 * Created on 19 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.trajets;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.metier.EOIndemnite;
import org.cocktail.kiwi.client.nibctrl.SaisieIndemnitesView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SaisieIndemnites {

	private static SaisieIndemnites sharedInstance;
		
	private EOEditingContext ec;
	
	private SaisieIndemnitesView myView;
		
	private EOIndemnite currentIndemnite;
	private boolean cancelPanel;
	
	/** 
	 * Constructeur 
	 */
	public SaisieIndemnites(EOEditingContext editingContext)	{
		
		super();

		ec = editingContext;
		
		myView = new SaisieIndemnitesView(new JFrame(), true);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});
		
		CocktailUtilities.initTextField(myView.getTfJoursIndemnises(), false, false);
		CocktailUtilities.initTextField(myView.getTfJoursDeduits(), false, false);

	}	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieIndemnites sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieIndemnites(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 * @param indemnite
	 * @return
	 */
	public NSDictionary getJours(EOIndemnite indemnite)	{
		
		currentIndemnite = indemnite;
		
		if (!indemnite.canUpdateMontant())	{
			CocktailUtilities.initTextField(myView.getTfNuitsGratuites(), false, false);
			CocktailUtilities.initTextField(myView.getTfRepasGratuits(), false, false);
		}
		else	{
			CocktailUtilities.initTextField(myView.getTfNuitsGratuites(), false, indemnite.indNbNuits().intValue() > 0);
			CocktailUtilities.initTextField(myView.getTfRepasGratuits(), false, indemnite.indNbRepas().intValue() > 0);			
		}


		myView.getTfJoursIndemnises().setText(indemnite.indJours().toString());
		myView.getTfJoursDeduits().setText(indemnite.indJoursGratuits().toString());
		myView.getTfRepasGratuits().setText(indemnite.indRepasGratuits().toString());
		myView.getTfNuitsGratuites().setText(indemnite.indNuitsGratuites().toString());
				
		myView.setVisible(true);
		
		if (!cancelPanel)	{
			NSMutableDictionary result = new NSMutableDictionary();
			
			result.setObjectForKey(new BigDecimal(StringCtrl.replace(myView.getTfJoursIndemnises().getText(), ",", ".")),"jours");
			result.setObjectForKey(new BigDecimal(myView.getTfJoursDeduits().getText()),"joursDeduits");
			result.setObjectForKey(new Integer(myView.getTfRepasGratuits().getText()),"repasDeduits");
			result.setObjectForKey(new Integer(myView.getTfNuitsGratuites().getText()),"nuitsDeduites");
			
			return result;
		}
		
		return null;
	}
	
	
	
	public void valider()	{
		
		// Test nombre de repas gratuits		
		if (new Integer(myView.getTfRepasGratuits().getText()).intValue() > currentIndemnite.indNbRepas().intValue())	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Nombre de repas déduits trop important !");
			return;
		}
		
		// Test nombre de nuits gratuites		
		if (new Integer(myView.getTfNuitsGratuites().getText()).intValue() > currentIndemnite.indNbNuits().intValue())	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Nombre de nuits déduites trop important !");
			return;
		}

		cancelPanel = false;
		myView.dispose();
	}
	
	
	
	public void annuler()	{
		
		cancelPanel = true;
		myView.dispose();
		
	}
   
}
