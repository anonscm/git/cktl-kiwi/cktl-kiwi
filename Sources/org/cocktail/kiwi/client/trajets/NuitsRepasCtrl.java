/*
 * Created on 27 fevr. 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.trajets;

import java.awt.Color;
import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactoryNuits;
import org.cocktail.kiwi.client.factory.FactoryRepas;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EONuits;
import org.cocktail.kiwi.client.metier.EORepas;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.nibctrl.NuitsRepasView;
import org.cocktail.kiwi.common.utilities.AskForBigDecimal;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NuitsRepasCtrl {

	private static NuitsRepasCtrl sharedInstance;
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private NuitsRepasView myView;
	
	public	EODisplayGroup eodNuits, eodRepas;

	boolean modifMontantRepas;

	private EOSegmentTarif 	currentSegment;
	private	EONuits 		currentNuit;
	private	EORepas 		currentRepas;

	/** 
	 * Constructeur 
	 */
	public NuitsRepasCtrl(EOEditingContext editingContext)	{
		
		super();
				
		ec = editingContext;

		eodNuits = new EODisplayGroup();
		eodRepas = new EODisplayGroup();
		
		myView = new NuitsRepasView(eodNuits, eodRepas);
		
		myView.getBtnAjouterNuits().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addNuit();
			}
		});

		myView.getBtnModifierNuits().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierNuits();
			}
		});

		myView.getBtnSupprimerNuits().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delNuit();
			}
		});

		myView.getBtnRecalculerNutis().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDefaultNuits();
			}
		});

		myView.getBtnSasieNuits().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateMontantNuits();
			}
		});

		
		myView.getBtnAjouterRepas().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addRepas();
			}
		});

		myView.getBtnModifierRepas().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierRepas();
			}
		});

		myView.getBtnSupprimerRepas().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delRepas();
			}
		});

		myView.getBtnRecalculerRepas().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDefaultRepas();
			}
		});

		myView.getBtnSasieRepas().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateMontantRepas();
			}
		});

		
		
		
		myView.getTfTitreNuits().setBackground(new Color(194,176,190));
		myView.getTfTitreRepas().setBackground(new Color(194,176,190));

		String paramModifMontantRepas = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_CHECK_MONTANT_REPAS);

		modifMontantRepas = (paramModifMontantRepas != null && "O".equals(paramModifMontantRepas));

		myView.getBtnSasieRepas().setEnabled(modifMontantRepas || NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION));

		myView.getMyEOTableNuits().addListener(new ListenerNuits());
		myView.getMyEOTableRepas().addListener(new ListenerRepas());


	}

	/**
	 * 
	 *
	 */
	public void clean()	{

		eodNuits.setObjectArray(new NSArray());
		eodRepas.setObjectArray(new NSArray());
		myView.getMyEOTableNuits().updateUI();
		myView.getMyEOTableRepas().updateUI();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static NuitsRepasCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new NuitsRepasCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * @return
	 */
	public JPanel view()	{
		return myView;
	}


	/**
	 * 
	 * @param segment
	 */
	public void setCurrentSegment(EOSegmentTarif segment)	{
		currentSegment = segment;
	}

	/**
	 * 
	 *
	 */
	public void actualiser()	{

		clean();
		updateData();
		updateUI();		

	}

	/**
	 * 
	 *
	 */
	public void updateMontants()	{

		myView.getMyEOTableRepas().updateUI();
		myView.getMyEOTableNuits().updateUI();

		for (int i=0;i<eodNuits.displayedObjects().count();i++)	{
			EONuits nuit = (EONuits)eodNuits.displayedObjects().objectAtIndex(i);
			nuit.calculerMontant();
		}

		for (int i=0;i<eodRepas.displayedObjects().count();i++)	{
			EORepas repas = (EORepas)eodRepas.displayedObjects().objectAtIndex(i);
			repas.calculerMontant();
		}
	}

	/**
	 * 
	 *
	 */
	public void updateData()	{

		if (currentSegment != null)	{

			eodNuits.setObjectArray(EONuits.findForSegment(ec, currentSegment));		
			eodRepas.setObjectArray(EORepas.findForSegment(ec, currentSegment));		

			myView.getMyEOTableNuits().updateData();
			myView.getMyEOTableRepas().updateData();
		}
	}

	/**
	 * 
	 *
	 */
	public void updateUI()	{

		if (
				currentSegment == null 
				|| currentSegment.mission() == null
				|| (currentSegment.mission().isPayee())
				|| currentSegment.mission().isAnnulee() // Mission ANNULEE ==> Pas de modifs
		) {

			myView.getBtnAjouterNuits().setVisible(false);myView.getBtnAjouterRepas().setVisible(false);
			myView.getBtnRecalculerNutis().setVisible(false);myView.getBtnRecalculerRepas().setVisible(false);
			myView.getBtnSupprimerNuits().setVisible(false);myView.getBtnSupprimerRepas().setVisible(false);
			myView.getBtnModifierNuits().setVisible(false);myView.getBtnModifierRepas().setVisible(false);
			myView.getBtnSasieNuits().setVisible(false);
			myView.getBtnSasieRepas().setVisible(false);
		}
		else	{	// On a un segment de defini, on peut ajouter des nuits et des repas

			myView.getBtnAjouterNuits().setVisible(eodNuits.displayedObjects().count() == 0);
			myView.getBtnAjouterRepas().setVisible(eodRepas.displayedObjects().count() == 0);

			myView.getBtnModifierNuits().setVisible(currentNuit != null);
			myView.getBtnModifierRepas().setVisible(currentRepas != null);

			myView.getBtnSasieNuits().setVisible(currentNuit != null && currentNuit.canUpdateMontant());
			myView.getBtnSasieRepas().setVisible( (modifMontantRepas && currentRepas != null) || (currentRepas != null && NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION)) );

			myView.getBtnSupprimerNuits().setVisible(currentNuit != null);
			myView.getBtnSupprimerRepas().setVisible(currentRepas != null);

			myView.getBtnRecalculerNutis().setVisible(currentNuit != null);
			myView.getBtnRecalculerRepas().setVisible(currentRepas != null);
		}

	}

	/**
	 * 
	 *
	 */
	public void setDisabled()	 {

		myView.getBtnAjouterNuits().setVisible(false);myView.getBtnAjouterRepas().setVisible(false);
		myView.getBtnRecalculerNutis().setVisible(false);myView.getBtnRecalculerRepas().setVisible(false);
		myView.getBtnSupprimerNuits().setVisible(false);myView.getBtnSupprimerRepas().setVisible(false);
		myView.getBtnModifierNuits().setVisible(false);myView.getBtnModifierRepas().setVisible(false);
		myView.getBtnSasieNuits().setVisible(false);
	}

	/**
	 * Calcul automatique des nuits a rembourser pour le segment selectionne
	 * 
	 * @param sender
	 */
	public void addNuit()	{

		NSApp.setWaitCursor();

		try {

			FactoryNuits.sharedInstance().calculerNuits(ec, currentSegment, NSApp.getUtilisateur());

			ec.saveChanges();

		}
		catch (Exception e){
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur d'ajout des nuits !");
			ec.revert();
			e.printStackTrace();
		}

		actualiser();
		NSApp.setDefaultCursor();
	}

	/**
	 * 
	 * @param sender
	 */
	public void addRepas()	{

		NSApp.setWaitCursor();

		try {

			FactoryRepas.sharedInstance().calculerRepas(ec, currentSegment, NSApp.getUtilisateur());

			ec.saveChanges();
		}
		catch (Exception e){
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur d'ajout des repas !");
			ec.revert();
			e.printStackTrace();
		}

		actualiser();
		NSApp.setDefaultCursor();
	}


	/**
	 * 
	 * @param sender
	 */
	public void delNuit()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Supprimer la ligne 'Nuit' sélectionnée ?",
				"OUI", "NON")) 
			return;

		try {
			FactoryNuits.sharedInstance().supprimerNuit(ec, currentNuit);
			currentSegment.mission().setUtilisateurModificationRelationship(NSApp.getUtilisateur());		
			ec.saveChanges();
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors de la suppression de la nuit sélectionnée !");
			ec.revert();
			e.printStackTrace();
		}
		actualiser();
	}

	/**
	 * 
	 * @param sender
	 */
	public void delRepas()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Supprimer la ligne 'Repas' sélectionnée ?",
				"OUI", "NON")) 
			return;

		try {
			currentRepas.removeObjectFromBothSidesOfRelationshipWithKey(currentSegment, "segmentTarif");

			ec.deleteObject(currentRepas);
			currentSegment.mission().setUtilisateurModificationRelationship(NSApp.getUtilisateur());		
			ec.saveChanges();
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors de la suppression du repas sélectionnée !");
			ec.revert();
			e.printStackTrace();
		}
		actualiser();
	}

	/**
	 * 
	 * @param sender
	 */
	public void modifierNuits()	{

		NSDictionary dicoSaisie = SaisieNuits.sharedInstance(ec).getNuits(currentNuit);

		if (dicoSaisie != null)	{

			try {
				if (currentNuit != null)	{
//					currentNuit.repartirNuits(((BigDecimal)dicoSaisie.objectForKey("totalNuits")).intValue());
					currentNuit.setNuiNbNuits((Integer)dicoSaisie.objectForKey("totalNuits"));
					currentNuit.setNuiNuitGratuits((Integer)dicoSaisie.objectForKey("nuitsGratuites"));
					currentNuit.calculerMontant();
					currentNuit.setNuiEtat("VALIDE");
				}

				currentSegment.mission().setUtilisateurModificationRelationship(NSApp.getUtilisateur());		

				ec.saveChanges();
				myView.getMyEOTableNuits().updateUI();
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}

			myView.getMyEOTableNuits().updateUI();
		}
	}


	/**
	 * 
	 * @param sender
	 */
	public void updateMontantNuits()	{

		BigDecimal montantSaisi = new BigDecimal(0);

		try {

			montantSaisi = AskForBigDecimal.sharedInstance().getMontant("Modification Nuits", "Montant des nuits : ", currentNuit.nuiMontantPaiement());

			if (montantSaisi != null) {

				// Ne pas depasser le seuil vote
				String checkSeuilNuits = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_CHECK_PLAFOND_NUITS);
				if (checkSeuilNuits == null || checkSeuilNuits.equals("O"))	{

					BigDecimal nbNuits = new BigDecimal(currentNuit.nuiNbNuits().intValue() - currentNuit.nuiNuitGratuits().intValue());
					BigDecimal tarifMaximum = currentNuit.tarifNuit().multiply(nbNuits);

					if (montantSaisi.floatValue() > tarifMaximum.floatValue())	{
						EODialogs.runErrorDialog("ERREUR","Vous ne pouvez pas dépasser le seuil de " + tarifMaximum.toString() + " \u20ac pour le remboursement des nuits");
						return;						
					}					
				}

				currentNuit.setNuiMontantPaiement(montantSaisi);

				ec.saveChanges();
				myView.getMyEOTableNuits().updateUI();	

			}

		}
		catch (Exception e)	{
			e.printStackTrace();
		}

	}



	/**
	 * 
	 * @param sender
	 */
	public void updateMontantRepas()	{

		BigDecimal montantSaisi = new BigDecimal(0);

		try {

			montantSaisi = AskForBigDecimal.sharedInstance().getMontant("Modification Repas", "Montant des repas : ", currentRepas.repMontantPaiement());

			if (montantSaisi != null) {
				// Ne pas depasser le seuil vote
				String checkSeuilRepas = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_CHECK_PLAFOND_REPAS);
				if (checkSeuilRepas == null || checkSeuilRepas.equals("O"))	{

					BigDecimal nbRepas = currentRepas.repNbRepas();
					BigDecimal tarifRepas = currentRepas.tarifRepas().multiply(nbRepas);

					if (montantSaisi.floatValue() > tarifRepas.floatValue())	{

						EODialogs.runErrorDialog("ERREUR","Vous ne pouvez pas dépasser le seuil de " + tarifRepas.toString() + " \u20ac pour le remboursement des repas !");
						return;						
					}					
				}

				currentRepas.setRepMontantPaiement(montantSaisi);

				ec.saveChanges();
				myView.getMyEOTableRepas().updateUI();				
			}

		}
		catch (Exception e) {

			e.printStackTrace();

		}


	}



	/**
	 * 
	 *
	 */
	public void calculerMontants()	{

		try {
			currentNuit.calculerMontant();
			currentRepas.calculerMontant();
			ec.saveChanges();
		}
		catch (Exception e)	{
			ec.revert();
		}

		myView.getMyEOTableNuits().updateUI();
		myView.getMyEOTableRepas().updateUI();

	}

	/**
	 * 
	 * @param sender
	 */
	public void modifierRepas()	{ 

		NSDictionary dicoSaisie = SaisieRepas.sharedInstance(ec).getRepas(currentRepas);

		if (dicoSaisie != null)	{
			try {

				currentRepas.setRepNbRepas((BigDecimal)dicoSaisie.objectForKey("totalRepas"));
				currentRepas.setRepRepasAdm((BigDecimal)dicoSaisie.objectForKey("repasAdmin"));
				currentRepas.setRepRepasGratuits((BigDecimal)dicoSaisie.objectForKey("repasGratuits"));

				currentRepas.calculerMontant();

				currentSegment.mission().setUtilisateurModificationRelationship(NSApp.getUtilisateur());		

				ec.saveChanges();			
				myView.getMyEOTableRepas().updateUI();
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}
		}
	}



	/**
	 * 
	 */
	public boolean calculerDefaultRepas(EOSegmentTarif segment) {

		try {

			FactoryRepas.supprimerRepas(ec, segment);

			FactoryRepas.sharedInstance().calculerRepas(ec, segment, NSApp.getUtilisateur());

			return true;
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",e.getMessage());
			e.printStackTrace();
			return false;
		}		
	}

	/**
	 * 
	 * @return
	 */
	public boolean calculerDefaultNuits(EOSegmentTarif segment) {

		try {

			FactoryNuits.supprimerNuits(ec, segment);

			FactoryNuits.sharedInstance().calculerNuits(ec, segment, NSApp.getUtilisateur());

			return true;
		}
		catch (Exception e)	{
			return false;
		}

	}

	/**
	 * 
	 * @param sender
	 */
	public void getDefaultNuits()	{

		try {

			calculerDefaultNuits(currentSegment);

			ec.saveChanges();			
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors du calcul des nuits !");
			ec.revert();
			e.printStackTrace();
			actualiser();
			return;
		}

		actualiser();
		myView.getMyEOTableNuits().updateUI();

	}

	/**
	 * 
	 * @param sender
	 */
	public void getDefaultRepas()	{

		// On supprime toutes les entrees et on recalcule les repas
		try {
			for (int i=0;i<eodRepas.displayedObjects().count();i++)	{
				EORepas repas = (EORepas)eodRepas.displayedObjects().objectAtIndex(i);
				repas.removeObjectFromBothSidesOfRelationshipWithKey(currentSegment, "segmentTarif");
				ec.deleteObject(repas);
			}
			ec.saveChanges();			
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors de la suppression du repas sélectionnée !");
			ec.revert();
			e.printStackTrace();
			actualiser();
			return;
		}

		addRepas();
		actualiser();
		myView.getMyEOTableRepas().updateUI();
	}




	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerRepas implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if ( currentSegment.mission().isValide())
				modifierRepas();			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {			
			currentRepas = (EORepas)eodRepas.selectedObject();
			updateUI();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerNuits implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			if ( currentSegment.mission().isValide())
				modifierNuits();			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentNuit = (EONuits)eodNuits.selectedObject();
		}

	}

}
