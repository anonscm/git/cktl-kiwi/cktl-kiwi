/*
 * Created on 19 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.trajets;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EONuits;
import org.cocktail.kiwi.client.nibctrl.SaisieNuitsView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SaisieNuits {
	
	private static SaisieNuits sharedInstance;
	private EOEditingContext ec;
	
	private SaisieNuitsView myView;
		
	private boolean cancelPanel;
	
	/** 
	 * Constructeur 
	 */
	public SaisieNuits(EOEditingContext editingContext)	{
		
		super();

		ec = editingContext;
		
		myView = new SaisieNuitsView(new JFrame(), true);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});
		
	}	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieNuits sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieNuits(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 * @param nuit
	 * @return
	 */
	public NSDictionary getNuits(EONuits nuit)	{
		
		String paramModifTotal = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_CHECK_NB_NUITS);

		if ( (paramModifTotal != null && "O".equals(paramModifTotal)) || nuit.segmentTarif().mission().isMissionPermanente() )
			CocktailUtilities.initTextField(myView.getTfNbNuits(), false, true);
		else
			CocktailUtilities.initTextField(myView.getTfNbNuits(), false, false);

		CocktailUtilities.setNumberToField(myView.getTfNbNuits(), nuit.nuiNbNuits());
		CocktailUtilities.setNumberToField(myView.getTfNbGratuites(), nuit.nuiNuitGratuits());
		
		myView.setVisible(true);
		
		if (!cancelPanel)	{			
					
			NSMutableDictionary result = new NSMutableDictionary();
			
			result.setObjectForKey(CocktailUtilities.getIntegerFromField(myView.getTfNbNuits()),"totalNuits");
			result.setObjectForKey(CocktailUtilities.getIntegerFromField(myView.getTfNbGratuites()),"nuitsGratuites");
			
			return result;
		}
		
		return null;
	}
	

	/**
	 * 
	 */
	public void valider()	{
		
		if (new BigDecimal(myView.getTfNbGratuites().getText()).floatValue() > new BigDecimal(myView.getTfNbNuits().getText()).floatValue())	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Le nombre de nuits gratuites ne peut etre supérieur au nombre de nuits total !");
			return;
		}
		
		cancelPanel = false;
		myView.dispose();
	}

	
	/**
	 * 
	 */
	public void annuler()	{
		
		cancelPanel = true;
		myView.dispose();
	}
	
}
