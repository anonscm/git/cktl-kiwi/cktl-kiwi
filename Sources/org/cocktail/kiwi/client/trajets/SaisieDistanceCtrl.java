package org.cocktail.kiwi.client.trajets;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactoryDistancesKm;
import org.cocktail.kiwi.client.metier.EODistancesKm;
import org.cocktail.kiwi.client.nibctrl.SaisieDistanceView;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieDistanceCtrl extends SaisieDistanceView 
{
	private static SaisieDistanceCtrl sharedInstance;
	
	private 	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private 	EOEditingContext		ec;
		
	private EODistancesKm 			currentDistance;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieDistanceCtrl (EOEditingContext globalEc) {

		super(new JFrame(), true);

		ec = globalEc;
		
		buttonValider.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		buttonAnnuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieDistanceCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieDistanceCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		tfDepart.setText("");
		tfArrivee.setText("");
		tfDistance.setText("");
	
	}
	
	
	
	/**
	 * 
	 * Fournis = null ==> Saisie du fournisseur
	 * 
	 * @param fournis		Fournisseur associe au vehicule	
	 * @return
	 */
	public EODistancesKm ajouter()	{

		clearTextFields();

		modeModification = false;
				
		// Creation et initialisation du nouveau vehicule
		currentDistance = FactoryDistancesKm.sharedInstance().creerTrajet(ec);
		
		show();	// Ouverture de la fenetre de saisie en mode modal.

		return currentDistance;
		
	}
	
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifier(EODistancesKm distance)	{
						
		clearTextFields();

		currentDistance = distance;

		modeModification = true;

		tfDepart.setText(currentDistance.lieuDepart());
		tfArrivee.setText(currentDistance.lieuArrivee());
		tfDistance.setText(currentDistance.distance().toString());

		show();
		
		return (currentDistance != null);
	}
	
	
	
	
	/**
	 *
	 */
	private void valider()	{
		
		try {

			currentDistance.setLieuDepart(tfDepart.getText());
			currentDistance.setLieuArrivee(tfArrivee.getText());
			
			tfDistance.setText((NSArray.componentsSeparatedByString(tfDistance.getText(),",")).componentsJoinedByString("."));			
			currentDistance.setDistance(new BigDecimal(tfDistance.getText()));
			
			currentDistance.setDModification(new NSTimestamp());
			
			ec.saveChanges();
			
		}
		catch (ValidationException ex)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Veuillez vérifier le format du nombre de kilomètres !");
			return;
		}

		dispose();
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentDistance);
			
		currentDistance = null;
		
		dispose();
		
	}
	
}