package org.cocktail.kiwi.client;

import java.math.BigDecimal;

import org.cocktail.application.client.ServerProxyCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Rassemble tous les appels aux methodes definies sur le serveur.
 */

public class ServerProxy  extends ServerProxyCocktail {


	public static final NSArray clientSideRequestCheckMissionsPayees(EOEditingContext ec) {
		return (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestCheckMissionsPayees", new Class[] {}, new Object[] {}, false);
	} 

	public static final String clientSideRequestGetResponsableService(EOEditingContext ec, Number noIndividu) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestGetResponsableService", new Class[] {Number.class}, new Object[] {noIndividu}, false);
	} 

	public static String serverApplicationFinalName (EOEditingContext ec)  {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestApplicationFinalName", new Class[] {}, new Object[] {}, false);
	}

	public static String serverBdConnexionName (EOEditingContext ec)  {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);
	}

	public static String serverLogs(EOEditingContext ec)  {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestServerLogs", new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestSendMail(EOEditingContext ec, String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) throws Exception {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestSendMail", new Class[] {String.class,String.class,String.class,String.class,String.class}, new Object[] { mailFrom, mailTo, mailCC, mailSubject, mailBody }, false);
	}

	public static String clientSideRequestGetParam(EOEditingContext ec, String paramKey)  {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestGetParam", new Class[] {String.class}, new Object[] {paramKey}, false);
	}

	public static String annulerMission(EOEditingContext ec, Number misOrdre) throws Exception {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestAnnulerMission", 
				new Class[] {Number.class}, 
				new Object[] {misOrdre},	
				false);
	}

	public static String setMissionSansFrais(EOEditingContext ec, Number misOrdre, Number utlOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (ec, "session", "clientSideRequestSetMissionSansFrais", new Class[] {Number.class, Number.class}, new Object[] {misOrdre, utlOrdre},	false);
	}

	public static void delDepense(EOEditingContext ec, Number depid) throws Exception {
		try {
			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (ec, "session", "clientSideRequestDelDepense", new Class[] {Number.class}, new Object[] {depid},	false);
		}
		catch (Exception e) {
			throw e;
		}
	}

	public static String clientSideRequestDelMissionPaiementEngage(EOEditingContext ec, Number mpeOrdre, Number utlOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (ec, "session", "clientSideRequestDelMissionPaiementEngage", new Class[] {Number.class, Number.class}, new Object[] {mpeOrdre, utlOrdre},	false);
	}

	/** Detruit la commande associee a une mission */
	public static void clientSideRequestUpdateEngage(EOEditingContext ec, NSDictionary parametres) throws Exception {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestUpdateEngage", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}

	public static void clientSideRequestModifierExerciceBudgetaire(EOEditingContext ec, NSDictionary parametres) throws Exception {

		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestModifierExerciceBudgetaire", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}


	public static String clientSideRequestInitialiserSignataires(EOEditingContext ec) {

		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestInitialiserSignataires", 
				null,  
				null, false);
	}

	public static String clientSideRequestInitialiserSignatairesServices(EOEditingContext ec) {

		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestInitialiserSignatairesServices", 
				null,  
				null, false);
	} 


	public static String clientSideRequestRevaliderMission(EOEditingContext ec, NSDictionary parametres) {

		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestRevaliderMission", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, false);
	}


	public static String clientSideRequestNumerotationMission(EOEditingContext ec, NSDictionary parametres) {

		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestNumerotationMission", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, false);
	}


	public static String clientSideRequestCreerAvance(EOEditingContext ec, NSDictionary parametres) {

		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestCreerAvance", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, false);
	}

	public static String clientSideRequestLiquiderAvance(EOEditingContext ec, NSDictionary parametres) {

		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestLiquiderAvance", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, false);
	}

	public static String clientSideRequestGenererOp(EOEditingContext ec, NSDictionary parametres) {

		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestGenererOp", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, false);
	}

	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @return
	 */
	public static void clientSideRequestSupprimerAvance(EOEditingContext ec, NSDictionary parametres) {

		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestSupprimerAvance", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, false);
	}



	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @return
	 */
	public static Number clientSideRequestgetNumeroMission(EOEditingContext ec, Number exercice) {
		return (Number)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestgetNumeroMission", 
				new Class[] {Number.class},  
				new Object[] {exercice}, false);
	}


	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @return
	 */
	public static Number clientSideRequestGetDisponible(EOEditingContext ec, NSDictionary parametres) {
		return (Number)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", "clientSideRequestGetDisponible", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, false);
	}


	/**
	 * 
	 * Recuperation du disponible budgetaire en fonction d'un exercice, organ, typeCredit
	 * 
	 * @param ec
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @param convention
	 * @return
	 */
	public static BigDecimal clientSideRequestGetDisponible(
			EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit, EOConvention convention) {

		try {
			if (organ == null || exercice == null || typeCredit == null || organ.orgNiveau().intValue() < 2)
				return new BigDecimal(0);

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(exercice, 	"EOExercice");
			parametres.setObjectForKey(organ, 		"EOOrgan");			
			parametres.setObjectForKey(typeCredit, 	"EOTypeCredit");

			if (convention != null)
				parametres.setObjectForKey(convention, "EOConvention");

			return (BigDecimal)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestGetDisponible", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new BigDecimal(0);
		}
	}

	/** Detruit la commande associee a une mission */
	public static NSArray clientSideRequestServeurDePlanning(EOEditingContext ec, NSDictionary parametres) throws Exception {

		return (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestServeurDePlanning", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}


	public static String engagerMission(EOEditingContext ec, Number mipOrdre, Number utlOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestEngagerMission", new Class[] {Number.class, Number.class},  new Object[] {mipOrdre, utlOrdre}, false);
	}

	public static String engagerEtatFrais(EOEditingContext ec, Number mipOrdre, Number utlOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestEngagerEtatFrais", new Class[] {Number.class, Number.class},  new Object[] {mipOrdre, utlOrdre}, false);
	}

	public static String desengagerMission(EOEditingContext ec, Number mipOrdre, Number utlOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (ec, "session", "clientSideRequestDesengagerMission", new Class[] {Number.class, Number.class}, new Object[] {mipOrdre, utlOrdre},	false);
	}

	public static void reimputerMission(EOEditingContext ec, Number depId, Number misOrdre, Number utlOrdre) throws Exception	{ 
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (ec, "session", "clientSideRequestReimputerMission", new Class[] {Number.class, Number.class, Number.class}, new Object[] {depId, misOrdre, utlOrdre},	false);
	}

	public static String desengagerMissionPartiel(EOEditingContext ec, Number mpeOrdre, Number utlOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (ec, "session", "clientSideRequestDesengagerMissionPartiel", new Class[] {Number.class, Number.class}, new Object[] {mpeOrdre, utlOrdre},	false);
	}


	public static String solderEngage(EOEditingContext ec, Number mpeOrdre, Number utlOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (ec, "session", "clientSideRequestSolderEngage", new Class[] {Number.class, Number.class}, new Object[] {mpeOrdre, utlOrdre},	false);
	}


	public static String liquidation(EOEditingContext ec, Number leMipOrdre)  throws Exception	{
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestLiquidation", new Class[] {Number.class},  new Object[] {leMipOrdre}, false);
	}
	public static String liquidationDefinitive(EOEditingContext ec, Number leMipOrdre)  throws Exception	{
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestLiquidationDefinitive", new Class[] {Number.class},  new Object[] {leMipOrdre}, false);
	}

	public static String reverser(EOEditingContext ec, Number mipOrdre, BigDecimal montant, Number utlOrdre, Number depId)  throws Exception	{
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestReverser", 
				new Class[] {Number.class, BigDecimal.class, Number.class, Number.class},  
				new Object[] {mipOrdre, montant, utlOrdre, depId}, 
				false);
	}

	public static String liquidationAvance(EOEditingContext ec, Number leMipOrdre) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestLiquidationAvance", new Class[] {Number.class},  new Object[] {leMipOrdre}, false);
	}

	public static Boolean traiterTelechargements(EOEditingContext ec) {
		return (Boolean)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestTraiterTelechargements", new Class[] {},  new Object[] {}, false);
		
	}
}