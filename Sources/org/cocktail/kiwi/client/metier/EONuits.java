

// EONuits.java
// 
package org.cocktail.kiwi.client.metier;


import java.math.BigDecimal;
import java.util.GregorianCalendar;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderPlageHoraire;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EONuits extends _EONuits {

	public EONuits() {
		super();
	}

	public static NSArray<EONuits> findForSegment(EOEditingContext ec, EOSegmentTarif segment)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEGMENT_TARIF_KEY + " = %@", new NSArray(segment)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);

	}

	public String stringMontantPaiement()	{
		return nuiMontantPaiement()+ " \u20ac";
	}

	/**
	 * 
	 * @return
	 */
	public boolean canUpdateMontant()	{

		if (segmentTarif() == null)
			return true;

		return DateCtrl.isAfterEq(segmentTarif().segFin(), DateCtrl.stringToDate("01/11/2006 00:00","%d/%m/%Y %H:%M"));

	}


	/**
	 * 
	 *
	 */
	public String stringTarifNuit()	{
		try {
			return tarifNuit().toString() + " \u20ac / Nuit";
		}
		catch (Exception e)	{
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal tarifNuit()	{

		try {		
			EOMission mission = segmentTarif().mission();
			BigDecimal tarif = rembJournalier().remTarif();

			tarif = tarif.multiply(new BigDecimal(mission.numQuotientRemb().intValue()));
			tarif = tarif.divide(new BigDecimal(mission.denQuotientRemb().intValue()), 2, BigDecimal.ROUND_HALF_UP);

			return tarif;
		}
		catch (Exception e)	{
			return null;
		}		
	}

	/**
	 * Une indemnite de nuitee lorsque l'agent se trouve en mission pendant la totalite de 
	 * la periode comprise entre zero heure et 5 heures.
	 *
	 */
	public void calculerNuits(EOEditingContext ec, NSTimestamp segmentDebut, NSTimestamp segmentFin)	{

		int nbNuits = 0;

		System.out.println(">>>> CALCUL NUITS du " + segmentDebut + " au " + segmentFin);

		NSTimestamp debutCalcul = segmentDebut;

		EOPlageHoraire plageNuit = FinderPlageHoraire.findPlageHoraire(ec, new Integer(1));
		System.out.println("\t\tPlage horaire : " + plageNuit.plaDebut() + " à " + plageNuit.plaFin());

		int heureFinNuit = new Integer(plageNuit.plaDebut().substring(0,2)).intValue();
		int heureDebutNuit = new Integer(plageNuit.plaFin().substring(0,2)).intValue();
		int minutesDebutNuit = new Integer(plageNuit.plaFin().substring(3,5)).intValue();

		System.out.println("\t\t Heure fin nuit : " + heureFinNuit);
		System.out.println("\t\t Heure debut nuit : " + heureDebutNuit);
		System.out.println("\t\t Minutes debut nuit : " + minutesDebutNuit);

		GregorianCalendar calDebutCalcul = new GregorianCalendar();
		GregorianCalendar calFinCalcul = new GregorianCalendar();
		calDebutCalcul.setTime(segmentDebut);
		calFinCalcul.setTime(segmentFin);

		int heureDepart = calDebutCalcul.get(GregorianCalendar.HOUR_OF_DAY);
		int minuteDepart = calDebutCalcul.get(GregorianCalendar.MINUTE);
		int heureArrivee = calFinCalcul.get(GregorianCalendar.HOUR_OF_DAY);

		System.out.println("\t\t Heure depart : " + heureDepart);
		System.out.println("\t\t Minutre depart : " + minuteDepart);
		System.out.println("\t\t Heure arrivée : " + heureArrivee);

		// Si le premier jour ne commence pas a minuit on se place sur le jour suivant a 00:00
		if (heureDepart > 0 || minuteDepart > 0)
			calDebutCalcul.setTime(debutCalcul.timestampByAddingGregorianUnits(0,0,1,-heureDepart,-minuteDepart,0));

		// On compte le nombre de jours entre la date de fin de calcul et la date de debut
		long ecartMinutes = (calFinCalcul.getTimeInMillis() - calDebutCalcul.getTimeInMillis()) / (60*1000);

		System.out.println("\t\tEcart minutes : " + ecartMinutes);

		if (ecartMinutes > 0)	{
			nbNuits = (int)(ecartMinutes / (24*60));
			System.out.println("\t\tNb nuits : " + nbNuits);
			if (heureArrivee >= heureFinNuit || ((heureDepart== heureDebutNuit && minuteDepart == minutesDebutNuit) && heureArrivee > heureFinNuit))
				System.out.println("\t\tAjout d'une nuit supplementaire");
			nbNuits = nbNuits + 1;
		}

		System.out.println("\t\tNombre total de nuits : " + nbNuits);

		setNuiNbNuits(new Integer(nbNuits));

	}

	/**
	 * 
	 */
	public void calculerMontant()	{

		BigDecimal tarif = new BigDecimal(0);

		if (webmiss() != null)	{
			tarif = webmiss().wmiGroupe1().multiply(segmentTarif().webtaux().wtaTaux());
		}

		// si Journalier recup des dates du tarif Journalier et montant tarif
		if (rembJournalier() != null)	{

			tarif = tarifNuit();

			//			BigDecimal montantNuits =nuiNbNuits().multiply(tarif);

			// Montant des nuits : 1 a 10 : Tarif ; 10-30 : tarif*90% ; >30 : tarif*80%
			//			montantNuits = nuiNbNuits().multiply(tarif);
			//			montantNuits = montantNuits.add(nuiNuit30().multiply(tarif));
			//			montantNuits = montantNuits.add(nuiNuitPlus().multiply(tarif));
			//			
			//			int totalNuits = nuiNbNuits().intValue();
			//			int totalNuitsGratuites = nuiNuitGratuits().intValue();

			BigDecimal resteNuits = new BigDecimal(nuiNbNuits().intValue() - nuiNuitGratuits().intValue());
			BigDecimal montantNuits = resteNuits.multiply(tarif);

			//			if (resteNuits.intValue() <= 10)	{
			//				montantNuits= resteNuits.multiply(tarif);
			//				montantNuits= resteNuits.multiply(tarif);
			//			}
			//			else	{  // Nuits > 30
			//				if (resteNuits.intValue() > 30)	{
			//					montantNuits = new BigDecimal(10).multiply(tarif);
			//					montantNuits = montantNuits.add(new BigDecimal(20).multiply(tarif));
			//					int reste = resteNuits.intValue() - 30;
			//					montantNuits = montantNuits.add(new BigDecimal(reste).multiply(tarif));
			//				}
			//				else	{	// Nuits entre 10 et 30
			//					montantNuits = new BigDecimal(10).multiply(tarif);
			//					int reste = resteNuits.intValue() - 10;
			//					montantNuits = montantNuits.add(new BigDecimal(reste).multiply(tarif));            	
			//				}
			//			}

			// Mise a jour du montant de paiement
			setNuiMontantPaiement(montantNuits.setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP));
		}
	}

	public void validateForSave () throws NSValidation.ValidationException
	{
		if (rembJournalier() == null && webmiss() == null)
			throw new NSValidation.ValidationException( "VOUS N AVEZ PAS DE ZONE NI DE TARIFS!");
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {


	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}
