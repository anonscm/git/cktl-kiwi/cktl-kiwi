

// EOParamImputation.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOParamImputation extends _EOParamImputation {

    public EOParamImputation() {
        super();
    }

	public static NSArray findParamImputations(EOEditingContext ec, EOExercice exercice)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(TO_EXERCICE_KEY + " = %@", new NSArray(exercice));
			EOFetchSpecification fs = new EOFetchSpecification(EOParamImputation.ENTITY_NAME,myQualifier, null);
			
			return ec.objectsWithFetchSpecification(fs);
	}


	/**
	 * 
	 * @param ec
	 * @param annee
	 * @param code
	 * @return
	 */
	public static EOParamImputation findParamForCode(EOEditingContext ec, EOExercice exercice, String code)	{

		NSMutableArray args = new NSMutableArray();
		args.addObject(exercice);
		args.addObject(code);
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(TO_EXERCICE_KEY + "=%@ and planco.pcoNum = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(EOParamImputation.ENTITY_NAME,myQualifier, null);
			
		try {return (EOParamImputation)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	  {return null;}
	}

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
