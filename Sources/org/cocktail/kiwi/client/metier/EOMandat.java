

// EOMandat.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOMandat extends _EOMandat {

    public EOMandat() {
        super();
    }

	public static NSArray findForDepense(EOEditingContext ec, EODepense depense)	{

			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(""+
					DEPENSE_CTRL_PLANCOS_KEY + "." + EODepenseCtrlPlanco.DEPENSE_KEY + " = %@ ", new NSArray(depense)));

			return fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);

	}

	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
