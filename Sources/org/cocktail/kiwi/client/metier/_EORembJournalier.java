// _EORembJournalier.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORembJournalier.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORembJournalier extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RembJournalier";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.REMB_JOURNALIER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "remOrdre";

	public static final String REM_DATE_KEY = "remDate";
	public static final String REM_DATE_FIN_KEY = "remDateFin";
	public static final String REM_TARIF_KEY = "remTarif";

//Colonnes dans la base de donnees
	public static final String REM_DATE_COLKEY = "REM_DATE";
	public static final String REM_DATE_FIN_COLKEY = "REM_DATE_FIN";
	public static final String REM_TARIF_COLKEY = "REM_TARIF";

// Relationships
	public static final String TO_PLAGE_HORAIRE_KEY = "toPlageHoraire";
	public static final String TO_ZONE_KEY = "toZone";



	// Accessors methods
  public NSTimestamp remDate() {
    return (NSTimestamp) storedValueForKey(REM_DATE_KEY);
  }

  public void setRemDate(NSTimestamp value) {
    takeStoredValueForKey(value, REM_DATE_KEY);
  }

  public NSTimestamp remDateFin() {
    return (NSTimestamp) storedValueForKey(REM_DATE_FIN_KEY);
  }

  public void setRemDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, REM_DATE_FIN_KEY);
  }

  public java.math.BigDecimal remTarif() {
    return (java.math.BigDecimal) storedValueForKey(REM_TARIF_KEY);
  }

  public void setRemTarif(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REM_TARIF_KEY);
  }

  public org.cocktail.kiwi.client.metier.EOPlageHoraire toPlageHoraire() {
    return (org.cocktail.kiwi.client.metier.EOPlageHoraire)storedValueForKey(TO_PLAGE_HORAIRE_KEY);
  }

  public void setToPlageHoraireRelationship(org.cocktail.kiwi.client.metier.EOPlageHoraire value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOPlageHoraire oldValue = toPlageHoraire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAGE_HORAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAGE_HORAIRE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EORembZone toZone() {
    return (org.cocktail.kiwi.client.metier.EORembZone)storedValueForKey(TO_ZONE_KEY);
  }

  public void setToZoneRelationship(org.cocktail.kiwi.client.metier.EORembZone value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EORembZone oldValue = toZone();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ZONE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ZONE_KEY);
    }
  }
  

  public static EORembJournalier createRembJournalier(EOEditingContext editingContext, NSTimestamp remDate
, java.math.BigDecimal remTarif
, org.cocktail.kiwi.client.metier.EOPlageHoraire toPlageHoraire, org.cocktail.kiwi.client.metier.EORembZone toZone) {
    EORembJournalier eo = (EORembJournalier) createAndInsertInstance(editingContext, _EORembJournalier.ENTITY_NAME);    
		eo.setRemDate(remDate);
		eo.setRemTarif(remTarif);
    eo.setToPlageHoraireRelationship(toPlageHoraire);
    eo.setToZoneRelationship(toZone);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORembJournalier.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORembJournalier.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EORembJournalier localInstanceIn(EOEditingContext editingContext) {
	  		return (EORembJournalier)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORembJournalier localInstanceIn(EOEditingContext editingContext, EORembJournalier eo) {
    EORembJournalier localInstance = (eo == null) ? null : (EORembJournalier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORembJournalier#localInstanceIn a la place.
   */
	public static EORembJournalier localInstanceOf(EOEditingContext editingContext, EORembJournalier eo) {
		return EORembJournalier.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORembJournalier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORembJournalier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORembJournalier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORembJournalier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORembJournalier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORembJournalier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORembJournalier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORembJournalier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORembJournalier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORembJournalier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORembJournalier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORembJournalier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
