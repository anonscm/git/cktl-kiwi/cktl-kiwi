
// _EOVLbConvention.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOVLbConvention.java instead.

package org.cocktail.kiwi.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOVLbConvention extends EOGenericRecord {

    public static final String ENTITY_NAME = "VLbConvention";

    public static final String ENTITY_TABLE_NAME = "CONVENTION.V_LB_CONVENTION";

    public static final String EXE_ORDRE_KEY = "exeOrdre";
    public static final String TCD_CODE_KEY = "tcdCode";

    public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
    public static final String TCD_CODE_COLKEY = "TCD_CODE";

    public static final String V_TRANCHE_GP_CONVENTION_KEY = "vTrancheGpConvention";




	
    public _EOVLbConvention() {
        super();
    }




    public Number exeOrdre() {
        return (Number)storedValueForKey(EXE_ORDRE_KEY);
    }
    public void setExeOrdre(Number aValue) {
        takeStoredValueForKey(aValue, EXE_ORDRE_KEY);
    }

    public String tcdCode() {
        return (String)storedValueForKey(TCD_CODE_KEY);
    }
    public void setTcdCode(String aValue) {
        takeStoredValueForKey(aValue, TCD_CODE_KEY);
    }




    public org.cocktail.kiwi.client.metier.budget.EOConvention vTrancheGpConvention() {
        return (org.cocktail.kiwi.client.metier.budget.EOConvention)storedValueForKey(V_TRANCHE_GP_CONVENTION_KEY);
    }
    public void setVTrancheGpConvention(org.cocktail.kiwi.client.metier.budget.EOConvention aValue) {
        takeStoredValueForKey(aValue, V_TRANCHE_GP_CONVENTION_KEY);
    }
	
    public void setVTrancheGpConventionRelationship(org.cocktail.kiwi.client.metier.budget.EOConvention value) {
        if (value == null) {
            org.cocktail.kiwi.client.metier.budget.EOConvention object = vTrancheGpConvention();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, V_TRANCHE_GP_CONVENTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, V_TRANCHE_GP_CONVENTION_KEY);
        }
    }





}

