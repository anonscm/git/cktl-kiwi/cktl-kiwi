

// EOVehicule.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOVehicule extends _EOVehicule {

	public static String ETAT_VALIDE = "VALIDE";
	public static String ETAT_ANNULE = "ANNULE";
	
    public EOVehicule() {
        super();
    }
    
	public static NSArray find(EOEditingContext ec)	{
		
		try {

			NSMutableArray mySort = new NSMutableArray();
			mySort.addObject(new EOSortOrdering(EOVehicule.FOURNIS_KEY+"."+EOFournis.NOM_KEY, EOSortOrdering.CompareAscending));
						
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.FOURNIS_KEY + " = nil or " + EOVehicule.FOURNIS_KEY+"."+EOFournis.FOU_VALIDE_KEY  + " = %@", new NSArray("O")));

			EOFetchSpecification fs = new EOFetchSpecification(EOVehicule.ENTITY_NAME, new EOAndQualifier(mesQualifiers),null);

			fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOVehicule.FOURNIS_KEY));
			
			return ec.objectsWithFetchSpecification(fs);
			
			//return fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}	
	}
	
		

	public static NSArray findForTransportAndFournis(EOEditingContext ec, EOTypeTransport typeTransport, EOFournis fournis)	{
		
		try {

			NSArray mySort = new NSArray(new EOSortOrdering(EOVehicule.VEH_ETAT_KEY, EOSortOrdering.CompareDescending));

			NSMutableArray mesQualifiers = new NSMutableArray();

			if (fournis != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.FOURNIS_KEY + " = %@", new NSArray(fournis)));

			if (typeTransport != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.TYPE_TRANSPORT_KEY + " = %@", new NSArray(typeTransport)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVehicule.VEH_ETAT_KEY + " != 'ANNULE'", null));

			return fetchAll(ec, new EOAndQualifier(mesQualifiers), mySort, true);

		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}
    
    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      
		if (typeTransport() == null)
			throw new ValidationException("Vous devez saisir un type de transport associé à ce véhicule !");

		if (fournis() == null && (typeTransport().isVehiculePersonnel() || typeTransport().isVehiculeSncf()))
			throw new ValidationException("Vous devez saisir un fournisseur associé à ce véhicule !");

		if (vehMarque() == null)
			throw new ValidationException("Vous devez saisir une marque de véhicule!");

		if (vehImmatriculation() == null && typeTransport().isVehiculePersonnel())
			throw new ValidationException("Vous devez saisir une immatriculation !");

		if (vehImmatriculation() != null && vehImmatriculation().length() > 10)
			throw new ValidationException("L'immatriculation ne doit pas dépasser 10 caractères !");

//		NSArray vehiculesMission = FinderMission.findMissionsForVehicule(ec, currentVehicule);

//		if (!modeModification)	{
//
//			NSArray vehiculesFournis = FinderVehicule.findVehiculesForImmatriculationAndFournis(ec, immatriculation.getText(), currentFournis);
//			if (vehiculesFournis.count() > 0)	{
//				MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Ce véhicule est déjà saisi pour ce fournisseur !");
//				return;
//			}
//		}
//		else	{
//
//			try {
//
//
//				if (vehiculesMission.count() > 0 &&
//						vehPuissance().equals((String)puissance.getSelectedItem()))	{				
//					MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Ce véhicule est déjà utilisé dans d'autres missions avec une puissance de " +
//							currentVehicule.vehPuissance() + " !");
//				}
//
//			}
//			catch (Exception e)	{
//
//			}
//		}


    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
