

// EOMissionParametres.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOMissionParametres extends _EOMissionParametres {

	
	public static final String CODE_SANS_CAP = "0";
	public static final String CODE_CAP_CLASSIQUE = "1";
	public static final String CODE_CAP_EXTOURNE = "2";
	
	public static final String ID_TELECHARGEMENT = "FREQUENCE_TELECHARGEMENT";
	public static final String DEFAULT_VALUE_TELECHARGEMENT = "1";

	public static final String ID_TELECHARGEMENT_AUTO = "TELECHARGEMENT_AUTO";
	public static final String DEFAULT_VALUE_TELECHARGEMENT_AUTO = "N";

	public static final String ID_CLOTURE_AUTO = "CLOTURE_AUTO";
	public static final String DEFAULT_VALUE_CLOTURE_AUTO = "N";

	public static final String ID_CHECK_PRORATA = "5/3_AUTORISE";
	public static final String DEFAULT_VALUE_CHECK_PRORATA = "N";

	public static final String ID_CHECK_FORFAIT = "CONTROLE_FORFAIT";
	public static final String DEFAULT_VALUE_CHECK_FORFAIT = "N";

	// Interdiction de saisir une mission après la date de début de la mission
	public static final String ID_CHECK_SAISIE_MISSION_DATES = "BLOQUAGE_AVANCES";
	public static final String DEFAULT_VALUE_CHECK_SAISIE_MISSION_DATES = "N";

	public static final String ID_CHECK_AVANCES = "BLOQUAGE_AVANCES";
	public static final String DEFAULT_VALUE_CHECK_AVANCES = "N";

	public static final String ID_CHECK_PRORATA_NUITS = "PRORATA_NUITS";
	public static final String DEFAULT_VALUE_CHECK_PRORATA_NUITS = "O";

	public static final String ID_CHECK_MULTI_LBUD = "MULTI_LBUD";
	public static final String DEFAULT_VALUE_CHECK_MULTI_LBUD = "N";

	public static final String ID_CHECK_NB_NUITS = "MODIF_NUITS";
	public static final String DEFAULT_VALUE_CHECK_NB_NUITS = "N";

	public static final String ID_SAISIE_MOTIFS = "AJOUT_MOTIFS";
	public static final String DEFAULT_VALUE_SAISIE_MOTIFS = "O";

	public static final String ID_UPDATE_MONTANT_INDEMNITES = "UPDATE_MONTANT_INDEMNITES";
	public static final String DEFAULT_VALUE_UPDATE_MONTANT_INDEMNITES = "O";

	public static final String ID_CHECK_NB_REPAS = "MODIF_REPAS";
	public static final String DEFAULT_VALUE_CHECK_NB_REPAS = "N";

	public static final String ID_CHECK_MONTANT_REPAS = "MODIF_MONTANT_REPAS";
	public static final String DEFAULT_VALUE_CHECK_MONTANT_REPAS = "N";

	public static final String ID_CHECK_PLAFOND_NUITS = "VERIF_SEUIL_NUITS";
	public static final String DEFAULT_VALUE_CHECK_PLAFOND_NUITS = "O";

	public static final String ID_CHECK_PLAFOND_REPAS = "VERIF_SEUIL_REPAS";
	public static final String DEFAULT_VALUE_CHECK_PLAFOND_REPAS = "O";

	public static final String ID_NUMERATEUR_REMB = "NUMERATEUR_REMB";
	public static final String DEFAULT_VALUE_NUMERATEUR_REMB = "5";

	public static final String ID_DENOMINATEUR_REMB = "DENOMINATEUR_REMB";
	public static final String DEFAULT_VALUE_DENOMINATEUR_REMB = "3";

	public static final String ID_ALL_CONVENTIONS = "ALL_CONVENTIONS";
	public static final String DEFAULT_VALUE_ALL_CONVENTIONS = "N";

	public static final String ID_CHECK_RESTRICTION_LBUDS = "RESTRICTION_LBUDS";
	public static final String DEFAULT_VALUE_CHECK_RESTRICTION_LBUDS = "N";

	public static final String ID_MODE_AVANCE = "MODE_CODE_AVANCE";
	public static final String DEFAULT_VALUE_MODE_AVANCE = "30";

	public static final String ID_MODE_CAP = "MODE_CODE_CAP";
	public static final String DEFAULT_VALUE_MODE_CAP = "60";

	public static final String ID_GESTION_AVANCES_AUTO = "GESTION_AVANCES_AUTO";
	public static final String DEFAULT_VALUE_GESTION_AVANCES_AUTO = "O";

	public static final String ID_TRAITEMENTS_AVANCES = "TRAITEMENT_AVANCES";
	public static final String DEFAULT_VALUE_TRAITEMENT_AVANCES = "DEP";

	public static final String ID_MODE_DPAO = "MODE_CODE_DPAO";
	public static final String DEFAULT_VALUE_MODE_DPAO= "92";

	public static final String ID_ORIGINE_SIGNATAIRES = "ORIGINE_SIGNATAIRES";
	public static final String DEFAULT_VALUE_ORIGINE_SIGNATAIRES = "JEFY_ADMIN";

	public static final String ID_NB_SIGNATAIRES_OM = "NB_SIGNATAIRES_OM";
	public static final String DEFAULT_VALUE_NB_SIGNATAIRES_OM= "1";

	public static final String ID_TYPE_SIGNATAIRES_OM = "TYPE_SIGNATAIRES_OM";
	public static final String DEFAULT_VALUE_TYPE_SIGNATAIRES_OM= "FINANCIER";

	public static final String ID_NB_SIGNATAIRES_ETAT_FRAIS = "NB_SIGNATAIRES_ETAT_FRAIS";
	public static final String DEFAULT_VALUE_NB_SIGNATAIRES_ETAT_FRAIS= "1";

	public static final String ID_ORDONNATEUR = "ORDONNATEUR";
	public static final String DEFAULT_VALUE_ORDONNATEUR= "UB";

	public static final String ID_COLOR_MIS_ANNULEES = "COLOR_MIS_ANNULEES";
	public static final String DEFAULT_VALUE_COLOR_MIS_ANNULEES = "255;0;0";

	public static final String ID_COLOR_MIS_VALIDES = "COLOR_MIS_ANNULEES";
	public static final String DEFAULT_VALUE_COLOR_MIS_VALIDES = "0;255;0";

	public static final String ID_DEFAULT_CORPS = "DEFAULT_CORPS";
	public static final String DEFAULT_VALUE_DEFAULT_CORPS = "999";

	
	public static final String ID_GESTION_CAP = "GESTION_CAP";
	public static final String DEFAULT_VALUE_GESTION_CAP = "0";

	
	public EOMissionParametres() {
        super();
    }


	public static Integer getNumerateurRemb(EOEditingContext ec, EOExercice exercice)	{
		
		String paramNumerateur = FinderMissionParametres.getValue(ec, exercice, ID_NUMERATEUR_REMB);
		
		Integer numerateur = new Integer(DEFAULT_VALUE_NUMERATEUR_REMB);
		
		if (paramNumerateur != null)
			numerateur = new Integer(paramNumerateur);
		
		return numerateur;
	}
	
	public static Integer getDenominateurRemb(EOEditingContext ec, EOExercice exercice)	{
		
		String paramDenominateur = FinderMissionParametres.getValue(ec, exercice, ID_DENOMINATEUR_REMB);

		Integer denominateur = new Integer(DEFAULT_VALUE_DENOMINATEUR_REMB);
		
		if (paramDenominateur != null)
			denominateur = new Integer(paramDenominateur);
		
		return denominateur;
	}

	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
