// _EODepense.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepense.java instead.
package org.cocktail.kiwi.client.metier.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODepense extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Depense";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "depId";

	public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
	public static final String DEP_ID_KEY = "depId";
	public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
	public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";
	public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";
	public static final String DPP_ID_KEY = "dppId";
	public static final String TAP_ID_KEY = "tapId";

//Colonnes dans la base de donnees
	public static final String DEP_HT_SAISIE_COLKEY = "DEP_HT_SAISIE";
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DEP_MONTANT_BUDGETAIRE_COLKEY = "DEP_MONTANT_BUDGETAIRE";
	public static final String DEP_TTC_SAISIE_COLKEY = "DEP_TTC_SAISIE";
	public static final String DEP_TVA_SAISIE_COLKEY = "DEP_TVA_SAISIE";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";

// Relationships
	public static final String DEPENSE_CTRL_PLANCO_KEY = "depenseCtrlPlanco";
	public static final String DEPENSE_PAPIER_KEY = "depensePapier";
	public static final String ENGAGE_KEY = "engage";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public java.math.BigDecimal depHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_HT_SAISIE_KEY);
  }

  public void setDepHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_HT_SAISIE_KEY);
  }

  public Integer depId() {
    return (Integer) storedValueForKey(DEP_ID_KEY);
  }

  public void setDepId(Integer value) {
    takeStoredValueForKey(value, DEP_ID_KEY);
  }

  public java.math.BigDecimal depMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDepMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal depTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TTC_SAISIE_KEY);
  }

  public void setDepTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal depTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TVA_SAISIE_KEY);
  }

  public void setDepTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TVA_SAISIE_KEY);
  }

  public Integer dppId() {
    return (Integer) storedValueForKey(DPP_ID_KEY);
  }

  public void setDppId(Integer value) {
    takeStoredValueForKey(value, DPP_ID_KEY);
  }

  public Integer tapId() {
    return (Integer) storedValueForKey(TAP_ID_KEY);
  }

  public void setTapId(Integer value) {
    takeStoredValueForKey(value, TAP_ID_KEY);
  }

  public org.cocktail.kiwi.client.metier.budget.EODepensePapier depensePapier() {
    return (org.cocktail.kiwi.client.metier.budget.EODepensePapier)storedValueForKey(DEPENSE_PAPIER_KEY);
  }

  public void setDepensePapierRelationship(org.cocktail.kiwi.client.metier.budget.EODepensePapier value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EODepensePapier oldValue = depensePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.budget.EOEngage engage() {
    return (org.cocktail.kiwi.client.metier.budget.EOEngage)storedValueForKey(ENGAGE_KEY);
  }

  public void setEngageRelationship(org.cocktail.kiwi.client.metier.budget.EOEngage value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOEngage oldValue = engage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOExercice toExercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.client.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.client.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray depenseCtrlPlanco() {
    return (NSArray)storedValueForKey(DEPENSE_CTRL_PLANCO_KEY);
  }

  public NSArray depenseCtrlPlanco(EOQualifier qualifier) {
    return depenseCtrlPlanco(qualifier, null, false);
  }

  public NSArray depenseCtrlPlanco(EOQualifier qualifier, boolean fetch) {
    return depenseCtrlPlanco(qualifier, null, fetch);
  }

  public NSArray depenseCtrlPlanco(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco.DEPENSE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseCtrlPlanco();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseCtrlPlancoRelationship(org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CTRL_PLANCO_KEY);
  }

  public void removeFromDepenseCtrlPlancoRelationship(org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CTRL_PLANCO_KEY);
  }

  public org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco createDepenseCtrlPlancoRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DepenseCtrlPlanco");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CTRL_PLANCO_KEY);
    return (org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco) eo;
  }

  public void deleteDepenseCtrlPlancoRelationship(org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CTRL_PLANCO_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseCtrlPlancoRelationships() {
    Enumeration objects = depenseCtrlPlanco().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseCtrlPlancoRelationship((org.cocktail.kiwi.client.metier.budget.EODepenseCtrlPlanco)objects.nextElement());
    }
  }


  public static EODepense createDepense(EOEditingContext editingContext, java.math.BigDecimal depHtSaisie
, Integer depId
, java.math.BigDecimal depMontantBudgetaire
, java.math.BigDecimal depTtcSaisie
, java.math.BigDecimal depTvaSaisie
, Integer dppId
, Integer tapId
) {
    EODepense eo = (EODepense) createAndInsertInstance(editingContext, _EODepense.ENTITY_NAME);    
		eo.setDepHtSaisie(depHtSaisie);
		eo.setDepId(depId);
		eo.setDepMontantBudgetaire(depMontantBudgetaire);
		eo.setDepTtcSaisie(depTtcSaisie);
		eo.setDepTvaSaisie(depTvaSaisie);
		eo.setDppId(dppId);
		eo.setTapId(tapId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EODepense.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EODepense.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EODepense localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepense)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EODepense localInstanceIn(EOEditingContext editingContext, EODepense eo) {
    EODepense localInstance = (eo == null) ? null : (EODepense)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EODepense#localInstanceIn a la place.
   */
	public static EODepense localInstanceOf(EOEditingContext editingContext, EODepense eo) {
		return EODepense.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EODepense fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODepense fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepense)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepense)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EODepense fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepense eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepense ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepense fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
