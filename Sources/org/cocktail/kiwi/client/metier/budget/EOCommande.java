

// EOCommande.java
// 
package org.cocktail.kiwi.client.metier.budget;


import java.math.BigDecimal;

import org.cocktail.kiwi.client.finders.FinderCommandeEngagement;
import org.cocktail.kiwi.client.metier.EOArticle;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;

import com.webobjects.foundation.NSValidation;

public class EOCommande extends _EOCommande {

    public EOCommande() {
        super();
    }

    
    public String etatCommande(  )	{

    	EOEngage engagement = FinderCommandeEngagement.findEngageForCommande(editingContext(),  this);
    	return "C";
    	
    }
    
    
    /**
     * 
     * @return
     */
    public BigDecimal montantCommande() {
    
    	return CocktailUtilities.computeSumForKey(articles(), EOArticle.ART_PRIX_TOTAL_TTC_KEY);    	
    	
    }
    

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
