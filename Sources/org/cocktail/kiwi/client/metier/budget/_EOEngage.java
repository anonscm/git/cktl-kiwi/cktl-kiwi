// _EOEngage.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngage.java instead.
package org.cocktail.kiwi.client.metier.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngage extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Engage";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.ENGAGE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "engId";

	public static final String ENG_DATE_SAISIE_KEY = "engDateSaisie";
	public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
	public static final String ENG_MONTANT_BUDGETAIRE_KEY = "engMontantBudgetaire";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_KEY = "engMontantBudgetaireReste";
	public static final String ENG_NUMERO_KEY = "engNumero";
	public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
	public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TAP_ID_KEY = "tapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ENG_DATE_SAISIE_COLKEY = "ENG_DATE_SAISIE";
	public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
	public static final String ENG_MONTANT_BUDGETAIRE_COLKEY = "ENG_MONTANT_BUDGETAIRE";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ENG_MONTANT_BUDGETAIRE_RESTE";
	public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
	public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
	public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";

// Relationships
	public static final String ENGAGEMENTS_KEY = "engagements";
	public static final String ORGAN_KEY = "organ";
	public static final String TO_TYPE_APPLICATION_KEY = "toTypeApplication";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";



	// Accessors methods
  public NSTimestamp engDateSaisie() {
    return (NSTimestamp) storedValueForKey(ENG_DATE_SAISIE_KEY);
  }

  public void setEngDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ENG_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal engHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_HT_SAISIE_KEY);
  }

  public void setEngHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEngMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEngMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public Integer engNumero() {
    return (Integer) storedValueForKey(ENG_NUMERO_KEY);
  }

  public void setEngNumero(Integer value) {
    takeStoredValueForKey(value, ENG_NUMERO_KEY);
  }

  public java.math.BigDecimal engTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TTC_SAISIE_KEY);
  }

  public void setEngTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal engTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TVA_SAISIE_KEY);
  }

  public void setEngTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TVA_SAISIE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public Integer tapId() {
    return (Integer) storedValueForKey(TAP_ID_KEY);
  }

  public void setTapId(Integer value) {
    takeStoredValueForKey(value, TAP_ID_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.kiwi.client.metier.budget.EOOrgan organ() {
    return (org.cocktail.kiwi.client.metier.budget.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kiwi.client.metier.budget.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeApplication toTypeApplication() {
    return (org.cocktail.application.client.eof.EOTypeApplication)storedValueForKey(TO_TYPE_APPLICATION_KEY);
  }

  public void setToTypeApplicationRelationship(org.cocktail.application.client.eof.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeApplication oldValue = toTypeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeCredit toTypeCredit() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TO_TYPE_CREDIT_KEY);
  }

  public void setToTypeCreditRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CREDIT_KEY);
    }
  }
  
  public NSArray engagements() {
    return (NSArray)storedValueForKey(ENGAGEMENTS_KEY);
  }

  public NSArray engagements(EOQualifier qualifier) {
    return engagements(qualifier, null, false);
  }

  public NSArray engagements(EOQualifier qualifier, boolean fetch) {
    return engagements(qualifier, null, fetch);
  }

  public NSArray engagements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.client.metier.EOMissionPaiementEngage.ENGAGE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.client.metier.EOMissionPaiementEngage.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = engagements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEngagementsRelationship(org.cocktail.kiwi.client.metier.EOMissionPaiementEngage object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENTS_KEY);
  }

  public void removeFromEngagementsRelationship(org.cocktail.kiwi.client.metier.EOMissionPaiementEngage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENTS_KEY);
  }

  public org.cocktail.kiwi.client.metier.EOMissionPaiementEngage createEngagementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MissionPaiementEngage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENTS_KEY);
    return (org.cocktail.kiwi.client.metier.EOMissionPaiementEngage) eo;
  }

  public void deleteEngagementsRelationship(org.cocktail.kiwi.client.metier.EOMissionPaiementEngage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementsRelationships() {
    Enumeration objects = engagements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementsRelationship((org.cocktail.kiwi.client.metier.EOMissionPaiementEngage)objects.nextElement());
    }
  }


  public static EOEngage createEngage(EOEditingContext editingContext, NSTimestamp engDateSaisie
, java.math.BigDecimal engHtSaisie
, java.math.BigDecimal engMontantBudgetaire
, java.math.BigDecimal engMontantBudgetaireReste
, Integer engNumero
, java.math.BigDecimal engTtcSaisie
, java.math.BigDecimal engTvaSaisie
, Integer exeOrdre
, Integer fouOrdre
, Integer tapId
, Integer utlOrdre
, org.cocktail.kiwi.client.metier.budget.EOOrgan organ) {
    EOEngage eo = (EOEngage) createAndInsertInstance(editingContext, _EOEngage.ENTITY_NAME);    
		eo.setEngDateSaisie(engDateSaisie);
		eo.setEngHtSaisie(engHtSaisie);
		eo.setEngMontantBudgetaire(engMontantBudgetaire);
		eo.setEngMontantBudgetaireReste(engMontantBudgetaireReste);
		eo.setEngNumero(engNumero);
		eo.setEngTtcSaisie(engTtcSaisie);
		eo.setEngTvaSaisie(engTvaSaisie);
		eo.setExeOrdre(exeOrdre);
		eo.setFouOrdre(fouOrdre);
		eo.setTapId(tapId);
		eo.setUtlOrdre(utlOrdre);
    eo.setOrganRelationship(organ);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEngage.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEngage.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOEngage localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngage)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEngage localInstanceIn(EOEditingContext editingContext, EOEngage eo) {
    EOEngage localInstance = (eo == null) ? null : (EOEngage)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEngage#localInstanceIn a la place.
   */
	public static EOEngage localInstanceOf(EOEditingContext editingContext, EOEngage eo) {
		return EOEngage.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEngage fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEngage fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngage)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngage)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEngage fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngage eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngage ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngage fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
