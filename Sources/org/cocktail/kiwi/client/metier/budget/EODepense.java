

// EODepense.java
// 
package org.cocktail.kiwi.client.metier.budget;


import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EODepense extends _EODepense {

    public EODepense() {
        super();
    }

	public static NSArray findDepensesAvanceForPaiementEngage(EOEditingContext ec, EOMissionPaiementEngage paiement)	{

		try {
			NSMutableArray args = new NSMutableArray();
			args.addObject(paiement.engage());
			args.addObject(paiement.toExercice());
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(""+
					EODepense.DEPENSE_PAPIER_KEY + "." + EODepensePapier.DPP_NUMERO_FACTURE_KEY + " caseInsensitiveLike '*AVANCE*' " +
					" and " + EODepense.ENGAGE_KEY + " = %@ and " + EODepense.TO_EXERCICE_KEY + " = %@", args);
			EOFetchSpecification fs = new EOFetchSpecification(EODepense.ENTITY_NAME,myQualifier, null);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}
	
	public static NSArray findDepensesForPaiementEngage(EOEditingContext ec, EOMissionPaiementEngage paiement)	{

		try {
			
			NSMutableArray args = new NSMutableArray();
			args.addObject(paiement.engage());
			args.addObject(FinderExercice.findExercice(ec, paiement.engage().exeOrdre()));
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(""+
					EODepense.ENGAGE_KEY + " = %@ and " + EODepense.TO_EXERCICE_KEY + " = %@", args);
			EOFetchSpecification fs = new EOFetchSpecification(EODepense.ENTITY_NAME,myQualifier, null);
						
			return ec.objectsWithFetchSpecification(fs);
			
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}

	public static NSArray findDepensesForEngage(EOEditingContext ec, EOEngage engagement)	{

		try {
			
			NSMutableArray qualifiers = new NSMutableArray();
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODepense.ENGAGE_KEY + "=%@", new NSArray(engagement)));
			
			return fetchAll(ec, new EOAndQualifier(qualifiers), null);
						
		}
		catch (Exception e)	{
			return new NSArray();
		}
	}

    
    /**
     * 
     * @return
     */
    public Number numeroMandat()	{

    	if (depenseCtrlPlanco() != null && depenseCtrlPlanco().count() > 0)	{
    		
    		EODepenseCtrlPlanco dep = (EODepenseCtrlPlanco)depenseCtrlPlanco().objectAtIndex(0);
    		
    		if (dep.mandat() != null)
    			return dep.mandat().manNumero();    		    		
    	}
    	
		return null;
    }
    
    
    /**
     * 
     * @return
     */
    public String etatMandat()	{    	
    	
    	if (depenseCtrlPlanco() != null && depenseCtrlPlanco().count() > 0)	{
    		
    		EODepenseCtrlPlanco dep = (EODepenseCtrlPlanco)depenseCtrlPlanco().objectAtIndex(0);
    		
    		if (dep.mandat() != null)
    			return dep.mandat().manEtat();    		
    	}
		return null;
    }
    
    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
