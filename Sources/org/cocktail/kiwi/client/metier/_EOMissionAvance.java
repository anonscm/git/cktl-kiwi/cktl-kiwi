// _EOMissionAvance.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMissionAvance.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMissionAvance extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MissionAvance";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.Mission_avance";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "mavId";

	public static final String MAV_DATE_SAISIE_KEY = "mavDateSaisie";
	public static final String MAV_MONTANT_KEY = "mavMontant";

//Colonnes dans la base de donnees
	public static final String MAV_DATE_SAISIE_COLKEY = "MAV_DATE_SAISIE";
	public static final String MAV_MONTANT_COLKEY = "MAV_MONTANT";

// Relationships
	public static final String DEPENSE_KEY = "depense";
	public static final String MISSION_KEY = "mission";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String VISA_AV_MISSION_KEY = "visaAvMission";



	// Accessors methods
  public NSTimestamp mavDateSaisie() {
    return (NSTimestamp) storedValueForKey(MAV_DATE_SAISIE_KEY);
  }

  public void setMavDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, MAV_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal mavMontant() {
    return (java.math.BigDecimal) storedValueForKey(MAV_MONTANT_KEY);
  }

  public void setMavMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAV_MONTANT_KEY);
  }

  public org.cocktail.kiwi.client.metier.budget.EODepense depense() {
    return (org.cocktail.kiwi.client.metier.budget.EODepense)storedValueForKey(DEPENSE_KEY);
  }

  public void setDepenseRelationship(org.cocktail.kiwi.client.metier.budget.EODepense value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EODepense oldValue = depense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOMission mission() {
    return (org.cocktail.kiwi.client.metier.EOMission)storedValueForKey(MISSION_KEY);
  }

  public void setMissionRelationship(org.cocktail.kiwi.client.metier.EOMission value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOMission oldValue = mission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MISSION_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOModePaiement modePaiement() {
    return (org.cocktail.kiwi.client.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.kiwi.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.kiwi.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.kiwi.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOVisaAvMission visaAvMission() {
    return (org.cocktail.kiwi.client.metier.EOVisaAvMission)storedValueForKey(VISA_AV_MISSION_KEY);
  }

  public void setVisaAvMissionRelationship(org.cocktail.kiwi.client.metier.EOVisaAvMission value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOVisaAvMission oldValue = visaAvMission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, VISA_AV_MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, VISA_AV_MISSION_KEY);
    }
  }
  

  public static EOMissionAvance createMissionAvance(EOEditingContext editingContext, NSTimestamp mavDateSaisie
, java.math.BigDecimal mavMontant
) {
    EOMissionAvance eo = (EOMissionAvance) createAndInsertInstance(editingContext, _EOMissionAvance.ENTITY_NAME);    
		eo.setMavDateSaisie(mavDateSaisie);
		eo.setMavMontant(mavMontant);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOMissionAvance.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOMissionAvance.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOMissionAvance localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMissionAvance)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOMissionAvance localInstanceIn(EOEditingContext editingContext, EOMissionAvance eo) {
    EOMissionAvance localInstance = (eo == null) ? null : (EOMissionAvance)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOMissionAvance#localInstanceIn a la place.
   */
	public static EOMissionAvance localInstanceOf(EOEditingContext editingContext, EOMissionAvance eo) {
		return EOMissionAvance.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOMissionAvance fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOMissionAvance fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMissionAvance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMissionAvance)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMissionAvance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMissionAvance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMissionAvance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMissionAvance)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOMissionAvance fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMissionAvance eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMissionAvance ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMissionAvance fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
