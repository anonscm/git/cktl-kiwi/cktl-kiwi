

// EORepas.java
// 
package org.cocktail.kiwi.client.metier;


import java.math.BigDecimal;
import java.util.TimeZone;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderPlageHoraire;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EORepas extends _EORepas {

	String heureDejeuner = "";
	String heureDiner = "";
	
    public EORepas() {
        super();
    }


    
	public static NSArray findForSegment(EOEditingContext ec, EOSegmentTarif segment)	{
		
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEGMENT_TARIF_KEY + " = %@", new NSArray(segment)));
			
			return fetchAll(ec, new EOAndQualifier(mesQualifiers),null, true);
			
	}

	public String stringTarifRepas()	{

		try {
			return tarifRepas().toString() + CocktailConstantes.STRING_EURO +  " / Repas";
		}
		catch (Exception e)	{
			return "";
		}
	}
	

	public BigDecimal tarifRepas()	{
		
		try {		
			EOMission mission = segmentTarif().mission();
			BigDecimal tarif = rembJournalier().remTarif();
			
			tarif = tarif.multiply(new BigDecimal(mission.numQuotientRemb().intValue()));
			tarif = tarif.divide(new BigDecimal(mission.denQuotientRemb().intValue()), 2, BigDecimal.ROUND_HALF_UP);
			
			return tarif;
		}
		catch (Exception e)	{
			return null;
		}		
	}


    public String stringMontantPaiement()	{
    	return repMontantPaiement()+ CocktailConstantes.STRING_EURO;
    }
    
		
	/**
	 * 
	 * @param segmentDebut
	 * @param segmentFin
	 */
	public void calculerRepas(EOEditingContext ec, NSTimestamp segmentDebut, NSTimestamp segmentFin)	{
		
		NSTimestamp debutcalcul = new NSTimestamp();
		NSTimestamp fincalcul = new NSTimestamp();
		int nbRepas = 0;
		NSTimestamp tempDateDebut;
		NSTimestamp tempDateFin;

		EOPlageHoraire plageDejeuner = FinderPlageHoraire.findPlageHoraire(ec, new Integer(EOPlageHoraire.CLE_REPAS_MIDI));
		EOPlageHoraire plageDiner = FinderPlageHoraire.findPlageHoraire(ec, new Integer(EOPlageHoraire.CLE_REPAS_SOIR));

		String heureDebutDejeuner = plageDejeuner.plaDebut();
		String heureFinDejeuner = plageDejeuner.plaFin();
		
		String heureDebutDiner = plageDiner.plaDebut();
		String heureFinDiner = plageDiner.plaFin();
		
		debutcalcul = segmentDebut;
		fincalcul = segmentFin;

		// calcul 1er jour -  si 11:00 > debutCalcul && 14:00 < fincalcul
		tempDateDebut = new NSTimestamp(debutcalcul.yearOfCommonEra(), debutcalcul.monthOfYear(), debutcalcul.dayOfMonth(), 
				new Integer(heureDebutDejeuner.substring(0,2)).intValue(), 
				new Integer(heureDebutDejeuner.substring(3,5)).intValue(), 
				0, TimeZone.getDefault());

		tempDateFin = new NSTimestamp(debutcalcul.yearOfCommonEra(), debutcalcul.monthOfYear(), debutcalcul.dayOfMonth(), 
				new Integer(heureFinDejeuner.substring(0,2)).intValue(), 
				new Integer(heureFinDejeuner.substring(3,5)).intValue(), 
				0, TimeZone.getDefault());
		
		if (DateCtrl.isBeforeEq(debutcalcul, tempDateDebut) && DateCtrl.isAfterEq(fincalcul, tempDateFin)) {
			nbRepas = nbRepas + 1;
		}
		
		// deuxieme test ajoute pour le 1er repas du soir.
		// finCalcul >= 21:00 && debutCalcul <= 18:00
		tempDateFin = new NSTimestamp(debutcalcul.yearOfCommonEra(), debutcalcul.monthOfYear(), debutcalcul.dayOfMonth(), 
				new Integer(heureFinDiner.substring(0,2)).intValue(), 
				new Integer(heureFinDiner.substring(3,5)).intValue(), 
				0, TimeZone.getDefault());
		NSTimestamp debSoir = new NSTimestamp(debutcalcul.yearOfCommonEra(), debutcalcul.monthOfYear(), debutcalcul.dayOfMonth(), 
				new Integer(heureDebutDiner.substring(0,2)).intValue(), 
				new Integer(heureDebutDiner.substring(3,5)).intValue(), 
				0, TimeZone.getDefault());
		
		if (DateCtrl.isAfterEq(fincalcul, tempDateFin) && DateCtrl.isBeforeEq(debutcalcul, debSoir)) {
			nbRepas = nbRepas + 1;
		}
		
		// Y a t il des jours entres les 2 dates ??
		NSTimestamp.IntRef gregorianDays = new NSTimestamp.IntRef();
		int nbJours = 0;
		tempDateDebut = new NSTimestamp(debutcalcul.yearOfCommonEra(), debutcalcul.monthOfYear(), debutcalcul.dayOfMonth(), 0, 0, 0, TimeZone.getDefault());
		tempDateFin = new NSTimestamp(fincalcul.yearOfCommonEra(), fincalcul.monthOfYear(), fincalcul.dayOfMonth(), 0, 0, 0, TimeZone.getDefault());
		
		tempDateFin.gregorianUnitsSinceTimestamp(null, null, gregorianDays, null, null, null, tempDateDebut);
		nbJours = gregorianDays.value;
		
		
		if (nbJours > 0) { // pas le meme jours et pas fin = debut + 1
			// calcul dernier jour si nbRepas > 1
			tempDateDebut = new NSTimestamp(fincalcul.yearOfCommonEra(), fincalcul.monthOfYear(), fincalcul.dayOfMonth(), 
					new Integer(heureFinDejeuner.substring(0,2)).intValue(), 
					new Integer(heureFinDejeuner.substring(3,5)).intValue(), 
					0, TimeZone.getDefault()); //11
			
			// fincalcul >=11:00 		nbRepas = nbRepas+1
			if (DateCtrl.isAfterEq(fincalcul, tempDateDebut)) {
				nbRepas = nbRepas + 1;
			}
			
			tempDateDebut = new NSTimestamp(fincalcul.yearOfCommonEra(), fincalcul.monthOfYear(), fincalcul.dayOfMonth(), 
					new Integer(heureFinDiner.substring(0,2)).intValue(), 
					new Integer(heureFinDiner.substring(3,5)).intValue(), 
					0, TimeZone.getDefault()); // 18 au lieu de 21

			if (DateCtrl.isAfterEq(fincalcul, tempDateDebut)) {
				nbRepas = nbRepas + 1;
			}
		}
		
		// calcul du nombre de jours entre les 2 dates : nbRepas =  nbRepas + nbJours*2 (dayOfYear)
		if (nbJours >= 2)
			nbRepas = nbRepas + ((nbJours - 1) * 2);

		if (nbRepas < 0)
			nbRepas = 0;

		setRepNbRepas(new BigDecimal(nbRepas));
	}
	
	
	/** Calcul du montant a rembourser suivant un nombre de repas donne */
	public void calculerMontant() {
		
		BigDecimal tarif = tarifRepas();
		
		if (rembJournalier() != null) {

			BigDecimal nbRepas = new BigDecimal(0);
			BigDecimal montantRepas = new BigDecimal(0);
			BigDecimal montantRepasAdministration = new BigDecimal(0);

			nbRepas = repNbRepas();
			nbRepas = nbRepas.subtract(repRepasGratuits());
			nbRepas = nbRepas.subtract(repRepasAdm());
			
			montantRepas = nbRepas.multiply(tarif);

			montantRepasAdministration = (repRepasAdm().multiply(tarif)).multiply(new BigDecimal(0.5));
			
			setRepMontantPaiement((montantRepas.add(montantRepasAdministration)).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP));
		}
	}

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
