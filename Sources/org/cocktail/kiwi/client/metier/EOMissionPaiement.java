

// EOMissionPaiement.java
// 
package org.cocktail.kiwi.client.metier;


import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.factory.Factory;
import org.cocktail.kiwi.client.finders.FinderRibfour;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOMissionPaiement extends _EOMissionPaiement {

	public final static String ETAT_VALIDE= "VALIDE";
	public final static String ETAT_ORDRE = "ORDRE";
	public final static String ETAT_SIGNATURE = "SIGNATURE";
	public final static String ETAT_LIQUIDE = "LIQUIDEE";
	public final static String ETAT_PAYE = "PAYEE";
	public final static String ETAT_EXTOURNE = "EXTOURNEE";
	public final static String ETAT_ANNULE= "ANNULE";

	public EOMissionPaiement() {
		super();
	}

	public boolean isChargeAPayer()	{
		return mipCap().equals("O");
	}

	public static EOMissionPaiement findPaiementForMission(EOEditingContext ec, EOMission mission)	{
		try {
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(MISSION_KEY + "=%@", new NSArray(mission));
			return fetchFirstByQualifier(ec, myQualifier);
		}
		catch (Exception e) {
			return null;
		}
	}

	public static NSArray<EOMissionPaiement> findPaiementsForMission(EOEditingContext ec, EOMission mission)	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(MISSION_KEY + "=%@", new NSArray(mission));
		return fetchAll(ec, myQualifier, null);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isValide()	{
		return mipEtat().equals(ETAT_VALIDE);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isSignature()	{
		return mipEtat().equals(ETAT_SIGNATURE);
	}

	public boolean isExtourne()	{		
		return (EOMissionPaiementDepense.findDepensesExtourneForMission(editingContext(), this)).size() > 0;
	}

	
	
	/**
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
		public static EOMissionPaiement creer (
				EOEditingContext ec,
				EOMission mission,
				EOExercice exercice
		)	{
			
			EOMissionPaiement missionPaiement = (EOMissionPaiement)Factory.instanceForEntity(ec, EOMissionPaiement.ENTITY_NAME);
				
			missionPaiement.addObjectToBothSidesOfRelationshipWithKey(mission, EOMissionPaiement.MISSION_KEY);

			missionPaiement.setMipCreation(new NSTimestamp());
			missionPaiement.setMipMontantPaiement(new BigDecimal(0));
			missionPaiement.setMipMontantTotal(new BigDecimal(0));
			missionPaiement.setMipEtat("VALIDE");
			missionPaiement.setMipCap("N");

			NSArray ribs = FinderRibfour.findRibsValidesForFournis(ec, mission.fournis());


			if (mission.isMissionPaiement() || mission.isSaisieLbud() )	{
				if (ribs.count() == 1)	{
					EORibfour rib = (EORibfour)ribs.objectAtIndex(0);
					missionPaiement.setRibfourRelationship(rib);
				}
			}
			ec.insertObject(missionPaiement);
			
			return missionPaiement;
		}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {


	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}
