

// EOIndemnite.java
// 
package org.cocktail.kiwi.client.metier;


import java.math.BigDecimal;
import java.util.GregorianCalendar;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOIndemnite extends _EOIndemnite {

	public final static BigDecimal TAUX_REPAS_GRATUIT = new BigDecimal(0.175);
	public final static BigDecimal TAUX_NUIT_GRATUITE = new BigDecimal(0.65);

	public EOIndemnite() {
		super();
	}

	public static NSArray findForSegment(EOEditingContext ec, EOSegmentTarif segment)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEGMENT_TARIF_KEY + " = %@", new NSArray(segment)));
		
		return fetchAll(ec, new EOAndQualifier(mesQualifiers),null, true);
		
}

	
	public boolean canUpdateMontant()	{

		if (segmentTarif() == null)
			return true;

		return DateCtrl.isAfterEq(segmentTarif().segFin(), DateCtrl.stringToDate("01/11/2006"));

	}

	/**
	 * 
	 *
	 */
	public String stringTarifIndemnite()	{
		try {
			return tarifIndemnite().toString() + " / Jour";
		}
		catch (Exception e)	{
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal tarifIndemnite()	{

		BigDecimal tarif = new BigDecimal("0");
		EOMission mission = segmentTarif().mission();
		BigDecimal taux = segmentTarif().webtaux().wtaTaux();

		if (mission.groupeMission() == null)
			tarif = webmiss().wmiGroupe1();
		else	{

			switch (new Integer(mission.groupeMission().grmCode()).intValue())	{
			case 1 : tarif = webmiss().wmiGroupe1();break;	
			case 2 : tarif = webmiss().wmiGroupe2();break;
			case 3 : tarif = webmiss().wmiGroupe3();break;
			default : tarif = webmiss().wmiGroupe1();break;
			}

		}

		tarif = tarif.multiply(taux);

		// On applique le quoitient de remboursement de la mission (1/1 ou 5/3)
		tarif = tarif.multiply(new BigDecimal(mission.numQuotientRemb().intValue()));
		tarif = tarif.divide(new BigDecimal(mission.denQuotientRemb().intValue()), 6, BigDecimal.ROUND_HALF_UP);

		return tarif;		
	}

	/**
	 * 
	 *
	 */
	public void calculerMontantDate(EOEditingContext ec, NSTimestamp segmentDebut, NSTimestamp segmentFin)	{

		BigDecimal nbJours = new BigDecimal(0);

		int nbNuits = 0;
		int nbRepas = 0;

		GregorianCalendar calDebutCalcul = new GregorianCalendar();
		GregorianCalendar calFinCalcul = new GregorianCalendar();
		calDebutCalcul.setTime(segmentDebut);
		calFinCalcul.setTime(segmentFin);

		int heureDepart = calDebutCalcul.get(GregorianCalendar.HOUR_OF_DAY);
		int minutesDepart = calDebutCalcul.get(GregorianCalendar.MINUTE);
		int heureArrivee = calFinCalcul.get(GregorianCalendar.HOUR_OF_DAY);

		// 1 seule journee : Si Depart < 5H ==> 0,65 Indemnites, sinon 0
		if (calDebutCalcul.get(GregorianCalendar.YEAR) == calFinCalcul.get(GregorianCalendar.YEAR) 
				&& calDebutCalcul.get(GregorianCalendar.DAY_OF_YEAR) == calFinCalcul.get(GregorianCalendar.DAY_OF_YEAR))	{

			if (heureDepart < 5)	{
				nbJours = TAUX_NUIT_GRATUITE;
				nbNuits = 1;
			}
			else	{
				nbRepas = 0;
				nbNuits = 0;
			}

			if (heureDepart < 11 && heureArrivee >= 14)	{
				nbJours = nbJours.add(TAUX_REPAS_GRATUIT);
				nbRepas++;				
			}

			if (heureArrivee >= 21)	{
				nbJours = nbJours.add(TAUX_REPAS_GRATUIT);
				nbRepas++;
			}
		}
		else	{	// Plusieurs jours indemnises

			nbJours = new BigDecimal(DateCtrl.nbJoursEntre(segmentDebut, segmentFin, true));
			nbNuits = nbJours.intValue();
			nbRepas = (nbJours.intValue()) * 2;
			
			if (heureDepart >= 5 && minutesDepart >= 0)	{
				nbJours = nbJours.subtract(TAUX_NUIT_GRATUITE);
				nbNuits--;
			}

			if (heureDepart >= 11)	{
				nbJours = nbJours.subtract(TAUX_REPAS_GRATUIT);	
				nbRepas--;
			}

			if (heureDepart >= 21)	{
				nbJours = nbJours.subtract(TAUX_REPAS_GRATUIT);	
				nbRepas--;
			}

			if (heureArrivee <= 5)	{
				nbJours = nbJours.subtract(TAUX_NUIT_GRATUITE);	
				nbNuits--;
			}

			if (heureArrivee < 14)	{
				nbJours = nbJours.subtract(TAUX_REPAS_GRATUIT);			
				nbJours = nbJours.subtract(TAUX_REPAS_GRATUIT);		
				nbRepas--;
				nbRepas--;
			}
			else	{
				if (heureArrivee < 21)	{
					nbJours = nbJours.subtract(TAUX_REPAS_GRATUIT);		
					nbRepas--;
				}
			}			
		}

		nbJours = nbJours.setScale(3, BigDecimal.ROUND_HALF_UP);

		setIndJours(nbJours);
		setIndNbNuits(new Integer(nbNuits));
		setIndNbRepas(new Integer(nbRepas));

		setIndJoursAuto(nbJours);
		setIndNuitsAuto(new Integer(nbNuits));
		setIndRepasAuto(new Integer(nbRepas));

	}	

	/**
	 * 
	 *
	 */
	public void calculerMontant()	{

		if (webmiss() == null)
			throw new NSValidation.ValidationException( "VOUS N AVEZ PAS DE ZONE ETRANGER !");
		else	{

			BigDecimal tarif = tarifIndemnite();

			BigDecimal joursGratuits = (new BigDecimal(indRepasGratuits()).multiply(TAUX_REPAS_GRATUIT))
			.add(new BigDecimal(indNuitsGratuites()).multiply(TAUX_NUIT_GRATUITE));

			setIndJoursGratuits(joursGratuits.setScale(3, BigDecimal.ROUND_HALF_UP));
			setIndJours( (indJoursAuto().subtract(indJoursGratuits())).setScale(3, BigDecimal.ROUND_HALF_UP));

			BigDecimal montantIndemnise = indJours().multiply(tarif);

			setIndMontantPaiement(montantIndemnise.setScale(ApplicationClient.USED_DECIMALES,BigDecimal.ROUND_HALF_DOWN));
		}
	}

}
