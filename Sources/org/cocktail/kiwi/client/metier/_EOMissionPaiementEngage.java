// _EOMissionPaiementEngage.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMissionPaiementEngage.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMissionPaiementEngage extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MissionPaiementEngage";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.MISSION_PAIEMENT_ENGAGE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "mpeOrdre";

	public static final String MPE_ETAT_KEY = "mpeEtat";
	public static final String MPE_INFOS_KEY = "mpeInfos";
	public static final String MPE_MONTANT_BUDGETAIRE_KEY = "mpeMontantBudgetaire";
	public static final String MPE_MONTANT_ENGAGEMENT_KEY = "mpeMontantEngagement";
	public static final String MPE_MONTANT_REMBOURSE_KEY = "mpeMontantRembourse";
	public static final String MPE_POURCENTAGE_KEY = "mpePourcentage";

//Colonnes dans la base de donnees
	public static final String MPE_ETAT_COLKEY = "MPE_ETAT";
	public static final String MPE_INFOS_COLKEY = "MPE_INFOS";
	public static final String MPE_MONTANT_BUDGETAIRE_COLKEY = "MPE_MONTANT_BUDGETAIRE";
	public static final String MPE_MONTANT_ENGAGEMENT_COLKEY = "MPE_MONTANT_ENGAGEMENT";
	public static final String MPE_MONTANT_REMBOURSE_COLKEY = "MPE_MONTANT_REMBOURSE";
	public static final String MPE_POURCENTAGE_COLKEY = "MPE_POURCENTAGE";

// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String CONVENTION_KEY = "convention";
	public static final String ENGAGE_KEY = "engage";
	public static final String MISSION_PAIEMENT_KEY = "missionPaiement";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TO_CODE_EXER_KEY = "toCodeExer";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";



	// Accessors methods
  public String mpeEtat() {
    return (String) storedValueForKey(MPE_ETAT_KEY);
  }

  public void setMpeEtat(String value) {
    takeStoredValueForKey(value, MPE_ETAT_KEY);
  }

  public String mpeInfos() {
    return (String) storedValueForKey(MPE_INFOS_KEY);
  }

  public void setMpeInfos(String value) {
    takeStoredValueForKey(value, MPE_INFOS_KEY);
  }

  public java.math.BigDecimal mpeMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(MPE_MONTANT_BUDGETAIRE_KEY);
  }

  public void setMpeMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MPE_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal mpeMontantEngagement() {
    return (java.math.BigDecimal) storedValueForKey(MPE_MONTANT_ENGAGEMENT_KEY);
  }

  public void setMpeMontantEngagement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MPE_MONTANT_ENGAGEMENT_KEY);
  }

  public java.math.BigDecimal mpeMontantRembourse() {
    return (java.math.BigDecimal) storedValueForKey(MPE_MONTANT_REMBOURSE_KEY);
  }

  public void setMpeMontantRembourse(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MPE_MONTANT_REMBOURSE_KEY);
  }

  public java.math.BigDecimal mpePourcentage() {
    return (java.math.BigDecimal) storedValueForKey(MPE_POURCENTAGE_KEY);
  }

  public void setMpePourcentage(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MPE_POURCENTAGE_KEY);
  }

  public org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.budget.EOConvention convention() {
    return (org.cocktail.kiwi.client.metier.budget.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.kiwi.client.metier.budget.EOConvention value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.budget.EOEngage engage() {
    return (org.cocktail.kiwi.client.metier.budget.EOEngage)storedValueForKey(ENGAGE_KEY);
  }

  public void setEngageRelationship(org.cocktail.kiwi.client.metier.budget.EOEngage value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOEngage oldValue = engage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOMissionPaiement missionPaiement() {
    return (org.cocktail.kiwi.client.metier.EOMissionPaiement)storedValueForKey(MISSION_PAIEMENT_KEY);
  }

  public void setMissionPaiementRelationship(org.cocktail.kiwi.client.metier.EOMissionPaiement value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOMissionPaiement oldValue = missionPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MISSION_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MISSION_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOModePaiement modePaiement() {
    return (org.cocktail.kiwi.client.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.kiwi.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.budget.EOOrgan organ() {
    return (org.cocktail.kiwi.client.metier.budget.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kiwi.client.metier.budget.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kiwi.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kiwi.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOCodeExer toCodeExer() {
    return (org.cocktail.application.client.eof.EOCodeExer)storedValueForKey(TO_CODE_EXER_KEY);
  }

  public void setToCodeExerRelationship(org.cocktail.application.client.eof.EOCodeExer value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOCodeExer oldValue = toCodeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CODE_EXER_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOExercice toExercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeCredit toTypeCredit() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TO_TYPE_CREDIT_KEY);
  }

  public void setToTypeCreditRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CREDIT_KEY);
    }
  }
  

  public static EOMissionPaiementEngage createMissionPaiementEngage(EOEditingContext editingContext, String mpeEtat
, java.math.BigDecimal mpeMontantBudgetaire
, java.math.BigDecimal mpeMontantEngagement
, java.math.BigDecimal mpeMontantRembourse
, java.math.BigDecimal mpePourcentage
, org.cocktail.application.client.eof.EOExercice toExercice) {
    EOMissionPaiementEngage eo = (EOMissionPaiementEngage) createAndInsertInstance(editingContext, _EOMissionPaiementEngage.ENTITY_NAME);    
		eo.setMpeEtat(mpeEtat);
		eo.setMpeMontantBudgetaire(mpeMontantBudgetaire);
		eo.setMpeMontantEngagement(mpeMontantEngagement);
		eo.setMpeMontantRembourse(mpeMontantRembourse);
		eo.setMpePourcentage(mpePourcentage);
    eo.setToExerciceRelationship(toExercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOMissionPaiementEngage.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOMissionPaiementEngage.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOMissionPaiementEngage localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMissionPaiementEngage)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOMissionPaiementEngage localInstanceIn(EOEditingContext editingContext, EOMissionPaiementEngage eo) {
    EOMissionPaiementEngage localInstance = (eo == null) ? null : (EOMissionPaiementEngage)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOMissionPaiementEngage#localInstanceIn a la place.
   */
	public static EOMissionPaiementEngage localInstanceOf(EOEditingContext editingContext, EOMissionPaiementEngage eo) {
		return EOMissionPaiementEngage.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOMissionPaiementEngage fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOMissionPaiementEngage fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMissionPaiementEngage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMissionPaiementEngage)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMissionPaiementEngage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMissionPaiementEngage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMissionPaiementEngage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMissionPaiementEngage)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOMissionPaiementEngage fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMissionPaiementEngage eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMissionPaiementEngage ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMissionPaiementEngage fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
