

// EOMissionPaiementEngage.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOMissionPaiementEngage extends _EOMissionPaiementEngage {

	public static final String ETAT_LIQUIDE = "LIQUIDEE";
	public final static String ETAT_VALIDE= "VALIDE";

    public EOMissionPaiementEngage() {
        super();
    }

    /**
     * 
     * @return
     */
    public String pourcentage() {
    	return mpePourcentage().floatValue()+ "";
    }    
    
    
    public boolean estLiquide() {
    	return mpeEtat().equals(ETAT_LIQUIDE); 
    }
    
    /**
     * Construction du libelle long de la convention
     * 
     * @return
     */
    public String getLongStringConvention()	{

    	String libelle = "";

    	if (convention() != null)	{
    		libelle = libelle+convention().convPremierExercice()+"-"+convention().convIndex();
    	
    		if (convention().convReferenceExterne() != null)
    			libelle = libelle +"-"+convention().convReferenceExterne();
    	}

    	return libelle;
    }
    
	public static NSArray<EOMissionPaiementEngage> findForMission(EOEditingContext ec, EOMission mission)	{

		try {
						
			NSMutableArray mySort = new NSMutableArray();
			mySort.addObject(new EOSortOrdering("mpeOrdre", EOSortOrdering.CompareDescending));

			NSMutableArray qualifiers = new NSMutableArray();
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					MISSION_PAIEMENT_KEY+"."+EOMissionPaiement.MISSION_KEY + " = %@", new NSArray(mission)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), mySort);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
