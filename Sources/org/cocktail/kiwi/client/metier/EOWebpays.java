

// EOWebpays.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.client.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOWebpays extends _EOWebpays {

    public EOWebpays() {
        super();
    }

    /**
     * 
     * @param edc
     * @param zone
     * @return
     */
	public static EOWebpays creer(EOEditingContext edc, EORembZone zone, EOWebmon monnaie) {
		
		EOWebpays newObject = (EOWebpays)Factory.instanceForEntity(edc, EOWebpays.ENTITY_NAME);

		newObject.setToZoneRelationship(zone);
		newObject.setToMonnaieRelationship(monnaie);
		newObject.setWpaCode(zone.remTaux()+"-EUR");
		newObject.setWpaLibelle("FRANCE - " + zone.remLibelle());
		edc.insertObject(newObject);
		return newObject;
		
	}

    public String libelleCourt()	{
    	
    	try {
    	    	return (String)NSArray.componentsSeparatedByString(wpaLibelle(), "     ").objectAtIndex(0);
    	}
    	catch (Exception e)	{
    		return "PAYS INCONU";
    	}
    }
    

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
