

// EOModePaiement.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOModePaiement extends _EOModePaiement {

	public static final EOSortOrdering SORT_CODE_ASC = new EOSortOrdering(MOD_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_CODE_ASC = new NSArray(SORT_CODE_ASC);
	
	public static final String CODE_MODE_VALIDE = "VALIDE";
	
	public static final String CODE_DOMAINE_INTERNE = "INTERNE";
	public static final String CODE_DOMAINE_VIREMENT = "VIREMENT";
	public static final String CODE_DOMAINE_EXTOURNE = "A EXTOURNER";
	
    public EOModePaiement() {
        super();
    }

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static EOModePaiement findModePaiementForCode(EOEditingContext ec, String code, EOExercice exercice)	{

		try {
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(EOModePaiement.CODE_MODE_VALIDE)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_CODE_KEY + " = %@", new NSArray(code)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

			EOFetchSpecification fs = new EOFetchSpecification(EOModePaiement.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);

			return (EOModePaiement)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			return null;
		}
	}

	
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOModePaiement> findModesPaiements(EOEditingContext ec, EOExercice exercice)	{
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(EOModePaiement.CODE_MODE_VALIDE)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers),SORT_ARRAY_CODE_ASC);
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOModePaiement> findModesPaiementAvances(EOEditingContext ec, EOExercice exercice)	{
				
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(EOModePaiement.CODE_MODE_VALIDE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.PCO_NUM_PAIEMENT_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers),SORT_ARRAY_CODE_ASC);
	}
	
	
	/**
	 * Recherche des modes de paiement possibles pour les charges a payer.
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOModePaiement> findModesPaiementCapClassique(EOEditingContext ec, EOExercice exercice)	{
				
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(EOModePaiement.CODE_MODE_VALIDE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.PCO_NUM_VISA_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_DOM_KEY + " = %@", new NSArray(EOModePaiement.CODE_DOMAINE_INTERNE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers),SORT_ARRAY_CODE_ASC);
	}

	
	/**
	 * Recherche des modes de paiement possibles pour les charges a payer.
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOModePaiement> findModesPaiementCapExtourne(EOEditingContext ec, EOExercice exercice)	{
				
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(EOModePaiement.CODE_MODE_VALIDE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.PCO_NUM_VISA_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_DOM_KEY + " = %@", new NSArray(EOModePaiement.CODE_DOMAINE_EXTOURNE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers),SORT_ARRAY_CODE_ASC);
	}
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findModesPaiementRegulOP(EOEditingContext ec, EOExercice exercice)	{
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(EOModePaiement.CODE_MODE_VALIDE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.PCO_NUM_VISA_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_DOM_KEY + " = %@", new NSArray(EOModePaiement.CODE_DOMAINE_INTERNE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers),SORT_ARRAY_CODE_ASC);
	}
    
    public String toString() {
    	return modCode() + " - " + modLibelle();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
