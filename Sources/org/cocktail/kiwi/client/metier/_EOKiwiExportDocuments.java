// _EOKiwiExportDocuments.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOKiwiExportDocuments.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOKiwiExportDocuments extends  EOGenericRecord {
	public static final String ENTITY_NAME = "KiwiExportDocuments";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.KIWI_EXPORT_DOCUMENTS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "kedId";

	public static final String KED_CODE_KEY = "kedCode";
	public static final String KED_COMMENTAIRE_KEY = "kedCommentaire";
	public static final String KED_SQL_KEY = "kedSql";
	public static final String KED_TEMPLATE_KEY = "kedTemplate";
	public static final String KED_TYPE_KEY = "kedType";

//Colonnes dans la base de donnees
	public static final String KED_CODE_COLKEY = "KED_CODE";
	public static final String KED_COMMENTAIRE_COLKEY = "KED_COMMENTAIRE";
	public static final String KED_SQL_COLKEY = "KED_SQL";
	public static final String KED_TEMPLATE_COLKEY = "KED_TEMPLATE";
	public static final String KED_TYPE_COLKEY = "KED_TYPE";

// Relationships



	// Accessors methods
  public String kedCode() {
    return (String) storedValueForKey(KED_CODE_KEY);
  }

  public void setKedCode(String value) {
    takeStoredValueForKey(value, KED_CODE_KEY);
  }

  public String kedCommentaire() {
    return (String) storedValueForKey(KED_COMMENTAIRE_KEY);
  }

  public void setKedCommentaire(String value) {
    takeStoredValueForKey(value, KED_COMMENTAIRE_KEY);
  }

  public String kedSql() {
    return (String) storedValueForKey(KED_SQL_KEY);
  }

  public void setKedSql(String value) {
    takeStoredValueForKey(value, KED_SQL_KEY);
  }

  public String kedTemplate() {
    return (String) storedValueForKey(KED_TEMPLATE_KEY);
  }

  public void setKedTemplate(String value) {
    takeStoredValueForKey(value, KED_TEMPLATE_KEY);
  }

  public String kedType() {
    return (String) storedValueForKey(KED_TYPE_KEY);
  }

  public void setKedType(String value) {
    takeStoredValueForKey(value, KED_TYPE_KEY);
  }


  public static EOKiwiExportDocuments createKiwiExportDocuments(EOEditingContext editingContext) {
    EOKiwiExportDocuments eo = (EOKiwiExportDocuments) createAndInsertInstance(editingContext, _EOKiwiExportDocuments.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOKiwiExportDocuments.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOKiwiExportDocuments.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOKiwiExportDocuments localInstanceIn(EOEditingContext editingContext) {
	  		return (EOKiwiExportDocuments)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOKiwiExportDocuments localInstanceIn(EOEditingContext editingContext, EOKiwiExportDocuments eo) {
    EOKiwiExportDocuments localInstance = (eo == null) ? null : (EOKiwiExportDocuments)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOKiwiExportDocuments#localInstanceIn a la place.
   */
	public static EOKiwiExportDocuments localInstanceOf(EOEditingContext editingContext, EOKiwiExportDocuments eo) {
		return EOKiwiExportDocuments.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOKiwiExportDocuments fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOKiwiExportDocuments fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOKiwiExportDocuments eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOKiwiExportDocuments)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOKiwiExportDocuments fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOKiwiExportDocuments fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOKiwiExportDocuments eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOKiwiExportDocuments)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOKiwiExportDocuments fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOKiwiExportDocuments eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOKiwiExportDocuments ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOKiwiExportDocuments fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
