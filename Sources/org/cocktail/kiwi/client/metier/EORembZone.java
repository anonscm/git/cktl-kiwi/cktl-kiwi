

// EORembZone.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.client.factory.Factory;
import org.cocktail.kiwi.client.finders.FinderRembZone;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EORembZone extends _EORembZone {

	public static EOSortOrdering SORT_VALIDE_DESC = new EOSortOrdering(TEM_VALIDE_KEY, EOSortOrdering.CompareDescending);
	public static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(REM_LIBELLE_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
    public EORembZone() {
        super();
    }

    /**
     * 
     * @param ec
     * @return
     */
	public static NSArray find(EOEditingContext edc)	{
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(SORT_VALIDE_DESC);
		mySort.addObject(SORT_LIBELLE_ASC);
		
		return fetchAll(edc, null, mySort);

	}

    /**
     * 
     * @return
     */
    public boolean estEtranger() {
    	return remEtranger() != null && remEtranger().equals("0") == false;
    }
    public void setEstEtranger(boolean value) {
    	if (value) 
    		setRemEtranger("2");
    	else
    		setRemEtranger("0");
    }
    
    public boolean estValide() {
    	return temValide() != null && temValide().equals("O");
    }
    public void setEstValide(boolean value) {
    	if (value) 
    		setTemValide("O");
    	else
    		setTemValide("N");
    }

    
	public static EORembZone creer(EOEditingContext edc) {
		
		EORembZone newObject = (EORembZone)Factory.instanceForEntity(edc, EORembZone.ENTITY_NAME);
								
		newObject.setEstValide(true);
		edc.insertObject(newObject);
		return newObject;
		
	}


	public String toString()	{
		return remLibelle();
	}
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	if (remTaux() == null)
    		throw new ValidationException("Le code est obligatoire !");
    	if (remLibelle() == null)
    		throw new ValidationException("Le libellé est obligatoire !");
		 

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
