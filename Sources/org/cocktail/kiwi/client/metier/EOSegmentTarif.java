

// EOSegmentTarif.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOSegmentTarif extends _EOSegmentTarif {

	public static final EOSortOrdering SORT_FIN_DESC = new EOSortOrdering(SEG_FIN_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_FIN_DESC = new NSArray(SORT_FIN_DESC);
	public static final EOSortOrdering SORT_FIN_ASC = new EOSortOrdering(SEG_FIN_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_FIN_ASC = new NSArray(SORT_FIN_ASC);
	public static final EOSortOrdering SORT_DEBUT_DESC = new EOSortOrdering(SEG_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_DEBUT_DESC);
	public static final EOSortOrdering SORT_DEBUT_ASC = new EOSortOrdering(SEG_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_DEBUT_ASC = new NSArray(SORT_DEBUT_ASC);
	
    public EOSegmentTarif() {
        super();
    }

	/**
	 * Recuperation des segments tarifs associes a une mission
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static NSArray<EOSegmentTarif> findSegmentsTarifsForMission(EOEditingContext ec, EOMission mission)	{		
		try {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegmentTarif.MISSION_KEY + " = %@", new NSArray(mission));
		return fetchAll(ec, myQualifier, SORT_ARRAY_DEBUT_ASC);
		}
		catch (Exception e) {
			return new NSArray<EOSegmentTarif>();
		}
	}
	
	/**
	 * Recuperation du dernier trajet associe a une mission
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static EOSegmentTarif findLastSegmentForMission(EOEditingContext ec, EOMission mission)	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegmentTarif.MISSION_KEY + " = %@", new NSArray(mission));		
		return fetchFirstByQualifier(ec, myQualifier, SORT_ARRAY_FIN_DESC);
	}
	
	/**
	 * Recuperation du premier trajet associe a une mission
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static EOSegmentTarif findFirstSegmentForMission(EOEditingContext ec, EOMission mission)	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegmentTarif.MISSION_KEY + " = %@", new NSArray(mission));		
		return fetchFirstByQualifier(ec, myQualifier, SORT_ARRAY_FIN_ASC);
	}

	public static NSArray findForVehicule(EOEditingContext ec, EOVehicule vehicule)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSegmentTarif.TRANSPORTS_KEY+"."+EOTransports.VEHICULE_KEY+ " = %@", new NSArray(vehicule)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSegmentTarif.MISSION_KEY+"."+EOMission.MIS_ETAT_KEY+ " != %@", new NSArray(EOMission.ETAT_ANNULE)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);

	}

    
	public String zonePays()	{
		if (zoneEtranger())	{
		
			String libellePays = (String)NSArray.componentsSeparatedByString(webpays().wpaLibelle(), "     ").objectAtIndex(0);
			return rembZone().remLibelle() + " - " + libellePays;
		}
		
		return rembZone().remLibelle();
	}
	
	// La zone correspondant au segment est elle a l'etranger
	public boolean zoneEtranger()	{
		return ( rembZone().remEtranger().equals("1") );
	}

	public boolean zoneParisProvince()	{
		return ( rembZone().remEtranger().equals("0") );
	}

	public boolean zoneDomTom()	{
		return ( rembZone().remEtranger().equals("2") );
	}

    public String stringSegDebut()	{
    	return DateCtrl.dateToString(segDebut(), "%d/%m/%Y %H:%M");
    }

    public String stringSegFin()	{
    	return DateCtrl.dateToString(segFin(), "%d/%m/%Y %H:%M");
    }

    
    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
