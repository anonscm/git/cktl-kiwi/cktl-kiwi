// _EORepas.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepas.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORepas extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Repas";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.REPAS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "repOrdre";

	public static final String REP_ETAT_KEY = "repEtat";
	public static final String REP_MONTANT_PAIEMENT_KEY = "repMontantPaiement";
	public static final String REP_NB_REPAS_KEY = "repNbRepas";
	public static final String REP_REPAS_ADM_KEY = "repRepasAdm";
	public static final String REP_REPAS_GRATUITS_KEY = "repRepasGratuits";

//Colonnes dans la base de donnees
	public static final String REP_ETAT_COLKEY = "REP_ETAT";
	public static final String REP_MONTANT_PAIEMENT_COLKEY = "REP_MONTANT_PAIEMENT";
	public static final String REP_NB_REPAS_COLKEY = "REP_NB_REPAS";
	public static final String REP_REPAS_ADM_COLKEY = "REP_REPAS_ADM";
	public static final String REP_REPAS_GRATUITS_COLKEY = "REP_REPAS_GRATUITS";

// Relationships
	public static final String REMB_JOURNALIER_KEY = "rembJournalier";
	public static final String SEGMENT_TARIF_KEY = "segmentTarif";
	public static final String WEBMISS_KEY = "webmiss";



	// Accessors methods
  public String repEtat() {
    return (String) storedValueForKey(REP_ETAT_KEY);
  }

  public void setRepEtat(String value) {
    takeStoredValueForKey(value, REP_ETAT_KEY);
  }

  public java.math.BigDecimal repMontantPaiement() {
    return (java.math.BigDecimal) storedValueForKey(REP_MONTANT_PAIEMENT_KEY);
  }

  public void setRepMontantPaiement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REP_MONTANT_PAIEMENT_KEY);
  }

  public java.math.BigDecimal repNbRepas() {
    return (java.math.BigDecimal) storedValueForKey(REP_NB_REPAS_KEY);
  }

  public void setRepNbRepas(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REP_NB_REPAS_KEY);
  }

  public java.math.BigDecimal repRepasAdm() {
    return (java.math.BigDecimal) storedValueForKey(REP_REPAS_ADM_KEY);
  }

  public void setRepRepasAdm(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REP_REPAS_ADM_KEY);
  }

  public java.math.BigDecimal repRepasGratuits() {
    return (java.math.BigDecimal) storedValueForKey(REP_REPAS_GRATUITS_KEY);
  }

  public void setRepRepasGratuits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REP_REPAS_GRATUITS_KEY);
  }

  public org.cocktail.kiwi.client.metier.EORembJournalier rembJournalier() {
    return (org.cocktail.kiwi.client.metier.EORembJournalier)storedValueForKey(REMB_JOURNALIER_KEY);
  }

  public void setRembJournalierRelationship(org.cocktail.kiwi.client.metier.EORembJournalier value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EORembJournalier oldValue = rembJournalier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REMB_JOURNALIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REMB_JOURNALIER_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOSegmentTarif segmentTarif() {
    return (org.cocktail.kiwi.client.metier.EOSegmentTarif)storedValueForKey(SEGMENT_TARIF_KEY);
  }

  public void setSegmentTarifRelationship(org.cocktail.kiwi.client.metier.EOSegmentTarif value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOSegmentTarif oldValue = segmentTarif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SEGMENT_TARIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SEGMENT_TARIF_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOWebmiss webmiss() {
    return (org.cocktail.kiwi.client.metier.EOWebmiss)storedValueForKey(WEBMISS_KEY);
  }

  public void setWebmissRelationship(org.cocktail.kiwi.client.metier.EOWebmiss value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOWebmiss oldValue = webmiss();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBMISS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBMISS_KEY);
    }
  }
  

  public static EORepas createRepas(EOEditingContext editingContext, java.math.BigDecimal repMontantPaiement
, java.math.BigDecimal repNbRepas
, java.math.BigDecimal repRepasAdm
, java.math.BigDecimal repRepasGratuits
, org.cocktail.kiwi.client.metier.EOSegmentTarif segmentTarif) {
    EORepas eo = (EORepas) createAndInsertInstance(editingContext, _EORepas.ENTITY_NAME);    
		eo.setRepMontantPaiement(repMontantPaiement);
		eo.setRepNbRepas(repNbRepas);
		eo.setRepRepasAdm(repRepasAdm);
		eo.setRepRepasGratuits(repRepasGratuits);
    eo.setSegmentTarifRelationship(segmentTarif);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORepas.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORepas.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EORepas localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepas)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORepas localInstanceIn(EOEditingContext editingContext, EORepas eo) {
    EORepas localInstance = (eo == null) ? null : (EORepas)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORepas#localInstanceIn a la place.
   */
	public static EORepas localInstanceOf(EOEditingContext editingContext, EORepas eo) {
		return EORepas.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORepas fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepas fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepas eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepas)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepas fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepas fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepas eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepas)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORepas fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepas eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepas ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepas fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
