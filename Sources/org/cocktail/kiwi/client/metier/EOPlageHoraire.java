

// EOPlageHoraire.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOPlageHoraire extends _EOPlageHoraire {

	public static final int CLE_NUITS =1;
	public static final int CLE_REPAS_MIDI = 2;
	public static final int CLE_REPAS_SOIR = 3;
	public static final int CLE_JOURNEE = 4;
	
    public EOPlageHoraire() {
        super();
    }

    public String plaType() {
    	
    	String retour = "";
    	
    	switch (plaOrdre().intValue()) {
    	
    	case CLE_NUITS: retour = "NUIT";break;
    	case CLE_REPAS_MIDI: retour = "REPAS MIDI";break;
    	case CLE_REPAS_SOIR: retour = "REPAS SOIR";break;
    	case CLE_JOURNEE: retour = "JOURNEE";break;
    	
    	}
    	
    	return retour;

    }
    
    public String plageObservations() {
    	
    	String retour = "";
    	    	
    	switch (plaOrdre().intValue()) {
    	
    	case CLE_NUITS: retour = "Heures de prise en compte d'une nuitée";break;
    	case CLE_REPAS_MIDI: retour = "Heure de prise en compte du déjeuner";break;
    	case CLE_REPAS_SOIR: retour = "Heure de prise en compte du diner";break;
    	
    	}
    	
    	return retour;
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
