

// EOMotifs.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.client.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOMotifs extends _EOMotifs {

    public EOMotifs() {
        super();
    }


	public static EOMotifs creer(EOEditingContext ec) {
		EOMotifs record = (EOMotifs)Factory.instanceForEntity(ec, EOMotifs.ENTITY_NAME);	
		ec.insertObject(record);
		return record;
	}	

	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
