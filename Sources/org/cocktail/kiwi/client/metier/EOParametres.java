

// EOParametres.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOParametres extends _EOParametres {

	public EOParametres() {
		super();
	}

	/**
	 * Recuperation d'une valeur pour une cle donnee
	 *
	 * @param ec Editing context global de l'application
	 * @param key Cle de la valeur a retourner.
	 */
	public static String getValue(EOEditingContext ec, EOExercice exercice, String key)	{

		NSMutableArray qualifiers = new NSMutableArray();

		try {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARAM_KEY_KEY + " = %@", new NSArray(key)));
			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers)).paramValue();
		}
		catch (Exception e) {
				return null;
		}
	}


	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {


	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}
