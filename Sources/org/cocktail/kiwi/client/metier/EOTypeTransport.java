

// EOTypeTransport.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTypeTransport extends _EOTypeTransport {

	public static final String VEHICULE_PERSONNEL = "VP";
	public static final String MOTOCYCLETTE = "MO";
	public static final String VELOMOTEUR = "VE";

	public static final String VEHICULE_SNCF = "XX";
	public static final String VEHICULE_SERVICE = "VS";
	public static final String VEHICULE_LOCATION = "VL";
	
    public EOTypeTransport() {
        super();
    }

	public String toString()	{
		return ttrLibelle();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public boolean isVehicule()	{
		return (VEHICULE_SNCF.equals(ttrType()) || VEHICULE_SERVICE.equals(ttrType()) || VEHICULE_PERSONNEL.equals(ttrType()) || VEHICULE_LOCATION.equals(ttrType())  || MOTOCYCLETTE.equals(ttrType())  || VELOMOTEUR.equals(ttrType()) );
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isMotocyclette()	{
		return (MOTOCYCLETTE.equals(ttrType()));
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isVelomoteur()	{
		return (VELOMOTEUR.equals(ttrType()));
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isVehiculeService()	{
		return (VEHICULE_SERVICE.equals(ttrType()) );
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isVehiculeLocation()	{
		return (VEHICULE_LOCATION.equals(ttrType()) );
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isVehiculePersonnel()	{
		return (ttrPersonnel().equals("1") );
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isVehiculeSncf()	{
		return VEHICULE_SNCF.equals(ttrType());
	}

	
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
