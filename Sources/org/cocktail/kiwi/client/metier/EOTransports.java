

// EOTransports.java
// 
package org.cocktail.kiwi.client.metier;


import java.math.BigDecimal;

import org.cocktail.kiwi.client.ApplicationClient;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOTransports extends _EOTransports {

    public EOTransports() {
        super();
    }

    
	public static NSArray findForSegment(EOEditingContext ec, EOSegmentTarif segment)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEGMENT_TARIF_KEY + " = %@", new NSArray(segment)));
				
		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);
		
	}


	public void calculerkm(float cumulKm)	{
		float l1 = 2000; // limite
		float l2 = 10000; // limite
//		float l3 = 99000; // limite
		float kl1 = 0; // valeur de km1
		float kl2 = 0; // valeur de km2
		float kl3 = 0; // valeur de km3
		float kcl1 = 0; // valeur de km1 cumul
		float kcl2 = 0; // valeur de km2 cumul
		float kcl3 = 0; // valeur de km3 cumul
		
		float c = 0;
		float p = 0;
		float t = 0;
				
		if ( indemniteKm() != null ) {
			p = traKmSaisie().floatValue();  // parcours
			c = cumulKm - p; // cumul km precedent - celui en cours
			t = p+c;  // total
						
			// calcul des valeurs avec le parcours total
			if (t <= l1)
				kl1 = t;
			else
			{
				kl1 = l1;
				if (t <= l2)
					kl2 = t-kl1;
				else
				{
					kl2 = l2-l1;
					kl3 = t-(l2);
				}
			}
			
			// calcul des valeurs avec du cumul
			if (c <= l1)
				kcl1 = c;
			else
			{
				kcl1 = l1;
				if (c <= l2)
					kcl2 = c-kcl1;
				else
				{
					kcl2 = l2-l1;
					kcl3 = c-(l2);
				}
			}
			
			if (kl1-kcl1 > 0)
				kl1 = kl1-kcl1;
			else
				kl1=0;
			
			if (kl2-kcl2 > 0)
				kl2 = kl2-kcl2;
			else
				kl2=0;
			
			if (kl3-kcl3 > 0)
				kl3 = kl3-kcl3;
			else
				kl3=0;
		}
				
		setSegKm1( new BigDecimal(kl1).setScale(2, BigDecimal.ROUND_HALF_UP));
		setSegKm2( new BigDecimal(kl2).setScale(2, BigDecimal.ROUND_HALF_UP));
		setSegKm3( new BigDecimal(kl3).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	/**
	 * 
	 *
	 */
	public void calculerMontant()	{
		
		float montantKm =0;
		float montantFrais =0;
		
		if (indemniteKm() != null ) // sil y a un vehicule
			montantKm =(segKm1().floatValue()*indemniteKm().ikmTarif1().floatValue()+
					segKm2().floatValue()*indemniteKm().ikmTarif2().floatValue()+
					segKm3().floatValue()*indemniteKm().ikmTarif3().floatValue());
		
		montantFrais =(traMontant().floatValue()*webtaux().wtaTaux().floatValue());
		
		if (!typeTransport().isVehiculeService()) {
			setTraMontantKm(new BigDecimal(montantKm).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP));
			setTraMontantPaiement(new BigDecimal(montantKm+montantFrais).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP));
		}
		else {
			setTraMontantKm(new BigDecimal(0));
			setTraMontantPaiement(new BigDecimal(0));
		}
			
	}
	
	public void calculerMontantBaseSncf() {

		double montantKm = 0.0;
		EOTarifSncf tarifSncf = tarifSncf();
		int kmSaisie = traKmSaisie().intValue();
		
		if(tarifSncf!=null && kmSaisie>0) {
			
			double facteur = tarifSncf.facteur().doubleValue();
			double prixKm = tarifSncf.prixKm().doubleValue();
			montantKm = (double)facteur+prixKm*kmSaisie;

		}	
		
		setTraMontantKm(new BigDecimal(montantKm).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP));
		
		double montantFrais =(double)(traMontant().floatValue()*webtaux().wtaTaux().floatValue());
		
		setTraMontantPaiement(new BigDecimal(montantKm+montantFrais).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP));
	}
	
	
	/**
	 * 
	 * @param sender
	 */
	public void calculerMontantPaiement()	{
		
		try {
			
			BigDecimal localMontantFrais = traMontant();
			BigDecimal taux = webtaux().wtaTaux();
			
			localMontantFrais = localMontantFrais.multiply(taux);

			localMontantFrais = localMontantFrais.setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);

			BigDecimal localMontantPaiement = localMontantFrais.add(traMontantKm()).setScale(ApplicationClient.USED_DECIMALES);

			setTraMontantPaiement(localMontantPaiement);			
		}
		catch (Exception e)	{
		}
		
	}

	
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
