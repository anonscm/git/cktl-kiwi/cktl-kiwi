

// EOPayeur.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.common.utilities.NumberCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPayeur extends _EOPayeur {

    public EOPayeur() {
        super();
    }

    public static NSArray find(EOEditingContext ec)	{
    	
    	return fetchAll(ec);
    	
    }
    
    public boolean estValide() {
    	return temValide().equals("O");
    }
    
    public static NSArray findPayeuresValides(EOEditingContext ec)	{
    	
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+"=%@", new NSArray("O"));
    	return fetchAll(ec, qualifier, null);
    	
    }

    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
    	
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      
		if (codePayeur() == null || codePayeur().length() > 1)
			throw new ValidationException("Vous devez saisir un code payeur (1 caractère) !");

		if (NumberCtrl.estUnChiffre(codePayeur()))
			throw new ValidationException("Veuillez utiliser un caractère alphanumérique pour le code payeur !" );

		if (libellePayeurLong() == null || libellePayeurLong().length() > 40)
			throw new ValidationException("Vous devez saisir un libellé long (40 caractères maximum)!");	

		if (libellePayeurCourt() == null || libellePayeurCourt().length() > 10)
			throw new ValidationException("Vous devez saisir un libellé court (10 caractères maximum) !");	



    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
