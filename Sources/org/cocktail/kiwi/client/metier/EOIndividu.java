

// EOIndividu.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOIndividu extends _EOIndividu {

    public EOIndividu() {
        super();
    }

	public String identite() {
		return nomUsuel() + " " + prenom();
	}
	public String identitePrenomFirst() {
		return prenom() + " " + nomUsuel();
	}

	/**
	 * trouve un individu d'apres son no_individu
	 * @param ec
	 * @param noIndividu
	 * @return
	 */
	public static EOIndividu findIndividuForNoIndividuInContext(EOEditingContext ec, Number noIndividu) {
	    EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOIndividu.NO_INDIVIDU_KEY + " = %@", new NSArray(noIndividu));	    
        return fetchFirstByQualifier(ec, qual);        
	}
	
	/**
     * tourver un individu par son persId
     * @param ec
     * @param persId
     * @return
     */
    public static EOIndividu findIndividuForPersIdInContext(EOEditingContext ec, Number persId) {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@", new NSArray(persId));
        return fetchFirstByQualifier(ec, qual);        
    }

    /**
     * 
     */
    public String toString() {
    	return nomUsuel();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
