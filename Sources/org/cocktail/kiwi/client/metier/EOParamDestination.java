

// EOParamDestination.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOParamDestination extends _EOParamDestination {

    public EOParamDestination() {
        super();
    }

	public static NSArray findParamDestinations(EOEditingContext ec, EOExercice exercice)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOParamDestination.TO_EXERCICE_KEY + " = %@", new NSArray(exercice));
			EOFetchSpecification fs = new EOFetchSpecification(EOParamDestination.ENTITY_NAME,myQualifier, null);
			
			return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 * 
	 * @param ec
	 * @param annee
	 * @param code
	 * @return
	 */
	public static EOParamDestination findParamForCode(EOEditingContext ec, EOExercice exercice, String code)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOParamDestination.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOParamDestination.TYPE_ACTION_KEY + "." + EOLolfNomenclatureDepense.LOLF_CODE_KEY + " = %@", new NSArray(code)));

		EOFetchSpecification fs = new EOFetchSpecification(EOParamDestination.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
		try {return (EOParamDestination)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	  {return null;}
	}

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
