// _EOMissionPreferencesPerso.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMissionPreferencesPerso.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMissionPreferencesPerso extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MissionPreferencesPerso";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.MISSION_PREFERENCES_PERSO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "mppOrdre";

	public static final String REM_TAUX_KEY = "remTaux";
	public static final String TYPE_LANCEMENT_KEY = "typeLancement";
	public static final String UPDATE_DATES_TRAJET_KEY = "updateDatesTrajet";

//Colonnes dans la base de donnees
	public static final String REM_TAUX_COLKEY = "REM_TAUX";
	public static final String TYPE_LANCEMENT_COLKEY = "TYPE_LANCEMENT";
	public static final String UPDATE_DATES_TRAJET_COLKEY = "UPDATE_DATES_TRAJET";

// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String PAYEUR_KEY = "payeur";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String REMB_ZONE_KEY = "rembZone";
	public static final String TITRE_KEY = "titre";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String remTaux() {
    return (String) storedValueForKey(REM_TAUX_KEY);
  }

  public void setRemTaux(String value) {
    takeStoredValueForKey(value, REM_TAUX_KEY);
  }

  public String typeLancement() {
    return (String) storedValueForKey(TYPE_LANCEMENT_KEY);
  }

  public void setTypeLancement(String value) {
    takeStoredValueForKey(value, TYPE_LANCEMENT_KEY);
  }

  public String updateDatesTrajet() {
    return (String) storedValueForKey(UPDATE_DATES_TRAJET_KEY);
  }

  public void setUpdateDatesTrajet(String value) {
    takeStoredValueForKey(value, UPDATE_DATES_TRAJET_KEY);
  }

  public org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOModePaiement modePaiement() {
    return (org.cocktail.kiwi.client.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.kiwi.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOPayeur payeur() {
    return (org.cocktail.kiwi.client.metier.EOPayeur)storedValueForKey(PAYEUR_KEY);
  }

  public void setPayeurRelationship(org.cocktail.kiwi.client.metier.EOPayeur value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOPayeur oldValue = payeur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAYEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAYEUR_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kiwi.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kiwi.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EORembZone rembZone() {
    return (org.cocktail.kiwi.client.metier.EORembZone)storedValueForKey(REMB_ZONE_KEY);
  }

  public void setRembZoneRelationship(org.cocktail.kiwi.client.metier.EORembZone value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EORembZone oldValue = rembZone();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REMB_ZONE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REMB_ZONE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOTitreMission titre() {
    return (org.cocktail.kiwi.client.metier.EOTitreMission)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.kiwi.client.metier.EOTitreMission value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOTitreMission oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeCredit toTypeCredit() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TO_TYPE_CREDIT_KEY);
  }

  public void setToTypeCreditRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense typeAction() {
    return (org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense)storedValueForKey(TYPE_ACTION_KEY);
  }

  public void setTypeActionRelationship(org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.client.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.client.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOMissionPreferencesPerso createMissionPreferencesPerso(EOEditingContext editingContext) {
    EOMissionPreferencesPerso eo = (EOMissionPreferencesPerso) createAndInsertInstance(editingContext, _EOMissionPreferencesPerso.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOMissionPreferencesPerso.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOMissionPreferencesPerso.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOMissionPreferencesPerso localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMissionPreferencesPerso)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOMissionPreferencesPerso localInstanceIn(EOEditingContext editingContext, EOMissionPreferencesPerso eo) {
    EOMissionPreferencesPerso localInstance = (eo == null) ? null : (EOMissionPreferencesPerso)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOMissionPreferencesPerso#localInstanceIn a la place.
   */
	public static EOMissionPreferencesPerso localInstanceOf(EOEditingContext editingContext, EOMissionPreferencesPerso eo) {
		return EOMissionPreferencesPerso.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOMissionPreferencesPerso fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOMissionPreferencesPerso fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMissionPreferencesPerso eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMissionPreferencesPerso)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMissionPreferencesPerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMissionPreferencesPerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMissionPreferencesPerso eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMissionPreferencesPerso)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOMissionPreferencesPerso fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMissionPreferencesPerso eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMissionPreferencesPerso ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMissionPreferencesPerso fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
