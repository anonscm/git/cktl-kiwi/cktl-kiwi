

// EOTitreMission.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTitreMission extends _EOTitreMission {

    public EOTitreMission() {
        super();
    }

    public String decodeCtrlDates()	{
    	if (titCtrlDates().intValue() == 1)
    		return "O";

    	return "N";
    }    

    public String decodeCtrlPlanning()	{
    	if (titCtrlPlanning().intValue() == 1)
    		return "O";

    	return "N";
    }    

    public String decodeCtrlConges()	{
    	if (titCtrlConge().intValue() == 1)
    		return "O";

    	return "N";
    }    

    public String decodePaiement()	{
    	if (titPaiement().intValue() == 1)
    		return "O";

    	return "N";
    }

    public String decodePermanent()	{
    	    	
    	if (titPermanent().intValue() == 1)
    		return "O";

    	return "N";
    }

    public String decodeLbud()	{
    	
    	if (titSaisieLbud().intValue() == 1)
    		return "O";

    	return "N";
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
