/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kiwi.client.metier;

import java.math.BigDecimal;

import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.metier.budget.EODepensePapier;
import org.cocktail.kiwi.client.metier.budget.EOEngage;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOMissionPaiementDepense extends _EOMissionPaiementDepense {

    public EOMissionPaiementDepense() {
        super();
    }

    
	public static NSArray<EOMissionPaiementDepense> findDepensesExtourneForMission(EOEditingContext ec, EOMissionPaiement missionPaiement)	{

		try {
						
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMissionPaiementDepense.MISSION_PAIEMENT_KEY+ " = %@", new NSArray(missionPaiement)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMissionPaiementDepense.DEPENSE_KEY+"."+EODepense.DEPENSE_PAPIER_KEY+"."+EODepensePapier.MODE_PAIEMENT_KEY+"."+EOModePaiement.MOD_DOM_KEY + " = %@", new NSArray(EOModePaiement.CODE_DOMAINE_EXTOURNE)));

			EOFetchSpecification fs = new EOFetchSpecification(EOMissionPaiementDepense.ENTITY_NAME, new EOAndQualifier(qualifiers), null);
			return ec.objectsWithFetchSpecification(fs);
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}

	
	public static NSArray<EOMissionPaiementDepense> findDepensesForMission(EOEditingContext ec, EOMission mission)	{

		try {
									
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
					EOMissionPaiementDepense.MISSION_PAIEMENT_KEY+"."+EOMissionPaiement.MISSION_KEY + " = %@", new NSArray(mission));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOMissionPaiementDepense.ENTITY_NAME,myQualifier, null);
			return ec.objectsWithFetchSpecification(fs);
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static NSArray findDepensesForMissionAndEngage(EOEditingContext ec, EOMission mission)	{

		try {
			
			NSMutableArray qualifiers = new NSMutableArray();
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMissionPaiementDepense.MISSION_PAIEMENT_KEY+"."+EOMissionPaiement.MISSION_KEY + " = %@", new NSArray(mission)));

			EOFetchSpecification fs = new EOFetchSpecification(EOMissionPaiementDepense.ENTITY_NAME,new EOAndQualifier(qualifiers), null);
			return ec.objectsWithFetchSpecification(fs);
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}

	
	/**
	 * 
	 * @param ec
	 * @param engage
	 * @return
	 */
	public static NSArray findDepensesForEngageBudget(EOEditingContext ec, EOEngage engage)	{

		try {

			NSMutableArray depenses = new NSMutableArray();

			// Depenses associees a la mission
			NSMutableArray qualifiers = new NSMutableArray();			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMissionPaiementDepense.DEPENSE_KEY+"."+EODepense.ENGAGE_KEY + " = %@", new NSArray(engage)));			
			depenses.addObjectsFromArray(EOMissionPaiementDepense.fetchAll(ec, new EOAndQualifier(qualifiers), null, true));
			
//			// Depenses associees a l'engagement 
//			NSArray depensesEngage = EODepense.findDepensesForEngage(ec, engage);
//			depenses.addObjectsFromArray(depensesEngage);

			return depenses.immutableClone();
			
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return new NSArray();
		}
	}

	
	public static NSArray findDepensesAvancesForMission(EOEditingContext ec, EOMission mission)	{

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMissionPaiementDepense.MISSION_PAIEMENT_KEY+"."+EOMissionPaiement.MISSION_KEY + " = %@", new NSArray(mission)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMissionPaiementDepense.DEPENSE_KEY+"."+EODepense.DEPENSE_PAPIER_KEY+
							"."+EODepensePapier.DPP_NUMERO_FACTURE_KEY + " caseInsensitiveLike %@", new NSArray("*AVANCE*")));

			EOFetchSpecification fs = new EOFetchSpecification(EOMissionPaiementDepense.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	public static BigDecimal montantDepensesForMission(EOEditingContext ec, EOMission mission) {
		
		BigDecimal montant = new BigDecimal(0);
		
		NSArray depenses = findDepensesForMission(ec, mission);
		
		for (int i=0;i<depenses.count();i++) {
			
			montant = montant.add( ( (EOMissionPaiementDepense)depenses.objectAtIndex(i)).depense().depTtcSaisie());
			
		}
		
		return montant;
		
	}

	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
