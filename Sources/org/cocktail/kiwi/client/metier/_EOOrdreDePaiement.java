// _EOOrdreDePaiement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrdreDePaiement.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOrdreDePaiement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "OrdreDePaiement";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.V_ODP_SUIVI";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "odpOrdre";

	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOD_CODE_KEY = "modCode";
	public static final String MOD_DOM_KEY = "modDom";
	public static final String MOD_LIBELLE_KEY = "modLibelle";
	public static final String ODP_DATE_SAISIE_KEY = "odpDateSaisie";
	public static final String ODP_ETAT_KEY = "odpEtat";
	public static final String ODP_HT_KEY = "odpHt";
	public static final String ODP_NUMERO_KEY = "odpNumero";
	public static final String ODP_TTC_KEY = "odpTtc";
	public static final String ODP_TVA_KEY = "odpTva";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_NUMERO_KEY = "paiNumero";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TVI_LIBELLE_KEY = "tviLibelle";
	public static final String VIR_DATE_CREATION_KEY = "virDateCreation";
	public static final String VIR_DATE_VALEUR_KEY = "virDateValeur";

//Colonnes dans la base de donnees
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String MOD_DOM_COLKEY = "MOD_DOM";
	public static final String MOD_LIBELLE_COLKEY = "MOD_LIBELLE";
	public static final String ODP_DATE_SAISIE_COLKEY = "ODP_DATE_SAISIE";
	public static final String ODP_ETAT_COLKEY = "ODP_ETAT";
	public static final String ODP_HT_COLKEY = "ODP_HT";
	public static final String ODP_NUMERO_COLKEY = "ODP_NUMERO";
	public static final String ODP_TTC_COLKEY = "ODP_TTC";
	public static final String ODP_TVA_COLKEY = "ODP_TVA";
	public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ORI_ORDRE";
	public static final String PAI_NUMERO_COLKEY = "PAI_NUMERO";
	public static final String PAI_ORDRE_COLKEY = "PAI_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String TVI_LIBELLE_COLKEY = "TVI_LIBELLE";
	public static final String VIR_DATE_CREATION_COLKEY = "VIR_DATE_CREATION";
	public static final String VIR_DATE_VALEUR_COLKEY = "VIR_DATE_VALEUR";

// Relationships



	// Accessors methods
  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public String modCode() {
    return (String) storedValueForKey(MOD_CODE_KEY);
  }

  public void setModCode(String value) {
    takeStoredValueForKey(value, MOD_CODE_KEY);
  }

  public String modDom() {
    return (String) storedValueForKey(MOD_DOM_KEY);
  }

  public void setModDom(String value) {
    takeStoredValueForKey(value, MOD_DOM_KEY);
  }

  public String modLibelle() {
    return (String) storedValueForKey(MOD_LIBELLE_KEY);
  }

  public void setModLibelle(String value) {
    takeStoredValueForKey(value, MOD_LIBELLE_KEY);
  }

  public NSTimestamp odpDateSaisie() {
    return (NSTimestamp) storedValueForKey(ODP_DATE_SAISIE_KEY);
  }

  public void setOdpDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ODP_DATE_SAISIE_KEY);
  }

  public String odpEtat() {
    return (String) storedValueForKey(ODP_ETAT_KEY);
  }

  public void setOdpEtat(String value) {
    takeStoredValueForKey(value, ODP_ETAT_KEY);
  }

  public java.math.BigDecimal odpHt() {
    return (java.math.BigDecimal) storedValueForKey(ODP_HT_KEY);
  }

  public void setOdpHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODP_HT_KEY);
  }

  public Integer odpNumero() {
    return (Integer) storedValueForKey(ODP_NUMERO_KEY);
  }

  public void setOdpNumero(Integer value) {
    takeStoredValueForKey(value, ODP_NUMERO_KEY);
  }

  public java.math.BigDecimal odpTtc() {
    return (java.math.BigDecimal) storedValueForKey(ODP_TTC_KEY);
  }

  public void setOdpTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODP_TTC_KEY);
  }

  public java.math.BigDecimal odpTva() {
    return (java.math.BigDecimal) storedValueForKey(ODP_TVA_KEY);
  }

  public void setOdpTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODP_TVA_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public Integer oriOrdre() {
    return (Integer) storedValueForKey(ORI_ORDRE_KEY);
  }

  public void setOriOrdre(Integer value) {
    takeStoredValueForKey(value, ORI_ORDRE_KEY);
  }

  public Integer paiNumero() {
    return (Integer) storedValueForKey(PAI_NUMERO_KEY);
  }

  public void setPaiNumero(Integer value) {
    takeStoredValueForKey(value, PAI_NUMERO_KEY);
  }

  public Integer paiOrdre() {
    return (Integer) storedValueForKey(PAI_ORDRE_KEY);
  }

  public void setPaiOrdre(Integer value) {
    takeStoredValueForKey(value, PAI_ORDRE_KEY);
  }

  public Integer ribOrdre() {
    return (Integer) storedValueForKey(RIB_ORDRE_KEY);
  }

  public void setRibOrdre(Integer value) {
    takeStoredValueForKey(value, RIB_ORDRE_KEY);
  }

  public String tviLibelle() {
    return (String) storedValueForKey(TVI_LIBELLE_KEY);
  }

  public void setTviLibelle(String value) {
    takeStoredValueForKey(value, TVI_LIBELLE_KEY);
  }

  public NSTimestamp virDateCreation() {
    return (NSTimestamp) storedValueForKey(VIR_DATE_CREATION_KEY);
  }

  public void setVirDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, VIR_DATE_CREATION_KEY);
  }

  public NSTimestamp virDateValeur() {
    return (NSTimestamp) storedValueForKey(VIR_DATE_VALEUR_KEY);
  }

  public void setVirDateValeur(NSTimestamp value) {
    takeStoredValueForKey(value, VIR_DATE_VALEUR_KEY);
  }


  public static EOOrdreDePaiement createOrdreDePaiement(EOEditingContext editingContext, Integer exeOrdre
, Integer fouOrdre
, String modCode
, String modDom
, String modLibelle
, NSTimestamp odpDateSaisie
, String odpEtat
, java.math.BigDecimal odpHt
, Integer odpNumero
, java.math.BigDecimal odpTtc
, java.math.BigDecimal odpTva
, Integer orgOrdre
) {
    EOOrdreDePaiement eo = (EOOrdreDePaiement) createAndInsertInstance(editingContext, _EOOrdreDePaiement.ENTITY_NAME);    
		eo.setExeOrdre(exeOrdre);
		eo.setFouOrdre(fouOrdre);
		eo.setModCode(modCode);
		eo.setModDom(modDom);
		eo.setModLibelle(modLibelle);
		eo.setOdpDateSaisie(odpDateSaisie);
		eo.setOdpEtat(odpEtat);
		eo.setOdpHt(odpHt);
		eo.setOdpNumero(odpNumero);
		eo.setOdpTtc(odpTtc);
		eo.setOdpTva(odpTva);
		eo.setOrgOrdre(orgOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOOrdreDePaiement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOOrdreDePaiement.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOOrdreDePaiement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrdreDePaiement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOOrdreDePaiement localInstanceIn(EOEditingContext editingContext, EOOrdreDePaiement eo) {
    EOOrdreDePaiement localInstance = (eo == null) ? null : (EOOrdreDePaiement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOOrdreDePaiement#localInstanceIn a la place.
   */
	public static EOOrdreDePaiement localInstanceOf(EOEditingContext editingContext, EOOrdreDePaiement eo) {
		return EOOrdreDePaiement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOOrdreDePaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOOrdreDePaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrdreDePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrdreDePaiement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrdreDePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrdreDePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrdreDePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrdreDePaiement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOOrdreDePaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrdreDePaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrdreDePaiement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrdreDePaiement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
