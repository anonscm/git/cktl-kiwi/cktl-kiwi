

// EOLogTelechargement.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOLogTelechargement extends _EOLogTelechargement {

	public static EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(LTC_DATE_KEY, EOSortOrdering.CompareDescending);
	public static NSArray SORT_ARRAY_DATE_DESC = new NSArray(SORT_DATE_DESC);  
	
    public EOLogTelechargement() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
