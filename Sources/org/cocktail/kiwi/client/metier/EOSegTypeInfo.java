

// EOSegTypeInfo.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.client.factory.Factory;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOSegTypeInfo extends _EOSegTypeInfo {

	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(new EOSortOrdering(STI_LIBELLE_KEY, EOSortOrdering.CompareAscending));
	public static String ETAT_VALIDE = "VALIDE";
	public static String ETAT_INVALIDE = "INVALIDE";
		
	public static String SIGNE_FORFAIT = "FORFAIT";
	public static String SIGNE_NEUTRE = "NEUTRE";
	public static String SIGNE_POSITIF = "POSITIF";
	public static String SIGNE_NEGATIF = "NEGATIF";
	
    public EOSegTypeInfo() {
        super();
    }
    
    public String toString() {
    	return stiLibelle();
    }
    
    public boolean estValide() {
    	return stiEtat().equals(ETAT_VALIDE);
    }
    public void setEstValide(boolean yn) {
    	if (yn)
    		setStiEtat(ETAT_VALIDE);
    	else
    		setStiEtat(ETAT_INVALIDE);
    }
    
	public static EOSegTypeInfo creer(EOEditingContext ec, EOSegGroupeInfo groupe) {
		
		EOSegTypeInfo object = (EOSegTypeInfo)Factory.instanceForEntity(ec, EOSegTypeInfo.ENTITY_NAME);		
		object.setSegGroupeInfoRelationship(groupe);
						
		object.setStiBcde("NON");
		object.setStiEtat(ETAT_VALIDE);
		
		ec.insertObject(object);
		return object;
	}	

	public static NSArray findTypesInfoForGroupe(EOEditingContext ec, EOSegGroupeInfo groupeInfo)	{		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(SEG_GROUPE_INFO_KEY + "= %@ ", new NSArray(groupeInfo));
		return fetchAll(ec, myQualifier, SORT_ARRAY_LIBELLE_ASC);
	}
	public static NSArray findTypesInfoValidesForGroupe(EOEditingContext ec, EOSegGroupeInfo groupeInfo)	{		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STI_ETAT_KEY + "= %@ ", new NSArray(ETAT_VALIDE)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEG_GROUPE_INFO_KEY + "= %@ ", new NSArray(groupeInfo)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE_ASC);
	}


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {

        if (stiExplications() == null)
        	throw new ValidationException("Veuillez entrer un commentaire pour ce type de remboursement !");

        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
       
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
