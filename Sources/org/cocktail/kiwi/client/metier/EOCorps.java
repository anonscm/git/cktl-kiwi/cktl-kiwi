

// EOCorps.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCorps extends _EOCorps {

	private static String DEFAULT_CODE_CORPS = "999";

    public EOCorps() {
        super();
    }
	
	public String toString()	{
		return llCorps();
	}
		
	
	public static NSArray findCorps(EOEditingContext ec) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		NSMutableArray orQualifiers = new NSMutableArray();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_OUVERTURE_CORPS_KEY + " <= %@",new NSArray(new NSTimestamp())));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_OUVERTURE_CORPS_KEY + " = nil ", null));

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		orQualifiers = new NSMutableArray();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_CORPS_KEY + " = nil ", null));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_CORPS_KEY + " >= %@",new NSArray(new NSTimestamp())));

		qualifiers.addObject(new EOOrQualifier(orQualifiers));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);
		
	}
	

    /**
     * 
     * @param f
     * @return
     */
	public static EOCorps getCorpsFromMangue(EOEditingContext ec, EOFournis f) {
		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOElementCarriere.TEM_VALIDE_KEY + " =%@", new NSArray("O")));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOElementCarriere.INDIVIDU_KEY+"."+EOIndividu.PERS_ID_KEY + " =%@", new NSArray(f.persId())));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOElementCarriere.D_EFFET_ELEMENT_KEY + " <= %@", new NSArray(new NSTimestamp())));

			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOElementCarriere.D_FIN_ELEMENT_KEY + " >= %@", new NSArray(new NSTimestamp())));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOElementCarriere.D_FIN_ELEMENT_KEY + " = nil", new NSArray()));

			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));
			
			NSArray elements = EOElementCarriere.fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);
						
			if (elements.count() > 0)
				return ((EOElementCarriere)elements.objectAtIndex(0)).corps();
					
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
