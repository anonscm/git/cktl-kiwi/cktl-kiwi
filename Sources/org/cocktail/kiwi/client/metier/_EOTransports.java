// _EOTransports.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTransports.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTransports extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Transports";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.TRANSPORTS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "traOrdre";

	public static final String SEG_KM1_KEY = "segKm1";
	public static final String SEG_KM2_KEY = "segKm2";
	public static final String SEG_KM3_KEY = "segKm3";
	public static final String TRA_ARRIVEE_KEY = "traArrivee";
	public static final String TRA_DEPART_KEY = "traDepart";
	public static final String TRA_DUREE_KEY = "traDuree";
	public static final String TRA_ETAT_KEY = "traEtat";
	public static final String TRA_KM_SAISIE_KEY = "traKmSaisie";
	public static final String TRA_LIB_FRAIS_KEY = "traLibFrais";
	public static final String TRA_MONTANT_KEY = "traMontant";
	public static final String TRA_MONTANT_KM_KEY = "traMontantKm";
	public static final String TRA_MONTANT_PAIEMENT_KEY = "traMontantPaiement";
	public static final String TRA_UTILISATEUR_KEY = "traUtilisateur";

//Colonnes dans la base de donnees
	public static final String SEG_KM1_COLKEY = "SEG_KM1";
	public static final String SEG_KM2_COLKEY = "SEG_KM2";
	public static final String SEG_KM3_COLKEY = "SEG_KM3";
	public static final String TRA_ARRIVEE_COLKEY = "TRA_ARRIVEE";
	public static final String TRA_DEPART_COLKEY = "TRA_DEPART";
	public static final String TRA_DUREE_COLKEY = "TRA_DUREE";
	public static final String TRA_ETAT_COLKEY = "TRA_ETAT";
	public static final String TRA_KM_SAISIE_COLKEY = "TRA_KM_SAISIE";
	public static final String TRA_LIB_FRAIS_COLKEY = "TRA_LIB_FRAIS";
	public static final String TRA_MONTANT_COLKEY = "TRA_MONTANT";
	public static final String TRA_MONTANT_KM_COLKEY = "TRA_MONTANT_KM";
	public static final String TRA_MONTANT_PAIEMENT_COLKEY = "TRA_MONTANT_PAIEMENT";
	public static final String TRA_UTILISATEUR_COLKEY = "TRA_UTILISATEUR";

// Relationships
	public static final String INDEMNITE_KM_KEY = "indemniteKm";
	public static final String SEGMENT_TARIF_KEY = "segmentTarif";
	public static final String TARIF_SNCF_KEY = "tarifSncf";
	public static final String TYPE_TRANSPORT_KEY = "typeTransport";
	public static final String VEHICULE_KEY = "vehicule";
	public static final String WEBTAUX_KEY = "webtaux";



	// Accessors methods
  public java.math.BigDecimal segKm1() {
    return (java.math.BigDecimal) storedValueForKey(SEG_KM1_KEY);
  }

  public void setSegKm1(java.math.BigDecimal value) {
    takeStoredValueForKey(value, SEG_KM1_KEY);
  }

  public java.math.BigDecimal segKm2() {
    return (java.math.BigDecimal) storedValueForKey(SEG_KM2_KEY);
  }

  public void setSegKm2(java.math.BigDecimal value) {
    takeStoredValueForKey(value, SEG_KM2_KEY);
  }

  public java.math.BigDecimal segKm3() {
    return (java.math.BigDecimal) storedValueForKey(SEG_KM3_KEY);
  }

  public void setSegKm3(java.math.BigDecimal value) {
    takeStoredValueForKey(value, SEG_KM3_KEY);
  }

  public String traArrivee() {
    return (String) storedValueForKey(TRA_ARRIVEE_KEY);
  }

  public void setTraArrivee(String value) {
    takeStoredValueForKey(value, TRA_ARRIVEE_KEY);
  }

  public String traDepart() {
    return (String) storedValueForKey(TRA_DEPART_KEY);
  }

  public void setTraDepart(String value) {
    takeStoredValueForKey(value, TRA_DEPART_KEY);
  }

  public java.math.BigDecimal traDuree() {
    return (java.math.BigDecimal) storedValueForKey(TRA_DUREE_KEY);
  }

  public void setTraDuree(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TRA_DUREE_KEY);
  }

  public String traEtat() {
    return (String) storedValueForKey(TRA_ETAT_KEY);
  }

  public void setTraEtat(String value) {
    takeStoredValueForKey(value, TRA_ETAT_KEY);
  }

  public Double traKmSaisie() {
    return (Double) storedValueForKey(TRA_KM_SAISIE_KEY);
  }

  public void setTraKmSaisie(Double value) {
    takeStoredValueForKey(value, TRA_KM_SAISIE_KEY);
  }

  public String traLibFrais() {
    return (String) storedValueForKey(TRA_LIB_FRAIS_KEY);
  }

  public void setTraLibFrais(String value) {
    takeStoredValueForKey(value, TRA_LIB_FRAIS_KEY);
  }

  public java.math.BigDecimal traMontant() {
    return (java.math.BigDecimal) storedValueForKey(TRA_MONTANT_KEY);
  }

  public void setTraMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TRA_MONTANT_KEY);
  }

  public java.math.BigDecimal traMontantKm() {
    return (java.math.BigDecimal) storedValueForKey(TRA_MONTANT_KM_KEY);
  }

  public void setTraMontantKm(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TRA_MONTANT_KM_KEY);
  }

  public java.math.BigDecimal traMontantPaiement() {
    return (java.math.BigDecimal) storedValueForKey(TRA_MONTANT_PAIEMENT_KEY);
  }

  public void setTraMontantPaiement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TRA_MONTANT_PAIEMENT_KEY);
  }

  public String traUtilisateur() {
    return (String) storedValueForKey(TRA_UTILISATEUR_KEY);
  }

  public void setTraUtilisateur(String value) {
    takeStoredValueForKey(value, TRA_UTILISATEUR_KEY);
  }

  public org.cocktail.kiwi.client.metier.EOIndemniteKm indemniteKm() {
    return (org.cocktail.kiwi.client.metier.EOIndemniteKm)storedValueForKey(INDEMNITE_KM_KEY);
  }

  public void setIndemniteKmRelationship(org.cocktail.kiwi.client.metier.EOIndemniteKm value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOIndemniteKm oldValue = indemniteKm();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDEMNITE_KM_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDEMNITE_KM_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOSegmentTarif segmentTarif() {
    return (org.cocktail.kiwi.client.metier.EOSegmentTarif)storedValueForKey(SEGMENT_TARIF_KEY);
  }

  public void setSegmentTarifRelationship(org.cocktail.kiwi.client.metier.EOSegmentTarif value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOSegmentTarif oldValue = segmentTarif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SEGMENT_TARIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SEGMENT_TARIF_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOTarifSncf tarifSncf() {
    return (org.cocktail.kiwi.client.metier.EOTarifSncf)storedValueForKey(TARIF_SNCF_KEY);
  }

  public void setTarifSncfRelationship(org.cocktail.kiwi.client.metier.EOTarifSncf value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOTarifSncf oldValue = tarifSncf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TARIF_SNCF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TARIF_SNCF_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOTypeTransport typeTransport() {
    return (org.cocktail.kiwi.client.metier.EOTypeTransport)storedValueForKey(TYPE_TRANSPORT_KEY);
  }

  public void setTypeTransportRelationship(org.cocktail.kiwi.client.metier.EOTypeTransport value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOTypeTransport oldValue = typeTransport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TRANSPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TRANSPORT_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOVehicule vehicule() {
    return (org.cocktail.kiwi.client.metier.EOVehicule)storedValueForKey(VEHICULE_KEY);
  }

  public void setVehiculeRelationship(org.cocktail.kiwi.client.metier.EOVehicule value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOVehicule oldValue = vehicule();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, VEHICULE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, VEHICULE_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOWebtaux webtaux() {
    return (org.cocktail.kiwi.client.metier.EOWebtaux)storedValueForKey(WEBTAUX_KEY);
  }

  public void setWebtauxRelationship(org.cocktail.kiwi.client.metier.EOWebtaux value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOWebtaux oldValue = webtaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBTAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBTAUX_KEY);
    }
  }
  

  public static EOTransports createTransports(EOEditingContext editingContext, java.math.BigDecimal segKm2
, java.math.BigDecimal segKm3
, java.math.BigDecimal traMontant
, java.math.BigDecimal traMontantKm
, java.math.BigDecimal traMontantPaiement
, String traUtilisateur
, org.cocktail.kiwi.client.metier.EOSegmentTarif segmentTarif, org.cocktail.kiwi.client.metier.EOTypeTransport typeTransport) {
    EOTransports eo = (EOTransports) createAndInsertInstance(editingContext, _EOTransports.ENTITY_NAME);    
		eo.setSegKm2(segKm2);
		eo.setSegKm3(segKm3);
		eo.setTraMontant(traMontant);
		eo.setTraMontantKm(traMontantKm);
		eo.setTraMontantPaiement(traMontantPaiement);
		eo.setTraUtilisateur(traUtilisateur);
    eo.setSegmentTarifRelationship(segmentTarif);
    eo.setTypeTransportRelationship(typeTransport);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTransports.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTransports.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOTransports localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTransports)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTransports localInstanceIn(EOEditingContext editingContext, EOTransports eo) {
    EOTransports localInstance = (eo == null) ? null : (EOTransports)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTransports#localInstanceIn a la place.
   */
	public static EOTransports localInstanceOf(EOEditingContext editingContext, EOTransports eo) {
		return EOTransports.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTransports fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTransports fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTransports eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTransports)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTransports fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTransports fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTransports eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTransports)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTransports fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTransports eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTransports ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTransports fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
