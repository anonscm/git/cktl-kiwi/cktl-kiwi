

// EOMission.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.factory.Factory;
import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOMission extends _EOMission {

	public final static EOSortOrdering SORT_DEBUT_DESC = new EOSortOrdering(MIS_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public final static NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_DEBUT_DESC);
	
	public final static String ETAT_ANNULE= "ANNULEE";
	public final static String ETAT_VALIDE = "VALIDE";
	public final static String ETAT_LIQUIDE = "LIQUIDEE";
	public final static String ETAT_PAYE = "PAYEE";
	public final static String ETAT_TERMINE = "TERMINEE";
	public final static String ETAT_CLOTURE = "CLOTUREE";

	public EOMission() {
		super();
	}
	
	public boolean isAnnulee()	{
		return misEtat().equals(ETAT_ANNULE);
	}
	public boolean isValide()	{
		return misEtat().equals(ETAT_VALIDE);
	}
	public boolean isPayee()	{
		return misEtat().equals(ETAT_PAYE) || misEtat().equals(ETAT_LIQUIDE) || misEtat().equals(ETAT_TERMINE);
	}
	public boolean isTerminee()	{
		return misEtat().equals(ETAT_TERMINE);
	}

	/**
	 * 
	 * Mission permanente ==> Pas de controles de dates lors de la saisie
	 * 
	 * @return
	 */
	public boolean isMissionPermanente()	{
		return titreMission().titCtrlDates().intValue() == 0;
	}

	public boolean isMissionPaiement()	{
		return titreMission().titPaiement().intValue() == 1;
	}

	public boolean isSaisieLbud()	{

		return titreMission().titSaisieLbud().intValue() == 1;
	}
	
	public String identiteCreateur() {
		
		if (utilisateurCreation() != null && utilisateurCreation().individu() != null)
			return utilisateurCreation().individu().prenom()+" " + utilisateurCreation().individu().nomUsuel();
		
		return "?";
	}

	/**
	 *  
	 * @param ec
	 * @param fournis
	 * @param exercice
	 * @return
	 */
	public static NSArray findMissionsForVehicule (EOEditingContext ec, EOVehicule vehicule)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.SEGMENTS_TARIF_KEY+"."+
				EOSegmentTarif.TRANSPORTS_KEY+"."+EOTransports.VEHICULE_KEY + " = %@", new NSArray(vehicule)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " != %@", new NSArray(EOMission.ETAT_ANNULE)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null);
	}


	/**
	 *  
	 * @param ec
	 * @param fournis
	 * @param exercice
	 * @return
	 */
	public static NSArray findMissionsValidesForFournisAndAnnee (EOEditingContext ec, EOFournis fournis, Number exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("misEtat != %@", new NSArray(EOMission.ETAT_ANNULE)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("misAnnee = %@", new NSArray(exercice)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null);
	}

	/**
	 * 
	 *
	 */
	public static NSArray findMissionsForFournisAndPeriode (EOEditingContext ec, EOFournis fournis, NSTimestamp debut, NSTimestamp fin, EOMission missionToExclude)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (missionToExclude != null)			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_NUMERO_KEY + " != %@", new NSArray(missionToExclude.misNumero())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("titreMission.titCtrlDates = 1", null));		

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " != %@", new NSArray(EOMission.ETAT_ANNULE)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.FOURNIS_KEY + " = %@", new NSArray(fournis)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_DEBUT_KEY + " <= %@", new NSArray(fin)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_FIN_KEY + " >= %@", new NSArray(debut)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null);

	}



	/**
	 * 
	 * @param ec
	 * @param fournis
	 * @return
	 */
	public static EOMission findMissionForKey(EOEditingContext ec, Number key)	{

		try {
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("misOrdre = %@", new NSArray(key));
			return fetchFirstByQualifier(ec, myQualifier);
		}
		catch (Exception e) {return null;}
	}



	/**
	 * 
	 * @param ec
	 * @param fournis
	 * @return
	 */
	public static EOMission findLastMissionForFournis(EOEditingContext ec, EOFournis fournis)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOMission.MIS_FIN_KEY, EOSortOrdering.CompareDescending));

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMission.FOURNIS_KEY + " = %@", new NSArray(fournis));

		EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME,myQualifier, mySort);

		try {return (EOMission)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static Number findDernierNumeroMission(EOEditingContext ec, EOExercice exercice)	{

		try {

			NSArray sort = new NSArray(new EOSortOrdering(EOMission.MIS_NUMERO_KEY, EOSortOrdering.CompareDescending));

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_NUMERO_KEY + " != nil", null));

			// mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY+ " != %@", new NSArray(EOMission.ETAT_ANNULE )));

			EOFetchSpecification	fs = new EOFetchSpecification(EOMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort);

			fs.setRefreshesRefetchedObjects(true);

			NSArray missions = ec.objectsWithFetchSpecification(fs);

			//			Number avantDernierNumero = null;
			//			
			//			if (missions.count() > 1)
			//				avantDernierNumero = ((EOMission)missions.objectAtIndex(1)).misNumero();

			Number dernierNumero = ((EOMission)missions.objectAtIndex(0)).misNumero();

			//			// On teste qu'il n'y ait pas plus de 5 numeros entre les 2 missions
			//			if (avantDernierNumero != null && dernierNumero != null) {
			//				
			//				if (dernierNumero.intValue() > (avantDernierNumero.intValue()+5) )
			//					MsgPanel.sharedInstance().runErrorDialog("ATTENTION","Erreur de numérotation. Contacter le responsable KIWI !");
			//				
			//			}

			if (missions.count() > 0)
				return dernierNumero;
			else 
				return (new Integer(0));
		}	
		catch (Exception e)	{e.printStackTrace();return null;}
	}


	public static NSArray findMissionsValidesForExercice(EOEditingContext ec, EOExercice exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MIS_ETAT_KEY + "=%@", new NSArray(EOMission.ETAT_VALIDE)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_EXERCICE_KEY + "=%@", new NSArray(exercice)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null);
	}

	
	/**
	 * 
	 * @param ec
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOMission> findMissionsValidesBeforeDate(EOEditingContext ec, NSTimestamp dateReference)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MIS_ETAT_KEY + "=%@", new NSArray(EOMission.ETAT_VALIDE)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MIS_FIN_KEY + "<=%@", new NSArray(dateReference)));
		
		return fetchAll(ec, new EOAndQualifier(mesQualifiers), SORT_ARRAY_DEBUT_DESC);
	}

	
	/**
	 * 
	 * @param ec
	 * @param fournis
	 */
	public static NSArray findMissionsValidesForFournisAvecVP(EOEditingContext ec, EOFournis fournis)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MIS_ETAT_KEY + "=%@", new NSArray(EOMission.ETAT_VALIDE)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(FOURNIS_KEY + " = %@", new NSArray(fournis)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEGMENTS_TARIF_KEY+"."+EOSegmentTarif.TRANSPORTS_KEY+"."+EOTransports.TYPE_TRANSPORT_KEY+"."+EOTypeTransport.TTR_TYPE_KEY + "=%@", new NSArray(new NSArray("VP"))));

		EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);

		return ec.objectsWithFetchSpecification(fs);
	}


	/**
	 * 
	 * @param ec
	 * @param fournis
	 */
	public static EOMission findMissionForRefresh(EOEditingContext ec, EOMission mission)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		Number misordre = (Number)ServerProxy.clientSideRequestPrimaryKeyForObject(ec, mission).objectForKey("misOrdre");

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("misOrdre = %@", new NSArray(misordre)));

		//		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toExercice = %@", new NSArray(mission.exercice())));
		//
		//		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toExerciceOrigine = %@", new NSArray(mission.exerciceOrigine())));

		EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);

		try {return (EOMission)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}
	}

	
	/**
	 * 
	 * @param ec
	 * @param titre
	 * @param agent
	 * @param exercice
	 * @return
	 */
	public static EOMission creerMission (
			EOEditingContext ec,
			NSTimestamp dateDebut,
			NSTimestamp dateFin, 
			EOTitreMission titre, 
			EOUtilisateur utilisateur,
			EOExercice exercice
	)	{
		
		EOMission mission = (EOMission)Factory.instanceForEntity(ec, EOMission.ENTITY_NAME);
			
		mission.setTitreMissionRelationship(titre);
		mission.setUtilisateurCreationRelationship(utilisateur);		
		mission.setUtilisateurModificationRelationship(utilisateur);		

		mission.setToExerciceRelationship(exercice);

		mission.setMisDebut(dateDebut);
		mission.setMisFin(dateFin);
		
		mission.setMisEtat("VALIDE");
		
		mission.setNumQuotientRemb(new Integer(1));
		mission.setDenQuotientRemb(new Integer(1));

		mission.setMisCreation(new NSTimestamp());
		
		ec.insertObject(mission);
		
		return mission;
	}
	
/**
 * Duplication d'une mission pour un nouveau fournisseur
 * 
 * @param ec
 * @param missionReference
 * @param fournis
 * @return		Retourne la nouvelle mission inseree dans l'editing context
 */
    public static EOMission dupliquerMission(EOEditingContext ec, EOMission missionReference, NSTimestamp debut, NSTimestamp fin, EOFournis fournis) throws NSValidation.ValidationException 	{

    	// Le missionnaire est il deja en mission a ces dates
		NSArray missions = EOMission.findMissionsForFournisAndPeriode(ec, fournis, debut, fin, null);
		if (missions.count() > 0)		
			throw new NSValidation.ValidationException("Agent déjà en mission");  

		// Recuperation de l'exercice de la mission en fonction des dates.
		Number exerciceDebut = new Integer(DateCtrl.dateToString(debut, "%Y"));
		EOExercice exerciceMission = FinderExercice.findExercice(ec, exerciceDebut);

    	EOMission mission = creerMission(ec, debut, fin,
    			missionReference.titreMission(), missionReference.utilisateurCreation(), exerciceMission);

		Number newNumero = ServerProxy.clientSideRequestgetNumeroMission(ec, mission.toExercice().exeExercice());
		mission.setMisNumero(new Integer(newNumero.intValue()));

    	// Fournisseur + Corps et Residence
		mission.addObjectToBothSidesOfRelationshipWithKey(fournis, EOMission.FOURNIS_KEY);

		EOMission lastMission = EOMission.findLastMissionForFournis(ec, fournis);
		
		if (lastMission != null)	{		
			mission.setMisResidence(lastMission.misResidence());

			if (lastMission.corps() != null)
				mission.addObjectToBothSidesOfRelationshipWithKey(lastMission.corps(), EOMission.CORPS_KEY);

			if (lastMission.misCorps() != null)
				mission.setMisCorps(lastMission.misCorps());
		}
		else	{
			
			mission.setMisResidence(missionReference.misResidence());

			// si on a gepeto, on recupere (si possible) le corps du missionnaire ...
			EOCorps c = EOCorps.getCorpsFromMangue(ec, fournis);
			if (c != null) {
				mission.addObjectToBothSidesOfRelationshipWithKey(c, EOMission.CORPS_KEY);
				mission.setMisCorps(c.lcCorps());
			}
		}		

    	// Payeur
    	mission.addObjectToBothSidesOfRelationshipWithKey(missionReference.payeur(), EOMission.PAYEUR_KEY);
    	
    	// Motifs, obs, type
    	mission.setMisMotif(missionReference.misMotif());
    	mission.setMisObservation(missionReference.misObservation());
    	mission.setMisPayeur(missionReference.misPayeur());
    	
    	mission.setNumQuotientRemb(missionReference.numQuotientRemb());
    	mission.setDenQuotientRemb(missionReference.denQuotientRemb());
    	
    	return mission;
    }

    public static boolean controleDatesTrajets(EOMission mission) {
    	
    	NSArray trajets = mission.segmentsTarif();
    	
    	for (int i=0;i<trajets.count();i++) {
    		
    		EOSegmentTarif localTrajet = (EOSegmentTarif)trajets.objectAtIndex(i);
    		
    		if (DateCtrl.isBefore(localTrajet.segDebut(), mission.misDebut()))
    			return false;
    		
    		if (DateCtrl.isAfter(localTrajet.segFin(), mission.misFin()))
    			return false;

    	}

    	return true;
    		
    }
    

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {





	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}
