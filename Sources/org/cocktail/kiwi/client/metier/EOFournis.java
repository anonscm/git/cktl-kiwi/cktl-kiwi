

// EOFournis.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.client.finders.FinderRibfour;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOFournis extends _EOFournis {

    public EOFournis() {
        super();
    }

    public String identite()	{

    	return prenom() + " " + nom();
    	
    }
    
    public String residence() {    	
    	return (toAdresse() != null)? toAdresse().ville() : "";
    }
    
    public String ribExistant() {    	
    	NSArray<EORibfour> ribs = FinderRibfour.findRibsValidesForFournis(editingContext(), this);
    	return (ribs.size() > 0)? "O":"N" ;
    }
    

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
