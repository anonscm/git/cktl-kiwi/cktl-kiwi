

// EOWebmon.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOWebmon extends _EOWebmon {

	public static String DEFAULT_MONNAIE = "EUR";
	
    public EOWebmon() {
        super();
    }

    public static EOWebmon findDefault(EOEditingContext edc) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(WMO_CODE_KEY + "=%@", new NSArray(DEFAULT_MONNAIE));
    	return fetchFirstByQualifier(edc, qualifier); 
    }
    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
