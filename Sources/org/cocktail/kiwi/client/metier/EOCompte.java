

// EOCompte.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOCompte extends _EOCompte {

    public EOCompte() {
        super();
    }

    public static EOCompte compteForPersId(EOEditingContext ec, Integer persId) {
    	
    		NSMutableArray qualifiers = new NSMutableArray();

    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@", new NSArray(persId)));

    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_EMAIL_KEY + " != nil", null));

    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_DOMAINE_KEY + " != nil", null));

    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_VLAN_KEY + " != nil", null));
    		
    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_VLAN_KEY + " != %@", new NSArray("E")));

    		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
    	
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
