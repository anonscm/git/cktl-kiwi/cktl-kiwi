

// EOSegGroupeInfo.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.kiwi.client.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOSegGroupeInfo extends _EOSegGroupeInfo {

	public static NSArray SORT_ETAT_DESC = new NSArray(new EOSortOrdering(STG_ETAT_KEY, EOSortOrdering.CompareDescending));
	public static NSArray SORT_LIBELLE_ASC = new NSArray(new EOSortOrdering(STG_LIBELLE_KEY, EOSortOrdering.CompareAscending));
	
    public EOSegGroupeInfo() {
        super();
    }

	public String toString()	{
		return stgLibelle();
	}
	
	public boolean estValide() {
		return stgEtat().equals("VALIDE");
	}
	
	public boolean estUneAvance()	{
		return ("AVANCES".equals(stgLibelle()));
	}

	public static NSArray findGroupesInfo(EOEditingContext ec)	{		
		NSMutableArray sorts = new NSMutableArray(new EOSortOrdering(STG_LIBELLE_KEY, EOSortOrdering.CompareAscending));		
		return fetchAll(ec, null, sorts);
	}

	public static NSArray findGroupesInfoValides(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegGroupeInfo.STG_ETAT_KEY + " = 'VALIDE'",null);
		return fetchAll(ec, myQualifier, SORT_LIBELLE_ASC);
	}

	
	public static EOSegGroupeInfo creer(EOEditingContext ec) {
			
		EOSegGroupeInfo object = (EOSegGroupeInfo)Factory.instanceForEntity(ec, EOSegGroupeInfo.ENTITY_NAME);
		
		object.setStgEtat("VALIDE");
						
		ec.insertObject(object);
		return object;
		
	}	


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
