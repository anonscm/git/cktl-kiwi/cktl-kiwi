// _EOIndemnite.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndemnite.java instead.
package org.cocktail.kiwi.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOIndemnite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Indemnite";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.INDEMNITE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "indOrdre";

	public static final String IND_ETAT_KEY = "indEtat";
	public static final String IND_JOURS_KEY = "indJours";
	public static final String IND_JOURS_AUTO_KEY = "indJoursAuto";
	public static final String IND_JOURS_GRATUITS_KEY = "indJoursGratuits";
	public static final String IND_MONTANT_AUTO_KEY = "indMontantAuto";
	public static final String IND_MONTANT_PAIEMENT_KEY = "indMontantPaiement";
	public static final String IND_MONTANT_SAISI_KEY = "indMontantSaisi";
	public static final String IND_MONTANT_TOTAL_KEY = "indMontantTotal";
	public static final String IND_NB_NUITS_KEY = "indNbNuits";
	public static final String IND_NB_REPAS_KEY = "indNbRepas";
	public static final String IND_NUITS_AUTO_KEY = "indNuitsAuto";
	public static final String IND_NUITS_GRATUITES_KEY = "indNuitsGratuites";
	public static final String IND_REPAS_AUTO_KEY = "indRepasAuto";
	public static final String IND_REPAS_GRATUITS_KEY = "indRepasGratuits";

//Colonnes dans la base de donnees
	public static final String IND_ETAT_COLKEY = "IND_ETAT";
	public static final String IND_JOURS_COLKEY = "IND_JOURS";
	public static final String IND_JOURS_AUTO_COLKEY = "IND_JOURS_AUTO";
	public static final String IND_JOURS_GRATUITS_COLKEY = "IND_JOURS_GRATUITS";
	public static final String IND_MONTANT_AUTO_COLKEY = "IND_MONTANT_AUTO";
	public static final String IND_MONTANT_PAIEMENT_COLKEY = "IND_MONTANT_PAIEMENT";
	public static final String IND_MONTANT_SAISI_COLKEY = "IND_MONTANT_SAISI";
	public static final String IND_MONTANT_TOTAL_COLKEY = "IND_MONTANT_TOTAL";
	public static final String IND_NB_NUITS_COLKEY = "IND_NB_NUITS";
	public static final String IND_NB_REPAS_COLKEY = "IND_NB_REPAS";
	public static final String IND_NUITS_AUTO_COLKEY = "IND_NUITS_AUTO";
	public static final String IND_NUITS_GRATUITES_COLKEY = "IND_NUITS_GRATUITES";
	public static final String IND_REPAS_AUTO_COLKEY = "IND_REPAS_AUTO";
	public static final String IND_REPAS_GRATUITS_COLKEY = "IND_REPAS_GRATUITS";

// Relationships
	public static final String SEGMENT_TARIF_KEY = "segmentTarif";
	public static final String WEBMISS_KEY = "webmiss";



	// Accessors methods
  public String indEtat() {
    return (String) storedValueForKey(IND_ETAT_KEY);
  }

  public void setIndEtat(String value) {
    takeStoredValueForKey(value, IND_ETAT_KEY);
  }

  public java.math.BigDecimal indJours() {
    return (java.math.BigDecimal) storedValueForKey(IND_JOURS_KEY);
  }

  public void setIndJours(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IND_JOURS_KEY);
  }

  public java.math.BigDecimal indJoursAuto() {
    return (java.math.BigDecimal) storedValueForKey(IND_JOURS_AUTO_KEY);
  }

  public void setIndJoursAuto(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IND_JOURS_AUTO_KEY);
  }

  public java.math.BigDecimal indJoursGratuits() {
    return (java.math.BigDecimal) storedValueForKey(IND_JOURS_GRATUITS_KEY);
  }

  public void setIndJoursGratuits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IND_JOURS_GRATUITS_KEY);
  }

  public java.math.BigDecimal indMontantAuto() {
    return (java.math.BigDecimal) storedValueForKey(IND_MONTANT_AUTO_KEY);
  }

  public void setIndMontantAuto(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IND_MONTANT_AUTO_KEY);
  }

  public java.math.BigDecimal indMontantPaiement() {
    return (java.math.BigDecimal) storedValueForKey(IND_MONTANT_PAIEMENT_KEY);
  }

  public void setIndMontantPaiement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IND_MONTANT_PAIEMENT_KEY);
  }

  public java.math.BigDecimal indMontantSaisi() {
    return (java.math.BigDecimal) storedValueForKey(IND_MONTANT_SAISI_KEY);
  }

  public void setIndMontantSaisi(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IND_MONTANT_SAISI_KEY);
  }

  public java.math.BigDecimal indMontantTotal() {
    return (java.math.BigDecimal) storedValueForKey(IND_MONTANT_TOTAL_KEY);
  }

  public void setIndMontantTotal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IND_MONTANT_TOTAL_KEY);
  }

  public Integer indNbNuits() {
    return (Integer) storedValueForKey(IND_NB_NUITS_KEY);
  }

  public void setIndNbNuits(Integer value) {
    takeStoredValueForKey(value, IND_NB_NUITS_KEY);
  }

  public Integer indNbRepas() {
    return (Integer) storedValueForKey(IND_NB_REPAS_KEY);
  }

  public void setIndNbRepas(Integer value) {
    takeStoredValueForKey(value, IND_NB_REPAS_KEY);
  }

  public Integer indNuitsAuto() {
    return (Integer) storedValueForKey(IND_NUITS_AUTO_KEY);
  }

  public void setIndNuitsAuto(Integer value) {
    takeStoredValueForKey(value, IND_NUITS_AUTO_KEY);
  }

  public Integer indNuitsGratuites() {
    return (Integer) storedValueForKey(IND_NUITS_GRATUITES_KEY);
  }

  public void setIndNuitsGratuites(Integer value) {
    takeStoredValueForKey(value, IND_NUITS_GRATUITES_KEY);
  }

  public Integer indRepasAuto() {
    return (Integer) storedValueForKey(IND_REPAS_AUTO_KEY);
  }

  public void setIndRepasAuto(Integer value) {
    takeStoredValueForKey(value, IND_REPAS_AUTO_KEY);
  }

  public Integer indRepasGratuits() {
    return (Integer) storedValueForKey(IND_REPAS_GRATUITS_KEY);
  }

  public void setIndRepasGratuits(Integer value) {
    takeStoredValueForKey(value, IND_REPAS_GRATUITS_KEY);
  }

  public org.cocktail.kiwi.client.metier.EOSegmentTarif segmentTarif() {
    return (org.cocktail.kiwi.client.metier.EOSegmentTarif)storedValueForKey(SEGMENT_TARIF_KEY);
  }

  public void setSegmentTarifRelationship(org.cocktail.kiwi.client.metier.EOSegmentTarif value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOSegmentTarif oldValue = segmentTarif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SEGMENT_TARIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SEGMENT_TARIF_KEY);
    }
  }
  
  public org.cocktail.kiwi.client.metier.EOWebmiss webmiss() {
    return (org.cocktail.kiwi.client.metier.EOWebmiss)storedValueForKey(WEBMISS_KEY);
  }

  public void setWebmissRelationship(org.cocktail.kiwi.client.metier.EOWebmiss value) {
    if (value == null) {
    	org.cocktail.kiwi.client.metier.EOWebmiss oldValue = webmiss();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBMISS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBMISS_KEY);
    }
  }
  

  public static EOIndemnite createIndemnite(EOEditingContext editingContext, java.math.BigDecimal indJours
, java.math.BigDecimal indJoursAuto
, java.math.BigDecimal indJoursGratuits
, java.math.BigDecimal indMontantAuto
, java.math.BigDecimal indMontantPaiement
, java.math.BigDecimal indMontantTotal
, Integer indNbNuits
, Integer indNbRepas
, Integer indNuitsAuto
, Integer indNuitsGratuites
, Integer indRepasAuto
, Integer indRepasGratuits
) {
    EOIndemnite eo = (EOIndemnite) createAndInsertInstance(editingContext, _EOIndemnite.ENTITY_NAME);    
		eo.setIndJours(indJours);
		eo.setIndJoursAuto(indJoursAuto);
		eo.setIndJoursGratuits(indJoursGratuits);
		eo.setIndMontantAuto(indMontantAuto);
		eo.setIndMontantPaiement(indMontantPaiement);
		eo.setIndMontantTotal(indMontantTotal);
		eo.setIndNbNuits(indNbNuits);
		eo.setIndNbRepas(indNbRepas);
		eo.setIndNuitsAuto(indNuitsAuto);
		eo.setIndNuitsGratuites(indNuitsGratuites);
		eo.setIndRepasAuto(indRepasAuto);
		eo.setIndRepasGratuits(indRepasGratuits);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOIndemnite.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOIndemnite.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOIndemnite localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndemnite)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOIndemnite localInstanceIn(EOEditingContext editingContext, EOIndemnite eo) {
    EOIndemnite localInstance = (eo == null) ? null : (EOIndemnite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOIndemnite#localInstanceIn a la place.
   */
	public static EOIndemnite localInstanceOf(EOEditingContext editingContext, EOIndemnite eo) {
		return EOIndemnite.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOIndemnite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOIndemnite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndemnite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndemnite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndemnite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndemnite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndemnite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndemnite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOIndemnite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndemnite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndemnite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndemnite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
