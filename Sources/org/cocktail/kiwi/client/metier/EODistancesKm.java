

// EODistancesKm.java
// 
package org.cocktail.kiwi.client.metier;


import java.math.BigDecimal;

import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.foundation.NSValidation;

public class EODistancesKm extends _EODistancesKm {

    public EODistancesKm() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      
		if (StringCtrl.chaineVide(lieuDepart()))
			throw new ValidationException("Veuillez renseigner un lieu de départ !");
			
		if (StringCtrl.chaineVide(lieuArrivee()))
			throw new ValidationException("Veuillez renseigner un lieu d'arrivée !");

		try {
			BigDecimal testDistance = new BigDecimal(distance().toString());			
		}
		catch (Exception ex) {
			throw new ValidationException("Veuillez vérifier le format du nombre de kilomètres saisi !");			
		}


    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
