

// EOFonction.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOFonction extends _EOFonction {

	public static final String FON_ID_INTERNE_ACTIONS_VOTEES = "DEAUTACT";
	public static final String FON_ID_INTERNE_ENGAGEMENT_RESTREINT = "DEENGINV";

	// FONCTIONS 
	public static final String ID_FCT_LIQUIDATION = "KLIQUIDE";
	public static final String ID_FCT_LIQUIDATION_AVANCES = "KLIQAV";
	public static final String ID_FCT_CREATION = "KCREAT";
	public static final String ID_FCT_DEL_DEPENSE = "KDELDEP";
	public static final String ID_FCT_REIMPUTATION = "KREIMPUT";
	public static final String ID_FCT_PARAMS_APP = "KPARAPP";
	public static final String ID_FCT_PARAMS_BUD = "KPARBUD";
	public static final String ID_FCT_TAUX_CHANCELLERIE = "KCHANCEL";
	public static final String ID_FCT_PREMISSION = "KPREMIS";
	public static final String ID_FCT_SIGNATAIRES = "KSIGNAT";
	public static final String ID_FCT_NUMEROTATION = "KNUMERO";
	public static final String ID_FCT_REVALIDATION = "KREVALID";
	public static final String ID_FCT_EXERCICE = "KEXER";
	public static final String ID_FCT_SOLDE_ENG = "KSOLDENG";
	public static final String ID_FCT_CAP = "KCAP";

    public EOFonction() {
        super();
    }

    public String toString()	{
    	return fonLibelle();
    }

	public static NSArray findForTypeApplication(EOEditingContext ec, Number tyapId)	{

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.TYAP_ID_KEY + "=%@", new NSArray(tyapId));
		
		return fetchAll(ec, qual, null, true);
		
	}
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
