

// EOParamTypeCredit.java
// 
package org.cocktail.kiwi.client.metier;


import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOParamTypeCredit extends _EOParamTypeCredit {

    public EOParamTypeCredit() {
        super();
    }

	public static NSArray findParamsTypeCredit(EOEditingContext ec, EOExercice exercice)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(TO_EXERCICE_KEY + "=%@", new NSArray(exercice));
		EOFetchSpecification fs = new EOFetchSpecification(EOParamTypeCredit.ENTITY_NAME,myQualifier, null);
		
		return ec.objectsWithFetchSpecification(fs);
}

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
