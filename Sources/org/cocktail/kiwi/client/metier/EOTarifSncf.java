

// EOTarifSncf.java
// 
package org.cocktail.kiwi.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTarifSncf extends _EOTarifSncf {

    public EOTarifSncf() {
        super();
    }
   
    public String toString() {
        return "Classe "+classe()+" ("+dtDebut()+"-"+dtFin()+") - " + prixKm() + " € / Km";
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
