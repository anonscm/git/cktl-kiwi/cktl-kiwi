package org.cocktail.kiwi.client.select;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOParamModePaiement;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class ModePaiementSelectCtrl  {

	
	private static ModePaiementSelectCtrl sharedInstance;
	private EOEditingContext ec;
	
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionSelect 		actionSelect = new ActionSelect();

	protected JPanel viewTable;
	

	/**
	 * 
	 *
	 */
	public ModePaiementSelectCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		
		initGUI();
		initView();
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ModePaiementSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ModePaiementSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	private void initView()	{
		
        mainWindow = new JDialog(mainFrame, "Modes de Paiement", true);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionSelect);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 115, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(400, 375));
		mainView.add(viewTable, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}
		

	
/**
 * 
 * Selection de types de credit parmi une liste proposee
 * Cette liste correspond aux types de credit DEPENSE et EXECUTOIRE
 * 
 * @param exercice
 * @param filtre
 * @return NSArray des typesCredit selectionnes
 */
	public NSArray getModesPaiement(EOExercice exercice, boolean filtre)	{
		
    	myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		if (!filtre)	
			eod.setObjectArray(EOModePaiement.findModesPaiements(ec, exercice));
		else	{
			
			NSArray params = EOParamModePaiement.findForExercice(ec, exercice);
			if (params.count() == 0)
				eod.setObjectArray(EOModePaiement.findModesPaiements(ec, exercice));
			else
				eod.setObjectArray((NSArray)params.valueForKey("modePaiement"));			
		}
			
		myEOTable.updateData();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.setVisible(true);
		
		if (eod.selectedObjects().count() > 0)
			return  eod.selectedObjects();			
		
		return null;
	}

/**
 * Selection d'UN SEUL mode de paiement parmi une liste proposee
 * Cette liste correspond aux modes de paiement

 * @param exercice
 * @param filtre
 * @return 1 objet EOTypeCredit
 */	
	public EOModePaiement getModePaiement(EOExercice exercice, boolean filtre)	{
		
		mainWindow.setTitle("Modes de Paiement (" + exercice.exeExercice() + " )");
    	myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		if (!filtre)	
			eod.setObjectArray(EOModePaiement.findModesPaiements(ec, exercice));
		else	{
			NSArray params = EOParamModePaiement.findForExercice(ec, exercice);
			if (params.count() == 0)
				eod.setObjectArray(EOModePaiement.findModesPaiements(ec, exercice));
			else {
    			if (params.count() == 1)
    				return ((EOParamModePaiement)params.objectAtIndex(0)).modePaiement();
    			else
    				eod.setObjectArray((NSArray)params.valueForKey(EOParamModePaiement.MODE_PAIEMENT_KEY));			
			}
		}
			
		myEOTable.updateData();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.setVisible(true);
		
		if (eod.selectedObject() != null)
			return  (EOModePaiement)eod.selectedObject();			
		
		return null;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void initGUI() {
		
		eod = new EODisplayGroup();
		
		eod.setSortOrderings(new NSArray(new EOSortOrdering(EOModePaiement.MOD_CODE_KEY, EOSortOrdering.CompareAscending)));
		
        viewTable = new JPanel();
		
		initTableModel();
		initTable();
				
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerModePaiement());
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

	}
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EOModePaiement.MOD_CODE_KEY, "Code", 50);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOModePaiement.MOD_LIBELLE_KEY, "Libellé", 300);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOModePaiement.MOD_DOM_KEY, "TYPE", 75);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

	    public ActionSelect() {
            super("Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	mainWindow.dispose();
        }  
	} 
	
    
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CANCEL);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	myTableModel.fireTableDataChanged();
        	mainWindow.dispose();
        }  
	} 

	/**
	 * 
	 * @author cpinsard
	 *
	 */
		   private class ListenerModePaiement implements ZEOTableListener {

			/* (non-Javadoc)
			 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
			 */
			public void onDbClick() {
				mainWindow.dispose();
			}

			/* (non-Javadoc)
			 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
			 */
			public void onSelectionChanged() {
			}
		   }	   

}
