package org.cocktail.kiwi.client.select;

import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderRepartPersonneAdresse;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOParametres;
import org.cocktail.kiwi.client.metier.EORepartPersonneAdresse;
import org.cocktail.kiwi.client.nibctrl.ResidenceSelectView;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ResidenceSelectCtrl 
{
	private static ResidenceSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	public 	EODisplayGroup		eod;
	
	private ResidenceSelectView myView;
	
	private NSDictionary currentObject;

	/** 
	 *
	 */
	public ResidenceSelectCtrl (EOEditingContext editingContext) {

		super();

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new ResidenceSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new Listener());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ResidenceSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ResidenceSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public String getResidence(EOFournis fournis, EOMission currentMission)	{
		
		NSMutableArray myResidences = new NSMutableArray();
		NSMutableDictionary myDico = new NSMutableDictionary();

		// Derniere mission
		
		EOMission lastMission = EOMission.findLastMissionForFournis(ec, fournis);

		if (lastMission != null)	{
		
			myDico.setObjectForKey("Résidence de la dernière mission","TYPE");
			myDico.setObjectForKey(lastMission.misResidence(),"LIBELLE");
			
			myResidences.addObject(myDico);
		}
		
		// Adresse personnelle

		EORepartPersonneAdresse adressePerso = FinderRepartPersonneAdresse.findAdresse(ec, fournis.persId(), "PERSO");
		
		if (adressePerso != null && adressePerso.adresse().ville() != null)	{
			
			myDico = new NSMutableDictionary();
			
			myDico.setObjectForKey("Résidence de l'adresse Personnelle","TYPE");
			myDico.setObjectForKey(adressePerso.adresse().ville(),"LIBELLE");
			
			myResidences.addObject(myDico);
		}

		// Adresse professionnelle

		EORepartPersonneAdresse adressePro = FinderRepartPersonneAdresse.findAdresse(ec, fournis.persId(), "PRO");

		if (adressePro != null && adressePro.adresse().ville() != null)	{
			
			myDico = new NSMutableDictionary();

			myDico.setObjectForKey("Résidence de l'adresse Professionnelle","TYPE");
			myDico.setObjectForKey(adressePro.adresse().ville(),"LIBELLE");
			
			myResidences.addObject(myDico);
		}
		
		// Payeur
		
		if (currentMission.payeur() != null)	{

			if (currentMission.payeur().ville() != null)	{
				
				myDico = new NSMutableDictionary();

				myDico.setObjectForKey("Résidence associée au payeur de la mission","TYPE");
				myDico.setObjectForKey(currentMission.payeur().ville(),"LIBELLE");
				
				myResidences.addObject(myDico);
			}
		}
		
		//
		String paramVille = EOParametres.getValue(ec, currentMission.toExercice(), "VILLE");
		if (paramVille != null)	{

				myDico = new NSMutableDictionary();

				myDico.setObjectForKey("Paramétrage JEFYCO","TYPE");
				myDico.setObjectForKey(paramVille,"LIBELLE");
				
				myResidences.addObject(myDico);
		}
		
		eod.setObjectArray(myResidences);
		myView.getMyEOTable().updateData();
		
		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(true);
		myView.setVisible(true);
		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(false);
		
		if (currentObject != null)
			return (String)currentObject.objectForKey("LIBELLE");			
		
		return null;
	}
	
	/**
	 *
	 */
	public void annuler()	{
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}
	

	   private class Listener implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentObject = (NSDictionary)eod.selectedObject();
		}
	   }

	   
	public void windowClosing(WindowEvent e)	{}

}