/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.finders.FinderConvention;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ConventionSelectCtrl  {

	
	private static ConventionSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private ConventionSelectView myView;
	
	private EODisplayGroup eod = new EODisplayGroup();
	
	private EOConvention currentConvention;


	/**
	 * 
	 *
	 */
	public ConventionSelectCtrl(EOEditingContext editingContext)	{

		super();
		ec = editingContext;
		
		myView = new ConventionSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerConvention());
		
		myView.getTfFindIndex().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindObjet().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindReference().getDocument().addDocumentListener(new ADocumentListener());

		
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ConventionSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ConventionSelectCtrl(editingContext);
		return sharedInstance;
	}

		
	
	/**
	 * 
	 * @return
	 */
	public EOConvention getConvention(EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit) 	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		NSMutableArray conventions = new NSMutableArray(FinderConvention.findConventionsLimitatives(ec,exercice, organ, typeCredit));

		conventions.addObjectsFromArray(FinderConvention.findConventionsNonLimitatives(ec,exercice, organ, typeCredit));

		eod.setObjectArray(conventions);
			
		if (eod.displayedObjects().count() == 0)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Aucune convention n'a pu être trouvée pour cette ligne budgétaire et ce type de crédit !");
			return null;
		}
		
		filter();
		
		myView.show();
		
		return currentConvention;
		
	}

    
	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFindIndex().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindIndex().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONV_INDEX_KEY + " caseInsensitiveLike %@",args));
		}
		
		if (!StringCtrl.chaineVide(myView.getTfFindObjet().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindObjet().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONV_OBJET_KEY + " caseInsensitiveLike %@",args));
		}
		
		if (!StringCtrl.chaineVide(myView.getTfFindReference().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindReference().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONV_REFERENCE_EXTERNE_KEY + " caseInsensitiveLike %@",args));
		}
		
		return new EOAndQualifier(mesQualifiers);        
	}

    
    /** 
    *
    */
	private void filter()	{
       		
		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

   }
	

	    
	/**
	 * 
	 */
	public void annuler() {
		
		currentConvention = null;
		
		eod.setSelectionIndexes(new NSArray());
		
		myView.dispose();
		
	}    

	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerConvention implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			
			currentConvention = (EOConvention)eod.selectedObject();
			
		}
	   }

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
