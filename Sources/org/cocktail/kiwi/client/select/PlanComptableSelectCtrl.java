package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.finders.FinderPlanComptable;
import org.cocktail.kiwi.client.metier.EOParamImputation;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class PlanComptableSelectCtrl  {

	
	private static PlanComptableSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private PlanComptableSelectView myView;
	
	private EODisplayGroup eod  = new EODisplayGroup();
	
	private EOPlanComptable currentPlanco;

	/**
	 * 
	 *
	 */
	public PlanComptableSelectCtrl(EOEditingContext editingContext)	{

		super();
		
		ec = editingContext;
		
		myView = new PlanComptableSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerPlanco());
		
		myView.getTfFindCode().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PlanComptableSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PlanComptableSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	
	/**
	 * 
	 * Selection unique d'un PlanComptable
	 * 
	 * @param classes		// Filtre sur les types de compte (6, 4, etc ...)
	 * @return EOplanComptable
	 */
	public EOPlanComptable getPlanComptable(NSArray classes)	{
		
		eod.setObjectArray(FinderPlanComptable.findPlancosPourClasses(ec, classes));		

		filter();
		
		myView.show();
		
		return currentPlanco;
		
	}	
    
	
	/**
	 * 
	 * Selection multiple de comptes comptables
	 * 
	 * @param classes		// Filtre sur les types de compte (6, 4, etc ...)
	 * @return EOplanComptable
	 */
	public NSArray getPlansComptable(NSArray classes)	{
		
		eod.setObjectArray(FinderPlanComptable.findPlancosPourClasses(ec, classes));		

		filter();
		
		myView.show();
		
		return eod.selectedObjects();
		
	}
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public NSArray getPlancos(NSArray classes, EOExercice exercice, boolean multiSelection, boolean filtrage)	{

    	if (multiSelection)
    		myView.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    	else
    		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		if (!filtrage)	
			eod.setObjectArray(FinderPlanComptable.findPlancosPourClasses(ec , classes));
		else	 {
			
			NSArray params = EOParamImputation.findParamImputations(ec, exercice);
			if (params.count() == 0)
				eod.setObjectArray(FinderPlanComptable.findPlancosPourClasses(ec , classes));
			else
				eod.setObjectArray((NSArray)params.valueForKey(EOParamImputation.PLAN_COMPTABLE_KEY));			
		}
		
		filter();
		if (eod.displayedObjects().count() == 1)
			return new NSArray((EOPlanComptable)eod.displayedObjects().objectAtIndex(0));

		myView.show();
		
		if (eod.selectedObjects().count() > 0)
			return eod.selectedObjects();			
		
		return null;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public EOPlanComptable getPlanComptable(String classe, EOExercice exercice)	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		NSArray params = EOParamImputation.findParamImputations(ec, exercice);
		if (params.count() == 0)
			eod.setObjectArray(FinderPlanComptable.findComptesForSelection(ec, classe));		
		else
			eod.setObjectArray((NSArray)params.valueForKey("planComptable"));			

		filter();
		
		myView.show();
		
		if (eod.selectedObject() != null)	
			return (EOPlanComptable)eod.selectedObject();
		
		return null;
		
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (!StringCtrl.chaineVide(myView.getTfFindCode().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFindCode().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_LIBELLE_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }
    
    /** 
    *
    */
	private void filter()	{
       
       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();              
       myView.getMyEOTable().updateData();

   }

	
	/**
	 * 
	 */
	private void annuler() {
		
		currentPlanco = null;
		
		eod.setSelectionIndexes(new NSArray());
		
		myView.dispose();
		
	}  
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerPlanco implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentPlanco = (EOPlanComptable)eod.selectedObject();
		}
	   }

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
