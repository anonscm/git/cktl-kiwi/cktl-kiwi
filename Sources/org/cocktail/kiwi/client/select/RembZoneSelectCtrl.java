package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.finders.FinderRembZone;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.nibctrl.RembZoneSelectView;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class RembZoneSelectCtrl 
{
	private static RembZoneSelectCtrl sharedInstance;
	
	public 	EODisplayGroup		eod;	
	private	EOEditingContext	ec;

	private RembZoneSelectView myView;

	private EORembZone currentObject;
	
	
	/** 
	 *
	 */
	public RembZoneSelectCtrl (EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new RembZoneSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new Listener());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static RembZoneSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new RembZoneSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public EORembZone getZoneTrajet()	{

		if (eod.displayedObjects().count() == 0)	{
			eod.setObjectArray(FinderRembZone.findZones(ec));
			myView.getMyEOTable().updateData();
		}
		
		myView.show();	
		
		return currentObject;
	}
	
	
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}  	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class Listener implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EORembZone)eod.selectedObject();
		}
	}
	
}