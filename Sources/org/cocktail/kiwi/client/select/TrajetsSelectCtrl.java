package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.finders.FinderDistanceskm;
import org.cocktail.kiwi.client.metier.EODistancesKm;
import org.cocktail.kiwi.client.nibctrl.TrajetsSelectView;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class TrajetsSelectCtrl  {


	private static TrajetsSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private TrajetsSelectView myView;

	private EODisplayGroup eod;

	private EODistancesKm currentTrajet;

	public TrajetsSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new TrajetsSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

        myView.getFiltreDepart().getDocument().addDocumentListener(new ADocumentListener());
        myView.getFiltreArrivee().getDocument().addDocumentListener(new ADocumentListener());

		myView.getMyEOTable().addListener(new ListenerTrajet());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TrajetsSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TrajetsSelectCtrl(editingContext);
		return sharedInstance;
	}

	
    
	/**
	 * 
	 * @return
	 */
    public EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (!StringCtrl.chaineVide(myView.getFiltreDepart().getText()))	{
            NSArray args = new NSArray("*"+myView.getFiltreDepart().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("lieuDepart caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getFiltreArrivee().getText()))	{
            NSArray args = new NSArray("*"+myView.getFiltreArrivee().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("lieuArrivee caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }
    
    /** 
    *
    */
   public void filter()	{
       
       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();              
       myView.getMyEOTable().updateData();

   }
   
   
	/**
	 * 
	 * @return
	 */
	public EODistancesKm getTrajet()	{

		if (eod.displayedObjects().count() == 0) {

			eod.setObjectArray(FinderDistanceskm.findTrajets(ec));
			myView.getMyEOTable().updateData();

		}

		filter();

		myView.show();

		if (currentTrajet == null)	
			return null;

		return currentTrajet;

	}

	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentTrajet = null;
		myView.dispose();
	}  	


	private class ListenerTrajet implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentTrajet = (EODistancesKm)eod.selectedObject();
		}
	}

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
