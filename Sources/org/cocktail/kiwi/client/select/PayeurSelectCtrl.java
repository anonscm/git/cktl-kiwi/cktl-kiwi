package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.metier.EOPayeur;
import org.cocktail.kiwi.client.nibctrl.PayeurSelectView;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class PayeurSelectCtrl 
{
	private static PayeurSelectCtrl sharedInstance;

	public 	EODisplayGroup		eod;	
	private	EOEditingContext	ec;

	private PayeurSelectView myView;

	private EOPayeur currentObject;


	/**
	 * 
	 * @param editingContext
	 */
	public PayeurSelectCtrl (EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new PayeurSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new Listener());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PayeurSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PayeurSelectCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * @return
	 */
	public EOPayeur getPayeur()	{

		eod.setObjectArray(EOPayeur.findPayeuresValides(ec));
		myView.getMyEOTable().updateData();

		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(true);
		myView.setVisible(true);
		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(false);

		return currentObject;
	}


	/**
	 * 
	 */
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}  	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class Listener implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOPayeur)eod.selectedObject();
		}
	}


	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();


		return new EOAndQualifier(mesQualifiers);        
	}


	private void filter() {

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


}