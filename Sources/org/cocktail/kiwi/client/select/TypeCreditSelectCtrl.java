package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.finders.FinderTypeCredit;
import org.cocktail.kiwi.client.metier.EOParamTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class TypeCreditSelectCtrl  {


	private static TypeCreditSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private TypeCreditSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EOTypeCredit currentTypeCredit;
	private EOExercice currentExercice;


	/**
	 * 
	 *
	 */
	public TypeCreditSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new TypeCreditSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerTypeCredit());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TypeCreditSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TypeCreditSelectCtrl(editingContext);
		return sharedInstance;
	}

	
	/**
	 * Selection d'UN SEUL type de credit parmi une liste proposee
	 * Cette liste correspond aux types de credit DEPENSE et EXECUTOIRE

	 * @param exercice
	 * @param filtre
	 * @return 1 objet EOTypeCredit
	 */	
	public EOTypeCredit getTypeCredit(EOExercice exercice, boolean filtre)	{

		// On recharge les donnees seulement si le ctrl est lance pour la premiere fois ou si on a change d'exercice budgetaire
		if (currentExercice == null || (currentExercice.exeExercice() != exercice.exeExercice()) || eod.displayedObjects().count() == 0) {
			
			currentExercice = exercice;
			myView.setTitle("Sélection d'un type de crédit ("+currentExercice.exeExercice()+")");		

			if (!filtre)	
				eod.setObjectArray(FinderTypeCredit.findTypesCreditDepExecutoire(ec, exercice));
			else	 {
				
				NSArray params = EOParamTypeCredit.findParamsTypeCredit(ec, exercice);
				if (params.count() == 0)
					eod.setObjectArray(FinderTypeCredit.findTypesCreditDepExecutoire(ec, exercice));
				else
					eod.setObjectArray((NSArray)params.valueForKey(EOParamTypeCredit.TO_TYPE_CREDIT_KEY));			
			}

			myView.getMyEOTable().updateData();
			
			if (eod.displayedObjects().count() == 1)
				return (EOTypeCredit)eod.displayedObjects().objectAtIndex(0);

		}

		myView.setVisible(true);

		return currentTypeCredit;
	}

	public void annuler() {

		currentTypeCredit = null;
		eod.setSelectionIndexes(new NSArray());

		myView.dispose();
	}  

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerTypeCredit implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentTypeCredit = (EOTypeCredit)eod.selectedObject();
		}
	}
	
}
