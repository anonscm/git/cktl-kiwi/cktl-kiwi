/*
 * Copyright COCKTAIL, 1995-2008 This software is governed by the CeCILL license
 * under French law and abiding by the rules of distribution of free software.
 * You can use, modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kiwi.client.select;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.finders.FinderCodeAnalytique;
import org.cocktail.kiwi.client.metier.EOTypeEtat;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.common.utilities.CocktailIcones;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CodeAnalytiqueSelectCtrl  {

	private static final Color BACKGROUND_COLOR = new Color(220,220,220);

	private static CodeAnalytiqueSelectCtrl sharedInstance;
	
	private 	EOEditingContext ec;

	protected	JDialog 	mainWindow;
	protected	JFrame 		mainFrame;
	protected 	JPanel 		viewTable;

	private EODisplayGroup 	eod;
	private ZEOTable 		myEOTable;
	private ZEOTableModel 	myTableModel;
	private TableSorter 	myTableSorter;

	private JTextField 		filtreCode, filtreLibelle, filtreCodePere;

	protected 	ActionCancel 		actionCancel = new ActionCancel();
	protected 	ActionSelect 		actionSelect = new ActionSelect();


	/**
	 * 
	 *
	 */
	public CodeAnalytiqueSelectCtrl(EOEditingContext editingContext)	{

		super();
		ec = editingContext;

		gui_initView();

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CodeAnalytiqueSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CodeAnalytiqueSelectCtrl(editingContext);
		return sharedInstance;
	}



	/**
	 * Selection d'un code analytique. 
	 * 
	 * @return EOCodeAnalytique
	 */
	public EOCodeAnalytique getCodeAnalytique(EOOrgan organ, EOExercice exercice)	{

		mainWindow.setTitle("Sélection d'un Code Analytique - Exercice " + exercice.exeExercice());
		
		if (organ == null)	// Tous les codes analytiques de l'exercice
			eod.setObjectArray(FinderCodeAnalytique.getCodesAnalytiques(ec, exercice));	
		else				// Codes Analytiques de l'exercice restreint a une ligne budgetaire
			eod.setObjectArray(FinderCodeAnalytique.getCodesAnalytiques(ec, organ, exercice));	

		filter();

		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
		if (eod.selectedObject() != null)	
			return (EOCodeAnalytique)eod.selectedObject();

		return null;

	}


	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (filtreLibelle.getText().length() > 0)	{
			NSArray args = new NSArray("*"+filtreLibelle.getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.CAN_LIBELLE_KEY + " caseInsensitiveLike %@",args));
		}

		if (filtreCodePere.getText().length() > 0)	{
			NSArray args = new NSArray("*"+filtreCodePere.getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.CODE_ANALYTIQUE_PERE_KEY+"."+EOCodeAnalytique.CAN_CODE_KEY + " caseInsensitiveLike %@",args));
		}

		if (filtreCode.getText().length() > 0)	{
			NSArray args = new NSArray("*"+filtreCode.getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.CAN_CODE_KEY + " caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);        
		
	}

	/** 
	 *
	 * Filtre des codes analytiques (Code, Code Pere ou Libelle)
	 *
	 */
	private void filter()	{

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myEOTable.updateData();

	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

		public ActionSelect() {
			super("Sélectionner");
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {
			mainWindow.dispose();
		}
		
	} 


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

		public ActionCancel() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CANCEL);
		}

		public void actionPerformed(ActionEvent e) {
			myTableModel.fireTableDataChanged();
			mainWindow.dispose();
		}
		
	} 


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerCodeAnalytique implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			mainWindow.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		}
	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


	
/************* INITIALISATION GRAPHIQUE ****************/	
	
	
	/**
	 * 
	 */
	private void gui_initTextFields() {

		filtreCode = new JTextField("");
		filtreCode.setPreferredSize(new Dimension(90,18));
		filtreCode.setFont(new Font("Times", Font.PLAIN, 11));
		filtreCodePere = new JTextField("");
		filtreCodePere.setPreferredSize(new Dimension(90,18));
		filtreCodePere.setFont(new Font("Times", Font.PLAIN, 11));
		filtreLibelle = new JTextField("");
		filtreLibelle.setPreferredSize(new Dimension(190,18));
		filtreLibelle.setFont(new Font("Times", Font.PLAIN, 11));

		filtreLibelle.getDocument().addDocumentListener(new ADocumentListener());
		filtreCode.getDocument().addDocumentListener(new ADocumentListener());
		filtreCodePere.getDocument().addDocumentListener(new ADocumentListener());

	}


	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildNorthPanel() {
		
		JPanel panel = new JPanel(new BorderLayout());
	
		JPanel panelFiltres = new JPanel(new FlowLayout());

		panelFiltres.add(filtreCode);
		panelFiltres.add(filtreLibelle);
		panelFiltres.add(filtreCodePere);

		panel.setBorder(BorderFactory.createEmptyBorder(0,2,0,2));
		panel.add(panelFiltres, BorderLayout.WEST);
		
		return panel;
	}
	
	
	/**
	 * 
	 * @return
	 */
	private JPanel gui_buildSouthPanel() {
		
		JPanel panel = new JPanel(new BorderLayout());
	
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionSelect);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));

		panel.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelButtons, BorderLayout.EAST);
		
		return panel;
	}
	
	/**
	 * 
	 * Initialisation Interface Graphique
	 * 
	 */
	private void gui_initView()	{

		mainWindow = new JDialog(mainFrame, "Sélection d'un code analytique", true);
		mainWindow.setResizable(false);
        mainWindow.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		viewTable = new JPanel();
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

		gui_init_model();
		
		gui_initTextFields();

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(500, 600));
		
		mainView.add(gui_buildNorthPanel(), BorderLayout.NORTH);
		mainView.add(viewTable, BorderLayout.CENTER);
		mainView.add(gui_buildSouthPanel(), BorderLayout.SOUTH);

		mainWindow.setContentPane(mainView);
		mainWindow.pack();
		
	}


/**
 * 
 * Initialisation des DisplayGroup, Tables et TableView.
 * 
 */
	private void gui_init_model() {

		eod = new EODisplayGroup();

		Vector myCols = new Vector();

		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EOCodeAnalytique.CAN_CODE_KEY, "Code", 100);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOCodeAnalytique.CAN_LIBELLE_KEY, "Libellé", 200);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOCodeAnalytique.CODE_ANALYTIQUE_PERE_KEY+"."+EOCodeAnalytique.CAN_CODE_KEY, "Père", 100);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOCodeAnalytique.CAN_NIVEAU_KEY, "Niv", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY +"."+EOTypeEtat.TYET_LIBELLE_KEY, "Public", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerCodeAnalytique());
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(BACKGROUND_COLOR);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}
}
