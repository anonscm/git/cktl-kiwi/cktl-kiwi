package org.cocktail.kiwi.client.select;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.finders.FinderTitreMission;
import org.cocktail.kiwi.client.metier.EOTitreMission;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class TitreMissionSelectCtrl  {

	
	private static TitreMissionSelectCtrl sharedInstance;
	private EOEditingContext ec;

	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionSelect 		actionSelect = new ActionSelect();

	protected JPanel viewTable;
	

	/**
	 * 
	 *
	 */
	public TitreMissionSelectCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		
		initGUI();
		initView();
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TitreMissionSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TitreMissionSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
        mainWindow = new JDialog(mainFrame, "Titres de Mission", true);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionSelect);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 115, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(650, 300));
		mainView.add(viewTable, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}
		
	

/**
 * Selection d'UN SEUL type de credit parmi une liste proposee
 * Cette liste correspond aux types de credit DEPENSE et EXECUTOIRE

 * @param exercice
 * @param filtre
 * @return 1 objet EOTypeCredit
 */	
	public EOTitreMission getTitreMission()	{
		
    	myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    	if (eod.displayedObjects().count() == 0)
    		eod.setObjectArray(FinderTitreMission.findTitresMissionValides(ec));
			
		myEOTable.updateData();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
		if (eod.selectedObject() != null)
			return  (EOTitreMission)eod.selectedObject();			
		
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eod = new EODisplayGroup();
		
		eod.setSortOrderings(new NSArray(new EOSortOrdering(EOTitreMission.TIT_LIBELLE_KEY, EOSortOrdering.CompareAscending)));
		
        viewTable = new JPanel();
		
		initTableModel();
		initTable();
				
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		
		myEOTable.addListener(new ListenerTypeCredit());

	}
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, "titLibelle", "Libellé", 400);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "decodePaiement", "Paiement", 70);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "decodeCtrlConges", "Congés", 70);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "decodeCtrlPlanning", "Planning", 70);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "decodePermanent", "Permanent", 70);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

	    public ActionSelect() {
            super("Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	mainWindow.dispose();
        }  
	} 
	
    
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CANCEL);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	myTableModel.fireTableDataChanged();
        	mainWindow.dispose();
        }  
	} 

/**
 * 
 * @author cpinsard
 *
 */
	   private class ListenerTypeCredit implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			mainWindow.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		}
	   }	   
}
