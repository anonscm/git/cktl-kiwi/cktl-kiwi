package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOCodeMarche;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.finders.FinderCodeExer;
import org.cocktail.kiwi.client.finders.FinderPlanComptable;
import org.cocktail.kiwi.client.metier.EOParamCodeExer;
import org.cocktail.kiwi.client.metier.EOParamImputation;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CodeMarcheSelectCtrl  {

	
	private static CodeMarcheSelectCtrl sharedInstance;
	private EOEditingContext edc;

	private CodeMarcheSelectView myView;
	
	private EODisplayGroup eod  = new EODisplayGroup();
	
	private EOCodeExer currentCodeExer;

	/**
	 * 
	 *
	 */
	public CodeMarcheSelectCtrl(EOEditingContext edc)	{

		super();
		
		this.edc = edc;
		
		myView = new CodeMarcheSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ObjectListener());
		
		myView.getTfFindCode().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CodeMarcheSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CodeMarcheSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	
	public EOCodeExer getCodeMarche(EOExercice exercice)	{
		
		eod.setObjectArray(FinderCodeExer.findCodesExer(edc, exercice));		

		filter();
		myView.setVisible(true);
		
		return currentCodeExer;
	}	
    
	public NSArray getCodesMarche(EOExercice exercice)	{
		
		eod.setObjectArray(FinderCodeExer.findCodesExer(edc, exercice));		

		filter();
		myView.setVisible(true);
		
		return eod.selectedObjects();
		
	}
	
	public NSArray getCodesMarche(EOExercice exercice, boolean multiSelection, boolean filtrage)	{

    	if (multiSelection)
    		myView.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    	else
    		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		if (!filtrage)	
			eod.setObjectArray(FinderCodeExer.findCodesExer(edc , exercice));
		else	 {
			
			NSArray params = EOParamCodeExer.findForExercice(edc, exercice);
			if (params.count() == 0)
				eod.setObjectArray(FinderCodeExer.findCodesExer(edc , exercice));
			else
				eod.setObjectArray((NSArray)params.valueForKey(EOParamCodeExer.TO_CODE_EXER_KEY));			
		}
		
		filter();
		if (eod.displayedObjects().count() == 1)
			return new NSArray((EOCodeExer)eod.displayedObjects().objectAtIndex(0));

		myView.setVisible(true);
		
		if (eod.selectedObjects().count() > 0)
			return eod.selectedObjects();			
		
		return null;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public EOPlanComptable getPlanComptable(String classe, EOExercice exercice)	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		NSArray params = EOParamImputation.findParamImputations(edc, exercice);
		if (params.count() == 0)
			eod.setObjectArray(FinderPlanComptable.findComptesForSelection(edc, classe));		
		else
			eod.setObjectArray((NSArray)params.valueForKey("planComptable"));			

		filter();
		
		myView.show();
		
		if (eod.selectedObject() != null)	
			return (EOPlanComptable)eod.selectedObject();
		
		return null;
		
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (!StringCtrl.chaineVide(myView.getTfFindCode().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFindCode().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_LIB_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }
    
    /** 
    *
    */
	private void filter()	{
       
       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();              
       myView.getMyEOTable().updateData();

   }

	
	/**
	 * 
	 */
	private void annuler() {
		
		currentCodeExer = null;
		
		eod.setSelectionIndexes(new NSArray());
		
		myView.dispose();
		
	}  
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ObjectListener implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentCodeExer = (EOCodeExer)eod.selectedObject();
		}
	   }

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
