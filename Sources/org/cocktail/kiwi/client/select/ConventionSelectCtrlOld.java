package org.cocktail.kiwi.client.select;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.finders.FinderConvention;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ConventionSelectCtrlOld  {

	
	private static ConventionSelectCtrlOld sharedInstance;
	private EOEditingContext ec;

	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	private JTextField filtreExercice, filtreIndex, filtreLibelle, filtreReference;
	
	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionSelect 		actionSelect = new ActionSelect();

	protected JPanel viewTable;
	

	/**
	 * 
	 *
	 */
	public ConventionSelectCtrlOld(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		
		initGUI();
		initView();
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ConventionSelectCtrlOld sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ConventionSelectCtrlOld(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
        mainWindow = new JDialog(mainFrame, "Conventions", true);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));


        filtreExercice = new JTextField("");
        filtreExercice.setPreferredSize(new Dimension(43,18));
        filtreExercice.setFont(new Font("Times", Font.PLAIN, 11));

        filtreIndex = new JTextField("");
        filtreIndex.setPreferredSize(new Dimension(43,18));
        filtreIndex.setFont(new Font("Times", Font.PLAIN, 11));

        filtreLibelle = new JTextField("");
        filtreLibelle.setPreferredSize(new Dimension(340,18));
        filtreLibelle.setFont(new Font("Times", Font.PLAIN, 11));

        filtreReference = new JTextField("");
        filtreReference.setPreferredSize(new Dimension(220,18));
        filtreReference.setFont(new Font("Times", Font.PLAIN, 11));

        filtreExercice.getDocument().addDocumentListener(new ADocumentListener());
        filtreLibelle.getDocument().addDocumentListener(new ADocumentListener());
        filtreIndex.getDocument().addDocumentListener(new ADocumentListener());
        filtreReference.getDocument().addDocumentListener(new ADocumentListener());

		JPanel panelNorth = new JPanel(new BorderLayout());
		JPanel panelNorthCenter = new JPanel(new FlowLayout());
		panelNorthCenter.add(filtreExercice, BorderLayout.WEST);
		panelNorthCenter.add(filtreIndex, BorderLayout.WEST);
		panelNorthCenter.add(filtreLibelle, BorderLayout.CENTER);
		panelNorthCenter.add(filtreReference, BorderLayout.CENTER);
		panelNorth.add(panelNorthCenter, BorderLayout.WEST);
		panelNorth.setBorder(BorderFactory.createEmptyBorder(0,2,0,2));

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionSelect);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(700, 500));
		mainView.add(panelNorth, BorderLayout.NORTH);
		mainView.add(viewTable, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}
		
	
	/**
	 * 
	 * @return
	 */
	public EOConvention getConvention(EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit, 
			String paramAllConventions)	{
		
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		NSMutableArray conventions = new NSMutableArray();
		
		conventions.addObjectsFromArray(FinderConvention.findConventionsNonLimitatives(ec,exercice, organ, typeCredit));
		
		if (paramAllConventions != null && "O".equals(paramAllConventions))
			conventions.addObjectsFromArray(FinderConvention.findConventionsLimitatives(ec,exercice, organ, typeCredit));
			
		eod.setObjectArray(conventions);		
		filter();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
		if (eod.selectedObject() != null)	
			return (EOConvention)eod.selectedObject();
		
		return null;
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eod = new EODisplayGroup();

        viewTable = new JPanel();
		
		initTableModel();
		initTable();
				
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerPlanco());
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		


	}
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EOConvention.TO_EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, "Exercice", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOConvention.CONV_INDEX_KEY, "Index", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOConvention.LIBELLE_KEY, "Libellé", 350);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOConvention.CONV_REFERENCE_EXTERNE_KEY, "Référence", 230);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOConvention.TEM_VALIDE_KEY, "V", 20);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

	    public ActionSelect() {
            super("Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	mainWindow.dispose();
        }  
	} 
	
    
	/**
	 * 
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(filtreIndex.getText()))	{
			NSArray args = new NSArray("*"+filtreIndex.getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("convIndex caseInsensitiveLike %@",args));
		}
		
		if (!StringCtrl.chaineVide(filtreLibelle.getText()))	{
			NSArray args = new NSArray("*"+filtreLibelle.getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("libelle caseInsensitiveLike %@",args));
		}
		
		if (!StringCtrl.chaineVide(filtreReference.getText()))	{
			NSArray args = new NSArray("*"+filtreReference.getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("convReferenceExterne caseInsensitiveLike %@",args));
		}
		
		return new EOAndQualifier(mesQualifiers);        
	}

    
    /** 
    *
    */
   public void filter()	{
       
       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();              
       myEOTable.updateData();

   }
	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CANCEL);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	myTableModel.fireTableDataChanged();
        	mainWindow.dispose();
        }  
	} 
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerPlanco implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			mainWindow.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		}
	   }

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
