package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.nibctrl.FournisseurSelectView;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FournisseurSelectCtrl 
{
	private static FournisseurSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	
	private EODisplayGroup eod;
		
	FournisseurSelectView myView;
	
	/**
	 * 
	 * @param globalEc
	 */
	public FournisseurSelectCtrl (EOEditingContext globalEc)	{
		super();

		ec = globalEc;

		eod = new EODisplayGroup();

		myView = new FournisseurSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getButtonFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfCode().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfNom().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});
		
		myView.getMyEOTable().addListener(new ListenerFournis());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static FournisseurSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new FournisseurSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	/**
	 * Selection d'un engagement
	 */
	public EOFournis getFournisseur()	{
		
		myView.show();
		
		if (eod.selectedObjects().count() > 0)
			return (EOFournis) eod.selectedObject();			
		
		return null;
	}

	
	/** 
	 * Recherche de tous les fournisseurs correspondant aux criteres entres
	 */
	public void rechercher()	{
		
		if (StringCtrl.chaineVide(myView.getTfNom().getText()) && StringCtrl.chaineVide(myView.getTfCode().getText()))	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez saisir un nom ou un code.");
			return;
		}
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_VALIDE_KEY + " = 'O'", null));
		
		if (!StringCtrl.chaineVide(myView.getTfNom().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.NOM_KEY + " caseInsensitiveLike '*" + myView.getTfNom().getText() + "*' and prenom != nil", null));

		if (!StringCtrl.chaineVide(myView.getTfCode().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_CODE_KEY + " = %@",new NSArray(myView.getTfCode().getText().toUpperCase())));

		EOFetchSpecification fs = new EOFetchSpecification(EOFournis.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				
		eod.setObjectArray(ec.objectsWithFetchSpecification(fs));	
		
		myView.getMyEOTable().updateData();
		
	}
		
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}  	
	
	private class ListenerFournis implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
		}
	}
	
}