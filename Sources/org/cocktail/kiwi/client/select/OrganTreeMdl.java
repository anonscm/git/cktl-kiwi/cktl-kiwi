/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.kiwi.client.select;


import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.cocktail.application.client.swing.ZTreeNode2;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class OrganTreeMdl extends DefaultTreeModel {
    
    
    private EOQualifier _qualDatesOrgan;
    private NSArray privateOrgans;
    

    public OrganTreeMdl(OrganTreeNode root) {
        super(root);
    }

    /**
     * @param root
     * @param asksAllowsChildren
     */
    public OrganTreeMdl(OrganTreeNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
    }
    

    public TreePath findOrgan(EOOrgan organ) {
        return find2(new TreePath(getRoot()), organ);
    }

    
    
    public void setPrivateOrgans(NSArray organs)	{
    	privateOrgans = organs;
    }
    
    
    public OrganTreeNode find(OrganTreeNode node, final String str, final ArrayList nodesToIgnore, final boolean ignoreCase) {
        //ZLogger.verbose("ignorer=" + nodesToIgnore);
        
        
        if (str == null ){
            return null;
        }
        if (node == null) {
            node = (OrganTreeNode) getRoot();
        }
        if (node == null) {
            return null;
        }
        if (ignoreCase) {
            if (nodesToIgnore.indexOf(node)<0 && node.getOrgan().getShortString().toUpperCase().indexOf(str.toUpperCase())>-1 ) {
                return node;
            }
        }
        else {
            if (nodesToIgnore.indexOf(node)<0 && node.getOrgan().getShortString().indexOf(str)>-1 ) {
                return node;
            }
        }
        
        for (final Enumeration e=node.children(); e.hasMoreElements(); ) {
            final OrganTreeNode result = find( (OrganTreeNode) e.nextElement(), str, nodesToIgnore, ignoreCase);
            if (result != null) {
                return result;
            }
        }        
        
        
        return null;
    }
    
    public OrganTreeNode find(OrganTreeNode node, final String str, final ArrayList nodesToIgnore) {
        return find(node, str, nodesToIgnore, false);
    }
    
    public OrganTreeNode find(OrganTreeNode node, final EOOrgan organ) {
        if (organ == null ){
            return null;
        }
        if (node == null) {
            node = (OrganTreeNode) getRoot();
        }
        if (node == null) {
            return null;
        }
        if (organ.equals(node.getOrgan()) ) {
            return node;
        }
        return null;
    }
    
    private TreePath find2(final TreePath parent, final EOOrgan organ) {
        final OrganTreeNode node = (OrganTreeNode)parent.getLastPathComponent();
        final Object o = node.getOrgan();
    
        if (o.equals(organ)) {
            return parent;
        }
        
        for (final Enumeration e=node.children(); e.hasMoreElements(); ) {
            final TreeNode n = (TreeNode)e.nextElement();
            final TreePath path = parent.pathByAddingChild(n);
            final TreePath result = find2( path, organ);
            if (result != null) {
                return result;
            }
        }
        
    
        return null;
    }
    

    public void setQualDatesOrgan(EOQualifier qualDatesOrgan) {
        _qualDatesOrgan = qualDatesOrgan;
        if (getRoot() != null) {
            ((ZTreeNode2)getRoot()).setQualifierRecursive(_qualDatesOrgan);
        }
    }

    public OrganTreeNode createNode(OrganTreeNode node, EOOrgan _org, OrganTreeNode.ILBudTreeNodeDelegate nodeDelegate) {
        final OrganTreeNode node1 = new OrganTreeNode(node,_org,nodeDelegate, privateOrgans);
//        node1.setQualDatesOrgan(_qualDatesOrgan);
        return node1; 
    }
    
    
    public void reload(TreeNode node) {
       // ZLogger.debug("LBudTreeMdl.reload > " + (((LBudTreeNode)node).getOrgan()!=null ? ((LBudTreeNode)node).getOrgan().getLongString() : "") );
        super.reload(node);
    }
    
    /**
     * reinitialise un node (a partir de l'organ correspondante)
     * et informe les listeners que la branche a ete modifiee
     * @param node
     */
    public void invalidateNode(OrganTreeNode node) {
       // ZLogger.debug("LBudTreeMdl.invalidateNode > " + (((LBudTreeNode)node).getOrgan()!=null ? ((LBudTreeNode)node).getOrgan().getLongString() : "") );
        node.invalidateNode();
        reload(node);
    } 
    
}
