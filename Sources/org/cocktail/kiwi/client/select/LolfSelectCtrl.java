package org.cocktail.kiwi.client.select;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.finders.FinderTypeAction;
import org.cocktail.kiwi.client.metier.EOParamDestination;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class LolfSelectCtrl  {

	
	private static LolfSelectCtrl sharedInstance;
	private EOEditingContext ec;

	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	private JTextField filtreCode, filtreLibelle;
	
	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionSelect 		actionSelect = new ActionSelect();

	protected JPanel viewTable;
	

	/**
	 * 
	 *
	 */
	public LolfSelectCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		
		initGUI();
		initView();
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LolfSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LolfSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
        mainWindow = new JDialog(mainFrame, "Actions LOLF", true);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

        filtreCode = new JTextField("");
        filtreCode.setPreferredSize(new Dimension(60,18));
        filtreCode.setFont(new Font("Times", Font.PLAIN, 11));
        filtreLibelle = new JTextField("");
        filtreLibelle.setPreferredSize(new Dimension(440,18));
        filtreLibelle.setFont(new Font("Times", Font.PLAIN, 11));

        filtreLibelle.getDocument().addDocumentListener(new ADocumentListener());
        filtreCode.getDocument().addDocumentListener(new ADocumentListener());

		JPanel panelNorth = new JPanel(new BorderLayout());
//		panelNorth.add(new JLabel("Compte : " ));
		panelNorth.add(filtreCode, BorderLayout.WEST);
//		panelNorth.add(new JLabel("Libellé : " ));
		panelNorth.add(filtreLibelle, BorderLayout.CENTER);
		panelNorth.setBorder(BorderFactory.createEmptyBorder(0,2,0,2));

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionSelect);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 23));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(500, 600));
		mainView.add(panelNorth, BorderLayout.NORTH);
		mainView.add(viewTable, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public EOLolfNomenclatureDepense getTypeAction(EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit, EOUtilisateur utilisateur)	{
		
		mainWindow.setTitle("Actions LOLF (" + exercice.exeExercice() + " )");

		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);


		NSArray params = EOParamDestination.findParamDestinations(ec, exercice);
		params = (NSArray)params.valueForKey(EOParamDestination.TYPE_ACTION_KEY);
		
		NSArray actions = FinderTypeAction.getTypesAction(ec, exercice, organ, typeCredit, utilisateur);

		if (params.count() > 0) {

			NSMutableArray listeActions = new NSMutableArray();
//			NSArray codeParams = (NSArray)params.valueForKey(EOTypeAction.LOLF_CODE_KEY);
			// Restreindre aux actions parametrees
			for (int i=0;i<actions.count();i++) {

				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)actions.objectAtIndex(i);
				if (params.containsObject(action))
					listeActions.addObject(action);

			}

			eod.setObjectArray(listeActions.immutableClone());

		}
		else
			eod.setObjectArray(actions);

		filter();

		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
		if (eod.selectedObject() != null)	
			return (EOLolfNomenclatureDepense)eod.selectedObject();
		
		return null;
		
	}
	

		
	
	/**
	 * 
	 * @return
	 */
	public EOLolfNomenclatureDepense getTypeAction(EOExercice exercice)	{
		
		mainWindow.setTitle("Actions LOLF (" + exercice.exeExercice() + " )");

		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		NSArray params = EOParamDestination.findParamDestinations(ec, exercice);
		if (params.count() == 0)
			eod.setObjectArray(FinderTypeAction.getTypeActions(ec, exercice));		
		else
			eod.setObjectArray((NSArray)params.valueForKey("typeAction"));			

		filter();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
		if (eod.selectedObject() != null)	
			return (EOLolfNomenclatureDepense)eod.selectedObject();
		
		return null;
		
	}
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public NSArray getTypesAction(EOExercice exercice, boolean filtrage)	{

   		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		if (!filtrage)	
			eod.setObjectArray(FinderTypeAction.findLolfsForCodes(ec, exercice, null));
		else	 {
			
			NSArray params = EOParamDestination.findParamDestinations(ec, exercice);
			if (params.count() == 0)
				eod.setObjectArray(FinderTypeAction.findLolfsForCodes(ec, exercice, null));
			else
				eod.setObjectArray((NSArray)params.valueForKey("typeAction"));			
		}
		
		filter();
		
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.setVisible(true);
		
		if (eod.selectedObjects().count() > 0)
			return eod.selectedObjects();			
		
		return null;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eod = new EODisplayGroup();

		eod.setSortOrderings(new NSArray(new EOSortOrdering(EOLolfNomenclatureDepense.LOLF_CODE_KEY, EOSortOrdering.CompareAscending)));
		
        viewTable = new JPanel();
		
		initTableModel();
		initTable();
				
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerLolf());
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		


	}
	
	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EOLolfNomenclatureDepense.LOLF_CODE_KEY, "Code", 60);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOLolfNomenclatureDepense.LOLF_LIBELLE_KEY, "Libellé", 440);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

/*		col = new ZEOTableModelColumn(eod, EOTypeAction.TO_EXERCICE_KEY + "." +EOExercice.EXE_EXERCICE_KEY, "Libellé", 440);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);*/

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

	    public ActionSelect() {
            super("Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	mainWindow.dispose();
        }  
	} 
	
    
	/**
	 * 
	 * @return
	 */
    public EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (!StringCtrl.chaineVide(filtreLibelle.getText()))	{
            NSArray args = new NSArray("*"+filtreLibelle.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_LIBELLE_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(filtreCode.getText()))	{
            NSArray args = new NSArray("*"+filtreCode.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_CODE_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }
    
    /** 
    *
    */
   public void filter()	{
       
       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();              
       myEOTable.updateData();

   }
	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CANCEL);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	myTableModel.fireTableDataChanged();
        	mainWindow.dispose();
        }  
	} 
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerLolf implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			mainWindow.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		}
	   }

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
