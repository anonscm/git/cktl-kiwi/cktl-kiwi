/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kiwi.client.select;

import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.finders.FinderConventionLimitative;
import org.cocktail.kiwi.client.finders.FinderConventionNonLimitative;
import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.client.finders.FinderOrgan;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EOConventionLimitative;
import org.cocktail.kiwi.client.metier.budget.EOConventionNonLimitative;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LbudSelectCtrl
{

	public static String DEFAULT_CODE_TYPE_CREDIT = "30"; 

	public static LbudSelectCtrl sharedInstance;

	private ListenerOrgan listenerOrgan = new ListenerOrgan();

	// Variables locales
	private		ApplicationClient	NSApp;
	protected	EOEditingContext 		ec;

	private LbudSelectView myView;

	private EODisplayGroup eod = new EODisplayGroup();
	private EODisplayGroup eodLolf = new EODisplayGroup();

	protected 	EOOrgan				currentOrgan;
	protected 	EOLolfNomenclatureDepense		currentAction;
	protected 	EOTypeCredit			currentTypeCredit;
	protected 	EOConvention			currentConvention;
	protected	EOCodeAnalytique		currentCodeAnalytique;
	private		EOExercice				currentExercice;
	private 	EOPlanComptable 		currentPlanComptable;
	private		EOModePaiement			currentModePaiement;
	private		EOCodeExer				currentCodeExer;

	protected		boolean 			validation;

	/**	
	 * Constructeur
	 */
	public LbudSelectCtrl(EOEditingContext globalEc) {
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		ec = globalEc;

		myView = new LbudSelectView(new JFrame(), true, eod, eodLolf);

		setCurrentExercice(FinderExercice.exerciceCourant(ec));

		myView.getMyEOTable().addListener(listenerOrgan);

		updateUI();

		initOrgan();

		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnGetTypeCredit().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getTypeCredit();
			}
		});

		myView.getBtnAddAction().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getAction();
			}
		});

		myView.getBtnDelAction().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delAction();
			}
		});

		myView.getBtnGetCodeAnalytique().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getCodeAnalytique();
			}
		});

		myView.getBtnGetConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getConvention();
			}
		});

		myView.getBtnDelCodeAnalytique().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delCodeAnalytique();
			}
		});

		myView.getBtnDelConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delConvention();
			}
		});

		myView.getBtnGetImputation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getPlanComptable();
			}
		});

		myView.getBtnGetModePaiement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getModePaiement();
			}
		});

		myView.getBtnGetCodeMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectCodeAchat();
			}
		});

		myView.getTfFindUb().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindCr().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindSousCr().getDocument().addDocumentListener(new ADocumentListener());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LbudSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LbudSelectCtrl(editingContext);
		return sharedInstance;
	}



	public EOOrgan getCurrentOrgan() {
		return currentOrgan;
	}

	public void setCurrentOrgan(EOOrgan currentOrgan) {
		this.currentOrgan = currentOrgan;
	}

	public EOLolfNomenclatureDepense getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(EOLolfNomenclatureDepense currentAction) {
		this.currentAction = currentAction;
	}

	public EOTypeCredit getCurrentTypeCredit() {
		return currentTypeCredit;
	}

	public void setCurrentTypeCredit(EOTypeCredit currentTypeCredit) {
		this.currentTypeCredit = currentTypeCredit;
		CocktailUtilities.setTextToField(myView.getTfTypeCredit(), "");
		if (currentTypeCredit != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeCredit(),currentTypeCredit.tcdCode() + " - " + currentTypeCredit.tcdLibelle()
					+ "(" + currentTypeCredit.exercice().exeExercice()+")");
		}

	}

	public EOConvention getCurrentConvention() {
		return currentConvention;
	}

	public void setCurrentConvention(EOConvention currentConvention) {
		this.currentConvention = currentConvention;
	}

	public EOCodeAnalytique getCurrentCodeAnalytique() {
		return currentCodeAnalytique;
	}

	public void setCurrentCodeAnalytique(EOCodeAnalytique currentCodeAnalytique) {
		this.currentCodeAnalytique = currentCodeAnalytique;
	}

	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

	public EOPlanComptable getCurrentPlanComptable() {
		return currentPlanComptable;
	}

	public void setCurrentPlanComptable(EOPlanComptable currentPlanComptable) {
		this.currentPlanComptable = currentPlanComptable;
	}

	public EOModePaiement getCurrentModePaiement() {
		return currentModePaiement;
	}

	public void setCurrentModePaiement(EOModePaiement currentModePaiement) {
		this.currentModePaiement = currentModePaiement;
	}

	public EOCodeExer getCurrentCodeExer() {
		return currentCodeExer;
	}

	public void setCurrentCodeExer(EOCodeExer currentCodeExer) {
		this.currentCodeExer = currentCodeExer;
		CocktailUtilities.setTextToField(myView.getTfCodeMarche(), "");
		if (currentCodeExer != null) {
			CocktailUtilities.setTextToField(myView.getTfCodeMarche(),currentCodeExer.codeMarche().cmCode() +" (" + currentCodeExer.exercice().exeExercice() + ") - " + currentCodeExer.codeMarche().cmLib());			
		}
	}

	private void clearTextFields()	{

		currentOrgan = null;
		setCurrentTypeCredit(null);
		setCurrentCodeExer(null);
		currentConvention = null;
		currentCodeAnalytique = null;
		currentAction = null;
		currentModePaiement = null;
		currentPlanComptable = null;

		myView.getTfLibelleLbud().setText("");
		myView.getTfLibelleImputation().setText("");
		myView.getTfLibelleModePaiement().setText("");
		myView.getTfConvention().setText("");
		myView.getTfCodeAnalytique().setText("");
		myView.getTfPourcentage().setText("");

	}

	/**
	 * 
	 * @param currentContrat
	 * @return
	 */
	public NSDictionary updateLbud(EOExercice exercice, NSDictionary parametres, boolean saisiePourcentage) {

		if (exercice.exeExercice().intValue() != currentExercice.exeExercice().intValue()) {
			currentExercice = FinderExercice.findExercice(ec, exercice.exeExercice());
			initOrgan();
		}

		clearTextFields();

		currentOrgan = (EOOrgan)parametres.objectForKey(EOOrgan.ENTITY_NAME);
		currentTypeCredit = (EOTypeCredit)parametres.objectForKey(EOTypeCredit.ENTITY_NAME);

		if (parametres.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME) != null)			
			currentAction = (EOLolfNomenclatureDepense)parametres.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);

		if (parametres.objectForKey(EOCodeAnalytique.ENTITY_NAME) != null)			
			currentCodeAnalytique = (EOCodeAnalytique)parametres.objectForKey(EOCodeAnalytique.ENTITY_NAME);

		if (parametres.objectForKey(EOConvention.ENTITY_NAME) != null)			
			currentConvention = (EOConvention)parametres.objectForKey(EOConvention.ENTITY_NAME);

		if (parametres.objectForKey(EOPlanComptable.ENTITY_NAME) != null)			
			currentPlanComptable = (EOPlanComptable)parametres.objectForKey(EOPlanComptable.ENTITY_NAME);

		if (parametres.objectForKey(EOModePaiement.ENTITY_NAME) != null)			
			currentModePaiement = (EOModePaiement)parametres.objectForKey(EOModePaiement.ENTITY_NAME);

		if (parametres.objectForKey(EOCodeExer.ENTITY_NAME) != null)			
			setCurrentCodeExer((EOCodeExer)parametres.objectForKey(EOCodeExer.ENTITY_NAME));

		if (parametres.objectForKey("ACTIONS") != null) {
			eodLolf.setObjectArray((NSArray)parametres.objectForKey("ACTIONS"));
		}

		updateData();

		myView.getTfPourcentage().setText(((BigDecimal)parametres.objectForKey("quotite")).toString());

		CocktailUtilities.initTextField(myView.getTfPourcentage(), !saisiePourcentage, saisiePourcentage);

		myView.setVisible(true);

		NSMutableDictionary dicoRetour = new NSMutableDictionary();

		if (validation)	{

			if (getCurrentOrgan() != null)
				dicoRetour.setObjectForKey(currentOrgan, EOOrgan.ENTITY_NAME);

			if (getCurrentTypeCredit() != null)
				dicoRetour.setObjectForKey(currentTypeCredit, EOTypeCredit.ENTITY_NAME);

			if (getCurrentPlanComptable() != null)
				dicoRetour.setObjectForKey(currentPlanComptable, EOPlanComptable.ENTITY_NAME);

			if (getCurrentModePaiement() != null)
				dicoRetour.setObjectForKey(currentModePaiement, EOModePaiement.ENTITY_NAME);

			if (getCurrentCodeAnalytique() != null)
				dicoRetour.setObjectForKey(currentCodeAnalytique, EOCodeAnalytique.ENTITY_NAME);

			if (getCurrentConvention() != null)
				dicoRetour.setObjectForKey(currentConvention, EOConvention.ENTITY_NAME);

			if (getCurrentCodeExer() != null)
				dicoRetour.setObjectForKey(getCurrentCodeExer(), EOCodeExer.ENTITY_NAME);

			dicoRetour.setObjectForKey(eodLolf.displayedObjects(), "LOLF");			

			if (myView.getTfPourcentage().getText().length() > 0)
				dicoRetour.setObjectForKey(new BigDecimal(myView.getTfPourcentage().getText()), "quotite");

			return dicoRetour.immutableClone();


		}	

		return null;

	}

	/**
	 * 
	 * @param lbud
	 */
	private void updateData()	{

		if (currentOrgan != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleLbud(),currentOrgan.getLongString());
		else
			myView.getTfLibelleLbud().setText("");

		if (currentTypeCredit != null)
			CocktailUtilities.setTextToField(myView.getTfTypeCredit(),currentTypeCredit.tcdCode() + " - " + currentTypeCredit.tcdLibelle()
					+ "(" + currentTypeCredit.exercice().exeExercice()+")");
		else
			myView.getTfTypeCredit().setText("");

		setSelectedOrgan(currentOrgan);

		if (currentCodeAnalytique != null)
			CocktailUtilities.setTextToField(myView.getTfCodeAnalytique(),currentCodeAnalytique.canCode() + " - " + currentCodeAnalytique.canLibelle());
		else
			myView.getTfCodeAnalytique().setText("");

		if (currentConvention != null)
			CocktailUtilities.setTextToField(myView.getTfConvention(),currentConvention.convIndex() + " - " + currentConvention.convObjet());
		else
			myView.getTfConvention().setText("");

		if (currentPlanComptable != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleImputation(),currentPlanComptable.pcoNum() + " - " + currentPlanComptable.pcoLibelle());

		if (currentModePaiement != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleModePaiement(), currentModePaiement.modCode()+" - "+currentModePaiement.modLibelle()
					+ "(" + currentModePaiement.toExercice().exeExercice()+")");

		myView.getMyEOTableLolf().updateData();
		myView.getMyTableModelLolf().fireTableDataChanged();

	}


	/**
	 * 
	 */
	private void annuler() {

		validation = false;
		myView.dispose();

	}  

	private void valider() {
		validation = true;
		myView.dispose();
	}  

	private void getTypeCredit() {

		EOTypeCredit type = TypeCreditSelectCtrl.sharedInstance(ec).getTypeCredit(currentExercice, true);

		if (type != null)	{

			setCurrentTypeCredit(type);

			myView.getLblDisponible().setText(
					ServerProxy.clientSideRequestGetDisponible(ec, currentExercice, currentOrgan, currentTypeCredit, null).toString() + " \u20ac");

		}

		updateUI();
	}  


	private void getAction() {

		//NSApp.setWaitCursor();
		CRICursor.setWaitCursor(myView);

		EOLolfNomenclatureDepense typeAction = LolfSelectCtrl.sharedInstance(ec).getTypeAction(currentExercice, currentOrgan, currentTypeCredit, NSApp.getUtilisateur());

		if (typeAction != null)	{

			BigDecimal pourcentageActuel = getPourcentageDestinations();

			NSMutableDictionary myDico = new NSMutableDictionary();

			myDico.setObjectForKey( typeAction, EOLolfNomenclatureDepense.ENTITY_NAME);
			myDico.setObjectForKey( typeAction.lolfCode(), EOLolfNomenclatureDepense.LOLF_CODE_COLKEY);
			myDico.setObjectForKey( typeAction.lolfLibelle(), EOLolfNomenclatureDepense.LOLF_LIBELLE_COLKEY);
			myDico.setObjectForKey(  new BigDecimal(100).subtract(pourcentageActuel) , "POURCENTAGE");

			NSMutableArray actions = new NSMutableArray(eodLolf.displayedObjects());
			actions.addObject(myDico);

			eodLolf.setObjectArray(actions);

			myView.getMyEOTableLolf().updateData();
			myView.getMyTableModelLolf().fireTableDataChanged();

		}

		updateUI();
		CRICursor.setDefaultCursor(myView);

	}  


	public void delAction() {

		eodLolf.deleteSelection();
		myView.getMyEOTableLolf().updateData();

		updateUI();
	}  

	private BigDecimal getPourcentageDestinations() {

		BigDecimal  pourcentageDestinations = new BigDecimal(0);
		for (int i=0;i<eodLolf.displayedObjects().count();i++)	{

			NSDictionary destin = (NSDictionary)eodLolf.displayedObjects().objectAtIndex(i);

			pourcentageDestinations = pourcentageDestinations.add(new BigDecimal(destin.objectForKey("POURCENTAGE").toString()));

		}

		return pourcentageDestinations;
	}

	public void getPlanComptable() {

		EOPlanComptable planco = PlanComptableSelectCtrl.sharedInstance(ec).getPlanComptable("6", currentExercice);

		if (planco != null)	{
			currentPlanComptable  = planco;
			CocktailUtilities.setTextToField(myView.getTfLibelleImputation(), currentPlanComptable.pcoNum()+" - "+currentPlanComptable.pcoLibelle());
		}
	} 	


	public void getModePaiement() {

		EOModePaiement modePaiement = ModePaiementSelectCtrl.sharedInstance(ec).getModePaiement(currentExercice, true);

		if (modePaiement != null)	{
			currentModePaiement  = modePaiement;
			CocktailUtilities.setTextToField(myView.getTfLibelleModePaiement(), currentModePaiement.modCode()+" - "+currentModePaiement.modLibelle()
					+ "(" + currentModePaiement.toExercice().exeExercice()+")");
		}

		updateUI();
	} 

	/**
	 * 
	 */
	public void selectCodeAchat() {

		NSArray<EOCodeExer> codesAchat = CodeMarcheSelectCtrl.sharedInstance(ec).getCodesMarche(currentExercice, false, true);
		if (codesAchat != null && codesAchat.size() > 0)	{
			setCurrentCodeExer(codesAchat.objectAtIndex(0));
		}

		updateUI();
	} 


	private void getCodeAnalytique() {

		CRICursor.setWaitCursor(myView);

		if (currentOrgan.orgCr() == null) {
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez sélectionner une ligne budgétaire de niveau 3 (CR) minimum.");
		}
		else {

			EOCodeAnalytique canal = CodeAnalytiqueSelectCtrl.sharedInstance(ec).getCodeAnalytique(currentOrgan, currentExercice);

			if (canal != null)	{
				currentCodeAnalytique = canal;
				CocktailUtilities.setTextToField(myView.getTfCodeAnalytique(), currentCodeAnalytique.canCode() + " - " + currentCodeAnalytique.canLibelle());
			}

			updateUI();

		}

		CRICursor.setDefaultCursor(myView);

	}  


	private void delCodeAnalytique() {

		myView.getTfCodeAnalytique().setText("");
		currentCodeAnalytique = null;
		updateUI();

	}  


	private void getConvention() {

		CRICursor.setWaitCursor(myView);

		EOConvention convention = ConventionSelectCtrl.sharedInstance(ec).getConvention(currentExercice, currentOrgan, currentTypeCredit);

		if (convention != null)	{
			currentConvention = convention;
			CocktailUtilities.setTextToField(myView.getTfConvention(), currentConvention.convIndex().toString() + " - " + currentConvention.convObjet());
		}

		updateUI();

		CRICursor.setDefaultCursor(myView);

	}  


	private void delConvention() {

		myView.getTfConvention().setText("");
		currentConvention = null;
		updateUI();

	}  


	private void updateUI()	{

		myView.getBtnGetCodeAnalytique().setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() > 2);

		myView.getBtnDelCodeAnalytique().setEnabled(currentCodeAnalytique != null);

		myView.getBtnGetConvention().setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() > 2 && currentTypeCredit != null);
		myView.getBtnDelConvention().setEnabled(currentConvention != null);

		if (currentOrgan == null)
			myView.getTfLibelleLbud().setText("Veuillez saisir une ligne budgétaire");

		//		myView.getButtonValider().setEnabled(
		//				currentOrgan != null 
		//				&& currentOrgan.orgNiveau().intValue() > 2
		//				&& currentTypeCredit != null
		//				&& getPourcentageDestinations().floatValue() == 100);
	}


	/**
	 * 
	 *
	 */
	private void initOrgan()	{

		NSMutableArray<EOSortOrdering> mySort = new NSMutableArray<EOSortOrdering>();

		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));

		eod.setSortOrderings(mySort);
		eod.setObjectArray(FinderOrgan.findOrgansForUtilisateur(ec, NSApp.getUtilisateur(), currentExercice));

		myView.getMyEOTable().updateData();
	}


	public void setTitle(String title) {
		myView.setTitle(title);
	}

	/**
	 * 
	 * @param organ
	 */
	private void setSelectedOrgan(EOOrgan organ)	{

		if (organ == null)	{
			setCurrentOrgan((EOOrgan)eod.selectedObject());
			listenerOrgan.onSelectionChanged();
		}
		else	{

			eod.setSelectedObject(organ);
			myView.getMyEOTable().forceNewSelection(eod.selectionIndexes());		

			myView.getMyEOTable().scrollToSelection();
			listenerOrgan.onSelectionChanged();

		}

	}


	private class ListenerOrgan implements ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {

			setCurrentOrgan((EOOrgan)eod.selectedObject());

			myView.getTfLibelleLbud().setText("");
			myView.getLblDisponible().setText("");

			if (getCurrentOrgan() != null)	{

				// Si c'est une ressource affectee on remplit la convention associees
				if (currentOrgan.typeOrgan().isRessourceAffectee()) {

					EOConventionLimitative conventionLimitative = FinderConventionLimitative.findConventionRA(ec, currentExercice, currentOrgan, currentTypeCredit);
					if (conventionLimitative != null) {						
						currentConvention = conventionLimitative.convention();
						CocktailUtilities.setTextToField(myView.getTfConvention(), currentConvention.convIndex().toString() + " - " + currentConvention.convObjet());
					}
					else {
						EOConventionNonLimitative conventionNonLimitative = FinderConventionNonLimitative.findConventionRA(ec, currentExercice, currentOrgan, currentTypeCredit);
						if (conventionNonLimitative != null) {						
							currentConvention = conventionNonLimitative.convention();
							CocktailUtilities.setTextToField(myView.getTfConvention(), currentConvention.convIndex().toString() + " - " + currentConvention.convObjet());
						}
					}
				}

				myView.getTfLibelleLbud().setText(currentOrgan.getLongString());
				myView.getLblDisponible().setText(
						ServerProxy.clientSideRequestGetDisponible(ec, currentExercice, currentOrgan, currentTypeCredit, null).toString() + " \u20ac");

			}

			updateUI();

		}
	}


	/**
	 * 
	 */
	private void filter()	{

		EOQualifier filterQualifier = null;

		if (!StringCtrl.chaineVide(myView.getTfFindUb().getText()))
			filterQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfFindUb().getText()+"*"));

		if (!StringCtrl.chaineVide(myView.getTfFindCr().getText()))
			filterQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfFindCr().getText()+"*"));

		if (!StringCtrl.chaineVide(myView.getTfFindSousCr().getText()))
			filterQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_SOUSCR_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfFindSousCr().getText()+"*"));

		eod.setQualifier(filterQualifier);

		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();

	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


}