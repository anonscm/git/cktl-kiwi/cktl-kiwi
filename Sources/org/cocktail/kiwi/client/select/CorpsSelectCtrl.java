package org.cocktail.kiwi.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.metier.EOCorps;
import org.cocktail.kiwi.client.nibctrl.CorpsSelectView;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CorpsSelectCtrl 
{
	private static CorpsSelectCtrl sharedInstance;

	private 	EOEditingContext	ec;
	private 	EODisplayGroup		eod;
	
	private CorpsSelectView myView;

	private EOCorps currentObject;

			
	/** 
	 *
	 */
	public CorpsSelectCtrl (EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new CorpsSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		eod.setSortOrderings(new NSArray (new EOSortOrdering(EOCorps.LL_CORPS_KEY, EOSortOrdering.CompareAscending)));
		myView.getMyEOTable().addListener(new Listener());

        myView.getTfFiltreCode().getDocument().addDocumentListener(new ADocumentListener());
        myView.getTfFiltreLbelleCourt().getDocument().addDocumentListener(new ADocumentListener());
        myView.getTfFiltreLibelleLong().getDocument().addDocumentListener(new ADocumentListener());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CorpsSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CorpsSelectCtrl(editingContext);
		return sharedInstance;
	}
		

	/**
	 * 
	 * @return
	 */
	public EOCorps getCorps()	{
		
		if (eod.displayedObjects().count() == 0)	{
			eod.setObjectArray(EOCorps.findCorps(ec));
			filter();
		}
		
		myView.show();	

		if (currentObject == null)	
			return null;

		return currentObject;
	}
		
	/**
	 * 
	 */
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}  	
	
	
	/**
	 * 
	 * @return
	 */
    public EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (!StringCtrl.chaineVide(myView.getTfFiltreCode().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFiltreCode().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.C_CORPS_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getTfFiltreLbelleCourt().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFiltreLbelleCourt().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.LC_CORPS_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getTfFiltreLibelleLong().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFiltreLibelleLong().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.LL_CORPS_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }

	private void filter() {

	       eod.setQualifier(getFilterQualifier());
	       eod.updateDisplayedObjects();              
	       myView.getMyEOTable().updateData();

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class Listener implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOCorps)eod.selectedObject();
		}
	}

}