/*
 * Created on 23 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.paiement;

import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.editions.ReportsCtrl;
import org.cocktail.kiwi.client.finders.FinderMissionAvance;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderSegmentInfos;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOIndemnite;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionAvance;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDepense;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EONuits;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EORepas;
import org.cocktail.kiwi.client.metier.EORibfour;
import org.cocktail.kiwi.client.metier.EOSegTypeInfo;
import org.cocktail.kiwi.client.metier.EOSegmentInfos;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTransports;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.nibctrl.PaiementView;
import org.cocktail.kiwi.client.select.RibSelectCtrl;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class PaiementCtrl extends EODialogController	{

	private static PaiementCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private PaiementView myView;

	private EODisplayGroup eodDetailTrajet = new EODisplayGroup();
	private EODisplayGroup eodDetailRemb = new EODisplayGroup();

	private	EOMission 			currentMission;
	private	EOMissionPaiement 	currentPaiement;
	private	EORibfour			currentRib;


	public PaiementCtrl(EOEditingContext editingContext)	{

		super();

		myView = new PaiementView(eodDetailTrajet, eodDetailRemb);
		ec = editingContext;

		myView.getBtnGetRib().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectionnerRib();
			}
		});

		myView.getBtnDelRib().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delRib();
			}
		});

		myView.getBtnOrdreDeMission().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ordreDeMission();
			}
		});

		myView.getBtnEtatDeFrais().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				etatDeFrais();
			}
		});

		myView.getBtnLiquidation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liquiderMission();
			}
		});

		myView.getBtnPrintOm().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimerOrdreDeMission();
			}
		});

		myView.getBtnPrintEtatFrais().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimerEtatDeFrais();
			}
		});


	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PaiementCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PaiementCtrl(editingContext);
		return sharedInstance;
	}


	public EOMissionPaiement getCurrentPaiement() {
		return currentPaiement;
	}

	public void setCurrentPaiement(EOMissionPaiement currentPaiement) {
		this.currentPaiement = currentPaiement;
	}

	public EORibfour getCurrentRib() {
		return currentRib;
	}

	public void setCurrentRib(EORibfour currentRib) {
		this.currentRib = currentRib;
		actualiserRib();
	}

	public EOMission getCurrentMission() {
		return currentMission;
	}

	public JPanel view()	{
		return myView;
	}

	/**
	 * 
	 * @param mission
	 */
	public void setCurrentMission(EOMission mission)	{

		currentMission = mission;
		currentPaiement = null;

		if (currentMission != null)
			currentPaiement = EOMissionPaiement.findPaiementForMission(ec, currentMission);
		else
			clean();
	}


	public void setDisabled()	{

		clean();

		myView.getBtnPrintEtatFrais().setEnabled(false);
		myView.getBtnPrintOm().setEnabled(false);

		myView.getBtnOrdreDeMission().setEnabled(false);
		myView.getBtnEtatDeFrais().setEnabled(false);
		myView.getBtnLiquidation().setEnabled(false);

		// RIB
		myView.getBtnDelRib().setEnabled(false);
		myView.getBtnGetRib().setEnabled(false);

		PaiementAvecFraisCtrl.sharedInstance(ec, this).setDisabled();

	}

	private void delRib()	{

		try {

			if (currentPaiement != null && currentRib != null)	{

				getCurrentPaiement().setRibfourRelationship(null);
				setCurrentRib(null);

				myView.getTfRib().setText("");
			}

			ec.saveChanges();

			updateInterface();

		}
		catch (Exception e)	{
			ec.revert();
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur de suppression du RIB !!!");
		}
	}

	private void clean()	{


		eodDetailTrajet.setObjectArray(new NSArray());
		eodDetailRemb.setObjectArray(new NSArray());

		myView.getMyEOTableDetailTrajet().updateData();
		myView.getMyEOTableDetailRemb().updateData();

		myView.getTfBudgetaire().setText("");
		myView.getTfRembourse().setText("");

		myView.getTfRib().setText("");

		PaiementAvecFraisCtrl.sharedInstance(ec, this).clean();
		PaiementSansFraisCtrl.sharedInstance(ec).clean();

	}


	public void actualiser()	{

		myView.getSwapViewPaiement().removeAll();

		if ( currentMission != null && !currentMission.isMissionPaiement() && !currentMission.isSaisieLbud())
			myView.getSwapViewPaiement().add(PaiementSansFraisCtrl.sharedInstance(ec).view());
		else
			myView.getSwapViewPaiement().add(PaiementAvecFraisCtrl.sharedInstance(ec, this).view());

		clean();
		updateData();
		updateInterface();		

	}



	/**
	 * 
	 */
	private void updateData()	{

		if (getCurrentMission() != null)	{

			if (!getCurrentMission().isMissionPaiement() && !getCurrentMission().isSaisieLbud())
				PaiementSansFraisCtrl.sharedInstance(ec).actualiser(getCurrentMission());
			else
				PaiementAvecFraisCtrl.sharedInstance(ec, this).actualiser(getCurrentMission(), getCurrentPaiement());

			// Mise a jour des differents paiements
			fillEodTrajets();	
			myView.getMyEOTableDetailTrajet().updateData();

			fillEodSegmentInfos();	
			myView.getMyEOTableDetailRemb().updateData();

			// Calcul des montants a payer
			myView.getTfBudgetaire().setText(calculTotal().toString());
			myView.getTfRembourse().setText(calculRemb().toString());

			setCurrentRib(null);

			// Mise a jour des paiements
			if (getCurrentPaiement() != null)	{

				myView.getTfEtat().setText(getCurrentPaiement().mipEtat());

				EORibfour rib = getCurrentPaiement().ribfour();
				if (rib != null)	{
					setCurrentRib(rib);
					actualiserRib();
				}

				if( currentMission.isValide() &&  ( currentMission.isMissionPaiement() || currentMission.isSaisieLbud()) )
					majMontantPaiement();
			}						
		}

	}


	private void fillEodSegmentInfos()	{

		NSMutableArray arrayInfos = new NSMutableArray();
		NSMutableDictionary dicoInfos = new NSMutableDictionary();

		NSArray segments = FinderSegmentInfos.findSegmentsInfosForMission(ec, currentMission);

		for (int s=0;s<segments.count();s++)	{

			EOSegmentInfos segment = (EOSegmentInfos)segments.objectAtIndex(s);

			dicoInfos = new NSMutableDictionary();
			dicoInfos.setObjectForKey(segment.segTypeInfo().segGroupeInfo().stgLibelle(),"TYPE");

			dicoInfos.setObjectForKey(segment.segTypeInfo().stiLibelle(),"LIBELLE");

			String chaineCommande ="";
			if (segment.commande() != null)	{				
				chaineCommande = "No " + segment.commande().commNumero();// +" - " + segment.commande().engTtc()+" \u20ac";
			}

			dicoInfos.setObjectForKey(chaineCommande,"COMMANDE");

			dicoInfos.setObjectForKey(segment.segTypeInfo().stiSigne(),"SIGNE");
			dicoInfos.setObjectForKey(segment.seiMontantPaiement(),"MONTANT");
			arrayInfos.addObject(dicoInfos);
		}

		NSArray<EOMissionAvance> avances = FinderMissionAvance.findAvancesForMission(ec, currentMission);

		for (int s=0;s<avances.count();s++)	{

			EOMissionAvance avance = (EOMissionAvance)avances.objectAtIndex(s);

			dicoInfos = new NSMutableDictionary();
			dicoInfos.setObjectForKey("AVANCE","TYPE");

			dicoInfos.setObjectForKey("AVANCE","LIBELLE");

			String chaineCommande ="";
			if (avance.visaAvMission() != null && avance.visaAvMission().ordreDePaiement() != null)	{				
				chaineCommande = "Op " + avance.visaAvMission().ordreDePaiement().odpNumero();
			}

			if (avance.depense() != null && avance.depense().depensePapier().dppNumeroFacture() != null)	{				
				chaineCommande = "Dep " + avance.depense().depensePapier().dppNumeroFacture();
			}			

			dicoInfos.setObjectForKey(chaineCommande,"COMMANDE");

			dicoInfos.setObjectForKey("BUDGETAIRE","SIGNE");
			dicoInfos.setObjectForKey(avance.mavMontant(),"MONTANT");
			arrayInfos.addObject(dicoInfos);
		}


		eodDetailRemb.setObjectArray(arrayInfos);

	}





	/**
	 * Affichage de toutes les donnees concernant le trajet
	 * 
	 * @param segment
	 */
	private void fillEodTrajets()	{

		NSMutableArray<NSMutableDictionary> arrayTrajets = new NSMutableArray();
		NSMutableDictionary dicoTrajets = new NSMutableDictionary();
		NSArray segments = currentMission.segmentsTarif();

		for (int s=0;s<segments.count();s++)	{

			EOSegmentTarif segment = (EOSegmentTarif)segments.objectAtIndex(s);

			EORembZone rembZone = segment.rembZone();

			NSArray nuits = segment.nuits();		
			NSArray repas = segment.repas();
			NSArray indemnites = segment.indemnites();
			NSArray transports = segment.transports();

			for (int i=0;i<nuits.count();i++)	{
				EONuits nuit = (EONuits)nuits.objectAtIndex(i);
				dicoTrajets = new NSMutableDictionary();
				dicoTrajets.setObjectForKey("NUITS","TYPE_REMB");
				if (segment.segLibelle() != null)
					dicoTrajets.setObjectForKey(segment.segLibelle(),"TYPE_TRAJET");
				else
					dicoTrajets.setObjectForKey("","TYPE_TRAJET");
				dicoTrajets.setObjectForKey(rembZone.remLibelle(),"ZONE");
				BigDecimal nbNuits = new BigDecimal(nuit.nuiNbNuits());
				dicoTrajets.setObjectForKey(nbNuits + " Nuits","INFOS");
				dicoTrajets.setObjectForKey(nuit.nuiMontantPaiement(),"MONTANT");
				arrayTrajets.addObject(dicoTrajets);
			}

			for (int i=0;i<repas.count();i++)	{
				EORepas rep = (EORepas)repas.objectAtIndex(i);
				dicoTrajets = new NSMutableDictionary();
				dicoTrajets.setObjectForKey("REPAS","TYPE_REMB");
				if (segment.segLibelle() != null)
					dicoTrajets.setObjectForKey(segment.segLibelle(),"TYPE_TRAJET");
				else
					dicoTrajets.setObjectForKey("","TYPE_TRAJET");
				dicoTrajets.setObjectForKey(rembZone.remLibelle(),"ZONE");
				BigDecimal nbRepas = rep.repNbRepas();
				dicoTrajets.setObjectForKey(nbRepas+ " Repas","INFOS");
				dicoTrajets.setObjectForKey(rep.repMontantPaiement(),"MONTANT");
				arrayTrajets.addObject(dicoTrajets);			
			}

			for (int i=0;i<transports.count();i++)	{
				EOTransports transport = (EOTransports)transports.objectAtIndex(i);
				dicoTrajets = new NSMutableDictionary();
				dicoTrajets.setObjectForKey("TRANSPORTS","TYPE_REMB");
				if (segment.segLibelle() != null)
					dicoTrajets.setObjectForKey(segment.segLibelle(),"TYPE_TRAJET");
				else
					dicoTrajets.setObjectForKey("","TYPE_TRAJET");
				dicoTrajets.setObjectForKey(rembZone.remLibelle(),"ZONE");

				if (transport.typeTransport().ttrLibelle() != null)
					dicoTrajets.setObjectForKey(transport.typeTransport().ttrLibelle(),"INFOS");

				// On transforme le montant du paiement par rapport a la devise
				BigDecimal montantPaiement = transport.traMontantPaiement();
				BigDecimal tauxDevise = new BigDecimal(1);//transport.segmentTarif().webtaux().wtaTaux();
				montantPaiement = (montantPaiement.multiply(tauxDevise)).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);
				dicoTrajets.setObjectForKey(montantPaiement,"MONTANT");
				arrayTrajets.addObject(dicoTrajets);
			}

			for (int i=0;i<indemnites.count();i++)	{
				EOIndemnite indemnite = (EOIndemnite)indemnites.objectAtIndex(i);
				dicoTrajets = new NSMutableDictionary();
				dicoTrajets.setObjectForKey("INDEMNITES","TYPE_REMB");
				if (segment.segLibelle() != null)
					dicoTrajets.setObjectForKey(segment.segLibelle(),"TYPE_TRAJET");
				else
					dicoTrajets.setObjectForKey("","TYPE_TRAJET");
				dicoTrajets.setObjectForKey(rembZone.remLibelle(),"ZONE");
				dicoTrajets.setObjectForKey(indemnite.indJours() + " Jours","INFOS");
				dicoTrajets.setObjectForKey(indemnite.indMontantPaiement(),"MONTANT");
				arrayTrajets.addObject(dicoTrajets);
			}

		}

		eodDetailTrajet.setObjectArray(arrayTrajets);

	}





	private void majMontantPaiement()	{

		try {			

			// On aligne les montants a rembourser et budgetaires sur le 
			currentPaiement.setMipMontantTotal(new BigDecimal(myView.getTfBudgetaire().getText()));				
			currentPaiement.setMipMontantPaiement(new BigDecimal(myView.getTfRembourse().getText()));

			// Mise a jour du montant des actions
			PaiementAvecFraisCtrl.sharedInstance(ec, this).updateMontants();

			ec.saveChanges();

		}
		catch (Exception e)	{
			e.printStackTrace();
		}

	}

	/**
	 * Mise a jour des boutons d'action en fonction de l'etat de la mission
	 * Donnees budgetaires + Impression 
	 *
	 */
	protected void updateInterface()	{

		// Si on  a les droits de prémission on ne peut voir que son RIB
//		if (NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION) 
//				&& !NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION)) {
//			myView.getBtnDelRib().setVisible(NSApp.getCurrentUtilisateur().persId().intValue() == currentMission.fournis().persId().intValue() );
//			myView.getBtnGetRib().setVisible(NSApp.getCurrentUtilisateur().persId().intValue() == currentMission.fournis().persId().intValue());
//		}

		if (currentMission.isMissionPaiement() || currentMission.isSaisieLbud())
			PaiementAvecFraisCtrl.sharedInstance(ec, this).updateUI();
		else
			PaiementSansFraisCtrl.sharedInstance(ec).updateUI();

		// Mise a jour du libelle du bouton de liquidation

		if (!currentMission.isMissionPaiement())	{

			if (currentMission.isPayee())
				myView.getBtnLiquidation().setText("REVALIDER MISSION");
			else
				myView.getBtnLiquidation().setText("CLOTURE MISSION");

		}
		else {

			if (currentPaiement.isChargeAPayer() == false) {
				myView.getBtnLiquidation().setText("LIQUIDATION");
			}
			else {	// Mode CHARGE A PAYER

				if (currentPaiement.isExtourne())
					myView.getBtnLiquidation().setText("LIQUIDATION DEFINITIVE");
				else {

					BigDecimal montant = getMontantLiquidationsPartielles();

					if (montant.floatValue() == 0)
						myView.getBtnLiquidation().setText("LIQUIDATION CAP");
					else
						myView.getBtnLiquidation().setText("LIQUIDATION DEFINITIVE");
				}

			}
		}


		// On ne peut rien saisir ou imprimer si la MISSION est ANNULEE ou PAYEE
		if ( 	currentMission == null
				|| currentMission.isAnnulee() 
				|| (currentMission.isPayee() && currentMission.isMissionPaiement())
				)	{

			myView.getBtnEtatDeFrais().setEnabled(false);
			myView.getBtnOrdreDeMission().setEnabled(false);
			myView.getBtnLiquidation().setEnabled(false);

			myView.getBtnGetRib().setEnabled(false);
			myView.getBtnDelRib().setEnabled(false);

			// Si la mission est PAYEE on peut quand meme ressortir l'ordre de mission et l'etat de frais
			myView.getBtnPrintOm().setEnabled(currentMission != null && currentMission.isPayee());
			myView.getBtnPrintEtatFrais().setEnabled(currentMission != null && currentMission.isPayee());
		}
		else	{

			// S'il s'agit d'une MISSION SANS FRAIS , Pas de saisie budgetaire mais on peut Imprimer
			if (!currentMission.isMissionPaiement())	{

				myView.getBtnPrintEtatFrais().setEnabled(currentMission.isSaisieLbud());

				myView.getBtnLiquidation().setEnabled(		
						NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION)
						||
						(
								!currentMission.isPayee()							
								&& 
								currentMission.utilisateurCreation().individu().persId().intValue() == NSApp.getUtilisateur().individu().persId().intValue() 
								)
						);					

				myView.getBtnOrdreDeMission().setEnabled(currentPaiement != null  && !currentMission.isPayee());

				myView.getBtnEtatDeFrais().setEnabled(false);

				myView.getBtnDelRib().setEnabled(currentRib != null);
				myView.getBtnGetRib().setEnabled(currentMission.isSaisieLbud());

				myView.getBtnPrintOm().setEnabled(currentPaiement != null && currentPaiement.mipInfos() == null);

			}			
			else	{	// MISSION OK - On peut saisir les donnees budgetaires

				// RIB - Pas de suppression possible. Modification seulement ==> RIB Obligatoire
				myView.getBtnGetRib().setEnabled(currentPaiement != null);

				EOMissionPaiementEngage paiementEngage = PaiementAvecFraisCtrl.sharedInstance(ec, this).getFirstEngagement();

				myView.getBtnDelRib().setEnabled(
						paiementEngage != null 
						&& paiementEngage.modePaiement() != null 
						&& !paiementEngage.modePaiement().modDom().equals(EOModePaiement.CODE_DOMAINE_VIREMENT) 
						&& currentRib != null);

				// Mise a jour des boutons : OM, Etat de frais et Liquidations
				myView.getBtnOrdreDeMission().setEnabled(true);
				myView.getBtnEtatDeFrais().setEnabled(true);

				myView.getBtnLiquidation().setEnabled(NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION));				

				myView.getBtnPrintOm().setEnabled(currentPaiement != null && currentPaiement.mipInfos() == null && currentMission.isPayee());
				myView.getBtnPrintEtatFrais().setEnabled(currentPaiement != null && currentPaiement.mipInfos() == null && currentMission.isPayee());

				if (currentPaiement != null)	{

					if (currentPaiement.isValide())	{
						myView.getBtnEtatDeFrais().setEnabled(false);
						myView.getBtnLiquidation().setEnabled(false);	
						myView.getBtnPrintOm().setEnabled(false);
						myView.getBtnPrintEtatFrais().setEnabled(false);
					}
					else	// ORDRE ==> ORDRE DE MISSION + ETAT DE FRAIS
						if ("ORDRE".equals(currentPaiement.mipEtat()))
							myView.getBtnLiquidation().setEnabled(false);

				}
			}
		}

	}

	/**
	 * Modification ou Suppression du RIB selectionne
	 * Selon le titre de la mission (Paiement ou non), on affecte une action differente au bouton
	 * 
	 * @param sender
	 */
	private void selectionnerRib()	{

		try {
			EORibfour rib = RibSelectCtrl.sharedInstance(ec).getRib(currentMission.fournis());

			if (rib != null)	{
				setCurrentRib(rib);
				currentPaiement.setRibfourRelationship(currentRib);
			}

			actualiserRib();
			ec.saveChanges();
		}
		catch (Exception e)	{
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur de modification du RIB !!!");
			return;
		}				

	}



	private void actualiserRib()	{

		myView.getTfRib().setText("");

		if (currentRib != null)	{

			if (currentRib.iban() != null) {

				String rib = "";

				rib = rib + "IBAN : " + currentRib.iban();

				if (currentRib.banque() != null)
					if (currentRib.banque().bic() != null)
						rib = rib + " - BIC : "  + currentRib.banque().bic() + " - ";

				if (currentRib.ribTitco() != null)
					rib = rib + " - TIT : " + currentRib.ribTitco();

				myView.getTfRib().setText(rib);

			}
			else
				myView.getTfRib().setText(currentRib.ribEtab()+" - " 
						+ currentRib.ribGuich() + " - " 
						+ currentRib.ribNum().toString() + " " 
						+ currentRib.ribCle()
						+ " - " + currentRib.ribTitco());

		}

	}


	/**
	 *  Recuperation du montant total des liquidations de la mission
	 * @return
	 */
	private BigDecimal getMontantLiquidationsPartielles() {

		BigDecimal montant = new BigDecimal(0);
		NSArray<EOMissionPaiementDepense> liquidations = EOMissionPaiementDepense.findDepensesForMission(ec, currentMission);
		for (EOMissionPaiementDepense myDepense : liquidations) {

			// On ne contrôle pas les depenses d'avances
			if (!StringCtrl.containsIgnoreCase(myDepense.depense().depensePapier().dppNumeroFacture(), "AVANCE") )	{
				montant = montant.add(myDepense.depense().depTtcSaisie());
			}

		}

		return montant;

	}


	/**
	 * Calcul du montant budgetaire de la mission
	 * Montant Trajet + Infos POSITIF ou FORFAIT ou BUDGETAIRE - Infos NEGATIF
	 *
	 */
	private BigDecimal calculTotal() {

		BigDecimal total = new BigDecimal(0);

		BigDecimal totalForfait = new BigDecimal(0);
		BigDecimal totalPositif = new BigDecimal(0);
		BigDecimal totalNegatif = new BigDecimal(0);

		total = CocktailUtilities.computeSumForKey(eodDetailTrajet, "MONTANT");

		for (int i = 0; i < eodDetailRemb.allObjects().count();i++) {

			String signe = (String)((NSDictionary) eodDetailRemb.allObjects().objectAtIndex(i)).objectForKey("SIGNE");
			BigDecimal montantSegment = (BigDecimal)((NSDictionary) eodDetailRemb.allObjects().objectAtIndex(i)).objectForKey("MONTANT");

			if (signe.equals("FORFAIT"))
				totalForfait = totalForfait.add(montantSegment);

			if (signe.equals("POSITIF"))
				totalPositif = totalPositif.add(montantSegment);

			if (signe.equals("NEGATIF"))
				totalNegatif = totalNegatif.add(montantSegment);
		}

		//if (totalForfait.floatValue() == 0 )
		total = total.add(totalPositif);

		String paramCheckForfait = FinderMissionParametres.getValue(ec, currentMission.toExercice() , EOMissionParametres.ID_CHECK_FORFAIT);
		if (paramCheckForfait != null && "O".equals(paramCheckForfait)) {

			if ( totalForfait.floatValue() > 0  &&  totalForfait.floatValue() < total.floatValue() )
				total = totalForfait;

		}
		else {

			if ( totalForfait.floatValue() > 0 )
				total = totalForfait;

		}

		total = total.subtract(totalNegatif);

		if (total.signum() == -1)
			total = new BigDecimal(0);

		// Si le montant de l'avance est superieur au montant de la mission, le montant budgetaire devient le montant de l'avance.
		NSArray avances = FinderMissionAvance.findAvancesForMission(ec, currentMission);
		BigDecimal montantAvances = new BigDecimal(0);

		for (int i=0;i<avances.count();i++) {
			EOMissionAvance avance = (EOMissionAvance)avances.objectAtIndex(i);
			montantAvances = montantAvances.add(avance.mavMontant());
		}

		if (montantAvances.floatValue() > total.floatValue())
			total = montantAvances;


		//total = total.subtract(FinderMissionPaiementDepense.montantDepensesForMission(ec, currentMission));

		//total = total.subtract(getMontantLiquidationsPartielles());

		return total.setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);
	}


	/**
	 * 
	 * @return
	 */
	public BigDecimal calculEngagement() {

		BigDecimal total = new BigDecimal(0);

		BigDecimal totalForfait = new BigDecimal(0);
		BigDecimal totalPositif = new BigDecimal(0);
		BigDecimal totalNegatif = new BigDecimal(0);

		total = CocktailUtilities.computeSumForKey(eodDetailTrajet, "MONTANT");

		for (int i = 0; i < eodDetailRemb.allObjects().count();i++) {

			String signe = (String)((NSDictionary) eodDetailRemb.allObjects().objectAtIndex(i)).objectForKey("SIGNE");
			BigDecimal montantSegment = (BigDecimal)((NSDictionary) eodDetailRemb.allObjects().objectAtIndex(i)).objectForKey("MONTANT");

			if (signe.equals(EOSegTypeInfo.SIGNE_FORFAIT))
				totalForfait = totalForfait.add(montantSegment);

			if (signe.equals(EOSegTypeInfo.SIGNE_POSITIF))
				totalPositif = totalPositif.add(montantSegment);

			if (signe.equals(EOSegTypeInfo.SIGNE_NEGATIF))
				totalNegatif = totalNegatif.add(montantSegment);
		}

		//if (totalForfait.floatValue() == 0 )
		total = total.add(totalPositif);

		String paramCheckForfait = FinderMissionParametres.getValue(ec, currentPaiement.mission().toExercice() ,EOMissionParametres.ID_CHECK_FORFAIT);
		if (paramCheckForfait != null && "O".equals(paramCheckForfait)) {
			if ( totalForfait.floatValue() > 0  &&  totalForfait.floatValue() < total.floatValue() ) {
				total = totalForfait;
			}
		}
		else {
			if ( totalForfait.floatValue() > 0 ) {
				total = totalForfait;
			}
		}

		total = total.subtract(totalNegatif);

		if (total.signum() == -1)
			total = new BigDecimal(0);

		// Si le montant de l'avance est superieur au montant de la mission, le montant budgetaire devient le montant de l'avance.
		NSArray avances = FinderMissionAvance.findAvancesForMission(ec, currentMission);
		BigDecimal montantAvances = new BigDecimal(0);

		for (int i=0;i<avances.count();i++) {
			EOMissionAvance avance = (EOMissionAvance)avances.objectAtIndex(i);
			montantAvances = montantAvances.add(avance.mavMontant());
		}

		if (montantAvances.floatValue() > total.floatValue())
			total = montantAvances;

		total = total.subtract(getMontantLiquidationsPartielles());

		return total.setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);
	}


	/**
	 * Calcul du montant a rembourser
	 * Montant Trajet + Infos POSITIF ou FORFAIT - Infos NEGATIF
	 * 
	 * @return
	 */
	private BigDecimal calculRemb() {

		BigDecimal total = new BigDecimal(0);		

		BigDecimal totalPositif = new BigDecimal(0);		
		BigDecimal totalNegatif = new BigDecimal(0);		
		BigDecimal totalForfait = new BigDecimal(0);

		total = CocktailUtilities.computeSumForKey(eodDetailTrajet, "MONTANT");

		for (int i=0;i < eodDetailRemb.displayedObjects().count();i++) {

			NSDictionary segment = (NSDictionary) eodDetailRemb.allObjects().objectAtIndex(i);
			BigDecimal montantSegment = (BigDecimal)segment.objectForKey("MONTANT");
			String signe = (String)segment.objectForKey("SIGNE");

			if (signe.equals(EOSegTypeInfo.SIGNE_FORFAIT))
				totalForfait = totalForfait.add(montantSegment);

			if (signe.equals(EOSegTypeInfo.SIGNE_POSITIF))
				totalPositif = totalPositif.add(montantSegment);

			if (signe.equals(EOSegTypeInfo.SIGNE_NEGATIF) || signe.equals("BUDGETAIRE"))
				totalNegatif = totalNegatif.add(montantSegment);

			//			String type = (String)segment.objectForKey("TYPE");
			//			if (type.indexOf("AVANCE") > -1 && signe.equals("BUDGETAIRE"))
			//				totalAvances = totalAvances.add(montantSegment);
		}	

		total = total.add(totalPositif);

		String paramCheckForfait = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_CHECK_FORFAIT);
		if (paramCheckForfait != null && "O".equals(paramCheckForfait)) {

			if ( totalForfait.floatValue() > 0  &&  totalForfait.floatValue() < total.floatValue() )
				total = totalForfait;

		}
		else {
			if ( totalForfait.floatValue() > 0 )
				total = totalForfait;
		}

		// Dans le cas d'une mission en charges a payer, verifier qu'il n'y ait pas de depenses associees
		if (currentPaiement.isChargeAPayer() && !currentMission.isPayee() && !currentPaiement.isExtourne()) {

			String paramModeCodeCap = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_MODE_CAP);

			if (paramModeCodeCap != null) {
				NSArray<EOMissionPaiementDepense> liquidations = EOMissionPaiementDepense.findDepensesForMission(ec, currentMission);		
				for (EOMissionPaiementDepense myDepense : liquidations) {
					if (myDepense.depense().depensePapier().modePaiement().modCode().equals(paramModeCodeCap))
						totalNegatif = totalNegatif.add(getMontantLiquidationsPartielles());
				}
			}
		}

		total = total.subtract(totalNegatif);

		return total.setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);		
	}


	/**
	 * 
	 * Cloture d'une mission sans frais.
	 * Si la mission etait deja cloturee, on demande si on souhaite la revalider.
	 * 
	 * ETAT = TERMINEE ou 'VALIDE'.
	 * 
	 */
	private void cloturerMission() {

		// On ne pourra cloturer la mission qu'apres le retour du missionnaire
		if (DateCtrl.isAfter(currentMission.misFin(), new NSTimestamp())) {
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","La clôture de cette mission " +
					"ne sera possible seulement lorsque le missionnaire sera rentré ("
					+ DateCtrl.dateToString(currentMission.misFin(), "%d/%m/%Y %H:%M") + ")");
			return;
		}

		try {

			NSArray<EOMissionPaiementEngage> paiements = EOMissionPaiementEngage.findForMission(ec, currentMission);

			if (!currentMission.isTerminee()) {

				if (EODialogs.runConfirmOperationDialog("Attention",
						"Confirmez-vous la clôture de cette mission ?",
						"OUI", "NON")) {

					currentMission.setMisEtat(EOMission.ETAT_TERMINE);
					currentPaiement.setMipEtat(EOMission.ETAT_TERMINE);

					for (EOMissionPaiementEngage myPaiement : paiements)
						myPaiement.setMpeEtat(EOMission.ETAT_TERMINE);
				}
			}
			else {

				if (EODialogs.runConfirmOperationDialog("Attention",
						"Souhaitez-vous réellement revalider cette mission ?",
						"OUI", "NON")) {

					currentMission.setMisEtat(EOMission.ETAT_VALIDE);
					currentPaiement.setMipEtat(EOMissionPaiement.ETAT_VALIDE);

					for (EOMissionPaiementEngage myPaiementEngage : paiements)
						myPaiementEngage.setMpeEtat(EOMissionPaiementEngage.ETAT_VALIDE);

				}

			}

			ec.saveChanges();

			EnteteMission.sharedInstance(ec).rafraichirMission();

		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}

	/**
	 * Liquidation effective de la mission. 
	 */
	private void liquiderMission()	{

		EOMissionPaiementEngage paiementEngage = PaiementAvecFraisCtrl.sharedInstance(ec, this).getFirstEngagement();

		// S'il s'agit d'une mission sans frais, soit on la CLOTURE (ETAT = TERMINEE), soit on la revalider.
		if (!currentMission.isMissionPaiement()) {			
			cloturerMission();
			return;
		}

		// L'etat de frais doit etre sorti avant de liquider
		if ( currentPaiement.isSignature() == false && currentPaiement.isExtourne() == false) {
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez vérifier que l'ordre de mission ET l'état de frais ont bien été imprimés !");
			return;
		}

		// Verification du RIB
		EORibfour rib = currentPaiement.ribfour();

		if (rib == null && paiementEngage.modePaiement().modDom().equals("1"))	{

			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez associer un RIB pour liquider la mission !");			
			return;

		}
		else	{

			if (rib != null)	{

				if (!rib.ribValide().equals("O"))	{
					EODialogs.runInformationDialog("ERREUR","La liquidation de la mission ne pourra se faire que si le RIB du missionnaire est VALIDE !");			
					return;			
				}

				// On verifie que le fournisseur associe a la mission soit bien le fournisseur associe au rib
				if (rib.fournis().persId().intValue() != currentMission.fournis().persId().intValue())	{
					EODialogs.runInformationDialog("ERREUR","Le RIB sur lequel sera viré le montant à rembourser n'est pas celui du missionnaire !");			
					return;							
				}
			}

		}

		if (!PaiementAvecFraisCtrl.sharedInstance(ec, this).testDonneesBudgetairesLiquidation())
			return;


		if (!PaiementAvecFraisCtrl.sharedInstance(ec, this).testDonneesBudgetaires())
			return;

		// LIQUIDATION
		try {

			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous la liquidation de la mission ?",
					"OUI", "NON")) {

				// Verification de la coherence entre la ligne budgetaire de la mission et celle de l'engagement
				if (currentMission.isMissionPaiement() 
						&& new BigDecimal(myView.getTfBudgetaire().getText()).floatValue() == 0
						&& new BigDecimal(myView.getTfRembourse().getText()).floatValue() == 0 
						//&& FinderSegmentInfos.countAvances(ec, currentMission) == 0
						//&& FinderMissionAvance.countAvances(ec, currentMission) == 0
						) {

					if (!EODialogs.runConfirmOperationDialog("Attention",
							"Le montant budgétaire ET le montant à rembourser sont égaux à 0 !\n Désirez vous poursuivre la liquidation ?",
							"OUI", "NON"))
						return;

				}

				// Mise a jour de l'agent liquidateur
				currentPaiement.setUtilisateurRelationship(NSApp.getUtilisateur());	

				ec.saveChanges();

				Number leMipOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentPaiement).objectForKey("mipOrdre");

				String msg = "";

				// Un etat EXTOURNE signifie que la mission a deja eu une liquidation sur extourne
				// On passe donc une liquidation definitive
				if (currentPaiement.isExtourne()) {
					msg = ServerProxy.liquidationDefinitive(ec, leMipOrdre);
				}
				else {
					msg = ServerProxy.liquidation(ec, leMipOrdre);
				}

				if (!msg.equals("OK")) {
					MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
					ec.revert();
					return;
				}

			}
			else {		// Annulation de la procedure de liquidation

				return;

			}
		}
		catch (Exception ex) {

			ec.revert();
			ex.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","ERREUR DE LIQUIDATION !\n\n" + CocktailUtilities.getErrorDialog(ex));
			return;

		}

		if (currentPaiement.mipInfos() != null)	{
			ec.revert();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",currentPaiement.mipInfos());
		}
		else	{

			// On remet les donnees a jour
			EnteteMission.sharedInstance(ec).rafraichirMission();

			if (currentMission.isMissionPermanente()) {

				try {

					currentMission.setMisEtat(EOMission.ETAT_VALIDE);
					currentPaiement.setMipEtat(EOMission.ETAT_VALIDE);

					if (EODialogs.runConfirmOperationDialog("Attention",
							"!! OM PERMANENT !!/nSouhaitez-vous cloturer définitivement cette mission permanente ?",
							"OUI", "NON")) {

						currentMission.setMisEtat(EOMission.ETAT_PAYE);
						currentPaiement.setMipEtat(EOMission.ETAT_PAYE);

						NSArray<EOMissionPaiementEngage> paiements = EOMissionPaiementEngage.findForMission(ec, currentMission);
						for (EOMissionPaiementEngage myPaiementEngage : paiements)
							myPaiementEngage.setMpeEtat(EOMission.ETAT_PAYE);

					}

					ec.saveChanges();

					MsgPanel.sharedInstance().runInformationDialog("LIQUIDATION OK","La mission a bien été liquidée.\n");

				}

				catch (Exception ex) {

					ex.printStackTrace();

				}

			}
			else {
				MsgPanel.sharedInstance().runInformationDialog("LIQUIDATION OK","La mission a bien été liquidée.\nElle n'est maintenant plus modifiable.");
			}
		}

		EnteteMission.sharedInstance(ec).rafraichirMission();

	}


	private void etatDeFrais()	{

		//		// On ne pourra editer l'etat de frais qu'apres le retour du missionnaire
		if (!currentMission.isMissionPermanente() && DateCtrl.isAfter(currentMission.misFin(), new NSTimestamp())) {

			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","L'édition de l'état de frais sera possible seulement lorsque le missionnaire sera rentré ("
					+ DateCtrl.dateToString(currentMission.misFin(), "%d/%m/%Y %H:%M") + ")");
			return;
		}

		// On doit avoir sorti l'ordre de mission avant d'editer l'etat de frais
		if ("VALIDE".equals(currentPaiement.mipEtat())) {
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Veuillez d'abord sortir l'ordre de mission !");
			return;
		}

		if (!PaiementAvecFraisCtrl.sharedInstance(ec, this).testDonneesBudgetaires())
			return;

		try {

			// Mise a jour de l'engagement
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
			Number mipOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentPaiement).objectForKey("mipOrdre");

			String msg = ServerProxy.engagerEtatFrais(ec, mipOrdre, utlOrdre);				

			if (!msg.equals("OK")) {
				MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
				return;
			}

			ReportsCtrl.sharedInstance(ec).printEtatFrais(currentMission, currentPaiement, new JFrame());

		}
		catch (Exception e) {
			ec.revert();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur lors du changement d'état budgétaire de la mission !");
			e.printStackTrace();
		}

		EnteteMission.sharedInstance(ec).rafraichirMission();	

	}


	/**
	 * Impression simple de l'ordre de mission
	 * 
	 * @param sender
	 */
	private void imprimerOrdreDeMission()	{

		if (currentMission.isMissionPaiement()) {
			if (!PaiementAvecFraisCtrl.sharedInstance(ec, this).testDonneesBudgetaires())
				return;
		}

		printAutorisationVehicule(currentMission);

		ReportsCtrl.sharedInstance(ec).printOrdreMission(currentMission, currentPaiement, Superviseur.sharedInstance(ec).mainFrame());

	}

	/**
	 * Impression simple de l'etat de frais
	 * 
	 */
	private void imprimerEtatDeFrais()	{

		// On ne pourra editer l'etat de frais qu'apres le retour du missionnaire
		if (!currentMission.isMissionPermanente() && DateCtrl.isAfter(currentMission.misFin(), new NSTimestamp())) {

			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","L'édition de l'état de frais sera possible seulement lorsque le missionnaire sera rentré ("
					+ DateCtrl.dateToString(currentMission.misFin(), "%d/%m/%Y %H:%M") + ")");
			return;

		}

		if (currentMission.isMissionPaiement()) {
			if (!PaiementAvecFraisCtrl.sharedInstance(ec, this).testDonneesBudgetaires())
				return;
		}

		ReportsCtrl.sharedInstance(ec).printEtatFrais(currentMission, currentPaiement, Superviseur.sharedInstance(ec).mainFrame());

	}



	/**
	 * Edition de l'ordre de mission
	 * Mise a jour de l'engagement de la mission 
	 */
	private void ordreDeMission() {

		if (currentMission.isMissionPaiement())	{
			if (!PaiementAvecFraisCtrl.sharedInstance(ec, this).testDonneesBudgetaires())	
				return;
		}
		else		
			if (!currentMission.isSaisieLbud() && currentRib != null)	{
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Vous ne devez pas avoir de RIB associé pour ce type de mission !");
				return;
			}		

		// Creation des engagements
		String msg = "OK";

		Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);
		Number mipOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentPaiement).objectForKey("mipOrdre");

		if (currentMission.isMissionPaiement()) {
			msg = ServerProxy.engagerMission(ec, mipOrdre, utlOrdre);				
		}
		else {
			msg = ServerProxy.desengagerMission(ec, mipOrdre, utlOrdre);
		}

		if (!msg.equals("OK")) {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
			try {
				currentPaiement.setMipInfos(null);
				ec.saveChanges();
			}
			catch (Exception e) {
				e.printStackTrace();
			}

			return;
		}

		if (currentMission.isMissionPaiement() || currentMission.isSaisieLbud())
			PaiementAvecFraisCtrl.sharedInstance(ec, this).invaliderObjets();

		EnteteMission.sharedInstance(ec).rafraichirMission();

		// ENGAGEMENT OK : Impression des autorisations de vehicules personnels et de l'ordre de mission
		if (currentPaiement.mipInfos() == null && msg.equals("OK")) {

			// Impression des autorisations de mission pour les vehicules personnels
			printAutorisationVehicule(currentMission);

			//			NSArray avances = FinderMissionAvance.findAvancesForMission(ec, currentMission);
			//
			//			if (avances.count() > 0)
			//				ReportsCtrl.sharedInstance(ec).printDemandeAvance(currentMission, window());

			// Impression de l'ordre de mission
			ReportsCtrl.sharedInstance(ec).printOrdreMission(currentMission, currentPaiement, Superviseur.sharedInstance(ec).mainFrame());			

		}
		else {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR 2","Impossible d'engager la mission.\n " + currentPaiement.mipInfos());
			return;
		}
	}


	/**
	 * 
	 * @param mission
	 */
	private void printAutorisationVehicule(EOMission mission)	{

		NSArray<EOSegmentTarif> segments = currentMission.segmentsTarif();

		int index = 1;
		for (EOSegmentTarif mySegment : segments)	{

			NSArray<EOTransports> transports = mySegment.transports();
			for (EOTransports myTransport : transports) {

				if (myTransport.vehicule() != null && 
						(myTransport.typeTransport().isVehiculePersonnel() || myTransport.typeTransport().isVehiculeSncf()))	{
					ReportsCtrl.sharedInstance(ec).printAutorisationVehicule(getCurrentMission(), myTransport, index, Superviseur.sharedInstance(ec).mainFrame());
					index++;
				}


			}
		}
	}




}
