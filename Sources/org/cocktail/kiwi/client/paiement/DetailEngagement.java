/*
 * Created on 1 mars 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.paiement;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.editions.ReportsCtrl;
import org.cocktail.kiwi.client.factory.FactoryMissionReimput;
import org.cocktail.kiwi.client.factory.FactoryMissionReimputLolf;
import org.cocktail.kiwi.client.finders.FinderMissionPaiementDestin;
import org.cocktail.kiwi.client.finders.FinderMissionReimput;
import org.cocktail.kiwi.client.finders.FinderMissionReimputLolf;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOMandat;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDepense;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDestin;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionReimput;
import org.cocktail.kiwi.client.metier.EOMissionReimputLolf;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.metier.budget.EOEngage;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.nibctrl.DetailEngagementView;
import org.cocktail.kiwi.client.select.LbudSelectCtrl;
import org.cocktail.kiwi.common.utilities.AskForBigDecimal;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DetailEngagement {

	private static DetailEngagement sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;

	private DetailEngagementView myView;

	public EODisplayGroup eodEngage, eodDepensesEngage, eodDepenses, eodMandat;
	ListenerEngagements listenerEngagements = new ListenerEngagements();

	private EOMissionPaiementEngage currentPaiementEngage;
	private	EOMissionPaiement currentPaiement;

	private	EOEngage	currentEngage;
	private	EODepense   currentDepenseEngage;
	private EOMission   currentMission;
	private	EOMandat 	currentMandat;

	/** 
	 * Constructeur 
	 */
	public DetailEngagement(EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		eodEngage = new EODisplayGroup();
		eodDepenses = new EODisplayGroup();
		eodDepensesEngage = new EODisplayGroup();
		eodMandat = new EODisplayGroup();

		myView = new DetailEngagementView(new JFrame(), true, eodEngage, eodDepensesEngage, eodDepenses, eodMandat);

		myView.getBtnDelEngagement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delEngage();
			}
		});

		myView.getBtnUpdateEngagement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateEngage();
			}
		});

		myView.getBtnDelDepense().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delDepense();
			}
		});

		myView.getBtnReverser().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reverser();
			}
		});

		myView.getButtonClose().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fermer();
			}
		});

		myView.getBtnReimputation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reimputer();
			}
		});

		myView.getMyEOTableEngagements().addListener(new ListenerEngagements());
		myView.getMyEOTableDepensesEngage().addListener(new ListenerDepensesEngage());
		myView.getMyEOTableMandats().addListener(new ListenerMandats());

		myView.getMyEOTableDepenses().setEnabled(false);
		
		eodDepenses.setSortOrderings(new NSArray(new EOSortOrdering(EOMissionPaiementDepense.DEPENSE_KEY + "." 
				+ EODepense.TO_EXERCICE_KEY + "." +
				EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending)));
		
	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static DetailEngagement sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new DetailEngagement(editingContext);
		return sharedInstance;
	}

	public void updateEngage() {

		try {

			if (!testDonneesBudgetaires())
				return;

			BigDecimal montantEngagement = AskForBigDecimal.sharedInstance().getMontant("Engagement", "Montant de l'engagement : ", null);

			NSMutableDictionary parametres = new NSMutableDictionary();

			Number mipOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentPaiement).objectForKey("mipOrdre");
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");

			parametres.setObjectForKey(mipOrdre, "mipOrdre");
			parametres.setObjectForKey(utlOrdre, "utlOrdre");				
			parametres.setObjectForKey(montantEngagement, "montantReste");

			ServerProxy.clientSideRequestUpdateEngage(ec, parametres);

			if (currentEngage != null)
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentEngage)));

			EnteteMission.sharedInstance(ec).rafraichirMission();

		}
		catch (Exception ex)	{
			ec.revert();
			ex.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur de mise a jour de l'engagement ! \n" + CocktailUtilities.getErrorDialog(ex));
		}


	}



	private void delEngage()	 {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous solder l'engagement associé à cette mission ?",
				"OUI", "NON"))
			return;   	

		try {
			// On detruit tous les anciens engagements
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
			Number mpeOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentPaiementEngage).objectForKey("mpeOrdre");

			String msg = ServerProxy.desengagerMissionPartiel(ec, mpeOrdre, utlOrdre);
			boolean success = msg.equals("OK");
			if (!success) {
				MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
				return;
			}

			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPaiementEngage)));

			currentPaiementEngage.setEngageRelationship(null);

			ec.saveChanges();

			EnteteMission.sharedInstance(ec).rafraichirMission();

			myView.setVisible(false);
		}
		catch (Exception e)	{
			e.printStackTrace();
		}
	}



	public void reimputer() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (currentPaiementEngage.organ() != null)
			parametres.setObjectForKey(currentPaiementEngage.organ(), EOOrgan.ENTITY_NAME);

		if (currentPaiementEngage.toTypeCredit() != null)
			parametres.setObjectForKey(currentPaiementEngage.toTypeCredit(), EOTypeCredit.ENTITY_NAME);

		if (currentPaiementEngage.codeAnalytique() != null)	
			parametres.setObjectForKey(currentPaiementEngage.codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);

		if (currentPaiementEngage.convention() != null)	
			parametres.setObjectForKey(currentPaiementEngage.convention(), EOConvention.ENTITY_NAME);

		if (currentPaiementEngage.modePaiement() != null)	
			parametres.setObjectForKey(currentPaiementEngage.modePaiement(), EOModePaiement.ENTITY_NAME);

		if (currentPaiementEngage.planComptable() != null)	
			parametres.setObjectForKey(currentPaiementEngage.planComptable(), EOPlanComptable.ENTITY_NAME);

		if (currentPaiementEngage.toCodeExer() != null)	
			parametres.setObjectForKey(currentPaiementEngage.toCodeExer(), EOCodeExer.ENTITY_NAME);

		parametres.setObjectForKey(currentPaiementEngage.mpePourcentage(), "quotite");

		NSArray lolfs = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(ec, currentPaiementEngage);
		if (lolfs.count() > 0) {
			NSMutableArray actions = new NSMutableArray();

			for (int i = 0;i<lolfs.count();i++) {

				EOMissionPaiementDestin lolf = (EOMissionPaiementDestin)lolfs.objectAtIndex(i);

				NSMutableDictionary myDico = new NSMutableDictionary();
				myDico.setObjectForKey(lolf.typeAction(), EOLolfNomenclatureDepense.ENTITY_NAME);
				myDico.setObjectForKey(lolf.typeAction().lolfCode(), "LOLF_CODE");
				myDico.setObjectForKey(lolf.typeAction().lolfLibelle(), "LOLF_LIBELLE");
				myDico.setObjectForKey(lolf.mpdPourcentage(), "POURCENTAGE");

				actions.addObject(myDico);
			}

			parametres.setObjectForKey(actions, "ACTIONS");

		}

		// Selection des nouvelles donnees budgetaires
		NSApp.setGlassPane(true);
		LbudSelectCtrl.sharedInstance(ec).setTitle("Réimputation Budgétaire");
		NSDictionary dicoReimputation = LbudSelectCtrl.sharedInstance(ec).updateLbud(currentPaiementEngage.toExercice(), parametres, false);
		NSApp.setGlassPane(false);

		EOMissionReimput reimputation = null;

		try {

			if (dicoReimputation != null) {

				EOOrgan organ = (EOOrgan)dicoReimputation.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoReimputation.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoReimputation.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoReimputation.objectForKey(EOConvention.ENTITY_NAME);
				EOPlanComptable planComptable = (EOPlanComptable)dicoReimputation.objectForKey(EOPlanComptable.ENTITY_NAME);
				EOCodeExer codeExer = (EOCodeExer)dicoReimputation.objectForKey(EOCodeExer.ENTITY_NAME);

				reimputation = FactoryMissionReimput.sharedInstance().creer(ec, currentPaiementEngage.missionPaiement(), currentDepenseEngage);				

				FactoryMissionReimput.sharedInstance().update(currentPaiementEngage.toExercice(), reimputation, typeCredit, 
						organ, planComptable, convention, codeAnalytique, codeExer);

				// Actions
				NSArray actionsLolf = (NSArray)dicoReimputation.objectForKey("LOLF");

				for (int i=0;i<actionsLolf.count();i++) {

					NSDictionary myDico = (NSDictionary)actionsLolf.objectAtIndex(i);

					FactoryMissionReimputLolf.sharedInstance().
					creer(ec, reimputation, 
							(EOLolfNomenclatureDepense)myDico.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME),
							new BigDecimal(myDico.objectForKey("POURCENTAGE").toString()));
				}


				ec.saveChanges();


				// Reimputation
				
				Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
				Number misOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentMission).objectForKey("misOrdre");
				Number depId = currentDepenseEngage.depId();//(Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentDepense).objectForKey(EOMissionPaiementDepense.DEPENSE_KEY+"."+EODepense.DEP_ID_COLKEY);
				ServerProxy.reimputerMission(ec, depId, misOrdre, utlOrdre);

				// Recuperation de la nouvelle reimputation				
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(reimputation)));
				EOMissionReimput newReimputation = FinderMissionReimput.findReimputation(ec, currentPaiement);

				// Impression du certificat de reimputation
				ReportsCtrl.sharedInstance(ec).printReimputaiton(newReimputation, Superviseur.sharedInstance(ec).mainFrame());			

				EnteteMission.sharedInstance(ec).rafraichirMission();

			}

		}
		catch(Exception ex) {

			// Annulation de la reimputation
			try {

				NSArray reimputLolfs = FinderMissionReimputLolf.findLolfs(ec, reimputation);
				for (int i=0;i<reimputLolfs.count();i++)
					ec.deleteObject((EOMissionReimputLolf)reimputLolfs.objectAtIndex(i));

				ec.deleteObject(reimputation);

				ec.saveChanges();
			}
			catch (Exception e) {

			}

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
			ex.printStackTrace();

		}
	}



	/**
	 * Generation d un reversement sur la depense selectionnee.
	 * 
	 * @param sender
	 */
	private void reverser()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous réellement reverser tout ou partie de cette mission ?",
				"OUI", "NON"))
			return;	

		try {

			// Montant a reverser ???
			BigDecimal montantReversement= new BigDecimal(0);



			String valeurSaisie="";
			boolean saisieOK = false;

			while (valeurSaisie != null && !saisieOK)	{
				valeurSaisie = AskForBigDecimal.sharedInstance().getMontant("Reversement", "Montant de la somme à reverser (<= " + currentDepenseEngage.depTtcSaisie().toString() + ")", currentDepenseEngage.depTtcSaisie()).toString();
				if (valeurSaisie != null)	{
					try {
						valeurSaisie = (NSArray.componentsSeparatedByString(valeurSaisie,",")).componentsJoinedByString(".");

						BigDecimal decimalValue = new BigDecimal(valeurSaisie);
						saisieOK = true;
					}
					catch (Exception ex)	{
						MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur du format de saisie. Veuillez entrer un nombre valide.");
					}					
				}
			}			

			if (valeurSaisie == null)
				return;

			montantReversement = new BigDecimal(valeurSaisie);

			if (montantReversement.floatValue() > 0)	{
				montantReversement = montantReversement.multiply(new BigDecimal(-1));
			}				

			// On teste que le montant ne soit pas superieur au montant de la depense
			if ( (montantReversement.multiply(new BigDecimal(-1)).floatValue() > currentDepenseEngage.depTtcSaisie().floatValue()))	{

				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Vous ne pouvez pas faire reverser un montant > à celui de la dépense sélectionnée !");
				return;

			}

			Number mipOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentPaiement).objectForKey("mipOrdre");
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
			Number depId = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentDepenseEngage).objectForKey("depId");

			String msg = ServerProxy.reverser(ec, mipOrdre, montantReversement, utlOrdre, depId);

			boolean success = msg.equals("OK");
			if (!success) {
				MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
				return;
			}

			EnteteMission.sharedInstance(ec).rafraichirMission();

			listenerEngagements.onSelectionChanged();
			actualiser();

			updateUI();
		}
		catch (Exception e)	{
			e.printStackTrace();
		}

	}


	/**
	 * Suppression de la depense selectionnee ==> Revalidation de la mission
	 * 
	 * @param sender
	 */
	private void delDepense()	 {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous annuler la liquidation associée à cette mission ?\nLa mission redeviendra alors valide.",
				"OUI", "NON"))
			return;   	

		try {

			Number depid = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentDepenseEngage).objectForKey("depId");
			ServerProxy.delDepense(ec, depid);

			EnteteMission.sharedInstance(ec).rafraichirMission();

			actualiser();
			listenerEngagements.onSelectionChanged();

			updateUI();
		}
		catch (Exception e)	{
			e.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(e));
		}
	}

	
	public void actualiser() {

		NSMutableArray<EOMissionPaiementDepense> depenses = new NSMutableArray();

		NSArray<EOMissionPaiementDepense> depensesMission = EOMissionPaiementDepense.findDepensesForMission(ec, currentMission);
		NSArray<EOMissionPaiementDepense> depensesEngage = EOMissionPaiementDepense.findDepensesForEngageBudget(ec, currentEngage);

		for (EOMissionPaiementDepense myPaiementDepense : depensesEngage) {
			if (!depenses.containsObject(myPaiementDepense))
				depenses.addObject(myPaiementDepense);
		}

		for (EOMissionPaiementDepense myPaiementDepense : depensesMission) {
			if (!depenses.containsObject(myPaiementDepense))
				depenses.addObject(myPaiementDepense);			
		}

		
		eodDepenses.setObjectArray(depenses.immutableClone());
		myView.getMyEOTableDepenses().updateData();
		myView.getMyTableModelDepenses().fireTableDataChanged();

		BigDecimal totalLiquidations = new BigDecimal(0);
		for (EOMissionPaiementDepense myPaiementDepense : depenses)
			totalLiquidations = totalLiquidations.add(myPaiementDepense.depense().depTtcSaisie());
		
		myView.getLblTotalLiquidations().setText("TOTAL DEPENSES : " + totalLiquidations + " " + CocktailConstantes.STRING_EURO);
		
	}


	/**
	 * 
	 *
	 */
	public void open(EOMissionPaiement missionPaiement, NSArray engagements)	{

		try {

			currentPaiement = missionPaiement;
			currentMission = currentPaiement.mission();

			myView.setTitle("MISSION No " + currentMission.misNumero() + " - " + currentMission.fournis().nom() + " - Gestion des Engagements / Dépenses / Mandats");
			myView.getTfTitreEngagements().setText("Détail Engage/Dépense de la mission No "+currentMission.misNumero());

			eodEngage.setObjectArray(engagements);
			myView.getMyEOTableEngagements().updateData();
			
			actualiser();

			updateUI();
			myView.setVisible(true);
		}
		catch (Exception ex)	{
			ex.printStackTrace();
		}

	}


	/**
	 * 
	 *
	 */
	public void updateUI()	{

		myView.getBtnDelEngagement().setEnabled(currentEngage != null && currentMission != null && 
				!currentMission.isPayee());

		myView.getBtnUpdateEngagement().setEnabled(currentMission != null && 
				!currentMission.isPayee() && currentMission.isMissionPermanente());

		myView.getBtnDelDepense().setEnabled(currentMission != null //&& currentMission.isPayee() 
				&& currentDepenseEngage != null 
				&& NSApp.hasFonction(EOFonction.ID_FCT_DEL_DEPENSE)
				&& (currentMandat == null || currentMandat.manEtat().equals("ANNULE")) );

		myView.getBtnReimputation().setEnabled(currentMission != null //&& currentMission.isPayee() 
				&& currentDepenseEngage != null 
				&& !(currentDepenseEngage.toExercice().exeStat().equals("C"))
				&& NSApp.hasFonction(EOFonction.ID_FCT_REIMPUTATION));

		BigDecimal montantDepenses = new BigDecimal(0);
		BigDecimal montantOrs = new BigDecimal(0);

		for (int i=0;i<eodDepensesEngage.displayedObjects().count();i++)	{			
			EODepense depense = ((EOMissionPaiementDepense)eodDepensesEngage.displayedObjects().objectAtIndex(i)).depense();

			if (depense.depTtcSaisie().floatValue() >= 0)
				montantDepenses = montantDepenses.add(depense.depTtcSaisie());
			else
				montantOrs = montantOrs.add(depense.depTtcSaisie());				
		}

		montantOrs = montantDepenses.multiply(new BigDecimal(-1));

		myView.getBtnReverser().setEnabled(currentMandat != null
				&& !(currentDepenseEngage.toExercice().exeStat().equals("C"))
				&& (currentMandat.manEtat().equals("VISE") || currentMandat.manEtat().equals("PAYE"))
				&& NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION)
				&& currentDepenseEngage.depTtcSaisie().floatValue() > 0
				&& (montantOrs.floatValue()) < montantDepenses.floatValue());

	}



	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerEngagements implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentPaiementEngage = (EOMissionPaiementEngage)eodEngage.selectedObject();
			eodDepensesEngage.setObjectArray(new NSArray());
			
			if (currentPaiementEngage != null) {
				currentEngage = currentPaiementEngage.engage();
				eodDepensesEngage.setObjectArray(EOMissionPaiementDepense.findDepensesForEngageBudget(ec, currentEngage));
			}

			myView.getMyEOTableDepensesEngage().updateData();

			updateUI();
		}
	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerDepensesEngage implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentDepenseEngage = null;
			eodMandat.setObjectArray(new NSArray());

			if (eodDepensesEngage.selectedObjects().count() > 0) {
				currentDepenseEngage = ((EOMissionPaiementDepense)eodDepensesEngage.selectedObject()).depense();
				eodMandat.setObjectArray(EOMandat.findForDepense(ec, currentDepenseEngage));
			}

			myView.getMyEOTableMandats().updateData();
			updateUI();
		}
	}


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerMandats implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentMandat = (EOMandat)eodMandat.selectedObject();

			updateUI();
		}
	}


	/**
	 * 
	 * @param sender
	 */
	public void fermer()	{
		myView.setVisible(false);
	}


	public boolean testDonneesBudgetaires()	{

		if ("DEMANDE D'AUTORISATION D'INVITATION".equals(currentMission.titreMission().titLibelle())) 
			return true;

		BigDecimal totalMontantBudgetaire = new BigDecimal(0), totalMontantRembourse = new BigDecimal(0);

		EOMissionPaiementEngage paiement = currentPaiementEngage;

		NSArray destins = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(ec, paiement);

		// La ligne budgetaire, le compte d'imputation, le type de credit et la destination sont obligatoires.
		if (	(paiement.organ() == null) 
				|| (paiement.modePaiement() == null) || paiement.modePaiement().modCode() == null
				||	(destins.count() == 0) 
				||	(paiement.planComptable() == null)
				||	(paiement.toTypeCredit() == null))	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Les informations budgétaires associées à cette mission sont incomplètes !");			
			return false;
		}

		if (paiement.organ().orgNiveau().intValue() < 3) {
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Vous ne pouvez pas engager sur une ligne budgétaire de niveau " + paiement.organ().orgNiveau() + " !");			
			return false;				
		}

		if (paiement.engage() != null) {

			if ( paiement.engage()!= null && paiement.toExercice().exeExercice().intValue() !=
				paiement.engage().exeOrdre().intValue() ) {

				MsgPanel.sharedInstance().runInformationDialog("ERREUR","L'engagement doit être basé sur l'exercice " +
						paiement.toExercice().exeExercice() + " et non sur " + paiement.engage().exeOrdre().intValue() + " !");			

				return false;				

			}				
		}

		if ( paiement.toExercice().exeExercice().intValue() !=
			paiement.toTypeCredit().exercice().exeExercice().intValue() ) {

			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Le type de crédit doit être basé sur l'exercice " +
					paiement.toExercice().exeExercice() + " et non sur " + paiement.toTypeCredit().exercice().exeExercice() + " !");			

			return false;				

		}

		if ( paiement.toExercice().exeExercice().intValue() !=
			paiement.modePaiement().toExercice().exeExercice().intValue() ) {

			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Le mode de paiement doit être basé sur l'exercice " +
					paiement.toExercice().exeExercice() + " et non sur " + paiement.modePaiement().toExercice().exeExercice() + " !");			

			return false;				

		}


		totalMontantBudgetaire = totalMontantBudgetaire.add(paiement.mpeMontantBudgetaire());
		totalMontantRembourse = totalMontantRembourse.add(paiement.mpeMontantRembourse());

		// Le total des montants budgetaires ou rembourses doivent etre egaux au montant global de la mission
		if (totalMontantBudgetaire.floatValue() < 0.0)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR"," Le montant budgétaire doit être supérieur à 0 !");			
			return false;
		}

		return true;
	}

}
