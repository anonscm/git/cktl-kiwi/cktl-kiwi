/*
 * Created on 23 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.paiement;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.admin.GestionPreferencesPerso;
import org.cocktail.kiwi.client.factory.FactoryMissionPaiementDestin;
import org.cocktail.kiwi.client.factory.FactoryMissionPaiementEngage;
import org.cocktail.kiwi.client.finders.FinderMissionPaiementDestin;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDestin;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOMissionPreferencesPerso;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.metier.budget.EOLolfNomenclatureDepense;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.nibctrl.PaiementAvecFraisView;
import org.cocktail.kiwi.client.select.ExerciceSelectCtrl;
import org.cocktail.kiwi.client.select.LbudSelectCtrl;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailFormats;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PaiementAvecFraisCtrl extends EODialogController	{

	private static PaiementAvecFraisCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext edc;

	private PaiementCtrl ctrlPaiement;
	private PaiementAvecFraisView myView;

	private EODisplayGroup eodEngage = new EODisplayGroup();
	private EODisplayGroup eodLolf = new EODisplayGroup();

	private	EOMission currentMission;
	private	EOMissionPaiement currentPaiement;
	private	EOMissionPaiementEngage currentPaiementEngage;
	//	private	EOEngage currentEngagement;

	private boolean multiLignesBudgetaires = false;

	/** 
	 * Constructeur 
	 */
	public PaiementAvecFraisCtrl(EOEditingContext edc, PaiementCtrl ctrlPaiement)	{

		super();

		myView = new PaiementAvecFraisView(eodEngage, eodLolf);
		this.edc = edc;
		this.ctrlPaiement = ctrlPaiement;

		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterLbud();
			}
		});

		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierLbud();
			}
		});

		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerLbud();
			}
		});

		myView.getBtnClean().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cleanLbud();
			}
		});

		myView.getBtnExercice().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateExercice();
			}
		});

		myView.getBtnDetailEngagement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				afficherDetailEngagement();
			}
		});


		if (NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION) && !NSApp.hasFonction(EOFonction.ID_FCT_CREATION))	{

			myView.getBtnModifier().setEnabled(false);
			myView.getBtnClean().setEnabled(false);
			myView.getBtnDetailEngagement().setEnabled(false);

		}

		myView.getMyEOTableEngage().addListener(new ListenerEngage());
		myView.getMyEOTableLolf().addListener(new ListenerLolf());

		myView.getCheckChargeAPayer().addItemListener(new CAPItemListener());

	}	

	/**
	 * 
	 * @param exercice
	 */
	public void setParametrages(EOExercice exercice) {

		String paramMultiLbud = FinderMissionParametres.getValue(edc, exercice, EOMissionParametres.ID_CHECK_MULTI_LBUD);
		if (paramMultiLbud == null)
			paramMultiLbud = EOMissionParametres.DEFAULT_VALUE_CHECK_MULTI_LBUD;
		multiLignesBudgetaires = (paramMultiLbud.equals("O"))?true:false;
		String paramCap = FinderMissionParametres.getValue(edc, exercice, EOMissionParametres.ID_GESTION_CAP);
		if (paramCap == null)
			paramCap = EOMissionParametres.DEFAULT_VALUE_GESTION_CAP;
		if (paramCap.equals(EOMissionParametres.CODE_SANS_CAP))
			myView.getCheckChargeAPayer().setVisible(false);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PaiementAvecFraisCtrl sharedInstance(EOEditingContext editingContext,PaiementCtrl ctrlPaiement)	{
		if (sharedInstance == null)	
			sharedInstance = new PaiementAvecFraisCtrl(editingContext, ctrlPaiement);
		return sharedInstance;
	}

	/**
	 * 
	 * @return
	 */
	public JPanel view()	{
		return myView;
	}


	public BigDecimal getTotalPourcentage() {

		NSMutableArray<EOMissionPaiementEngage> engagements = new NSMutableArray<EOMissionPaiementEngage>();

		for (EOMissionPaiementEngage myEngage : (NSArray<EOMissionPaiementEngage>)eodEngage.displayedObjects()) {
			engagements.addObject(myEngage);
		}

		return CocktailUtilities.computeSumForKey(engagements, EOMissionPaiementEngage.MPE_POURCENTAGE_KEY);

	}

	public void invaliderObjets() {

		edc.invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(edc.globalIDForObject(currentPaiement)));
		edc.invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(edc.globalIDForObject(currentPaiementEngage)));

		if (currentPaiementEngage.engage() != null)
			edc.invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(edc.globalIDForObject(currentPaiementEngage.engage())));

	}

	public EOExercice exerciceBudgetaire() {

		if (eodEngage.displayedObjects().count() > 0)
			return ((EOMissionPaiementEngage)eodEngage.displayedObjects().objectAtIndex(0)).toExercice();

		return null;
	}


	public EOMissionPaiementEngage getFirstEngagement() {

		if (eodEngage.displayedObjects().count() > 0)
			return(EOMissionPaiementEngage)eodEngage.displayedObjects().objectAtIndex(0);

		return null;

	}

	private void cleanLbud()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de toutes les infos budgétaires liées à la mission ?",
				"OUI", "NON")) 
			return;

		NSApp.setWaitCursor();

		try {				

			currentPaiement.setMipEtat(EOMissionPaiement.ETAT_VALIDE);

			for (int i=0;i<eodEngage.displayedObjects().count();i++)	{

				EOMissionPaiementEngage paiement = (EOMissionPaiementEngage)eodEngage.displayedObjects().objectAtIndex(i);

				paiement.setOrganRelationship(null);
				paiement.setToTypeCreditRelationship(null);
				paiement.setModePaiementRelationship(null);
				paiement.setPlanComptableRelationship(null);

				paiement.setMpeEtat(EOMissionPaiementEngage.ETAT_VALIDE);

				NSArray destins = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(edc, paiement);
				for (int j=0;j<destins.count();j++)	{
					EOMissionPaiementDestin destin = (EOMissionPaiementDestin)destins.objectAtIndex(j);
					destin.removeObjectFromBothSidesOfRelationshipWithKey(paiement, EOMissionPaiementDestin.MISSION_PAIEMENT_ENGAGE_KEY);
					edc.deleteObject(destin);
				}
			}
			edc.saveChanges();

			// On detruit tous les anciens engagements
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(edc, NSApp.getUtilisateur()).objectForKey("utlOrdre");
			Number mpeOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(edc, currentPaiementEngage).objectForKey("mpeOrdre");
			String msg = ServerProxy.desengagerMissionPartiel(edc, mpeOrdre, utlOrdre);
			boolean success = msg.equals("OK");
			if (!success) {
				MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
				NSApp.setDefaultCursor();
				return;
			}

		}
		catch (Exception e)	{

			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur de suppression des données budgétaires !");
			e.printStackTrace();
			edc.revert();			
		}

		updateData();
		updateUI();

		NSApp.setDefaultCursor();

	}


	public void setDisabled()	{

		cleanDonneesBudgetaires();

		myView.getBtnAjouter().setVisible(false);
		myView.getBtnModifier().setVisible(false);
		myView.getBtnClean().setVisible(false);

	}


	/**
	 * 
	 *
	 */
	private void cleanDonneesBudgetaires()	{

		eodLolf.setObjectArray(new NSArray());
		myView.getMyEOTableLolf().updateData();

		myView.getTfEngNumero().setText("");
		myView.getTfEngMontant().setText("");
		myView.getTfEngDate().setText("");
		myView.getLblDispo().setText("");

	}


	public void clean()	{

		eodEngage.setObjectArray(new NSArray());
		eodLolf.setObjectArray(new NSArray());

		myView.getMyEOTableEngage().updateData();
		myView.getMyEOTableLolf().updateData();

	}


	public void actualiser(EOMission mission, EOMissionPaiement paiement)	{

		currentMission = mission;
		currentPaiement = paiement;

		setParametrages(mission.toExercice());

		clean();
		updateData();
		updateUI();		

	}


	/**
	 * 
	 *
	 */
	private void updateData()	{

		myView.getCheckChargeAPayer().setSelected(currentPaiement.mipCap().equals("O"));

		eodEngage.setObjectArray(EOMissionPaiementEngage.findForMission(edc, currentMission));

		myView.getMyEOTableEngage().updateData();

	}


	public void updateUI()	{

		NSArray depenses = EODepense.findDepensesForPaiementEngage(edc,currentPaiementEngage);

		myView.getCheckChargeAPayer().setEnabled( currentMission != null
				&& currentPaiement != null
				&& NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION)
				&& !currentMission.isPayee()
				&& !currentMission.isMissionPermanente()
		);

		myView.getBtnAjouter().setVisible( currentMission != null 
				&& !currentMission.isPayee()
				&& (multiLignesBudgetaires || eodEngage.displayedObjects().count() == 0) 
				&& !currentMission.isMissionPermanente()
		);

		myView.getBtnModifier().setVisible(!currentMission.isPayee() 
				&& currentPaiementEngage != null  && !currentPaiementEngage.estLiquide()
				&& depenses.count() == 0 
				&& NSApp.hasFonction(EOFonction.ID_FCT_CREATION) );

		myView.getBtnSupprimer().setVisible(!currentMission.isPayee() 
				&& currentPaiementEngage != null   && !currentPaiementEngage.estLiquide()
				&& depenses.count() == 0 
				&& NSApp.hasFonction(EOFonction.ID_FCT_CREATION) );

		myView.getBtnClean().setVisible(!currentMission.isPayee() 
				&& currentPaiementEngage != null  && !currentPaiementEngage.estLiquide()
				&& depenses.count() == 0  
				&& NSApp.hasFonction(EOFonction.ID_FCT_CREATION) );

		myView.getBtnSupprimer().setVisible(!currentMission.isPayee() 
				&& eodEngage.displayedObjects().count() > 1);

		myView.getBtnExercice().setVisible(!currentMission.isPayee() 
				&& currentPaiementEngage != null  && !currentPaiementEngage.estLiquide());

	}

	public void ajouterLbud()	{

		try {

			currentPaiement = EOMissionPaiement.findPaiementForMission(edc, currentMission);

			if (currentPaiement == null)
				currentPaiement =EOMissionPaiement.creer(edc, currentMission, currentMission.toExercice());				

			EOMissionPreferencesPerso preferences = GestionPreferencesPerso.sharedInstance(edc).getPreferences();

			BigDecimal totalPourcentages = CocktailUtilities.computeSumForKey(eodEngage, EOMissionPaiementEngage.MPE_POURCENTAGE_KEY);

			currentPaiementEngage = FactoryMissionPaiementEngage.sharedInstance().creerMissionPaiementEngage(edc, currentPaiement, currentMission.toExercice(), preferences);
			currentPaiementEngage.setMpePourcentage(new BigDecimal(100).subtract(totalPourcentages));

			BigDecimal totalEngagementLbuds = CocktailUtilities.computeSumForKey(eodEngage, EOMissionPaiementEngage.MPE_MONTANT_ENGAGEMENT_KEY);
			BigDecimal totalBudgetaireLbuds = CocktailUtilities.computeSumForKey(eodEngage, EOMissionPaiementEngage.MPE_MONTANT_BUDGETAIRE_KEY);
			BigDecimal totalRembourseLbuds = CocktailUtilities.computeSumForKey(eodEngage, EOMissionPaiementEngage.MPE_MONTANT_REMBOURSE_KEY);

			currentPaiementEngage.setMpeMontantEngagement(PaiementCtrl.sharedInstance(edc).calculEngagement().subtract(totalEngagementLbuds));
			currentPaiementEngage.setMpeMontantBudgetaire(currentPaiement.mipMontantTotal().subtract(totalBudgetaireLbuds));
			currentPaiementEngage.setMpeMontantRembourse(currentPaiement.mipMontantPaiement().subtract(totalRembourseLbuds));

			edc.saveChanges();

		}
		catch (Exception e)	{
			e.printStackTrace();
			edc.revert();
		}

		eodEngage.setObjectArray(EOMissionPaiementEngage.findForMission(edc, currentMission));
		myView.getMyEOTableEngage().updateData();

		updateUI();

	}

	private void modifierLbud()	{

		NSApp.setWaitCursor();

		if (currentPaiementEngage == null || eodEngage.displayedObjects().count() == 0
				|| eodEngage.selectedObject() == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez sélectionner une ligne budgétaire à modifier !");			
			return;
		}

		NSApp.setGlassPane(true);

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (currentPaiementEngage.organ() != null)
			parametres.setObjectForKey(currentPaiementEngage.organ(), EOOrgan.ENTITY_NAME);

		if (currentPaiementEngage.toTypeCredit() != null)
			parametres.setObjectForKey(currentPaiementEngage.toTypeCredit(), EOTypeCredit.ENTITY_NAME);

		if (currentPaiementEngage.codeAnalytique() != null)	
			parametres.setObjectForKey(currentPaiementEngage.codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);

		if (currentPaiementEngage.convention() != null)	
			parametres.setObjectForKey(currentPaiementEngage.convention(), EOConvention.ENTITY_NAME);

		if (currentPaiementEngage.modePaiement() != null)	
			parametres.setObjectForKey(currentPaiementEngage.modePaiement(), EOModePaiement.ENTITY_NAME);

		if (currentPaiementEngage.planComptable() != null)	
			parametres.setObjectForKey(currentPaiementEngage.planComptable(), EOPlanComptable.ENTITY_NAME);

		if (currentPaiementEngage.toCodeExer() != null)	
			parametres.setObjectForKey(currentPaiementEngage.toCodeExer(), EOCodeExer.ENTITY_NAME);

		parametres.setObjectForKey(currentPaiementEngage.mpePourcentage(), "quotite");

		if (eodLolf.displayedObjects().count() > 0) {
			NSMutableArray actions = new NSMutableArray();

			for (int i = 0;i<eodLolf.displayedObjects().count();i++) {

				EOMissionPaiementDestin action = (EOMissionPaiementDestin)eodLolf.displayedObjects().objectAtIndex(i);

				NSMutableDictionary myDico = new NSMutableDictionary();
				myDico.setObjectForKey(action.typeAction(), EOLolfNomenclatureDepense.ENTITY_NAME);
				myDico.setObjectForKey(action.typeAction().lolfCode(), "LOLF_CODE");
				myDico.setObjectForKey(action.typeAction().lolfLibelle(), "LOLF_LIBELLE");
				myDico.setObjectForKey(action.mpdPourcentage(), "POURCENTAGE");

				actions.addObject(myDico);
			}

			parametres.setObjectForKey(actions, "ACTIONS");

		}

		NSApp.setGlassPane(true);
		LbudSelectCtrl.sharedInstance(edc).setTitle("Saisie des informations budgétaires");
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(edc).updateLbud(currentPaiementEngage.toExercice(), parametres, true);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				EOModePaiement modePaiement = (EOModePaiement)dicoSaisie.objectForKey(EOModePaiement.ENTITY_NAME);
				EOPlanComptable planComptable = (EOPlanComptable)dicoSaisie.objectForKey(EOPlanComptable.ENTITY_NAME);
				EOCodeExer codeExer = (EOCodeExer)dicoSaisie.objectForKey(EOCodeExer.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				FactoryMissionPaiementEngage.sharedInstance().updateMissionPaiementEngage(
						currentPaiementEngage.toExercice(), currentPaiementEngage, typeCredit, organ, 
						planComptable, modePaiement,
						convention,  codeAnalytique, codeExer);

				currentPaiementEngage.setMpePourcentage(quotite);

				// Suppression des anciennes actions
				NSArray oldActions = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(edc, currentPaiementEngage);
				for (int a = 0;a<oldActions.count();a++) {
					EOMissionPaiementDestin destin = (EOMissionPaiementDestin)oldActions.objectAtIndex(a);

					destin.setMissionPaiementEngageRelationship(null);
					edc.deleteObject(destin);

				}

				//			// Actions
				NSArray actionsLolf = (NSArray)dicoSaisie.objectForKey("LOLF");

				for (int i=0;i<actionsLolf.count();i++) {

					NSDictionary myDico = (NSDictionary)actionsLolf.objectAtIndex(i);

					FactoryMissionPaiementDestin.sharedInstance().
					creerMissionPaiementDestin(edc, 
							currentPaiementEngage, 
							(EOLolfNomenclatureDepense)myDico.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME),
							new BigDecimal(myDico.objectForKey("POURCENTAGE").toString()));
				}


				// Mise  a jour des donnees budgetaires

				edc.saveChanges();

				eodEngage.updateDisplayedObjects();
				myView.getMyEOTableEngage().updateData();
				myView.getMyEOTableLolf().updateData();

				updateMontants();

				edc.saveChanges();

				updateUI();

			}
			catch (Exception e)	{
				edc.revert();
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur d'enregistrement des données budgétaires !\n"+e.getMessage());	
				e.printStackTrace();
			}

		}

		NSApp.setDefaultCursor();

	}

	/**
	 * 
	 * @param sender
	 */
	private void supprimerLbud()	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de la ligne budgétaire sélectionnée séléctionnée ?",
				"OUI", "NON"))
			return;

		try {

			Number mpeOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(edc, currentPaiementEngage).objectForKey("mpeOrdre");
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(edc, NSApp.getUtilisateur()).objectForKey("utlOrdre");
			String msg = ServerProxy.clientSideRequestDelMissionPaiementEngage(edc, mpeOrdre, utlOrdre);
			boolean success = msg.equals("OK");
			if (!success) {
				MsgPanel.sharedInstance().runErrorDialog("ERREUR",msg);
				return;
			}

			updateData();
			updateMontants();
			updateUI();

		}
		catch (Exception e)	{			
			edc.revert();
			e.printStackTrace();
		}
	}


	public boolean testDonneesBudgetairesLiquidation() {

		if (currentPaiementEngage.mpeMontantBudgetaire().floatValue() > 0) {

			if (currentPaiementEngage.engage() == null) {
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","L'engagement de la mission n'a pas été fait, veuillez recliquer sur 'ORDRE DE MISSION'.");
				return false;
			}

			EOOrgan organMission = currentPaiementEngage.organ();
			EOOrgan organEngage = currentPaiementEngage.engage().organ();

			// Verification de la coherence entre la ligne budgetaire de la mission et celle de l'engagement
			if (!organMission.equals(organEngage)) {
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Les lignes budgétaires de l'engagement et de la mission sont différentes.\nMerci de recliquer sur 'ETAT DE FRAIS' pour les mettre à jour.");
				return false;
			}

		}

		return true;

	}


	public boolean testDonneesBudgetaires()	{

		if (currentPaiement == null) {
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Les informations budgétaires associées à cette mission sont incomplètes (Pas de paiement)!");
			return false;
		}

		if ("DEMANDE D'AUTORISATION D'INVITATION".equals(currentMission.titreMission().titLibelle())) 
			return true;

		BigDecimal totalMontantBudgetaire = new BigDecimal(0), totalMontantRembourse = new BigDecimal(0);

		BigDecimal totalPourcentages = getTotalPourcentage();
		if (totalPourcentages.intValue() != 100)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Le pourcentage global des lignes budgétaires renseignées doit être de 100% !");
			return false;
		}

		EOExercice exerciceCourant;
		EOExercice exercicePrecedent = null;

		for (int i=0;i<eodEngage.displayedObjects().count();i++) {

			EOMissionPaiementEngage localMissionPaiementEngage = (EOMissionPaiementEngage)eodEngage.displayedObjects().objectAtIndex(i);
			exerciceCourant = localMissionPaiementEngage.toExercice();

			if (exercicePrecedent != null && exercicePrecedent.exeExercice().intValue() != exerciceCourant.exeExercice().intValue()) {
				MsgPanel.sharedInstance().runInformationDialog("ERREUR"," Vous ne pouvez avoir 2 exercices budgétaires différents !");			
				return false;				
			}

			if (localMissionPaiementEngage.mpeMontantBudgetaire().floatValue() > 0) {

				NSArray destins = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(edc, localMissionPaiementEngage);

				// La ligne budgetaire, le compte d'imputation, le type de credit et la destination sont obligatoires.
				if (	(localMissionPaiementEngage.organ() == null) 
						|| (localMissionPaiementEngage.modePaiement() == null) || localMissionPaiementEngage.modePaiement().modCode() == null
						||	(destins.count() == 0) 
						||	(localMissionPaiementEngage.toCodeExer() == null) 
						||	(localMissionPaiementEngage.planComptable() == null)
						||	(localMissionPaiementEngage.toTypeCredit() == null))	{
					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Les informations budgétaires associées à cette mission sont incomplètes !");			
					return false;
				}

				if (localMissionPaiementEngage.organ().orgNiveau().intValue() < 3) {
					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Vous ne pouvez pas engager sur une ligne budgétaire de niveau " + localMissionPaiementEngage.organ().orgNiveau() + " !");			
					return false;				
				}

				if (localMissionPaiementEngage.engage() != null) {

					if ( localMissionPaiementEngage.engage()!= null && localMissionPaiementEngage.toExercice().exeExercice().intValue() !=
						localMissionPaiementEngage.engage().exeOrdre().intValue() ) {

						MsgPanel.sharedInstance().runInformationDialog("ERREUR","L'engagement doit être basé sur l'exercice " +
								localMissionPaiementEngage.toExercice().exeExercice() + " et non sur " + localMissionPaiementEngage.engage().exeOrdre().intValue() + " !");			

						return false;				

					}				
				}

				if ( localMissionPaiementEngage.toExercice().exeExercice().intValue() !=
					localMissionPaiementEngage.toTypeCredit().exercice().exeExercice().intValue() ) {

					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Le type de crédit doit être basé sur l'exercice " +
							localMissionPaiementEngage.toExercice().exeExercice() + " et non sur " + localMissionPaiementEngage.toTypeCredit().exercice().exeExercice() + " !");			

					return false;				

				}

				if ( localMissionPaiementEngage.toExercice().exeExercice().intValue() !=
					localMissionPaiementEngage.modePaiement().toExercice().exeExercice().intValue() ) {

					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Le mode de paiement doit être basé sur l'exercice " +
							localMissionPaiementEngage.toExercice().exeExercice() + " et non sur " + localMissionPaiementEngage.modePaiement().toExercice().exeExercice() + " !");			

					return false;			
				}

				if ( localMissionPaiementEngage.toExercice().exeExercice().intValue() !=
					localMissionPaiementEngage.toCodeExer().exercice().exeExercice().intValue() ) {

					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Le code achat doit être basé sur l'exercice " +
							localMissionPaiementEngage.toExercice().exeExercice() + " et non sur " + localMissionPaiementEngage.toCodeExer().exercice().exeExercice() + " !");			

					return false;			
				}


				totalMontantBudgetaire = totalMontantBudgetaire.add(localMissionPaiementEngage.mpeMontantBudgetaire());
				totalMontantRembourse = totalMontantRembourse.add(localMissionPaiementEngage.mpeMontantRembourse());

				exercicePrecedent = exerciceCourant;

			}
		}

		// Le total des montants budgetaires ou rembourses doivent etre egaux au montant global de la mission
		if (totalMontantBudgetaire.floatValue() < 0.0)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR"," Le montant budgétaire doit être supérieur à 0 !");			
			return false;
		}	

		return true;

	}

	private class ListenerEngage implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
			if (currentMission.isValide())
				modifierLbud();			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			cleanDonneesBudgetaires();
			currentPaiementEngage = (EOMissionPaiementEngage)eodEngage.selectedObject();

			if (currentPaiementEngage != null)	{

				if (currentPaiementEngage.engage() != null)	{

					myView.getTfEngNumero().setText(currentPaiementEngage.engage().engNumero().toString());	
					myView.getTfEngMontant().setText(currentPaiementEngage.engage().engTtcSaisie().toString() + " \u20ac");
					myView.getTfEngDate().setText(DateCtrl.dateToString(currentPaiementEngage.engage().engDateSaisie()));

				}

				eodLolf.setObjectArray(FinderMissionPaiementDestin.findDestinationsForPaiementEngage(edc, currentPaiementEngage));
				myView.getMyEOTableLolf().updateData();

				updateDisponible();

			}

			updateUI();

		}
	}


	/**
	 * 
	 *
	 */
	public void updateDisponible()	{

		if (currentPaiementEngage.organ() != null && currentPaiementEngage.toTypeCredit() != null)	{

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey((EOEnterpriseObject)currentPaiementEngage.organ(), "EOOrgan");
			parametres.setObjectForKey((EOEnterpriseObject)currentPaiementEngage.toTypeCredit(), "EOTypeCredit");
			parametres.setObjectForKey(currentPaiementEngage.toExercice(), "EOExercice");

			BigDecimal disponible = (BigDecimal)ServerProxy.clientSideRequestGetDisponible(edc, parametres);

			myView.getLblDispo().setText(CocktailFormats.FORMAT_DECIMAL.format(disponible) + " " + CocktailConstantes.STRING_EURO);

		}
	}


	public void updateExercice() {

		try {

			EOExercice exercice = ExerciceSelectCtrl.sharedInstance(edc).getExerciceOuvert();

			if (exercice != null) {

				NSMutableDictionary parametres = new NSMutableDictionary();

				Number mpeOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(edc, currentPaiementEngage).objectForKey("mpeOrdre");
				Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(edc, NSApp.getUtilisateur()).objectForKey("utlOrdre");

				parametres.setObjectForKey(mpeOrdre, "mpeOrdre");
				parametres.setObjectForKey(exercice.exeExercice(), "exeOrdre");
				parametres.setObjectForKey(utlOrdre, "utlOrdre");				

				ServerProxy.clientSideRequestModifierExerciceBudgetaire(edc, parametres);

				EnteteMission.sharedInstance(edc).rafraichirMission();

			}

		}
		catch (Exception ex)	{
			edc.revert();
			ex.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur de mise a jour de l'exercice budgétaire ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

	}


	/**
	 * 
	 */
	public void updateMontants()	{

		BigDecimal montantEngagementGlobal = PaiementCtrl.sharedInstance(edc).calculEngagement(); 
		BigDecimal montantBudgetaireGlobal = currentPaiement.mipMontantTotal(); 
		BigDecimal montantPaiementGlobal = currentPaiement.mipMontantPaiement(); 

		BigDecimal montantEngagementPartiel = new BigDecimal(0);
		BigDecimal montantBudgetairePartiel = new BigDecimal(0);
		BigDecimal montantPaiementPartiel = new BigDecimal(0);

		BigDecimal totalPourcentages = getTotalPourcentage();

		int index = 1;
		for (EOMissionPaiementEngage paiementEngage : (NSArray<EOMissionPaiementEngage>)eodEngage.displayedObjects()) {

			if (!paiementEngage.estLiquide()) {

				BigDecimal pourcentage = paiementEngage.mpePourcentage().divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);

				if (index == eodEngage.displayedObjects().size() && totalPourcentages.intValue() == 100) {
					paiementEngage.setMpeMontantEngagement(montantEngagementGlobal.subtract(montantEngagementPartiel));
					paiementEngage.setMpeMontantBudgetaire(montantBudgetaireGlobal.subtract(montantBudgetairePartiel));
					paiementEngage.setMpeMontantRembourse(montantPaiementGlobal.subtract(montantPaiementPartiel));
				}
				else	{				
					BigDecimal montantEngagementIsole = (montantEngagementGlobal.multiply(pourcentage)).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);
					BigDecimal montantBudgetaireIsole = (montantBudgetaireGlobal.multiply(pourcentage)).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);
					BigDecimal montantPaiementIsole = (montantPaiementGlobal.multiply(pourcentage)).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);
					montantEngagementPartiel = montantEngagementPartiel.add(montantEngagementIsole);		
					montantBudgetairePartiel = montantBudgetairePartiel.add(montantBudgetaireIsole);		
					montantPaiementPartiel = montantPaiementPartiel.add(montantPaiementIsole);		

					paiementEngage.setMpeMontantEngagement(montantEngagementIsole);
					paiementEngage.setMpeMontantBudgetaire(montantBudgetaireIsole);
					paiementEngage.setMpeMontantRembourse(montantPaiementIsole);
				}

				// Ajout des liquidations

//				if (paiementEngage.engage() != null) {
//
//					BigDecimal montantLiquidations = new BigDecimal(0);
//					NSArray<EODepense> liquidations = EODepense.findDepensesForEngage(edc, paiementEngage.engage());
//					for (EODepense myDepense : liquidations) {
//						montantLiquidations = montantLiquidations.add(myDepense.depTtcSaisie());			
//					}					
//					paiementEngage.setMpeMontantEngagement(paiementEngage.mpeMontantEngagement().add(montantLiquidations));
//				}

				// Mise a jour du montant des actions
				NSArray actions = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(edc, paiementEngage);

				BigDecimal montantMission = new BigDecimal(0);

				if (currentMission.isMissionPermanente() && paiementEngage.engage() != null)
					montantMission = paiementEngage.engage().engTtcSaisie();
				else
					montantMission =  paiementEngage.mpeMontantEngagement();

				BigDecimal montantPartiel = new BigDecimal(0);

				for (int a=0;a<actions.count();a++)	{

					EOMissionPaiementDestin destin = (EOMissionPaiementDestin)actions.objectAtIndex(a);

					if (a == actions.count() - 1)
						destin.setMpdMontant(montantMission.subtract(montantPartiel));
					else	{
						BigDecimal montantAction = (montantMission.multiply(destin.mpdPourcentage())).divide(new BigDecimal(100), ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);
						montantPartiel = montantPartiel.add(montantAction);				
						destin.setMpdMontant(montantAction);
					}
				}							
			}
			index++;
		}

		myView.getMyEOTableEngage().updateUI();
		myView.getMyEOTableLolf().updateUI();

	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerLolf implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
			if (currentMission.isValide())
				modifierLbud();			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

		}
	}	


	private void afficherDetailEngagement()	{

		Superviseur.sharedInstance(edc).setGlassPane(true);

		NSMutableArray<EOMissionPaiementEngage> engagements = new NSMutableArray();
		for (EOMissionPaiementEngage myPaiementEngage : (NSArray<EOMissionPaiementEngage>)eodEngage.displayedObjects())  {
			if (myPaiementEngage.engage() != null) 
				engagements.addObject(myPaiementEngage);
		}

		DetailEngagement.sharedInstance(edc).open(currentPaiement, engagements);
		Superviseur.sharedInstance(edc).setGlassPane(false);

	}


	public class CAPItemListener implements ItemListener 	{
		public void itemStateChanged(ItemEvent e)		{

			try {
				currentPaiement.setMipCap((myView.getCheckChargeAPayer().isSelected())?"O":"N");

				// Si le parametre est CAP EXTOURNE ==> Changement du mode de paiement A EXTOURNER
				ctrlPaiement.updateInterface();

				edc.saveChanges();
			}
			catch (Exception ex){
				ex.printStackTrace();
				edc.revert();
			}				

		}
	} 

}
