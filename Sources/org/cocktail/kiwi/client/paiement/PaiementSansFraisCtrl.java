/*
 * Created on 23 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.paiement;

import javax.swing.JPanel;

import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.budget.EOCodeAnalytique;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.nibctrl.PaiementSansFraisView;
import org.cocktail.kiwi.client.select.CodeAnalytiqueSelectCtrl;
import org.cocktail.kiwi.client.select.ConventionSelectCtrlOld;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class PaiementSansFraisCtrl extends EODialogController	{

	private static PaiementSansFraisCtrl sharedInstance;

	private EOEditingContext ec;

	private PaiementSansFraisView myView;

	private EOMission currentMission;
	private EOMissionPaiementEngage currentPaiementEngage;
	private EOCodeAnalytique currentCanal;
	private EOConvention currentConvention;


	public PaiementSansFraisCtrl(EOEditingContext editingContext)	{

		super();

		myView = new PaiementSansFraisView();
		ec = editingContext;

		myView.getBtnGetCodeAnalytique().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getCodeAnalytique();
			}
		});

		myView.getBtnGetConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getConvention();
			}
		});

		myView.getBtnDelCodeAnlaytique().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delCodeAnalytique();
			}
		});

		myView.getBtnDelConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delConvention();
			}
		});

		CocktailUtilities.initTextField(myView.getTfCodeAnalytique(), false, false);
		CocktailUtilities.initTextField(myView.getTfConvention(), false, false);

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PaiementSansFraisCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PaiementSansFraisCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 * @return
	 */
	public JPanel view()	{
		return myView;
	}


	public EOCodeAnalytique codeAnalytique() {
		return currentCanal;
	}

	public EOConvention convention() {
		return currentConvention;
	}

	private void getCodeAnalytique()	{

		EOCodeAnalytique canal = CodeAnalytiqueSelectCtrl.sharedInstance(ec).getCodeAnalytique(null, currentPaiementEngage.toExercice());

		if (canal != null)	{

			try {				
				currentCanal = canal;
				currentPaiementEngage.setCodeAnalytiqueRelationship(currentCanal);

				ec.saveChanges();

				CocktailUtilities.setTextToField(myView.getTfCodeAnalytique(),currentCanal.canCode() + " - " + currentCanal.canLibelle());			

				updateUI();

			}
			catch (Exception e )	{
				ec.revert();
				e.printStackTrace();
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur d'ajout du code analytique !");
			}
		}
	}

	/**
	 * 
	 * @param sender
	 */
	private void delCodeAnalytique()	{

		try {
			currentPaiementEngage.setCodeAnalytiqueRelationship(null);
			ec.saveChanges();

			currentCanal = null;
			myView.getTfCodeAnalytique().setText("");

			updateUI();
		}
		catch (Exception e)	 {
			ec.revert();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur de suppression du code analytique !");		
		}

	}

	/**
	 * 
	 * @param convention
	 */
	public void setConvention(EOConvention convention)	{

		currentConvention = convention;

		currentPaiementEngage.setConventionRelationship(currentConvention);

	}


	private void getConvention()	{

		String paramAllConventions = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_ALL_CONVENTIONS);
		EOConvention convention = ConventionSelectCtrlOld.sharedInstance(ec).getConvention(currentPaiementEngage.toExercice(), null, null, paramAllConventions);

		if (convention != null)	{

			try {
				currentConvention = convention;

				currentPaiementEngage.setConventionRelationship(currentConvention);
				ec.saveChanges();

				CocktailUtilities.setTextToField(myView.getTfConvention(),currentConvention.convIndex().toString() + " - " +currentConvention.libelleCourt());

				updateUI();
			}
			catch (Exception e)	 {
				ec.revert();
				MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur d'ajout de la convention !");		
			}
		}
	}


	private void delConvention()	{

		try {

			currentPaiementEngage.setConventionRelationship(null);

			ec.saveChanges();

			currentConvention = null;
			myView.getTfConvention().setText("");

			updateUI();

		}
		catch (Exception e)	 {
			ec.revert();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur de suppression de la convention !");		
		}

	}



	public void clean()	{

		currentCanal = null;
		currentConvention = null;

		myView.getTfCodeAnalytique().setText("");
		myView.getTfConvention().setText("");

	}



	public void setDisabled()	{

		clean();
		myView.getBtnGetCodeAnalytique().setEnabled(false);
		myView.getBtnGetConvention().setEnabled(false);

	}


	public void actualiser(EOMission mission)	{

		clean();
		currentMission = mission;

		NSArray paiements = EOMissionPaiementEngage.findForMission(ec, currentMission);

		if (paiements.count() > 0) {

			currentPaiementEngage = (EOMissionPaiementEngage)paiements.objectAtIndex(0);
			updateData();

		}

		updateUI();		

	}



	public void updateData()	{

		currentConvention = currentPaiementEngage.convention();
		currentCanal = currentPaiementEngage.codeAnalytique();

		if (currentConvention != null)
			CocktailUtilities.setTextToField(myView.getTfConvention(),currentConvention.convIndex().toString() + " - " +currentConvention.libelleCourt());

		if (currentCanal != null)
			CocktailUtilities.setTextToField(myView.getTfCodeAnalytique(),currentCanal.canCode() + " - " + currentCanal.canLibelle());			


	}


	public void updateUI()	{

		myView.getBtnGetCodeAnalytique().setEnabled(currentMission.isValide());
		myView.getBtnGetConvention().setEnabled(currentMission.isValide());

		myView.getBtnDelConvention().setEnabled(currentMission.isValide() && currentConvention != null);
		myView.getBtnDelCodeAnlaytique().setEnabled(currentMission.isValide() && currentCanal != null);

	}

}
