package org.cocktail.kiwi.client.remboursements;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateurOrgan;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.metier.budget.EOCommande;
import org.cocktail.kiwi.client.metier.budget.EOCommandeEngagement;
import org.cocktail.kiwi.client.metier.budget.EOEngage;
import org.cocktail.kiwi.client.metier.budget.EOOrgan;
import org.cocktail.kiwi.client.nibctrl.CommandeSelectView;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CommandeSelectCtrl
{
	private static CommandeSelectCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private 	EOEditingContext		ec;

	public 	EODisplayGroup		eod;

	private CommandeSelectView myView;

	protected EOExercice currentExercice;	
	private EOCommandeEngagement currentObject;


	/** 
	 *
	 */
	public CommandeSelectCtrl (EOEditingContext globalEc)	{

		super();

		eod = new EODisplayGroup();
		myView = new CommandeSelectView(new JFrame(), true, eod);
		ec = globalEc;


		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.initExercices(NSApp.listeExercices());
		myView.getExercices().setSelectedItem(NSApp.getExerciceCourant());

		myView.getMyEOTable().addListener(new ListenerCommande());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CommandeSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CommandeSelectCtrl(editingContext);
		return sharedInstance;
	}


	private class ListenerCommande implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentObject = (EOCommandeEngagement)eod.selectedObject();
		}

	}

	/**
	 * 
	 *
	 */
	public void clean()	{

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

	}

	/**
	 * Selection d'un engagement
	 */
	public EOCommande getCommande()	{

		clean();

		NSApp.setGlassPane(true);
		myView.show();
		NSApp.setGlassPane(false);

		if (eod.selectedObjects().count() > 0)
			return ((EOCommandeEngagement) eod.selectedObject()).commande();			

		return null;
	}

	/**
	 * 
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(
				EOQualifier.qualifierWithQualifierFormat(
						EOCommandeEngagement.COMMANDE_KEY + " != nil",
						null));

		mesQualifiers.addObject(
				EOQualifier.qualifierWithQualifierFormat(
						EOCommandeEngagement.ENGAGE_KEY + "." + EOEngage.EXE_ORDRE_KEY + " = %@",
						new NSArray(((EOExercice)myView.getExercices().getSelectedItem()).exeExercice())));

		if (!StringCtrl.chaineVide(myView.getTfUb().getText()))	{
			mesQualifiers.addObject(
					EOQualifier.qualifierWithQualifierFormat(
							EOCommandeEngagement.ENGAGE_KEY + "." + EOEngage.ORGAN_KEY + "." + EOOrgan.ORG_UB_KEY + " = %@",
							new NSArray(myView.getTfUb().getText())));
		}

		if (!StringCtrl.chaineVide(myView.getTfCr().getText()))	{
			mesQualifiers.addObject(
					EOQualifier.qualifierWithQualifierFormat(EOCommandeEngagement.ENGAGE_KEY + ".organ.orgCr caseInsensitiveLike %@",
							new NSArray("*"+myView.getTfCr().getText()+"*")));
		}

		if (!StringCtrl.chaineVide(myView.getTfSousCr().getText()))	{
			mesQualifiers.addObject(
					EOQualifier.qualifierWithQualifierFormat(EOCommandeEngagement.ENGAGE_KEY + ".organ.orgSouscr caseInsensitiveLike %@",
							new NSArray("*"+myView.getTfSousCr().getText()+"*")));
		}

		//if (NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION)) {
		mesQualifiers.addObject(
				EOQualifier.qualifierWithQualifierFormat(EOCommandeEngagement.ENGAGE_KEY + "." + 
						EOEngage.ORGAN_KEY + "." + EOOrgan.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurOrgan.UTILISATEUR_KEY + " = %@",
						new NSArray(NSApp.getUtilisateur())));
		//}

		if (!StringCtrl.chaineVide(myView.getTfNoCommande().getText()))	{
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCommandeEngagement.COMMANDE_KEY + "." + EOCommande.COMM_NUMERO_KEY + " = %@",
					new NSArray(new Integer(myView.getTfNoCommande().getText()))));
		}

		if (!StringCtrl.chaineVide(myView.getTfNoEngagement().getText()))	{
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCommandeEngagement.ENGAGE_KEY + "." + EOEngage.ENG_NUMERO_KEY + " = %@",
					new NSArray(new Integer(myView.getTfNoEngagement().getText()))));
		}

		if (!StringCtrl.chaineVide(myView.getTfLibelle().getText()))	{
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCommandeEngagement.COMMANDE_KEY + "." + EOCommande.COMM_LIBELLE_KEY + " caseInsensitiveLike %@",
					new NSArray("*"+myView.getTfLibelle().getText()+"*")));
		}

		if (mesQualifiers.count() == 1)
			return null;

		return new EOAndQualifier(mesQualifiers);
	}


	public void annuler()	{
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}

	/** 
	 *
	 */
	public void rechercher()	{

		EOQualifier myQualifier = getFilterQualifier();

		if (myQualifier == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Vous devez entrer au moins un critère de recherche !");
			return;
		}

		EOFetchSpecification fs = new EOFetchSpecification(EOCommandeEngagement.ENTITY_NAME,myQualifier, null);
		fs.setUsesDistinct(true);
		fs.setRefreshesRefetchedObjects(true);
		eod.setObjectArray(ec.objectsWithFetchSpecification(fs));

		myView.getMyEOTable().updateData();
	}

}