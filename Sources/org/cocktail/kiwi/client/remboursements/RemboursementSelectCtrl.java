package org.cocktail.kiwi.client.remboursements;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.metier.EOSegGroupeInfo;
import org.cocktail.kiwi.client.metier.EOSegTypeInfo;
import org.cocktail.kiwi.client.metier.budget.EOCommande;
import org.cocktail.kiwi.client.nibctrl.RemboursementSelectView;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class RemboursementSelectCtrl 
{
	private static RemboursementSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	
	private RemboursementSelectView myView;
	
	private EODisplayGroup		eodGroupe, eodDetail;	
	private EOSegGroupeInfo 	currentGroupe;
	private	EOSegTypeInfo 		currentDetail;
	private	EOCommande			currentCommande;
	
	/** 
	 *
	 */
	public RemboursementSelectCtrl (EOEditingContext globalEc) {

		super();

		eodGroupe = new EODisplayGroup();
		eodDetail = new EODisplayGroup();

		myView = new RemboursementSelectView(new JFrame(), true, eodGroupe, eodDetail);
		ec = globalEc;


		myView.getBtnGetCommande().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getCommande();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTableTransport().addListener(new ListenerGroupe());
		myView.getMyEOTableDetail().addListener(new ListenerDetail());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static RemboursementSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new RemboursementSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public EOSegTypeInfo getSegTypeInfo()	{

		if (eodGroupe.displayedObjects().count() == 0)	{
			eodGroupe.setObjectArray(EOSegGroupeInfo.findGroupesInfoValides(ec));
			myView.getMyEOTableTransport().updateData();
		}

		currentCommande = null;
		myView.getTfCommande().setText("");
		myView.getTfMontant().setText("0.00");
		
		myView.setVisible(true);
		return currentDetail;

	}
	
    /**
     * Saisie d'un engagement parmi une liste d'engagements coherents.
     * On choisit des engagements valides et correspondant a la ligne budgetaire + type de credit.
     * 
     * @param sender
     */
    public void getCommande()	{
        
        EOCommande commande = CommandeSelectCtrl.sharedInstance(ec).getCommande();

        if (commande != null)	{
        	currentCommande = commande;                
        	myView.getTfCommande().setText(currentCommande.commNumero().toString());
            
        	myView.getTfMontant().setText(currentCommande.montantCommande().toString());
        }
        
    }

	
	/**
	 * 
	 *
	 */
	public BigDecimal getMontant()	{

		String result = (NSArray.componentsSeparatedByString(myView.getTfMontant().getText(),",")).componentsJoinedByString(".");			

		return new BigDecimal(result);
	}

	/**
	 * 
	 *
	 */
	public EOCommande getCurrentCommande()	{
		return currentCommande;
	}


	
	public void annuler()	{
		
		eodGroupe.setSelectionIndexes(new NSArray());
		eodDetail.setSelectionIndexes(new NSArray());

		currentDetail = null;
		currentGroupe = null;
		
		myView.dispose();
		
	}


	
	
	   /**
	    * 
	    * @author cpinsard
	    *
	    * TODO To change the template for this generated type comment go to
	    * Window - Preferences - Java - Code Style - Code Templates
	    */
	   private class ListenerGroupe implements ZEOTableListener {

			public void onDbClick() {
			}
			public void onSelectionChanged() {
				currentGroupe = (EOSegGroupeInfo)eodGroupe.selectedObject();
				
				if (currentGroupe != null)	{
					
					NSMutableArray types = new NSMutableArray(EOSegTypeInfo.findTypesInfoValidesForGroupe(ec, currentGroupe));
					
					EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegTypeInfo.STI_BCDE_KEY + " = 'NON'", null);
					EOQualifier.filterArrayWithQualifier(types, myQualifier);
				
					eodDetail.setObjectArray(types);
				}
				
				myView.getMyEOTableDetail().updateData();
				updateUI();
			}			
	   }
	

	   public void updateUI()	{
		   
		   myView.getBtnGetCommande().setVisible(currentDetail != null && !currentDetail.stiSigne().equals("POSITIF")
				   && !currentDetail.stiSigne().equals("FORFAIT"));

			currentCommande = null;
			myView.getTfCommande().setText("");
			
	   }
	   
	   private class ListenerDetail implements ZEOTableListener {
			public void onDbClick() {
				myView.dispose();
			}
			public void onSelectionChanged() {
				currentDetail = (EOSegTypeInfo)eodDetail.selectedObject();
				
				if (currentDetail != null && currentDetail.stiExplications() != null)
					myView.getTfObservations().setText(currentDetail.stiExplications());
				else
					myView.getTfObservations().setText("");

				updateUI();
			}
	   }
}