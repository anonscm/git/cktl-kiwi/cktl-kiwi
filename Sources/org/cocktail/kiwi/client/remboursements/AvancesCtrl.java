/*
 * Created on 23 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.remboursements;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.AskForValeurs;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.editions.ReportsCtrl;
import org.cocktail.kiwi.client.finders.FinderMissionAvance;
import org.cocktail.kiwi.client.finders.FinderMissionPaiementDestin;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderSegmentInfos;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionAvance;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDepense;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOModePaiement;
import org.cocktail.kiwi.client.metier.EOOrdreDePaiement;
import org.cocktail.kiwi.client.metier.EORibfour;
import org.cocktail.kiwi.client.metier.EOTypeEtat;
import org.cocktail.kiwi.client.metier.EOVisaAvMission;
import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.metier.budget.EODepensePapier;
import org.cocktail.kiwi.client.metier.budget.EOEngage;
import org.cocktail.kiwi.client.mission.EnteteMission;
import org.cocktail.kiwi.client.paiement.PaiementCtrl;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AvancesCtrl  extends EODialogController{

	private static BigDecimal TAUX_AVANCE = new BigDecimal(0.75);

	private static AvancesCtrl sharedInstance;
	ApplicationClient NSApp;
	private EOEditingContext ec;

	public 	EOView 			view, viewDetail;//, viewTableLbud;
	public	EODisplayGroup 	eodAvance, eodOdp, eodDepense;//, eodLbud;

	public	EOView			viewTableAvance, viewTableOdp, viewTableDepense;
	private ZEOTable 		myEOTable, myEOTableOdp, myEOTableDepense;//, myEOTableLbud;
	private ZEOTableModel 	myTableModel, myTableModelOdp, myTableModelDepense;//, myTableModelLbud;
	private TableSorter 	myTableSorter, myTableSorterOdp, myTableSorterDepense;//, myTableSorterLbud;

	public	JTextField		libelleRib;	
	public	JButton			btnAddOp, btnRegulOp, btnAddAvance, btnDelAvance, btnImprimer, btnAddDepense, btnDelDepense;

	private EOMissionPaiement 	currentPaiement;
	private EOMission 			currentMission;
	private EOMissionAvance 	currentAvance;
	private EODepense			currentDepense;
	private EOVisaAvMission	   	currentOp;

	private String paramTypeAvance;

	/**
	 * Constructeur
	 */
	public AvancesCtrl(EOEditingContext editingContext)	{
		super();
		EOArchive.loadArchiveNamed("AvancesCtrl", this,"kiwi.client", this.disposableRegistry());
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		gui_initView();
		gui_initModel();

		libelleRib.setText("");

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AvancesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AvancesCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 *
	 */
	private void gui_initView()	{

		view.setBorder(BorderFactory.createEmptyBorder());
		//viewTableLbud.setBorder(BorderFactory.createEmptyBorder());

		viewDetail.setBorder(BorderFactory.createEtchedBorder(1));

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_ADD,null,btnAddAvance,"");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_ADD,null,btnAddOp,"");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_DELETE,null,btnDelAvance,"");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_WIZARD_16,null,btnRegulOp,"Liquider Avance");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_DELETE,null,btnDelDepense,"");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_PRINTER_16,null,btnImprimer,"Demande Avance");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_ADD,null,btnAddDepense,"Liquider avance");

		CocktailUtilities.initTextField(libelleRib, false, false);

	}


	/**
	 * 
	 *
	 */
	public void updateData()	{

		EORibfour rib = null;
		libelleRib.setText("");

		if (currentDepense != null)			
			rib = currentDepense.depensePapier().rib();

		if (currentOp != null)			
			rib = currentOp.ribfour();

		if (rib != null) {
			libelleRib.setText(
					rib.banque().domiciliation() + "  -  " + rib.ribEtab()+" " + rib.ribGuich()+" - "+rib.ribNum()+" " + rib.ribCle() + "  -  " +rib.ribTitco());

		}			

	}


	/**
	 * 
	 */
	public void addDepense(Object sender) {

		try {

			if (!testDonneesBudgetaires())
				return;			

			String paramModeAvance = FinderMissionParametres.getValue(ec, currentMission.toExercice(), EOMissionParametres.ID_MODE_AVANCE);
			if (paramModeAvance == null)
				paramModeAvance = EOMissionParametres.DEFAULT_VALUE_MODE_AVANCE;

			NSArray paiements = EOMissionPaiementEngage.findForMission(ec, currentMission);
			EOExercice exerciceBudgetaire = ((EOMissionPaiementEngage)paiements.objectAtIndex(0)).toExercice();

			EOModePaiement modeAvance = EOModePaiement.findModePaiementForCode(ec, paramModeAvance, exerciceBudgetaire);

			NSDictionary paramsAvance = AskForValeurs.sharedInstance().getModePaiementAvance("Mode de paiement pour la liquidation : ", currentAvance.mavMontant().toString(), modeAvance, "DEP", exerciceBudgetaire);

			if (paramsAvance == null) 
				return;

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey((EOEnterpriseObject)currentAvance,"EOMissionAvance");
			parametres.setObjectForKey((EOEnterpriseObject)paramsAvance.objectForKey("modePaiement"),"EOModePaiement");
			parametres.setObjectForKey((EOEnterpriseObject)NSApp.getUtilisateur(),"EOUtilisateur");

			ServerProxy.clientSideRequestLiquiderAvance(ec, parametres);

			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentAvance)));

			NSArray engages = EOMissionPaiementEngage.findForMission(ec, currentMission);
			for (int i=0;i<engages.count();i++)
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOMissionPaiementEngage)engages.objectAtIndex(i))));		

			actualiser();

		}
		catch (Exception e) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur de Liquidation de l'avance !\n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();

		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void addOp(Object sender) {

		try {

			if (!testDonneesBudgetaires())
				return;			

			EOModePaiement modeAvance = null;

			if (currentAvance.modePaiement() != null)
				modeAvance = currentAvance.modePaiement();
			else {
				//	        	String paramModeAvance = FinderMissionParametres.getValue(ec, EOMissionParametres.ID_MODE_DPAO);
				//	        	if (paramModeAvance == null)
				//	        		paramModeAvance = EOMissionParametres.DEFAULT_VALUE_MODE_DPAO;
				//
				//	        	modeAvance = FinderModePaiement.findModePaiementForCode(ec, paramModeAvance, NSApp.getCurrentExercice());
			}	        

			// Choix du mode de paiement de l'avance
			NSDictionary paramsAvance = AskForValeurs.sharedInstance().getModePaiementAvance("Mode de paiement pour la liquidation : ", currentAvance.mavMontant().toString(), modeAvance, "OP", currentMission.toExercice());

			if (paramsAvance == null) 
				return;

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey((EOEnterpriseObject)currentAvance,"EOMissionAvance");
			parametres.setObjectForKey((EOEnterpriseObject)paramsAvance.objectForKey("modePaiement"),"EOModePaiement");
			parametres.setObjectForKey((EOEnterpriseObject)NSApp.getUtilisateur(),"EOUtilisateur");

			ServerProxy.clientSideRequestGenererOp(ec, parametres);

			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentAvance)));

			NSArray engages = EOMissionPaiementEngage.findForMission(ec, currentMission);
			for (int i=0;i<engages.count();i++)
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOMissionPaiementEngage)engages.objectAtIndex(i))));		

			actualiser();

		}
		catch (Exception e) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur de Liquidation de l'avance !\n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();

		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void liquiderAvanceOp(Object sender) {

		try {

			if (!testDonneesBudgetaires())
				return;			

			String paramModeAvance = FinderMissionParametres.getValue(ec, currentMission.toExercice(), EOMissionParametres.ID_MODE_DPAO);
			if (paramModeAvance == null)
				paramModeAvance = EOMissionParametres.DEFAULT_VALUE_MODE_DPAO;

			EOModePaiement modeAvance = EOModePaiement.findModePaiementForCode(ec, paramModeAvance, currentMission.toExercice());

			// Choix du mode de paiement de l'avance
			//NSDictionary paramsAvance = AskForValeurs.sharedInstance().getModePaiementAvance("Mode de paiement pour la liquidation : ", currentAvance.mavMontant().toString(), modeAvance, "DEP");

			//if (paramsAvance == null) 
			//	return;

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey((EOEnterpriseObject)currentAvance,"EOMissionAvance");
			parametres.setObjectForKey((EOEnterpriseObject)modeAvance,"EOModePaiement");
			parametres.setObjectForKey((EOEnterpriseObject)NSApp.getUtilisateur(),"EOUtilisateur");

			ServerProxy.clientSideRequestLiquiderAvance(ec, parametres);

			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentAvance)));

			NSArray engages = EOMissionPaiementEngage.findForMission(ec, currentMission);
			for (int i=0;i<engages.count();i++)
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOMissionPaiementEngage)engages.objectAtIndex(i))));		

			actualiser();

		}
		catch (Exception e) {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Erreur de Liquidation de l'avance !\n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();

		}

	}


	/**
	 * 
	 *
	 */
	public void updateUI()	{

		btnAddAvance.setEnabled(
				currentMission != null 
				&& !currentMission.isPayee() 
				&& !currentMission.isAnnulee()
				&& (currentMission.isMissionPaiement() || currentMission.isSaisieLbud() )
				//&& ( currentDepense == null )
				//&& (eodAvance.allObjects().count() == 0)
				&& ( currentOp == null || (currentOp.typeEtat().tyetLibelle().equals("REJETE")) )
				);

		btnDelAvance.setEnabled(currentMission != null 
				&& currentAvance != null 
				&& !currentMission.isPayee()
				&& !currentMission.isAnnulee()
				&& (currentDepense == null || NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION_AVANCES))
				&& ( currentOp == null || (currentOp.typeEtat().tyetLibelle().equals("EN ATTENTE")) || (currentOp.typeEtat().tyetLibelle().equals("REJETE")) )
				);

		btnAddDepense.setEnabled(
				currentMission != null 
				&& currentAvance != null 
				//&& eodDepense.allObjects().count() == 0 
				&& !currentMission.isPayee()
				&& !currentMission.isAnnulee()
				&& NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION_AVANCES)
				&& ( currentOp == null || (currentOp.typeEtat().tyetLibelle().equals("ACCEPTE")) )
				);

		btnAddOp.setEnabled(
				currentMission != null 
				&& currentAvance != null 
				&& !currentMission.isPayee()
				&& NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION_AVANCES)
				//&& eodOdp.allObjects().count() == 0 
				&& ( currentOp == null || (currentOp.typeEtat().tyetLibelle().equals("REJETE")) )
				);

		btnRegulOp.setEnabled(
				currentMission != null 
				&& currentOp != null 
				&& (eodDepense.displayedObjects().count() == 0)
				&& !currentMission.isPayee()
				&& NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION_AVANCES)
				&& currentOp.typeEtat().tyetLibelle().equals("ACCEPTE")
				);

		btnDelDepense.setEnabled(
				currentMission != null 
				&& currentDepense != null 
				&& !currentMission.isPayee()
				&& !currentMission.isAnnulee()
				&& NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION_AVANCES)
				);

		btnImprimer.setEnabled(
				currentAvance != null
				&& !currentMission.isAnnulee()
				&& ( currentOp == null || (currentOp.typeEtat().tyetLibelle().equals("REJETE")) || (currentOp.typeEtat().tyetLibelle().equals("EN ATTENTE"))) 
				);

	}

	/**
	 * 
	 * @return
	 */
	public EOView view()	{
		return view;
	}



	/**
	 * 
	 * @param sender
	 */
	public void imprimer(Object sender) {

		ReportsCtrl.sharedInstance(ec).printDemandeAvance(currentMission, window());

	}

	/**
	 * 
	 *
	 */
	public void clearTextFields()	{

		eodDepense.setObjectArray(new NSArray());
		myEOTableDepense.updateData();

		eodOdp.setObjectArray(new NSArray());
		myEOTableOdp.updateData();

	}

	/**
	 * 
	 * @param mission
	 */
	public void setCurrentMission(EOMission mission)	{

		currentMission = mission;

		if (currentMission== null)	{
			clearTextFields();
			eodAvance.setObjectArray(new NSArray());
			myEOTable.updateData();
		}
	}


	/**
	 * 
	 *
	 */
	public void actualiser()	{

		if (currentMission != null) {
			paramTypeAvance = FinderMissionParametres.getValue(ec, currentMission.toExercice() , EOMissionParametres.ID_TRAITEMENTS_AVANCES);
			if (paramTypeAvance ==  null)
				paramTypeAvance = EOMissionParametres.DEFAULT_VALUE_TRAITEMENT_AVANCES;

			if (paramTypeAvance.equals("DEP")) {
				viewTableOdp.setVisible(false);
				btnAddOp.setVisible(false);
				btnRegulOp.setVisible(false);
			}
			else {
				if (paramTypeAvance.equals("OP")) {
					btnAddDepense.setVisible(false);
					btnDelDepense.setVisible(false);
				}
			}
		}
		if (currentMission != null)			
			eodAvance.setObjectArray(FinderMissionAvance.findAvancesForMission(ec, currentMission));
		else 
			eodAvance.setObjectArray(new NSArray());

		myEOTable.updateData();

		updateUI();
	}


	/**
	 * 
	 * @return
	 */
	public boolean testDonneesBudgetaires()	{

		NSArray paiements = EOMissionPaiementEngage.findForMission(ec, currentMission);

		BigDecimal totalMontantBudgetaire = new BigDecimal(0), totalMontantRembourse = new BigDecimal(0);

		for (int i=0;i<paiements.count();i++)	{

			EOMissionPaiementEngage paiement = (EOMissionPaiementEngage)paiements.objectAtIndex(i);

			NSArray destins = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(ec, paiement);

			// La ligne budgetaire, le compte d'imputation, le type de credit et la destination sont obligatoires.
			if (	(paiement.organ() == null) 
					||  (paiement.modePaiement() == null)
					||	(destins.count() == 0) 
					||	(paiement.planComptable() == null)
					||	(paiement.toTypeCredit() == null))	{
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Les informations budgétaires associées à cette mission sont incomplètes !");			
				return false;
			}

			if (paiement.organ().orgNiveau().intValue() < 3) {
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Vous ne pouvez pas engager sur une ligne budgétaire de niveau " + paiement.organ().orgNiveau() + " !");			
				return false;				
			}

			//			if (paiement.engage() == null) {
			//				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez générer les engagements (Cliquer sur 'Ordre de Mission' avant de liquider l'avance !");
			//				return false;
			//			}

			totalMontantBudgetaire = totalMontantBudgetaire.add(paiement.mpeMontantBudgetaire());
			totalMontantRembourse = totalMontantRembourse.add(paiement.mpeMontantRembourse());

		}

		// Le total des montants budgetaires ou rembourses doivent etre egaux au montant global de la mission
		if (totalMontantBudgetaire.floatValue() < 0.0)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR"," Le montant budgétaire doit être supérieur à 0 !");			
			return false;
		}

		//		if (totalMontantRembourse.floatValue() < 0.0)	{
		//			MsgPanel.sharedInstance().runInformationDialog("ERREUR"," Le montant remboursé ne doit pas être négatif !");			
		//			return false;
		//		}

		return true;
	}


	/**
	 * 
	 * @param sender
	 */
	public void addAvance(Object sender)	{

		//Demande du montant de l'avance		

		PaiementCtrl.sharedInstance(ec).setCurrentMission(currentMission);
		PaiementCtrl.sharedInstance(ec).actualiser();

		if ( currentMission.isMissionPaiement() && !testDonneesBudgetaires())
			return;			

		BigDecimal montantAvance = new BigDecimal(0);

		try {

			int nbAvancesSaisies = FinderSegmentInfos.countAvances(ec, currentMission);
			if (nbAvancesSaisies > 0) {
				MsgPanel.sharedInstance().runInformationDialog("ERREUR","Une avance a déjà été saisie dans l'onglet 'BILLETS'.\n" +
						"Veuilez la supprimer avant d'en saisir une autre dans cet écran.");
				return;
			}

			// On teste la saisie des informations budgetaires

			currentPaiement = EOMissionPaiement.findPaiementForMission(ec, currentMission);

			String paramAvance = FinderMissionParametres.getValue(ec, currentMission.toExercice(), EOMissionParametres.ID_CHECK_AVANCES);
			if (paramAvance == null)
				paramAvance = EOMissionParametres.DEFAULT_VALUE_CHECK_AVANCES;

			Boolean bloquageAvance = (paramAvance != null && "O".equals(paramAvance));

			montantAvance = currentPaiement.mipMontantTotal().multiply(TAUX_AVANCE);
			montantAvance = montantAvance.setScale(0, BigDecimal.ROUND_HALF_UP);

			if ( ! bloquageAvance) {

				String valeurSaisie="";
				boolean saisieOK = false;

				NSDictionary paramsAvance = new NSDictionary();

				while (paramsAvance != null && !saisieOK)	{

					paramsAvance = 	AskForValeurs.sharedInstance().getMontantAvance("Montant de l'avance ( 75% de " + currentPaiement.mipMontantTotal()+")", montantAvance.toString(), paramTypeAvance);

					if (paramsAvance == null)	
						return;

					if (paramsAvance != null)	{
						try {
							valeurSaisie = ((BigDecimal)paramsAvance.objectForKey("montant")).toString();

							BigDecimal decimalValue = new BigDecimal(valeurSaisie);
							saisieOK = true;

						}
						catch (Exception ex)	{
							MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur du format de saisie. Veuillez entrer un nombre valide.");
						}					
					}
				}			

				montantAvance = new BigDecimal(valeurSaisie);
			}				

			//			// Le montant de l'avance ne doit pas depasser le montant de la mission
			if (currentPaiement.mipMontantTotal().floatValue() < montantAvance.floatValue()) {

				MsgPanel.sharedInstance().runErrorDialog("ERREUR","L'avance ne peut être supérieure au montant de la mission !");
				return;

			}

			NSMutableDictionary parametres = new NSMutableDictionary();		
			parametres.setObjectForKey((EOEnterpriseObject)currentMission,"EOMission");
			parametres.setObjectForKey((EOEnterpriseObject)NSApp.getUtilisateur(),"EOUtilisateur");

			//			if (paramsAvance.objectForKey("modePaiement") != null)
			//				parametres.setObjectForKey((EOEnterpriseObject)paramsAvance.objectForKey("modePaiement"),"EOModePaiement");

			parametres.setObjectForKey(montantAvance,"montant");

			ServerProxy.clientSideRequestCreerAvance(ec, parametres);

			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPaiement)));
			NSArray engages = EOMissionPaiementEngage.findForMission(ec, currentMission);
			for (int i=0;i<engages.count();i++)
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOMissionPaiementEngage)engages.objectAtIndex(i))));		

			EnteteMission.sharedInstance(ec).rafraichirMission();

		}
		catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur de modification du paramètre !\n" + CocktailUtilities.getErrorDialog(ex));
		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void delDepense(Object sender)	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de cette/ces dépenses ?", "OUI", "NON"))
			return;

		try {
			for (int i=0;i<eodDepense.displayedObjects().count();i++) {

				EOMissionPaiementDepense paiementDepense = (EOMissionPaiementDepense)eodDepense.displayedObjects().objectAtIndex(i);

				EODepense depense = paiementDepense.depense();
				Number depid = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, depense).objectForKey("depId");

				ServerProxy.delDepense(ec, depid);
			}

			updateUI();

			eodDepense.setObjectArray(EOMissionPaiementDepense.findDepensesAvancesForMission(ec, currentMission));

			myEOTableDepense.updateData();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",CocktailUtilities.getErrorDialog(ex));
		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void delAvance(Object sender)	{

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de cette avance ?", "OUI", "NON"))
			return;

		try {

			if (eodDepense.displayedObjects().count() > 0)
				delDepense(this);

			//			if (eodOdp.displayedObjects().count() > 0)
			//				delOp(this);

			NSMutableDictionary parametres = new NSMutableDictionary();		
			parametres.setObjectForKey((EOEnterpriseObject)currentAvance,"EOMissionAvance");

			ServerProxy.clientSideRequestSupprimerAvance(ec, parametres);

			actualiser();
		}
		catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur de suppression de l'avance !\n" + CocktailUtilities.getErrorDialog(ex));
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void gui_initModel() {

		eodAvance = new EODisplayGroup();
		eodOdp = new EODisplayGroup();
		eodDepense = new EODisplayGroup();
		//eodLbud = new EODisplayGroup();

		initTableModel();
		initTable();

		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTable.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);

		viewTableAvance.setBorder(BorderFactory.createEmptyBorder());
		viewTableAvance.removeAll();
		viewTableAvance.setLayout(new BorderLayout());
		viewTableAvance.add(new JScrollPane(myEOTable), BorderLayout.CENTER);		

		myEOTableOdp.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTableOdp.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);

		viewTableOdp.setBorder(BorderFactory.createEmptyBorder());
		viewTableOdp.removeAll();
		viewTableOdp.setLayout(new BorderLayout());
		viewTableOdp.add(new JScrollPane(myEOTableOdp), BorderLayout.CENTER);		


		myEOTableDepense.setSelectionBackground(CocktailConstantes.COLOR_SELECT_NOMENCLATURES);
		myEOTableDepense.setBackground(CocktailConstantes.COLOR_FOND_NOMENCLATURES);

		viewTableDepense.setBorder(BorderFactory.createEmptyBorder());
		viewTableDepense.removeAll();
		viewTableDepense.setLayout(new BorderLayout());
		viewTableDepense.add(new JScrollPane(myEOTableDepense), BorderLayout.CENTER);		

		//		myEOTableLbud.setSelectionBackground(ConstantesCocktail.COLOR_SELECT_NOMENCLATURES);
		//		myEOTableLbud.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		//
		//		viewTableLbud.setBorder(BorderFactory.createEmptyBorder());
		//		viewTableLbud.removeAll();
		//		viewTableLbud.setLayout(new BorderLayout());
		//		viewTableLbud.add(new JScrollPane(myEOTableLbud), BorderLayout.CENTER);		

	}

	/**
	 * Initialise la table a afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerAvance());
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTableOdp = new ZEOTable(myTableSorterOdp);
		myEOTableOdp.addListener(new ListenerOp());
		myTableSorterOdp.setTableHeader(myEOTableOdp.getTableHeader());	

		myEOTableDepense = new ZEOTable(myTableSorterDepense);
		myEOTableDepense.addListener(new ListenerDepense());
		myTableSorterDepense.setTableHeader(myEOTableDepense.getTableHeader());	

		//		myEOTableLbud = new ZEOTable(myTableSorterLbud);
		//		myEOTableLbud.addListener(new ListenerDepense());
		//		myTableSorterLbud.setTableHeader(myEOTableLbud.getTableHeader());	

	}

	/**
	 * Initialise le modeele le la table a afficher.
	 *  
	 */
	private void initTableModel() {

		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

		ZEOTableModelColumn col = new ZEOTableModelColumn(eodAvance, EOMissionAvance.MAV_DATE_SAISIE_KEY, "Date", 130);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay((DateFormat)new SimpleDateFormat("dd/MM/yyyy"));
		myCols.add(col);		

		col = new ZEOTableModelColumn(eodAvance, EOMissionAvance.MAV_MONTANT_KEY, "Montant", 130);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);		

		col = new ZEOTableModelColumn(eodAvance, EOMissionAvance.MODE_PAIEMENT_KEY+"."+EOModePaiement.MOD_CODE_KEY, "Mode", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodAvance, EOMissionAvance.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY, "Etat", 100);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);		

		myTableModel = new ZEOTableModel(eodAvance, myCols);
		myTableSorter = new TableSorter(myTableModel);



		myCols= new Vector<ZEOTableModelColumn>();

		col = new ZEOTableModelColumn(eodOdp, EOVisaAvMission.ORDRE_DE_PAIEMENT_KEY+"."+EOOrdreDePaiement.ODP_NUMERO_KEY , "Numéro", 75);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodOdp, EOVisaAvMission.VAM_MONTANT_KEY , "Montant", 75);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodOdp, EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY+"."+EOModePaiement.MOD_CODE_KEY, "Mode", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodOdp, EOVisaAvMission.VAM_DATE_DEM_KEY , "Date", 100);
		col.setFormatDisplay((DateFormat)new SimpleDateFormat("dd/MM/yyyy"));
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodOdp, EOVisaAvMission.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY , "Etat", 100);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodOdp, EOVisaAvMission.VAM_MOTIF_REJET_KEY , "Motif Rejet", 250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		myTableModelOdp = new ZEOTableModel(eodOdp, myCols);
		myTableSorterOdp = new TableSorter(myTableModelOdp);


		myCols= new Vector<ZEOTableModelColumn>();

		col = new ZEOTableModelColumn(eodDepense, EOMissionPaiementDepense.DEPENSE_KEY + "." + EODepense.TO_EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, "Exer", 45);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodDepense, EOMissionPaiementDepense.DEPENSE_KEY + "." + EODepense.DEPENSE_PAPIER_KEY+"."+EODepensePapier.DPP_NUMERO_FACTURE_KEY, "Numéro", 250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodDepense, EOMissionPaiementDepense.DEPENSE_KEY + "." + EODepense.DEP_TTC_SAISIE_KEY, "Montant", 75);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodDepense, EOMissionPaiementDepense.DEPENSE_KEY + "." + EODepense.DEPENSE_PAPIER_KEY+"."+EODepensePapier.MODE_PAIEMENT_KEY+"."+EOModePaiement.MOD_CODE_KEY, "Mode", 50);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodDepense, EOMissionPaiementDepense.DEPENSE_KEY + "." + EODepense.ENGAGE_KEY +"."+EOEngage.ORGAN_KEY+".getLongString", "Ligne", 200);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodDepense, EOMissionPaiementDepense.DEPENSE_KEY + "." + EODepense.UTILISATEUR_KEY+"."+EOUtilisateur.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, "Utilisateur", 150);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		myTableModelDepense = new ZEOTableModel(eodDepense, myCols);
		myTableSorterDepense = new TableSorter(myTableModelDepense);


	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerAvance implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			clearTextFields();

			currentAvance = (EOMissionAvance)eodAvance.selectedObject();

			if (currentAvance != null && currentAvance.visaAvMission() != null && currentAvance.visaAvMission() != null)	{
				eodOdp.setObjectArray(new NSArray(currentAvance.visaAvMission()));
				myEOTableOdp.updateData();
			}

			if (currentAvance != null)
				eodDepense.setObjectArray(EOMissionPaiementDepense.findDepensesAvancesForMission(ec, currentMission));

			myEOTableDepense.updateData();

			updateData();

			updateUI();
		}

	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerDepense implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentDepense = null;

			if (eodDepense.selectedObject() != null)
				currentDepense = ((EOMissionPaiementDepense)eodDepense.selectedObject()).depense();

		}

	}



	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerOp implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentOp = (EOVisaAvMission)eodOdp.selectedObject();

		}

	}


	//	/**
	//	 * 
	//	 * @author cpinsard
	//	 *
	//	 * TODO To change the template for this generated type comment go to
	//	 * Window - Preferences - Java - Code Style - Code Templates
	//	 */
	//	private class ListenerLbud implements ZEOTableListener {
	//		
	//		/* (non-Javadoc)
	//		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
	//		 */
	//		public void onDbClick() {
	//
	//		}
	//		
	//		/* (non-Javadoc)
	//		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
	//		 */
	//		public void onSelectionChanged() {
	//			
	//			currentLbud = (EOMissionPaiementEngage)eodLbud.selectedObject();
	//
	//		}
	//	}


}