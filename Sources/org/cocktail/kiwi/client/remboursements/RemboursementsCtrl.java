/*
 * Created on 23 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.remboursements;

import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactorySegmentInfos;
import org.cocktail.kiwi.client.finders.FinderMissionAvance;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOSegTypeInfo;
import org.cocktail.kiwi.client.metier.EOSegmentInfos;
import org.cocktail.kiwi.client.metier.budget.EOCommande;
import org.cocktail.kiwi.client.nibctrl.RemboursementsView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RemboursementsCtrl {

	private static RemboursementsCtrl sharedInstance;
	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private RemboursementsView myView;
	private	EODisplayGroup eodSegment = new EODisplayGroup(), eodCommande = new EODisplayGroup();

	private EOMission 		currentMission;
	private EOSegmentInfos 	currentSegmentInfo;
	private	EOCommande		currentCommande;

	/** 
	 * Constructeur 
	 */
	public RemboursementsCtrl(EOEditingContext editingContext)	{

		super();

		myView = new RemboursementsView(eodSegment, eodCommande);
		ec = editingContext;


		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		
		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});

		
		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				validerInfos();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annulerInfos();
			}
		});

		myView.getBtnUpdateCommande().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getCommande();
			}
		});

		
		myView.getBtnDelCommande().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delCommande();
			}
		});

		myView.getMyEOTableSegment().addListener(new ListenerSegment());
		myView.getMyEOTableCommande().addListener(new ListenerCommande());

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static RemboursementsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new RemboursementsCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * Saisie d'un engagement parmi une liste d'engagements coherents.
	 * On choisit des engagements valides et correspondant a la ligne budgetaire + type de credit.
	 * 
	 * @param sender
	 */
	private void getCommande()	{

		NSApp.setWaitCursor();

		EOCommande commande = CommandeSelectCtrl.sharedInstance(ec).getCommande();

		if (commande != null)	{
			currentCommande = commande;
			currentSegmentInfo.setCommandeRelationship(currentCommande);
			ec.saveChanges();
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentSegmentInfo)));
			updateData();
		}

		updateUI();

		NSApp.setDefaultCursor();

	}

	/**
	 * 
	 * @param sender
	 */
	private void delCommande()	{

		if (EODialogs.runConfirmOperationDialog("Attention",
				"La commande "+currentCommande.commNumero()+" sera désassociée du remboursement.\nVoulez-vous continuer ?", "OUI", "NON")) {

			currentCommande = null;
			eodCommande.setObjectArray(new NSArray());
			myView.getMyEOTableCommande().updateData();
			currentSegmentInfo.removeObjectFromBothSidesOfRelationshipWithKey(currentSegmentInfo.commande(), EOSegmentInfos.COMMANDE_KEY);
			ec.saveChanges();
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentSegmentInfo)));

			updateUI();
		}

	}

	/**
	 * 
	 *
	 */
	private void updateUI()	{

		try {
		myView.getViewTableCommande().setVisible(currentSegmentInfo != null && !currentSegmentInfo.segTypeInfo().stiSigne().equals("POSITIF")
				&& !currentSegmentInfo.segTypeInfo().stiSigne().equals("FORFAIT"));

		myView.getBtnUpdateCommande().setVisible(currentSegmentInfo != null 
				&& !currentSegmentInfo.segTypeInfo().stiSigne().equals("POSITIF")
				&& !currentSegmentInfo.segTypeInfo().stiSigne().equals("FORFAIT"));

		myView.getBtnDelCommande().setVisible(eodCommande.displayedObjects().count() == 1);

		if ( currentMission == null || currentMission.misEtat() == null 
				|| currentMission.isAnnulee()
				|| (currentMission.isPayee())
		)	{
			setDisabled();
		}
		else	{

			myView.getBtnAjouter().setVisible(true);

			myView.getBtnValider().setVisible(currentSegmentInfo != null);
			myView.getBtnAnnuler().setVisible(false);//currentSegmentInfo != null);				
			myView.getBtnSupprimer().setVisible(currentSegmentInfo != null);	

			CocktailUtilities.initTextField(myView.getTfMontant(), false, currentSegmentInfo != null);
			CocktailUtilities.initTextField(myView.getTfMotif(), false, currentSegmentInfo != null);
			CocktailUtilities.initTextField(myView.getTfRemarques(), false, currentSegmentInfo != null);
			CocktailUtilities.initTextField(myView.getTfInfos(), false, currentSegmentInfo != null);
		}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 *
	 */
	public void setDisabled()	{

		myView.getBtnAjouter().setVisible(false);
		myView.getBtnValider().setVisible(false);
		myView.getBtnAnnuler().setVisible(false);
		myView.getBtnSupprimer().setVisible(false);

		myView.getBtnUpdateCommande().setVisible(false);
		myView.getBtnDelCommande().setVisible(false);

		CocktailUtilities.initTextField(myView.getTfMontant(), false, false);
		CocktailUtilities.initTextField(myView.getTfMotif(), false, false);
		CocktailUtilities.initTextField(myView.getTfRemarques(), false, false);
		CocktailUtilities.initTextField(myView.getTfInfos(), false, false);

	}

	/**
	 * 
	 * @return
	 */
	public JPanel view()	{
		return myView;
	}

	/**
	 * 
	 *
	 */
	private void clearTextFields()	{

		myView.getTfInfos().setText("");
		myView.getTfDevise().setText("");
		myView.getTfCoeff().setText("");
		myView.getTfRemarques().setText("");
		myView.getTfMontant().setText("");
		myView.getTfPriseEnCharge().setText("");
		myView.getTfMotif().setText("");

	}

	/**
	 * 
	 * @param mission
	 */
	public void setCurrentMission(EOMission mission)	{
		
		currentMission = mission;

		if (currentMission== null)	{
			clearTextFields();
			eodSegment.setObjectArray(new NSArray());
			myView.getMyEOTableSegment().updateData();
		}
		
	}

	/**
	 * 
	 *
	 */
	public void actualiser()	{

		if (currentMission != null)
			eodSegment.setObjectArray(currentMission.segmentsInfos());
		else
			eodSegment.setObjectArray(new NSArray());

		myView.getMyEOTableSegment().updateData();

		updateUI();		

	}

	/**
	 * 
	 * @param sender
	 */
	private void ajouter()	{

		EOSegTypeInfo newTypeInfo = RemboursementSelectCtrl.sharedInstance(ec).getSegTypeInfo();

		if (newTypeInfo != null)	{

			// On verifie que le montant de l'avance ne soit pas superieur au montant de la mission
			BigDecimal montantRemboursement = RemboursementSelectCtrl.sharedInstance(ec).getMontant().setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);

			// On ne peut pas avoir 2 lignes d'avance pour une seule mission						
			if (newTypeInfo.segGroupeInfo().estUneAvance())	{

				// On verifie qu'une avance n est pas ete saisie dans l'onglet AVANCES
				int nbAvancesSaisies = FinderMissionAvance.countAvances(ec, currentMission);
				if (nbAvancesSaisies > 0) {
					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Une avance a déjà été saisie dans l'onglet 'AVANCES'.\n" +
					"Veuilez la supprimer avant d'en saisir une autre dans cet écran.");
					return;
				}

				NSArray typesUtilises = (NSArray)eodSegment.displayedObjects().valueForKeyPath("segTypeInfo.segGroupeInfo.stgLibelle");
				if (typesUtilises.containsObject("AVANCES"))	{
					MsgPanel.sharedInstance().runInformationDialog("ERREUR","Vous ne pouvez sélectionner qu'une seule ligne d'avances pour la même mission.");
					return;
				}

			}

			try {

				// On cree un nouveau SegmentInfos
				EOSegmentInfos newSegment = FactorySegmentInfos.sharedInstance().creerSegmentInfos(ec, currentMission);	

				newSegment.setSegTypeInfoRelationship(newTypeInfo);
				newSegment.setSeiMontantSaisie(montantRemboursement);
				newSegment.setSeiMontantPaiement(montantRemboursement);

				EOCommande commande = RemboursementSelectCtrl.sharedInstance(ec).getCurrentCommande();
				if (commande != null)	{
					newSegment.setCommandeRelationship(commande);
				}

				ec.saveChanges();

				actualiser();

				eodSegment.setSelectedObject(newSegment);
				myView.getMyEOTableSegment().forceNewSelection(eodSegment.selectionIndexes());
				myView.getMyEOTableSegment().scrollToSelection();
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}

		}

	}


	private void validerInfos()	{

		try {
			
			String result = (NSArray.componentsSeparatedByString(myView.getTfMontant().getText(),",")).componentsJoinedByString(".");			
			BigDecimal montantSaisi = new BigDecimal(result).setScale(ApplicationClient.USED_DECIMALES, BigDecimal.ROUND_HALF_UP);

			if (currentCommande != null)
				currentSegmentInfo.setCommandeRelationship(currentCommande);

			currentSegmentInfo.setSeiMontantSaisie(montantSaisi);
			currentSegmentInfo.setSeiMontantPaiement(montantSaisi);

			if (!StringCtrl.chaineVide(myView.getTfMotif().getText()))
				currentSegmentInfo.setSeiLibelle(myView.getTfMotif().getText());
			else
				currentSegmentInfo.setSeiLibelle(null);

			if (!StringCtrl.chaineVide(myView.getTfInfos().getText()))
				currentSegmentInfo.setSeiJefyco(myView.getTfInfos().getText());
			else
				currentSegmentInfo.setSeiJefyco(null);

			if (!StringCtrl.chaineVide(myView.getTfRemarques().getText()))
				currentSegmentInfo.setSeiDivers(myView.getTfRemarques().getText());
			else
				currentSegmentInfo.setSeiDivers(null);

			currentSegmentInfo.setSeiEtat("VALIDE");

			currentMission.setUtilisateurModificationRelationship(NSApp.getUtilisateur());
			
			ec.saveChanges();
			myView.getMyEOTableSegment().updateUI();
			
		}
		catch (NSValidation.ValidationException e)	{
			ec.revert();
			updateData();
		}
		catch (Exception e)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez vérifier la validité du montant saisi !!!");
			ec.revert();
			updateData();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 *
	 */
	private void annulerInfos()	{

		try {
			ec.revert();
			myView.getMyEOTableSegment().updateData();
		}
		catch (Exception e) {

		}
	}

	/**
	 * 
	 *
	 */
	private void updateData()	{

		if (currentSegmentInfo != null && currentMission != null)	{

			myView.getTfPriseEnCharge().setText(currentSegmentInfo.segTypeInfo().stiExplications());
			myView.getTfDevise().setText(currentSegmentInfo.webtaux().webmon().devises().llDevise());
			myView.getTfCoeff().setText(currentSegmentInfo.webtaux().wtaTaux().toString());

			myView.getTfMontant().setText(currentSegmentInfo.seiMontantPaiement().toString());

			myView.getTfMotif().setText(currentSegmentInfo.seiLibelle());
			myView.getTfRemarques().setText(currentSegmentInfo.seiDivers());
			myView.getTfInfos().setText(currentSegmentInfo.seiJefyco());

			currentCommande = currentSegmentInfo.commande();
			if (currentCommande != null)
				eodCommande.setObjectArray(new NSArray(currentCommande));
			else
				eodCommande.setObjectArray(new NSArray());

			myView.getMyEOTableCommande().updateData();
		}

	}	


	/**
	 * 
	 * @param sender
	 */
	public void supprimer()	{

		if (EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous cette suppression ?", "OUI", "NON")) {

			try {				
				currentSegmentInfo.removeObjectFromBothSidesOfRelationshipWithKey(currentMission, "mission");
				ec.deleteObject(currentSegmentInfo);
				ec.saveChanges();
			}
			catch (Exception e)	{
				ec.revert();
				e.printStackTrace();
			}
			actualiser();	

		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerSegment implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			clearTextFields();

			currentSegmentInfo = (EOSegmentInfos)eodSegment.selectedObject();

			updateData();

			updateUI();
		}

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerCommande implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentCommande = (EOCommande)eodCommande.selectedObject();
		}

	}
}