package org.cocktail.kiwi.client;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/*
 * Created on 13 dec. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ServerProxyPrint {

	private static final String KEY_PATH = "session.remoteCallPrint";

	
	public static NSData clientSideRequestPrintAndWait(EOEditingContext ec, String report, String sql, NSDictionary parametres) {
		return (NSData) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallPrint", "clientSideRequestPrintAndWait", new Class[] {String.class, String.class, NSDictionary.class}, new Object[] {report, sql, parametres}, false);
	}
	public static void clientSideRequestPrintByThread(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId, final Boolean printIfEmpty) {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallPrint", "clientSideRequestPrintByThread", new Class[] {String.class, String.class, NSDictionary.class, String.class, Boolean.class}, new Object[] {report, sql, parametres, customJasperId, printIfEmpty}, false);
	}
	public static void clientSideRequestPrintByThreadXls(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId) {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallPrint", "clientSideRequestPrintByThreadXls", new Class[] {String.class, String.class, NSDictionary.class, String.class}, new Object[] {report, sql, parametres, customJasperId}, false);
	}
	public static NSDictionary clientSideRequestGetPrintProgression(EOEditingContext ec) {
		return (NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallPrint", "clientSideRequestGetPrintProgression", new Class[] {}, new Object[] {}, false);
	}
	public static void clientSideRequestPrintKillCurrentTask(EOEditingContext ec) {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallPrint", "clientSideRequestPrintKillCurrentTask", new Class[] {}, new Object[] {}, false);
	}
	public static NSData clientSideRequestPrintDifferedGetPdf(EOEditingContext ec) {
		return (NSData)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallPrint", "clientSideRequestPrintDifferedGetPdf", new Class[] {}, new Object[] {}, false);
	} 

	/**
	 * 
	 * @param ec
	 * @param eo
	 * @return
	 */
	public static void clientSideRequest(EOEditingContext ec, NSDictionary parametres) 	throws Exception{

		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				KEY_PATH, 
				"clientSideRequest", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres}, 
				false);	
	}


}
