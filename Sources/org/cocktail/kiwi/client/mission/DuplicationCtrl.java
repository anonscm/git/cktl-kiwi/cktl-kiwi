package org.cocktail.kiwi.client.mission;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.factory.FactoryMissionPaiementDestin;
import org.cocktail.kiwi.client.factory.FactoryMissionPaiementEngage;
import org.cocktail.kiwi.client.factory.FactorySegmentInfos;
import org.cocktail.kiwi.client.factory.FactorySegmentTarif;
import org.cocktail.kiwi.client.finders.FinderMissionPaiementDestin;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDestin;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.nibctrl.DuplicationView;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;
import org.cocktail.kiwi.common.utilities.TimeCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class DuplicationCtrl {
	
	private static DuplicationCtrl sharedInstance;
	
	private EOEditingContext ec;
	private ApplicationClient NSApp;
	
	private DuplicationView myView;
	
	private EOMission currentMission;
	private EOFournis currentAgent;
	
	public EODisplayGroup eodAgents = new EODisplayGroup();
	private EODisplayGroup eodAgentsSelectionnes = new EODisplayGroup();
	
	/** 
	 * Constructeur 
	 */
	public DuplicationCtrl(EOEditingContext editingContext)	{
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;

		myView = new DuplicationView(new JFrame(), true, eodAgents, eodAgentsSelectionnes);
		
		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fermer();
			}
		});

		myView.getBtnGetAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercherAgents();
			}
		});

		myView.getBtnSetAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterAgent();
			}
		});

		myView.getBtnDelAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerAgent();
			}
		});

		myView.getBtnDupliquer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dupliquer();
			}			
		});

		eodAgents.setSortOrderings(new NSArray(new EOSortOrdering(EOFournis.NOM_KEY, EOSortOrdering.CompareAscending)));
		eodAgentsSelectionnes.setSortOrderings(new NSArray(new EOSortOrdering(EOFournis.NOM_KEY, EOSortOrdering.CompareAscending)));

		myView.getMyEOTableAgents().addListener(new ListenerAgents());

		myView.getTfFiltreNom().addActionListener(new ActionListenerRecherchePersonnel());
		
		myView.getCheckTrajets().setSelected(true);
		myView.getCheckNuits().setSelected(true);
		myView.getCheckTransports().setSelected(true);
		myView.getCheckIndemnites().setSelected(true);
		myView.getCheckRemboursements().setSelected(true);
		myView.getCheckBudget().setSelected(false);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));

		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));
	}	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static DuplicationCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new DuplicationCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 *
	 */
	public void open()	{
		
		currentMission = EnteteMission.sharedInstance(ec).getCurrentMission();
		
		if (currentMission == null)	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Aucune mission n'est sélectionnée !");
			return;			
		}
		
		if (currentMission.isAnnulee()) {//|| currentMission.isPayee())	{
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Vous ne pouvez pas dupliquer une mission ANNULEE !");
			return;
		}
		
		eodAgents.setObjectArray(new NSArray());
		eodAgentsSelectionnes.setObjectArray(new NSArray());
		
		myView.getMyEOTableAgents().updateData();
		myView.getMyEOTableSelection().updateData();
		
		myView.getTfTitre().setText("Mission No " + currentMission.misNumero() + " : " + 
				StringCtrl.capitalizedString(currentMission.fournis().prenom()) + " " + 
						currentMission.fournis().nom().toUpperCase());

		myView.getTfDateDebut().setText(DateCtrl.dateToString(currentMission.misDebut(),"%d/%m/%Y"));
		myView.getTfDateFin().setText(DateCtrl.dateToString(currentMission.misFin(),"%d/%m/%Y"));

		myView.getTfHeureDebut().setText(DateCtrl.dateToString(currentMission.misDebut(),"%H:%M"));
		myView.getTfHeureFin().setText(DateCtrl.dateToString(currentMission.misFin(),"%H:%M"));
		
		myView.getLblDates().setText(DateCtrl.dateToString(currentMission.misDebut(), "%d/%m/%Y %H:%M")+" au " + 
				DateCtrl.dateToString(currentMission.misFin(), "%d/%m/%Y %H:%M"));
		CocktailUtilities.setTextToArea(myView.getTaMotif(), currentMission.misMotif());
		
		myView.setVisible(true);
	}
	
	/**
	 * 
	 * @param sender
	 */
	private void dupliquer()	{
				
		if (eodAgents.displayedObjects().count() == 0) {
			EODialogs.runInformationDialog("INFO","Veuillez sélectionner des agents pour lesquels dupliquer la mission.");							
			return;
		}
		
		CRICursor.setWaitCursor(myView);		

		NSArray fournisseurs = eodAgentsSelectionnes.displayedObjects();
				
		EOMission missionToDuplicate = currentMission;
		EOMission newMission = null;
		String messageConfirmation = "";
		String messageErreur = "";
				
		EOFournis fournis = null;
		
		for (int i = 0;i<fournisseurs.count();i++)	{
			try {
				
				fournis = (EOFournis)fournisseurs.objectAtIndex(i);
				
				// Ajout de la mission
				NSTimestamp dateDebutSaisie = DateCtrl.stringToDate(myView.getTfDateDebut().getText()+" "+myView.getTfHeureDebut().getText(), "%d/%m/%Y %H:%M");
				NSTimestamp dateFinSaisie = DateCtrl.stringToDate(myView.getTfDateFin().getText()+" "+myView.getTfHeureFin().getText(), "%d/%m/%Y %H:%M");
								
				newMission = EOMission.dupliquerMission(ec, missionToDuplicate, dateDebutSaisie, dateFinSaisie, fournis);
				newMission.setMisDebut(dateDebutSaisie);
				newMission.setMisFin(dateFinSaisie);
				newMission.setMisEtat(EOMission.ETAT_VALIDE);
				
				// Duplication des trajets
				NSArray<EOSegmentTarif> segments = EOSegmentTarif.findSegmentsTarifsForMission(ec, missionToDuplicate);
				int index = 0;
				for (EOSegmentTarif segment : segments) {

					EOSegmentTarif newSegment = FactorySegmentTarif.sharedInstance().dupliquerTrajet(ec, newMission, segment, missionToDuplicate, 
							myView.getCheckNuits().isSelected(), myView.getCheckTransports().isSelected(), myView.getCheckIndemnites().isSelected());

					if (index == 0) {
						newSegment.setSegDebut(dateDebutSaisie);
					}
					else
						if (index == segments.size()) {
							newSegment.setSegDebut(dateFinSaisie);
						}		
					
					index++;
				}

				// Duplication des remboursements
				if (myView.getCheckRemboursements().isSelected())
					FactorySegmentInfos.sharedInstance().dupliquerSegmentInfos(ec, newMission, missionToDuplicate);

				// PAIEMENT
				EOMissionPaiement paiement = EOMissionPaiement.creer(ec, newMission, NSApp.getExerciceCourant());				
//				EOMissionPreferencesPerso preferences = GestionPreferencesPerso.sharedInstance(ec).getPreferences();
				EOMissionPaiementEngage paiementEngage = FactoryMissionPaiementEngage.sharedInstance().creerMissionPaiementEngage(ec, paiement, NSApp.getExerciceCourant(), null);

				// Duplication du budget
				if (myView.getCheckBudget().isSelected()) {

					EOMissionPaiementEngage paiementToDuplicate = (EOMissionPaiementEngage)EOMissionPaiementEngage.findForMission(ec, missionToDuplicate).objectAtIndex(0);
					
					paiementEngage.setOrganRelationship(paiementToDuplicate.organ());
					paiementEngage.setToTypeCreditRelationship(paiementToDuplicate.toTypeCredit());
					paiementEngage.setModePaiementRelationship(paiementToDuplicate.modePaiement());
					paiementEngage.setPlanComptableRelationship(paiementToDuplicate.planComptable());
					paiementEngage.setCodeAnalytiqueRelationship(paiementToDuplicate.codeAnalytique());
					paiementEngage.setConventionRelationship(paiementToDuplicate.convention());
					paiementEngage.setToCodeExerRelationship(paiementToDuplicate.toCodeExer());

					// Destinations
					
					NSArray<EOMissionPaiementDestin> actionsToDuplicate = FinderMissionPaiementDestin.findDestinationsForPaiementEngage(ec, paiementToDuplicate);
					for (EOMissionPaiementDestin action : actionsToDuplicate) {
												
						EOMissionPaiementDestin newAction = FactoryMissionPaiementDestin.sharedInstance().creerMissionPaiementDestin(ec,  paiementEngage, action.typeAction(), action.mpdPourcentage());
						newAction.setMissionPaiementEngageRelationship(paiementEngage);
						newAction.setMpdMontant(action.mpdMontant());
						
					}
					
				}

				ec.saveChanges();
				
				messageConfirmation = messageConfirmation.concat("- "+fournis.prenom()+" "+fournis.nom()+" (Mission No "+newMission.misNumero()+").\n");
				
				eodAgentsSelectionnes.setObjectArray(new NSArray());
				myView.getMyEOTableSelection().updateData();
				
			}
			catch (NSValidation.ValidationException e)	{
				messageErreur = messageErreur.concat("- "+fournis.prenom()+" "+fournis.nom()+" ("+e.getMessage()+")\n");
				ec.revert();
			}	
			catch (Exception e)	{
				messageErreur = messageErreur.concat("- "+fournis.prenom()+" "+fournis.nom()+".\n");
				e.printStackTrace();
				ec.revert();
			}	
		}
		
		if (messageErreur.length() > 0) {
			messageErreur = "\n\nCette mission n'a pu être dupliquée pour les agents :\n" + messageErreur;
			if (messageConfirmation.length() > 0) {
				messageConfirmation = "La mission No "+currentMission.misNumero()+" a bien été dupliquée pour les agents suivants :\n" + messageConfirmation;
				MsgPanel.sharedInstance().runInformationDialog("OK",messageConfirmation+messageErreur);
			}
			else {
				MsgPanel.sharedInstance().runInformationDialog("OK",messageErreur);
			}
		}
		else {
			MsgPanel.sharedInstance().runConfirmationDialog("OK",messageConfirmation);			
			if (fournisseurs.count() == 1)
				EnteteMission.sharedInstance(ec).setMission(newMission);
		}
		
		CRICursor.setDefaultCursor(myView);		
	}
	
	/**
	 * 
	 * @param sender
	 */
	private void fermer()	{
		
		myView.setVisible(false);
		
	}
	

	private void ajouterAgent()	{
		
		if (currentAgent == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez sélectionner un agent à ajouter.");
			return;
		}
		
		NSMutableArray agentsSelectionnes = new NSMutableArray(eodAgentsSelectionnes.displayedObjects());
		
		if (!agentsSelectionnes.containsObject(currentAgent))	{
			agentsSelectionnes.addObject(currentAgent);
			eodAgentsSelectionnes.setObjectArray(agentsSelectionnes);
			
			myView.getMyEOTableSelection().updateData();
		}
	}
	

	private void supprimerAgent()	{
		
		eodAgentsSelectionnes.deleteSelection();
		myView.getMyEOTableSelection().updateData();
		
	}
	

	private void rechercherAgents()	{
		
		if (StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Veuillez saisir un filtre sur le nom du fournisseur.");
			return;
		}
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_VALIDE_KEY + " = 'O'", null));		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.NOM_KEY  + " caseInsensitiveLike '*" + myView.getTfFiltreNom().getText() + "*' and " + EOFournis.PRENOM_KEY + " != nil", null));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOFournis.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		
		eodAgents.setObjectArray(ec.objectsWithFetchSpecification(fs));		
		myView.getMyEOTableAgents().updateData();
		
	}
			
	
	
	/** 
	 * Classe d'ecoute pour le changement d'affichage de la liste des contrats.
	 */
	public class MissionnaireItemListener implements ItemListener 	{
		public void itemStateChanged(ItemEvent e)		{

			if (e.getStateChange() == ItemEvent.SELECTED) {
				
			}
			
		}
	} 

	
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerAgents implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
			ajouterAgent();			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentAgent = (EOFournis)eodAgents.selectedObject();
		}
	}


	public class ActionListenerRecherchePersonnel implements ActionListener 	{ 
		public void actionPerformed(ActionEvent e) 	{ 
			rechercherAgents();
		} 
	}
	
	
	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ActionListenerHeureDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfHeureDebut().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureDebut().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début de mission n'est pas valide !");
				myView.getTfHeureDebut().selectAll();
			}
			else	{
				myView.getTfHeureDebut().setText(myTime);
				myView.getTfHeureFin().requestFocus();
			}
		}
	}


	/** 
	 * Classe d'ecoute sur la date de fin de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ActionListenerHeureFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfHeureFin().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureFin().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de fin de mission n'est pas valide !");
				myView.getTfHeureFin().selectAll();
			}
			else	{
				myView.getTfHeureFin().setText(myTime);
			}
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ListenerTextFieldHeureDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfHeureDebut().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureDebut().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début de mission n'est pas valide !");
				myView.getTfHeureDebut().selectAll();
			}
			else	{
				myView.getTfHeureDebut().setText(myTime);
				myView.getTfHeureFin().requestFocus();
			}
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ListenerTextFieldHeureFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfHeureFin().getText()))	return;

			String myTime = TimeCtrl.timeCompletion(myView.getTfHeureFin().getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de fin de mission n'est pas valide !");
				myView.getTfHeureFin().selectAll();
			}
			else	{
				myView.getTfHeureFin().setText(myTime);
			}
		}
	}

	
	private class ActionListenerDateTextField implements ActionListener	{

		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}	



	
}
