// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFicheIdentiteCtrl.java

package org.cocktail.kiwi.client.mission;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.nibctrl.MissionDetailView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;

public class MissionDetailCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static MissionDetailCtrl sharedInstance;
	private EOEditingContext ec;
	private MissionDetailView myView;

	private EOMission currentMission;

	public MissionDetailCtrl(EOEditingContext globalEc) {
		ec = globalEc;
		myView = new MissionDetailView(new JFrame(), true);

		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){myView.setVisible(false);}}
		);

		CocktailUtilities.initTextField(myView.getTfNumero(), false, false);
		CocktailUtilities.initTextField(myView.getTfMissionnaire(), false, false);
		CocktailUtilities.initTextField(myView.getTfDebut(), false, false);
		CocktailUtilities.initTextField(myView.getTfFin(), false, false);
		CocktailUtilities.initTextArea(myView.getTaMotif(), false, false);

	}

	public static MissionDetailCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new MissionDetailCtrl(editingContext);
		return sharedInstance;
	}

	public EOMission currentMission() {
		return currentMission;
	}
	public void setCurrentMission(EOMission currentMission) {
		this.currentMission = currentMission;
		updateData();
	}

	private void clearTextFields() {

		myView.getTfNumero().setText("");
		myView.getTfMissionnaire().setText("");
		myView.getTfCreation().setText("");
		myView.getTfModification().setText("");
		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getTaMotif().setText("");

	}

	public void open() {
		setCurrentMission(EnteteMission.sharedInstance(ec).getCurrentMission());
		myView.setVisible(true);
	}

	private void updateData() {

		clearTextFields();

		if (currentMission() != null) {

			myView.setTitle("DETAIL MISSION No " + currentMission().misNumero());

			CocktailUtilities.setNumberToField(myView.getTfNumero(), currentMission().misNumero());

			EOUtilisateur createur = currentMission().utilisateurCreation();
			EOUtilisateur modificateur = currentMission().utilisateurModification();

			CocktailUtilities.setTextToLabel(myView.getTfCreation(), "Créée le " + DateCtrl.dateToString(currentMission().misCreation())+ " par " + createur.individu().personne_persNomPrenom());
			CocktailUtilities.setTextToLabel(myView.getTfModification(), "Modifiée par " + modificateur.individu().personne_persNomPrenom());

			CocktailUtilities.setTextToField(myView.getTfMissionnaire(), currentMission().fournis().individu().identitePrenomFirst());

			String dateDebut = DateCtrl.dateToString(currentMission.misDebut(),"%d/%m/%Y");
			String dateFin = DateCtrl.dateToString(currentMission.misFin(),"%d/%m/%Y");

			String heureDebut = DateCtrl.dateToString(currentMission.misDebut(),"%H:%M");
			String heureFin = DateCtrl.dateToString(currentMission.misFin(),"%H:%M");

			CocktailUtilities.setTextToField(myView.getTfDebut(), dateDebut + " " + heureDebut);
			CocktailUtilities.setTextToField(myView.getTfFin(), dateFin + " " + heureFin);
			CocktailUtilities.setTextToArea(myView.getTaMotif(), currentMission.misMotif());

		}
	}
}