/*
 * Created on 21 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.mission;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.Superviseur;
import org.cocktail.kiwi.client.admin.GestionPreferencesPerso;
import org.cocktail.kiwi.client.factory.FactoryMissionPaiementEngage;
import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderTitreMission;
import org.cocktail.kiwi.client.finders.FinderVisaAvMission;
import org.cocktail.kiwi.client.metier.EOCorps;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOMandat;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementDepense;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOMissionPreferencesPerso;
import org.cocktail.kiwi.client.metier.EOParamCodeExer;
import org.cocktail.kiwi.client.metier.EOPayeur;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTitreMission;
import org.cocktail.kiwi.client.metier.EOVisaAvMission;
import org.cocktail.kiwi.client.metier.budget.EODepense;
import org.cocktail.kiwi.client.paiement.PaiementCtrl;
import org.cocktail.kiwi.client.remboursements.AvancesCtrl;
import org.cocktail.kiwi.client.remboursements.RemboursementsCtrl;
import org.cocktail.kiwi.client.select.CorpsSelectCtrl;
import org.cocktail.kiwi.client.select.ExerciceSelectCtrl;
import org.cocktail.kiwi.client.select.FournisseurSelectCtrl;
import org.cocktail.kiwi.client.select.PayeurSelectCtrl;
import org.cocktail.kiwi.client.select.ResidenceSelectCtrl;
import org.cocktail.kiwi.client.select.TitreMissionSelectCtrl;
import org.cocktail.kiwi.client.trajets.NuitsRepasCtrl;
import org.cocktail.kiwi.client.trajets.TrajetsCtrl;
import org.cocktail.kiwi.common.utilities.CocktailConstantes;
import org.cocktail.kiwi.common.utilities.CocktailIcones;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;
import org.cocktail.kiwi.common.utilities.TimeCtrl;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.eointerface.swing.EOTextField;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * @author cpinsard
 *
 * Gestion de la fenetre concernant l'entete d'une mission
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EnteteMission extends EODialogController{

	/** SPOccupation.typeTemps pour les occupations issues de conges (absences) */
	public final static String SPOCC_TYPE_ABSENCE		= "ABS";
	/** SPOccupation.typeTemps pour les occupations provenant d'une source iCalendar */
	public final static String SPOCC_TYPE_PLANNING 			= "Edt-Ens";

	public final static String LC_CORPS_INTERVENANT_EXTERIEURS = "INT. EXT.";
	public final static String LC_CORPS_ETUDIANT = "ETUDIANT";

	private	static EnteteMission sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private	EOEditingContext ec;

	// Composants Graphiques
	public EOView view;

	public	EOTextArea	motif;
	public	EOTextField	libelleCorps, complementCorps, libellePayeurCourt, libellePayeurLong, identiteMissionnaire, observation, createur, exercice, residence;
	public	EOTextField heureDebut, heureFin, debutMission, finMission, numeroMission, etatMission, etatBudgetaire;
	public	EOTextField	titreMission, labelTitreMission, messageClient;

	public 	EOTextField	lblDebut, lblFin, lblCorps, lblMissionnaire, lblResidence, lblMotif, lblComplement, lblPayeur; 

	public	JButton		btnAddCorps, btnGetExercice, btnAddMissionnaire, btnAddPayeur, btnAddResidence, btnGetTitre;
	public	JButton		btnAjouterMission, btnRechercherMission;
	public	JButton		btnAnnulerMission, btnAnnulerSaisie, btnDupliquer, btnValider, btnModifier;
	public	JButton		btnGetDateDebut, btnGetDateFin;
	public	JCheckBox	tem53;

	private	EOMission 			currentMission;
	private	EOMission 			missionSaved;
	private	EOTitreMission		currentTitreMission;
	private	EOFournis			currentFournis;
	private	EOCorps				currentCorps;
	private	EOPayeur			currentPayeur;
	//	private	EOGroupesMission 	currentGroupe;
	private	EOExercice			currentExercice;
	private	EOUtilisateur		currentUtilisateur;

	private EOMissionPaiement	currentPaiement;
	//private EOMissionPaiementEngage	currentPaiementEngage;

	private EOMissionPreferencesPerso currentPreference;

	private	String param53;

	private	boolean	modeCreation, modeModification;

	/** 
	 * Constructeur 
	 */
	public EnteteMission(EOEditingContext editingContext)	{
		super();
		EOArchive.loadArchiveNamed("EnteteMission", this,"org.cocktail.kiwi.client", this.disposableRegistry());

		ec = editingContext;

		initObject();
	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EnteteMission sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EnteteMission(editingContext);
		return sharedInstance;
	}

	/** 
	 * Initialisation du module. Mise en place des objets graphiques, chargement des nibs, des delegues et des notifications.
	 */
	public void initObject()	{

		currentExercice = NSApp.getExerciceCourant();
		exercice.setText(currentExercice.exeExercice().toString());

		currentUtilisateur = NSApp.getUtilisateur();
		currentTitreMission = FinderTitreMission.findDefaultTitreMission(ec);

		param53 = FinderMissionParametres.getValue(ec, currentExercice, EOMissionParametres.ID_CHECK_PRORATA);

		initView();

		currentMission = null;

		updateUI();
	}

	/**
	 * 
	 *
	 */
	public void initView()	{

		view.setBorder(BorderFactory.createEmptyBorder());

		messageClient.setForeground(new Color(0,0,255));

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_DELETE,null,btnAnnulerMission,"Annuler la mission");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_DUPLICATE,null,btnDupliquer,"Dupliquer l'entete de la mission");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CANCEL,null,btnAnnulerSaisie,"Annuler la saisie");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_VALID,null,btnValider,"Valider la mission");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_UPDATE,null,btnModifier,"Modifier l'entete de la mission");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CALENDAR,null,btnGetDateDebut,"Date de début de la mission");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_CALENDAR,null,btnGetDateFin,"Date de fin de la mission");

		CocktailUtilities.initTextField(identiteMissionnaire, false, false);
		CocktailUtilities.initTextField(numeroMission, false, false);
		CocktailUtilities.initTextField(etatMission, false, false);
		CocktailUtilities.initTextField(etatBudgetaire, false, false);
		CocktailUtilities.initTextField(libelleCorps, false, false);
		//		CocktailUtilities.initTextField(libelleGroupe, false, false);
		CocktailUtilities.initTextField(libellePayeurCourt, false, false);
		CocktailUtilities.initTextField(libellePayeurLong, false, false);
		CocktailUtilities.initTextField(exercice, false, false);
		CocktailUtilities.initTextField(createur, false, false);
		CocktailUtilities.initTextField(debutMission, false, false);
		CocktailUtilities.initTextField(finMission, false, false);		
		CocktailUtilities.initTextField(titreMission, false, false);

		tem53.setSelected(false);

		titreMission.setBackground(new Color(176,180,185));
		etatBudgetaire.setBackground(new Color(176,180,185));
		etatMission.setBackground(new Color(176,180,185));
		numeroMission.setBackground(new Color(176,180,185));

		etatBudgetaire.setToolTipText("Etat budgétaire de la mission");

		//labelTitreMission.setForeground(new Color(255,0,0));
		lblDebut.setForeground(new Color(255,0,0));
		lblFin.setForeground(new Color(255,0,0));
		lblComplement.setForeground(new Color(0,0,0));
		lblCorps.setForeground(new Color(0,0,0));
		//		lblGroupe.setForeground(new Color(255,0,0));
		lblMotif.setForeground(new Color(255,0,0));
		lblMissionnaire.setForeground(new Color(255,0,0));
		lblPayeur.setForeground(new Color(255,0,0));
		lblResidence.setForeground(new Color(255,0,0));

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_ADD, null, btnAjouterMission,"Ajouter une nouvelle mission");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnRechercherMission, "Rechercher une mission");

		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnAddCorps,"Ajouter un corps");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnAddMissionnaire,"Ajouter un missionnaire");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnAddPayeur,"Ajouter un organisme payeur");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnAddResidence,"Ajouter une résidence administrative");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetTitre,"Choix du titre de la mission");
		CocktailUtilities.affecterImageEtTextAuBouton(null, CocktailIcones.ICON_SELECT_16, null, btnGetExercice, "Modifier l'exercice");

		heureDebut.addFocusListener(new ListenerTextFieldHeureDebut());
		heureDebut.addActionListener(new ActionListenerHeureDebut());

		heureFin.addFocusListener(new ListenerTextFieldHeureFin());
		heureFin.addActionListener(new ActionListenerHeureFin());
	}

	/**
	 * 
	 * @return
	 */
	public EOView view()	{
		return view;
	}

	/**
	 * 
	 *
	 */
	public void clearTextFields()	{

		messageClient.setText("");
		titreMission.setText("");
		residence.setText("");
		complementCorps.setText("");
		identiteMissionnaire.setText("");

		libelleCorps.setText("");

		libellePayeurCourt.setText("");
		libellePayeurLong.setText("");
		observation.setText("");
		motif.setText("");
		debutMission.setText("");
		finMission.setText("");
		heureDebut.setText("");
		heureFin.setText("");
		etatMission.setText("");
		etatBudgetaire.setText("");
		numeroMission.setText("");
		createur.setText("");

		tem53.setSelected(false);
	}

	/** Validation et ajout du missionnaire */
	public void addMissionnaire(Object sender) {
		NSApp.setWaitCursor();
		EOFournis fournisseur = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();

		if (fournisseur != null)	{

			btnAddResidence.setEnabled(true);

			currentFournis = fournisseur;

			identiteMissionnaire.setText(currentFournis.nom() + " " + StringCtrl.capitalizedString(currentFournis.prenom()));

			EOMission lastMission = EOMission.findLastMissionForFournis(ec, currentFournis);

			if (lastMission != null)	{
				CocktailUtilities.setTextToField(residence,lastMission.misResidence());		

				currentPayeur = lastMission.payeur();
				libellePayeurCourt.setText(currentPayeur.libellePayeurCourt());
				libellePayeurLong.setText(currentPayeur.libellePayeurLong());

			}

			// si on a Mangue, on recupere (si possible) le corps du missionnaire ...
			EOCorps c = EOCorps.getCorpsFromMangue(ec, currentFournis);

			if (c != null) {
				currentCorps = c;
				currentMission.setCorpsRelationship(c);
				CocktailUtilities.setTextToField(libelleCorps, c.llCorps());

				if (StringCtrl.chaineVide(complementCorps.getText()))
					CocktailUtilities.setTextToField(complementCorps, currentMission.misCorps());
			}
			else	{				
				if (lastMission != null)	{
					currentCorps = lastMission.corps();
					currentMission.setCorpsRelationship(currentCorps);
					currentMission.setMisCorps(lastMission.misCorps());

					if (lastMission.corps() != null)
						CocktailUtilities.setTextToField(libelleCorps,lastMission.corps().lcCorps());

					CocktailUtilities.setTextToField(complementCorps,lastMission.misCorps());
				}
			}			
		}		

		NSApp.setDefaultCursor();
	}



	/**
	 * Ajout d'un nouveau corps pour la mission
	 */	
	public void addCorps(Object sender) {

		NSApp.setWaitCursor();
		EOCorps corps = CorpsSelectCtrl.sharedInstance(ec).getCorps();

		if (corps != null)	{

			currentCorps = corps;
			currentMission.setCorpsRelationship(currentCorps);

			// Mise a jour de l'affichage
			CocktailUtilities.setTextToField(libelleCorps, currentCorps.llCorps());
			//			complementCorps.setText(currentCorps.llCorps());
			//
			//			if (complementCorps.getText().equals(""))
			//				complementCorps.setText(currentMission.misCorps());
		}
		NSApp.setDefaultCursor();
	}

	/**
	 * Ajout d'un nouveau payeur
	 */
	public void addPayeur(Object sender) {

		EOPayeur payeur = PayeurSelectCtrl.sharedInstance(ec).getPayeur();

		if (payeur != null)	{

			currentPayeur = payeur;
			libellePayeurCourt.setText(payeur.libellePayeurImprime());
			libellePayeurLong.setText(payeur.libellePayeurLong());			
		}
	}

	/**
	 * 
	 * @param sender
	 */
	public void getTitre(Object sender)	{

		EOTitreMission titre = TitreMissionSelectCtrl.sharedInstance(ec).getTitreMission();

		if (titre != null)	{
			currentTitreMission = titre;
			titreMission.setText(currentTitreMission.titLibelle());
		}

	}


	/**
	 * 
	 * @param sender
	 */
	public void getResidence(Object sender)	 {

		String res = ResidenceSelectCtrl.sharedInstance(ec).getResidence(currentFournis, currentMission);

		if (res != null)
			residence.setText(res);

	}

	public EOMission getCurrentMission()	{
		return currentMission;
	}


	/**
	 * 
	 * @param sender
	 */
	public void ajouterMission(Object sender)	{

		modeCreation = true;

		missionSaved = currentMission;

		currentMission = null;

		clearTextFields();	

		TrajetsCtrl.sharedInstance(ec).setSelectedOnglet(0);		

		TrajetsCtrl.sharedInstance(ec).setCurrentMission(currentMission);
		RemboursementsCtrl.sharedInstance(ec).setCurrentMission(currentMission);
		AvancesCtrl.sharedInstance(ec).setCurrentMission(currentMission);
		PaiementCtrl.sharedInstance(ec).setCurrentMission(currentMission);

		currentFournis = null;
		currentCorps = null;
		currentPayeur = null;

		currentPreference = GestionPreferencesPerso.sharedInstance(ec).getPreferences();

		majPreferencesPerso();

		createur.setText(StringCtrl.capitalizedString(NSApp.getUtilisateur().individu().prenom())+" "+NSApp.getUtilisateur().individu().nomUsuel());

		debutMission.setText(DateCtrl.dateToString(new NSTimestamp(new Date()), "%d/%m/%Y"));
		finMission.setText(DateCtrl.dateToString(new NSTimestamp(new Date()), "%d/%m/%Y"));

		heureDebut.setText(DateCtrl.dateToString(new NSTimestamp(new Date()), "%H")+":"+DateCtrl.dateToString(new NSTimestamp(new Date()), "%M"));
		heureFin.setText(DateCtrl.dateToString(new NSTimestamp(new Date()), "%H")+":"+DateCtrl.dateToString(new NSTimestamp(new Date()), "%M"));

		etatMission.setText("VALIDE");

		currentExercice = NSApp.getExerciceCourant();
		exercice.setText(currentExercice.exeExercice().toString());

		if (currentPreference != null && currentPreference.titre() != null)
			currentTitreMission = currentPreference.titre();
		else
			currentTitreMission = FinderTitreMission.findDefaultTitreMission(ec);

		if (currentTitreMission != null)
			titreMission.setText(currentTitreMission.titLibelle());
		else
			titreMission.setText("?????");

		// Initialisation de la mission
		currentMission = EOMission.creerMission(ec, 
				new NSTimestamp(),
				new NSTimestamp(),
				currentTitreMission, currentUtilisateur, currentExercice
		);

		updateUI();
	}

	/**
	 * 
	 * @param sender
	 */
	public void modifierMission(Object sender)	{

		modeModification = true;

		missionSaved = currentMission;

		currentPreference = GestionPreferencesPerso.sharedInstance(ec).getPreferences();

		updateUI();

	}

	/**
	 * 
	 *
	 */
	public void majPreferencesPerso()	{

		if (currentPreference != null)	{
			currentPayeur = currentPreference.payeur();
			if (currentPayeur != null)	{
				libellePayeurCourt.setText(currentPayeur.libellePayeurCourt());
				CocktailUtilities.setTextToField(libellePayeurLong, currentPayeur.libellePayeurLong());
			}
		}

	}

	/**
	 * 
	 * @param sender
	 */
	public void dupliquer(Object sender)	{
	
		TrajetsCtrl.sharedInstance(ec).setSelectedOnglet(0);
		NSNotificationCenter.defaultCenter().postNotification("missionHasChanged", null);		
		renouvelerMission(currentMission);

	}

	/**
	 * 
	 *
	 */
	public void renouvelerMission(EOMission missionReference)	{

		ajouterMission(this);

		currentFournis = missionReference.fournis();

		identiteMissionnaire.setText(currentFournis.nom() + " " + StringCtrl.capitalizedString(currentFournis.prenom()));

		CocktailUtilities.setTextToField(residence,missionReference.misResidence());		

		currentPayeur = missionReference.payeur();
		libellePayeurCourt.setText(currentPayeur.libellePayeurCourt());
		libellePayeurLong.setText(currentPayeur.libellePayeurLong());

		currentCorps = missionReference.corps();
		CocktailUtilities.setTextToField(libelleCorps,missionReference.corps().lcCorps());
		CocktailUtilities.setTextToField(complementCorps, missionReference.misCorps());

		if (missionReference.misMotif() != null)	
			motif.setText(missionReference.misMotif());

	}

	/**
	 * 
	 *
	 */
	public void annulerSaisie()	{

		ec.revert();

		currentMission = missionSaved;

		if (currentMission == null)
			clearTextFields();
		else
			actualiser();

		modeCreation = modeModification = false;

		updateUI();

		Superviseur.sharedInstance(ec).missionHasChanged(currentMission);
	}

	/**
	 * 
	 *
	 */	
	public void actualiser()	{

		debutMission.setText(DateCtrl.dateToString(currentMission.misDebut(),"%d/%m/%Y"));
		finMission.setText(DateCtrl.dateToString(currentMission.misFin(),"%d/%m/%Y"));

		heureDebut.setText(DateCtrl.dateToString(currentMission.misDebut(),"%H:%M"));
		heureFin.setText(DateCtrl.dateToString(currentMission.misFin(),"%H:%M"));

		currentTitreMission = currentMission.titreMission();
		CocktailUtilities.setTextToField(titreMission,currentTitreMission.titLibelle());

		numeroMission.setText(currentMission.misNumero().toString());

		currentPaiement = EOMissionPaiement.findPaiementForMission(ec, currentMission);

		try {

			if (!currentMission.isAnnulee())	{

				if (currentMission.utilisateurCreation() == null)
					currentMission.setUtilisateurCreationRelationship(NSApp.getUtilisateur());

				if (currentMission.utilisateurModification() == null)
					currentMission.setUtilisateurModificationRelationship(NSApp.getUtilisateur());


				//				// Si la date de fin de mission est superieure au 01/01/ de l'exercice en cours, on force l'exercice
				//				if ( DateCtrl.isAfterEq(currentMission.misFin(), 
				//						DateCtrl.stringToDate("01/01/"+NSApp.getCurrentExercice().exeExercice()))) {
				//										
				//					if (currentPaiement != null) {
				//						
				//						currentPaiement.setExerciceRelationship(NSApp.getCurrentExercice());
				//					
				//					}
				//					
				//				}

				ec.saveChanges();

			}

		}
		catch (Exception e)	{
			e.printStackTrace();
		}

		exercice.setText(currentMission.toExercice().exeExercice().toString());

		if (currentPaiement != null) {
			etatBudgetaire.setText(currentPaiement.mipEtat());
		}
		else {
			etatBudgetaire.setText(currentMission.misEtat());
		}

		if (currentPaiement != null && currentPaiement.mipEtat().equals("PAYEE"))
			currentMission.setMisEtat("PAYEE");

		etatMission.setText(currentMission.misEtat());

		if (currentMission.numQuotientRemb() != null && currentMission.numQuotientRemb().intValue() > 1)
			tem53.setSelected(true);
		else
			tem53.setSelected(false);

		currentFournis = currentMission.fournis();
		identiteMissionnaire.setText(currentFournis.nom()+" "+ currentFournis.prenom());//+" ("+currentFournis.fouCode()+")");

		EOUtilisateur utilisateurCreation = currentMission.utilisateurCreation();
		//EOUtilisateur utilisateurModification = currentMission.utilisateurModification();

		if (utilisateurCreation != null)
			createur.setText(StringCtrl.capitalizedString(utilisateurCreation.individu().prenom())+" "+utilisateurCreation.individu().nomUsuel());
		else
			createur.setText("Non Renseigné");

		currentCorps = currentMission.corps();

		if (currentCorps != null)
			CocktailUtilities.setTextToField(libelleCorps, currentCorps.lcCorps());
		else
			libelleCorps.setText("");

		if (currentMission.misCorps() != null)
			CocktailUtilities.setTextToField(complementCorps, currentMission.misCorps());
		else
			complementCorps.setText("");

		currentPayeur = currentMission.payeur();
		libellePayeurCourt.setText(currentPayeur.codePayeur());
		libellePayeurLong.setText(currentPayeur.libellePayeurLong());

		motif.setText(currentMission.misMotif());

		if (currentMission.misResidence() != null)	
			residence.setText(currentMission.misResidence());
		else
			residence.setText("");

		if (currentMission.misObservation() != null)	
			CocktailUtilities.setTextToField(observation, currentMission.misObservation());
		else
			observation.setText("");
	}

	/**
	 * 
	 * @param sender
	 */
	public void rechercherMission(Object sender)	{

		EOMission mission = MissionSelectCtrl.sharedInstance(ec).getMission(NSApp.getUtilisateur());

		NSApp.setWaitCursor();

		if (mission !=null)	{
			currentMission = mission;

			actualiser();

			if (!EOMission.controleDatesTrajets(currentMission))
				EODialogs.runInformationDialog("ATTENTION","Certaines dates de trajets ne correspondent pas aux dates de la mission. Merci de les modifier !");

			updateUI();

			Superviseur.sharedInstance(ec).missionHasChanged(currentMission);
		}

		NSApp.setDefaultCursor();
	}



	/**
	 * Rafraichir toutes les donnees de la mission affichee
	 *
	 */
	public void rafraichirMission()	{

		if (currentMission != null)	{

			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentMission)));

			currentMission = EOMission.findMissionForRefresh(ec, currentMission);

			currentPaiement = EOMissionPaiement.findPaiementForMission(ec, currentMission);
			if (currentPaiement != null)
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPaiement)));

			NSArray engages = EOMissionPaiementEngage.findForMission(ec, currentMission);

			for (int i=0;i<engages.count();i++)	{

				EOMissionPaiementEngage paiementEngage = (EOMissionPaiementEngage)engages.objectAtIndex(i);
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(paiementEngage)));

				if (paiementEngage.engage() != null)
					ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(paiementEngage.engage())));

				NSArray depenses = EODepense.findDepensesForPaiementEngage(ec, paiementEngage);
				for (int d=0;d<depenses.count();d++) {

					EODepense depense = (EODepense)depenses.objectAtIndex(d);
					ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(depense)));					

					NSArray mandats = EOMandat.findForDepense(ec, depense);
					for (int m=0;m<mandats.count();m++) {

						EOMandat mandat = (EOMandat)mandats.objectAtIndex(m);
						ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(mandat)));					

					}
				}
			}

			NSArray avances  = FinderVisaAvMission.findAvancesForMission(ec, currentMission);
			for (int i=0;i<avances.count();i++)	{
				EOVisaAvMission avance = (EOVisaAvMission)avances.objectAtIndex(i);
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(avance)));
			}

			actualiser();
			updateUI();

			Superviseur.sharedInstance(ec).missionHasChanged(currentMission);			
		}
	}


	/**
	 * 
	 * @param mission
	 */
	public void setMission (EOMission mission)	{

		NSApp.setWaitCursor();
		if (mission !=null)	{

			currentMission = mission;
			rafraichirMission();

		}
		NSApp.setDefaultCursor();		
	}

	/**
	 * 
	 *
	 */
	public void updateUI()	{

		btnAjouterMission.setVisible(true);
		btnModifier.setVisible(true);
		btnRechercherMission.setVisible(true);

		// La mission est nulle ou ANNULEE ou PAYEE ==> Pas de modifs possibles
		if ( 
				currentMission == null 
				|| currentMission.misEtat() == null 
				|| currentMission.isAnnulee()
				|| (currentMission.isPayee()) 
		)	{

			btnDupliquer.setVisible(currentMission != null && !currentMission.isAnnulee());
			btnAnnulerMission.setVisible(false);
			btnModifier.setVisible(false);
			btnValider.setVisible(false);
			btnAnnulerSaisie.setVisible(false);

			btnAddCorps.setEnabled(false);
			btnAddMissionnaire.setEnabled(false);
			btnAddPayeur.setEnabled(false);
			btnAddResidence.setEnabled(false);
			btnGetDateDebut.setEnabled(false);
			btnGetDateFin.setEnabled(false);
			btnGetTitre.setEnabled(false);
			btnGetExercice.setVisible(false);

			tem53.setVisible(param53 != null && ("O".equals(param53)));
			tem53.setEnabled(false);

			CocktailUtilities.initTextField(complementCorps, false, false);
			CocktailUtilities.initTextField(observation, false, false);
			CocktailUtilities.initTextField(residence, false, false);

			CocktailUtilities.initTextField(heureDebut, false, false);
			CocktailUtilities.initTextField(heureFin, false, false);

			motif.setEditable(false);
			motif.textArea().setBackground(CocktailConstantes.COULEUR_FOND_INACTIF);
		}
		else	{		// La Mission est VALIDE

			if (modeModification || modeCreation)	{

				btnValider.setVisible(true);
				btnAnnulerSaisie.setVisible(true);
				btnRechercherMission.setVisible(false);
				btnDupliquer.setVisible(false);
				btnModifier.setVisible(false);
				btnAjouterMission.setVisible(false);
				btnAnnulerMission.setVisible(false);

				btnAddCorps.setEnabled(true);
				btnAddMissionnaire.setEnabled(modeCreation);
				btnAddPayeur.setEnabled(true);
				btnAddResidence.setEnabled(currentFournis != null);
				btnGetDateDebut.setEnabled(true);
				btnGetDateFin.setEnabled(true);
				btnGetTitre.setEnabled(true);
				btnGetExercice.setVisible(false);

				tem53.setVisible(param53 != null && ("O".equals(param53)));

				tem53.setEnabled( param53 != null && ("O".equals(param53)) );

				CocktailUtilities.initTextField(complementCorps, false, true);
				CocktailUtilities.initTextField(observation, false, true);
				CocktailUtilities.initTextField(residence, false, true);

				CocktailUtilities.initTextField(heureDebut, false, true);
				CocktailUtilities.initTextField(heureFin, false, true);

				motif.setEditable(true);
				motif.textArea().setBackground(CocktailConstantes.COULEUR_FOND_ACTIF);

			}
			else	{
				btnValider.setVisible(false);
				btnAnnulerSaisie.setVisible(false);
				btnAjouterMission.setVisible(true);
				btnDupliquer.setVisible(true);
				btnRechercherMission.setVisible(true);
				btnAnnulerMission.setVisible(true);				

				btnAddCorps.setEnabled(false);
				btnAddMissionnaire.setEnabled(false);
				btnAddPayeur.setEnabled(false);
				btnAddResidence.setEnabled(false);
				btnGetDateDebut.setEnabled(false);
				btnGetDateFin.setEnabled(false);
				btnGetTitre.setEnabled(false);
				btnGetExercice.setVisible(false);

				tem53.setEnabled(false);

				CocktailUtilities.initTextField(complementCorps, false, false);
				CocktailUtilities.initTextField(observation, false, false);
				CocktailUtilities.initTextField(residence, false, false);

				CocktailUtilities.initTextField(heureDebut, false, false);
				CocktailUtilities.initTextField(heureFin, false, false);

				motif.setEditable(false);
				motif.textArea().setBackground(CocktailConstantes.COULEUR_FOND_INACTIF);
			}

			if (modeModification || modeCreation)	{
				Superviseur.sharedInstance(ec).setDisabled();
				TrajetsCtrl.sharedInstance(ec).setDisabled();
				RemboursementsCtrl.sharedInstance(ec).setDisabled();
				PaiementCtrl.sharedInstance(ec).setDisabled();
			}

		}
	}

	/**
	 * 
	 * @param sender
	 */
	public void annulerMission(Object sender)	 {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Cette mission sera définitivement ANNULEE !\nVous ne pourrez plus revenir dessus.\nConfirmez vous cette suppression ?",
				"OUI", "NON"))
			return;

		try {
			ServerProxy.annulerMission(ec, (Number)ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentMission).objectForKey("misOrdre"));
			currentMission.setUtilisateurModificationRelationship(NSApp.getUtilisateur());		
			ec.saveChanges();
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression !\n" + CocktailUtilities.getErrorDialog(e));
		}

		rafraichirMission();
		updateUI();
	}


	/**
	 * 
	 * Verification des occupations de l'individu. Planning (Superplan) et Conges (HAMAC).
	 * 
	 * On ne controle que les plannings d'enseignement concernant Superplan.
	 * On ne controle pas les conges pour les etudiants (Doctorants) ou les intervenants exterieurs.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void testOccupations() throws NSValidation.ValidationException {

		try {

			// Verification de l'occupation de l'individu
			// On ne controle que si le titre de mission est parametre pour un controle des plannings ou conges.
			if (currentTitreMission.titCtrlConge().intValue() == 1 
					|| currentTitreMission.titCtrlPlanning().intValue() == 1) {

				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey(currentMission.misDebut(), "dateDebut");
				parametres.setObjectForKey(currentMission.misFin(), "dateFin");
				parametres.setObjectForKey(currentFournis.individu(), "EOIndividu");

				NSArray occupations = ServerProxy.clientSideRequestServeurDePlanning(ec, parametres);

				if (occupations != null && occupations.count() > 0) {

					String libelleOccupations = "";
					for (int i=0;i<occupations.count();i++) {

						NSDictionary occ = (NSDictionary)occupations.objectAtIndex(i);

						String typeTemps = (String)occ.objectForKey("typeTemps");

						// Pas de controle des conges si ETUDIANT ou INTERVENANT EXTERIEUR
						if (currentMission.corps() != null && typeTemps.equals(SPOCC_TYPE_ABSENCE)) {

							if ( currentMission.corps().lcCorps().equals(LC_CORPS_ETUDIANT)
									||  currentMission.corps().lcCorps().equals(LC_CORPS_INTERVENANT_EXTERIEURS) )
								continue;

						}

						// Controles a effectuer en fonction du titre de mission (titCtrlConge et titCtrkPlanning)

						if (typeTemps.equals(SPOCC_TYPE_ABSENCE) && currentTitreMission.titCtrlConge().intValue() == 1
								||
								typeTemps.equals(SPOCC_TYPE_PLANNING) && currentTitreMission.titCtrlPlanning().intValue() == 1) {

							libelleOccupations += " \n ";
							libelleOccupations += " - Du " + occ.objectForKey("dateDebut");
							libelleOccupations += " au " + occ.objectForKey("dateFin");
							libelleOccupations += " - " + occ.objectForKey("detailsTemps");

						}
					}

					if (libelleOccupations.length() > 0)
						throw new NSValidation.ValidationException("Cet agent a déjà une occupation sur cet intervalle de temps !\n " + libelleOccupations);

				}
			}
		}
		catch (ValidationException ex) {
			throw ex;
		}
		catch (Exception e) {
		}
	}


	/**
	 * 
	 * @return
	 */
	public void testSaisieValide() throws NSValidation.ValidationException	{

		if (currentFournis == null)
			throw new NSValidation.ValidationException("Vous devez saisir un missionnaire !");

		if (StringCtrl.chaineVide(debutMission.getText()) || StringCtrl.chaineVide(finMission.getText()))
			throw new NSValidation.ValidationException("Vous devez saisir une date de début ET une date de fin !");

		if (StringCtrl.chaineVide(heureDebut.getText()) || StringCtrl.chaineVide(heureFin.getText()))
			throw new NSValidation.ValidationException("Vous devez saisir une heure de début ET une heure de fin !");

		NSTimestamp debut = DateCtrl.stringToDate(debutMission.getText()+" "+heureDebut.getText(), "%d/%m/%Y %H:%M");
		NSTimestamp fin = DateCtrl.stringToDate(finMission.getText()+" "+heureFin.getText(), "%d/%m/%Y %H:%M");

		if (debut == null || fin == null)			
			throw new NSValidation.ValidationException("Veuillez vérifier le format des dates et heures de la misssion !");

		if (DateCtrl.isBeforeEq(fin, debut))
			throw new NSValidation.ValidationException("La date de fin de la mission doit être supérieure à la date de début !");

		// L'agent est il deja en mission ? On ne verifie pas les ordres de mission permanents.

		if (currentMission.titreMission().titCtrlDates().intValue() == 1){
			NSArray missions = new NSArray();
			if (modeCreation)
				missions = EOMission.findMissionsForFournisAndPeriode(ec, currentFournis, debut, fin, null);
			else
				missions = EOMission.findMissionsForFournisAndPeriode(ec, currentFournis, debut, fin, currentMission );

			if (missions.count() > 0)	{
				String chaineErreur = "CHEVAUCHEMENT DE MISSIONS !\n\n";
				chaineErreur = chaineErreur.concat("L'agent " + currentFournis.prenom() + " " + currentFournis.nom()+" est déjà affecté aux missions suivantes à cette période :\n");

				for (int i=0;i<missions.count();i++)	{
					EOMission mission = (EOMission)missions.objectAtIndex(i);
					chaineErreur = chaineErreur.concat("\t- Mission No " + mission.misNumero() + " du " +DateCtrl.dateToString(mission.misDebut())+" au "+DateCtrl.dateToString(mission.misFin())+" : " + mission.misMotif()+"\n");
				}

				throw new NSValidation.ValidationException(chaineErreur);
			}

			if (currentCorps == null && StringCtrl.chaineVide(complementCorps.getText()))
				throw new NSValidation.ValidationException("Vous devez saisir un corps OU un complément !");

			if (currentPayeur == null)
				throw new NSValidation.ValidationException("Vous devez saisir un payeur !");

			if (StringCtrl.chaineVide(motif.textArea().getText()))
				throw new NSValidation.ValidationException("Vous devez saisir un motif !");

			if (StringCtrl.chaineVide(residence.getText()))
				throw new NSValidation.ValidationException("Vous devez saisir une résidence !");

			try {
				testOccupations();
			}
			catch (ValidationException e) { throw e; }	
		}


	}

	/**
	 * 
	 * @param sender
	 */
	public void validerMission(Object sender)	{

		boolean datesModifiees = true;

		try {

			boolean modifDatesTrajet = false;

			if (currentPreference != null && "O".equals(currentPreference.updateDatesTrajet()))
				modifDatesTrajet = true;					

			NSApp.setWaitCursor();

			NSTimestamp dateDebutSaisie = DateCtrl.stringToDate(debutMission.getText()+" "+heureDebut.getText(), "%d/%m/%Y %H:%M");
			NSTimestamp dateFinSaisie = DateCtrl.stringToDate(finMission.getText()+" "+heureFin.getText(), "%d/%m/%Y %H:%M");

			if (dateDebutSaisie.getTime() == currentMission.misDebut().getTime() && dateFinSaisie.getTime() == currentMission.misFin().getTime() )
				datesModifiees = false;

			// Dates
			currentMission.setMisDebut(dateDebutSaisie);
			currentMission.setMisFin(dateFinSaisie);

			// Titre
			currentMission.setTitreMissionRelationship(currentTitreMission);

			testSaisieValide();

			Number exerciceDebut = new Integer(DateCtrl.dateToString(currentMission.misDebut(), "%Y"));
			EOExercice exerciceMission = FinderExercice.findExercice(ec, exerciceDebut);

			if (exerciceMission == null) {
				throw new NSValidation.ValidationException("Veuillez créer l'exercice " + exerciceDebut + " dans Jefy_Admin !");
			}

			if (modeCreation)	{	// Affectation de l'exercice et du numero de mission

				currentMission.setToExerciceRelationship(exerciceMission);

				Number newNumero = ServerProxy.clientSideRequestgetNumeroMission(ec, exerciceMission.exeExercice());

				currentMission.setMisNumero(new Integer(newNumero.intValue()));

			}
			else	{		

				EOSegmentTarif premierSegment = EOSegmentTarif.findFirstSegmentForMission(ec, currentMission);
				EOSegmentTarif dernierSegment = EOSegmentTarif.findLastSegmentForMission(ec, currentMission);

				// Si on a plusieurs trajets et que le debut du dernier trajet se termine après la date de fin de la mission ==> Warning 
				if (currentMission.segmentsTarif().count() > 1 && DateCtrl.isAfter(dernierSegment.segDebut(), currentMission.misFin())) {
					EODialogs.runInformationDialog("INFO", "Merci de vérifier les dates des trajets, celles ci n'ont pu être mises à jour automatiquement.");
				}

				if (premierSegment != null)	{

					if (!modifDatesTrajet)	{

						if (DateCtrl.isBefore(premierSegment.segDebut(), currentMission.misDebut()))
							premierSegment.setSegDebut(currentMission.misDebut());

						if (DateCtrl.isBefore(currentMission.misFin(), premierSegment.segDebut() ))
							premierSegment.setSegDebut(currentMission.misDebut());

						if (DateCtrl.isAfter(dernierSegment.segFin(), currentMission.misFin()))
							dernierSegment.setSegFin(currentMission.misFin());					
					}
					else	{

						premierSegment.setSegDebut(currentMission.misDebut());
						dernierSegment.setSegFin(currentMission.misFin());

					}

					// On ne recalcule les nuits seulement si les dates de la mission ont change

					if (datesModifiees && premierSegment.zoneParisProvince() && currentMission.isMissionPaiement())	{

						if (!NuitsRepasCtrl.sharedInstance(ec).calculerDefaultNuits(premierSegment))
							throw new Exception("Erreur de calcul des nuits !");
						if (!NuitsRepasCtrl.sharedInstance(ec).calculerDefaultRepas(premierSegment))
							throw new Exception("Erreur de calcul des repas !");

					}					
				}				

			}

			if (!modeCreation)
				currentMission.setUtilisateurModificationRelationship(NSApp.getUtilisateur());		

			// Missionnaire
			currentMission.setFournisRelationship(currentFournis);

			currentMission.setMisCreation(new NSTimestamp());

			// Quotient Remboursement
			if (tem53.isSelected())	{
				currentMission.setNumQuotientRemb(EOMissionParametres.getNumerateurRemb(ec, currentMission.toExercice()));
				currentMission.setDenQuotientRemb(EOMissionParametres.getDenominateurRemb(ec, currentMission.toExercice()));
			}
			else	{
				currentMission.setNumQuotientRemb(new Integer(1));
				currentMission.setDenQuotientRemb(new Integer(1));				
			}

			// Corps / Groupe
			currentMission.setCorpsRelationship(currentCorps);

			if (!StringCtrl.chaineVide(complementCorps.getText()))
				currentMission.setMisCorps(complementCorps.getText());
			else
				if (currentCorps != null)
					currentMission.setMisCorps(currentCorps.llCorps());

			//Payeur
			currentMission.setPayeurRelationship(currentPayeur);
			currentMission.setMisPayeur(currentPayeur.libellePayeurImprime());

			//Motif
			currentMission.setMisMotif(motif.textArea().getText());

			// RESIDENCE
			currentMission.setMisResidence(residence.getText());

			// Observations
			if (!StringCtrl.chaineVide(observation.getText()))
				currentMission.setMisObservation(observation.getText());
			else
				currentMission.setMisObservation(null);

			// PAIEMENT
			if (modeCreation && currentMission.missionPaiements().count() == 0)	{

				EOMissionPaiement paiement = EOMissionPaiement.creer(ec, currentMission, NSApp.getExerciceCourant());

				if (NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION)) {
					if (NSApp.getCurrentUtilisateur().persId().intValue() != currentMission.fournis().persId().intValue()) {
						paiement.setRibfourRelationship(null);
					}
				}

				EOMissionPreferencesPerso preferences = GestionPreferencesPerso.sharedInstance(ec).getPreferences();
				NSArray paramsCodesExer = EOParamCodeExer.findForExercice(ec, currentMission.toExercice());
				FactoryMissionPaiementEngage.sharedInstance().creerMissionPaiementEngage(ec, paiement, NSApp.getExerciceCourant(), preferences);

			}

			// Mise a jour pour une mission sans frais
			if (modeModification && !currentMission.isMissionPaiement() && !currentMission.isSaisieLbud() )	{

				Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
				Number misOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentMission).objectForKey("misOrdre");
				ServerProxy.setMissionSansFrais(ec, misOrdre, utlOrdre);

			}

			ec.saveChanges();
			
			// Actualisation des donnees
			modeCreation = modeModification = false;

			if (currentMission !=null)	{
				TrajetsCtrl.sharedInstance(ec).updateDatas();
				numeroMission.setText(currentMission.misNumero().toString());
				updateUI();
			}

			rafraichirMission();

			Superviseur.sharedInstance(ec).missionHasChanged(currentMission);			

		}		
		catch (NSValidation.ValidationException e)	{			
			MsgPanel.sharedInstance().runErrorDialog("ERREUR",e.getMessage());
		}
		catch (Exception e)	{
			e.printStackTrace();
			MsgPanel.sharedInstance().runErrorDialog("ERREUR","Erreur d'enregistrement de la mission !\n"+CocktailUtilities.getErrorDialog(e));
		}

		Superviseur.sharedInstance(ec).showSouthPanel(true);
		NSApp.setDefaultCursor();
	}	


	/**
	 * 
	 * @param sender
	 */
	public void getDateDebut(Object sender)	{

		CocktailUtilities.setMyTextField(debutMission);
		CocktailUtilities.showDatePickerPanel(new Dialog(((ApplicationClient)ApplicationClient.sharedApplication()).getMainWindow()));
	}

	/**
	 * 
	 * @param sender
	 */
	public void getExercice(Object sender) {

		NSApp.setWaitCursor();

		EOExercice exer = ExerciceSelectCtrl.sharedInstance(ec).getExercice();

		if (exer != null)	{

			currentExercice = exer;
			clearTextFields();
		}

		NSApp.setDefaultCursor();

	}



	/**
	 * 
	 * @param sender
	 */
	public void getDateFin(Object sender)	{

		NSTimestamp dateDebut = DateCtrl.stringToDate(debutMission.getText()+" "+heureDebut.getText(), "%d/%m/%Y %H:%M");
		NSTimestamp dateFin = DateCtrl.stringToDate(finMission.getText()+" "+heureFin.getText(), "%d/%m/%Y %H:%M");

		if (DateCtrl.isAfter(dateDebut, dateFin))	{
			finMission.setText(debutMission.getText());
		}

		CocktailUtilities.setMyTextField(finMission);
		CocktailUtilities.showDatePickerPanel(new Dialog(((ApplicationClient)ApplicationClient.sharedApplication()).getMainWindow()));

	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ActionListenerHeureDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(heureDebut.getText()))	return;

			String myTime = TimeCtrl.timeCompletion(heureDebut.getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début de mission n'est pas valide !");
				heureDebut.selectAll();
			}
			else	{
				heureDebut.setText(myTime);
				heureFin.requestFocus();
			}
		}
	}


	/** 
	 * Classe d'ecoute sur la date de fin de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ActionListenerHeureFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(heureFin.getText()))	return;

			String myTime = TimeCtrl.timeCompletion(heureFin.getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de début de mission n'est pas valide !");
				heureFin.selectAll();
			}
			else	{
				heureFin.setText(myTime);
				motif.requestFocus();
			}
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ListenerTextFieldHeureDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(heureDebut.getText()))	return;

			String myTime = TimeCtrl.timeCompletion(heureDebut.getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure non valide","L'heure de début de mission n'est pas valide !");
				heureDebut.selectAll();
			}
			else	{
				heureDebut.setText(myTime);
				heureFin.requestFocus();
			}
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ListenerTextFieldHeureFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(heureFin.getText()))	return;

			String myTime = TimeCtrl.timeCompletion(heureFin.getText());
			if ("".equals(myTime))	{
				MsgPanel.sharedInstance().runInformationDialog("Heure de fin non valide","L'heure de début de mission n'est pas valide !");
				heureFin.selectAll();
			}
			else	{
				heureFin.setText(myTime);
				motif.requestFocus();
			}
		}
	}



	/**
	 * 
	 */
	public void revaliderMission() {

		if (currentMission == null)  {
			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Veuillez sélectionner une mission !");
			return;
		}

		if (currentMission.isAnnulee())  {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Vous ne pouvez pas revalider une mission ANNULEE !");
			return;
		}

		if (currentMission.isPayee())  {

			NSArray<EOMissionPaiementDepense> depenses = EOMissionPaiementDepense.findDepensesForMission(ec, currentMission);
			BigDecimal montantDepenses = new BigDecimal(0);
			for (EOMissionPaiementDepense myPaiementDepense : depenses)				
				montantDepenses = montantDepenses.add(myPaiementDepense.depense().depTtcSaisie());

			if (montantDepenses.compareTo(new BigDecimal(0)) > 0) {
				MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Cette mission a des dépenses associées, vous ne pouvez pas la re-valider !");
				return;
			}
		}

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la revalidation de cette mission ?", "OUI", "NON"))
			return;

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();		
			parametres.setObjectForKey((EOEnterpriseObject)currentMission,"EOMission");

			ServerProxy.clientSideRequestRevaliderMission(ec, parametres);

			MsgPanel.sharedInstance().runConfirmationDialog("OK","La mission est de nouveau VALIDE !");

			EnteteMission.sharedInstance(ec).rafraichirMission();

		}
		catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur lors de la validation !\n" + CocktailUtilities.getErrorDialog(ex));
		}

	} 


	/**
	 * 
	 */
	public void modifierExerciceMission() {


		if (currentMission == null)  {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Veuillez sélectionner une mission !");
			return;
		}

		if (currentMission.isAnnulee())  {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Vous ne pouvez pas modifier l'exercice d'une mission ANNULEE !");
			return;
		}

		EOExercice newExercice = ExerciceSelectCtrl.sharedInstance(ec).getExercice();

		if (newExercice != null) {

			try {

				currentMission.setToExerciceRelationship(newExercice);

				ec.saveChanges();

				EnteteMission.sharedInstance(ec).rafraichirMission();

			}
			catch (Exception ex) {
				ec.revert();
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR","Erreur lors de la validation !\n" + CocktailUtilities.getErrorDialog(ex));
			}
		}

	} 
}
