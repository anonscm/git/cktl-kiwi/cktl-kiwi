/*
 * Created on 22 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.client.mission;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.finders.FinderExercice;
import org.cocktail.kiwi.client.finders.FinderMissionParametres;
import org.cocktail.kiwi.client.finders.FinderRembZone;
import org.cocktail.kiwi.client.finders.FinderTypeTransport;
import org.cocktail.kiwi.client.metier.EOFonction;
import org.cocktail.kiwi.client.metier.EOFournis;
import org.cocktail.kiwi.client.metier.EOIndividu;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.metier.EOMissionPaiement;
import org.cocktail.kiwi.client.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.client.metier.EOMissionParametres;
import org.cocktail.kiwi.client.metier.EOPlanComptable;
import org.cocktail.kiwi.client.metier.EORembZone;
import org.cocktail.kiwi.client.metier.EOSegGroupeInfo;
import org.cocktail.kiwi.client.metier.EOSegTypeInfo;
import org.cocktail.kiwi.client.metier.EOSegmentTarif;
import org.cocktail.kiwi.client.metier.EOTitreMission;
import org.cocktail.kiwi.client.metier.EOTypeTransport;
import org.cocktail.kiwi.client.metier.EOWebpays;
import org.cocktail.kiwi.client.metier.budget.EOConvention;
import org.cocktail.kiwi.client.nibctrl.MissionSelectView;
import org.cocktail.kiwi.client.select.ConventionSelectCtrlOld;
import org.cocktail.kiwi.client.select.FournisseurSelectCtrl;
import org.cocktail.kiwi.client.select.PaysSelectCtrl;
import org.cocktail.kiwi.client.select.PlanComptableSelectCtrl;
import org.cocktail.kiwi.client.select.TitreMissionSelectCtrl;
import org.cocktail.kiwi.common.utilities.CRICursor;
import org.cocktail.kiwi.common.utilities.CocktailExports;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;
import org.cocktail.kiwi.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MissionSelectCtrl {

	private static MissionSelectCtrl sharedInstance;
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private	EOEditingContext ec;

	private MissionSelectView myView;

	public  EODisplayGroup 	eod;

	private	EOFournis		currentFournis;
	private EOTitreMission 	currentTitreMission;
	private	EOConvention 	currentConvention;
	private EOWebpays 		currentPays;
	private EOPlanComptable currentCompte;


	public MissionSelectCtrl (EOEditingContext globalEc)	{

		super();

		eod = new EODisplayGroup();
		myView = new MissionSelectView(new JFrame(), true, eod);
		ec = globalEc;


		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getBtnSelectionner().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectionner();}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}
		});

		myView.getBtnExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {exporter();}
		});

		myView.getBtnGetPays().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getPays();}
		});

		myView.getBtnGetDate().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getDateReference();
			}
		});
		myView.getBtnDelDate().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delDateReference();
			}
		});
		myView.getBtnGetMissionnaire().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {rechercherMissionnaire();
			}
		});
		myView.getBtnGetTitre().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getTitre();}
		});

		myView.getBtnDelTitre().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delTitre();}
		});
		myView.getBtnDelPays().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delPays();}
		});

		myView.getBtnGetConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getConvention();}
		});

		myView.getBtnDelConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delConvention();}
		});
		myView.getBtnGetCompte().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getPlanComptable();}
		});
		myView.getBtnDelCompte().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delPlanComptable();}
		});

		CocktailUtilities.initTextField(myView.getTfCompte(), false, false);

		eod.setSortOrderings(EOMission.SORT_ARRAY_DEBUT_DESC);

		myView.initExercices(NSApp.listeExercices());

		myView.initZones(FinderRembZone.findZones(ec));

		myView.initTransports(FinderTypeTransport.findTypesTransport(ec));

		myView.initRemboursements(EOSegGroupeInfo.findGroupesInfo(ec));
		myView.initDetailsRemboursements(EOSegTypeInfo.fetchAll(ec, EOSegTypeInfo.SORT_ARRAY_LIBELLE_ASC));

		myView.getMyEOTable().addListener(new ListenerMission());

		initObject();



	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static MissionSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new MissionSelectCtrl(editingContext);
		return sharedInstance;
	}

	public EOPlanComptable getCurrentCompte() {
		return currentCompte;
	}

	public void setCurrentCompte(EOPlanComptable currentCompte) {
		this.currentCompte = currentCompte;
		myView.getTfCompte().setText("");
		if (currentCompte != null)
			myView.getTfCompte().setText(currentCompte.pcoNum());
	}

	public void windowClosing(WindowEvent e)	{}

	/**
	 * 
	 *
	 */
	public void initObject()	{

		myView.getTfMessageClient().setText("");

		myView.getExercices().setSelectedItem(NSApp.getExerciceCourant());

		myView.getTfMissionnaire().addActionListener(new ActionListenerTextField());
		myView.getTfCreateur().addActionListener(new ActionListenerTextField());
		myView.getTfNumeroMission().addActionListener(new ActionListenerTextField());

		CocktailUtilities.initTextField(myView.getTfDate(), false, false);
		CocktailUtilities.initTextField(myView.getTfPays(), false, false);
		CocktailUtilities.initTextField(myView.getTfConvention(), false, false);
		CocktailUtilities.initTextField(myView.getTfTitreMission(), false, false);
		CocktailUtilities.initTextField(myView.getTfMissionnaire(), false, true);
		CocktailUtilities.initTextField(myView.getTfCreateur(), false, true);
		CocktailUtilities.initTextField(myView.getTfNumeroMission(), false, true);

		myView.getCheckFraisAll().addItemListener(new EtatMissionItemListener());
		myView.getCheckFraisSans().addItemListener(new EtatMissionItemListener());
		myView.getCheckFraisAvec().addItemListener(new EtatMissionItemListener());

		myView.getCheckBudgetAll().addItemListener(new EtatBudgetaireItemListener());
		myView.getCheckBudgetValide().addItemListener(new EtatBudgetaireItemListener());
		myView.getCheckBudgetOrdre().addItemListener(new EtatBudgetaireItemListener());
		myView.getCheckBudgetSignature().addItemListener(new EtatBudgetaireItemListener());
		myView.getCheckBudgetLiquidee().addItemListener(new EtatBudgetaireItemListener());
		myView.getCheckBudgetPayee().addItemListener(new EtatBudgetaireItemListener());
		myView.getCheckBudgetExtourne().addItemListener(new EtatBudgetaireItemListener());

		myView.getCheckEtatAll().addItemListener(new EtatMissionItemListener());
		myView.getCheckEtatValide().addItemListener(new EtatMissionItemListener());
		myView.getCheckEtatAnnulee().addItemListener(new EtatMissionItemListener());
		myView.getCheckEtatTerminee().addItemListener(new EtatMissionItemListener());


		// Les agents liquidation ont le droit de voir toutes les missions
		if (!NSApp.hasFonction(EOFonction.ID_FCT_LIQUIDATION))
			myView.getTfCreateur().setText(NSApp.getUtilisateur().individu().nomUsuel());

		if (NSApp.hasFonction(EOFonction.ID_FCT_PREMISSION) && !NSApp.hasFonction(EOFonction.ID_FCT_CREATION) )	{

			myView.getTfCreateur().setText(NSApp.getUtilisateur().individu().nomUsuel());
			CocktailUtilities.initTextField(myView.getTfCreateur(), false, false);

		}

	}


	/**
	 * 
	 * @return
	 */
	public EOMission getMission(EOUtilisateur utilisateur)	{

		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(true);

		myView.show();

		((ApplicationClient)ApplicationClient.sharedApplication()).setGlassPane(false);

		if (eod.selectedObjects().count() == 1)	{

			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOMission)eod.selectedObject())));

			return (EOMission)eod.selectedObject();
		}

		return null;
	}

	/**
	 * 
	 * @param sender
	 */
	public void getDateReference()	{

		CocktailUtilities.setMyTextField(myView.getTfDate());
		CocktailUtilities.showDatePickerPanel(myView);

	}


	/**
	 * 
	 * @param sender
	 */
	public void delDateReference()	{

		myView.getTfDate().setText("");

	}
	/**
	 * 
	 * @param sender
	 */
	public void getPays()	{

		if (myView.getZones().getSelectedIndex() == 0)	{
			MsgPanel.sharedInstance().runInformationDialog("ERREUR","Aucune zone n'est sélectionnée !");
			return;
		}

		EOWebpays pays = PaysSelectCtrl.sharedInstance(ec).getPays((EORembZone)myView.getZones().getSelectedItem());

		if (pays != null)	{
			currentPays = pays;
			myView.getTfPays().setText(currentPays.libelleCourt());
			rechercher();
		}
	}

	/**
	 * 
	 *
	 */
	public void delPays()	{

		currentPays = null;
		myView.getTfPays().setText("");

	}


	public void getPlanComptable() {
		EOPlanComptable planco = PlanComptableSelectCtrl.sharedInstance(ec).getPlanComptable("6", getExercice());
		if (planco != null)
			setCurrentCompte(planco);
	} 	
	public void delPlanComptable() {
		setCurrentCompte(null);
	}

	public void getConvention() {

		NSApp.setWaitCursor();

		EOExercice exerciceOrigine = FinderExercice.findExercice(ec, ((EOExercice)myView.getExercices().getSelectedItem()).exeExercice());

		String paramAllConventions = FinderMissionParametres.getValue(ec, exerciceOrigine, EOMissionParametres.ID_ALL_CONVENTIONS);

		EOConvention convention = ConventionSelectCtrlOld.sharedInstance(ec).getConvention(exerciceOrigine, null, null, paramAllConventions);

		if (convention != null)	{
			currentConvention = convention;
			CocktailUtilities.setTextToField(myView.getTfConvention(), currentConvention.convIndex().toString() + " - " + currentConvention.convReferenceExterne());
		}

		updateInterface();
		NSApp.setDefaultCursor();

	}  

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public void delConvention() {

		myView.getTfConvention().setText("");
		currentConvention = null;

		updateInterface();

	} 

	/**
	 * 
	 * @param sender
	 */
	public void getTitre()	{
		EOTitreMission titre = TitreMissionSelectCtrl.sharedInstance(ec).getTitreMission();

		if (titre != null)	{
			currentTitreMission = titre;
			myView.getTfTitreMission().setText(currentTitreMission.titLibelle());
		}
	}

	/**
	 * @param sender
	 */
	public void delTitre()	{
		currentTitreMission = null;
		myView.getTfTitreMission().setText("");
		rechercher();
	}

	/**
	 * 
	 * @param sender
	 */
	public void rechercherMissionnaire()	{

		EOFournis fournisseur = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();

		if (fournisseur != null)	{
			currentFournis = fournisseur;
			myView.getTfMissionnaire().setText(currentFournis.nom());
		}
	}


	private EOExercice getExercice() {
		try {
			return (EOExercice)myView.getExercices().getSelectedItem();
		}
		catch (Exception e) {
			return null;
		}
	}

	public EOQualifier getQualifierRecherche()	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		EOExercice exerciceOrigine = FinderExercice.findExercice(ec, ((EOExercice)myView.getExercices().getSelectedItem()).exeExercice());

		// Exercice
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TO_EXERCICE_KEY + " = %@", new NSArray(exerciceOrigine)));


		// charges a payer
		if (myView.getCheckChargeAPayer().isSelected())
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_PAIEMENTS_KEY + "."+EOMissionPaiement.MIP_CAP_KEY + "= %@", new NSArray("O")));

		// Numero
		try {
			if (!StringCtrl.chaineVide(myView.getTfNumeroMission().getText()))
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_NUMERO_KEY + "=%@", new NSArray(new Integer(myView.getTfNumeroMission().getText()))));
		}
		catch (Exception e)	{
			EODialogs.runErrorDialog("ERREUR","Veuillez vérifier le format du numéro de mission !");
		}

		// Agent
		if (!StringCtrl.chaineVide(myView.getTfCreateur().getText()))	{

			NSMutableArray qualifsCreateur = new NSMutableArray();

			String recherche = "*"+myView.getTfCreateur().getText().toUpperCase()+"*";
			qualifsCreateur.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMission.UTILISATEUR_CREATION_KEY+"."+EOUtilisateur.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY+" caseInsensitiveLike %@", new NSArray(recherche)));
			qualifsCreateur.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOMission.UTILISATEUR_MODIFICATION_KEY+"."+EOUtilisateur.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY+" caseInsensitiveLike %@", new NSArray(recherche)));

			mesQualifiers.addObject(new EOOrQualifier(qualifsCreateur));

		}

		// Date
		if (!StringCtrl.chaineVide(myView.getTfDate().getText()))	{

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_DEBUT_KEY + " <= %@ ", new NSArray(DateCtrl.stringToDate(myView.getTfDate().getText() + " 23:59", "%d/%m/%Y %H:%M"))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_FIN_KEY + " >= %@ ", new NSArray(DateCtrl.stringToDate(myView.getTfDate().getText()  + " 00:00", "%d/%m/%Y %H:%M"))));

		}

		// Motif / Observations
		if (CocktailUtilities.getTextFromField(myView.getTfMotif()) != null) {
			NSMutableArray orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_MOTIF_KEY + " caseInsensitiveLike %@ ", new NSArray("*"+myView.getTfMotif().getText()+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_OBSERVATION_KEY + " caseInsensitiveLike %@ ", new NSArray("*"+myView.getTfMotif().getText()+"*")));
			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		// Titre Mission
		if (currentTitreMission != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TITRE_MISSION_KEY + " = %@", new NSArray(currentTitreMission)));			


		// Avec ou sans frais
		if (myView.getCheckFraisAvec().isSelected() || myView.getCheckFraisSans().isSelected()) {

			if (myView.getCheckFraisAvec().isSelected())
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TITRE_MISSION_KEY+"."+ EOTitreMission.TIT_PAIEMENT_KEY + " = %@", new NSArray(new Integer("1"))));
			else
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TITRE_MISSION_KEY+"."+ EOTitreMission.TIT_PAIEMENT_KEY + " = %@", new NSArray(new Integer("0"))));	
		}

		// Missionnaire
		if (!StringCtrl.chaineVide(myView.getTfMissionnaire().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.FOURNIS_KEY+"."+EOFournis.NOM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfMissionnaire().getText()+"*")));

		//Etat Mission
		if (!myView.getCheckEtatAll().isSelected())	{

			if( myView.getCheckEtatValide().isSelected())
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " != %@", new NSArray(EOMission.ETAT_ANNULE)));
			else
				if( myView.getCheckEtatAnnulee().isSelected())
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " = %@", new NSArray(EOMission.ETAT_ANNULE)));
				else
					if( myView.getCheckEtatTerminee().isSelected())
						mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " = %@", new NSArray(EOMission.ETAT_TERMINE)));
					else
						if( myView.getCheckEtatCloturee().isSelected())
							mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " = %@", new NSArray(EOMission.ETAT_CLOTURE)));
		}

		// Etat budgetaire
		if (!myView.getCheckBudgetAll().isSelected() && (myView.getCheckBudgetValide().isSelected() || myView.getCheckBudgetOrdre().isSelected() 
				|| myView.getCheckBudgetSignature().isSelected() || myView.getCheckBudgetLiquidee().isSelected() 
				|| myView.getCheckBudgetPayee().isSelected() || myView.getCheckBudgetExtourne().isSelected() ) ) {

			NSMutableArray mesQualifiersEtat = new NSMutableArray();

			if (myView.getCheckBudgetValide().isSelected())
				mesQualifiersEtat.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_PAIEMENTS_KEY+"."+EOMissionPaiement.MIP_ETAT_KEY +"= %@", new NSArray(EOMissionPaiement.ETAT_VALIDE)));

			if (myView.getCheckBudgetOrdre().isSelected())
				mesQualifiersEtat.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_PAIEMENTS_KEY+"."+EOMissionPaiement.MIP_ETAT_KEY +"= %@", new NSArray(EOMissionPaiement.ETAT_ORDRE)));

			if (myView.getCheckBudgetSignature().isSelected())
				mesQualifiersEtat.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_PAIEMENTS_KEY+"."+EOMissionPaiement.MIP_ETAT_KEY +"= %@", new NSArray(EOMissionPaiement.ETAT_SIGNATURE)));

			if (myView.getCheckBudgetLiquidee().isSelected())
				mesQualifiersEtat.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_PAIEMENTS_KEY+"."+EOMissionPaiement.MIP_ETAT_KEY +"= %@", new NSArray(EOMissionPaiement.ETAT_LIQUIDE)));

			if (myView.getCheckBudgetPayee().isSelected())
				mesQualifiersEtat.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_PAIEMENTS_KEY+"."+EOMissionPaiement.MIP_ETAT_KEY +"= %@", new NSArray(EOMissionPaiement.ETAT_PAYE)));

			if (myView.getCheckBudgetExtourne().isSelected())
				mesQualifiersEtat.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_PAIEMENTS_KEY+"."+EOMissionPaiement.MIP_ETAT_KEY +"= %@", new NSArray(EOMissionPaiement.ETAT_EXTOURNE)));

			mesQualifiers.addObject(new EOOrQualifier(mesQualifiersEtat));

		}

		if (getCurrentCompte() != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("missionPaiements.missionPaiementEngages"+"."+EOMissionPaiementEngage.PLAN_COMPTABLE_KEY + "=%@", new NSArray(getCurrentCompte())));

		if (currentConvention != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("missionPaiements.missionPaiementEngages.convention = %@", new NSArray(currentConvention)));			

		if (myView.getTfUb().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("missionPaiements.missionPaiementEngages.organ.orgUb caseInsensitiveLike %@", new NSArray("*"+myView.getTfUb().getText().toUpperCase()+"*")));

		if (myView.getTfCr().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("missionPaiements.missionPaiementEngages.organ.orgCr caseInsensitiveLike %@", new NSArray("*"+myView.getTfCr().getText().toUpperCase()+"*")));

		if (myView.getTfSousCr().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("missionPaiements.missionPaiementEngages.organ.orgSouscr caseInsensitiveLike %@", new NSArray("*"+myView.getTfSousCr().getText().toUpperCase()+"*")));

		if (!myView.getCheckMesMissions().isSelected()) {
			//Lignes Budgetaires Utilisateur
			String paramRestrictionLbuds = FinderMissionParametres.getValue(ec, exerciceOrigine, EOMissionParametres.ID_CHECK_RESTRICTION_LBUDS);
			if (paramRestrictionLbuds != null && "O".equals(paramRestrictionLbuds)) {

				NSMutableArray mesOrQualifiers = new NSMutableArray();

				mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("missionPaiements.missionPaiementEngages.organ = nil", null));

				mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("missionPaiements.missionPaiementEngages.organ.utilisateurOrgans.utilisateur = %@", new NSArray(NSApp.getUtilisateur())));

				mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));

			}		
		}
		else {

			NSMutableArray mesOrQualifiers = new NSMutableArray();
			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.UTILISATEUR_CREATION_KEY + " = %@", new NSArray(NSApp.getUtilisateur())));
			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.UTILISATEUR_MODIFICATION_KEY + " = %@", new NSArray(NSApp.getUtilisateur())));

			mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));

		}

		if (myView.getZones().getSelectedIndex() > 0)	{
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentsTarif.rembZone = %@",new NSArray((EORembZone)myView.getZones().getSelectedItem())));

			if (currentPays != null)	{
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentsTarif.webpays = %@",new NSArray(currentPays)));
			}	
		}

		if (myView.getTypesTransport().getSelectedIndex() > 0)	{

			EOTypeTransport typeTransport =(EOTypeTransport)myView.getTypesTransport().getSelectedItem();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentsTarif.transports.typeTransport = %@",new NSArray(typeTransport)));

		}

		if (myView.getRemboursements().getSelectedIndex() > 0)	{

			NSMutableArray mesOrQualifiers = new NSMutableArray();

			EOSegGroupeInfo groupeInfo = (EOSegGroupeInfo)myView.getRemboursements().getSelectedItem();

			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentsInfos.segTypeInfo.segGroupeInfo = %@",new NSArray(groupeInfo)));

			if (groupeInfo.stgLibelle().toUpperCase().indexOf("AVANCE") >= 0)				
				mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MISSION_AVANCES_KEY + ".mavId != nil", null));				

			mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));

		}

		if (myView.getPopupDetailRemb().getSelectedIndex() > 0)	{
			EOSegTypeInfo typeInfo = (EOSegTypeInfo)myView.getPopupDetailRemb().getSelectedItem();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("segmentsInfos.segTypeInfo = %@",new NSArray(typeInfo)));
		}


		if (mesQualifiers.count() == 1)	
			return null;

		return new EOAndQualifier(mesQualifiers);
	}

	/**
	 * 
	 * @param sender
	 */
	public void rechercher()	{

		CRICursor.setWaitCursor(myView);	
		EOQualifier myQualifier = getQualifierRecherche();

		if (myQualifier == null)	{
			MsgPanel.sharedInstance().runInformationDialog("ATTENTION","Veuillez entrer au moins un critère de recherche !");
			return;
		}

		EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME, myQualifier, null);
		fs.setUsesDistinct(true);
		NSArray missions = ec.objectsWithFetchSpecification(fs);		
		eod.setObjectArray(missions);
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();
		myView.getTfMessageClient().setText(eod.displayedObjects().count()+ " Missions");

		CRICursor.setDefaultCursor(myView);	

	}


	public void selectionner()	{
		myView.dispose();
	}


	public void annuler()	{
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}


	/** 
	 * Classe d'ecoute pour le changement d'affichage de la liste des contrats.
	 */
	public class EtatMissionItemListener implements ItemListener 	{
		public void itemStateChanged(ItemEvent e)		{

			if (e.getStateChange() == ItemEvent.SELECTED)
				rechercher();

		}
	} 

	public class EtatBudgetaireItemListener implements ItemListener 	{
		public void itemStateChanged(ItemEvent e)		{

			rechercher();

		}
	} 

	/**
	 * 
	 * @param sender
	 */
	public void nouvelleMission(Object sender)	{

		myView.dispose();

		EnteteMission.sharedInstance(ec).ajouterMission(this);

	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ActionListenerTextField implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			rechercher();
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat 
	 */
	public class ListenerTextField implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			rechercher();
		}
	}


	private class ListenerMission implements ZEOTableListener {

		public void onDbClick() {
			selectionner();
		}

		public void onSelectionChanged() {
		}
	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {

		String entete = "";

		entete += CocktailExports.ajouterChamp("No");
		entete += CocktailExports.ajouterChamp("NOM");
		entete += CocktailExports.ajouterChamp("PRENOM");	
		entete += CocktailExports.ajouterChamp("DEBUT");	
		entete += CocktailExports.ajouterChamp("FIN");	
		entete += CocktailExports.ajouterChamp("TITRE");	
		entete += CocktailExports.ajouterChamp("MOTIF");	
		entete += CocktailExports.ajouterChamp("ZONE(S)");	
		entete += CocktailExports.ajouterChamp("ETAT");
		entete += CocktailExports.ajouterChamp("MONTANT BUD");	
		entete += CocktailExports.ajouterChamp("MONTANT REMB");	
		entete += CocktailExports.ajouterChamp("UB");	
		entete += CocktailExports.ajouterChamp("CR");	
		entete += CocktailExports.ajouterChamp("SOUS CR");	
		entete += CocktailExports.ajouterChamp("TCD");	
		entete += CocktailExports.ajouterChamp("COMPTE");	
		entete += CocktailExports.ajouterChamp("CREATEUR");	
		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;
	}

	/**
	 * 
	 * @param sender
	 */
	public void exporter()	{

		CRICursor.setWaitCursor(myView);

		String chaineExcel = enteteExport();


		for (int i=0;i<eod.displayedObjects().count();i++)	{

			EOMission mission =  (EOMission)eod.displayedObjects().objectAtIndex(i);

			// 1 - UAI
			chaineExcel += CocktailExports.ajouterChampNumber(mission.misNumero());

			if (mission.fournis() != null) {
				chaineExcel += CocktailExports.ajouterChamp(mission.fournis().nom());
				chaineExcel += CocktailExports.ajouterChamp(mission.fournis().prenom());
			}

			chaineExcel += CocktailExports.ajouterChampDate(mission.misDebut(), "%d/%m/%Y %H:%M");
			chaineExcel += CocktailExports.ajouterChampDate(mission.misFin(), "%d/%m/%Y %H:%M");
			chaineExcel += CocktailExports.ajouterChamp(mission.titreMission().titLibelle());

			String motifMission = StringCtrl.replace(mission.misMotif(), "\n"," ");
			chaineExcel += CocktailExports.ajouterChamp(motifMission);

			NSMutableArray<EOSegmentTarif> trajets = new NSMutableArray<EOSegmentTarif>(mission.segmentsTarif());
			EOSortOrdering.sortArrayUsingKeyOrderArray(trajets, EOSegmentTarif.SORT_ARRAY_DEBUT_DESC);
			String chaineZones = "";

			for (EOSegmentTarif trajet : trajets)	{

				chaineZones = chaineZones.concat(trajet.rembZone().remLibelle());
				if (trajet.zoneEtranger())	{
					String libellePays = (String)NSArray.componentsSeparatedByString(trajet.webpays().wpaLibelle(), "     ").objectAtIndex(0);
					chaineZones = chaineZones.concat("("+libellePays+")");
				}

				if (trajet != trajets.lastObject())
					chaineZones = chaineZones.concat(" - ");

			}

			chaineExcel += CocktailExports.ajouterChamp(chaineZones);
			chaineExcel += CocktailExports.ajouterChamp(mission.misEtat());

			EOMissionPaiement missionPaiement = EOMissionPaiement.findPaiementForMission(ec, mission);

			// S agit t il d'une mission AVEC FRAIS ?
			if (missionPaiement != null && missionPaiement.mission().isMissionPaiement()) {

				chaineExcel += CocktailExports.ajouterChampDecimal(missionPaiement.mipMontantTotal());
				chaineExcel += CocktailExports.ajouterChampDecimal(missionPaiement.mipMontantPaiement());

				EOMissionPaiementEngage paiementEngage = EOMissionPaiementEngage.findForMission(ec, mission).get(0);

				if (paiementEngage != null) {

					if (paiementEngage.organ() != null) {
						chaineExcel += CocktailExports.ajouterChamp(paiementEngage.organ().orgUb());
						chaineExcel += CocktailExports.ajouterChamp(paiementEngage.organ().orgCr());
						chaineExcel += CocktailExports.ajouterChamp(paiementEngage.organ().orgSouscr());
					}
					else {
						chaineExcel += CocktailExports.ajouterChampVide(3);
					}

					if (paiementEngage.toTypeCredit() != null)
						chaineExcel += CocktailExports.ajouterChamp(paiementEngage.toTypeCredit().tcdCode());

					if (paiementEngage.planComptable() != null)	
						chaineExcel += CocktailExports.ajouterChamp(paiementEngage.planComptable().pcoNum());

				}
			}
			else {

				chaineExcel += CocktailExports.ajouterChampDecimal(new BigDecimal(0));
				chaineExcel += CocktailExports.ajouterChampDecimal(new BigDecimal(0));
				chaineExcel += CocktailExports.ajouterChampVide(5);

			}

			chaineExcel += CocktailExports.ajouterChamp(mission.utilisateurCreation().individu().nomUsuel());

			chaineExcel += CocktailExports.SAUT_DE_LIGNE;
		}

		NSApp.exportExcel(chaineExcel, "ExportMissions");
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	public void updateInterface() {

		myView.getBtnDelConvention().setEnabled(currentConvention != null);

	}

}
