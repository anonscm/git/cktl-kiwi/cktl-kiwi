package org.cocktail.kiwi.client.mission;

import javax.swing.JFrame;

import org.cocktail.kiwi.client.ApplicationClient;
import org.cocktail.kiwi.client.ServerProxy;
import org.cocktail.kiwi.client.metier.EOMission;
import org.cocktail.kiwi.client.nibctrl.NumerotationView;
import org.cocktail.kiwi.common.utilities.CocktailUtilities;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.common.utilities.MsgPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSMutableDictionary;

public class NumerotationCtrl {
	
	private static NumerotationCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private NumerotationView myView;
	
	private EOMission currentMission;
	
	public NumerotationCtrl(EOEditingContext editingContext) {
		
		super();

		ec = editingContext;

		myView = new NumerotationView(new JFrame(), true);
		
		myView.getButtonValidate().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});
		
	}

	
	public static NumerotationCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new NumerotationCtrl(editingContext);
		return sharedInstance;
	}
	
	
	public void open() {

		currentMission = EnteteMission.sharedInstance(ec).getCurrentMission();
		
		actualiser();
		
		myView.show();
	
	}

	
	/**
	 * 
	 */
	private void actualiser() {
		
		if (currentMission == null)  {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Veuillez sélectionner une mission !");
			return;
		}

		if (currentMission.isAnnulee())  {

			MsgPanel.sharedInstance().runErrorDialog("ERREUR", "Vous ne pouvez pas modifier l'exercice d'une mission ANNULEE !");
			return;
		}

		
		myView.getTfMission().setText("Mission No " + currentMission.misNumero() + " - du " + DateCtrl.dateToString(currentMission.misDebut()) + " au " + DateCtrl.dateToString(currentMission.misFin()));

		myView.getTfNumero().setText("");
		
				
	}
	
	
	
	/**
	 * 
	 */
	private void valider() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous le changement de numéro de cette mission ?", "OUI", "NON"))
			return;

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();		
			parametres.setObjectForKey((EOEnterpriseObject)currentMission,"EOMission");
			parametres.setObjectForKey(new Integer(myView.getTfNumero().getText()),"numero");

			ServerProxy.clientSideRequestNumerotationMission(ec, parametres);

			MsgPanel.sharedInstance().runConfirmationDialog("OK","Le changement de numérotation a bien été enregistré !");
			
			EnteteMission.sharedInstance(ec).rafraichirMission();
			
			myView.dispose();

		}
		catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur de modification du paramètre !\n" + CocktailUtilities.getErrorDialog(ex));
		}

	}
	
}
