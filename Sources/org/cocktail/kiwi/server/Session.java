
package org.cocktail.kiwi.server;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.cocktail.application.serveur.CocktailSession;
import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.application.serveur.eof.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.kiwi.server.factory.FactoryMission;
import org.cocktail.kiwi.server.factory.JefycoFactory;
import org.cocktail.kiwi.server.metier.EOIndividu;
import org.cocktail.kiwi.server.metier.EOMission;
import org.cocktail.kiwi.server.metier.EOMissionAvance;
import org.cocktail.kiwi.server.metier.EOModePaiement;
import org.cocktail.kiwi.server.metier.EOOrgan;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPathUtilities;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

import fr.univlr.cri.planning.DemandePlanning;
import fr.univlr.cri.planning.SPConstantes;
import fr.univlr.cri.planning.SPOccupation;

public class Session extends CocktailSession {

	public Application woApplication;

	public JefycoFactory myJefycoFactory;
	private GregorianCalendar myCalendar;
	public SessionPrint remoteCallPrint;

	public Session() {
		super();
		// Recuperation de l'application en cours
		woApplication = (Application) WOApplication.application();
		woApplication.getMySessions().put(sessionID(), this);
		remoteCallPrint = new SessionPrint(woApplication,defaultEditingContext());

		//initialisation des factory
		if (myJefycoFactory == null)
			myJefycoFactory = new JefycoFactory();
	}
	
	public String clientSideRequestAppVersion() { return VersionMe.txtAppliVersion();}

	public boolean distributionContextShouldFollowKeyPath(EODistributionContext distributionContext, String path) {
		return (path.startsWith("session"));
	}

	public String clientSideRequestVersion() { return ((Application)WOApplication.application()).version();}

	/** Permet d'envoyer un mail à partir du client. L'emetteur est l'utilisateur de l'application.
	 * @param from
	 * @param mailTo
	 * @param sujet
	 * @param texte
	 */
	public String clientSideRequestEnvoyerMailAgent(String from,String mailTo,String sujet, String texte) throws Exception {
		try {
			CktlWebApplication application = (CktlWebApplication)WOApplication.application();
			application.mailBus().sendMail(from,mailTo,null, sujet, texte);
			return "Le mail a bien été envoyé.";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	/**
	 * 
	 * @return
	 */
	public NSTimestamp clientSideRequestGetCurrentDate()	{
		return new NSTimestamp();
	}

	public void clientSideRequestCheckMissionsPayees()	{

		JefycoFactory.checkMissionsPayees(defaultEditingContext());
	}

	/**
	 * 
	 * @return
	 */
	public CktlConfig config() {
		return woApplication.config();
	}

	/**
	 * 
	 * @param nomTable
	 * @param nomQualifier
	 * @param leDico
	 * @return
	 */
	public NSArray getFetchSpec(String nomTable, String nomQualifier, NSDictionary leDico) {
		return EOUtilities.objectsWithFetchSpecificationAndBindings(defaultEditingContext(), nomTable, nomQualifier, leDico);
	}

	/**
	 * 
	 * @param nomQualifier
	 * @param leDico
	 * @param key
	 * @return
	 */
	public String getStoreProcedure(String nomQualifier, NSDictionary leDico, String key) {
		return (EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), nomQualifier, leDico).objectForKey(key).toString());
	}

	/**
	 * 
	 * @return
	 */
	public String getHomeDir() {
		return (NSPathUtilities.homeDirectory());
	}

	/**
	 * 
	 * @param homePath
	 * @param fileName
	 * @return
	 */
	public String appendingPath(String homePath, String fileName) {
		return (NSPathUtilities.stringByAppendingPathComponent(homePath, fileName));
	}

	/**
	 * 
	 * @param myArray
	 */
	public void invalidObj(NSArray myArray) {
		defaultEditingContext().invalidateObjectsWithGlobalIDs(myArray);
	}

	/**
	 * 
	 * @param nomTable
	 * @param nomQualifier
	 * @param leDico
	 * @return
	 */
	public NSArray clientSideRequestFetchSpec(String nomTable, String nomQualifier, NSDictionary leDico) {
		return this.getFetchSpec(nomTable, nomQualifier, leDico);
	}

	/**
	 * 
	 * @param eodDisp
	 * @return
	 */
	public NSDictionary clientSideRequestPrimarySpec(EOEnterpriseObject eodDisp) {
		return this.getPrimarySpec(eodDisp);
	}

	/**
	 * 
	 * @param eodDisp
	 * @return
	 */
	public NSDictionary getPrimarySpec(EOEnterpriseObject eodDisp) {
		return EOUtilities.primaryKeyForObject(defaultEditingContext(), eodDisp);
	}

	/**
	 * 
	 * @param nomQualifier
	 * @param leDico
	 * @param key
	 * @return
	 */
	public String clientSideRequestStoreProcedure(String nomQualifier, NSDictionary leDico, String key) {
		return this.getStoreProcedure(nomQualifier, leDico, key);
	}

	/**
	 * 
	 * @param cdeOrdre
	 * @return
	 */
	public String clientSideRequestGetResponsableCr(Number cdeOrdre) {
		NSLog.out.appendln("cdeOrdre=" + cdeOrdre);
		if (cdeOrdre == null)
			return "";
		String respCr = "";
		try {
			NSDictionary retour = EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "responsablecr", new NSDictionary(cdeOrdre, "cdeordre"));
			NSLog.out.appendln("retour:" + retour);
			Object objResp = retour.objectForKey("responsablecr");
			if (objResp instanceof String)
				respCr = (String) objResp;
		}
		catch (Exception e) {
			e.printStackTrace();
			respCr = " ";
		}
		return respCr;
	}

	/**
	 * 
	 * @return
	 */
	public String clientSideRequestHomeDir() {
		return this.getHomeDir();
	}

	/**
	 * 
	 * @param homePath
	 * @param fileName
	 * @return
	 */
	public String clientSideRequestAppendingPath(String homePath, String fileName) {
		return this.appendingPath(homePath, fileName);
	}


	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public String clientSideRequestGetParam(String paramKey) {
		return woApplication.getParam(paramKey);
	}



	public Number clientSideRequestgetNumeroMission(Number exercice)  throws Exception	{

		return FactoryMission.getNumeroMission(defaultEditingContext(), exercice);

	}



	public BigDecimal clientSideRequestGetDisponible(NSDictionary parametres)  throws Exception	{

		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");
		EOTypeCredit typeCredit = (EOTypeCredit)parametres.objectForKey("EOTypeCredit");
		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		Number primaryKeyTypeCredit = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),typeCredit)).objectForKey("tcdOrdre");
		Number primaryKeyExercice = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),exercice)).objectForKey("exeOrdre");

		dicoProc.setObjectForKey(primaryKeyOrgan, "01orgid");
		dicoProc.setObjectForKey(primaryKeyTypeCredit, "02tcdordre");
		dicoProc.setObjectForKey(primaryKeyExercice, "03exeordre");

		return JefycoFactory.getDisponible(defaultEditingContext(), dicoProc);

	}


	/**
	 * 
	 * @param mipOrdre
	 * @param titOrdre
	 * @param etat
	 * @return
	 */
	public String clientSideRequestLiquidation(Number mipOrdre) {
		String message = "OK";
		try {
			myJefycoFactory.liquiderMission(defaultEditingContext(), mipOrdre);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}

	
	public String clientSideRequestLiquidationDefinitive(Number mipOrdre) {
		String message = "OK";
		try {
			myJefycoFactory.liquidationDefinitive(defaultEditingContext(), mipOrdre);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}


	/**
	 * 
	 * @param mipOrdre
	 * @param titOrdre
	 * @param etat
	 * @return
	 */
	public String clientSideRequestReverser(Number mipOrdre, BigDecimal montant, Number utlOrdre, Number depId) {

		String message = "OK";

		try {

			myJefycoFactory.reverser(defaultEditingContext(), mipOrdre, montant, utlOrdre, depId);

		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}

	public String clientSideRequestGetResponsableService(Number noIndividu) {

		String message = "OK";

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(noIndividu, "01noindividu");

			return JefycoFactory.getResponsableService(defaultEditingContext(), parametres);

		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}

	/**
	 * 
	 * @param mipOrdre
	 * @param titOrdre
	 * @param etat
	 * @return
	 */
	public String clientSideRequestLiquidationAvance(Number mipOrdre) {
		String message = "OK";
		try {
			myJefycoFactory.insLiquidationAvance(defaultEditingContext(), mipOrdre);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}


	/**
	 * 
	 * @param mipOrdre
	 * @param cdeOrdre
	 * @param titOrdre
	 * @return
	 */
	public String clientSideRequestEngagerMission(Number mipOrdre, Number utlOrdre) {
		String message = "OK";
		try {
			myJefycoFactory.engagerMission(defaultEditingContext(), mipOrdre, utlOrdre);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}

	/**
	 * 
	 * @param mipOrdre
	 * @param cdeOrdre
	 * @param titOrdre
	 * @return
	 */
	public String clientSideRequestEngagerEtatFrais(Number mipOrdre, Number utlOrdre) {
		String message = "OK";
		try {
			myJefycoFactory.engagerEtatFrais(defaultEditingContext(), mipOrdre, utlOrdre);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}


	public String clientSideRequestInitialiserSignataires() {

		String message = "OK";
		try {
			System.out.println("Session.clientSideRequestInitialiserSignataires() ");
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "initialiserSignataires", null);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}

	public String clientSideRequestInitialiserSignatairesServices() {

		String message = "OK";
		try {
			System.out.println("Session.clientSideRequestInitialiserSignatairesServices() ");
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "initSignatairesServices", null);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}

	/**
	 * 
	 * @param mplOrdre
	 * @return
	 */
	public String clientSideRequestDelMissionPaiementEngage(Number mplOrdre, Number utlOrdre) {

		NSMutableDictionary leDico = new NSMutableDictionary(mplOrdre, "01mpeordre");
		leDico.setObjectForKey(utlOrdre, "02utlordre");

		String message = "OK";
		try {
			System.out.println("Session.clientSideRequestDelMissionPaiementEngage() " + leDico);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "delMissionPaiementEngage", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}


	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestCreerAvance(NSDictionary parametres) throws Exception {

		String message = "OK";

		try {
			EOMission mission =(EOMission)parametres.objectForKey("EOMission"); 
			EOUtilisateur utilisateur =(EOUtilisateur)parametres.objectForKey("EOUtilisateur"); 

			Number primaryKeyMission = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),mission)).objectForKey("misOrdre");
			Number primaryKeyUtilisateur = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),utilisateur)).objectForKey("utlOrdre");

			NSMutableDictionary leDico = new NSMutableDictionary(primaryKeyMission, "01misordre");

			leDico.setObjectForKey((BigDecimal)parametres.objectForKey("montant"), "02montant");

			if (parametres.objectForKey("EOModePaiement") != null) {

				EOModePaiement modePaiement = (EOModePaiement)parametres.objectForKey("EOModePaiement");
				Number primaryKeyModePaiement = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),modePaiement)).objectForKey("modOrdre");
				leDico.setObjectForKey(primaryKeyModePaiement, "03modordre");
			}

			leDico.setObjectForKey(primaryKeyUtilisateur, "04utlordre");

			System.out.println("Session.clientSideRequestCreerAvance() PARAMETRES : " + leDico);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "creerAvance", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
			throw new Exception(message);
		}

		return message;
	}


	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestRevaliderMission(NSDictionary parametres) throws Exception {

		String message = "OK";

		try {
			EOMission mission =(EOMission)parametres.objectForKey("EOMission"); 

			Number primaryKeyMission = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),mission)).objectForKey("misOrdre");

			NSMutableDictionary leDico = new NSMutableDictionary(primaryKeyMission, "01misordre");

			System.out.println("Session.clientSideRequestRevaliderMission() PARAMETRES : " + leDico);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "revaliderMission", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
			throw new Exception(message);
		}

		return message;
	}


	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestNumerotationMission(NSDictionary parametres) throws Exception {

		String message = "OK";

		try {
			EOMission mission =(EOMission)parametres.objectForKey("EOMission"); 

			Number primaryKeyMission = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),mission)).objectForKey("misOrdre");

			NSMutableDictionary leDico = new NSMutableDictionary(primaryKeyMission, "01misordre");

			leDico.setObjectForKey((Integer)parametres.objectForKey("numero"), "02newnumero");

			System.out.println("Session.clientSideRequestNumerotationMission() PARAMETRES : " + leDico);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "renumeroterMission", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
			throw new Exception(message);
		}

		return message;
	}



	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestLiquiderAvance(NSDictionary parametres) throws Exception {

		String message = "OK";

		try {

			EOMissionAvance avance =(EOMissionAvance)parametres.objectForKey("EOMissionAvance"); 
			EOModePaiement modePaiement =(EOModePaiement)parametres.objectForKey("EOModePaiement"); 
			EOUtilisateur utilisateur =(EOUtilisateur)parametres.objectForKey("EOUtilisateur"); 

			Number primaryKeyAvance = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),avance)).objectForKey("mavId");
			Number primaryKeyModePaiement = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),modePaiement)).objectForKey("modOrdre");
			Number primaryKeyUtilisateur = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),utilisateur)).objectForKey("utlOrdre");

			NSMutableDictionary leDico = new NSMutableDictionary(primaryKeyAvance, "01mavid");
			leDico.setObjectForKey(primaryKeyModePaiement, "02modordre");
			leDico.setObjectForKey(primaryKeyUtilisateur, "03utlordre");

			System.out.println("Session.clientSideRequestLiquiderAvance() PARAMETRES : " + leDico);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "liquiderAvance", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
			throw new Exception(message);
		}

		return message;
	}

	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestGenererOp(NSDictionary parametres) throws Exception {

		String message = "OK";

		try {

			EOMissionAvance avance =(EOMissionAvance)parametres.objectForKey("EOMissionAvance"); 
			EOModePaiement modePaiement =(EOModePaiement)parametres.objectForKey("EOModePaiement"); 
			EOUtilisateur utilisateur =(EOUtilisateur)parametres.objectForKey("EOUtilisateur"); 

			Number primaryKeyAvance = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),avance)).objectForKey("mavId");
			Number primaryKeyModePaiement = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),modePaiement)).objectForKey("modOrdre");
			Number primaryKeyUtilisateur = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),utilisateur)).objectForKey("utlOrdre");

			NSMutableDictionary leDico = new NSMutableDictionary(primaryKeyAvance, "01mavid");
			leDico.setObjectForKey(primaryKeyModePaiement, "02modordre");
			leDico.setObjectForKey(primaryKeyUtilisateur, "03utlordre");

			System.out.println("Session.clientSideRequestGenererOp() PARAMETRES : " + leDico);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "genererOp", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
			throw new Exception(message);
		}

		return message;
	}


	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public void clientSideRequestSupprimerAvance(NSDictionary parametres) throws Exception {

		EOMissionAvance avance = (EOMissionAvance)parametres.objectForKey("EOMissionAvance"); 

		Number primaryKeyVisa = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),avance)).objectForKey("mavId");

		NSMutableDictionary leDico = new NSMutableDictionary(primaryKeyVisa, "mavid");

		EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "supprimerAvance", leDico);

	}


	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public void clientSideRequestDelDepense(Number depid) throws Exception {
		NSMutableDictionary leDico = new NSMutableDictionary(depid, "depid");
//		String message = "OK";
		try {
			System.out.println("Session.clientSideRequestDelDepense() DEL DEPENSE : DEP_ID = " + depid);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "delDepense", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
//			message = woApplication.getErrorDialog(e);
		}
//		return message;
	}


	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestDesengagerMission(Number mipOrdre, Number utlOrdre) {
		NSMutableDictionary leDico = new NSMutableDictionary(mipOrdre, "01mipordre");
		leDico.setObjectForKey(utlOrdre, "02utlordre");
		String message = "OK";
		try {
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "desengagerMission", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}


	public String clientSideRequestSolderEngage(Number engId, Number utlOrdre) {
		NSMutableDictionary leDico = new NSMutableDictionary(engId, "01engid");
		leDico.setObjectForKey(utlOrdre, "02utlordre");
		String message = "OK";
		try {
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "solderEngage", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}



	public void clientSideRequestModifierExerciceBudgetaire(NSDictionary parametres) throws Exception {

		NSMutableDictionary leDico = new NSMutableDictionary((Number)parametres.objectForKey("mpeOrdre"), "01mpeordre");
		leDico.setObjectForKey((Number)parametres.objectForKey("exeOrdre"), "02exeordre");
		leDico.setObjectForKey((Number)parametres.objectForKey("utlOrdre"), "03utlordre");


		EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "modifierExerciceBudgetaire", leDico);

	}




	public void clientSideRequestReimputerMission(Number depId, Number misOrdre, Number utlOrdre) throws Exception {

		try {
			NSMutableDictionary leDico = new NSMutableDictionary(depId, "01depid");
			leDico.setObjectForKey(misOrdre, "02misordre");
			leDico.setObjectForKey(utlOrdre, "03utlordre");
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "reimputerMission", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestDesengagerMissionPartiel(Number mipOrdre, Number utlOrdre) {
		NSMutableDictionary leDico = new NSMutableDictionary(mipOrdre, "01mpeordre");
		leDico.setObjectForKey(utlOrdre, "02utlordre");
		String message = "OK";
		try {
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "desengagerMissionPartiel", leDico);
		}
		catch (Exception e) {
			e.printStackTrace();
			message = woApplication.getErrorDialog(e);
		}
		return message;
	}

	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public void clientSideRequestAnnulerMission(Number misOrdre) throws Exception {

		NSMutableDictionary leDico = new NSMutableDictionary(misOrdre, "misordre");

		EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "annulerMission", leDico);

	}


	/**
	 * 
	 * @param mipOrdre
	 * @return
	 */
	public String clientSideRequestSetMissionSansFrais(Number misOrdre, Number utlOrdre) throws Exception {

		NSMutableDictionary leDico = new NSMutableDictionary(misOrdre, "01misordre");
		leDico.setObjectForKey(utlOrdre, "02utlordre");

		String message = "OK";

		System.out.println("Session.clientSideRequestSetMissionSansFrais() PARAMETRES : " + leDico);
		EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "setMissionSansFrais", leDico);

		return message;
	}


	/**
	 * 
	 * @param myArray
	 */
	public void clientSideRequestInvalidateObj(NSArray myArray) {
		this.invalidObj(myArray);
	}

	/**
	 * 
	 * @return
	 */
	public String clientSideRequestGetEmailResponsable() {
		return woApplication.getParam("EMAIL_RESPONSABLE");
	}


	/**
	 * Verification du login/mot de passe
	 */
	public NSDictionary clientSideRequestCheckPassword(String login, String passwd) {

		NSMutableDictionary myDico = new NSMutableDictionary();
		
		CktlUserInfoDB userInfo = new CktlUserInfoDB(woApplication.dataBus());
		
		userInfo.compteForLogin(login, passwd, true);
		myDico.setObjectForKey(userInfo.errorCode(), "errno");
		myDico.setObjectForKey(userInfo.errorMessage(), "msg");

		if (userInfo.persId() != null)
			myDico.setObjectForKey(userInfo.persId(), "persId");

//		LRConfig cfg = woApplication.config();
//
//		if(cfg==null) {
//			return null;
//		}
//
//		SAUTClient sautClient = new SAUTClient(cfg.stringForKey("SAUT_URL"));
//
//		String alias = cfg.stringForKey("APP_ALIAS");
//		java.util.Properties prop = SAUTClient.toProperties(sautClient.requestDecryptedUserInfo(login,passwd,alias));
//
//
//		myDico.setObjectForKey(prop.getProperty("errno"), "errno");
//
//		myDico.setObjectForKey(prop.getProperty("msg"), "msg");
//
//		if (prop.getProperty("persId") != null)
//			myDico.setObjectForKey(prop.getProperty("persId"), "persId");

		return myDico.immutableClone();
	}




	/** Exercice budgetaire en cours */
	public String getExercice() {
		if (myCalendar == null)
			myCalendar = new GregorianCalendar();

		return String.valueOf(myCalendar.get(Calendar.YEAR));
	}

	/**
	 * 
	 */
	public Boolean clientSideRequestSendMail(NSDictionary data) {
		CktlConfig cfg = woApplication.config();
		String from, to, text, subject, cc;
		CktlMailBus mailBus = new CktlMailBus(cfg);

		from = (String) data.objectForKey("expediteur");
		if (from == null)
			from = "";

		to = (String) data.objectForKey("destinataire");
		if (to == null)
			return Boolean.FALSE;

		Object occ = data.objectForKey("cc");
		if (occ != null) {
			cc = (String) data.objectForKey("cc");
		}
		else {
			cc = null;
		}

		subject = (String) data.objectForKey("sujet");
		if (subject == null)
			subject = "";

		text = (String) data.objectForKey("texte");
		if (text == null)
			text = "";

		if (mailBus.sendMail(from, to, cc, subject, text)) {
			return Boolean.TRUE;
		}
		else {
			NSLog.out.appendln("impossible d'envoyer le mail");
			return Boolean.FALSE;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String clientSideRequestServerLogs() {
		StringBuffer str = new StringBuffer("[Serveur]\n");
		str.append("[Config]\n");

		String cfgFilePath = woApplication.configFilePath();
		StringBuffer cfgContent = new StringBuffer();

		try {
			FileInputStream cfgFile = new FileInputStream(cfgFilePath);
			if (cfgFile != null) {
				int byteRead;
				while ((byteRead = cfgFile.read()) != -1) {
					cfgContent.append((char) byteRead);
				}
				cfgFile.close();
				str.append(cfgContent.toString());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		str.append("\n\n[Logs]\n");
		str.append(woApplication.outLog());
		str.append("\n");
		str.append(woApplication.errLog());
		return str.toString();
	}

	//	/**
	//	 * 
	//	 * @return
	//	 */
	//	public String clientSideRequestVersion() {
	//		return woApplication.versionTxt();
	//	}

	/**
	 * 
	 * @return
	 */
	public String clientSideRequestCopyright() {
		return woApplication.copyright();
	}

	/**
	 * 
	 * @return
	 */
	public String clientSideRequestMinDbVersion() {
		return VersionMe.MINDBVERSION;
	}

	/**
	 * 
	 * @return
	 */
	public String clientSideRequestApplicationFinalName() {
		return woApplication.applicationFinalName();
	}

	/**
	 * Permet d'envoyer un mail a partir du client.
	 * 
	 * @param ec
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) throws Exception {
		try {
			if (!woApplication.sendMail(mailFrom,mailTo,mailCC, mailSubject,mailBody)) {
				throw new Exception ("Erreur lors de l'envoi du mail.");  
			}
		} catch (Throwable e) {
			throw new Exception (e.getMessage());
		}

	}

	/** traitement des donnees minefi dans Kiwi */
	public Boolean clientSideRequestTraiterTelechargements() {

		DownloadController dc = woApplication.downloadController();

		boolean result = true;
		try {
			result = dc.traitement();
		}
		catch (Exception e) {
			result = false;
			e.printStackTrace();
		}
		return new Boolean(result);
	}



	 public void clientSideRequestUpdateEngage(NSDictionary parametres) throws Exception	{

		 NSMutableDictionary dicoProc = new NSMutableDictionary();

		 dicoProc.setObjectForKey((Number)parametres.objectForKey("mipOrdre"),"01mipordre");
		 dicoProc.setObjectForKey((BigDecimal)parametres.objectForKey("montantReste"),"02montant");
		 dicoProc.setObjectForKey((Number)parametres.objectForKey("utlOrdre"),"03utlordre");

		 JefycoFactory.updateEngage(defaultEditingContext(), dicoProc);

	 }



	 public NSArray clientSideRequestServeurDePlanning(NSDictionary parametres) {

		 NSTimestamp dateDebut = (NSTimestamp)parametres.objectForKey("dateDebut");
		 NSTimestamp dateFin = (NSTimestamp)parametres.objectForKey("dateFin");
		 EOIndividu individu = (EOIndividu)parametres.objectForKey("EOIndividu");

		 try {
			 NSMutableArray resas = new NSMutableArray();
			 NSArray  occupsFromServeurPlanning = null;
			 boolean serveurPlanningAvailable = true;//woApplication.useServeurPlanning();
			 if (serveurPlanningAvailable) {
				 try {
					 if (fr.univlr.cri.planning.DemandePlanning.serveurPlanningAccessible()) {
						 serveurPlanningAvailable = true;
					 }
					 else {
						 System.out.println("ATTENTION : le serveur de Planning est inaccessible !!!!!");
						 serveurPlanningAvailable = false;
					 }
				 }
				 catch (java.lang.NoClassDefFoundError noclass) {
					 System.out.println("ATTENTION : pas de framework ServeurPlanningFwk, serveur de Planning inaccessible !!!!!");
					 noclass.printStackTrace();
					 serveurPlanningAvailable = false;
				 }
				 catch (Throwable e) {
					 System.out.println("ATTENTION : le serveur de Planning est inaccessible !!");
					 e.printStackTrace();
					 serveurPlanningAvailable = false;
				 }
			 }

			 if (serveurPlanningAvailable) {

				 try {

					 Number noIndividu = (Number) ((NSDictionary)clientSideRequestPrimarySpec(individu)).objectForKey(EOIndividu.NO_INDIVIDU_KEY);
					 Properties response = DemandePlanning.connectionAuServeur(
							 "clientKiwi",
							 SPConstantes.IDKEY_INDIVIDU, noIndividu, dateDebut, dateFin);

					 if (DemandePlanning.getStatut(response))
						 occupsFromServeurPlanning = DemandePlanning.getPlanning(response);
					 else {
						 occupsFromServeurPlanning = null;
						 throw new Exception(DemandePlanning.getError(response));
					 }
				 }
				 catch (Exception e) {
					 System.out.println("Erreur lors de la tentative d'accès au serveur de planning : " + e);
					 e.printStackTrace();
				 }
				 if (occupsFromServeurPlanning == null) {
					 return null;
				 }

				 for (int i = 0; i < occupsFromServeurPlanning.count(); i++) {
					 NSArray spOcc = (NSArray) occupsFromServeurPlanning.objectAtIndex(i);
					 for (int j = 0; j < spOcc.count(); j++) {
						 SPOccupation occ = (SPOccupation)spOcc.objectAtIndex(j);
						 if (occ.getDateDebut() != null && occ.getDateFin() != null) {
							 String detail = occ.getDetailsTemps();
							 String type = occ.getTypeTemps();

							 if (detail != null) {
								 int indexClean = detail.indexOf("statutEvent");
								 if (indexClean > 0) {
									 detail = detail.substring(0, indexClean);
								 }
							 }
							 else {
								 detail = "";
							 }									
							 Object[] objects = { 
									 dateToString(occ.getDateDebut(), "%d/%m/%Y %H:%M"), 
									 dateToString(occ.getDateFin(), "%d/%m/%Y %H:%M"),
									 detail + " [" + type + "]",
									 type};
							 String[] keys = { "dateDebut", "dateFin", "detailsTemps", "typeTemps" };
							 try {
								 NSDictionary ressource = new NSDictionary(objects, keys);
								 resas.addObject(ressource);
							 }
							 catch (Exception e) {
								 e.printStackTrace();
							 }
						 }
					 }
				 }
			 }

			 return resas;
		 }
		 catch (Throwable t) {
			 t.printStackTrace();
			 return null;
		 }
	 }

	 public static String dateToString(NSTimestamp gregorianDate, String dateFormat) {
		 String dateString = "";

		 NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
		 try {
			 dateString = formatter.format(gregorianDate);
		 } catch(Exception ex) { }
		 return dateString;
	 }




}