/*
 * Created on 7 oct. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.server.finder;

import java.math.BigDecimal;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOGrhumParametres;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.kiwi.server.metier.EOCorps;
import org.cocktail.kiwi.server.metier.EOElementCarriere;
import org.cocktail.kiwi.server.metier.EOFournis;
import org.cocktail.kiwi.server.metier.EOIndividu;
import org.cocktail.kiwi.server.metier.EOLogTelechargement;
import org.cocktail.kiwi.server.metier.EOMission;
import org.cocktail.kiwi.server.metier.EOMissionPaiementEngage;
import org.cocktail.kiwi.server.metier.EOMissionParametres;
import org.cocktail.kiwi.server.metier.EOOrgan;
import org.cocktail.kiwi.server.metier.EOOrganSignataire;
import org.cocktail.kiwi.server.metier.EOOrganSignataireTc;
import org.cocktail.kiwi.server.metier.EOSegmentInfos;
import org.cocktail.kiwi.server.metier.EOSegmentTarif;
import org.cocktail.kiwi.server.metier.EOTitreMission;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Finder {


	public static EOExercice exerciceCourant(EOEditingContext edc) {
		try {

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_EXERCICE_KEY + " = %@", new NSArray(DateCtrl.getCurrentYear()));	
			return EOExercice.fetchFirstByQualifier(edc, myQualifier);

		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static EOGrhumParametres parametrePourCle(EOEditingContext edc, String key) {
		try {

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + " = %@", new NSArray(key));	
			return EOGrhumParametres.fetchFirstByQualifier(edc, myQualifier);

		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}



	/**
	 * Recuperation d'une valeur pour une cle donnee
	 *
	 * @param ec Editing context global de l'application
	 * @param key Cle de la valeur a retourner.
	 */
	public static String getParametreValue(EOEditingContext ec, String key)	{

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOMissionParametres.PARAM_KEY_KEY + " = %@", new NSArray(key));
		EOFetchSpecification fs = new EOFetchSpecification(EOMissionParametres.ENTITY_NAME,qual,null);
		fs.setRefreshesRefetchedObjects(true);

		NSArray params = ec.objectsWithFetchSpecification(fs);

		try {return ((EOMissionParametres)params.objectAtIndex(0)).paramValue();}
		catch (Exception e) {return null;}
	}

	/**
	 * Recuperation d'une valeur pour une cle donnee
	 *
	 * @param ec Editing context global de l'application
	 * @param key Cle de la valeur a retourner.
	 */
	public static String getParametreValue(EOEditingContext edc, String key, EOExercice exercice)	{

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionParametres.PARAM_KEY_KEY + " = %@", new NSArray(key)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMissionParametres.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));

			return EOMissionParametres.fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), null).paramValue();

		}
		catch (Exception e) {return null;}
	}


	/**
	 * 
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{

		try {
			EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
			fs.setRefreshesRefetchedObjects(true);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{

		EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);

	}


	/**
	 *  
	 * @param ec
	 * @param fournis
	 * @param exercice
	 * @return
	 */
	public static NSArray findMissionsValidesForFournisAndAnnee (EOEditingContext ec, EOFournis fournis, Number exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("fournis = %@", new NSArray(fournis)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("misEtat != %@", new NSArray(EOMission.ETAT_ANNULE)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("misAnneeOrigine = %@", new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

		return ec.objectsWithFetchSpecification(fs);
	}	

	/**
	 * 
	 * @param ec
	 * @param fournis
	 * @param debut
	 * @param fin
	 * @param missionToExclude
	 * @return
	 */
	public static NSArray findMissionsForFournisAndPeriode (EOEditingContext ec, EOFournis fournis, NSTimestamp debut, NSTimestamp fin, EOMission missionToExclude)	{

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			if (missionToExclude != null)	{
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_DEBUT_KEY  + " != %@", new NSArray(missionToExclude.misDebut())));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_FIN_KEY + " != %@", new NSArray(missionToExclude.misFin())));			
			}

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_ETAT_KEY + " != %@ ", new NSArray(EOMission.ETAT_ANNULE)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.FOURNIS_KEY + " = %@", new NSArray(fournis)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_DEBUT_KEY + " <= %@", new NSArray(fin)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.MIS_FIN_KEY + " >= %@", new NSArray(debut)));

			//		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TITRE_MISSION_KEY + "." + EOTitreMission.TIT_CTRL_DATES_KEY + " = %@", new NSArray(new Integer(1))));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMission.TITRE_MISSION_KEY + "." + EOTitreMission.TIT_PERMANENT_KEY + " = %@", new NSArray(new Integer(0))));

			EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();

		}


	}

	/**
	 * 
	 * @param ec
	 * @param fournis
	 * @return
	 */
	public static EOMission findLastMissionForFournis(EOEditingContext ec, EOFournis fournis)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOMission.MIS_FIN_KEY, EOSortOrdering.CompareDescending));

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMission.FOURNIS_KEY + "= %@", new NSArray(fournis));

		EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME,myQualifier, mySort);

		try {return (EOMission)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}
	}

	/**
	 * 
	 * @param ec
	 * @param fournis
	 * @return
	 */
	public static NSArray findMissionsForAnnee(EOEditingContext ec, Number annee)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("misAnnee = %@", new NSArray(annee));

		EOFetchSpecification fs = new EOFetchSpecification(EOMission.ENTITY_NAME,myQualifier, null);

		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static Number findDernierNumeroMission(EOEditingContext ec, Number annee)	{

		try {

			NSArray sort = new NSArray(new EOSortOrdering(EOMission.MIS_NUMERO_KEY, EOSortOrdering.CompareDescending));

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("misAnneeOrigine = %@ and misNumero != nil", new NSArray(annee));

			EOFetchSpecification	fs = new EOFetchSpecification(EOMission.ENTITY_NAME, myQualifier, sort);
			fs.setRefreshesRefetchedObjects(true);

			NSArray missions = ec.objectsWithFetchSpecification(fs);

			if (missions.count() > 0)
				return ((EOMission)missions.objectAtIndex(0)).misNumero();
			else 
				return (new Integer(0));
		}	
		catch (Exception e)	{e.printStackTrace();return null;}
	}


	/**
	 * Recuperation des segments tarifs associes a une mission
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static NSArray findSegmentsTarifsForMission(EOEditingContext ec, EOMission mission)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOSegmentTarif.SEG_DEBUT_KEY, EOSortOrdering.CompareAscending));
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegmentTarif.MISSION_KEY + " = %@", new NSArray(mission));

		EOFetchSpecification fs = new EOFetchSpecification(EOSegmentTarif.ENTITY_NAME,myQualifier,mySort);
		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 * Recuperation des segments tarifs associes a une mission
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static NSArray findSegmentsInfosForMission(EOEditingContext ec, EOMission mission)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOSegmentInfos.MISSION_KEY + " = %@", new NSArray(mission));

		EOFetchSpecification fs = new EOFetchSpecification(EOSegmentInfos.ENTITY_NAME,myQualifier,null);
		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 * 
	 * @param ec
	 * @param mission
	 * @return
	 */
	public static NSArray findPaiementsForMission(EOEditingContext ec, EOMission mission)	{

		try {
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("missionPaiement.mission = %@", new NSArray(mission));

			EOFetchSpecification fs = new EOFetchSpecification(EOMissionPaiementEngage.ENTITY_NAME,myQualifier, null);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return new NSArray();
		}
	}


	/**
	 * 
	 * @param f
	 * @return
	 */
	public static EOCorps getCorpsFromGepeto(EOEditingContext ec, EOFournis f) {
		try {

			NSMutableArray args = new NSMutableArray();
			args.addObject(f.persId());
			args.addObject(new NSTimestamp());
			args.addObject(new NSTimestamp());

			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					"individu.persId=%@ and dEffetElement<=%@ " +
					"and (dFinElement=nil or dFinElement>=%@)", args);

			EOElementCarriere element = (EOElementCarriere)Finder.fetchObject(ec, EOElementCarriere.ENTITY_NAME, qual, null );
			return element.corps();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//	/**
	//	 * trouve un individu d'apres son numero
	//	 * @param ec
	//	 * @param noIndividu
	//	 * @return
	//	 */
	public static EOIndividu findIndividuForNoIndividuInContext(EOEditingContext ec, Number noIndividu) {

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				EOIndividu.NO_INDIVIDU_KEY + " = %@", new NSArray(noIndividu));	 

		EOFetchSpecification fs = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qual, null);

		try {return (EOIndividu)ec.objectsWithFetchSpecification(fs).lastObject();}
		catch (Exception e)	{return null;}
	}

	/**
	 * tourver un individu par son persId
	 * @param ec
	 * @param persId
	 * @return
	 */
	public static EOFournis findFournisForPersId(EOEditingContext ec, Number persId) {

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFournis.PERS_ID_KEY + " = %@", new NSArray(persId));
		EOFetchSpecification fs = new EOFetchSpecification(EOFournis.ENTITY_NAME, qual, null);

		try {return (EOFournis)ec.objectsWithFetchSpecification(fs).lastObject();}
		catch (Exception e)	{return null;}
	}



	/**
	 * Recupere une liste de organSignataires associes a une branche de l'organigrame budgetaire. 
	 * Si aucun signataire possible (en fonction des limitations par type de credit) n'est trouve au niveau de la branche, on remonte a la branche pere, 
	 * jusqu'eventuellement en haut de l'organigramme.
	 * 
	 * @param organ Branche de l'organigramme sur laquelle est faite la depense
	 * @param timestamp Date (logiquement la date du jour)
	 * @param montant Montant de la depense
	 * @param typeCredit typeCredit Type de credit associe a la depense 
	 * @return un NSArray des ordonnateurs (responsables) associes a une branche de l'organigramme budgetaire.
	 */
	public static NSArray getOrganSignataires(final EOOrgan organ, final NSTimestamp timestamp, final BigDecimal montant, final EOTypeCredit typeCredit) {
		final NSMutableArray signataires = new NSMutableArray();  
		EOOrgan org = organ;

		try {

			while (signataires.count()==0 && org != null ) {
				//	          Filtre sur la date        
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("("+EOOrganSignataire.ORSI_DATE_OUVERTURE_KEY +"=nil or " + 
						EOOrganSignataire.ORSI_DATE_OUVERTURE_KEY +"<=%@) and (" + "("+EOOrganSignataire.ORSI_DATE_CLOTURE_KEY +"=nil or " + 
						EOOrganSignataire.ORSI_DATE_CLOTURE_KEY +">=%@)  )", new NSArray(new Object[]{timestamp, timestamp}));

				//Recuperer tous les organSignataires associes a la ligne budgetaire.
				final NSArray organSignataires = EOQualifier.filteredArrayWithQualifier(org.organSignataires(), qual);

				signataires.addObjectsFromArray(organSignataires);

				// Tri sur le champ TysiId pour l'ordre des signataires
				EOSortOrdering.sortArrayUsingKeyOrderArray(signataires, new NSArray(new EOSortOrdering(EOOrganSignataire.TYSI_ID_KEY, EOSortOrdering.CompareAscending)));

				final EOQualifier qualTcBad = EOQualifier.qualifierWithQualifierFormat(EOOrganSignataireTc.TO_TYPE_CREDIT_KEY +"=%@ and "+ EOOrganSignataireTc.OST_MAX_MONTANT_TTC_KEY + "<%@", new NSArray(new Object[]{typeCredit, montant}));

				//	          Verifier s'il y a des limites depassees sur les types de credits, dans ce cas on ecarte les signataires
				for (int i = 0; i < organSignataires.count(); i++) {
					final EOOrganSignataire element = (EOOrganSignataire) organSignataires.objectAtIndex(i);
					if (EOQualifier.filteredArrayWithQualifier(element.organSignataireTcs(), qualTcBad).count() > 0 ) {
						signataires.removeObject(element);
					}
				}
				org = org.organPere(); 
			}
			return signataires.immutableClone();
		}
		catch (Exception e) {
			return new NSArray();
		}
	}


	public static NSArray findTelechargements (EOEditingContext ec)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOLogTelechargement.LTC_DATE_KEY, EOSortOrdering.CompareDescending));

		EOFetchSpecification fs = new EOFetchSpecification(EOLogTelechargement.ENTITY_NAME, null, mySort);

		return ec.objectsWithFetchSpecification(fs);
	}


}
