package org.cocktail.kiwi.server;

import java.util.Timer;
import java.util.TimerTask;

import org.cocktail.kiwi.server.factory.Factory;
import org.cocktail.kiwi.server.utilities.URLReader;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;


public class DownloadController {

	private EOEditingContext eContext;
	private URLReader urlReader;
	private String urlChanceleriePays, urlChancelerieMonnaie, urlChancelerieTaux, urlChancelerieMission;
	private Timer currentTimer;

	public DownloadController(String urlChanceleriePays, String urlChancelerieMonnaie, String urlChancelerieTaux, String urlChancelerieMission) {
		urlReader = new URLReader();
		this.urlChanceleriePays = urlChanceleriePays;
		this.urlChancelerieMonnaie = urlChancelerieMonnaie;
		this.urlChancelerieTaux = urlChancelerieTaux;
		this.urlChancelerieMission = urlChancelerieMission;
		eContext = ERXEC.newEditingContext();
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean traitement() throws Exception {

		System.out.println("DownloadController.traitement() ...");

		EOUtilities.executeStoredProcedureNamed(eContext, "initTmp", null);
		
		System.out.println("DownloadController.traitement() LOAD PAYS : " + urlChanceleriePays);
		loadFile(urlChanceleriePays, "WebpaysTmp");
		System.out.println("DownloadController.traitement() LOAD MONNAIE : " + urlChancelerieMonnaie);
		loadFile(urlChancelerieMonnaie, "WebmonTmp");
		System.out.println("DownloadController.traitement() LOAD TAUX : " + urlChancelerieTaux);
		loadFile(urlChancelerieTaux, "WebtauxTmp");
		System.out.println("DownloadController.traitement() LOAD MISSION : " + urlChancelerieMission);
		loadFile(urlChancelerieMission, "WebmissTmp");

		
		NSDictionary out = EOUtilities.executeStoredProcedureNamed(eContext, "importer", null);

		String etat = (String) out.objectForKey("disponible");

		System.out.println("DownloadController.traitement() FIN TRAITEMENT - ETAT : " + etat);
		
		if (etat != null && etat.equals("TRAITEMENT TERMINE")) {
			return true;
		}
		else
			return false;
	}

	
	/**
	 * 
	 * @param stringPath
	 * @param entityName
	 * @throws Exception
	 */
	public void loadFile(String stringPath, String entityName) throws Exception {
		
		String maString = urlReader.getURLZip(stringPath);
		
		System.out.println("DownloadController.loadFile() " + stringPath + " , length : " + maString.length());

		insertion(maString, entityName);
		
		eContext.saveChanges();
		
	}

	/**
	 * 
	 * @param datas
	 * @param entityName
	 * @throws Exception
	 */
	private void insertion(String datas, String entityName) throws Exception {

		NSArray resultRows = NSArray.componentsSeparatedByString(datas, "\r"); // "\r" ==> CR

		if (resultRows.count() == 1) {
			resultRows = NSArray.componentsSeparatedByString(datas, "\n"); // "\r" ==> CR
		}
		
		int i = 0;
		while (i < resultRows.count()) {
			String resultRecord = ((String) resultRows.objectAtIndex(i)).trim();
			if (resultRecord.equals("")) {
				break;
			}
			EOGenericRecord record = Factory.getInstanceAndInsert(eContext, entityName);
			record.takeStoredValueForKey(resultRecord, "ligne");
			i = i + 1;
		}
	}

	/**
	 * 
	 * @return
	 */
	public Timer getTimer() {
		return currentTimer;
	}

	/**
	 * 
	 * @param aTimer
	 */
	public void setTimer(Timer aTimer) {
		currentTimer = aTimer;
	}

	
	/**
	 * 
	 * @param jours
	 */
	public void programmer(int jours) {
		currentTimer = new Timer(true);
		long delay = ((long) jours) * 86400000;
		currentTimer.schedule(new DownloadTask(), delay, delay);
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class DownloadTask extends TimerTask {

		public DownloadTask() {
		}

		public void run() {
			try {
				System.out.println(">>>>>>>> Debut de mise a jour des MINEFI : " + new NSTimestamp());
				traitement();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

}