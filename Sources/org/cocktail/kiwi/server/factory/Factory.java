/*
 * Created on 6 oct. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.kiwi.server.factory;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Factory {

	/**
	 * 
	 * Creation d'une nouvelle instance d'objet dans l'editingContext ec
	 * 
	 * @param ec
	 * @param entity
	 * @return
	 */
    public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {

        EOClassDescription description = (EOClassDescription) EOClassDescription.classDescriptionForEntityName(entity);
        EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);

        return object;

    }
    
    /** cree une instance de l'enregistrement d'entite entity et avec l'editingContext eContext */
    public static EOGenericRecord getInstanceAndInsert(EOEditingContext eContext, String entity) {
        EOClassDescription descriptionClass = EOClassDescription.classDescriptionForEntityName(entity);
        EOGenericRecord instance = (EOGenericRecord)descriptionClass.createInstanceWithEditingContext (eContext, null);
        eContext.insertObject(instance);
        return instance;
    }  
    
	/**
	 * 
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String stringQualifier, NSArray sortOrderings, boolean refresh)	{
		
		try {
			
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, null);
			
			EOFetchSpecification fs = new EOFetchSpecification(entityName, myQualifier, sortOrderings);
			fs.setRefreshesRefetchedObjects(refresh);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}
}
