package org.cocktail.kiwi.server.factory;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOUtilisateur;
import org.cocktail.kiwi.server.finder.Finder;
import org.cocktail.kiwi.server.metier.EOCorps;
import org.cocktail.kiwi.server.metier.EOFournis;
import org.cocktail.kiwi.server.metier.EOMission;
import org.cocktail.kiwi.server.metier.EOTitreMission;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


public class FactoryMission {

	private static FactoryMission sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryMission sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryMission();
		return sharedInstance;
	}

	
	/**
	 * 
	 * @param ec
	 * @param titre
	 * @param agent
	 * @param exercice
	 * @return
	 */
	public EOMission creerMission (
			EOEditingContext ec,
			NSTimestamp dateDebut,
			NSTimestamp dateFin, 
			EOTitreMission titre, 
			EOUtilisateur utilisateur,
			EOExercice exercice
	)	{
		
		EOMission mission = (EOMission)Factory.instanceForEntity(ec, EOMission.ENTITY_NAME);
			
		mission.setTitreMissionRelationship(titre);

		mission.setUtilisateurCreationRelationship(utilisateur);		
		mission.setUtilisateurModificationRelationship(utilisateur);
		mission.setToExerciceRelationship(exercice);
		mission.setMisDebut(dateDebut);
		mission.setMisFin(dateFin);
		
		mission.setMisCreation(new NSTimestamp());
		mission.setMisEtat("VALIDE");

		ec.insertObject(mission);
		
		return mission;
	}
	
	
	

/**
 * 
 * @param ec
 * @param exercice
 * @return
 * @throws Exception
 */
	public static Integer getNumeroMission(EOEditingContext ec, Number exercice) throws Exception	{
		
		
		NSMutableDictionary parametres = new NSMutableDictionary();
		parametres.setObjectForKey(exercice, "01exeordre");
		
		NSDictionary retourProc = new NSDictionary();
		
		try {					
			retourProc = EOUtilities.executeStoredProcedureNamed(ec,"getNumeroMission",parametres);
			return new Integer(((Number)retourProc.objectForKey("02numero")).intValue());
		}
		catch (Exception e) {
			System.out.println("FactoryMission.getNumeroMission() - ERREUR - PARAMETRES : " + parametres);
			e.printStackTrace();
			throw new Exception("Erreur de recuperation du numero de mission !/nMESSAGE : " + e.getMessage());
		}

	
	}
	

	
/**
 * Duplication d'une mission pour un nouveau fournisseur
 * 
 * @param ec
 * @param missionReference
 * @param fournis
 * @return		Retourne la nouvelle mission inseree dans l'editing context
 */
    public EOMission dupliquerMission(EOEditingContext ec, EOMission missionReference, EOFournis fournis)	{

    	EOMission mission = creerMission(ec, missionReference.misDebut(), missionReference.misFin(),
    			missionReference.titreMission(), missionReference.utilisateurCreation(), missionReference.toExercice());

    	try {
    		
			Integer newNumero = getNumeroMission(ec, missionReference.toExercice().exeExercice());
			mission.setMisNumero(newNumero);
			
    	}
    	catch (Exception ex) {
    		
    	}
    	
    	// Fournisseur + Corps et Residence
		mission.setFournisRelationship(fournis);

		EOMission lastMission = Finder.findLastMissionForFournis(ec, fournis);
		
		if (lastMission != null)
			mission.setMisResidence(lastMission.misResidence());
		else
			mission.setMisResidence(missionReference.misResidence());
		
		// si on a gepeto, on recupere (si possible) le corps du missionnaire ...
		EOCorps c = Finder.getCorpsFromGepeto(ec, fournis);
		if (c != null) {
			System.out.println("EOMission.dupliquerMission() Ajout Corps Gepeto");
			mission.setCorpsRelationship(c);
			mission.setMisCorps(c.lcCorps());
		}
		else	{				
			if (lastMission != null)	{
				System.out.println("EOMission.dupliquerMission() Ajout Corps LAST MISSION");
				mission.setCorpsRelationship(lastMission.corps());
				mission.setMisCorps(lastMission.misCorps());
			}
		}			
    	
		//mission.setCCorps(mission.corps().cCorps());
		
    	// Payeur
    	mission.setPayeurRelationship(missionReference.payeur());
    	
    	// Motifs, obs, type
    	mission.setMisMotif(missionReference.misMotif());
    	mission.setMisObservation(missionReference.misObservation());
    	mission.setMisPayeur(missionReference.misPayeur());

    	ec.insertObject(mission);
    	
    	return mission;
    }

}
