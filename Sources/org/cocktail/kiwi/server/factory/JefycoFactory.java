/* JefycoFactory.java created by frivalla on Wed 28-May-2003 */
package org.cocktail.kiwi.server.factory;

import java.math.BigDecimal;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;

/**
* @author frivalla
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */


public class JefycoFactory extends Object {

	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static BigDecimal getDisponible(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		
		NSDictionary retourProc = new NSDictionary();
		
		try {
						
			retourProc = EOUtilities.executeStoredProcedureNamed(ec,"getDisponible",parametres);
			
            return new BigDecimal(retourProc.objectForKey("04disponible").toString());
		}
		catch (Exception e) {
			System.out.println("JefycoFactory.getDisponible() - ERREUR - PARAMETRES : " + parametres);
			e.printStackTrace();
			throw new Exception("Erreur de recuperation du disponible budgetaire !/nMESSAGE : " + e.getMessage());
		}
	}

	
	public static String getResponsableService(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		
		NSDictionary retourProc = new NSDictionary();
		
		try {					
			System.out.println("getResponsableService() - PARAMETRES : " + parametres);
			retourProc = EOUtilities.executeStoredProcedureNamed(ec,"getResponsableService",parametres);
			System.out.println("getResponsableService() - DICO : " + retourProc);
			return (String)retourProc.objectForKey("02chaineresp");
		}
		catch (Exception e) {
			System.out.println("getResponsableService() - ERREUR - PARAMETRES : " + parametres);
			e.printStackTrace();
			throw new Exception("Erreur de recuperation du responsable de service !/nMESSAGE : " + e.getMessage());
		}
	}
	

    public static void updateEngage(EOEditingContext ec, NSDictionary parametres) throws Exception	{
        
        try {
            EOUtilities.executeStoredProcedureNamed(ec,"updateEngage",parametres);
        }
        catch (Exception e) {
            throw new Exception("Erreur lors de la modification de l'engagement!/nMESSAGE : " + e.getMessage());
        }
    }
    
    /**
     * Mise a jour des missions LIQUIDEE ==> PAYEE. Etat mis en fonction de l'etat du mandatement (PAYE).
     * @param ec
     */
    public static void checkMissionsPayees(EOEditingContext ec)	{        
        try {
    		EOUtilities.executeStoredProcedureNamed(ERXEC.newEditingContext(), "checkMissionsPayees", null);
        }
        catch (Exception e) {
        	e.printStackTrace();
        }
    }
    /**
     * Cloture des missions sans FRAIS.
     * @param ec
     */
    public static void clotureAutomatiqueMissions(EOEditingContext ec)	{
    	try {
    		EOUtilities.executeStoredProcedureNamed(ERXEC.newEditingContext(), "clotureAutomatiqueMissions", null);
        }
        catch (Exception e) {
        	e.printStackTrace();
        }
    }    
    
/**
 * 
 * Generation d'un Ordre de Reversement pour la mission
 * 
 * @param myEditingContext
 * @param mipOrdre
 */
public void reverser(EOEditingContext myEditingContext, Number mipOrdre, BigDecimal montant, Number utlOrdre, Number depId) {

	System.out.println("JefycoFactory.reverser() ==> MIPORDRE " + mipOrdre+" , DEPID : " + depId + " , Montant : " + montant);

	NSMutableDictionary parametres = new NSMutableDictionary();
	
	parametres.setObjectForKey(mipOrdre, "01mipordre");
	parametres.setObjectForKey(montant, "02montant");
	parametres.setObjectForKey(depId, "03depidrev");
	parametres.setObjectForKey(utlOrdre, "04utlordre");
	
	EOUtilities.executeStoredProcedureNamed(myEditingContext, "reverser", parametres);
}


/** creation de la commande et de lengagement dans jefyco.
* @param myEditingContext
* @param mipOrdre cle de la table mission paiement
* @param cdeOrdre 0 si inexistante mission.paiement.cdeOrdre
* @param titPaiement voir titre_mission
* @param exer exercice sur 4 lettres
*/
public void engagerMission(EOEditingContext myEditingContext, Number mipOrdre, Number utlOrdre) {
	
	Object [] keys = {new String("01mipordre"), new String("02utlordre")};
	Object [] values = {mipOrdre, utlOrdre};
	NSDictionary args = new NSDictionary(values,keys);

	System.out.println("JefycoFactory.ENGAGER_MISSION ==> " + args);

	EOUtilities.executeStoredProcedureNamed(myEditingContext, "engagerMission", args);
}


/** creation de la commande et de lengagement dans jefyco.
* @param myEditingContext
* @param mipOrdre cle de la table mission paiement
* @param cdeOrdre 0 si inexistante mission.paiement.cdeOrdre
* @param titPaiement voir titre_mission
* @param exer exercice sur 4 lettres
*/
public void engagerEtatFrais(EOEditingContext myEditingContext, Number mipOrdre, Number utlOrdre) {
	
	Object [] keys = {new String("01mipordre"), new String("02utlordre")};
	Object [] values = {mipOrdre, utlOrdre};
	NSDictionary args = new NSDictionary(values,keys);

	System.out.println("JefycoFactory.ENGAGER_ETAT_FRAIS ==> " + args);

	EOUtilities.executeStoredProcedureNamed(myEditingContext, "engagerEtatFrais", args);
}


/** creation de ou des liquidations dans jefyco
 * @param myEditingContext
 * @param mipOrdre cle de la table mission paiement
 */
public void liquiderMission(EOEditingContext myEditingContext,Number mipOrdre) {

	System.out.println("JefycoFactory.liquiderMission() ==> MIPORDRE " + mipOrdre);

	Object [] keys = {new String("mipordre")};
	Object [] values = {mipOrdre};
	NSDictionary args = new NSDictionary(values,keys);
	
	EOUtilities.executeStoredProcedureNamed(myEditingContext,"liquiderMission",args);
}

/** creation de ou des liquidations dans jefyco
 * @param myEditingContext
 * @param mipOrdre cle de la table mission paiement
 */
public void liquidationDefinitive(EOEditingContext myEditingContext,Number mipOrdre) {

	System.out.println("JefycoFactory.liquidationDefinitive() ==> MIPORDRE " + mipOrdre);

	Object [] keys = {new String("mipordre")};
	Object [] values = {mipOrdre};
	NSDictionary args = new NSDictionary(values,keys);
	
	EOUtilities.executeStoredProcedureNamed(myEditingContext,"liquiderExtourne",args);
}


/** creation de ou des liquidations dans jefyco
 * @param myEditingContext
 * @param mipOrdre cle de la table mission paiement
 */
public void insLiquidationAvance(EOEditingContext myEditingContext,Number mipOrdre) {

	System.out.println("JefycoFactory.INS_LIQUIDATION_AVANCE() ==> MIPORDRE " + mipOrdre);

	Object [] keys = {new String("mipordre")};
	Object [] values = {mipOrdre};
	NSDictionary args = new NSDictionary(values,keys);
	
	EOUtilities.executeStoredProcedureNamed(myEditingContext,"insLiquidationAvance",args);
}

}
