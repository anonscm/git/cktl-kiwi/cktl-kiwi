package org.cocktail.kiwi.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JasperReport;

import org.cocktail.application.serveur.CocktailApplication;
import org.cocktail.application.serveur.eof.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.kiwi.common.utilities.DateCtrl;
import org.cocktail.kiwi.server.factory.JefycoFactory;
import org.cocktail.kiwi.server.finder.Finder;
import org.cocktail.kiwi.server.metier.EOLogTelechargement;

import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.jdbcadaptor.JDBCContext;
import com.woinject.WOInject;

import er.extensions.eof.ERXEC;


public class Application extends CocktailApplication {

	private static final String[] REQUIRED_PARAMS = { "URL_REP_CHANCELERIE_PAYS",
		"URL_REP_CHANCELERIE_MONNAIE",
		"URL_REP_CHANCELERIE_TAUX",
		"URL_REP_CHANCELERIE_MISSION",
		"SAUT_URL"};

	private DownloadController dc;
	private NSMutableDictionary appParametres;

	public static final boolean SHOWSQLLOGS    = false;

	private MyByteArrayOutputStream	redirectedOutStream, redirectedErrStream;

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
        WOInject.init("org.cocktail.kiwi.server.Application", argv);
	}

	public Application() {
		
		super();
		
		String timeZone = defaultTimeZone();
		TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
		NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(timeZone,false));

		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		
		// redirection des logs
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		
		((NSLog.PrintStreamLogger)NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.err).setPrintStream(System.err);
		
		if (showSqlLogs()) {
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(-1);
		}
		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}

		final Package jasperPackage = JasperReport.class.getPackage();

        try {
        	JefycoFactory.checkMissionsPayees(ERXEC.newEditingContext());
    		
//        	String paramClotureAutomatique = Finder.getParametreValue(ERXEC.newEditingContext(), "CLOTURE_AUTO");
//    		if (paramClotureAutomatique != null && paramClotureAutomatique.equals("O")) {
//        		System.out.println(">>>>>>>> CLOTURE AUTOMATIQUE DES MISSIONS");
//    			JefycoFactory.clotureAutomatiqueMissions(ERXEC.newEditingContext());
//    		}
        }
        catch (Exception ex) {
        	ex.printStackTrace();
        }

		// Telechargement automatique des taux. Programmation.
        
		dc = new DownloadController(getParam("URL_REP_CHANCELERIE_PAYS"),
				getParam("URL_REP_CHANCELERIE_MONNAIE"),
				getParam("URL_REP_CHANCELERIE_TAUX"),
				getParam("URL_REP_CHANCELERIE_MISSION"));


		// On charge les taux de chancellerie a chaque lancement du serveur.
		// Ensuite ce traitement est programme
		
		String paramTelechargementAutomatique = Finder.getParametreValue(ERXEC.newEditingContext(), "TELECHARGEMENT_AUTO", Finder.exerciceCourant(new EOEditingContext()));
		if (paramTelechargementAutomatique != null && paramTelechargementAutomatique.equals("O")) {

	        try {        		
	        	System.out.println(">>>> Lancement du téléchargement des taux d'indemnités MINEFI (Parametre " + Finder.exerciceCourant(new EOEditingContext()).exeExercice() + ")");
				dc.traitement();
	        	System.out.println(">>>> OK - Téléchargement effectué");
	        }
	        catch (Exception ex) {
	        	System.out.println(">>>> ERREUR Traitement Téléchargement");
	        	ex.printStackTrace();
	        }
		}

        //Programmation du telechargement (TOUS LES JOURS)
		System.out.println("Programmation quotidienne du telechargement");
		dc.programmer(1);

		
		// Verification du dernier telechargement effectue
		
		NSArray<EOLogTelechargement> telechargements = EOLogTelechargement.fetchAll(ERXEC.newEditingContext(), EOLogTelechargement.SORT_ARRAY_DATE_DESC);
		try {
			String paramSeuil = getParam("SEUIL_CONTROLE_TELECHARGEMENT_TAUX");
			if (paramSeuil != null) {
				Integer seuilControle = new Integer(paramSeuil);
				NSTimestamp dateDernierTelechargement = telechargements.get(0).ltcDate();
				if (DateCtrl.isAfter(DateCtrl.now(), DateCtrl.dateAvecAjoutJours(dateDernierTelechargement, seuilControle.intValue()))) {
					sendMail(getParam("EMAIL_RESPONSABLE"), getParam("EMAIL_RESPONSABLE"), "", 
							"TELECHARGEMENT TAUX MISSION", 
							"Message d'alerte KIWI.\n\nLes taux de chancelerie n'ont pas été mis à jour depuis au moins " + seuilControle + " jours.");
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(" KIWI - HEURE DE LANCEMENT : " + new NSTimestamp());

	}
	
	/**
	 * 
	 * Recuperation du time zone par defaut.
	 * Recuperation du timezone du config, puis GRHUM_PARAMETRES si inexistant et sinon GMT.s
	 * 
	 * @return
	 */
	private String defaultTimeZone() {

		String valeur = getParam("DEFAULT_TIME_ZONE");

		if (valeur == null) {
			
			EOEditingContext editingContext = ERXEC.newEditingContext();
			
			EOGrhumParametres param = Finder.parametrePourCle(editingContext, "DEFAULT_TIME_ZONE");
			
			if (param != null)
				valeur = param.paramValue();
			
			if (valeur == null) {
				valeur = "Europe/Paris";
			}
		}

		System.out.println("Application.defaultTimeZone() DEFAULT TIME ZONE : " + valeur);

		return valeur; 
	}

	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (EOSharedEditingContext.defaultSharedEditingContext() == null || parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
			EOGenericRecord vTmpRec;
			String previousParamKey = null;
			NSMutableArray a = null;
			for (java.util.Enumeration<EOGenericRecord> enumerator = vParam.objectEnumerator();enumerator.hasMoreElements();) {
				vTmpRec = enumerator.nextElement();
				if (vTmpRec.valueForKey("paramKey") == null || ((String) vTmpRec.valueForKey("paramKey")).equals("")
						|| vTmpRec.valueForKey("paramValue") == null) {
					continue;
				}
				if (!((String) vTmpRec.valueForKey("paramKey")).equalsIgnoreCase(previousParamKey)) {
					if (a != null && a.count() > 0) {
						appParametres.setObjectForKey(a, previousParamKey);
					}
					previousParamKey = (String) vTmpRec.valueForKey("paramKey");
					a = new NSMutableArray();
				}
				if (vTmpRec.valueForKey("paramValue") != null) {
					a.addObject((String) vTmpRec.valueForKey("paramValue"));
				}
			}
			if (a != null && a.count() > 0) {
				appParametres.setObjectForKey(a, previousParamKey);
			}
			EOSharedEditingContext.defaultSharedEditingContext().invalidateAllObjects();
		}
		return appParametres;
	}
	
	public String getParam(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		}
		else {
			res = (String) a.objectAtIndex(0);
		}
		return res;
	}


	
	/**
	 * 
	 * @return
	 */
	protected boolean showSqlLogs() {
		if     (config().valueForKey("SHOWSQLLOGS") == null) {
			return SHOWSQLLOGS ;
		}
		return config().valueForKey("SHOWSQLLOGS").equals("1");
	} 
	
	
	/**
	 *
	 */
	public String bdConnexionName() {

		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		EOModel vModel = vModelGroup.modelNamed(mainModelName());
		NSDictionary vDico = vModel.connectionDictionary();
		
		NSArray url = NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@");
		
		if (url.count() == 0)
			return " Connexion ??";
		
		if (url.count() == 1)
			return (String)url.objectAtIndex(0);
		
		return (String)(NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@")).objectAtIndex(1);
	}
	
	/**
	 * Utile pour recuperer la connection vers la base de donnees.
	 * @return
	 */
	public EOAdaptorContext getAdaptorContext() {
		return getAdaptorChannel().adaptorContext();
	} 

	public Connection getJDBCConnection() {
		return ((JDBCContext)getAdaptorContext()).connection();
	} 
	public EODatabaseContext getDatabaseContext() {
		return CktlDataBus.databaseContext();
	}

	
	public EOAdaptorChannel getAdaptorChannel() {
		return getDatabaseContext().availableChannel().adaptorChannel();
	}
	
	/**
	 * @return
	 */
	public String getReportsLocation() {
		
		String aPath = getDefaultReportsLocation() ;

		if ((config().stringForKey("REPORTS_LOCATION")!=null) && (config().stringForKey("REPORTS_LOCATION").length()>0) ) {

			String reportsLocation = config().stringForKey("REPORTS_LOCATION");

			File resourceFile = new File(reportsLocation);
			if (resourceFile.exists())
				aPath = reportsLocation;
			else
				System.err.println("CHEMIN NON VALIDE : " + resourceFile);
			
		}
		
		System.out.println("Application.getReportsLocation() CHEMIN D IMPRESSION : " + aPath);
			
		return aPath;
	} 
	
	/**
	 * 
	 * @return
	 */
	public String getDefaultReportsLocation() {

		return path().concat("/Contents/Resources/reports/") ;
		
	}
	
	public String applicationFinalName() {
		return VersionMe.APPLICATIONFINALNAME;
	}

	public String versionTxt() {
		return VersionMe.txtAppliVersion();
	}
	public String mainModelName() {
		return "kiwi";
	}

	public String parametresTableName() {
		return "MissionParametres";
	}

	public String configFileName() {
		return "Kiwi.config";
	}

	public String configTableName() {
		return "FwkCktlWebApp_GrhumParametres";
	}

	public String[] requiredParams() {
		return REQUIRED_PARAMS;
	}

	public void cleanLogs()	{
		redirectedErrStream.reset();
		redirectedOutStream.reset();		
	}
	
	
	public String getErrorDialog(Exception e) {
	       e.printStackTrace();

	       String text = e.getMessage();

	       if ((text == null) || (text.trim().length() == 0)) {
	           System.out.println("ERREUR... Impossible de recuperer le message...");
	           e.printStackTrace();
	           text = "Une erreur est survenue. Impossible de récupérer le message, il doit etre accessible dans la console...";
	       }
	       else {
	           String[] msgs = text.split("ORA-20001:");
	           if (msgs.length > 1) {
	               text = msgs[1].split("\n")[0];
	           }
	       }
	       return text;
	}
	
	public String outLogs()	{
		System.out.println("");
		return redirectedOutStream.toString();
	}
	
	public String errLogs()	{
		return redirectedErrStream.toString();
	}
	
	public boolean sendMail( String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) {
		return mailBus().sendMail(mailFrom, mailTo, "", mailSubject, mailBody);
	}
	
	public DownloadController downloadController() {
		return dc;
	}

	public void setDownloadController(DownloadController aDc) {
		this.dc = aDc;
	}
	
		protected String[] maquettesSix() {
			// TODO Auto-generated method stub
			return null;
		}

		protected String fwkVersionNum() {
			// TODO Auto-generated method stub
			return null;
		}

		protected String fwkVersionTxt() {
			// TODO Auto-generated method stub
			return null;
		}

		
		// Retourne les logs out et err
		public String outLog() {
			return redirectedOutStream.toString();
		}

		public String errLog() {
			return redirectedErrStream.toString();
		}


		// Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
		// Garde en memoire un log de taille entre maxCount/2 et maxCount octets
		public class MyByteArrayOutputStream extends ByteArrayOutputStream {
			protected PrintStream out;
			protected int maxCount;

			public MyByteArrayOutputStream() {
				this(System.out, 0);
			}

			public MyByteArrayOutputStream(PrintStream out) {
				this(out, 0);
			}

			public MyByteArrayOutputStream(PrintStream out, int maxCount) {
				super(maxCount);
				this.out = out;
				this.maxCount = maxCount;
			}

			public synchronized void write(int b) {
				if (maxCount > 0 && count + 1 > maxCount) {
					shift(Math.min(maxCount >> 1, count));
				}
				super.write(b);
				out.write(b);
			}

			public synchronized void write(byte[] b, int off, int len) {
				if (maxCount > 0 && count + len > maxCount) {
					shift(Math.min(maxCount >> 1, count));
				}
				super.write(b, off, len);
				out.write(b, off, len);
			}

			private void shift(int shift) {
				for (int i = shift; i < count; i++) {
					buf[i - shift] = buf[i];
				}
				count = count - shift;
			}
		}

}