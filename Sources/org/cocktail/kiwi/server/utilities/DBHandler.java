package org.cocktail.kiwi.server.utilities;
//
//  DBHandler.java
//  EdtScol
//
//  Created by Adour on Tue Apr 06 2004.
//  Copyright (c) 2004 __Université de La Rochelle__. All rights reserved.
//

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
* Cette classe permet de faciliter les fetchs dans une application JavaClient cote client.
* Une version cote serveur existe egalement.
*/

public class DBHandler {

    public static final int PLUS_BASSE = 0;
    public static final int PLUS_HAUTE = 1;
    
    
    /** fait une selection de la derniere valeur d'une sequence 
    * return nextval de la sequence
    */
    public static Number seqValue(String seqName,EOEditingContext eContext) {
        EOFetchSpecification myFetch = new EOFetchSpecification(seqName, null,null);
        myFetch.setUsesDistinct(true);
        NSArray retour = eContext.objectsWithFetchSpecification(myFetch);
        if(retour.count()>0)
            return (Number)((EOGenericRecord)retour.objectAtIndex(0)).valueForKey("nextval");
        else
            return null;
    }
    
    /** renvoi un qualifier construit avec l'expression et les valeurs */
    public static EOQualifier getQualifier(String expression, NSArray values) {
        return EOQualifier.qualifierWithQualifierFormat(expression,values);
    }
    
    /** renvoi un qualifier construit avec l'expression simple sans arguments */
    public static EOQualifier getQualifier(String expression) {
        return EOQualifier.qualifierWithQualifierFormat(expression,null);
    }
    
    /** renvoi un qualifier construit avec le champs field et la valeur que l'on veut lui associer.
    exemple : <br />
    DBHandler.getSimpleQualifier("individuUlr",eoIndividu) va faire :<br />
    EOQualifier.qualifierWithQualifierFormat("individuUlr=%@",new NSArray(eoIndividu))*/
    public static EOQualifier getSimpleQualifier(String field,Object value) {
        return EOQualifier.qualifierWithQualifierFormat(field+"=%@",new NSArray(value));
    }
       
    
    /** 
    * retourne les globalIDs des objets dans l'editingcontext
    * @param eContext : l'edition context de l'objet
    * @param objects NSArray de EOGenericRecord ou sous-classe
    * @return un NSArray de EOGlobalID des objets
    */
    public static NSArray globalIDsForObjects(EOEditingContext eContext,NSArray objects) {
        NSMutableArray gids = new NSMutableArray();
        for(int i=0;i<objects.count();i++)
            gids.addObject( eContext.globalIDForObject((EOEnterpriseObject)objects.objectAtIndex(i)) );
        return gids;
    }
    
    /** retourne objets a partir des globalID, attention cette methode provoque une exception si l'objet n'est pas  connu de l'editingContext, utiliser faultsForGlobalIDs pour etre sur.<br />
    @param  eContext editingContext de travail
    @param ids NSArray contenant les globalIDs
    @retrun NSArray contenant les faults des globalids passes en parametres
    */
    public static NSArray objectsForGlobalIDs(EOEditingContext eContext,NSArray ids) {
        NSMutableArray objects = new NSMutableArray();
        for(int i=0;i<ids.count();i++)
            objects.addObject( eContext.objectForGlobalID( (EOGlobalID)ids.objectAtIndex(i) ) );
        return objects;
    }

    /** retourne des faults d'objets à partir des globalIDs */
    public static NSArray faultsForGlobalIDs(EOEditingContext eContext,NSArray ids) {
        NSMutableArray objects = new NSMutableArray();
        for(int i=0;i<ids.count();i++)
            objects.addObject( (EOEnterpriseObject)eContext.faultForGlobalID( (EOGlobalID)ids.objectAtIndex(i),eContext ) );
        return objects;
    }
    
    /** invalide les objects passes en parametres dans list */
    public static void invalidateObjects(EOEditingContext ec, NSArray list) {
        NSMutableArray listGIDs = new NSMutableArray();
        for(int i = 0 ; i < list.count() ; i++) {
            listGIDs.addObject(ec.globalIDForObject((EOEnterpriseObject)list.objectAtIndex(i)));
        }
        ec.invalidateObjectsWithGlobalIDs(listGIDs);
    }

    /** invalide l'object record passe en parametre */
    public static void invalidateObject(EOEditingContext ec, EOGenericRecord record) {
        NSArray listGIDs = new NSArray( new Object[]{ ec.globalIDForObject(record) } );
        ec.invalidateObjectsWithGlobalIDs(listGIDs);
    }

    
    
    
    
    /**
        EOEditingContext context, 
        String tableName,
        String key,
        Object value
    */
    public static EOGenericRecord fetchUniqueData(EOEditingContext context, String tableName,String key,Object value) {
        EOQualifier qualifie = EOQualifier.qualifierWithQualifierFormat( key+"=%@", new NSArray(value) );
        NSArray objets = fetchData(context,tableName,qualifie);
        if(objets.count()>0)
            return (EOGenericRecord)objets.objectAtIndex(0);
        else
            return null;
    }
    
    /** EOEditingContext eContext,
         String tableName,
         EOQualifier leQualifier */
    public static EOGenericRecord fetchUniqueData(EOEditingContext eContext,String tableName,EOQualifier qualifier) {
        NSArray objets = fetchData(eContext,tableName,qualifier);
        if(objets.count()>0)
            return (EOGenericRecord)objets.objectAtIndex(0);
        else
            return null;
    }

    /** EOEditingContext context 
         String tableName
         String key
         Object value */
    public static NSArray fetchData(EOEditingContext context, String tableName,String key,Object value) {
        EOQualifier qualifie = EOQualifier.qualifierWithQualifierFormat( key+"=%@", new NSArray(value) );
        return fetchData(context,tableName,qualifie);
    }

    /** faire un fetch avec un qualifier */
    public static NSArray fetchData(EOEditingContext context, String leNomTable, EOQualifier leQualifier) {
        EOFetchSpecification myFetch = new EOFetchSpecification(leNomTable,leQualifier,null);
        myFetch.setUsesDistinct(true);
        return context.objectsWithFetchSpecification(myFetch);
    }
    
    /** faire un fetch avec un qualifier et un SortOrdering */
    public static NSArray fetchData(EOEditingContext context, String leNomTable, EOQualifier leQualifier,EOSortOrdering sort) {
        if(sort==null)
            return fetchData(context,leNomTable,leQualifier,(NSArray)null);
        else 
            return fetchData(context,leNomTable,leQualifier,new NSArray(sort));
    }
    
    /** faire un fetch avec un qualifier et un tableau NSArray de SortOrdering */
    public static NSArray fetchData(EOEditingContext context, String leNomTable, EOQualifier leQualifier,NSArray sorts) {
        EOFetchSpecification myFetch;
        myFetch = new EOFetchSpecification(leNomTable,leQualifier,sorts);
        myFetch.setUsesDistinct(true);
        return context.objectsWithFetchSpecification(myFetch);
    }



    /** faire un fetch avec un qualifier et un tableau NSArray de SortOrdering en limitant le nombre d'objets a limit*/
    public static NSArray fetchLimitedData(
                                           EOEditingContext context, 
                                           String entity, 
                                           EOQualifier qualifier,
                                           EOSortOrdering sort,
                                           int limit
                                           ) {
        EOFetchSpecification myFetch;
        myFetch = new EOFetchSpecification(entity,qualifier,new NSArray(sort));
        myFetch.setUsesDistinct(true);
        myFetch.setFetchLimit(limit);
        return context.objectsWithFetchSpecification(myFetch);
    }

    /** purge le contenu du displayGroup passe en parametre */
    public static void flushDisplayGroup(EODisplayGroup disp) {
        disp.dataSource().editingContext().revert();
        disp.setObjectArray(new NSArray());
    }
    
    /** enleve tous les autres pointeurs d'un objet */
    public static NSMutableArray retirerMultiples(NSArray array) {
        NSMutableArray resultat = new NSMutableArray();
        for(int i=0;i<array.count();i++) {
            Object obj = array.objectAtIndex(i);
            if(!resultat.containsObject(obj))
                resultat.addObject(obj);	
        }
        return resultat;
    }
       
    /** construit un qualifier a partir des dates dans le tableau (debut, fin) */
    public static EOQualifier qualifierFromPeriodicites(NSArray periodicites) {
        NSMutableArray qDates = new NSMutableArray();
        for(int i=0;i<periodicites.count();i+=2) {
            NSTimestamp debut = (NSTimestamp)periodicites.objectAtIndex(i);
            NSTimestamp fin   = (NSTimestamp)periodicites.objectAtIndex(i+1);
            qDates.addObject(
            EOQualifier.qualifierWithQualifierFormat("dateFin>%@ and dateDeb<%@",new NSArray(new Object[]{debut,fin}))
                             );
        }
        EOQualifier qualifierDates = new EOOrQualifier(qDates);
        return qualifierDates;
    }
       
    

}
