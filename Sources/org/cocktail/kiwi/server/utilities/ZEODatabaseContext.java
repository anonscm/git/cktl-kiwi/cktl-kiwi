package org.cocktail.kiwi.server.utilities;
import com.webobjects.eoaccess.EODatabase;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EORelationship;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFaulting;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;

/**
 * Classe qui permet de corriger un bug d'EOF concernant le refreshing des relationships.
 * (fournie sur dev-apple par Pierre Bernard) //merci Jeff
 * Utiliser la methode 
 * <code>EODatabaseContext.setContextClassToRegister(ZEODatabaseContext.class);  //merci Jeff</code>
 * dans application() (avant la connexion a la base de donnees depuis le mod?le). A priori, une fois que le mod?le s'est connecte, il est trop tard pour redefinir le databaseContext (merci Arunas).<br>
 * Il faut egalement utiliser <code>setPrefetchingRelationshipKeyPaths</code> sur la fecthSpecification pour que les modifs soient bien recuperees.
 */
public class ZEODatabaseContext extends EODatabaseContext {
	
	/**
	 * @param arg0
	 */
	public ZEODatabaseContext(EODatabase arg0) {
		super(arg0);
	}
	
	/** Internal method that handles prefetching of to-many relationships.<BR>
	 *
	 * // TBD This is a workaround to what looks like a bug in WO 5.1 & WO 5.2. Remove as soon as it's no longer needed
	 *
	 * The problem is that even refreshing fetches don't refresh the to-many relationships they prefetch.
	 **/
	public void _followToManyRelationshipWithFetchSpecification(
			EORelationship relationship,
			EOFetchSpecification fetchspecification,
			NSArray objects,
			EOEditingContext editingcontext) {
		int count = objects.count();
		for (int i = 0; i < count; i++){
			EOEnterpriseObject object = (EOEnterpriseObject) objects.objectAtIndex(i);
			EOGlobalID sourceGlobalID = editingcontext.globalIDForObject(object);
			String relationshipName = relationship.name();
			if (!object.isFault()) {
				EOFaulting toManyArray = (EOFaulting) object.storedValueForKey(relationshipName);
				if (!toManyArray.isFault()) {
					EOFaulting tmpToManyArray =
						(EOFaulting) arrayFaultWithSourceGlobalID(sourceGlobalID,
								relationshipName,
								editingcontext);
					// Turn the existing array back into a fault by assigning it the fault handler of the newly created fault
					toManyArray.turnIntoFault(tmpToManyArray.faultHandler());
				}
			}
		}
		super._followToManyRelationshipWithFetchSpecification(
				relationship,
				fetchspecification,
				objects,
				editingcontext);
	}
}
