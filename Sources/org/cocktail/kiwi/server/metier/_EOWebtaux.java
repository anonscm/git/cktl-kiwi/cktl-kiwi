// _EOWebtaux.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOWebtaux.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOWebtaux extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Webtaux";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.WEB_TAUX";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "wtaOrdre";

	public static final String WMO_CODE_KEY = "wmoCode";
	public static final String WTA_COURANTE_KEY = "wtaCourante";
	public static final String WTA_DATE_KEY = "wtaDate";
	public static final String WTA_TAUX_KEY = "wtaTaux";

//Colonnes dans la base de donnees
	public static final String WMO_CODE_COLKEY = "WMO_CODE";
	public static final String WTA_COURANTE_COLKEY = "WTA_COURANTE";
	public static final String WTA_DATE_COLKEY = "WTA_DATE";
	public static final String WTA_TAUX_COLKEY = "WTA_TAUX";

// Relationships
	public static final String WEBMON_KEY = "webmon";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String wmoCode() {
    return (String) storedValueForKey(WMO_CODE_KEY);
  }

  public void setWmoCode(String value) {
    takeStoredValueForKey(value, WMO_CODE_KEY);
  }

  public Double wtaCourante() {
    return (Double) storedValueForKey(WTA_COURANTE_KEY);
  }

  public void setWtaCourante(Double value) {
    takeStoredValueForKey(value, WTA_COURANTE_KEY);
  }

  public NSTimestamp wtaDate() {
    return (NSTimestamp) storedValueForKey(WTA_DATE_KEY);
  }

  public void setWtaDate(NSTimestamp value) {
    takeStoredValueForKey(value, WTA_DATE_KEY);
  }

  public java.math.BigDecimal wtaTaux() {
    return (java.math.BigDecimal) storedValueForKey(WTA_TAUX_KEY);
  }

  public void setWtaTaux(java.math.BigDecimal value) {
    takeStoredValueForKey(value, WTA_TAUX_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOWebmon webmon() {
    return (org.cocktail.kiwi.server.metier.EOWebmon)storedValueForKey(WEBMON_KEY);
  }

  public void setWebmonRelationship(org.cocktail.kiwi.server.metier.EOWebmon value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOWebmon oldValue = webmon();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBMON_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBMON_KEY);
    }
  }
  

/**
 * Créer une instance de EOWebtaux avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOWebtaux createEOWebtaux(EOEditingContext editingContext, String wmoCode
, NSTimestamp wtaDate
, java.math.BigDecimal wtaTaux
			) {
    EOWebtaux eo = (EOWebtaux) createAndInsertInstance(editingContext, _EOWebtaux.ENTITY_NAME);    
		eo.setWmoCode(wmoCode);
		eo.setWtaDate(wtaDate);
		eo.setWtaTaux(wtaTaux);
    return eo;
  }

  
	  public EOWebtaux localInstanceIn(EOEditingContext editingContext) {
	  		return (EOWebtaux)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOWebtaux creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOWebtaux creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOWebtaux object = (EOWebtaux)createAndInsertInstance(editingContext, _EOWebtaux.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOWebtaux localInstanceIn(EOEditingContext editingContext, EOWebtaux eo) {
    EOWebtaux localInstance = (eo == null) ? null : (EOWebtaux)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOWebtaux#localInstanceIn a la place.
   */
	public static EOWebtaux localInstanceOf(EOEditingContext editingContext, EOWebtaux eo) {
		return EOWebtaux.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOWebtaux fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOWebtaux fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOWebtaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOWebtaux)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOWebtaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOWebtaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOWebtaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOWebtaux)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOWebtaux fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOWebtaux eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOWebtaux ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOWebtaux fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
