
// _EOMissionAvDep.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOMissionAvDep.java instead.

package org.cocktail.kiwi.server.metier;

import org.cocktail.fwkcktlwebapp.common.database.CktlRecord;


public abstract class _EOMissionAvDep extends CktlRecord
{

    public static final String ENTITY_NAME = "MissionAvDep";

    public static final String ENTITY_TABLE_NAME = "MISSION_AV_DEP";

    public static final String DEP_ID_KEY = "depId";
    public static final String MAD_ID_KEY = "madId";
    public static final String MIS_ORDRE_KEY = "misOrdre";
    public static final String VAM_ID_KEY = "vamId";

    public static final String DEP_ID_COLKEY = "DEP_ID";
    public static final String MAD_ID_COLKEY = "MAD_ID";
    public static final String MIS_ORDRE_COLKEY = "MIS_ORDRE";
    public static final String VAM_ID_COLKEY = "VAM_ID";

    public static final String DEPENSE_KEY = "depense";
    public static final String MISSION_KEY = "mission";
    public static final String VISA_AV_MISSION_KEY = "visaAvMission";




    

    public _EOMissionAvDep() {
        super();
    }






    public org.cocktail.kiwi.server.metier.EODepense depense() {
        return (org.cocktail.kiwi.server.metier.EODepense)storedValueForKey(DEPENSE_KEY);
    }
    public void setDepense(org.cocktail.kiwi.server.metier.EODepense aValue) {
        takeStoredValueForKey(aValue, DEPENSE_KEY);
    }
    public void setDepenseRelationship(org.cocktail.kiwi.server.metier.EODepense value) {
        if (value == null) {
            org.cocktail.kiwi.server.metier.EODepense object = depense();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_KEY);
        }
    }

    public org.cocktail.kiwi.server.metier.EOMission mission() {
        return (org.cocktail.kiwi.server.metier.EOMission)storedValueForKey(MISSION_KEY);
    }
    public void setMission(org.cocktail.kiwi.server.metier.EOMission aValue) {
        takeStoredValueForKey(aValue, MISSION_KEY);
    }
    public void setMissionRelationship(org.cocktail.kiwi.server.metier.EOMission value) {
        if (value == null) {
            org.cocktail.kiwi.server.metier.EOMission object = mission();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, MISSION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, MISSION_KEY);
        }
    }

    public org.cocktail.kiwi.server.metier.EOVisaAvMission visaAvMission() {
        return (org.cocktail.kiwi.server.metier.EOVisaAvMission)storedValueForKey(VISA_AV_MISSION_KEY);
    }
    public void setVisaAvMission(org.cocktail.kiwi.server.metier.EOVisaAvMission aValue) {
        takeStoredValueForKey(aValue, VISA_AV_MISSION_KEY);
    }
    public void setVisaAvMissionRelationship(org.cocktail.kiwi.server.metier.EOVisaAvMission value) {
        if (value == null) {
            org.cocktail.kiwi.server.metier.EOVisaAvMission object = visaAvMission();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, VISA_AV_MISSION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, VISA_AV_MISSION_KEY);
        }
    }





}

