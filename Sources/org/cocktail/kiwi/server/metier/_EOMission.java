// _EOMission.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMission.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOMission extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Mission";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.MISSION";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "misOrdre";

	public static final String DEN_QUOTIENT_REMB_KEY = "denQuotientRemb";
	public static final String MIS_CORPS_KEY = "misCorps";
	public static final String MIS_CREATION_KEY = "misCreation";
	public static final String MIS_DEBUT_KEY = "misDebut";
	public static final String MIS_ETAT_KEY = "misEtat";
	public static final String MIS_FIN_KEY = "misFin";
	public static final String MIS_MOTIF_KEY = "misMotif";
	public static final String MIS_NUMERO_KEY = "misNumero";
	public static final String MIS_OBSERVATION_KEY = "misObservation";
	public static final String MIS_PAYEUR_KEY = "misPayeur";
	public static final String MIS_RESIDENCE_KEY = "misResidence";
	public static final String NUM_QUOTIENT_REMB_KEY = "numQuotientRemb";

//Colonnes dans la base de donnees
	public static final String DEN_QUOTIENT_REMB_COLKEY = "DEN_QUOTIENT_REMB";
	public static final String MIS_CORPS_COLKEY = "MIS_CORPS";
	public static final String MIS_CREATION_COLKEY = "MIS_CREATION";
	public static final String MIS_DEBUT_COLKEY = "MIS_DEBUT";
	public static final String MIS_ETAT_COLKEY = "MIS_ETAT";
	public static final String MIS_FIN_COLKEY = "MIS_FIN";
	public static final String MIS_MOTIF_COLKEY = "MIS_MOTIF";
	public static final String MIS_NUMERO_COLKEY = "MIS_NUMERO";
	public static final String MIS_OBSERVATION_COLKEY = "MIS_OBSERVATION";
	public static final String MIS_PAYEUR_COLKEY = "MIS_PAYEUR";
	public static final String MIS_RESIDENCE_COLKEY = "MIS_RESIDENCE";
	public static final String NUM_QUOTIENT_REMB_COLKEY = "NUM_QUOTIENT_REMB";

// Relationships
	public static final String CORPS_KEY = "corps";
	public static final String FOURNIS_KEY = "fournis";
	public static final String GROUPE_MISSION_KEY = "groupeMission";
	public static final String MISSION_AVANCES_KEY = "missionAvances";
	public static final String MISSION_PAIEMENTS_KEY = "missionPaiements";
	public static final String PAYEUR_KEY = "payeur";
	public static final String SEGMENTS_INFOS_KEY = "segmentsInfos";
	public static final String SEGMENTS_TARIF_KEY = "segmentsTarif";
	public static final String TITRE_MISSION_KEY = "titreMission";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String UTILISATEUR_CREATION_KEY = "utilisateurCreation";
	public static final String UTILISATEUR_MODIFICATION_KEY = "utilisateurModification";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer denQuotientRemb() {
    return (Integer) storedValueForKey(DEN_QUOTIENT_REMB_KEY);
  }

  public void setDenQuotientRemb(Integer value) {
    takeStoredValueForKey(value, DEN_QUOTIENT_REMB_KEY);
  }

  public String misCorps() {
    return (String) storedValueForKey(MIS_CORPS_KEY);
  }

  public void setMisCorps(String value) {
    takeStoredValueForKey(value, MIS_CORPS_KEY);
  }

  public NSTimestamp misCreation() {
    return (NSTimestamp) storedValueForKey(MIS_CREATION_KEY);
  }

  public void setMisCreation(NSTimestamp value) {
    takeStoredValueForKey(value, MIS_CREATION_KEY);
  }

  public NSTimestamp misDebut() {
    return (NSTimestamp) storedValueForKey(MIS_DEBUT_KEY);
  }

  public void setMisDebut(NSTimestamp value) {
    takeStoredValueForKey(value, MIS_DEBUT_KEY);
  }

  public String misEtat() {
    return (String) storedValueForKey(MIS_ETAT_KEY);
  }

  public void setMisEtat(String value) {
    takeStoredValueForKey(value, MIS_ETAT_KEY);
  }

  public NSTimestamp misFin() {
    return (NSTimestamp) storedValueForKey(MIS_FIN_KEY);
  }

  public void setMisFin(NSTimestamp value) {
    takeStoredValueForKey(value, MIS_FIN_KEY);
  }

  public String misMotif() {
    return (String) storedValueForKey(MIS_MOTIF_KEY);
  }

  public void setMisMotif(String value) {
    takeStoredValueForKey(value, MIS_MOTIF_KEY);
  }

  public Integer misNumero() {
    return (Integer) storedValueForKey(MIS_NUMERO_KEY);
  }

  public void setMisNumero(Integer value) {
    takeStoredValueForKey(value, MIS_NUMERO_KEY);
  }

  public String misObservation() {
    return (String) storedValueForKey(MIS_OBSERVATION_KEY);
  }

  public void setMisObservation(String value) {
    takeStoredValueForKey(value, MIS_OBSERVATION_KEY);
  }

  public String misPayeur() {
    return (String) storedValueForKey(MIS_PAYEUR_KEY);
  }

  public void setMisPayeur(String value) {
    takeStoredValueForKey(value, MIS_PAYEUR_KEY);
  }

  public String misResidence() {
    return (String) storedValueForKey(MIS_RESIDENCE_KEY);
  }

  public void setMisResidence(String value) {
    takeStoredValueForKey(value, MIS_RESIDENCE_KEY);
  }

  public Integer numQuotientRemb() {
    return (Integer) storedValueForKey(NUM_QUOTIENT_REMB_KEY);
  }

  public void setNumQuotientRemb(Integer value) {
    takeStoredValueForKey(value, NUM_QUOTIENT_REMB_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOCorps corps() {
    return (org.cocktail.kiwi.server.metier.EOCorps)storedValueForKey(CORPS_KEY);
  }

  public void setCorpsRelationship(org.cocktail.kiwi.server.metier.EOCorps value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOCorps oldValue = corps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CORPS_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOFournis fournis() {
    return (org.cocktail.kiwi.server.metier.EOFournis)storedValueForKey(FOURNIS_KEY);
  }

  public void setFournisRelationship(org.cocktail.kiwi.server.metier.EOFournis value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOGroupesMission groupeMission() {
    return (org.cocktail.kiwi.server.metier.EOGroupesMission)storedValueForKey(GROUPE_MISSION_KEY);
  }

  public void setGroupeMissionRelationship(org.cocktail.kiwi.server.metier.EOGroupesMission value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOGroupesMission oldValue = groupeMission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GROUPE_MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GROUPE_MISSION_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOPayeur payeur() {
    return (org.cocktail.kiwi.server.metier.EOPayeur)storedValueForKey(PAYEUR_KEY);
  }

  public void setPayeurRelationship(org.cocktail.kiwi.server.metier.EOPayeur value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOPayeur oldValue = payeur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAYEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAYEUR_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOTitreMission titreMission() {
    return (org.cocktail.kiwi.server.metier.EOTitreMission)storedValueForKey(TITRE_MISSION_KEY);
  }

  public void setTitreMissionRelationship(org.cocktail.kiwi.server.metier.EOTitreMission value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOTitreMission oldValue = titreMission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_MISSION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice toExercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateurCreation() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_CREATION_KEY);
  }

  public void setUtilisateurCreationRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateurCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_CREATION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateurModification() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_MODIFICATION_KEY);
  }

  public void setUtilisateurModificationRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateurModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_MODIFICATION_KEY);
    }
  }
  
  public NSArray missionAvances() {
    return (NSArray)storedValueForKey(MISSION_AVANCES_KEY);
  }

  public NSArray missionAvances(EOQualifier qualifier) {
    return missionAvances(qualifier, null, false);
  }

  public NSArray missionAvances(EOQualifier qualifier, boolean fetch) {
    return missionAvances(qualifier, null, fetch);
  }

  public NSArray missionAvances(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EOMissionAvance.MISSION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EOMissionAvance.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = missionAvances();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMissionAvancesRelationship(org.cocktail.kiwi.server.metier.EOMissionAvance object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MISSION_AVANCES_KEY);
  }

  public void removeFromMissionAvancesRelationship(org.cocktail.kiwi.server.metier.EOMissionAvance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MISSION_AVANCES_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOMissionAvance createMissionAvancesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MissionAvance");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MISSION_AVANCES_KEY);
    return (org.cocktail.kiwi.server.metier.EOMissionAvance) eo;
  }

  public void deleteMissionAvancesRelationship(org.cocktail.kiwi.server.metier.EOMissionAvance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MISSION_AVANCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMissionAvancesRelationships() {
    Enumeration objects = missionAvances().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMissionAvancesRelationship((org.cocktail.kiwi.server.metier.EOMissionAvance)objects.nextElement());
    }
  }

  public NSArray missionPaiements() {
    return (NSArray)storedValueForKey(MISSION_PAIEMENTS_KEY);
  }

  public NSArray missionPaiements(EOQualifier qualifier) {
    return missionPaiements(qualifier, null, false);
  }

  public NSArray missionPaiements(EOQualifier qualifier, boolean fetch) {
    return missionPaiements(qualifier, null, fetch);
  }

  public NSArray missionPaiements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EOMissionPaiement.MISSION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EOMissionPaiement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = missionPaiements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMissionPaiementsRelationship(org.cocktail.kiwi.server.metier.EOMissionPaiement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MISSION_PAIEMENTS_KEY);
  }

  public void removeFromMissionPaiementsRelationship(org.cocktail.kiwi.server.metier.EOMissionPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MISSION_PAIEMENTS_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOMissionPaiement createMissionPaiementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MissionPaiement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MISSION_PAIEMENTS_KEY);
    return (org.cocktail.kiwi.server.metier.EOMissionPaiement) eo;
  }

  public void deleteMissionPaiementsRelationship(org.cocktail.kiwi.server.metier.EOMissionPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MISSION_PAIEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMissionPaiementsRelationships() {
    Enumeration objects = missionPaiements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMissionPaiementsRelationship((org.cocktail.kiwi.server.metier.EOMissionPaiement)objects.nextElement());
    }
  }

  public NSArray segmentsInfos() {
    return (NSArray)storedValueForKey(SEGMENTS_INFOS_KEY);
  }

  public NSArray segmentsInfos(EOQualifier qualifier) {
    return segmentsInfos(qualifier, null, false);
  }

  public NSArray segmentsInfos(EOQualifier qualifier, boolean fetch) {
    return segmentsInfos(qualifier, null, fetch);
  }

  public NSArray segmentsInfos(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EOSegmentInfos.MISSION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EOSegmentInfos.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = segmentsInfos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSegmentsInfosRelationship(org.cocktail.kiwi.server.metier.EOSegmentInfos object) {
    addObjectToBothSidesOfRelationshipWithKey(object, SEGMENTS_INFOS_KEY);
  }

  public void removeFromSegmentsInfosRelationship(org.cocktail.kiwi.server.metier.EOSegmentInfos object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SEGMENTS_INFOS_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOSegmentInfos createSegmentsInfosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("SegmentInfos");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, SEGMENTS_INFOS_KEY);
    return (org.cocktail.kiwi.server.metier.EOSegmentInfos) eo;
  }

  public void deleteSegmentsInfosRelationship(org.cocktail.kiwi.server.metier.EOSegmentInfos object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SEGMENTS_INFOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSegmentsInfosRelationships() {
    Enumeration objects = segmentsInfos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSegmentsInfosRelationship((org.cocktail.kiwi.server.metier.EOSegmentInfos)objects.nextElement());
    }
  }

  public NSArray segmentsTarif() {
    return (NSArray)storedValueForKey(SEGMENTS_TARIF_KEY);
  }

  public NSArray segmentsTarif(EOQualifier qualifier) {
    return segmentsTarif(qualifier, null, false);
  }

  public NSArray segmentsTarif(EOQualifier qualifier, boolean fetch) {
    return segmentsTarif(qualifier, null, fetch);
  }

  public NSArray segmentsTarif(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EOSegmentTarif.MISSION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EOSegmentTarif.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = segmentsTarif();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSegmentsTarifRelationship(org.cocktail.kiwi.server.metier.EOSegmentTarif object) {
    addObjectToBothSidesOfRelationshipWithKey(object, SEGMENTS_TARIF_KEY);
  }

  public void removeFromSegmentsTarifRelationship(org.cocktail.kiwi.server.metier.EOSegmentTarif object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SEGMENTS_TARIF_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOSegmentTarif createSegmentsTarifRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("SegmentTarif");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, SEGMENTS_TARIF_KEY);
    return (org.cocktail.kiwi.server.metier.EOSegmentTarif) eo;
  }

  public void deleteSegmentsTarifRelationship(org.cocktail.kiwi.server.metier.EOSegmentTarif object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SEGMENTS_TARIF_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSegmentsTarifRelationships() {
    Enumeration objects = segmentsTarif().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSegmentsTarifRelationship((org.cocktail.kiwi.server.metier.EOSegmentTarif)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOMission avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMission createEOMission(EOEditingContext editingContext, Integer denQuotientRemb
, NSTimestamp misCreation
, String misMotif
, Integer misNumero
, String misPayeur
, Integer numQuotientRemb
, org.cocktail.kiwi.server.metier.EOFournis fournis, org.cocktail.kiwi.server.metier.EOTitreMission titreMission, org.cocktail.application.serveur.eof.EOExercice toExercice, org.cocktail.application.serveur.eof.EOUtilisateur utilisateurCreation, org.cocktail.application.serveur.eof.EOUtilisateur utilisateurModification			) {
    EOMission eo = (EOMission) createAndInsertInstance(editingContext, _EOMission.ENTITY_NAME);    
		eo.setDenQuotientRemb(denQuotientRemb);
		eo.setMisCreation(misCreation);
		eo.setMisMotif(misMotif);
		eo.setMisNumero(misNumero);
		eo.setMisPayeur(misPayeur);
		eo.setNumQuotientRemb(numQuotientRemb);
    eo.setFournisRelationship(fournis);
    eo.setTitreMissionRelationship(titreMission);
    eo.setToExerciceRelationship(toExercice);
    eo.setUtilisateurCreationRelationship(utilisateurCreation);
    eo.setUtilisateurModificationRelationship(utilisateurModification);
    return eo;
  }

  
	  public EOMission localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMission)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMission creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMission creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOMission object = (EOMission)createAndInsertInstance(editingContext, _EOMission.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMission localInstanceIn(EOEditingContext editingContext, EOMission eo) {
    EOMission localInstance = (eo == null) ? null : (EOMission)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMission#localInstanceIn a la place.
   */
	public static EOMission localInstanceOf(EOEditingContext editingContext, EOMission eo) {
		return EOMission.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMission fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMission fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMission)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMission)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMission fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMission eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMission ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMission fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
