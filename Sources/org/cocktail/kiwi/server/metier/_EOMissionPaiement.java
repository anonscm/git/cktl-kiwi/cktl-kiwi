// _EOMissionPaiement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMissionPaiement.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOMissionPaiement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "MissionPaiement";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.MISSION_PAIEMENT";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "mipOrdre";

	public static final String MIP_CAP_KEY = "mipCap";
	public static final String MIP_CREATION_KEY = "mipCreation";
	public static final String MIP_ETAT_KEY = "mipEtat";
	public static final String MIP_INFOS_KEY = "mipInfos";
	public static final String MIP_MONTANT_PAIEMENT_KEY = "mipMontantPaiement";
	public static final String MIP_MONTANT_TOTAL_KEY = "mipMontantTotal";

//Colonnes dans la base de donnees
	public static final String MIP_CAP_COLKEY = "MIP_CAP";
	public static final String MIP_CREATION_COLKEY = "MIP_CREATION";
	public static final String MIP_ETAT_COLKEY = "MIP_ETAT";
	public static final String MIP_INFOS_COLKEY = "MIP_INFOS";
	public static final String MIP_MONTANT_PAIEMENT_COLKEY = "MIP_MONTANT_PAIEMENT";
	public static final String MIP_MONTANT_TOTAL_COLKEY = "MIP_MONTANT_TOTAL";

// Relationships
	public static final String MISSION_KEY = "mission";
	public static final String MISSION_PAIEMENT_ENGAGES_KEY = "missionPaiementEngages";
	public static final String RIBFOUR_KEY = "ribfour";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String mipCap() {
    return (String) storedValueForKey(MIP_CAP_KEY);
  }

  public void setMipCap(String value) {
    takeStoredValueForKey(value, MIP_CAP_KEY);
  }

  public NSTimestamp mipCreation() {
    return (NSTimestamp) storedValueForKey(MIP_CREATION_KEY);
  }

  public void setMipCreation(NSTimestamp value) {
    takeStoredValueForKey(value, MIP_CREATION_KEY);
  }

  public String mipEtat() {
    return (String) storedValueForKey(MIP_ETAT_KEY);
  }

  public void setMipEtat(String value) {
    takeStoredValueForKey(value, MIP_ETAT_KEY);
  }

  public String mipInfos() {
    return (String) storedValueForKey(MIP_INFOS_KEY);
  }

  public void setMipInfos(String value) {
    takeStoredValueForKey(value, MIP_INFOS_KEY);
  }

  public java.math.BigDecimal mipMontantPaiement() {
    return (java.math.BigDecimal) storedValueForKey(MIP_MONTANT_PAIEMENT_KEY);
  }

  public void setMipMontantPaiement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MIP_MONTANT_PAIEMENT_KEY);
  }

  public java.math.BigDecimal mipMontantTotal() {
    return (java.math.BigDecimal) storedValueForKey(MIP_MONTANT_TOTAL_KEY);
  }

  public void setMipMontantTotal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MIP_MONTANT_TOTAL_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOMission mission() {
    return (org.cocktail.kiwi.server.metier.EOMission)storedValueForKey(MISSION_KEY);
  }

  public void setMissionRelationship(org.cocktail.kiwi.server.metier.EOMission value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOMission oldValue = mission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MISSION_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EORibfour ribfour() {
    return (org.cocktail.kiwi.server.metier.EORibfour)storedValueForKey(RIBFOUR_KEY);
  }

  public void setRibfourRelationship(org.cocktail.kiwi.server.metier.EORibfour value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EORibfour oldValue = ribfour();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIBFOUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIBFOUR_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray missionPaiementEngages() {
    return (NSArray)storedValueForKey(MISSION_PAIEMENT_ENGAGES_KEY);
  }

  public NSArray missionPaiementEngages(EOQualifier qualifier) {
    return missionPaiementEngages(qualifier, null, false);
  }

  public NSArray missionPaiementEngages(EOQualifier qualifier, boolean fetch) {
    return missionPaiementEngages(qualifier, null, fetch);
  }

  public NSArray missionPaiementEngages(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EOMissionPaiementEngage.MISSION_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EOMissionPaiementEngage.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = missionPaiementEngages();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMissionPaiementEngagesRelationship(org.cocktail.kiwi.server.metier.EOMissionPaiementEngage object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MISSION_PAIEMENT_ENGAGES_KEY);
  }

  public void removeFromMissionPaiementEngagesRelationship(org.cocktail.kiwi.server.metier.EOMissionPaiementEngage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MISSION_PAIEMENT_ENGAGES_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOMissionPaiementEngage createMissionPaiementEngagesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MissionPaiementEngage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MISSION_PAIEMENT_ENGAGES_KEY);
    return (org.cocktail.kiwi.server.metier.EOMissionPaiementEngage) eo;
  }

  public void deleteMissionPaiementEngagesRelationship(org.cocktail.kiwi.server.metier.EOMissionPaiementEngage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MISSION_PAIEMENT_ENGAGES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMissionPaiementEngagesRelationships() {
    Enumeration objects = missionPaiementEngages().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMissionPaiementEngagesRelationship((org.cocktail.kiwi.server.metier.EOMissionPaiementEngage)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOMissionPaiement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMissionPaiement createEOMissionPaiement(EOEditingContext editingContext, String mipCap
, String mipEtat
, java.math.BigDecimal mipMontantPaiement
, java.math.BigDecimal mipMontantTotal
, org.cocktail.kiwi.server.metier.EOMission mission			) {
    EOMissionPaiement eo = (EOMissionPaiement) createAndInsertInstance(editingContext, _EOMissionPaiement.ENTITY_NAME);    
		eo.setMipCap(mipCap);
		eo.setMipEtat(mipEtat);
		eo.setMipMontantPaiement(mipMontantPaiement);
		eo.setMipMontantTotal(mipMontantTotal);
    eo.setMissionRelationship(mission);
    return eo;
  }

  
	  public EOMissionPaiement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMissionPaiement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMissionPaiement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMissionPaiement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOMissionPaiement object = (EOMissionPaiement)createAndInsertInstance(editingContext, _EOMissionPaiement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMissionPaiement localInstanceIn(EOEditingContext editingContext, EOMissionPaiement eo) {
    EOMissionPaiement localInstance = (eo == null) ? null : (EOMissionPaiement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMissionPaiement#localInstanceIn a la place.
   */
	public static EOMissionPaiement localInstanceOf(EOEditingContext editingContext, EOMissionPaiement eo) {
		return EOMissionPaiement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMissionPaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMissionPaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMissionPaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMissionPaiement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMissionPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMissionPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMissionPaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMissionPaiement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMissionPaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMissionPaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMissionPaiement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMissionPaiement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
