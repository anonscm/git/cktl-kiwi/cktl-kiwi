

// EOIndemnite.java
// 

package org.cocktail.kiwi.server.metier;


import java.math.BigDecimal;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOIndemnite extends _EOIndemnite
{
    public EOIndemnite() {
        super();
    }

	public static NSArray findIndemnitesForSegment(EOEditingContext ec, EOSegmentTarif segment)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEGMENT_TARIF_KEY + " = %@", new NSArray(segment)));
				
		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null, true);
		
	}
	
	public BigDecimal tarifIndemnite()	{
		
		BigDecimal tarif = new BigDecimal("0");
		EOMission mission = segmentTarif().mission();
		BigDecimal taux = segmentTarif().webtaux().wtaTaux();
		
//		switch (new Integer(mission.groupesMission().grmCode()).intValue())	{
//		case 1 : 
		tarif = webmiss().wmiGroupe1();
//		break;
//		case 2 : tarif = webmiss().wmiGroupe2();break;
//		case 3 : tarif = webmiss().wmiGroupe3();break;
//		}
		
		tarif = tarif.multiply(taux);
		
		// On applique le quoitient de remboursement de la mission (1/1 ou 5/3)
		tarif = tarif.multiply(new BigDecimal(mission.numQuotientRemb().intValue()));
		tarif = tarif.divide(new BigDecimal(mission.denQuotientRemb().intValue()), 6, BigDecimal.ROUND_HALF_UP);
		
		return tarif;		
	}

    
/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
