// _EOTitreMission.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitreMission.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOTitreMission extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TitreMission";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.TITRE_MISSION";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "titOrdre";

	public static final String TIT_CTRL_CONGE_KEY = "titCtrlConge";
	public static final String TIT_CTRL_DATES_KEY = "titCtrlDates";
	public static final String TIT_CTRL_PLANNING_KEY = "titCtrlPlanning";
	public static final String TIT_ETAT_KEY = "titEtat";
	public static final String TIT_LIBELLE_KEY = "titLibelle";
	public static final String TIT_PAIEMENT_KEY = "titPaiement";
	public static final String TIT_PERMANENT_KEY = "titPermanent";
	public static final String TIT_REMARQUES_KEY = "titRemarques";
	public static final String TIT_SAISIE_LBUD_KEY = "titSaisieLbud";

//Colonnes dans la base de donnees
	public static final String TIT_CTRL_CONGE_COLKEY = "tit_Ctrl_Conges";
	public static final String TIT_CTRL_DATES_COLKEY = "tit_ctrl_dates";
	public static final String TIT_CTRL_PLANNING_COLKEY = "tit_ctrl_planning";
	public static final String TIT_ETAT_COLKEY = "tit_ETAT";
	public static final String TIT_LIBELLE_COLKEY = "TIT_LIBELLE";
	public static final String TIT_PAIEMENT_COLKEY = "tit_Paiement";
	public static final String TIT_PERMANENT_COLKEY = "tit_Permanent";
	public static final String TIT_REMARQUES_COLKEY = "TIT_REMARQUES";
	public static final String TIT_SAISIE_LBUD_COLKEY = "tit_saisie_lbud";

// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer titCtrlConge() {
    return (Integer) storedValueForKey(TIT_CTRL_CONGE_KEY);
  }

  public void setTitCtrlConge(Integer value) {
    takeStoredValueForKey(value, TIT_CTRL_CONGE_KEY);
  }

  public Integer titCtrlDates() {
    return (Integer) storedValueForKey(TIT_CTRL_DATES_KEY);
  }

  public void setTitCtrlDates(Integer value) {
    takeStoredValueForKey(value, TIT_CTRL_DATES_KEY);
  }

  public Integer titCtrlPlanning() {
    return (Integer) storedValueForKey(TIT_CTRL_PLANNING_KEY);
  }

  public void setTitCtrlPlanning(Integer value) {
    takeStoredValueForKey(value, TIT_CTRL_PLANNING_KEY);
  }

  public String titEtat() {
    return (String) storedValueForKey(TIT_ETAT_KEY);
  }

  public void setTitEtat(String value) {
    takeStoredValueForKey(value, TIT_ETAT_KEY);
  }

  public String titLibelle() {
    return (String) storedValueForKey(TIT_LIBELLE_KEY);
  }

  public void setTitLibelle(String value) {
    takeStoredValueForKey(value, TIT_LIBELLE_KEY);
  }

  public Integer titPaiement() {
    return (Integer) storedValueForKey(TIT_PAIEMENT_KEY);
  }

  public void setTitPaiement(Integer value) {
    takeStoredValueForKey(value, TIT_PAIEMENT_KEY);
  }

  public Integer titPermanent() {
    return (Integer) storedValueForKey(TIT_PERMANENT_KEY);
  }

  public void setTitPermanent(Integer value) {
    takeStoredValueForKey(value, TIT_PERMANENT_KEY);
  }

  public String titRemarques() {
    return (String) storedValueForKey(TIT_REMARQUES_KEY);
  }

  public void setTitRemarques(String value) {
    takeStoredValueForKey(value, TIT_REMARQUES_KEY);
  }

  public Integer titSaisieLbud() {
    return (Integer) storedValueForKey(TIT_SAISIE_LBUD_KEY);
  }

  public void setTitSaisieLbud(Integer value) {
    takeStoredValueForKey(value, TIT_SAISIE_LBUD_KEY);
  }


/**
 * Créer une instance de EOTitreMission avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTitreMission createEOTitreMission(EOEditingContext editingContext, Integer titCtrlConge
, Integer titCtrlPlanning
, String titEtat
, String titLibelle
, Integer titPaiement
, Integer titPermanent
, Integer titSaisieLbud
			) {
    EOTitreMission eo = (EOTitreMission) createAndInsertInstance(editingContext, _EOTitreMission.ENTITY_NAME);    
		eo.setTitCtrlConge(titCtrlConge);
		eo.setTitCtrlPlanning(titCtrlPlanning);
		eo.setTitEtat(titEtat);
		eo.setTitLibelle(titLibelle);
		eo.setTitPaiement(titPaiement);
		eo.setTitPermanent(titPermanent);
		eo.setTitSaisieLbud(titSaisieLbud);
    return eo;
  }

  
	  public EOTitreMission localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTitreMission)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitreMission creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitreMission creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTitreMission object = (EOTitreMission)createAndInsertInstance(editingContext, _EOTitreMission.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTitreMission localInstanceIn(EOEditingContext editingContext, EOTitreMission eo) {
    EOTitreMission localInstance = (eo == null) ? null : (EOTitreMission)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTitreMission#localInstanceIn a la place.
   */
	public static EOTitreMission localInstanceOf(EOEditingContext editingContext, EOTitreMission eo) {
		return EOTitreMission.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTitreMission fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTitreMission fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitreMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitreMission)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitreMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitreMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitreMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitreMission)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTitreMission fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitreMission eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitreMission ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitreMission fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
