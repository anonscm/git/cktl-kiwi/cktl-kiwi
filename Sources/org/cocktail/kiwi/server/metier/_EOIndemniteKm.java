// _EOIndemniteKm.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndemniteKm.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOIndemniteKm extends  EOGenericRecord {
	public static final String ENTITY_NAME = "IndemniteKm";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.INDEMNITE_KM";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "ikmOrdre";

	public static final String IKM_DATE_KEY = "ikmDate";
	public static final String IKM_DATE_FIN_KEY = "ikmDateFin";
	public static final String IKM_PF_DEBUT_KEY = "ikmPfDebut";
	public static final String IKM_PF_FIN_KEY = "ikmPfFin";
	public static final String IKM_TARIF1_KEY = "ikmTarif1";
	public static final String IKM_TARIF2_KEY = "ikmTarif2";
	public static final String IKM_TARIF3_KEY = "ikmTarif3";
	public static final String REM_TAUX_KEY = "remTaux";

//Colonnes dans la base de donnees
	public static final String IKM_DATE_COLKEY = "IKM_DATE";
	public static final String IKM_DATE_FIN_COLKEY = "IKM_DATE_FIN";
	public static final String IKM_PF_DEBUT_COLKEY = "IKM_PF_DEBUT";
	public static final String IKM_PF_FIN_COLKEY = "IKM_PF_FIN";
	public static final String IKM_TARIF1_COLKEY = "IKM_TARIF1";
	public static final String IKM_TARIF2_COLKEY = "IKM_TARIF2";
	public static final String IKM_TARIF3_COLKEY = "IKM_TARIF3";
	public static final String REM_TAUX_COLKEY = "REM_TAUX";

// Relationships
	public static final String TO_TYPE_TRANSPORT_KEY = "toTypeTransport";
	public static final String TO_ZONE_KEY = "toZone";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp ikmDate() {
    return (NSTimestamp) storedValueForKey(IKM_DATE_KEY);
  }

  public void setIkmDate(NSTimestamp value) {
    takeStoredValueForKey(value, IKM_DATE_KEY);
  }

  public NSTimestamp ikmDateFin() {
    return (NSTimestamp) storedValueForKey(IKM_DATE_FIN_KEY);
  }

  public void setIkmDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, IKM_DATE_FIN_KEY);
  }

  public Double ikmPfDebut() {
    return (Double) storedValueForKey(IKM_PF_DEBUT_KEY);
  }

  public void setIkmPfDebut(Double value) {
    takeStoredValueForKey(value, IKM_PF_DEBUT_KEY);
  }

  public Double ikmPfFin() {
    return (Double) storedValueForKey(IKM_PF_FIN_KEY);
  }

  public void setIkmPfFin(Double value) {
    takeStoredValueForKey(value, IKM_PF_FIN_KEY);
  }

  public java.math.BigDecimal ikmTarif1() {
    return (java.math.BigDecimal) storedValueForKey(IKM_TARIF1_KEY);
  }

  public void setIkmTarif1(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IKM_TARIF1_KEY);
  }

  public java.math.BigDecimal ikmTarif2() {
    return (java.math.BigDecimal) storedValueForKey(IKM_TARIF2_KEY);
  }

  public void setIkmTarif2(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IKM_TARIF2_KEY);
  }

  public java.math.BigDecimal ikmTarif3() {
    return (java.math.BigDecimal) storedValueForKey(IKM_TARIF3_KEY);
  }

  public void setIkmTarif3(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IKM_TARIF3_KEY);
  }

  public String remTaux() {
    return (String) storedValueForKey(REM_TAUX_KEY);
  }

  public void setRemTaux(String value) {
    takeStoredValueForKey(value, REM_TAUX_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOTypeTransport toTypeTransport() {
    return (org.cocktail.kiwi.server.metier.EOTypeTransport)storedValueForKey(TO_TYPE_TRANSPORT_KEY);
  }

  public void setToTypeTransportRelationship(org.cocktail.kiwi.server.metier.EOTypeTransport value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOTypeTransport oldValue = toTypeTransport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_TRANSPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_TRANSPORT_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EORembZone toZone() {
    return (org.cocktail.kiwi.server.metier.EORembZone)storedValueForKey(TO_ZONE_KEY);
  }

  public void setToZoneRelationship(org.cocktail.kiwi.server.metier.EORembZone value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EORembZone oldValue = toZone();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ZONE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ZONE_KEY);
    }
  }
  

/**
 * Créer une instance de EOIndemniteKm avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOIndemniteKm createEOIndemniteKm(EOEditingContext editingContext, NSTimestamp ikmDate
, NSTimestamp ikmDateFin
, Double ikmPfDebut
, Double ikmPfFin
, java.math.BigDecimal ikmTarif1
, java.math.BigDecimal ikmTarif2
, String remTaux
			) {
    EOIndemniteKm eo = (EOIndemniteKm) createAndInsertInstance(editingContext, _EOIndemniteKm.ENTITY_NAME);    
		eo.setIkmDate(ikmDate);
		eo.setIkmDateFin(ikmDateFin);
		eo.setIkmPfDebut(ikmPfDebut);
		eo.setIkmPfFin(ikmPfFin);
		eo.setIkmTarif1(ikmTarif1);
		eo.setIkmTarif2(ikmTarif2);
		eo.setRemTaux(remTaux);
    return eo;
  }

  
	  public EOIndemniteKm localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndemniteKm)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndemniteKm creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndemniteKm creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOIndemniteKm object = (EOIndemniteKm)createAndInsertInstance(editingContext, _EOIndemniteKm.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOIndemniteKm localInstanceIn(EOEditingContext editingContext, EOIndemniteKm eo) {
    EOIndemniteKm localInstance = (eo == null) ? null : (EOIndemniteKm)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOIndemniteKm#localInstanceIn a la place.
   */
	public static EOIndemniteKm localInstanceOf(EOEditingContext editingContext, EOIndemniteKm eo) {
		return EOIndemniteKm.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOIndemniteKm fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndemniteKm fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndemniteKm eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndemniteKm)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndemniteKm fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndemniteKm fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndemniteKm eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndemniteKm)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOIndemniteKm fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndemniteKm eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndemniteKm ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndemniteKm fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
