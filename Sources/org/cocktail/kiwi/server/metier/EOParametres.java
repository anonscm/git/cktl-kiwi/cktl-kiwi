

// EOParametres.java
// 

package org.cocktail.kiwi.server.metier;


import org.cocktail.application.serveur.eof.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOParametres extends _EOParametres
{
    public EOParametres() {
        super();
    }

	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
*/
	public static String getValue(EOEditingContext ec, String key, EOExercice exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOParametres.PARAM_KEY_KEY + " = %@", new NSArray(key)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOParametres.TO_EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOParametres.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		NSArray params = ec.objectsWithFetchSpecification(fs);
		fs.setRefreshesRefetchedObjects(true);
		try {return ((EOParametres)params.objectAtIndex(0)).paramValue();}
		catch (Exception e) {e.printStackTrace();return null;}
	}
	
/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
