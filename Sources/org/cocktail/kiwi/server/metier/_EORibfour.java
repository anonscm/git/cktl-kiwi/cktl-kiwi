// _EORibfour.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORibfour.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EORibfour extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Ribfour";
	public static final String ENTITY_TABLE_NAME = "GRHUM.RIBFOUR_ULR";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "ribOrdre";

	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String IBAN_KEY = "iban";
	public static final String MOD_CODE_KEY = "modCode";
	public static final String RIB_CLE_KEY = "ribCle";
	public static final String RIB_ETAB_KEY = "ribEtab";
	public static final String RIB_GUICH_KEY = "ribGuich";
	public static final String RIB_NUM_KEY = "ribNum";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String RIB_TITCO_KEY = "ribTitco";
	public static final String RIB_VALIDE_KEY = "ribValide";

//Colonnes dans la base de donnees
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String IBAN_COLKEY = "IBAN";
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String RIB_CLE_COLKEY = "CLE_RIB";
	public static final String RIB_ETAB_COLKEY = "C_BANQUE";
	public static final String RIB_GUICH_COLKEY = "C_GUICHET";
	public static final String RIB_NUM_COLKEY = "NO_COMPTE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String RIB_TITCO_COLKEY = "RIB_TITCO";
	public static final String RIB_VALIDE_COLKEY = "RIB_VALIDE";

// Relationships
	public static final String BANQUE_KEY = "banque";
	public static final String FOURNIS_KEY = "fournis";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public String iban() {
    return (String) storedValueForKey(IBAN_KEY);
  }

  public void setIban(String value) {
    takeStoredValueForKey(value, IBAN_KEY);
  }

  public String modCode() {
    return (String) storedValueForKey(MOD_CODE_KEY);
  }

  public void setModCode(String value) {
    takeStoredValueForKey(value, MOD_CODE_KEY);
  }

  public String ribCle() {
    return (String) storedValueForKey(RIB_CLE_KEY);
  }

  public void setRibCle(String value) {
    takeStoredValueForKey(value, RIB_CLE_KEY);
  }

  public String ribEtab() {
    return (String) storedValueForKey(RIB_ETAB_KEY);
  }

  public void setRibEtab(String value) {
    takeStoredValueForKey(value, RIB_ETAB_KEY);
  }

  public String ribGuich() {
    return (String) storedValueForKey(RIB_GUICH_KEY);
  }

  public void setRibGuich(String value) {
    takeStoredValueForKey(value, RIB_GUICH_KEY);
  }

  public String ribNum() {
    return (String) storedValueForKey(RIB_NUM_KEY);
  }

  public void setRibNum(String value) {
    takeStoredValueForKey(value, RIB_NUM_KEY);
  }

  public Integer ribOrdre() {
    return (Integer) storedValueForKey(RIB_ORDRE_KEY);
  }

  public void setRibOrdre(Integer value) {
    takeStoredValueForKey(value, RIB_ORDRE_KEY);
  }

  public String ribTitco() {
    return (String) storedValueForKey(RIB_TITCO_KEY);
  }

  public void setRibTitco(String value) {
    takeStoredValueForKey(value, RIB_TITCO_KEY);
  }

  public String ribValide() {
    return (String) storedValueForKey(RIB_VALIDE_KEY);
  }

  public void setRibValide(String value) {
    takeStoredValueForKey(value, RIB_VALIDE_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOBanque banque() {
    return (org.cocktail.kiwi.server.metier.EOBanque)storedValueForKey(BANQUE_KEY);
  }

  public void setBanqueRelationship(org.cocktail.kiwi.server.metier.EOBanque value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOBanque oldValue = banque();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BANQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BANQUE_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOFournis fournis() {
    return (org.cocktail.kiwi.server.metier.EOFournis)storedValueForKey(FOURNIS_KEY);
  }

  public void setFournisRelationship(org.cocktail.kiwi.server.metier.EOFournis value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_KEY);
    }
  }
  

/**
 * Créer une instance de EORibfour avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORibfour createEORibfour(EOEditingContext editingContext, Integer fouOrdre
, String iban
, String modCode
, String ribCle
, String ribEtab
, String ribGuich
, String ribNum
, Integer ribOrdre
, String ribTitco
, org.cocktail.kiwi.server.metier.EOFournis fournis			) {
    EORibfour eo = (EORibfour) createAndInsertInstance(editingContext, _EORibfour.ENTITY_NAME);    
		eo.setFouOrdre(fouOrdre);
		eo.setIban(iban);
		eo.setModCode(modCode);
		eo.setRibCle(ribCle);
		eo.setRibEtab(ribEtab);
		eo.setRibGuich(ribGuich);
		eo.setRibNum(ribNum);
		eo.setRibOrdre(ribOrdre);
		eo.setRibTitco(ribTitco);
    eo.setFournisRelationship(fournis);
    return eo;
  }

  
	  public EORibfour localInstanceIn(EOEditingContext editingContext) {
	  		return (EORibfour)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORibfour creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORibfour creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORibfour object = (EORibfour)createAndInsertInstance(editingContext, _EORibfour.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORibfour localInstanceIn(EOEditingContext editingContext, EORibfour eo) {
    EORibfour localInstance = (eo == null) ? null : (EORibfour)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORibfour#localInstanceIn a la place.
   */
	public static EORibfour localInstanceOf(EOEditingContext editingContext, EORibfour eo) {
		return EORibfour.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORibfour fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORibfour fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORibfour eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORibfour)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORibfour fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORibfour fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORibfour eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORibfour)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORibfour fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORibfour eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORibfour ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORibfour fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
