
// _EOVTrancheGpConvention.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOVTrancheGpConvention.java instead.

package org.cocktail.kiwi.server.metier;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.fwkcktlwebapp.common.database.CktlRecord;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOVTrancheGpConvention extends CktlRecord
{

    public static final String ENTITY_NAME = "VTrancheGpConvention";

    public static final String ENTITY_TABLE_NAME = "CONVENTION.V_TRANCHE_GP_CONVENTION";

    public static final String CONV_INDEX_KEY = "convIndex";
    public static final String CONV_ORDRE_KEY = "convOrdre";
    public static final String CONV_PREMIER_EXERCICE_KEY = "convPremierExercice";
    public static final String CONV_REFERENCE_EXTERNE_KEY = "convReferenceExterne";
    public static final String EXE_ORDRE_KEY = "exeOrdre";
    public static final String GROUPE_KEY = "groupe";
    public static final String LIBELLE_KEY = "libelle";
    public static final String LIBELLE_COURT_KEY = "libelleCourt";
    public static final String VALIDE_KEY = "valide";

    public static final String CONV_INDEX_COLKEY = "CONV_INDEX";
    public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
    public static final String CONV_PREMIER_EXERCICE_COLKEY = "CONV_PREMIER_EXERCICE";
    public static final String CONV_REFERENCE_EXTERNE_COLKEY = "CONV_REFERENCE_EXTERNE";
    public static final String EXE_ORDRE_COLKEY = "EXERCICE";
    public static final String GROUPE_COLKEY = "GROUPE";
    public static final String LIBELLE_COLKEY = "LIBELLE";
    public static final String LIBELLE_COURT_COLKEY = "LIBELLE_COURT";
    public static final String VALIDE_COLKEY = "VALIDE";

    public static final String EXERCICE_KEY = "exercice";

    public static final String V_LB_CONVENTION_KEY = "vLbConvention";



    

    public _EOVTrancheGpConvention() {
        super();
    }




    public Number convIndex() {
        return (Number)storedValueForKey(CONV_INDEX_KEY);
    }
    public void setConvIndex(Number aValue) {
        takeStoredValueForKey(aValue, CONV_INDEX_KEY);
    }

    public Number convPremierExercice() {
        return (Number)storedValueForKey(CONV_PREMIER_EXERCICE_KEY);
    }
    public void setConvPremierExercice(Number aValue) {
        takeStoredValueForKey(aValue, CONV_PREMIER_EXERCICE_KEY);
    }

    public String convReferenceExterne() {
        return (String)storedValueForKey(CONV_REFERENCE_EXTERNE_KEY);
    }
    public void setConvReferenceExterne(String aValue) {
        takeStoredValueForKey(aValue, CONV_REFERENCE_EXTERNE_KEY);
    }

    public String groupe() {
        return (String)storedValueForKey(GROUPE_KEY);
    }
    public void setGroupe(String aValue) {
        takeStoredValueForKey(aValue, GROUPE_KEY);
    }

    public String libelle() {
        return (String)storedValueForKey(LIBELLE_KEY);
    }
    public void setLibelle(String aValue) {
        takeStoredValueForKey(aValue, LIBELLE_KEY);
    }

    public String libelleCourt() {
        return (String)storedValueForKey(LIBELLE_COURT_KEY);
    }
    public void setLibelleCourt(String aValue) {
        takeStoredValueForKey(aValue, LIBELLE_COURT_KEY);
    }

    public String valide() {
        return (String)storedValueForKey(VALIDE_KEY);
    }
    public void setValide(String aValue) {
        takeStoredValueForKey(aValue, VALIDE_KEY);
    }



    public EOExercice exercice() {
        return (EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
    public void setExerciceRelationship(EOExercice value) {
        if (value == null) {
            EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }




    public NSArray vLbConvention() {
        return (NSArray)storedValueForKey(V_LB_CONVENTION_KEY);
    }
    public void setVLbConvention(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, V_LB_CONVENTION_KEY);
    }
    public void addToVLbConvention(org.cocktail.kiwi.server.metier.EOVLbConvention object) {
        NSMutableArray array = (NSMutableArray)vLbConvention();
        willChange();
        array.addObject(object);
    }
    public void removeFromVLbConvention(org.cocktail.kiwi.server.metier.EOVLbConvention object) {
        NSMutableArray array = (NSMutableArray)vLbConvention();
        willChange();
        array.removeObject(object);
    }
    public void addToVLbConventionRelationship(org.cocktail.kiwi.server.metier.EOVLbConvention object) {
        addObjectToBothSidesOfRelationshipWithKey(object, V_LB_CONVENTION_KEY);
    }
    public void removeFromVLbConventionRelationship(org.cocktail.kiwi.server.metier.EOVLbConvention object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, V_LB_CONVENTION_KEY);
    }
    
    
    


}

