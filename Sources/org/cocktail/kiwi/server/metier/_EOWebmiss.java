// _EOWebmiss.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOWebmiss.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOWebmiss extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Webmiss";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.WEB_MISS";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "wmiOrdre";

	public static final String WMI_DEBUT_KEY = "wmiDebut";
	public static final String WMI_FIN_KEY = "wmiFin";
	public static final String WMI_GROUPE1_KEY = "wmiGroupe1";
	public static final String WMI_GROUPE2_KEY = "wmiGroupe2";
	public static final String WMI_GROUPE3_KEY = "wmiGroupe3";
	public static final String WMI_GROUPE4_KEY = "wmiGroupe4";
	public static final String WMI_GROUPE5_KEY = "wmiGroupe5";
	public static final String WMI_TYPE_KEY = "wmiType";
	public static final String WMO_CODE_KEY = "wmoCode";
	public static final String WPA_CODE_KEY = "wpaCode";

//Colonnes dans la base de donnees
	public static final String WMI_DEBUT_COLKEY = "WMI_DEBUT";
	public static final String WMI_FIN_COLKEY = "WMI_FIN";
	public static final String WMI_GROUPE1_COLKEY = "WMI_GROUPE1";
	public static final String WMI_GROUPE2_COLKEY = "WMI_GROUPE2";
	public static final String WMI_GROUPE3_COLKEY = "WMI_GROUPE3";
	public static final String WMI_GROUPE4_COLKEY = "WMI_GROUPE4";
	public static final String WMI_GROUPE5_COLKEY = "WMI_GROUPE5";
	public static final String WMI_TYPE_COLKEY = "WMI_TYPE";
	public static final String WMO_CODE_COLKEY = "WMO_CODE";
	public static final String WPA_CODE_COLKEY = "WPA_CODE";

// Relationships
	public static final String WEBMON_KEY = "webmon";
	public static final String WEBPAYS_KEY = "webpays";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp wmiDebut() {
    return (NSTimestamp) storedValueForKey(WMI_DEBUT_KEY);
  }

  public void setWmiDebut(NSTimestamp value) {
    takeStoredValueForKey(value, WMI_DEBUT_KEY);
  }

  public NSTimestamp wmiFin() {
    return (NSTimestamp) storedValueForKey(WMI_FIN_KEY);
  }

  public void setWmiFin(NSTimestamp value) {
    takeStoredValueForKey(value, WMI_FIN_KEY);
  }

  public java.math.BigDecimal wmiGroupe1() {
    return (java.math.BigDecimal) storedValueForKey(WMI_GROUPE1_KEY);
  }

  public void setWmiGroupe1(java.math.BigDecimal value) {
    takeStoredValueForKey(value, WMI_GROUPE1_KEY);
  }

  public java.math.BigDecimal wmiGroupe2() {
    return (java.math.BigDecimal) storedValueForKey(WMI_GROUPE2_KEY);
  }

  public void setWmiGroupe2(java.math.BigDecimal value) {
    takeStoredValueForKey(value, WMI_GROUPE2_KEY);
  }

  public java.math.BigDecimal wmiGroupe3() {
    return (java.math.BigDecimal) storedValueForKey(WMI_GROUPE3_KEY);
  }

  public void setWmiGroupe3(java.math.BigDecimal value) {
    takeStoredValueForKey(value, WMI_GROUPE3_KEY);
  }

  public java.math.BigDecimal wmiGroupe4() {
    return (java.math.BigDecimal) storedValueForKey(WMI_GROUPE4_KEY);
  }

  public void setWmiGroupe4(java.math.BigDecimal value) {
    takeStoredValueForKey(value, WMI_GROUPE4_KEY);
  }

  public java.math.BigDecimal wmiGroupe5() {
    return (java.math.BigDecimal) storedValueForKey(WMI_GROUPE5_KEY);
  }

  public void setWmiGroupe5(java.math.BigDecimal value) {
    takeStoredValueForKey(value, WMI_GROUPE5_KEY);
  }

  public String wmiType() {
    return (String) storedValueForKey(WMI_TYPE_KEY);
  }

  public void setWmiType(String value) {
    takeStoredValueForKey(value, WMI_TYPE_KEY);
  }

  public String wmoCode() {
    return (String) storedValueForKey(WMO_CODE_KEY);
  }

  public void setWmoCode(String value) {
    takeStoredValueForKey(value, WMO_CODE_KEY);
  }

  public String wpaCode() {
    return (String) storedValueForKey(WPA_CODE_KEY);
  }

  public void setWpaCode(String value) {
    takeStoredValueForKey(value, WPA_CODE_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOWebmon webmon() {
    return (org.cocktail.kiwi.server.metier.EOWebmon)storedValueForKey(WEBMON_KEY);
  }

  public void setWebmonRelationship(org.cocktail.kiwi.server.metier.EOWebmon value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOWebmon oldValue = webmon();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBMON_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBMON_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOWebpays webpays() {
    return (org.cocktail.kiwi.server.metier.EOWebpays)storedValueForKey(WEBPAYS_KEY);
  }

  public void setWebpaysRelationship(org.cocktail.kiwi.server.metier.EOWebpays value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOWebpays oldValue = webpays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBPAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBPAYS_KEY);
    }
  }
  

/**
 * Créer une instance de EOWebmiss avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOWebmiss createEOWebmiss(EOEditingContext editingContext, NSTimestamp wmiDebut
, java.math.BigDecimal wmiGroupe1
, java.math.BigDecimal wmiGroupe2
, java.math.BigDecimal wmiGroupe3
, java.math.BigDecimal wmiGroupe4
, java.math.BigDecimal wmiGroupe5
, String wmiType
, String wmoCode
, String wpaCode
			) {
    EOWebmiss eo = (EOWebmiss) createAndInsertInstance(editingContext, _EOWebmiss.ENTITY_NAME);    
		eo.setWmiDebut(wmiDebut);
		eo.setWmiGroupe1(wmiGroupe1);
		eo.setWmiGroupe2(wmiGroupe2);
		eo.setWmiGroupe3(wmiGroupe3);
		eo.setWmiGroupe4(wmiGroupe4);
		eo.setWmiGroupe5(wmiGroupe5);
		eo.setWmiType(wmiType);
		eo.setWmoCode(wmoCode);
		eo.setWpaCode(wpaCode);
    return eo;
  }

  
	  public EOWebmiss localInstanceIn(EOEditingContext editingContext) {
	  		return (EOWebmiss)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOWebmiss creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOWebmiss creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOWebmiss object = (EOWebmiss)createAndInsertInstance(editingContext, _EOWebmiss.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOWebmiss localInstanceIn(EOEditingContext editingContext, EOWebmiss eo) {
    EOWebmiss localInstance = (eo == null) ? null : (EOWebmiss)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOWebmiss#localInstanceIn a la place.
   */
	public static EOWebmiss localInstanceOf(EOEditingContext editingContext, EOWebmiss eo) {
		return EOWebmiss.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOWebmiss fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOWebmiss fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOWebmiss eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOWebmiss)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOWebmiss fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOWebmiss fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOWebmiss eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOWebmiss)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOWebmiss fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOWebmiss eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOWebmiss ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOWebmiss fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
