// _EOSegmentInfos.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSegmentInfos.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOSegmentInfos extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SegmentInfos";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.SEGMENT_INFOS";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "seiOrdre";

	public static final String SEI_DIVERS_KEY = "seiDivers";
	public static final String SEI_ETAT_KEY = "seiEtat";
	public static final String SEI_JEFYCO_KEY = "seiJefyco";
	public static final String SEI_LIBELLE_KEY = "seiLibelle";
	public static final String SEI_MONTANT_PAIEMENT_KEY = "seiMontantPaiement";
	public static final String SEI_MONTANT_SAISIE_KEY = "seiMontantSaisie";

//Colonnes dans la base de donnees
	public static final String SEI_DIVERS_COLKEY = "SEI_DIVERS";
	public static final String SEI_ETAT_COLKEY = "SEI_ETAT";
	public static final String SEI_JEFYCO_COLKEY = "SEI_JEFYCO";
	public static final String SEI_LIBELLE_COLKEY = "SEI_LIBELLE";
	public static final String SEI_MONTANT_PAIEMENT_COLKEY = "SEI_MONTANT_PAIEMENT";
	public static final String SEI_MONTANT_SAISIE_COLKEY = "SEI_MONTANT_SAISIE";

// Relationships
	public static final String COMMANDE_KEY = "commande";
	public static final String MISSION_KEY = "mission";
	public static final String SEG_TYPE_INFO_KEY = "segTypeInfo";
	public static final String WEBTAUX_KEY = "webtaux";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String seiDivers() {
    return (String) storedValueForKey(SEI_DIVERS_KEY);
  }

  public void setSeiDivers(String value) {
    takeStoredValueForKey(value, SEI_DIVERS_KEY);
  }

  public String seiEtat() {
    return (String) storedValueForKey(SEI_ETAT_KEY);
  }

  public void setSeiEtat(String value) {
    takeStoredValueForKey(value, SEI_ETAT_KEY);
  }

  public String seiJefyco() {
    return (String) storedValueForKey(SEI_JEFYCO_KEY);
  }

  public void setSeiJefyco(String value) {
    takeStoredValueForKey(value, SEI_JEFYCO_KEY);
  }

  public String seiLibelle() {
    return (String) storedValueForKey(SEI_LIBELLE_KEY);
  }

  public void setSeiLibelle(String value) {
    takeStoredValueForKey(value, SEI_LIBELLE_KEY);
  }

  public java.math.BigDecimal seiMontantPaiement() {
    return (java.math.BigDecimal) storedValueForKey(SEI_MONTANT_PAIEMENT_KEY);
  }

  public void setSeiMontantPaiement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, SEI_MONTANT_PAIEMENT_KEY);
  }

  public java.math.BigDecimal seiMontantSaisie() {
    return (java.math.BigDecimal) storedValueForKey(SEI_MONTANT_SAISIE_KEY);
  }

  public void setSeiMontantSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, SEI_MONTANT_SAISIE_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOCommande commande() {
    return (org.cocktail.kiwi.server.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
  }

  public void setCommandeRelationship(org.cocktail.kiwi.server.metier.EOCommande value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOCommande oldValue = commande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOMission mission() {
    return (org.cocktail.kiwi.server.metier.EOMission)storedValueForKey(MISSION_KEY);
  }

  public void setMissionRelationship(org.cocktail.kiwi.server.metier.EOMission value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOMission oldValue = mission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MISSION_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOSegTypeInfo segTypeInfo() {
    return (org.cocktail.kiwi.server.metier.EOSegTypeInfo)storedValueForKey(SEG_TYPE_INFO_KEY);
  }

  public void setSegTypeInfoRelationship(org.cocktail.kiwi.server.metier.EOSegTypeInfo value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOSegTypeInfo oldValue = segTypeInfo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SEG_TYPE_INFO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SEG_TYPE_INFO_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOWebtaux webtaux() {
    return (org.cocktail.kiwi.server.metier.EOWebtaux)storedValueForKey(WEBTAUX_KEY);
  }

  public void setWebtauxRelationship(org.cocktail.kiwi.server.metier.EOWebtaux value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOWebtaux oldValue = webtaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBTAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBTAUX_KEY);
    }
  }
  

/**
 * Créer une instance de EOSegmentInfos avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSegmentInfos createEOSegmentInfos(EOEditingContext editingContext, org.cocktail.kiwi.server.metier.EOMission mission, org.cocktail.kiwi.server.metier.EOSegTypeInfo segTypeInfo			) {
    EOSegmentInfos eo = (EOSegmentInfos) createAndInsertInstance(editingContext, _EOSegmentInfos.ENTITY_NAME);    
    eo.setMissionRelationship(mission);
    eo.setSegTypeInfoRelationship(segTypeInfo);
    return eo;
  }

  
	  public EOSegmentInfos localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSegmentInfos)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSegmentInfos creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSegmentInfos creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOSegmentInfos object = (EOSegmentInfos)createAndInsertInstance(editingContext, _EOSegmentInfos.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSegmentInfos localInstanceIn(EOEditingContext editingContext, EOSegmentInfos eo) {
    EOSegmentInfos localInstance = (eo == null) ? null : (EOSegmentInfos)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSegmentInfos#localInstanceIn a la place.
   */
	public static EOSegmentInfos localInstanceOf(EOEditingContext editingContext, EOSegmentInfos eo) {
		return EOSegmentInfos.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSegmentInfos fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSegmentInfos fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSegmentInfos eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSegmentInfos)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSegmentInfos fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSegmentInfos fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSegmentInfos eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSegmentInfos)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSegmentInfos fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSegmentInfos eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSegmentInfos ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSegmentInfos fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
