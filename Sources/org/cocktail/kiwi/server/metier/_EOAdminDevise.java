
// _EOAdminDevise.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOAdminDevise.java instead.

package org.cocktail.kiwi.server.metier;

import org.cocktail.fwkcktlwebapp.common.database.CktlRecord;


public abstract class _EOAdminDevise extends CktlRecord
{

    public static final String ENTITY_NAME = "AdminDevise";

    public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.DEVISE";

    public static final String DEV_CODE_KEY = "devCode";
    public static final String DEV_ID_KEY = "devId";
    public static final String DEV_LIBELLE_KEY = "devLibelle";
    public static final String DEV_NB_DECIMALES_KEY = "devNbDecimales";

    public static final String DEV_CODE_COLKEY = "DEV_CODE";
    public static final String DEV_ID_COLKEY = "DEV_ID";
    public static final String DEV_LIBELLE_COLKEY = "DEV_LIBELLE";
    public static final String DEV_NB_DECIMALES_COLKEY = "DEV_NB_DECIMALES";





    

    public _EOAdminDevise() {
        super();
    }




    public String devCode() {
        return (String)storedValueForKey(DEV_CODE_KEY);
    }
    public void setDevCode(String aValue) {
        takeStoredValueForKey(aValue, DEV_CODE_KEY);
    }

    public String devLibelle() {
        return (String)storedValueForKey(DEV_LIBELLE_KEY);
    }
    public void setDevLibelle(String aValue) {
        takeStoredValueForKey(aValue, DEV_LIBELLE_KEY);
    }

    public Number devNbDecimales() {
        return (Number)storedValueForKey(DEV_NB_DECIMALES_KEY);
    }
    public void setDevNbDecimales(Number aValue) {
        takeStoredValueForKey(aValue, DEV_NB_DECIMALES_KEY);
    }







}

