// _EOSegmentTarif.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSegmentTarif.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOSegmentTarif extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SegmentTarif";
	public static final String ENTITY_TABLE_NAME = "JEFY_MISSION.SEGMENT_TARIF";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "segOrdre";

	public static final String REM_TAUX_KEY = "remTaux";
	public static final String SEG_DEBUT_KEY = "segDebut";
	public static final String SEG_ETAT_KEY = "segEtat";
	public static final String SEG_FIN_KEY = "segFin";
	public static final String SEG_LIBELLE_KEY = "segLibelle";
	public static final String SEG_LIEU_DEPART_KEY = "segLieuDepart";
	public static final String SEG_LIEU_DESTINATION_KEY = "segLieuDestination";
	public static final String SEG_PAYEUR_KEY = "segPayeur";
	public static final String WPA_CODE_KEY = "wpaCode";

//Colonnes dans la base de donnees
	public static final String REM_TAUX_COLKEY = "REM_TAUX";
	public static final String SEG_DEBUT_COLKEY = "SEG_DEBUT";
	public static final String SEG_ETAT_COLKEY = "SEG_ETAT";
	public static final String SEG_FIN_COLKEY = "SEG_FIN";
	public static final String SEG_LIBELLE_COLKEY = "SEG_LIBELLE";
	public static final String SEG_LIEU_DEPART_COLKEY = "SEG_LIEU_DEPART";
	public static final String SEG_LIEU_DESTINATION_COLKEY = "SEG_LIEU_DESTINATION";
	public static final String SEG_PAYEUR_COLKEY = "SEG_PAYEUR";
	public static final String WPA_CODE_COLKEY = "WPA_CODE";

// Relationships
	public static final String INDEMNITES_KEY = "indemnites";
	public static final String MISSION_KEY = "mission";
	public static final String NUITS_KEY = "nuits";
	public static final String REMB_ZONE_KEY = "rembZone";
	public static final String REPAS_KEY = "repas";
	public static final String TRANSPORTS_KEY = "transports";
	public static final String WEBPAYS_KEY = "webpays";
	public static final String WEBTAUX_KEY = "webtaux";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String remTaux() {
    return (String) storedValueForKey(REM_TAUX_KEY);
  }

  public void setRemTaux(String value) {
    takeStoredValueForKey(value, REM_TAUX_KEY);
  }

  public NSTimestamp segDebut() {
    return (NSTimestamp) storedValueForKey(SEG_DEBUT_KEY);
  }

  public void setSegDebut(NSTimestamp value) {
    takeStoredValueForKey(value, SEG_DEBUT_KEY);
  }

  public String segEtat() {
    return (String) storedValueForKey(SEG_ETAT_KEY);
  }

  public void setSegEtat(String value) {
    takeStoredValueForKey(value, SEG_ETAT_KEY);
  }

  public NSTimestamp segFin() {
    return (NSTimestamp) storedValueForKey(SEG_FIN_KEY);
  }

  public void setSegFin(NSTimestamp value) {
    takeStoredValueForKey(value, SEG_FIN_KEY);
  }

  public String segLibelle() {
    return (String) storedValueForKey(SEG_LIBELLE_KEY);
  }

  public void setSegLibelle(String value) {
    takeStoredValueForKey(value, SEG_LIBELLE_KEY);
  }

  public String segLieuDepart() {
    return (String) storedValueForKey(SEG_LIEU_DEPART_KEY);
  }

  public void setSegLieuDepart(String value) {
    takeStoredValueForKey(value, SEG_LIEU_DEPART_KEY);
  }

  public String segLieuDestination() {
    return (String) storedValueForKey(SEG_LIEU_DESTINATION_KEY);
  }

  public void setSegLieuDestination(String value) {
    takeStoredValueForKey(value, SEG_LIEU_DESTINATION_KEY);
  }

  public String segPayeur() {
    return (String) storedValueForKey(SEG_PAYEUR_KEY);
  }

  public void setSegPayeur(String value) {
    takeStoredValueForKey(value, SEG_PAYEUR_KEY);
  }

  public String wpaCode() {
    return (String) storedValueForKey(WPA_CODE_KEY);
  }

  public void setWpaCode(String value) {
    takeStoredValueForKey(value, WPA_CODE_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOMission mission() {
    return (org.cocktail.kiwi.server.metier.EOMission)storedValueForKey(MISSION_KEY);
  }

  public void setMissionRelationship(org.cocktail.kiwi.server.metier.EOMission value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOMission oldValue = mission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MISSION_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EORembZone rembZone() {
    return (org.cocktail.kiwi.server.metier.EORembZone)storedValueForKey(REMB_ZONE_KEY);
  }

  public void setRembZoneRelationship(org.cocktail.kiwi.server.metier.EORembZone value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EORembZone oldValue = rembZone();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REMB_ZONE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REMB_ZONE_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOWebpays webpays() {
    return (org.cocktail.kiwi.server.metier.EOWebpays)storedValueForKey(WEBPAYS_KEY);
  }

  public void setWebpaysRelationship(org.cocktail.kiwi.server.metier.EOWebpays value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOWebpays oldValue = webpays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBPAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBPAYS_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOWebtaux webtaux() {
    return (org.cocktail.kiwi.server.metier.EOWebtaux)storedValueForKey(WEBTAUX_KEY);
  }

  public void setWebtauxRelationship(org.cocktail.kiwi.server.metier.EOWebtaux value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOWebtaux oldValue = webtaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, WEBTAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, WEBTAUX_KEY);
    }
  }
  
  public NSArray indemnites() {
    return (NSArray)storedValueForKey(INDEMNITES_KEY);
  }

  public NSArray indemnites(EOQualifier qualifier) {
    return indemnites(qualifier, null, false);
  }

  public NSArray indemnites(EOQualifier qualifier, boolean fetch) {
    return indemnites(qualifier, null, fetch);
  }

  public NSArray indemnites(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EOIndemnite.SEGMENT_TARIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EOIndemnite.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = indemnites();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToIndemnitesRelationship(org.cocktail.kiwi.server.metier.EOIndemnite object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INDEMNITES_KEY);
  }

  public void removeFromIndemnitesRelationship(org.cocktail.kiwi.server.metier.EOIndemnite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INDEMNITES_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOIndemnite createIndemnitesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Indemnite");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INDEMNITES_KEY);
    return (org.cocktail.kiwi.server.metier.EOIndemnite) eo;
  }

  public void deleteIndemnitesRelationship(org.cocktail.kiwi.server.metier.EOIndemnite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INDEMNITES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllIndemnitesRelationships() {
    Enumeration objects = indemnites().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteIndemnitesRelationship((org.cocktail.kiwi.server.metier.EOIndemnite)objects.nextElement());
    }
  }

  public NSArray nuits() {
    return (NSArray)storedValueForKey(NUITS_KEY);
  }

  public NSArray nuits(EOQualifier qualifier) {
    return nuits(qualifier, null, false);
  }

  public NSArray nuits(EOQualifier qualifier, boolean fetch) {
    return nuits(qualifier, null, fetch);
  }

  public NSArray nuits(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EONuits.SEGMENT_TARIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EONuits.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = nuits();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToNuitsRelationship(org.cocktail.kiwi.server.metier.EONuits object) {
    addObjectToBothSidesOfRelationshipWithKey(object, NUITS_KEY);
  }

  public void removeFromNuitsRelationship(org.cocktail.kiwi.server.metier.EONuits object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NUITS_KEY);
  }

  public org.cocktail.kiwi.server.metier.EONuits createNuitsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Nuits");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, NUITS_KEY);
    return (org.cocktail.kiwi.server.metier.EONuits) eo;
  }

  public void deleteNuitsRelationship(org.cocktail.kiwi.server.metier.EONuits object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NUITS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllNuitsRelationships() {
    Enumeration objects = nuits().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteNuitsRelationship((org.cocktail.kiwi.server.metier.EONuits)objects.nextElement());
    }
  }

  public NSArray repas() {
    return (NSArray)storedValueForKey(REPAS_KEY);
  }

  public NSArray repas(EOQualifier qualifier) {
    return repas(qualifier, null, false);
  }

  public NSArray repas(EOQualifier qualifier, boolean fetch) {
    return repas(qualifier, null, fetch);
  }

  public NSArray repas(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EORepas.SEGMENT_TARIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EORepas.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repas();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepasRelationship(org.cocktail.kiwi.server.metier.EORepas object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPAS_KEY);
  }

  public void removeFromRepasRelationship(org.cocktail.kiwi.server.metier.EORepas object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPAS_KEY);
  }

  public org.cocktail.kiwi.server.metier.EORepas createRepasRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Repas");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPAS_KEY);
    return (org.cocktail.kiwi.server.metier.EORepas) eo;
  }

  public void deleteRepasRelationship(org.cocktail.kiwi.server.metier.EORepas object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPAS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepasRelationships() {
    Enumeration objects = repas().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepasRelationship((org.cocktail.kiwi.server.metier.EORepas)objects.nextElement());
    }
  }

  public NSArray transports() {
    return (NSArray)storedValueForKey(TRANSPORTS_KEY);
  }

  public NSArray transports(EOQualifier qualifier) {
    return transports(qualifier, null, false);
  }

  public NSArray transports(EOQualifier qualifier, boolean fetch) {
    return transports(qualifier, null, fetch);
  }

  public NSArray transports(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kiwi.server.metier.EOTransports.SEGMENT_TARIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kiwi.server.metier.EOTransports.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = transports();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTransportsRelationship(org.cocktail.kiwi.server.metier.EOTransports object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TRANSPORTS_KEY);
  }

  public void removeFromTransportsRelationship(org.cocktail.kiwi.server.metier.EOTransports object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TRANSPORTS_KEY);
  }

  public org.cocktail.kiwi.server.metier.EOTransports createTransportsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Transports");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TRANSPORTS_KEY);
    return (org.cocktail.kiwi.server.metier.EOTransports) eo;
  }

  public void deleteTransportsRelationship(org.cocktail.kiwi.server.metier.EOTransports object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TRANSPORTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTransportsRelationships() {
    Enumeration objects = transports().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTransportsRelationship((org.cocktail.kiwi.server.metier.EOTransports)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOSegmentTarif avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSegmentTarif createEOSegmentTarif(EOEditingContext editingContext, String remTaux
, NSTimestamp segDebut
, NSTimestamp segFin
, String wpaCode
			) {
    EOSegmentTarif eo = (EOSegmentTarif) createAndInsertInstance(editingContext, _EOSegmentTarif.ENTITY_NAME);    
		eo.setRemTaux(remTaux);
		eo.setSegDebut(segDebut);
		eo.setSegFin(segFin);
		eo.setWpaCode(wpaCode);
    return eo;
  }

  
	  public EOSegmentTarif localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSegmentTarif)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSegmentTarif creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSegmentTarif creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOSegmentTarif object = (EOSegmentTarif)createAndInsertInstance(editingContext, _EOSegmentTarif.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSegmentTarif localInstanceIn(EOEditingContext editingContext, EOSegmentTarif eo) {
    EOSegmentTarif localInstance = (eo == null) ? null : (EOSegmentTarif)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSegmentTarif#localInstanceIn a la place.
   */
	public static EOSegmentTarif localInstanceOf(EOEditingContext editingContext, EOSegmentTarif eo) {
		return EOSegmentTarif.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSegmentTarif fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSegmentTarif fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSegmentTarif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSegmentTarif)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSegmentTarif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSegmentTarif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSegmentTarif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSegmentTarif)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSegmentTarif fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSegmentTarif eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSegmentTarif ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSegmentTarif fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
