

// EONuits.java
// 

package org.cocktail.kiwi.server.metier;


import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

public class EONuits extends _EONuits
{
    public EONuits() {
        super();
    }
    
    
	/**
	 * 
	 *
	 */
	public String stringTarifNuit()	{
		try {
			return tarifNuit().toString() + " \u20ac / Nuit";
		}
		catch (Exception e)	{
			return null;
		}
	}

	
	/**
	 * 
	 * @return
	 */
	public BigDecimal tarifNuit()	{
		
		try {		
			EOMission mission = segmentTarif().mission();
			BigDecimal tarif = rembJournalier().remTarif();
			
			tarif = tarif.multiply(new BigDecimal(mission.numQuotientRemb().intValue()));
			tarif = tarif.divide(new BigDecimal(mission.denQuotientRemb().intValue()), 2, BigDecimal.ROUND_HALF_UP);
			
			return tarif;
		}
		catch (Exception e)	{
			return null;
		}		
	}


/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
