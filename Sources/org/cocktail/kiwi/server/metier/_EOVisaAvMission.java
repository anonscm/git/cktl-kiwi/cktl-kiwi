// _EOVisaAvMission.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVisaAvMission.java instead.
package org.cocktail.kiwi.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOVisaAvMission extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VisaAvMission";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.VISA_AV_MISSION";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "vamId";

	public static final String VAM_DATE_DEM_KEY = "vamDateDem";
	public static final String VAM_DATE_VISA_KEY = "vamDateVisa";
	public static final String VAM_MONTANT_KEY = "vamMontant";
	public static final String VAM_MOTIF_REJET_KEY = "vamMotifRejet";

//Colonnes dans la base de donnees
	public static final String VAM_DATE_DEM_COLKEY = "VAM_DATE_DEM";
	public static final String VAM_DATE_VISA_COLKEY = "VAM_DATE_VISA";
	public static final String VAM_MONTANT_COLKEY = "VAM_MONTANT";
	public static final String VAM_MOTIF_REJET_COLKEY = "VAM_MOTIF_REJET";

// Relationships
	public static final String DEMANDEUR_KEY = "demandeur";
	public static final String FOURNIS_KEY = "fournis";
	public static final String MISSION_KEY = "mission";
	public static final String MODE_PAIEMENT_AVANCE_KEY = "modePaiementAvance";
	public static final String MODE_PAIEMENT_REGUL_KEY = "modePaiementRegul";
	public static final String ORDRE_DE_PAIEMENT_KEY = "ordreDePaiement";
	public static final String ORGAN_KEY = "organ";
	public static final String RIBFOUR_KEY = "ribfour";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String VALIDEUR_KEY = "valideur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp vamDateDem() {
    return (NSTimestamp) storedValueForKey(VAM_DATE_DEM_KEY);
  }

  public void setVamDateDem(NSTimestamp value) {
    takeStoredValueForKey(value, VAM_DATE_DEM_KEY);
  }

  public NSTimestamp vamDateVisa() {
    return (NSTimestamp) storedValueForKey(VAM_DATE_VISA_KEY);
  }

  public void setVamDateVisa(NSTimestamp value) {
    takeStoredValueForKey(value, VAM_DATE_VISA_KEY);
  }

  public java.math.BigDecimal vamMontant() {
    return (java.math.BigDecimal) storedValueForKey(VAM_MONTANT_KEY);
  }

  public void setVamMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VAM_MONTANT_KEY);
  }

  public String vamMotifRejet() {
    return (String) storedValueForKey(VAM_MOTIF_REJET_KEY);
  }

  public void setVamMotifRejet(String value) {
    takeStoredValueForKey(value, VAM_MOTIF_REJET_KEY);
  }

  public org.cocktail.application.serveur.eof.EOUtilisateur demandeur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(DEMANDEUR_KEY);
  }

  public void setDemandeurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = demandeur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEMANDEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEMANDEUR_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOFournis fournis() {
    return (org.cocktail.kiwi.server.metier.EOFournis)storedValueForKey(FOURNIS_KEY);
  }

  public void setFournisRelationship(org.cocktail.kiwi.server.metier.EOFournis value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOMission mission() {
    return (org.cocktail.kiwi.server.metier.EOMission)storedValueForKey(MISSION_KEY);
  }

  public void setMissionRelationship(org.cocktail.kiwi.server.metier.EOMission value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOMission oldValue = mission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MISSION_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOModePaiement modePaiementAvance() {
    return (org.cocktail.kiwi.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_AVANCE_KEY);
  }

  public void setModePaiementAvanceRelationship(org.cocktail.kiwi.server.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOModePaiement oldValue = modePaiementAvance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_AVANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_AVANCE_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOModePaiement modePaiementRegul() {
    return (org.cocktail.kiwi.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_REGUL_KEY);
  }

  public void setModePaiementRegulRelationship(org.cocktail.kiwi.server.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOModePaiement oldValue = modePaiementRegul();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_REGUL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_REGUL_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOOrdreDePaiement ordreDePaiement() {
    return (org.cocktail.kiwi.server.metier.EOOrdreDePaiement)storedValueForKey(ORDRE_DE_PAIEMENT_KEY);
  }

  public void setOrdreDePaiementRelationship(org.cocktail.kiwi.server.metier.EOOrdreDePaiement value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOOrdreDePaiement oldValue = ordreDePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORDRE_DE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORDRE_DE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOOrgan organ() {
    return (org.cocktail.kiwi.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kiwi.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EORibfour ribfour() {
    return (org.cocktail.kiwi.server.metier.EORibfour)storedValueForKey(RIBFOUR_KEY);
  }

  public void setRibfourRelationship(org.cocktail.kiwi.server.metier.EORibfour value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EORibfour oldValue = ribfour();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIBFOUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIBFOUR_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice toExercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kiwi.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.kiwi.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.kiwi.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.kiwi.server.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur valideur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(VALIDEUR_KEY);
  }

  public void setValideurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = valideur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, VALIDEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, VALIDEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOVisaAvMission avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVisaAvMission createEOVisaAvMission(EOEditingContext editingContext, NSTimestamp vamDateDem
, java.math.BigDecimal vamMontant
, String vamMotifRejet
, org.cocktail.application.serveur.eof.EOExercice toExercice			) {
    EOVisaAvMission eo = (EOVisaAvMission) createAndInsertInstance(editingContext, _EOVisaAvMission.ENTITY_NAME);    
		eo.setVamDateDem(vamDateDem);
		eo.setVamMontant(vamMontant);
		eo.setVamMotifRejet(vamMotifRejet);
    eo.setToExerciceRelationship(toExercice);
    return eo;
  }

  
	  public EOVisaAvMission localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVisaAvMission)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVisaAvMission creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVisaAvMission creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVisaAvMission object = (EOVisaAvMission)createAndInsertInstance(editingContext, _EOVisaAvMission.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVisaAvMission localInstanceIn(EOEditingContext editingContext, EOVisaAvMission eo) {
    EOVisaAvMission localInstance = (eo == null) ? null : (EOVisaAvMission)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVisaAvMission#localInstanceIn a la place.
   */
	public static EOVisaAvMission localInstanceOf(EOEditingContext editingContext, EOVisaAvMission eo) {
		return EOVisaAvMission.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVisaAvMission fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVisaAvMission fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVisaAvMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVisaAvMission)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVisaAvMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVisaAvMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVisaAvMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVisaAvMission)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVisaAvMission fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVisaAvMission eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVisaAvMission ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVisaAvMission fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
