package org.cocktail.kiwi.server;


import org.cocktail.fwkcktlwebapp.server.CktlWebAction;
import org.cocktail.kiwi.server.finder.Finder;
import org.cocktail.kiwi.server.metier.EOFournis;
import org.cocktail.kiwi.server.metier.EOIndividu;
import org.cocktail.kiwi.server.metier.EOMission;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;
import fr.univlr.cri.planning.PartagePlanning;
import fr.univlr.cri.planning.SPOccupation;


public class DirectAction extends CktlWebAction {

	public DirectAction(WORequest aRequest) {
		super(aRequest);
	}

	public WOActionResults defaultAction() {
		return pageWithName("Main");
	}	

	/**
	 * 
	 * @return
	 */
	public WOActionResults missionsPourPeriodeAction() {

		NSDictionary dicoParams = PartagePlanning.dicoParams(request());
		Number noIndividu   = (Number) dicoParams.valueForKey("noIndividu");
		NSTimestamp debut   = (NSTimestamp) dicoParams.valueForKey("debut");
		NSTimestamp fin     = (NSTimestamp) dicoParams.valueForKey("fin");

		NSArray spOccupations = new NSArray();

		EOEditingContext ec = ERXEC.newEditingContext();

		try {

			EOIndividu individu = Finder.findIndividuForNoIndividuInContext(ec, noIndividu);

			int status = 1;
			String errMessage = "";

			EOFournis fournis = Finder.findFournisForPersId(ec , individu.persId());

			if (fournis != null) {

				NSArray lesMissions = Finder.findMissionsForFournisAndPeriode(ec, fournis, debut, fin, null);

				for (int i=0;i<lesMissions.count();i++)	{

					EOMission recMission = (EOMission) lesMissions.objectAtIndex(i);

					SPOccupation spOccupation = new SPOccupation(recMission.misDebut(), recMission.misFin(),
							"MIS", recMission.misMotif());
					spOccupations = spOccupations.arrayByAddingObject(spOccupation);
					// si yua une erreur, passer [status = 1] et mettre un message dans errMessage
				}
			}

			WOResponse resultat = new WOResponse();
			resultat = PartagePlanning.reponsePlanning(spOccupations, status, errMessage);

			return resultat;
		}
		catch (Exception e) {
			e.printStackTrace();
			return new WOResponse();

		}
	}

	@Override
	public WOActionResults loginCasFailurePage(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WOActionResults loginCasSuccessPage(String arg0, NSDictionary arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WOActionResults loginNoCasPage(NSDictionary arg0) {
		// TODO Auto-generated method stub
		return null;
	} 
}
