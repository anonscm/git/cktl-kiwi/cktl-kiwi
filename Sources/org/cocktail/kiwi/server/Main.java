package org.cocktail.kiwi.server;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eodistribution.WOJavaClientComponent;

public class Main extends WOComponent {

    public Main(WOContext context) {
        super(context);
    }

    public String javaClientLink() {
        return WOJavaClientComponent.webStartActionURL(context(), "JavaClient");
    }    
}
