//
// CRITreeNode.java
// Project CourrierJC
//
// Created by jfguilla on Tue Apr 30 2002
//
package org.cocktail.kiwi.common.utilities;

import javax.swing.tree.DefaultMutableTreeNode;

import com.webobjects.eocontrol.EOGenericRecord;

public class CRITreeNode extends DefaultMutableTreeNode {

    protected EOGenericRecord nodeRecord;
    protected String key, parentKey, fieldForDisplay;
    
    // Constructeur par defaut cree un noeud vide, principalement pour le noeud racine (non affiche)
    public CRITreeNode() {
        super();
        nodeRecord = null;
        key = null;
        parentKey = null;
        fieldForDisplay = null;
    }
    
    // constructeur avec un objet a afficher, eventuellement pour le noeud racine
    // s'il doit etre affiche
    public CRITreeNode(Object nodeValue) {
        super(nodeValue);
        nodeRecord = null;
        key = null;
        parentKey = null;
        fieldForDisplay = null;
    }
    
    // constructeur de CRITreeNode, recoit un generique record + les valeurs 
    // key, parentKey et fieldForDisplay.
    // utilise par CRITreeView
    public CRITreeNode(EOGenericRecord nodeValue, String keyValue, String parentKeyValue, String fieldForDisplayValue) {
        super(nodeValue.valueForKey(fieldForDisplayValue));
        nodeRecord = nodeValue;
        key = keyValue;
        parentKey = parentKeyValue;
        fieldForDisplay = fieldForDisplayValue;
    }
    
    // ajoute un noeud fils au noeud appele
    public void add(CRITreeNode newChild) {
        super.add(newChild);
    }
    // definit le record associe a un noeud
    public void setRecord(EOGenericRecord nodeValue) {
        nodeRecord = nodeValue;
    }
    // retourne le record associe a un noeud
    public EOGenericRecord record() {
        return nodeRecord;
    }
    // definit le champ key de l'enregistrement
    public void setKey(String keyValue) {
        key = keyValue;
    }
    // retourne la valeur contenue dans le champ defini par key
    // ou null si celui-ci n'est pas defini
    public Object keyValue() {
        if(key != null) {
            return nodeRecord.valueForKey(key);
        }
        else {
            return null;
        }
    }
    // definit la valeur du champ contenant la cle pere
    public void setParentKey(String parentKeyValue) {
        parentKey = parentKeyValue;
    }
    // retourne la valeur contenue dans le champ defini par parentKey ou
    // null s'il n'est pas defini
    public Object parentKeyValue() {
        if(parentKey != null) {
            return nodeRecord.valueForKey(parentKey);
        }
        else {
            return null;
        }
    }
    // retourne la valeur contenue dans le champ defini par fieldForDisplay
    // ou null s'il n'est pas defini
    public Object displayValue() {
        if(fieldForDisplay != null) {
            return nodeRecord.valueForKey(fieldForDisplay);
        }
        else {
            return null;
        }
    }

}
