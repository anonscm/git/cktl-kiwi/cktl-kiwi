package org.cocktail.kiwi.common.utilities;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import org.cocktail.application.client.swing.ZUiUtil;
import org.cocktail.kiwi.client.ApplicationIcones;

public class MsgPanel 
{
	private static MsgPanel sharedInstance;
		
	private JPanel mainPanel;
	private	JDialog mainWindow;
	private JScrollPane scrollPane;

	private ActionOk actionOk = new ActionOk();
	
    private		JLabel				icone;
    private 	JTextArea			messageInfo;
    
    /**
     * 
     *
     */
    public MsgPanel() {
        super();

        initView();

    }
       
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static MsgPanel sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new MsgPanel();
		return sharedInstance;
	}
	
		
    /**
     * 
     *
     */
    public void initView() {
    	
		mainWindow = new JDialog(new JDialog(), "", true);
        mainWindow.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		mainPanel = new JPanel(new BorderLayout());
		mainPanel.setPreferredSize(new Dimension(430, 230));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(4,15,4,4));

		// Center Panel (Message)
		JPanel centerPanel  =new JPanel(new BorderLayout());
		centerPanel.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));

		messageInfo = new JTextArea();
		messageInfo.setBorder(BorderFactory.createEtchedBorder());
		messageInfo.setFont(new Font("Arial", Font.PLAIN, 12));
		messageInfo.setFocusable(false);
		messageInfo.setEditable(false);
		messageInfo.setLineWrap(true);
		messageInfo.setAutoscrolls(true);
		messageInfo.setWrapStyleWord(true);

		scrollPane = new JScrollPane(messageInfo);

		centerPanel.add(scrollPane, BorderLayout.CENTER);

		// Left Panel (Icone)
		JPanel leftPanel  =new JPanel(new BorderLayout());
		leftPanel.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));

		icone = new JLabel("");		
		icone.setFocusable(false);

		leftPanel.add(icone, BorderLayout.NORTH);

		// South Panel
		JPanel southPanel = new JPanel(new BorderLayout());
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionOk);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 90, 21));                
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));
		
		southPanel.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		southPanel.add(new JSeparator(), BorderLayout.NORTH);
		southPanel.add(panelButtons, BorderLayout.EAST);

		mainPanel.add(leftPanel, BorderLayout.WEST);
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainPanel);
		mainWindow.pack();
		ZUiUtil.centerWindow(mainWindow);
    }
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runConfirmationDialog(String titre, String message) {

    	icone.setIcon(ApplicationIcones.ICON_ALERT_OK);
    	
    	mainWindow.setTitle(titre);
        messageInfo.setBackground(new Color(153,238, 138));

        messageInfo.setText(message);
        mainWindow.show();
    }
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runInformationDialog(String titre, String message) {

    	icone.setIcon(ApplicationIcones.ICON_ALERT_INFO);
    	
    	mainWindow.setTitle(titre);
        messageInfo.setBackground(new Color(251,236,208));

        messageInfo.setText(message);
        mainWindow.show();
    }
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runLargeInformationDialog(String titre, String message) {

		mainPanel.setPreferredSize(new Dimension(450,400));
		mainWindow.setContentPane(mainPanel);
		mainWindow.pack();

    	icone.setIcon(ApplicationIcones.ICON_ALERT_INFO);
    	
    	mainWindow.setTitle(titre);
        messageInfo.setBackground(new Color(251,236,208));

        messageInfo.setText(message);
        mainWindow.show();
    }
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runLargeErrorDialog(String titre, String message) {

		mainPanel.setPreferredSize(new Dimension(600, 400));
		mainWindow.setContentPane(mainPanel);
		mainWindow.pack();

    	icone.setIcon(ApplicationIcones.ICON_ALERT_ERROR);

    	mainWindow.setTitle(titre);

        messageInfo.setBackground(new Color(238, 153, 138));
        messageInfo.setText(message);
        mainWindow.show();
    }
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runErrorDialog(String titre, String message) {

    	icone.setIcon(ApplicationIcones.ICON_ALERT_ERROR);

    	mainWindow.setTitle(titre);

        messageInfo.setBackground(new Color(238, 153, 138));
        messageInfo.setText(message);
        mainWindow.show();
    }
    
    /**
     * 
     * @param sender
     */
    public void ok(Object sender) {
    	mainWindow.hide();
    }
    
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionOk extends AbstractAction {
		
		public ActionOk() {
			super("OK");
			this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_VALID);
		}
		
		public void actionPerformed(ActionEvent e) {			
			mainWindow.dispose();
		}
	}
	
   
}