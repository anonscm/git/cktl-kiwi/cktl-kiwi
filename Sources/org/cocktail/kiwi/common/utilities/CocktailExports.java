/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kiwi.common.utilities;

import java.math.BigDecimal;

import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.foundation.NSTimestamp;

public class CocktailExports extends EOModalDialogController 
{
	public static final String ENCODAGE_ISO_8859_1 = "ISO-8859-1";
	public static final String ENCODAGE_UTF8 = "UTF-8";

	public static final String SAUT_DE_LIGNE = "\n";
	
	public static final String SEPARATEUR_EXPORT = ";";
	public static final String SEPARATEUR_TABULATION = "\t";

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String ajouterChamp(String value) {

		if (value == null)
			return SEPARATEUR_EXPORT;

		String retour = StringCtrl.replace (value, ";", " ");
		return retour + SEPARATEUR_EXPORT;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String ajouterChampVide(int nbChamps) {
		String retour = "";
		for (int i=0;i<nbChamps;i++)
			retour += SEPARATEUR_EXPORT;
		
		return retour;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String ajouterChampVide() {
		return ajouterChampVide(1);
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String ajouterChampDate(NSTimestamp value) {
		return ajouterChampDate(value,  "%d/%m/%Y");
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String ajouterChampDate(NSTimestamp value, String format) {

		if (value == null)
			return ajouterChampVide();

		return DateCtrl.dateToString(value, format) + SEPARATEUR_EXPORT;
	}
	
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String ajouterChampDecimal(BigDecimal value) {

		if (value == null)
			return ajouterChampVide();

		return StringCtrl.replace(value.toString(), ".", ",") + SEPARATEUR_EXPORT;
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String ajouterChampNumber(Number value) {

		if (value == null)
			return ajouterChampVide();

		return StringCtrl.replace(value.toString(), ".", ",") + SEPARATEUR_EXPORT;
	}
}