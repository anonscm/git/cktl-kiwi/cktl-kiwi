//
//ServerRequest.java
//TestThread
//
//Created by Christine Buttin on Mon Jun 28 2004.
//Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
package org.cocktail.kiwi.common.utilities;

import java.util.TimerTask;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotificationCenter;

public class ServerRequest extends TimerTask {
private EODistributedObjectStore store;
private EOEditingContext editingContext;

public ServerRequest(EODistributedObjectStore aStore,EOEditingContext ec,ThreadInterfaceController interf) {
	super();
	store = aStore;
	editingContext = ec;
}

public void run() {
	try {
		String message = (String)store.invokeRemoteMethodWithKeyPath(editingContext,"session","clientSideRequestMessage", null,null,false);
		if (message != null) {
			if (message.equals("termine")) {
				NSDictionary resultatAction = (NSDictionary)store.invokeRemoteMethodWithKeyPath(editingContext,"session","clientSideRequestResultatAction", null,null,false);
				// verifier si une exception c'est produite
				message = extraireInformationDiagnostic(resultatAction,"exception");
				if (message != null) {
					NSNotificationCenter.defaultCenter().postNotification("erreurArrivee",message);
					cancel();
				} else {
					// poster le resultat obtenu via une notification si il existe un resultat
					if (resultatAction.objectForKey("resultat") != null) {
							NSNotificationCenter.defaultCenter().postNotification("resultatObtenu",resultatAction.objectForKey("resultat"));
					}
					cancel();
				}
				NSNotificationCenter.defaultCenter().postNotification("threadTermine",null);
			} else {
				NSNotificationCenter.defaultCenter().postNotification("messageInformation",message);
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
}

// methodes privees
private String extraireInformationDiagnostic(NSDictionary dictionnaire,String cle) {
	NSDictionary diagnostic = (NSDictionary)dictionnaire.objectForKey("diagnostic");
	if (diagnostic != null) {
		return (String)diagnostic.objectForKey(cle);
	} else {
		return null;
	}
}
}
