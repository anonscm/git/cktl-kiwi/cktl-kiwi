//
//ThreadInterfaceController.java
//TestThread
//
//Created by Christine Buttin on Tue Jun 29 2004.
//Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
//Contient les methodes pour la preparation d'un traitement asynchrone cote client
//Utilise les timers Java

package org.cocktail.kiwi.common.utilities;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public abstract class ThreadInterfaceController extends EOInterfaceController {
	
private EOEditingContext ec;	
	
public ThreadInterfaceController() {
    super();
}

/**
 * 
 * @param substitutionEditingContext
 */
public ThreadInterfaceController(EOEditingContext substitutionEditingContext) {
    super(substitutionEditingContext);
}


/**
 * 
 * @param editingContext
 */
public void preparerPourTraitementAsynchrone(EOEditingContext editingContext) {
	
	ec =editingContext();
	
	EODistributedObjectStore store = (EODistributedObjectStore)ec.parentObjectStore();

	enregistrerPourNotifications(this);	
	Thread.yield();
	java.util.Timer timer = new java.util.Timer();
	ServerRequest serverTask = new ServerRequest(store,ec,this);
	timer.schedule(serverTask,1,1);
}


public void preparerPourTraitementAsynchrone() {
	
	EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
// s'inscrire pour recevoir une notification
	enregistrerPourNotifications(this);	
	// declencher un timer pour scheduler une task
	java.util.Timer timer = new java.util.Timer();
	ServerRequest serverTask = new ServerRequest(store,editingContext(),this);
	timer.schedule(serverTask,0,1);
}



/** enregistrer un objet pour qu'il re?oive les notifications */
public void enregistrerPourNotifications(Object demandeur) {
	NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("recupererResultat",new Class [] {NSNotification.class}),"resultatObtenu",null);
	NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("recupererMessage",new Class [] {NSNotification.class}),"messageInformation",null);
	NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("terminerTraitement",new Class [] {NSNotification.class}),"threadTermine",null);
	NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("recupererErreur",new Class [] {NSNotification.class}),"erreurArrivee",null);
}   
/** supprimer la r&eacute;ception des notifications */
public void supprimerPourNotifications(Object demandeur) {
	NSNotificationCenter.defaultCenter().removeObserver(demandeur);
}		

/** retourne la classe du r&eacute;sultat de l'action
*/
public String classeResultatAction(NSNotification aNotif) {
	NSDictionary resultat = (NSDictionary)aNotif.object();
	if (resultat != null) {
		return (String)resultat.objectForKey("classe");
	} else {
		return null;
	}
}
/** retourne le du r&eacute;sultat de l'action
*/
public Object objetResultatAction(NSNotification aNotif) {
	NSDictionary resultat = (NSDictionary)aNotif.object();
	if (resultat != null) {
		return resultat.objectForKey("valeur");
	} else {
		return null;
	}
}
/** retourne un message envoy&eacute; par le serveur
*/
public String message(NSNotification aNotif) {
	return (String)aNotif.object();
}

/** ces m?thodes doivent ?tre d?finies par la sous-classe */
public abstract void recupererMessage(NSNotification aNotif);
public abstract void recupererResultat(NSNotification aNotif);
public abstract void recupererErreur(NSNotification aNotif);
public abstract void terminerTraitement(NSNotification aNotif);
}