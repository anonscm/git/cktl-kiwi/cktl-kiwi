<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="kiwi_budget"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="ETABLISSEMENT" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""
]]></defaultValueExpression>
	</parameter>
	<parameter name="REP_BASE_PATH" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="EXERCICE" isForPrompting="true" class="java.lang.Integer">
		<defaultValueExpression ><![CDATA[new Integer("2009")]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT o.org_ub UB, o.org_cr CR, o.org_souscr SOUS_CR, sum(mpe.mpe_montant_budgetaire) MONTANT, sum(mpe.mpe_montant_rembourse) MONTANT_REMB
from jefy_mission.mission m, jefy_admin.organ o, jefy_mission.mission_paiement mp, jefy_mission.mission_paiement_engage mpe,
jefy_depense.engage_budget eb
where m.exe_ordre = $P{EXERCICE} and m.mis_ordre = mp.mis_ordre and mp.mip_ordre = mpe.mip_ordre and mpe.org_id = o.org_id
and mpe.exe_ordre = $P{EXERCICE}
and mpe.eng_id = eb.eng_id
and m.mis_etat in ('VALIDE', 'PAYEE', 'LIQUIDEE')
group by o.org_ub,o.org_cr, o.org_souscr
order by o.org_ub,o.org_cr, o.org_souscr]]></queryString>

	<field name="UB" class="java.lang.String"/>
	<field name="CR" class="java.lang.String"/>
	<field name="SOUS_CR" class="java.lang.String"/>
	<field name="MONTANT" class="java.math.BigDecimal"/>
	<field name="MONTANT_REMB" class="java.math.BigDecimal"/>

	<variable name="SUM_LOLF" class="java.math.BigDecimal" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$F{MONTANT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="SUM_UB" class="java.math.BigDecimal" resetType="Group" resetGroup="COMP" calculation="Sum">
		<variableExpression><![CDATA[$F{MONTANT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="SUM_SOUS_CR" class="java.math.BigDecimal" resetType="Group" resetGroup="SOUS_CR" calculation="Sum">
		<variableExpression><![CDATA[$F{MONTANT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="SUM_CR" class="java.math.BigDecimal" resetType="Group" resetGroup="CR" calculation="Sum">
		<variableExpression><![CDATA[$F{MONTANT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="V_SUM_MONTANT" class="java.math.BigDecimal" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$F{MONTANT}]]></variableExpression>
	</variable>

		<group  name="COMP" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{UB}]]></groupExpression>
			<groupHeader>
			<band height="16"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="1"
						y="0"
						width="454"
						height="16"
						forecolor="#000000"
						backcolor="#FFCCCC"
						key="textField-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $F{UB}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="COMP"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="455"
						y="0"
						width="80"
						height="16"
						backcolor="#FFCCCC"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{SUM_UB}.toString()+" €"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="CR" >
			<groupExpression><![CDATA[$F{CR}]]></groupExpression>
			<groupHeader>
			<band height="15"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="20"
						y="2"
						width="435"
						height="13"
						backcolor="#CCCCFF"
						key="textField-5"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{CR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="CR"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="455"
						y="2"
						width="80"
						height="13"
						backcolor="#CCCCFF"
						key="textField-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{SUM_CR}.toString()+" €"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="SOUS_CR" >
			<groupExpression><![CDATA[$F{SOUS_CR}]]></groupExpression>
			<groupHeader>
			<band height="16"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="41"
						y="2"
						width="407"
						height="13"
						backcolor="#FFFFFF"
						key="textField-6">
							<printWhenExpression><![CDATA[new Boolean($F{SOUS_CR} != null)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{SOUS_CR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="SOUS_CR"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="448"
						y="2"
						width="86"
						height="13"
						backcolor="#FFFFFF"
						key="textField-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{SUM_SOUS_CR}.toString() + " €"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="42"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="448"
						height="11"
						key="textField-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{ETABLISSEMENT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="448"
						y="0"
						width="83"
						height="11"
						key="textField-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="15"
						width="534"
						height="22"
						backcolor="#CCCCCC"
						key="textField-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="1Point" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000" bottomPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica" size="14" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["SUIVI BUDGET MISSIONS  - Exercice : "+$P{EXERCICE}]]></textFieldExpression>
				</textField>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="7"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="26"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="1"
						y="5"
						width="534"
						height="21"
						backcolor="#CCFFCC"
						key="textField"/>
					<box topBorder="1Point" topBorderColor="#000000" leftBorder="1Point" leftBorderColor="#000000" rightBorder="1Point" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["TOTAL : " + $V{V_SUM_MONTANT}.toString() + " €"]]></textFieldExpression>
				</textField>
			</band>
		</summary>
</jasperReport>
